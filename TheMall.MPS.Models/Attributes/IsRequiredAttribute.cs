﻿using System;

namespace TheMall.MPS.Models.Attributes
{
    public class IsRequiredAttribute : Attribute
    {
         public bool IsRequired { get; set; }
         public IsRequiredAttribute(bool isRequired = true)
            : base()
        {
            IsRequired = isRequired;
        }
    }
}
