﻿using System;
using System.Reflection;

namespace TheMall.MPS.Models.Attributes
{
    public class ResourceDescriptionAttribute : Attribute
    {
        public string Description { get; set; }
        public ResourceDescriptionAttribute(Type resourceType, string resourceName)
            : base()
        {
            Description = ResourceHelper.GetResourceLookup(resourceType, resourceName);
        }
        
    }
    class ResourceHelper
    {
        public static string GetResourceLookup(Type resourceType, string resourceName)
        {
            if (resourceType != null && !string.IsNullOrEmpty(resourceName))
            {
                PropertyInfo property = resourceType.GetProperty(resourceName, BindingFlags.Public | BindingFlags.Static);
                if (property == null)
                {
                    throw new InvalidOperationException(string.Format("Resource type {0} does not have property {1}.", resourceType.Name, resourceName));
                }
                if (property.PropertyType != typeof(string))
                {
                    throw new InvalidOperationException(string.Format("Resource type {0} property {1} is not a string.", resourceType.Name, resourceName));
                }
                return (string)property.GetValue(null, null);
            }
            return null;
        }
    }
}
