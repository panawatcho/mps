﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrossingSoft.Framework.Models.Interfaces;
using TheMall.MPS.Models.Abstracts;
using TheMall.MPS.Models.Memo;
using TheMall.MPS.Models.Proposal;

namespace TheMall.MPS.Models.CashAdvance
{
    public class MPS_CashAdvanceComment : BaseCommentTable
    {
        [ForeignKey("ParentId")]
        public virtual MPS_CashAdvanceTable MpsAdvanceTable { get; set; }
    }
}
