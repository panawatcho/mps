﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrossingSoft.Framework.Models.Interfaces;
using TheMall.MPS.Models.Abstracts;

namespace TheMall.MPS.Models.CashAdvance
{
   public class MPS_CashAdvanceLine : MpsLineTable
    {
        
        public string Description { get; set; }
        public decimal Unit { get; set; }
        public decimal Amount { get; set; }
        public string Remark { get; set; }
        public decimal? Vat { get; set; }
        public decimal? Tax { get; set; }
        public decimal? NetAmount { get; set; }
        [ForeignKey("ParentId")]
        public virtual MPS_CashAdvanceTable CashAdvanceTable { get; set; }
        public override IWorkflowTable Parent
        {
            get { return CashAdvanceTable; }
        }
    }
}
