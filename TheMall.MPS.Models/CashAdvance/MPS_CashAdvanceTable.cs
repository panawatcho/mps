﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.Models.Abstracts;
using TheMall.MPS.Models.CashClearing;
using TheMall.MPS.Models.PettyCash;
using TheMall.MPS.Models.Proposal;

namespace TheMall.MPS.Models.CashAdvance
{
    public class MPS_CashAdvanceTable : MpsWorkflowTable
    {

        [StringLength(Metadata.ExpenseTopicCode)]
        public string ExpenseTopicCode { get; set; }
       [StringLength(Metadata.APCode)]
        public string APCode { get; set; }
        public DateTime? AdvanceDate { get; set; }
        public DateTime? AdvanceDueDate { get; set; }
        [StringLength(100)]
        public string Branch { get; set; }
        public decimal Budget { get; set; }
        public string Description { get; set; }
        public string RequestForEmpId { get; set; }
        public int ProposalRefID { get; set; }
        public int? RePrintMemoNo { get; set; }
        [StringLength(Metadata.DocumentNumberLength)]
        public string ProposalRefDocumentNumber { get; set; }
       
        [StringLength(Metadata.UsernameLength)]
        public string RequestForUserName{ get; set; }
        
        public int ProposalLineID { get; set; }
        [ForeignKey("ProposalLineID")]
        public virtual MPS_ProposalLine ProposalLine { get; set; }
        public virtual ICollection<MPS_CashAdvanceLine> CashAdvanceLine { get; set; }
        public virtual ICollection<MPS_CashAdvanceApproval> CashAdvanceApproval { get; set; }
        public virtual ICollection<MPS_CashAdvanceComment> Comments { get; set; }
        public virtual ICollection<MPS_CashAdvanceAttachment> Attachments { get; set; }

        public string VendorCode { get; set; }

    }
}
