﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrossingSoft.Framework.Models.Interfaces;
using TheMall.MPS.Models.Abstracts;
using TheMall.MPS.Models.Memo;

namespace TheMall.MPS.Models.CashAdvance
{
    public class MPS_CashAdvanceAttachment : BaseAttachmentTable
    {
        [ForeignKey("ParentId")]
        public virtual MPS_CashAdvanceTable MpsAdvanceTable { get; set; }
    }
}
