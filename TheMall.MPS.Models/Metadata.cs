﻿namespace TheMall.MPS.Models
{
    public static class Metadata
    {
        public const int StupidNameLength = 100;

        public const int ActionLength = 100;
        public const int ActivityLength = 100;

        public const int DimensionLength = 100;
        public const int DocumentNumberLength = 20;

        public const int MailContentIdLength = 50;
        public const int MailContentNameLength = 100;
        public const int MailTemplateIdLength = 50;
        public const int MailTemplateNameLength = 100;

        public const int NumberSeqCodeLength = 50;

        public const int ProcessCodeLength = 50;
        public const int ProcessNameLength = 100;

        public const int RefererIdLength = 500;

        public const int SerialNumberLength = 20;
        
        public const int UsernameLength = 100;
        public const int EmployeeId = 10;
        public const int UserDisplayNameLength = 200;
        public const int Email = 200;

        public const int MPSProcessing = 20;

        //Master
        public const int UnitCode = 20;
        public const int UnitName = 1000;
        public const int TypeProposalCode = 10;
        public const int PlaceCode = 100;
        public const int ExpenseTopicCode = 20;
        public const int ExpenseTopicName = 200;
        public const int TypeMemoCode = 50;
        public const int APCode = 20;
        public const int APName = 200;
        public const int BudgetYearID = 15;
        public const int BranchCode = 100;

        // main table
        public const int ApprovalLevel = 50;

    }
}
