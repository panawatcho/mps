﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrossingSoft.Framework.Models.Interfaces;
using TheMall.MPS.Models.Abstracts;
using TheMall.MPS.Models.Interfaces;
using TheMall.MPS.Models.Master;

namespace TheMall.MPS.Models.Receipt
{
    public class MPS_ReceiptLine : BaseTrackableModel, IMasterTable
    {
        [Key]
        public int Id { get; set; }
        public float LineNo { get; set; }
        public string Description { get; set; }
        public decimal Unit { get; set; }
        public decimal Amount { get; set; }
        public string Remark { get; set; }
        public decimal? Vat { get; set; }
        public decimal? Tax { get; set; }
        public decimal? NetAmount { get; set; }
        public decimal? NetAmountNoVatTax { get; set; }
        public int ReceiptTableId { get; set; }
        [ForeignKey("ReceiptTableId")]
        public virtual MPS_ReceiptTable ReceiptTable { get; set; }
        public bool Deleted { get; set; }
        public DateTime? DeletedDate { get; set; }
        public string DeletedBy { get; set; }
        public string DeletedByName { get; set; }

    }
}
