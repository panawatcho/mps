﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrossingSoft.Framework.Models.Interfaces;
using TheMall.MPS.Models.Abstracts;
using TheMall.MPS.Models.Interfaces;
using TheMall.MPS.Models.Master;

namespace TheMall.MPS.Models.Receipt
{
    public class MPS_ReceiptTable : BaseTrackableModel
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [StringLength(Metadata.UsernameLength)]
        public string RequesterUserName { get; set; }
        [Required]
        [StringLength(Metadata.UserDisplayNameLength)]
        public string RequesterName { get; set; }
        [StringLength(Metadata.UnitCode)]
        public string UnitCode { get; set; }
        [ForeignKey("UnitCode")]
        public virtual MPS_M_UnitCode Unitcode { get; set; }
        [StringLength(Metadata.UnitName)]
        public string UnitName { get; set; }
        [StringLength(Metadata.UsernameLength)]
        public string RequestForUserName { get; set; }
        [StringLength(Metadata.UserDisplayNameLength)]
        public string RequesterForName { get; set; }
        [StringLength(Metadata.UnitCode)]
        public string UnitCodeFor { get; set; }
        [StringLength(Metadata.UnitName)]
        public string UnitNameFor { get; set; }
        public virtual ICollection<MPS_ReceiptLine> ReceiptLine { get; set; }
        public decimal BudgetNoVatTax { get; set; }
        public decimal BudgetDetail { get; set; }
        [StringLength(Metadata.DocumentNumberLength)]
        public string DocumentNumber { get; set; }
        [StringLength(100)]
        public string Status { get; set; }
        public int StatusFlag { get; set; }
        public int? RePrintNo { get; set; }
        public DateTime? CompletedDate { get; set; }
        [StringLength(Metadata.UsernameLength)]
        public string CompletedByUserName { get; set; }
        [StringLength(Metadata.UserDisplayNameLength)]
        public string CompletedByName { get; set; }
        public bool Deleted { get; set; }
        public DateTime? DeletedDate { get; set; }
        public string DeletedBy { get; set; }
        public string DeletedByName { get; set; }
    }
}
