﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrossingSoft.Framework.Models.Interfaces;
using TheMall.MPS.Models.Abstracts;
using TheMall.MPS.Models.Memo;

namespace TheMall.MPS.Models.Memo
{
    public class MPS_MemoAttachment : BaseAttachmentTable
    {
        [ForeignKey("ParentId")]
        public virtual MPS_MemoTable MpsMemoTable { get; set; }
    }
}
