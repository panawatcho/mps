﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.Models.Abstracts;
using TheMall.MPS.Models.Proposal;

namespace TheMall.MPS.Models.Memo
{
    public class MPS_MemoTable : MpsWorkflowTable
    {
        [StringLength(Metadata.ExpenseTopicCode)]
        public string ExpenseTopicCode { get; set; }

        [StringLength(Metadata.APCode)]
        public string APCode { get; set; }
        public decimal? BudgetAPCode { get; set; }
        public decimal? BudgetDetail { get; set; }
        [StringLength(50)]
        public string MemoType { get; set; }
        public int ProposalRefID { get; set; }
        [StringLength(Metadata.DocumentNumberLength)]
        public string ProposalRefDocumentNumber { get; set; }
        public int? UseProposalID { get; set; }
        [StringLength(Metadata.DocumentNumberLength)]
        public string UseProposalDocumentNumber { get; set; }
        [StringLength(100)]
        public string PaymentName { get; set; }
           [StringLength(200)]
        public string PaymentDepartment { get; set; }
        public string Description { get; set; }
       
        public int ProposalLineID { get; set; }

        [ForeignKey("ProposalLineID")]
        public virtual MPS_ProposalLine ProposalLine { get; set; }
        public virtual ICollection<MPS_MemoLine> MemoLine { get; set; }
        public virtual ICollection<MPS_MemoApproval> MemoApproval { get; set; }
        public virtual ICollection<MPS_MemoComment> Comments { get; set; }
        public virtual ICollection<MPS_MemoAttachment> Attachments { get; set; }
        [StringLength(Metadata.UnitCode)]
        public string PayToUnitCode { get; set; }
        public bool SpecialType  { get; set; }

        [StringLength(100)]
        public string Branch { get; set; }
        public bool ActualCharge { get; set; }
        public int? IncomeDepositId { get; set; }

        [ForeignKey("IncomeDepositId")]
        public virtual MPS_IncomeDeposit IncomeDeposit { get; set; }
    }
}
