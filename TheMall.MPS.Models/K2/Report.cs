//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TheMall.MPS.Models.K2
{
    using System;
    using System.Collections.Generic;
    
    public partial class Report
    {
        public int ID { get; set; }
        public string ReportName { get; set; }
        public string ReportDescr { get; set; }
        public string ReportXML { get; set; }
        public string TemplXML { get; set; }
        public string CustomPath { get; set; }
        public Nullable<bool> ViewReport { get; set; }
    }
}
