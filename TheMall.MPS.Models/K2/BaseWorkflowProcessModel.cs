﻿namespace TheMall.MPS.Models.K2
{
    public interface IWorkflowProcessModel
    {
        string ProcessFullPath { get; }
        string AttachmentLibrary { get; }
        string PrimaryKeyDataFieldName { get; }
        string TitleDataFieldName { get; }
        string ProcessCodeDataFieldName { get; }
        string ProcessCode { get; }
        string ExceptionDataFieldName { get; }
        string Dimension1 { get; set; }
        string Dimension2 { get; set; }
        string Dimension3 { get; set; }
    }
    public abstract class BaseWorkflowProcessModel : IWorkflowProcessModel
    {
        public const string PRIMARYKEY_DATAFIELD = "DFWorkflowTableId";
        public const string TITLE_DATAFIELD = "DFTitle";
        public const string PROCESSCODE_DATAFIELD = "DFProcessCode";
        public const string EXCEPTION_DATAFIELD = "DFException";

        public abstract string ProcessFullPath { get; }
        public string AttachmentLibrary { get; private set; }

        public virtual string PrimaryKeyDataFieldName
        {
            get { return PRIMARYKEY_DATAFIELD; }
        }

        public virtual string TitleDataFieldName
        {
            get { return TITLE_DATAFIELD; }
        }

        //public string ProcessCodeDataFieldName { get; private set; }
        public string ProcessCodeDataFieldName { get { return PROCESSCODE_DATAFIELD; } }
        public abstract string ProcessCode { get; }

        public virtual string ExceptionDataFieldName
        {
            get { return EXCEPTION_DATAFIELD; }
        }

        public string Dimension1 { get; set; }
        public string Dimension2 { get; set; }
        public string Dimension3 { get; set; }
    }
}
