//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TheMall.MPS.Models.K2
{
    using System;
    using System.Collections.Generic;
    
    public partial class ProcView
    {
        public int ID { get; set; }
        public int ProcID { get; set; }
        public string Name { get; set; }
        public byte[] Data { get; set; }
        public bool Default { get; set; }
    }
}
