﻿using System;
using System.Collections.Generic;

namespace TheMall.MPS.Models.K2
{
    public class ProcInstCustom
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Guid Guid { get; set; }
        public int ExpectedDuration { get; set; }
        public string Folio { get; set; }
        public int Priority { get; set; }
        public DateTime StartDate { get; set; }

        public IReadOnlyList<ProcInstDataCustom> DataFields { get; set; }
        public Actioner Originator { get; set;}
    }
}
