﻿using System.Collections.Generic;

namespace TheMall.MPS.Models.K2
{
    public sealed class ProcInstStatus
    {
        public int ProcInstId { get; set; }
        public string Folio { get; set; }
        public string ProcDescription { get; set; }
        //public string ActivityName { get; set; }
        //public string EventName { get; set; }
        //public DateTime ActInstStartDate { get; set; }
        public IEnumerable<ActInstStatus> ActInsts { get; set; }
    }
}
