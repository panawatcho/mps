﻿using System;

namespace TheMall.MPS.Models.K2
{
    public sealed class ActInstStatus
    {
        public int ActInstId { get; set; }
        public string ActivityName { get; set; }
        public string ActivityDescription { get; set; }
        public int ActInstDestId { get; set; }
        public string ActInstDestFqn { get; set; }
        public string EventName { get; set; }
        public DateTime EventStartDate { get; set; }
        public int EventExpectedDuration { get; set; }
        public DateTime EventDueDate { get; set; }
    }
}
