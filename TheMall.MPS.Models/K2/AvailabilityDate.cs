//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TheMall.MPS.Models.K2
{
    using System;
    using System.Collections.Generic;

    public partial class AvailabilityDate : CrossingSoft.Framework.Infrastructure.IObjectState
    {
        public System.Guid DateID { get; set; }
        public System.Guid ZoneID { get; set; }
        public short Month { get; set; }
        public short Day { get; set; }
        public string Description { get; set; }
        public System.DateTime TimeOfDay { get; set; }
        public int Duration { get; set; }
        public Nullable<bool> IsNonWorkDate { get; set; }

        [System.ComponentModel.DataAnnotations.Schema.NotMapped]
        public CrossingSoft.Framework.Infrastructure.ObjectState ObjectState { get; set; }
    }
}
