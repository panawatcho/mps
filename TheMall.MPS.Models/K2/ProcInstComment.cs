//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TheMall.MPS.Models.K2
{
    using System;
    using System.Collections.Generic;
    
    public partial class ProcInstComment
    {
        public int ID { get; set; }
        public int ProcInstID { get; set; }
        public int ActInstDestID { get; set; }
    
        public virtual Comment Comment { get; set; }
        public virtual ProcInst1 ProcInst1 { get; set; }
    }
}
