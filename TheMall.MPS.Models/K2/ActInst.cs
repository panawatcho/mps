//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TheMall.MPS.Models.K2
{
    using System;
    using System.Collections.Generic;
    
    public partial class ActInst
    {
        public int ProcInstID { get; set; }
        public int ID { get; set; }
        public int ActID { get; set; }
        public byte Status { get; set; }
        public System.DateTime StartDate { get; set; }
        public System.DateTime FinishDate { get; set; }
        public int Slots { get; set; }
        public int Priority { get; set; }
        public int ExpectedDuration { get; set; }
        public byte PrecRuleResult { get; set; }
        public byte StartRuleResult { get; set; }
        public byte DestRuleResult { get; set; }
    }
}
