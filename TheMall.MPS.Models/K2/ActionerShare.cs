//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TheMall.MPS.Models.K2
{
    using System;
    using System.Collections.Generic;
    
    public partial class ActionerShare
    {
        public int ID { get; set; }
        public int ActionerID { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public int Type { get; set; }
    }
}
