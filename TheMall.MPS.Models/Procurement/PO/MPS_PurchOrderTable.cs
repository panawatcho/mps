﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.Models.Abstracts;
using TheMall.MPS.Models.Procurement.PR;

namespace TheMall.MPS.Models.Procurement.PO
{
    public class MPS_PurchOrderTable : BaseTrackableModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int PO_ID { get; set; }
        [StringLength(200)]
        public string PO_NUM { get; set; }
        [StringLength(200)]
        public string IP_ID { get; set; }
        public int REQ_ID { get; set; }
        [StringLength(200)]
        public string PR_NUM { get; set; }
        public DateTime?  PO_DATE { get; set; }
        public bool? CLOSE_PO { get; set; }
        public bool ClosePO { get; set; }
        [StringLength(200)]
        public string Status { get; set; }
        public int StatusFlag { get; set; }

        public virtual ICollection<MPS_PurchOrderLine> PurchOrderLines { get; set; }
        [ForeignKey("REQ_ID")]
        public virtual MPS_PurchReqTable PurchReqTable { get; set; }
    }
}
