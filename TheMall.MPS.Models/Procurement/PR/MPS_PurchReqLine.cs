﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrossingSoft.Framework.Models.Interfaces;
using TheMall.MPS.Models.Abstracts;

namespace TheMall.MPS.Models.Procurement.PR
{
    public class MPS_PurchReqLine : BaseTrackableModel, IPseudoDelete
    {
        [Key, Column(Order = 0)]
        public int REQ_ID { get; set; }
        //REQ_ITEMS
        [Key, Column(Order = 1)]
        public int ITEM_NUM { get; set; }
        public decimal? QUANTITY { get; set; }
        [StringLength(1000)]
        public string ITEM_DESC { get; set; }
        [StringLength(200)]
        public string UOM { get; set; }
        [StringLength(200)]
        public string OLD_PRICE { get; set; }
        public decimal? PRICE { get; set; }
        [StringLength(200)]
        public string OLD_TAX_AMOUNT { get; set; }
        public decimal? TAX_AMOUNT { get; set; }
        [StringLength(200)]

        public string OLD_CURRENCY_CODE { get; set; }
        [StringLength(200)]
        public string CURRENCY_CODE { get; set; }
        public DateTime? DELIV_DATE { get; set; }
        [StringLength(1000)]
        public string LONGDESC { get; set; }
        [StringLength(200)]
        public string SHIP_INST { get; set; }
        [StringLength(1000)]
        public string PAYMENTDESC { get; set; }
        public decimal ACTUALPRICE { get; set; }
        //REQ_ACCT_ASSGNS
        [StringLength(200)]
        public string COSTCENTER { get; set; }
        [StringLength(200)]
        public string PODOCTYPE { get; set; }
        [StringLength(200)]
        public string PURTYPE { get; set; }
        [StringLength(200)]
        public string GLACCOUNT { get; set; }
        [StringLength(200)]
        public string DEPARTMENT { get; set; }
        [StringLength(200)]
        public string PURGROUP { get; set; }
        [StringLength(200)]
        public string PROPOSALID { get; set; }
        [StringLength(200)]
        public string APCODE { get; set; }
        [StringLength(255)]
        public string APDESC { get; set; }
        [StringLength(200)]
        public string REQUESTFORID { get; set; }
        [StringLength(200)]
        public string OLD_BUDGET_CODE { get; set; }
        [StringLength(200)]
        public string OLD_ORG_ID { get; set; }
        public int StatusFlag { get; set; }
        public bool Deleted { get; set; }
        public DateTime? DeletedDate { get; set; }
        [StringLength(100)]
        public string DeletedBy { get; set; }
        [StringLength(200)]
        public string DeletedByName { get; set; }
        [ForeignKey("REQ_ID")]
        public virtual MPS_PurchReqTable PurchReqTable { get; set; }
    }
}
