﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.Models.Abstracts;

namespace TheMall.MPS.Models.Procurement.PR
{
    public class MPS_PurchReqTable : BaseTrackableModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int REQ_ID { get; set; }
        [StringLength(200)]
        public string IP_ID { get; set; }
        [StringLength(200)]
        public string PR_NUM { get; set; }
        [StringLength(100)]
        public string SUBCOMMAND { get; set; }
        [StringLength(200)]
        public string REQUISITIONER { get; set; }
        [StringLength(200)]
        public string BS_REQID { get; set; }
        [StringLength(200)]
        public string REQ_NAME { get; set; }
        [StringLength(200)]
        public string COMPANY_CODE { get; set; }
        public DateTime? POST_DATE { get; set; }
        [StringLength(200)]
        public string Status { get; set; }
        public int StatusFlag { get; set; }

        public virtual ICollection<MPS_PurchReqLine> PurchReqLines { get; set; }
    }
}
