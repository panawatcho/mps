﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.Models.Abstracts;
using TheMall.MPS.Models.Proposal;

namespace TheMall.MPS.Models.Procurement
{
    public class MPS_ProcurementTable : MpsWorkflowTable
    {
        [StringLength(2)]
        public string ProcurementType { get; set; }
        public string Process { get; set; }
    
        public decimal BudgetAPCode { get; set; }
        public int ProposalRefID { get; set; }
        [StringLength(Metadata.DocumentNumberLength)]
        public string ProposalRefDocumentNumber { get; set; }
        [StringLength(Metadata.BranchCode)]
        public string BranchCode { get; set; }
        public string Description { get; set; }
        public int ProposalLineID { get; set; }

        [ForeignKey("ProposalLineID")]
        public virtual MPS_ProposalLine ProposalLine { get; set; }
        [StringLength(50)]
        public string PR_Number { get; set; }
        [StringLength(50)]
        public string PO_Number { get; set; }
        public decimal? PR_Amount { get; set; }
        public decimal? PO_Amount { get; set; }
    }
}
