﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.Models.Abstracts;
using TheMall.MPS.Models.Procurement.PO;

namespace TheMall.MPS.Models.Procurement.GR
{
    public class MPS_GoodsReceiptTable : BaseTrackableModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int RECEIPT_ID { get; set; }
        public int PO_ID { get; set; }
        [StringLength(200)]
        public string PO_NUM { get; set; }
        public DateTime? DOC_DATE { get; set; }
        public DateTime? POST_DATE { get; set; }
        [StringLength(500)]
        public string DELIV_NOTE { get; set; }
        [StringLength(500)]
        public string HEADER_TXT { get; set; }
        [StringLength(200)]
        public string BILL_LAD { get; set; }
        [StringLength(200)]
        public string SLIP_NUM { get; set; }
        [StringLength(200)]
        public string Status { get; set; }
        public int StatusFlag { get; set; }
        public virtual ICollection<MPS_GoodsReceiptLine> GoodsReceiptLines { get; set; }
        [ForeignKey("PO_ID")]
        public virtual MPS_PurchOrderTable PurchOrderTable { get; set; }
    }
}
