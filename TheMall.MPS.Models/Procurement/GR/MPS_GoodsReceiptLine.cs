﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrossingSoft.Framework.Models.Interfaces;
using TheMall.MPS.Models.Abstracts;

namespace TheMall.MPS.Models.Procurement.GR
{
    public class MPS_GoodsReceiptLine : BaseTrackableModel, IPseudoDelete
    {
        [Key, Column(Order = 0)]
        public int RECEIPT_ID { get; set; }
        //GR_ITEMS
        [Key, Column(Order = 1)]
        public int ITEM_NUM { get; set; }
        [StringLength(200)]
        public string LOCATION { get; set; }
        public decimal? QUANTITY { get; set; }
        [StringLength(200)]
        public string DELIV_CMPL { get; set; }
        [StringLength(1000)]
        public string REASON { get; set; }
        [StringLength(1000)]
        public string ITEM_TEXT { get; set; }
        [StringLength(100)]
        public string MOVEMENTTYPE { get; set; }
        public decimal ACTUALPRICE { get; set; }
        public int? REFERNUM { get; set; }
        public int StatusFlag { get; set; }
        public bool Deleted { get; set; }
        public DateTime? DeletedDate { get; set; }
        [StringLength(100)]
        public string DeletedBy { get; set; }
        [StringLength(200)]
        public string DeletedByName { get; set; }
        [ForeignKey("RECEIPT_ID")]
        public virtual MPS_GoodsReceiptTable GoodsReceiptTable { get; set; }
    }
}
