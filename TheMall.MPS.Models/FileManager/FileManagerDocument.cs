﻿using System.ComponentModel.DataAnnotations;

namespace TheMall.MPS.Models.FileManager
{
    public class FileManagerDocument : Models.Abstracts.BaseTrackableModel
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        [Required]
        public int DirectoryId { get; set; }
        public virtual FileManagerDirectory Directory { get; set; }
    }
}
