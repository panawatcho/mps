﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TheMall.MPS.Models.FileManager
{
    public class FileManagerDirectory : Models.Abstracts.BaseTrackableModel
    {
        public FileManagerDirectory()
        {
            this.Subfolders = new List<FileManagerDirectory>();
        }
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        [ForeignKey("Parent")]
        public int? ParentId { get; set; }
        public virtual FileManagerDirectory Parent { get; set; }

        public virtual ICollection<FileManagerDirectory> Subfolders { get; set; }
    }
}
