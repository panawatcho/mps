﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrossingSoft.Framework.Models.Interfaces;

namespace TheMall.MPS.Models.Interfaces
{
    public  interface IMpsWorkflowTable : IWorkflowTable
    {
        string UnitCode { get; set; }
        string UnitName { get; set; }
        string RequesterUserName { get; set; }
        string RequesterName { get; set; }
        string AccountingBranch { get; set; }
        string AccountingUsername { get; set; }
        string AccountingEmail { get; set; }
        string InformUsername { get; set; }
        string InformEmail { get; set; }
        string CCUsername { get; set; }
        string CCEmail { get; set; }

        DateTime? DueDatePropose { get; set; }
        DateTime? DueDateAccept { get; set; }
        DateTime? DueDateApprove { get; set; }
        DateTime? DueDateFinalApprove { get; set; }
        int? RePrintNo { get; set; }
    }
}
