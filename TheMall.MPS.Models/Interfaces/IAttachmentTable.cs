﻿namespace TheMall.MPS.Models.Interfaces
{
    public interface IAttachmentTable : CrossingSoft.Framework.Models.Interfaces.IAttachmentTable
    {
        string DocumentNumber { get; set; }
        string ContentType { get; set; }

        string ControllerName();
    }
}
