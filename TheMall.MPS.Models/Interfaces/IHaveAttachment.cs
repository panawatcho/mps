﻿using System.Collections.Generic;

namespace TheMall.MPS.Models.Interfaces
{
    public interface IHaveAttachment<TAttachmentTable> where TAttachmentTable : IAttachmentTable
    {
        ICollection<TAttachmentTable> Attachments { get; set; }
    }
}
