﻿using CrossingSoft.Framework.Models.Interfaces;

namespace TheMall.MPS.Models.Interfaces
{
    public interface ILineTable : CrossingSoft.Framework.Models.Interfaces.ILineTable, CrossingSoft.Framework.Models.Interfaces.IConcurrencyCheck
    {
        IWorkflowTable Parent { get; }
    }
}
