﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CrossingSoft.Framework.Models.Interfaces;
using TheMall.MPS.Models.Abstracts;

namespace TheMall.MPS.Models
{
    public class ApplicationSetting : BaseTrackableModel, IApplicationSetting
    {
        [Key]
        public int Id { get; set; }

        [Index("IX_EnvironmentKey", 1, IsUnique = true)]
        [Required]
        [StringLength(5)]
        public string Environment { get; set; }

        [Index("IX_EnvironmentKey", 2, IsUnique = true)]
        [Required]
        [StringLength(100)]
        public string Key { get; set; }

        [StringLength(500)]
        public string Value { get; set; }

        [StringLength(100)]
        public string Category { get; set; }

        public bool Preload { get; set; }
        
    }
}
