﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using TheMall.MPS.Models.Abstracts;
using TheMall.MPS.Models.Interfaces;

namespace TheMall.MPS.Models
{
    public class ExampleMasterTable : BaseModel, IMasterTable
    {
        [Key]
        [StringLength(20)]
        public string Id { get; set; }

        public string Name { get; set; }

        public virtual ICollection<ExampleMasterTable2> Lines { get; set; } 
    }
}
