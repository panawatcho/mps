﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrossingSoft.Framework.Models.Interfaces;
using TheMall.MPS.Models.Abstracts;
using TheMall.MPS.Models.Interfaces;
using TheMall.MPS.Models.Proposal;

namespace TheMall.MPS.Models.Budget
{
    public class MPS_BudgetProposalTrans : BaseModel, IMasterTable, IPKAutoNumber
    {
        [Key]
        public int Id { get; set; }
        public int ProposalId { get; set; }
        public int? ProposalLineId { get; set; }
        [StringLength(Metadata.APCode)]
        public string APCode { get; set; }
        [StringLength(Metadata.UnitCode)]
        public string UnitCode { get; set; }
        [StringLength(100)] //Reserve,Return,Income,Cancel
        public string Status { get; set; }
        [StringLength(100)] //Submit,Revise,Reject,Cancel,Close
        public string DocumentStatus { get; set; }
        [StringLength(100)] //Memo,CashAdvance,PrettyCash,PR,PO,Proposal
        public string DocumentType { get; set; }
        public int? DocumentId { get; set; }
        [StringLength(Metadata.DocumentNumberLength)]
        public string DocumentNo { get; set; }
        [StringLength(100)]
        public string RefDocumentType { get; set; }
        public int? RefDocumentId { get; set; }
        [StringLength(Metadata.DocumentNumberLength)]
        public string RefDocumentNumber { get; set; }
        public decimal Amount { get; set; }
        public DateTime TransDate { get; set; }
        public bool InActive { get; set; }
        [ForeignKey("ProposalId")]
        public virtual MPS_ProposalTable ProposalTable { get; set; }
        [ForeignKey("ProposalLineId")]
        public virtual MPS_ProposalLine ProposalLine { get; set; }
    }
}
