﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrossingSoft.Framework.Models.Interfaces;
using TheMall.MPS.Models.Proposal;

namespace TheMall.MPS.Models.Abstracts
{
    public class BaseApproval : MpsLineTable
    {

        public int ApproverSequence { get; set; }
        [StringLength(Metadata.ApprovalLevel)]
        public string ApproverLevel { get; set; }
        [StringLength(50)]
        public string ApproverUserName { get; set; }
        [StringLength(100)]
        public string ApproverFullName { get; set; }
        public string ApproverEmail { get; set; }
        [StringLength(100)]
        public string Position { get; set; }
        [StringLength(100)]
        public string Department { get; set; }
        public DateTime? DueDate { get; set; }
        public bool LockEditable { get; set; }
        public bool ApproveStatus { get; set; }

        public override IWorkflowTable Parent
        {
            get { throw new NotImplementedException(); }
        }
    }

}
