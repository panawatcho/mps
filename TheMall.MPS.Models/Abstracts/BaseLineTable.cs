﻿using System;
using System.ComponentModel.DataAnnotations;
using CrossingSoft.Framework.Models.Interfaces;
using TheMall.MPS.Models.Interfaces;

namespace TheMall.MPS.Models.Abstracts
{
    public abstract class BaseLineTable : BaseTrackableModel, Interfaces.ILineTable
    {
        [Key]
        public int Id { get; set; }
        public int StatusFlag { get; set; }
        public bool Deleted { get; set; }
        public DateTime? DeletedDate { get; set; }
        public string DeletedBy { get; set; }
        public string DeletedByName { get; set; }
        public float LineNo { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }

        public virtual int ParentId { get; set; }

       // public abstract IPseudoDelete Parent { get; }
        public abstract IWorkflowTable Parent { get; }
        
    }
}
