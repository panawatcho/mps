﻿using System.ComponentModel.DataAnnotations.Schema;
using CrossingSoft.Framework.Infrastructure;

namespace TheMall.MPS.Models.Abstracts
{
    public abstract class BaseModel : IObjectState
    {
        [NotMapped]
        public ObjectState ObjectState { get; set; }
    }
}
