﻿using System.ComponentModel.DataAnnotations;
using TheMall.MPS.Models.Interfaces;

namespace TheMall.MPS.Models.Abstracts
{
    public abstract class BaseCommentTable : BaseTrackableModel, ICommentTable
    {
        [Key]
        public int Id { get; set; }

        public int ParentId { get; set; }

        public int? ProcInstId { get; set; }

        [StringLength(Metadata.SerialNumberLength)]
        public string SerialNumber { get; set; }

        [StringLength(Metadata.ProcessNameLength)]
        public string ProcessName { get; set; }

        [StringLength(Metadata.ActivityLength)]
        public string Activity { get; set; }

        [StringLength(Metadata.ActionLength)]
        public string Action { get; set; }

        public string Comment { get; set; }
    }
}
