﻿using System;
using System.ComponentModel.DataAnnotations;
using CrossingSoft.Framework.Models.Interfaces;

namespace TheMall.MPS.Models.Abstracts
{
    public abstract class BaseTrackableModel : BaseModel, ITrackableModel
    {
        [StringLength(Metadata.UsernameLength)]
        public string CreatedBy { get; set; }
        [StringLength(Metadata.UserDisplayNameLength)]
        public string CreatedByName { get; set; }
        public DateTime? CreatedDate { get; set; }
        [StringLength(Metadata.UsernameLength)]
        public string ModifiedBy { get; set; }
        [StringLength(Metadata.UserDisplayNameLength)]
        public string ModifiedByName { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
