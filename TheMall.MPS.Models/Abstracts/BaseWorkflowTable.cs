﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CrossingSoft.Framework.Models.Interfaces;

namespace TheMall.MPS.Models.Abstracts
{
    public abstract class BaseWorkflowTable : BaseTrackableModel, IWorkflowTable , IConcurrencyCheck
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(200)]
        public string Title { get; set; }

        [Index("DocumentNumber_Revision", IsUnique = false, Order = 1)]
        [StringLength(Metadata.DocumentNumberLength)]
        public string DocumentNumber { get; set; }

        [Index("DocumentNumber_Revision", IsUnique = false, Order = 2)]
        public int? Revision { get; set; }

        [Required]
        [StringLength(Metadata.UsernameLength)]
        public string RequesterUserName { get; set; }

        [Required]
        [StringLength(Metadata.UserDisplayNameLength)]
        public string RequesterName { get; set; }

        //[DateTimeKind(DateTimeKind.Utc)]
        public DateTime? SubmittedDate { get; set; }

        [StringLength(100)]
        public string Status { get; set; }

        [StringLength(100)]
        public string DocumentStatus { get; set; }

        public int StatusFlag { get; set; }

        public int? ProcInstId { get; set; }

        public int? ExpectedDuration { get; set; }
        public DateTime? DocumentDate { get; set; }
        public DateTime? CompletedDate { get; set; }

        public DateTime? CancelledDate { get; set; }

        public virtual string[] NumberSequenceExtra()
        {
            return null;
        }

        [Timestamp]
        public byte[] RowVersion { get; set; }
        public bool Deleted { get; set; }
        public DateTime? DeletedDate { get; set; }

        [StringLength(Metadata.UsernameLength)]
        public string DeletedBy { get; set; }

        [StringLength(Metadata.UserDisplayNameLength)]
        public string DeletedByName { get; set; }
    }
}
