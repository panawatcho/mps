﻿using System.ComponentModel.DataAnnotations;
using TheMall.MPS.Models.Interfaces;

namespace TheMall.MPS.Models.Abstracts
{
    public abstract class BaseAttachmentTable : BaseTrackableModel, IAttachmentTable
    {
        [Key]
        public int Id { get; set; }

        public int ParentId { get; set; }

        [StringLength(Metadata.DocumentNumberLength)]
        public string DocumentNumber { get; set; }

        [StringLength(100)]
        public string ContentType { get; set; }

        public int? ProcInstId { get; set; }

        [StringLength(Metadata.SerialNumberLength)]
        public string SerialNumber { get; set; }

        [StringLength(Metadata.ActivityLength)]
        public string Activity { get; set; }

        [StringLength(300)]
        public string FileDescription { get; set; }

        [StringLength(300)]
        public string FileName { get; set; }

        public long FileSize { get; set; }

        [StringLength(300)]
        public string FileSizeText { get; set; }

        [StringLength(300)]
        public string DocumentLibrary { get; set; }

        [StringLength(600)]
        public string Location { get; set; }

        [StringLength(600)]
        public string DownloadUrl { get; set; }

        public int? DocumentTypeId { get; set; }

        [StringLength(100)]
        public string DocumentTypeName { get; set; }

        public virtual string ControllerName()
        {
            var type = this.GetType();
            if (type.Namespace == "System.Data.Entity.DynamicProxies")
            {
                if (type.BaseType.Name.Contains("MPS_"))
                {
                    var controllerName = type.BaseType.Name.Replace("Attachment", "");
                    return controllerName.Replace("MPS_", "");
                }
                else
                {
                    return type.BaseType.Name.Replace("Attachment", "");
                }
              
            }
            else
            {
                if (this.GetType().Name.Contains("MPS_"))
                {
                    var controllerName = this.GetType().Name.Replace("Attachment", "");
                    return controllerName.Replace("MPS_", "");
                }
                else
                {
                    return this.GetType().Name.Replace("Attachment", "");
                }
              
            }
        }
    }
}
