﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.Models.Interfaces;
using TheMall.MPS.Models.Master;

namespace TheMall.MPS.Models.Abstracts
{
    public abstract class MpsWorkflowTable : BaseWorkflowTable , IMpsWorkflowTable
    {
        [StringLength(Metadata.UnitCode)]
        public string UnitCode { get; set; }

        [StringLength(Metadata.UnitName)]
        public string UnitName { get; set; }

        [ForeignKey("UnitCode")]
        public virtual MPS_M_UnitCode Unitcode { get; set; }
          [StringLength(Metadata.BranchCode)]
        public string AccountingBranch { get; set; }
        public string AccountingUsername { get; set; }
        public string AccountingEmail { get; set; }
        public string InformUsername { get; set; }
        public string InformEmail { get; set; }
        public string CCUsername { get; set; }
        public string CCEmail { get; set; }

        public DateTime? DueDatePropose { get; set; }
        public DateTime? DueDateAccept { get; set; }
        public DateTime? DueDateApprove { get; set; }
        public DateTime? DueDateFinalApprove { get; set; }
        public int? RePrintNo { get; set; }
    }
}
