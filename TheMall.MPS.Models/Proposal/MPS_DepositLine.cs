﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrossingSoft.Framework.Models.Interfaces;
using TheMall.MPS.Models.Abstracts;

namespace TheMall.MPS.Models.Proposal
{
    public class MPS_DepositLine : MpsLineTable
    {
        public string PaymentNo { get; set; }
        public DateTime? DatePeriod { get; set; }
        public decimal Amount { get; set; }
        [StringLength(100)]
        public string ProcessStatus { get; set; }
        public string Remark { get; set; }
        public string InvoiceNo { get; set; }
        public decimal? Actual { get; set; }

        [ForeignKey("ParentId")]
        public virtual MPS_ProposalTable ProposalTable { get; set; }
        public override IWorkflowTable Parent
        {
            get { return ProposalTable; }
        }
    }
}
