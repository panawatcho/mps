﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrossingSoft.Framework.Models.Interfaces;
using TheMall.MPS.Models.Abstracts;
using TheMall.MPS.Models.Master;

namespace TheMall.MPS.Models.Proposal
{
    public class MPS_EstimateTotalSaleTemplate : BaseTrackableModel
    {
        [Key]
        public int Id { get; set; }
        [StringLength(Metadata.BranchCode)]
        public string BranchCode { get; set; }
        public int EstimateTotalSaleId { get; set; }
        public decimal PercentOfDepartment { get; set; }
        public decimal TR_Amount { get; set; }
        public decimal TR_Percent { get; set; }
        public decimal RE_Amount { get; set; }
        public decimal RE_Percent { get; set; }
        [ForeignKey("BranchCode")]
        public virtual MPS_M_SharedBranch SharedBranch { get; set; }
        [ForeignKey("EstimateTotalSaleId")]
        public virtual MPS_EstimateTotalSale EstimateTotalSale { get; set; }

    }
}
