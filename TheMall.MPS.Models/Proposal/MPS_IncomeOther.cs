﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrossingSoft.Framework.Models.Interfaces;
using TheMall.MPS.Models.Abstracts;

namespace TheMall.MPS.Models.Proposal
{
    public class MPS_IncomeOther : MpsLineTable
    {
        [ForeignKey("ParentId")]
        public virtual MPS_ProposalTable ProposalTable { get; set; }
        public override IWorkflowTable Parent
        {
            get { return ProposalTable; }
        }

        [StringLength(200)]
        public string SponcorName { get; set; }
      
        public string Description { get; set; }
        public decimal Budget { get; set; }
        public DateTime DueDate { get; set; }
        [StringLength(200)]
        public string ContactName { get; set; }
        [StringLength(50)]
        public string ContactTel { get; set; }
        [StringLength(50)]
        public string ContactEmail { get; set; }
        [StringLength(100)]
        public string ContactDepartment { get; set; }
        public string Remark { get; set; }
        public decimal? Actual { get; set; }
        public bool PostActual { get; set; }
        public virtual ICollection<MPS_AccountTrans> AccountTrans { get; set; }

        public int? ProposalLineId { get; set; }
       [ForeignKey("ProposalLineId")]
        public virtual MPS_ProposalLine ProposalLine { get; set; }
    }
}
