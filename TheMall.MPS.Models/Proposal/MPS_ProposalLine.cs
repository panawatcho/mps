﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrossingSoft.Framework.Models.Interfaces;
using TheMall.MPS.Models.Abstracts;
using TheMall.MPS.Models.CashAdvance;
using TheMall.MPS.Models.Memo;
using TheMall.MPS.Models.PettyCash;

namespace TheMall.MPS.Models.Proposal
{
    public class MPS_ProposalLine : MpsLineTable
    {
        [StringLength(Metadata.ExpenseTopicCode)]
        public string ExpenseTopicCode { get; set; }
        [StringLength(Metadata.APCode)]
        public string APCode { get; set; }
        [StringLength(255)]
        public string Description { get; set; }
        public decimal BudgetPlan { get; set; }
        public decimal? PreviousActual { get; set; }
        public decimal? Actual { get; set; }
        public string Remark { get; set; }
       // public bool IsTotalSale { get; set; }
        [StringLength(Metadata.UnitCode)]
        public string UnitCode { get; set; }
        [StringLength(Metadata.UnitName)]
        public string UnitName { get; set; }

        [ForeignKey("ParentId")]
        public virtual MPS_ProposalTable ProposalTable { get; set; }
        public override IWorkflowTable Parent
        {
            get { return ProposalTable; }
        }
        public virtual ICollection<MPS_ProposalLineTemplate> ProposalLineTemplates { get; set; }

        public virtual ICollection<MPS_MemoTable> MemoTables { get; set; }
        public virtual ICollection<MPS_PettyCashTable> PettyCashTables { get; set; }
       public virtual ICollection<MPS_CashAdvanceTable> CashAdvanceTables { get; set; }
       public bool CheckBudget { get; set; }

    }
}
