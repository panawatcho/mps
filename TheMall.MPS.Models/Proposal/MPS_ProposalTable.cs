﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.Models.Abstracts;
using TheMall.MPS.Models.Interfaces;
using TheMall.MPS.Models.Master;

namespace TheMall.MPS.Models.Proposal
{
    public class MPS_ProposalTable : MpsWorkflowTable
    {
       
        [StringLength(Metadata.TypeProposalCode)]
        public string ProposalTypeCode { get; set; }
        [StringLength(100)]
        public string ProposalTypeName { get; set; }
        [ForeignKey("ProposalTypeCode")]
        public virtual MPS_M_TypeProposal TypeProposal { get; set; }
        public string Place { get; set; }
        public string Source { get; set; }
        [StringLength(100)]
        public string Other { get; set; }
        public int DepositId { get; set; }
        public string DepositNumber { get; set; }
       
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime? ProjectStartDate { get; set; }
        public DateTime? ProjectEndDate { get; set; }
        public decimal Budget { get; set; }
        public string CustomersTargetGroup { get; set; }
        public bool CheckBudget { get; set; }
        public bool TransferToNextYear { get; set; }
        public string Objective { get; set; }

        public bool TR { get; set; }
        public bool RE { get; set; }
        public bool CloseFlag { get; set; }
      
        [StringLength(200)]
        public string DepartmentException { get; set; }
        public virtual ICollection<MPS_ProposalComment> Comments { get; set; }
        public virtual ICollection<MPS_ProposalAttachment> Attachments { get; set; }
        public virtual ICollection<MPS_ProposalLine> ProposalLine { get; set; }
        public virtual ICollection<MPS_ProposalLineSharedTemplate> TemplateLines { get; set; }
        public virtual ICollection<MPS_DepositLine> DepositLine { get; set; }
        public virtual ICollection<MPS_IncomeTotalSale> IncomeTotalSale { get; set; }
        public virtual ICollection<MPS_IncomeDeposit> IncomeDeposit { get; set; }
        public virtual ICollection<MPS_IncomeOther> IncomeOther { get; set; }
        public virtual ICollection<MPS_EstimateTotalSale> EstimateTotalSale { get; set; }
        public virtual ICollection<MPS_ProposalApproval> ProposalApproval { get; set; }
      
        public decimal TotalBudget { get; set; }
        public decimal? TotalIncomeDeposit { get; set; }
        public decimal? TotalIncomeOther { get; set; }
        public decimal? TotalMarketingExpense { get; set; }
        public decimal? TotalSaleTarget { get; set; }
        public decimal? TotalAdvertising { get; set; }
        public decimal? TotalPromotion { get; set; }
        public decimal? TotalOtherExpense { get; set; }
        public decimal? PecentAdvertising { get; set; }
        public decimal? PecentPromotion { get; set; }
        public decimal? PecentMarketingExpense { get; set; }
        public decimal? PercentOtherExpense { get; set; }
        public decimal? GrandTotal { get; set; }
        public decimal? PercentGrandTotal { get; set; }
        public string DepositApprover { get; set; }

        public DateTime? CloseDate { get; set; }
        [StringLength(Metadata.UsernameLength)]
        public string ClosedBy { get; set; }
        [StringLength(Metadata.UserDisplayNameLength)]
        public string CloseByName { get; set; }
        public string ThemesCode { get; set; }
    }
}
