﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrossingSoft.Framework.Models.Interfaces;
using TheMall.MPS.Models.Abstracts;

namespace TheMall.MPS.Models.Proposal
{
    public class MPS_IncomeTotalSale : MpsLineTable
    {
        [ForeignKey("ParentId")]
        public virtual MPS_ProposalTable ProposalTable { get; set; }
        public override IWorkflowTable Parent
        {
            get { return ProposalTable; }
        }
     
        public string Description { get; set; }
        public decimal Budget { get; set; }
        public string Remark { get; set; }

        public virtual ICollection<MPS_IncomeTotalSaleTemplate> IncomeTotalSaleTemplate { get; set; }
        public decimal? ActualLastYear { get; set; }
        public decimal? ActualAmount { get; set; }
    }
}
