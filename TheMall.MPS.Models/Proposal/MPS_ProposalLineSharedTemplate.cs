﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrossingSoft.Framework.Models.Interfaces;
using TheMall.MPS.Models.Abstracts;
using TheMall.MPS.Models.Master;

namespace TheMall.MPS.Models.Proposal
{
    public class MPS_ProposalLineSharedTemplate : BaseTrackableModel
    {
        [Key]
        public int Id { get; set; }
        public int ParentId { get; set; }
        public IPseudoDelete Parent
        {
            get { return ProposalTable; }
        }

        [ForeignKey("ParentId")]
        public virtual MPS_ProposalTable ProposalTable { get; set; }
        [StringLength(Metadata.BranchCode)]
        public string BranchCode { get; set; }

        [ForeignKey("BranchCode")]
        public virtual MPS_M_SharedBranch SharedBranch { get; set; }
        public bool Selected { get; set; }
    }
}
