﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.Models.Abstracts;

namespace TheMall.MPS.Models.Proposal
{
    public class MPS_DepositAccountTrans : BaseModel
    {
        [Key]
        public int Id { get; set; }
        public decimal? Actual { get; set; }
        public string Remark { get; set; }
        public int MemoIncomeId { get; set; }
        public int DepositLineId { get; set; }
        public string InvoiceNo { get; set; }

        [ForeignKey("DepositLineId")]
        public virtual MPS_DepositLine DepositLine { get; set; }
    }
}
