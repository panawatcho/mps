﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.Models.Abstracts;

namespace TheMall.MPS.Models.Proposal
{
    public class MPS_ProposalBudgetPlan : BaseTrackableModel
    {
        [Key]
        [Column("ProposalId", Order = 0)]
        public int ProposalId { get; set; }
        [Key]
        [Column("APCode", Order = 1)]
        [StringLength(Metadata.APCode)]
        public string APCode { get; set; }
        [Key]
        [Column("UnitCode", Order = 2)]
        [StringLength(Metadata.UnitCode)]
        public string UnitCode { get; set; }
        public decimal BudgetPlan { get; set; }

        [ForeignKey("ProposalId")]
        public virtual MPS_ProposalTable ProposalTable { get; set; }
    }
}
