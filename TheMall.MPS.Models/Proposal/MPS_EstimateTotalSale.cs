﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrossingSoft.Framework.Models.Interfaces;
using TheMall.MPS.Models.Abstracts;

namespace TheMall.MPS.Models.Proposal
{
    public class MPS_EstimateTotalSale : MpsLineTable
    {
        [StringLength(Metadata.ExpenseTopicCode)]
        public string ExpenseTopicCode { get; set; }
        [StringLength(Metadata.ExpenseTopicName)]
        public string ExpenseTopicName { get; set; }
        public decimal BudgetPlan { get; set; }
        public decimal Actual { get; set; }
        public decimal PreviousActual { get; set; }
        public string Remark { get; set; }

        [ForeignKey("ParentId")]
        public virtual MPS_ProposalTable ProposalTable { get; set; }
        public override IWorkflowTable Parent
        {
            get { return ProposalTable; }
        }
        public virtual ICollection<MPS_EstimateTotalSaleTemplate> EstimateTotalSaleTemplate { get; set; }
      
    }
}
