﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrossingSoft.Framework.Models.Interfaces;
using TheMall.MPS.Models.Abstracts;
using TheMall.MPS.Models.Master;

namespace TheMall.MPS.Models.Proposal
{
    public class MPS_IncomeTotalSaleTemplate : BaseTrackableModel
    {
        [Key]
        public int Id { get; set; }
        [StringLength(Metadata.BranchCode)]
        public string BranchCode { get; set; }
        public int IncomeTotalSaleId { get; set; }
        public decimal? PercentOfDepartment { get; set; }
        public decimal? Actual { get; set; }
        public decimal? ActualAmount { get; set; }
     
        [ForeignKey("BranchCode")]
        public virtual MPS_M_SharedBranch SharedBranch { get; set; }

        [ForeignKey("IncomeTotalSaleId")]
        public virtual MPS_IncomeTotalSale IncomeTotalSale { get; set; }
        

    }
}
