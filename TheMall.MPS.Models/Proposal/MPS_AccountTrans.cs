﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.Models.Abstracts;

namespace TheMall.MPS.Models.Proposal
{
    public class MPS_AccountTrans : BaseModel
    {
        [Key]
        public int Id { get; set; }
        public decimal? Actual { get; set; }
        public DateTime? ActualDate { get; set; }
        public string Remark { get; set; }
        public int IncomeOtherId { get; set; }
        // flag ที่เกิดจาก MemoIncome
        public bool FlagMemoInvoice { get; set; }

        [ForeignKey("IncomeOtherId")]
        public virtual MPS_IncomeOther IncomeOther { get; set; }
        public string Source { get; set; }
    }
}
