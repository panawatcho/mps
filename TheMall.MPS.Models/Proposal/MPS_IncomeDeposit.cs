﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrossingSoft.Framework.Models.Interfaces;
using TheMall.MPS.Models.Abstracts;

namespace TheMall.MPS.Models.Proposal
{
     public class MPS_IncomeDeposit : MpsLineTable
    {
       
        [ForeignKey("ParentId")]
        public virtual MPS_ProposalTable ProposalTable { get; set; }
        public override IWorkflowTable Parent
        {
            get { return ProposalTable; }
        }
        public int ProposalDepositRefID { get; set; }
        [StringLength(50)]
        public string ProposalDepositRefDoc { get; set; }
        [StringLength(200)]
        public string ProposalDepositRefTitle { get; set; }
        public decimal Budget { get; set; }
        public string Remark { get; set; }
        public string DepositApprover { get; set; }
        public int? ProcInstId { get; set; }
        [StringLength(100)]
        public string Status { get; set; }
        public bool ActualCharge { get; set; }
        public int? RefMemoId { get; set; }
        
    }
}
