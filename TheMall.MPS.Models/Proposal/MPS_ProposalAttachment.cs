﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrossingSoft.Framework.Models.Interfaces;
using TheMall.MPS.Models.Abstracts;

namespace TheMall.MPS.Models.Proposal
{
    public class MPS_ProposalAttachment : BaseAttachmentTable
    {
        [ForeignKey("ParentId")]
        public virtual MPS_ProposalTable MpsProposalTable { get; set; }
    }
}
