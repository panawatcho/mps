﻿using System.ComponentModel.DataAnnotations;

namespace TheMall.MPS.Models
{
    public class Process : Abstracts.BaseTrackableModel
    {
        [Key]
        [StringLength(Metadata.ProcessCodeLength)]
        public string ProcessCode { get; set; }

        [StringLength(Metadata.ProcessNameLength)]
        public string Name { get; set; }
    }
}
