﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.Models.Abstracts;
using TheMall.MPS.Models.Interfaces;

namespace TheMall.MPS.Models.Master
{
    public class MPS_M_UnitCodeForEmployee : BaseTrackableModel, IMasterTable
    {
        [Key]
        [Column("EmpId", Order = 1)]
        [StringLength(Metadata.EmployeeId)]
        public string EmpId { get; set; }
        [Key]
        [Column("Username", Order = 3)]
        [StringLength(Metadata.UsernameLength)]
        public string Username { get; set; }
        [Key]
        [Column("UnitCode", Order = 2)]
        [StringLength(Metadata.UnitCode)]
        public string UnitCode { get; set; }

        [StringLength(Metadata.UnitName)]
        public string UnitName { get; set; }
        
        public bool InActive { get; set; }
        public decimal? Order { get; set; }
        [ForeignKey("EmpId")]
        public virtual MPS_M_EmployeeTable Employee { get; set; }
    }
}
