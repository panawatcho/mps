﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.Models.Abstracts;

namespace TheMall.MPS.Models.Master
{
    public class MPS_M_Accounting : BaseTrackableModel
    {
        [Key]
        [Column("BranchCode", Order = 1)]
        [StringLength(Metadata.BranchCode)]
        public string BranchCode { get; set; }
        [Key]
        [Column("Username", Order = 2)]
        [StringLength(Metadata.UsernameLength)]
        public string Username { get; set; }
        public string Email { get; set; }
        public string EmpId { get; set; }
        [ForeignKey("EmpId")]
        public virtual MPS_M_EmployeeTable Employee { get; set; }
        public virtual MPS_M_SharedBranch SharedBranch { get; set; }
        public bool InActive { get; set; }
    }
}
