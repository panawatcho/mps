﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.Models.Abstracts;
using TheMall.MPS.Models.Interfaces;
using TheMall.MPS.Models.Master;

namespace TheMall.MPS.Models.Master
{
    public class MPS_M_Company : BaseTrackableModel ,IMasterTable
    {
        [Key]
        public string VendorCode { get; set; }
        public string CompanyCode { get; set; }
        public string CompanyName { get; set; }
        public bool InActive { get; set; }
        public decimal? Order { get; set; }
        public string Address { get; set; }
        public string Description { get; set; }
        public string TelephoneNumber { get; set; }
        public string BranchCode { get; set; }
        [ForeignKey("BranchCode")]
        public virtual MPS_M_SharedBranch Branch { get; set; }
    }
}
