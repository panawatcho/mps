﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.Models.Abstracts;

namespace TheMall.MPS.Models.Master 
{
    public class MPS_M_MailGroup : BaseTrackableModel
    {
        [Key]
        [StringLength(100)]
        public string GroupCode { get; set; }
        public string Email { get; set; }
        public bool InActive { get; set; }
    }
}
