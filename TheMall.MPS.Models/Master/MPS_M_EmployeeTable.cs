﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.Models.Abstracts;
using TheMall.MPS.Models.Master;

namespace TheMall.MPS.Models
{
    public class MPS_M_EmployeeTable : BaseModel
    {
        [Key]
        [StringLength(Metadata.EmployeeId)]
        public string EmpId { get; set; }
        [StringLength(Metadata.UsernameLength)]
        public string Username { get; set; }
        [StringLength(50)]
        public string EmpStatus { get; set; }
         [StringLength(200)]
        public string ThFName { get; set; }
         [StringLength(200)]
        public string ThLName { get; set; }
        [StringLength(200)]
        public string EnFName { get; set; }
        [StringLength(200)]
        public string EnLName { get; set; }
       [StringLength(10)]
        public string DepartmentCode { get; set; }
        [StringLength(200)]
        public string DepartmentName { get; set; }
        [StringLength(10)]
        public string PositionCode { get; set; }
        [StringLength(200)]
        public string PositionName { get; set; }
        [StringLength(11)]
        public string DivisionCode { get; set; }
        [StringLength(200)]
        public string DivisionName { get; set; }

         [StringLength(Metadata.Email)]
        public string Email { get; set; }
         [StringLength(10)]
        public string TelephoneNo { get; set; }
        
        [StringLength(5)]
        public string CompanyCode { get; set; }
        [StringLength(200)]
        public string CompanyName { get; set; }
        [StringLength(Metadata.EmployeeId)]
        public string Authority { get; set; }

        [NotMapped]
        public string ThFullName
        {
            get { return (string.Format("{0} {1} ", ThFName, ThLName)); }
           
        }
        public string BranchCode { get; set; }
        public string BranchName { get; set; }
        public ICollection<MPS_M_UnitCodeForEmployee> UnitCodeforRequester { get; set; }

    }
}
