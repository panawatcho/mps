﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.Models.Abstracts;

namespace TheMall.MPS.Models.Master
{
    public class MPS_M_ProcessApprove : BaseTrackableModel
    {
        [Key]
        
        [StringLength(Metadata.ProcessCodeLength)]
        public string ProcessCode { get; set; }
        public string ProcessName { get; set; }
        public string ProcessType { get; set; }
        public bool InActive { get; set; }
    }
}
