﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.Models.Abstracts;
using TheMall.MPS.Models.Interfaces;

namespace TheMall.MPS.Models.Master
{
    public class MPS_M_Themes : BaseTrackableModel, IMasterTable
    {
        [Key]
        public string ThemesCode { get; set; }
        public string ThemesName { get; set; }
        public bool InActive { get; set; }
        public decimal? Order { get; set; }
    }
}
