﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.Models.Abstracts;
using TheMall.MPS.Models.Interfaces;

namespace TheMall.MPS.Models.Master
{
    public class MPS_M_TypeMemo : BaseTrackableModel, IMasterTable
    {
        [Key]
        [StringLength(Metadata.TypeMemoCode)]
        public string TypeMemoCode { get; set; }
        [StringLength(50)]
        public string TypeMemoName { get; set; }
        public bool InActive { get; set; }
        [StringLength(Metadata.UnitCode)]
        public string UnitCode { get; set; }

        [StringLength(Metadata.UnitName)]
        public string UnitName { get; set; }
        public decimal? Order { get; set; }
        public bool ActualCharge { get; set; }
       
    }
}
