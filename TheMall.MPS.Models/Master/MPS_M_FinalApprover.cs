﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.Models.Abstracts;

namespace TheMall.MPS.Models.Master
{
    public class MPS_M_FinalApprover :BaseTrackableModel
    {
        [Key]
        [Column("ProcessCode", Order = 1)]
        [StringLength(Metadata.ProcessCodeLength)]
        public string ProcessCode { get; set; }
        [Key]
        [Column("UnitCode", Order = 2)]
        [StringLength(Metadata.UnitCode)]
        public string UnitCode { get; set; }
        [Key]
        [Column("Username", Order = 3)]
        [StringLength(Metadata.UsernameLength)]
        public string Username { get; set; }
        public Decimal? Amount { get; set; }
        public bool InActive { get; set; }
       
        public virtual MPS_M_ProcessApprove Process { get; set; }
        [StringLength(Metadata.EmployeeId)]
        public string EmpId { get; set; }
        [ForeignKey("EmpId")]
        public virtual MPS_M_EmployeeTable Employee { get; set; }

    }
}
