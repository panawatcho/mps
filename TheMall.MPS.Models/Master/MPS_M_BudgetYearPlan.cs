﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.Models.Abstracts;
using TheMall.MPS.Models.Interfaces;

namespace TheMall.MPS.Models.Master
{
    public class MPS_M_BudgetYearPlan : BaseTrackableModel, IMasterTable
    {
        //[Key]
        //[StringLength(Metadata.BudgetYearID)]
        //public string BudgetYearID { get; set; }
        [Key]
        [Column("UnitCode", Order = 1)]
        [StringLength(Metadata.UnitCode)]
        public string UnitCode { get; set; }

        [StringLength(Metadata.UnitName)]
        public string UnitName { get; set; }
        [Key]
        [Column("Year", Order = 2)]
        [StringLength(4)]
        public string Year { get; set; }
        
        [StringLength(5)]
        public string Type { get; set; }
        public string TeamName { get; set; }
        public decimal Budget { get; set; }
        [Key]
        [Column("TypeYearPlan", Order = 3)]
        [StringLength(50)]
        public string TypeYearPlan { get; set; }
        public decimal? Order { get; set; }

      
    }
}
