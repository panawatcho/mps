﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.Models.Abstracts;
using TheMall.MPS.Models.Interfaces;

namespace TheMall.MPS.Models.Master
{
    public class MPS_M_APCode : BaseTrackableModel, IMasterTable
    {
        [Key]
        [Column("APCode", Order = 1)]
        [StringLength(Metadata.APCode)]
        public string APCode { get; set; }
        [StringLength(Metadata.APName)]
        public string APName { get; set; }
        public string Description { get; set; }
        public bool CheckBudget { get; set; }
        public bool SendMail { get; set; }

        [Key]
        [Column("ExpenseTopicCode", Order = 2)]
        [StringLength(Metadata.ExpenseTopicCode)]
        public string ExpenseTopicCode { get; set; }
        public virtual MPS_M_ExpenseTopic ExpenseTopic  { get; set; }
        public bool InActive { get; set; }
        public decimal? Order { get; set; }
      

    }
}
