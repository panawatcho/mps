﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.Models.Abstracts;
using TheMall.MPS.Models.Interfaces;

namespace TheMall.MPS.Models.Master
{
    public class MPS_M_UnitCode : BaseTrackableModel, IMasterTable
    {
        [Key]
        [StringLength(Metadata.UnitCode)]
        public string UnitCode { get; set; }

        [StringLength(Metadata.UnitName)]
        public string UnitName { get; set; }
        [StringLength(5)]
        public string Type { get; set; }
        public bool InActive { get; set; }
        public decimal? Order { get; set; }
       
    }
}
