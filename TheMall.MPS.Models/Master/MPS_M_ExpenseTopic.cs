﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.Models.Abstracts;
using TheMall.MPS.Models.Interfaces;

namespace TheMall.MPS.Models.Master
{
    public class MPS_M_ExpenseTopic : BaseTrackableModel, IMasterTable
    {
        [Key]
        [StringLength(Metadata.ExpenseTopicCode)]
        public string ExpenseTopicCode { get; set; }
        [StringLength(Metadata.ExpenseTopicName)]
        public string ExpenseTopicName { get; set; }
        public string Description { get; set; }
        [StringLength(1)]
        public string TypeAP { get; set; }
        public virtual ICollection<MPS_M_APCode> APCode { get; set; }
        public bool InActive { get; set; }
        public decimal? Order { get; set; }
        

      
    }
}
