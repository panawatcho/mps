﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.Models.Abstracts;
using TheMall.MPS.Models.Interfaces;

namespace TheMall.MPS.Models.Master
{
    public class MPS_M_SharedBranch : BaseTrackableModel, IMasterTable
    {    
        [Key]
        [StringLength(Metadata.BranchCode)]
        public string BranchCode { get; set; }
        //[StringLength(100)]
        public string BranchName { get; set; }
        public string BranchAddress { get; set; }
        public bool Inactive { get; set; }
        public decimal? Order { get; set; }
    }
}
