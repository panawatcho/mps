﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.Models.Abstracts;

namespace TheMall.MPS.Models.Master
{
    public class MPS_M_Configulation : BaseTrackableModel
    {
        [Key]
        [Column("Id", Order = 1)]
        public string Id { get; set; }
        [Key]
        [Column("Value", Order = 2)]
        public string Value { get; set; }
        public string Text { get; set; }
        public string Group { get; set; }
        public string Description { get; set; }
        public bool ActiveFlag { get; set; }
        public float? Sequence { get; set; }
    }
}
