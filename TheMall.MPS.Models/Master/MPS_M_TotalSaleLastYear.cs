﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.Models.Abstracts;
using TheMall.MPS.Models.Master;

namespace TheMall.MPS.Models
{
    public class MPS_M_TotalSaleLastYear : BaseTrackableModel
    {
        [Key]
        [Column("BranchCode", Order = 1)]
        [StringLength(Metadata.BranchCode)]
        public string BranchCode { get; set; }
        public decimal? TotalSale { get; set; }
        [Key]
        [Column("Year", Order = 2)]
        [StringLength(4)]
        public string Year { get; set; }
        public decimal? PercentTR { get; set; }
        public decimal? PercentRE { get; set; }
        public decimal? AreaTR { get; set; }
        public decimal? AreaRE { get; set; }
        public virtual MPS_M_SharedBranch SharedBranch { get; set; }
    }

}
