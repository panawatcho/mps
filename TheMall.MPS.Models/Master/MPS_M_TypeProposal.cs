﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.Models.Abstracts;
using TheMall.MPS.Models.Interfaces;

namespace TheMall.MPS.Models.Master
{
    public class MPS_M_TypeProposal : BaseTrackableModel, IMasterTable
    {
        [Key]
        [StringLength(Metadata.TypeProposalCode)]
        public string TypeProposalCode { get; set; }
        [StringLength(100)]
        public string TypeProposalName { get; set; }
        
        public bool InActive { get; set; }
        [StringLength(50)]
        public string TypeYearPlan { get; set; }
        public decimal? Order { get; set; }
    }
}
