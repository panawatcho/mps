﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using TheMall.MPS.Models.Interfaces;

namespace TheMall.MPS.Models.Example
{
    public class ExampleTable : Abstracts.BaseWorkflowTable, IHaveAttachment<ExampleAttachment>
    {
        public ExampleTable()
        {
            Lines = new List<ExampleLine>();
            Attachments = new List<ExampleAttachment>();
        }
        [StringLength(Metadata.StupidNameLength)]
        public string Name { get; set; }

        public virtual ICollection<ExampleLine> Lines { get; set; } 
        public virtual ICollection<ExampleAttachment> Attachments { get; set; }
    }
}
