﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CrossingSoft.Framework.Models.Interfaces;
using ILineTable = TheMall.MPS.Models.Interfaces.ILineTable;

namespace TheMall.MPS.Models.Example
{
    public class ExampleLine : Abstracts.BaseLineTable, ILineTable
    {
        public ExampleLine()
        {
            Attachments = new List<ExampleLineAttachment>();
        }

        [StringLength(Metadata.StupidNameLength)]
        public string Name { get; set; }

        public virtual ICollection<ExampleLineAttachment> Attachments { get; set; }

        [ForeignKey("ParentId")]
        public virtual ExampleTable ExampleTable { get; set; }

        public override IWorkflowTable Parent
        {
            get { return ExampleTable; }
        }
    }
}
