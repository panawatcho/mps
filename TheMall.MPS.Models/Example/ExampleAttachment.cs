﻿using System.ComponentModel.DataAnnotations.Schema;
using TheMall.MPS.Models.Abstracts;

namespace TheMall.MPS.Models.Example
{
    public class ExampleAttachment : BaseAttachmentTable
    {
        [ForeignKey("ParentId")]
        public virtual ExampleTable ExampleTable { get; set; }
    }
}
