﻿using System.ComponentModel.DataAnnotations.Schema;
using TheMall.MPS.Models.Abstracts;

namespace TheMall.MPS.Models.Example
{
    public class ExampleLineAttachment : BaseAttachmentTable
    {
        [ForeignKey("ParentId")]
        public virtual ExampleLine ExampleLine { get; set; }
    }
}
