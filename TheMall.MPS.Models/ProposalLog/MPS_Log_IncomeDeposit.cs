﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrossingSoft.Framework.Models.Interfaces;
using TheMall.MPS.Models.Abstracts;
using TheMall.MPS.Models.Proposal;

namespace TheMall.MPS.Models.ProposalLog
{
    public class MPS_Log_IncomeDeposit : MpsLineTable
    {
        public int? IncomeDepositId { get; set; }
        [ForeignKey("IncomeDepositId")]
        public virtual MPS_IncomeDeposit IncomeDeposit { get; set; }
        [ForeignKey("ParentId")]
        public virtual MPS_Log_ProposalTable ProposalTable { get; set; }
        public override IWorkflowTable Parent
        {
            get { return ProposalTable; }
        }
        public int ProposalDepositRefID { get; set; }
        [StringLength(50)]
        public string ProposalDepositRefDoc { get; set; }
        [StringLength(200)]
        public string ProposalDepositRefTitle { get; set; }
        public decimal Budget { get; set; }
        public string Remark { get; set; }
        public string DepositApprover { get; set; }
        public int? ProcInstId { get; set; }
        [StringLength(100)]
        public string Status { get; set; }
        public bool ActualCharge { get; set; }
        public int? RefMemoId { get; set; }
        public DateTime? LogCreatedDate { get; set; }
        [StringLength(Metadata.UsernameLength)]
        public string LogCreatedBy { get; set; }
    }
}
