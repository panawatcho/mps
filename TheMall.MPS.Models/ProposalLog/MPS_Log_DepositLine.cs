﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrossingSoft.Framework.Models.Interfaces;
using TheMall.MPS.Models.Abstracts;
using TheMall.MPS.Models.Proposal;

namespace TheMall.MPS.Models.ProposalLog
{
    public class MPS_Log_DepositLine : MpsLineTable
    {
        public int? DepositLineId { get; set; }
        [ForeignKey("DepositLineId")]
        public virtual MPS_DepositLine DepositLine { get; set; }
        [StringLength(50)]
        public string PaymentNo { get; set; }
        [StringLength(50)]
        public string DatePeriod { get; set; }
        public decimal Amount { get; set; }
        [StringLength(100)]
        public string ProcessStatus { get; set; }

        [ForeignKey("ParentId")]
        public virtual MPS_Log_ProposalTable ProposalTable { get; set; }
        public override IWorkflowTable Parent
        {
            get { return ProposalTable; }
        }

        public DateTime? LogCreatedDate { get; set; }
        [StringLength(Metadata.UsernameLength)]
        public string LogCreatedBy { get; set; }
    }
}
