﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.Models.Abstracts;

namespace TheMall.MPS.Models.ProposalLog
{
    public class MPS_Log_ProposalTrackChanges : BaseTrackableModel
    {
        [Key]
        public long Id { get; set; }
        public int ProposalId { get; set; }
        public int ProposalLogId { get; set; }
        [StringLength(200)]
        public string RefTableName { get; set; }
        [StringLength(300)]
        public string RefTableDesc { get; set; }
        public int? RefTableId { get; set; }
        public int? RefLogId { get; set; }
        [StringLength(200)]
        public string FieldName { get; set; }
        [StringLength(300)]
        public string FieldDescription { get; set; }
        [StringLength(200)]
        public string State { get; set; }
        public string OldValue { get; set; }
        public string NewValue { get; set; }
    }
}
