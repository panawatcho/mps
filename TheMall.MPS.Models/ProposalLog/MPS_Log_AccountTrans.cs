﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.Models.Abstracts;
using TheMall.MPS.Models.Proposal;

namespace TheMall.MPS.Models.ProposalLog
{
    public class MPS_Log_AccountTrans : BaseModel
    {
        [Key]
        public int Id { get; set; }
        public int? AccountTransId { get; set; }
        [ForeignKey("AccountTransId")]
        public virtual MPS_AccountTrans AccountTrans { get; set; }
        public decimal? Actual { get; set; }
        public DateTime? ActualDate { get; set; }
        public string Remark { get; set; }
        public int IncomeOtherId { get; set; }

        [ForeignKey("IncomeOtherId")]
        public virtual MPS_Log_IncomeOther IncomeOther { get; set; }
        public string Source { get; set; }

        public DateTime? LogCreatedDate { get; set; }
        [StringLength(Metadata.UsernameLength)]
        public string LogCreatedBy { get; set; }
    }
}
