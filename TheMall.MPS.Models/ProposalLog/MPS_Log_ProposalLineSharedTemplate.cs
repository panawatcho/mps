﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrossingSoft.Framework.Models.Interfaces;
using TheMall.MPS.Models.Abstracts;
using TheMall.MPS.Models.Master;
using TheMall.MPS.Models.Proposal;

namespace TheMall.MPS.Models.ProposalLog
{
    public class MPS_Log_ProposalLineSharedTemplate : BaseTrackableModel
    {
        [Key]
        public int Id { get; set; }
        public int? ProposalLineSharedTemplateId { get; set; }
        [ForeignKey("ProposalLineSharedTemplateId")]
        public virtual MPS_ProposalLineSharedTemplate ProposalLineSharedTemplate { get; set; }
        public int ParentId { get; set; }
        public IPseudoDelete Parent
        {
            get { return ProposalTable; }
        }

        [ForeignKey("ParentId")]
        public virtual MPS_Log_ProposalTable ProposalTable { get; set; }
        [StringLength(Metadata.BranchCode)]
        public string BranchCode { get; set; }

        //[ForeignKey("BranchCode")]
        //public virtual MPS_M_SharedBranch SharedBranch { get; set; }
        public bool Selected { get; set; }

        public DateTime? LogCreatedDate { get; set; }
        [StringLength(Metadata.UsernameLength)]
        public string LogCreatedBy { get; set; }
    }
}
