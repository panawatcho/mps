﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrossingSoft.Framework.Models.Interfaces;
using TheMall.MPS.Models.Abstracts;
using TheMall.MPS.Models.Master;
using TheMall.MPS.Models.Proposal;

namespace TheMall.MPS.Models.ProposalLog
{
    public class MPS_Log_IncomeTotalSaleTemplate : BaseTrackableModel
    {
        [Key]
        public int Id { get; set; }
        public int? IncomeTotalSaleTemplateId { get; set; }
        [ForeignKey("IncomeTotalSaleTemplateId")]
        public virtual MPS_IncomeTotalSaleTemplate IncomeTotalSaleTemplate { get; set; }
        [StringLength(Metadata.BranchCode)]
        public string BranchCode { get; set; }
        public int IncomeTotalSaleId { get; set; }
        public decimal? PercentOfDepartment { get; set; }
        public decimal? Actual { get; set; }
     
        //[ForeignKey("BranchCode")]
        //public virtual MPS_M_SharedBranch SharedBranch { get; set; }

        [ForeignKey("IncomeTotalSaleId")]
        public virtual MPS_Log_IncomeTotalSale IncomeTotalSale { get; set; }

        public DateTime? LogCreatedDate { get; set; }
        [StringLength(Metadata.UsernameLength)]
        public string LogCreatedBy { get; set; }
    }
}
