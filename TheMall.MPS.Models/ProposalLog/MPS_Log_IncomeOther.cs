﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrossingSoft.Framework.Models.Interfaces;
using TheMall.MPS.Models.Abstracts;
using TheMall.MPS.Models.Proposal;

namespace TheMall.MPS.Models.ProposalLog
{
    public class MPS_Log_IncomeOther : MpsLineTable
    {
        public int? IncomeOtherId { get; set; }
        [ForeignKey("IncomeOtherId")]
        public virtual MPS_IncomeOther IncomeOther { get; set; }
        [ForeignKey("ParentId")]
        public virtual MPS_Log_ProposalTable ProposalTable { get; set; }
        public override IWorkflowTable Parent
        {
            get { return ProposalTable; }
        }

        [StringLength(200)]
        public string SponcorName { get; set; }
      
        public string Description { get; set; }
        public decimal Budget { get; set; }
        public DateTime DueDate { get; set; }
        [StringLength(200)]
        public string ContactName { get; set; }
        [StringLength(50)]
        public string ContactTel { get; set; }
        [StringLength(50)]
        public string ContactEmail { get; set; }
        [StringLength(100)]
        public string ContactDepartment { get; set; }
        public string Remark { get; set; }
        public decimal? Actual { get; set; }
        public bool PostActual { get; set; }
        public virtual ICollection<MPS_Log_AccountTrans> AccountTrans { get; set; }
        public int? ProposalLineId { get; set; }
        [ForeignKey("ProposalLineId")]
        public virtual MPS_ProposalLine ProposalLine { get; set; }
        public DateTime? LogCreatedDate { get; set; }
        [StringLength(Metadata.UsernameLength)]
        public string LogCreatedBy { get; set; }
      
    }
}
