﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrossingSoft.Framework.Models.Interfaces;
using TheMall.MPS.Models.Abstracts;
using TheMall.MPS.Models.Proposal;

namespace TheMall.MPS.Models.ProposalLog
{
    public class MPS_Log_IncomeTotalSale : MpsLineTable
    {
        public int? IncomeTotalSaleId { get; set; }
        [ForeignKey("IncomeTotalSaleId")]
        public virtual MPS_IncomeTotalSale IncomeTotalSale { get; set; }
        [ForeignKey("ParentId")]
        public virtual MPS_Log_ProposalTable ProposalTable { get; set; }
        public override IWorkflowTable Parent
        {
            get { return ProposalTable; }
        }
     
        public string Description { get; set; }
        public decimal Budget { get; set; }
        public string Remark { get; set; }
        public decimal? ActualLastYear { get; set; }
        public virtual ICollection<MPS_Log_IncomeTotalSaleTemplate> IncomeTotalSaleTemplate { get; set; }
        public DateTime? LogCreatedDate { get; set; }
        [StringLength(Metadata.UsernameLength)]
        public string LogCreatedBy { get; set; }

    }
}
