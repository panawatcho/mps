﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.Models.Abstracts;

namespace TheMall.MPS.Models.ProposalLog
{
    public class MPS_Log_ProposalExpenseTopic : BaseTrackableModel
    {
        [Key]
        public int Id { get; set; }
        public int ProposalLogId { get; set; }
        [ForeignKey("ProposalLogId")]
        public virtual MPS_Log_ProposalTable ProposalLog { get; set; }
        [StringLength(Metadata.ExpenseTopicCode)]
        public string ExpenseTopicCode { get; set; }
        public decimal? Amount { get; set; }

    }
}
