﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrossingSoft.Framework.Models.Interfaces;
using TheMall.MPS.Models.Abstracts;
using TheMall.MPS.Models.Master;
using TheMall.MPS.Models.Proposal;

namespace TheMall.MPS.Models.ProposalLog
{
    public class MPS_Log_ProposalLineTemplate : BaseTrackableModel
    {
        [Key]
        public int Id { get; set; }
        public int? ProposalLineTemplateId { get; set; }
        [ForeignKey("ProposalLineTemplateId")]
        public virtual MPS_ProposalLineTemplate ProposalLineTemplate { get; set; }
        [StringLength(Metadata.BranchCode)]
        public string BranchCode { get; set; }
        public int ProposalLineId { get; set; }
        public decimal PercentOfDepartment { get; set; }
        public decimal TR_Amount { get; set; }
        public decimal TR_Percent { get; set; }
        public decimal RE_Amount { get; set; }
        public decimal RE_Percent { get; set; }
        //[ForeignKey("BranchCode")]
        //public virtual MPS_M_SharedBranch SharedBranch { get; set; }
        [ForeignKey("ProposalLineId")]
        public virtual MPS_Log_ProposalLine ProposalLine { get; set; }

        public DateTime? LogCreatedDate { get; set; }
        [StringLength(Metadata.UsernameLength)]
        public string LogCreatedBy { get; set; }
    }
}
