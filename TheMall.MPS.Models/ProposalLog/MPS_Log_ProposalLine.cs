﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrossingSoft.Framework.Models.Interfaces;
using TheMall.MPS.Models.Abstracts;
using TheMall.MPS.Models.CashAdvance;
using TheMall.MPS.Models.Memo;
using TheMall.MPS.Models.PettyCash;
using TheMall.MPS.Models.Proposal;

namespace TheMall.MPS.Models.ProposalLog
{
    public class MPS_Log_ProposalLine : MpsLineTable
    {
        public int? ProposalLineId { get; set; }
        [ForeignKey("ProposalLineId")]
        public virtual MPS_ProposalLine ProposalLine { get; set; }
        [StringLength(Metadata.ExpenseTopicCode)]
        public string ExpenseTopicCode { get; set; }
        [StringLength(Metadata.APCode)]
        public string APCode { get; set; }
        public string Description { get; set; }
        public decimal BudgetPlan { get; set; }
        public decimal? PreviousActual { get; set; }
        public decimal? Actual { get; set; }
        public string Remark { get; set; }
        // public bool IsTotalSale { get; set; }
        [StringLength(Metadata.UnitCode)]
        public string UnitCode { get; set; }
        [StringLength(Metadata.UnitName)]
        public string UnitName { get; set; }

        [ForeignKey("ParentId")]
        public virtual MPS_Log_ProposalTable ProposalTable { get; set; }
        public override IWorkflowTable Parent
        {
            get { return ProposalTable; }
        }
        public virtual ICollection<MPS_Log_ProposalLineTemplate> ProposalLineTemplates { get; set; }

        //public virtual ICollection<MPS_MemoTable> MemoTables { get; set; }
        //public virtual ICollection<MPS_PettyCashTable> PettyCashTables { get; set; }
        //public virtual ICollection<MPS_CashAdvanceTable> CashAdvanceTables { get; set; }
        public bool CheckBudget { get; set; }

        public DateTime? LogCreatedDate { get; set; }
        [StringLength(Metadata.UsernameLength)]
        public string LogCreatedBy { get; set; }
    }
}
