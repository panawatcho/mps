﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TheMall.MPS.Models.Abstracts;

namespace TheMall.MPS.Models.NumberSeq
{
    [Table("NumberSeqTable")]
    public class NumberSeqTable : BaseTrackableModel
    {
        [Key]
        [StringLength(Metadata.NumberSeqCodeLength)]
        public string NumberSeqCode { get; set; }

        [StringLength(Metadata.StupidNameLength)]
        public string Name { get; set; }

        [Required]
        public int NextNum { get; set; }

        [StringLength(Metadata.DocumentNumberLength)]
        public string Format { get; set; }
    }
}
