﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TheMall.MPS.Models.Abstracts;

namespace TheMall.MPS.Models.NumberSeq
{
    [Table("NumberSeqSetup")]
    public class NumberSeqSetup : BaseTrackableModel, CrossingSoft.Framework.Models.Interfaces.IPKAutoNumber
    {
        [Key]
        public int Id { get; set; }
        [StringLength(Metadata.NumberSeqCodeLength)]
        public string NumberSeqCode { get; set; }
        [StringLength(Metadata.StupidNameLength)]
        public string Name { get; set; }       
        [StringLength(4)]
        public string Year { get; set; }
        [StringLength(Metadata.ProcessNameLength)]
        public string Process { get; set; }
        [StringLength(Metadata.DimensionLength)]
        public string Dimension1 { get; set; }
        [StringLength(Metadata.DimensionLength)]
        public string Dimension2 { get; set; }
        [StringLength(Metadata.DimensionLength)]
        public string Dimension3 { get; set; }

        [ForeignKey("NumberSeqCode")]
        public virtual NumberSeqTable NumberSeqTable { get; set; }
    }
}
