﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.Models.Abstracts;
using TheMall.MPS.Models.Proposal;

namespace TheMall.MPS.Models.PettyCash
{
    public class MPS_PettyCashTable : MpsWorkflowTable
    {

        [StringLength(Metadata.ExpenseTopicCode)]
        public string ExpenseTopicCode { get; set; }

        [StringLength(Metadata.APCode)]
        public string APCode { get; set; }

        public DateTime? PettyCashDate { get; set; }
        public decimal BudgetDetail { get; set; }
        public decimal BudgetAPCode { get; set; }
        public int ProposalRefID { get; set; }
        [StringLength(Metadata.DocumentNumberLength)]
        public string ProposalRefDocumentNumber { get; set; }

        [StringLength(100)]
        public string Branch { get; set; }
        public string Description { get; set; }
        public int ProposalLineID { get; set; }

        [ForeignKey("ProposalLineID")]
        public virtual MPS_ProposalLine ProposalLine { get; set; }
        public virtual ICollection<MPS_PettyCashLine> PettyCashLine { get; set; }
        public virtual ICollection<MPS_PettyCashApproval> PettyCashApproval { get; set; }
        public virtual ICollection<MPS_PettyCashComment> Comments { get; set; }
        public virtual ICollection<MPS_PettyCashAttachment> Attachments { get; set; }

        public bool GoodsIssue { get; set; }
        public string VendorCode { get; set; }
    }
}
