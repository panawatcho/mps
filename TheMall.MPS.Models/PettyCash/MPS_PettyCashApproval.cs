﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrossingSoft.Framework.Models.Interfaces;
using TheMall.MPS.Models.Abstracts;
using TheMall.MPS.Models.Interfaces;

namespace TheMall.MPS.Models.PettyCash
{
    public class MPS_PettyCashApproval : BaseApproval
    {
        
        [ForeignKey("ParentId")]
        public virtual MPS_PettyCashTable PettyCashTable { get; set; }
        public override IWorkflowTable Parent
        {
            get { return PettyCashTable; }
        }
     

    }
}
