﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrossingSoft.Framework.Models.Interfaces;
using TheMall.MPS.Models.Abstracts;
using TheMall.MPS.Models.Master;
using TheMall.MPS.Models.Proposal;

namespace TheMall.MPS.Models.MemoIncome
{
    public class MPS_MemoIncomeInvoice : MpsLineTable
    {   
        public string InvoiceNo { get; set; }
        public decimal Actual { get; set; }
        public DateTime? InvoiceDate { get; set; }
        public string Remark { get; set; }

        [ForeignKey("ParentId")]
        public virtual MPS_MemoIncomeTable MemoIncomeTable { get; set; }
        public override IWorkflowTable Parent
        {
            get { return MemoIncomeTable; }
        }
    }
}
