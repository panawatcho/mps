﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.Models.Abstracts;
using TheMall.MPS.Models.Master;
using TheMall.MPS.Models.Proposal;

namespace TheMall.MPS.Models.MemoIncome
{
    public class MPS_MemoIncomeTable : MpsWorkflowTable
    {
        public decimal TotalAmount { get; set; }
        public decimal BudgetDetail { get; set; }
        public string Category { get; set; }
        public int ProposalRefID { get; set; }

        [StringLength(Metadata.DocumentNumberLength)]
        public string ProposalRefDocumentNumber { get; set; }

        [StringLength(Metadata.TypeProposalCode)]
        public string ProposalTypeCode { get; set; }

        [StringLength(100)]
        public string Branch { get; set; }

        public string Description { get; set; }
        public string InvoiceNo { get; set; }
        public string AccountingStatus{ get; set; }
        public string AccOnProcessUsername { get; set; }
        public string AccCompleteUsername { get; set; }
        public int? OtherIncomeRefId { get; set; }
        public int? DepositLineId { get; set; }
        public decimal? Actual { get; set; }


        [ForeignKey("ProposalTypeCode")]
        public virtual MPS_M_TypeProposal TypeProposal { get; set; }
        [ForeignKey("OtherIncomeRefId")]
        public virtual MPS_IncomeOther IncomeOther { get; set; }
        [ForeignKey("DepositLineId")]
        public virtual MPS_DepositLine DepositLine { get; set; }
        [ForeignKey("ProposalRefID")]
        public virtual MPS_ProposalTable ProposalTable { get; set; }

        public virtual ICollection<MPS_MemoIncomeLine> MemoIncomeLine { get; set; }
        public virtual ICollection<MPS_MemoIncomeInvoice> MemoIncomeInvoice { get; set; }
        public virtual ICollection<MPS_MemoIncomeApproval> MemoIncomeApproval { get; set; }
        public virtual ICollection<MPS_MemoIncomeComment> Comments { get; set; }
        public virtual ICollection<MPS_MemoIncomeAttachment> Attachments { get; set; }
        
    }
}
