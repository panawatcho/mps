﻿using System.ComponentModel;

namespace TheMall.MPS.Models.Enums
{
    public static class DocumentStatusFlagId
    {
        public const int Draft = 0;
        public const int Active = 1;
        public const int InProgress = 2;
        
        public const int Approve = 4;
        public const int Reject = 5;
        public const int Cancelled = 6;
        public const int Recalled = 7;
        public const int Deleted = 8;
        public const int ReviseCompleted = 8;
        public const int Completed = 9;
        public const int Close = 3;
        public const int OnProcess = 10;
    }
    public enum DocumentStatusFlag
    {
        Draft = DocumentStatusFlagId.Draft,
        Active = DocumentStatusFlagId.Active,
        [Description("In Progress")]
        InProgress = DocumentStatusFlagId.InProgress,
        Approve = DocumentStatusFlagId.Approve,
        Reject = DocumentStatusFlagId.Reject,
        Cancelled = DocumentStatusFlagId.Cancelled,
        Recalled = DocumentStatusFlagId.Recalled,
        Deleted = DocumentStatusFlagId.Deleted,
        ReviseCompleted = DocumentStatusFlagId.ReviseCompleted,
        Completed = DocumentStatusFlagId.Completed,
        Close = DocumentStatusFlagId.Close,
        OnProcess = DocumentStatusFlagId.OnProcess,
    }
    public enum DocumentStatusFlagSearch
    {
        Draft = DocumentStatusFlagId.Draft,
        Active = DocumentStatusFlagId.Active, // 1 or 2 or 7
        Deleted = DocumentStatusFlagId.Deleted,
        Completed = DocumentStatusFlagId.Completed, // 6 or 9
    }

   
}
