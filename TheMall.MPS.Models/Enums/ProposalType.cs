﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheMall.MPS.Models.Enums
{
     public static class ProposalType
    {
         public static class ProposalTypeCode
        {
            public const string AP = "AP";
            public const string Deposit = "DEPOSIT";
            public const string DepositInternal = "DEPOSITIN";
            public const string PreOperation = "PREOP";
            public const string SpecialEvent = "SPECIAL";
            public const string UnBudeget = "UNBUDGET";
        }
        
    }
}
