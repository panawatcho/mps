﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheMall.MPS.Models.Enums
{
    public static class Approval
    {
        public static class ApproveLevel
        {
            public const string Propose = "เสนอ";
            public const string Accept = "เห็นชอบ";
            public const string Approval = "อนุมัติ";
            public const string FinalApproval = "อนุมัติขั้นสุดท้าย";
            public const string FYI = "ทราบ";
            public const string Dear = "สำเนาเรียน";
        }
        public static class ApproveKey
        {
            public const int Propose = 1;
            public const int Accept = 2;
            public const int Approval = 3;
            public const int FinalApproval = 4;
            public const int FYI =5;
            public const int Dear = 6;
        }

        public enum  ApproveLevelList
        {
            [Description("เสนอ")]
            Propose = ApproveKey.Propose,
              [Description("เห็นชอบ")]
            Accept = ApproveKey.Accept,
              [Description("อนุมัติ")]
            Approval = ApproveKey.Approval,
              [Description("อนุมัติขั้นสุดท้าย")]
            FinalApproval = ApproveKey.FinalApproval,
              [Description("ทราบ")]
            FYI = ApproveKey.FYI,
              [Description("สำเนาเรียน")]
            Dear = ApproveKey.Dear,
        }
    }
}
