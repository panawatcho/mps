﻿namespace TheMall.MPS.Models.Enums
{
    public enum MailType
    {
        Notification = 1,
        Escalation = 2,
        Inform = 3
    }
}
