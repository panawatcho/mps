﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheMall.MPS.Models.Enums
{
    public static class MProcurementConstant
    {
        public static class TableOrStructureName
        {
            public const string PRHeader = "REQ_HEADER";
            public const string PRItem = "REQ_ITEMS";
            public const string PRAcctAssgn = "REQ_ACCT_ASSGNS";

            public const string POHeader = "PO_HEADER";
            public const string POItem = "PO_ITEMS";
            public const string POAcctAssgn = "PO_ACCT_ASSGNS";

            public const string GRHeader = "GR_HEADER";
            public const string GRItem = "GR_ITEMS";
        }

        public static class GRMovementType
        {
            public const string CREATE = "101";
            public const string CANCEL = "102";
            public const string RETURN = "122";

            public static string GetStatusText(string movementTypeCode)
            {
                if (movementTypeCode == CREATE)
                    return "Create";
                if (movementTypeCode == CANCEL)
                    return "Cancel";
                if (movementTypeCode == RETURN)
                    return "Return";
                return null; 
            }
            public static int GetStatusId(string movementTypeCode)
            {
                if (movementTypeCode == CREATE)
                    return 1;
                if (movementTypeCode == CANCEL)
                    return 5;
                if (movementTypeCode == RETURN)
                    return 2;
                return 0;
            }
        }
    }
}
