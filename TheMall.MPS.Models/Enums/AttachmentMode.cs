﻿namespace TheMall.MPS.Models.Enums
{
    public enum AttachmentMode
    {
        FileSystem = 0,
        SharePoint
    }
}
