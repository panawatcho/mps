﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheMall.MPS.Models.Enums
{
    public static class Budget
    {
        public static class BudgetStatus
        {
            public const string Reserve = "Reserve";
            public const string Income = "Income";
            public const string Return = "Return";
            public const string Cancel = "Cancel";
            public const string Commit = "Commit";
            public const string Actual = "Actual";
            public const string Close = "Close";
            public const string Change = "Change";
        }

        public static class DocumentType
        {
            //public const string Proposal = "Proposal";
            //public const string Memo = "Memo";
            //public const string CashAdvance = "Cash Advance";
            //public const string CashClearing = "Cash Clearing";
            //public const string PettyCash = "Petty Cash";
            public const string Proposal = "proposal";
            public const string Memo = "memo";
            public const string PettyCash = "pettycash";
            public const string CashAdvance = "cashadvance";
            public const string CashClearing = "cashclearing";
            public const string PurchaseRequisition = "PR";
            public const string PurchaseOrder = "PO";
            public const string GoodsReceipt = "GR";

            public const string MemoIncome = "memoincome";
        }

        public static class PurchaseType
        {
            public const string Proposal = "M";
        }
        public static class APCode
        {
            public const string DO3 = "DO3";
        }
    }
}
