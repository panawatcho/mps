﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TheMall.MPS.Models
{
    public class DocumentType : Abstracts.BaseTrackableModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        [StringLength(Metadata.StupidNameLength)]
        public string Name { get; set; }

        [StringLength(Metadata.ProcessCodeLength)]
        public string ProcessCode { get; set; }
        [ForeignKey("ProcessCode")]
        public virtual Models.Process Process { get; set; }

        [StringLength(Metadata.ActivityLength)]
        public string Activity { get; set; }

        public bool IsRequired { get; set; }
    }
}
