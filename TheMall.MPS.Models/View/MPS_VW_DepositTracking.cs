﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.Models.Abstracts;

namespace TheMall.MPS.Models.View
{
    public class MPS_VW_DepositTracking : BaseModel
    {
        [Key]
        public long RowNo { get; set; }
        public int ProposalId { get; set; }
        public string ProposalDocNo { get; set; }
        public string Type { get; set; }
        public int DepositId { get; set; }
        public decimal? TotalBudget { get; set; }
        public string Status { get; set; }
    }
}
