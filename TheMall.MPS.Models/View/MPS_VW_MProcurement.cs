﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.Models.Abstracts;

namespace TheMall.MPS.Models.View
{
    public class MPS_VW_MProcurement : BaseModel
    {
        [Key]
        public long RowNo { get; set; }
        public int REQ_ID { get; set; }
        public string PR_NUM { get; set; }
        public string PROPOSALID { get; set; }
        public int PRStatusFlag { get; set; } 
        public int? PO_ID { get; set; }
        public string PO_NUM { get; set; }
        public int? POStatusFlag { get; set; } 
    }
}
