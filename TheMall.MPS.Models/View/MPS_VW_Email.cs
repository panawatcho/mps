﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.Models.Abstracts;

namespace TheMall.MPS.Models.View
{
    public class MPS_VW_Email : BaseModel
    {
        [Key]
        public long RowNo { get; set; }
        public string Name { get; set; }// user or Group
        public string Email { get; set; }
        public string TypeMail { get; set; }// user or Group
    }
}
