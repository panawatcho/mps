﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.Models.Abstracts;

namespace TheMall.MPS.Models.View
{
    public class MPS_VW_BudgetYearTracking : BaseModel
    {
        [Key]
        public long RowNo { get; set; }
        public string Year { get; set; }
        public string UnitCode { get; set; }
        public string UnitName { get; set; }
        public decimal Budget { get; set; }
        public string TypeYearPlan { get; set; }
        public decimal Reserve { get; set; }
        public decimal Return { get; set; }
        public decimal IncomeDeposit { get; set; }
        public decimal IncomeOther { get; set; }
        public decimal ReturnMemo { get; set; }
        public decimal Remaining { get; set; }
    }
}
