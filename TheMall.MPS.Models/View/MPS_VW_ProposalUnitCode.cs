﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.Models.Abstracts;

namespace TheMall.MPS.Models.View
{
    public class MPS_VW_ProposalUnitCode : BaseModel
    {
        [Key]
        public long RowNo { get; set; }
        public string UnitCodeAP { get; set; }
        public int Id { get; set; }
        public string ProposalTypeCode { get; set; }
        public string ProposalTypeName { get; set; }
        public string Place { get; set; }
        public string Source { get; set; }
        public string Other { get; set; }
        public int DepositId { get; set; }
        public string DepositNumber { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime? ProjectStartDate { get; set; }
        public DateTime? ProjectEndDate { get; set; }
        public decimal Budget { get; set; }
        public string CustomersTargetGroup { get; set; }
        public bool CheckBudget { get; set; }
        public bool TransferToNextYear { get; set; }
        //public string Objective { get; set; }
        public bool TR { get; set; }
        public bool RE { get; set; }
        public bool CloseFlag { get; set; }
        public string DepartmentException { get; set; }
        public decimal TotalBudget { get; set; }
        public decimal? TotalIncomeDeposit { get; set; }
        public decimal? TotalIncomeOther { get; set; }
        public decimal? TotalMarketingExpense { get; set; }
        public decimal? TotalSaleTarget { get; set; }
        public decimal? TotalAdvertising { get; set; }
        public decimal? TotalPromotion { get; set; }
        public decimal? PecentAdvertising { get; set; }
        public decimal? PecentPromotion { get; set; }
        public decimal? PecentMarketingExpense { get; set; }
        public string UnitCode { get; set; }
        public string UnitName { get; set; }
        public string AccountingBranch { get; set; }
        public string AccountingUsername { get; set; }
        public string AccountingEmail { get; set; }
        public string InformUsername { get; set; }
        public string InformEmail { get; set; }
        public string CCUsername { get; set; }
        public string CCEmail { get; set; }
        public DateTime? DueDatePropose { get; set; }
        public DateTime? DueDateAccept { get; set; }
        public DateTime? DueDateApprove { get; set; }
        public DateTime? DueDateFinalApprove { get; set; }
        public string Title { get; set; }
        public string DocumentNumber { get; set; }
        public int? Revision { get; set; }
        public string RequesterUserName { get; set; }
        public string RequesterName { get; set; }
        public DateTime? SubmittedDate { get; set; }
        public string Status { get; set; }
        public string DocumentStatus { get; set; }
        public int StatusFlag { get; set; }
        public int? ProcInstId { get; set; }
        public int? ExpectedDuration { get; set; }
        public DateTime? DocumentDate { get; set; }
        public DateTime? CompletedDate { get; set; }
        public DateTime? CancelledDate { get; set; }
        public byte[] RowVersion { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedByName { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedByName { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool Deleted { get; set; }
        public DateTime? DeletedDate { get; set; }
        public string DeletedBy { get; set; }
        public string DeletedByName { get; set; }
    }
}
