﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.Models.Abstracts;

namespace TheMall.MPS.Models.View
{
    public class MPS_VW_ProposalTracking : BaseModel
    {
        [Key]
        public long RowNo { get; set; }
        public int ProposalId { get; set; }
        public string ProposalDocNo { get; set; }
        public int ProposalLineId { get; set; }
        public string ExpenseTopicCode { get; set; }
        public string APCode { get; set; }
        public string UnitCode { get; set; }
        public string UnitName { get; set; }
        public string Description { get; set; }
        public string DocumentType { get; set; }
        public int DocumentId { get; set; }
        public string DocumentNo { get; set; }
        public string Status { get; set; }
        public string DocumentStatus { get; set; }
        public int? StatusFlag { get; set; }
        public decimal? Amount { get; set; }
        public string RefDocumentType { get; set; }
        public int? RefDocumentId { get; set; }
        public string RefDocumentNo { get; set; }
        public string RefStatus { get; set; }
        public string RefDocumentStatus { get; set; }
        public int? RefStatusFlag { get; set; }
        public decimal? RefAmount { get; set; }

    }
}
