﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.Models.Proposal;
using TheMall.MPS.Models.Abstracts;

namespace TheMall.MPS.Models.View
{
    public class MPS_VW_BudgetTracking : BaseModel
    {
        [Key]
        public long RowNo { get; set; }
        public int ProposalId { get; set; }
        public int ProposalLineId { get; set; }
        public string ProposalNo { get; set; }
        public string ProposalUnitCode { get; set; }
        public string UnitCode { get; set; }
        public string APCode { get; set; }
        public string Description { get; set; }
        public decimal BudgetPlan { get; set; }
        public decimal BudgetUsed { get; set; }
        public decimal Memo { get; set; }
        public decimal CashAdvance { get; set; }
        public decimal PettyCash { get; set; }
        public decimal PR { get; set; }
        public decimal PO { get; set; }
        public decimal GR { get; set; }
        public decimal BudgetRemaining { get; set; }
        public bool CloseFlag { get; set; }
       
    }
}
