﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.Models.Abstracts;

namespace TheMall.MPS.Models.View
{
    public class MPS_VW_DocumentRelated : BaseModel
    {
        [Key]
        public long RowNo { get; set; }
        public int Id { get; set; }
        public string DocumentType { get; set; }
        public string RefDocumentNumber { get; set; }
        public int? RefDocumentID { get; set; }
        public string UnitCode { get; set; }
        public string UnitName { get; set; }
        public string AccountingBranch { get; set; }
        public string AccountingUsername { get; set; }
        public string AccountingEmail { get; set; }
        public string InformUsername { get; set; }
        public string InformEmail { get; set; }
        public string CCUsername { get; set; }
        public string CCEmail { get; set; }
        public string Title { get; set; }
        public string DocumentNumber { get; set; }
        public string RequesterUserName { get; set; }
        public string RequesterName { get; set; }
        public DateTime? SubmittedDate { get; set; }
        public string Status { get; set; }
        public string DocumentStatus { get; set; }
        public int StatusFlag { get; set; }
        public string UsernameApprover { get; set; }
        public string UsernameAccounting { get; set; }
        public int? Revision { get; set; }
    }
}
