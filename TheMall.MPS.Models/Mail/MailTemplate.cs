﻿using System.ComponentModel.DataAnnotations;
using TheMall.MPS.Models.Abstracts;

namespace TheMall.MPS.Models.Mail
{
    public class MailTemplate : BaseTrackableModel
    {
        [Key]
        [Required]
        [StringLength(Metadata.MailTemplateIdLength)]
        public string MailTemplateId { get; set; }

        [Required]
        [StringLength(Metadata.MailTemplateNameLength)]
        public string Name { get; set; }

        public string TemplatePath { get; set; }

        public string Template { get; set; }
    }
}
