﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TheMall.MPS.Models.Abstracts;

namespace TheMall.MPS.Models.Mail
{
    public class MailContent : BaseTrackableModel
    {
        [Key]
        [Required]
        [StringLength(Metadata.MailContentIdLength)]
        public string MailContentId { get; set; }

        [Required]
        [StringLength(Metadata.MailContentNameLength)]
        public string Name { get; set; }

        [Required]
        public string Subject { get; set; }

        //[Required]
        public string Body { get; set; }

        [Required]
        [StringLength(Metadata.MailTemplateIdLength)]
        public string MailTemplateId { get; set; }

        [ForeignKey("MailTemplateId")]
        public virtual MailTemplate MailTemplate { get; set; }
    }
}
