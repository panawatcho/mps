﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TheMall.MPS.Models.Abstracts;
using TheMall.MPS.Models.Enums;

namespace TheMall.MPS.Models.Mail
{
    public class MailRecipient : BaseTrackableModel
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [Index("IX_ProcessMailType", 1, IsUnique = false)]
        [StringLength(Metadata.ProcessNameLength)]
        public string ProcessName { get; set; }

        //[StringLength(Metadata.ProcessCodeLength)]
        //public string ProcessCode { get; set; }

        [Required]
        [Index("IX_ProcessMailType", 2, IsUnique = false)]
        public MailType MailType { get; set; }

        [Required]
        [StringLength(300)]
        [Index("IX_ProcessMailType", 3, IsUnique = false)]
        public string ActivityName { get; set; }

        [StringLength(100)]
        public string Email { get; set; }

        public MailRecipientType RecipientType { get; set; }
    }
}
