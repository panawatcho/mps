﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TheMall.MPS.Models.Abstracts;
using TheMall.MPS.Models.Enums;

namespace TheMall.MPS.Models.Mail
{
    public class MailSetup : BaseTrackableModel, CrossingSoft.Framework.Models.Interfaces.IPKAutoNumber
    {
        [Key]
        public int Id { get; set; }

        [Index("IX_ProcessMailType", 1, IsUnique = true)]
        [StringLength(Metadata.ProcessNameLength)]
        public string ProcessName { get; set; }

        //[StringLength(Metadata.ProcessCodeLength)]
        //public string ProcessCode{ get; set; }

        [Required]
        [Index("IX_ProcessMailType", 2, IsUnique = true)]
        public MailType MailType { get; set; }

        [StringLength(100)]
        [Index("IX_ProcessMailType", 4, IsUnique = true)]
        public string Extra { get; set; }

        [Required]
        public string MailContentId { get; set; }

        [ForeignKey("MailContentId")]
        public MailContent MailContent { get; set; }

        [StringLength(300)]
        [Index("IX_ProcessMailType", 3, IsUnique = true)]
        public string ActivityName { get; set; }
    }
}
