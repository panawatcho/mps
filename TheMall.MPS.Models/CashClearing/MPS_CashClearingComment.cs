﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrossingSoft.Framework.Models.Interfaces;
using TheMall.MPS.Models.Abstracts;
using TheMall.MPS.Models.Proposal;

namespace TheMall.MPS.Models.CashClearing
{
    public class MPS_CashClearingComment : BaseCommentTable
    {
        [ForeignKey("ParentId")]
        public virtual MPS_CashClearingTable MpsClearingTable { get; set; }
    }
}
