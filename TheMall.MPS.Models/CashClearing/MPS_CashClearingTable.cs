﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.Models.Abstracts;
using TheMall.MPS.Models.CashAdvance;

namespace TheMall.MPS.Models.CashClearing
{
    public class MPS_CashClearingTable : MpsWorkflowTable
    {
        
        [StringLength(Metadata.DocumentNumberLength)]
        public string RefAdvanceNumber { get; set; }
        public DateTime ClearingDate { get; set; }
        [StringLength(100)]
        public string Branch { get; set; }
        public decimal Budget { get; set; }
        public string Description { get; set; }
      
        public int CashAdvanceID { get; set; }

        [ForeignKey("CashAdvanceID")]
        public virtual MPS_CashAdvanceTable CashAdvanceTable { get; set; }
        public virtual ICollection<MPS_CashClearingLine> CashClearingLine { get; set; }
        public virtual ICollection<MPS_CashClearingApproval> CashClearingApproval { get; set; }
        public virtual ICollection<MPS_CashClearingComment> Comments { get; set; }
        public virtual ICollection<MPS_CashClearingAttachment> Attachments { get; set; }
        public decimal Actual { get; set; }

        public string VendorCode { get; set; }
    }
}
