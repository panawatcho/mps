﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrossingSoft.Framework.Models.Interfaces;
using TheMall.MPS.Models.Abstracts;
using TheMall.MPS.Models.CashAdvance;
using TheMall.MPS.Models.Interfaces;

namespace TheMall.MPS.Models.CashClearing
{
    public class MPS_CashClearingApproval : BaseApproval
    {
        
        [ForeignKey("ParentId")]
        public virtual MPS_CashClearingTable CashClearingTable { get; set; }
        public override IWorkflowTable Parent
        {
            get { return CashClearingTable; }
        }
       

    }
}
