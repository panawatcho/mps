﻿using System.ComponentModel.DataAnnotations;
using CrossingSoft.Framework.Infrastructure;
using TheMall.MPS.Models.Abstracts;
using TheMall.MPS.Models.Interfaces;

namespace TheMall.MPS.Models
{
    public class ExampleMasterTable2 : BaseModel, IMasterTable
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public string ExampleMasterTableId { get; set; }
        public virtual ExampleMasterTable ExampleMasterTable { get; set; }
    }
}
