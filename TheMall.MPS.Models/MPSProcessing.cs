﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.Models.Abstracts;
using TheMall.MPS.Models.Proposal;

namespace TheMall.MPS.Models
{
    public sealed class MPSProcessing : BaseModel
    {
        public MPSProcessing()
        {
            this.MPS_ProposalTables = new List<MPS_ProposalTable>();
           
          
        }
        [Key]
        [StringLength(Metadata.MPSProcessing)]
        public string MPSProcessingNo { get; set; }

        public ICollection<MPS_ProposalTable> MPS_ProposalTables { get; set; }
    }
}
