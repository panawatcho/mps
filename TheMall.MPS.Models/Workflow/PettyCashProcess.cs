﻿using System;
using TheMall.MPS.Models.PettyCash;
using TheMall.MPS.Models.Workflow.Abstracts;
using TheMall.MPS.Models.Workflow.Interfaces;


namespace TheMall.MPS.Models.Workflow
{
    public interface IPettyCashProcess : IMpsWorkflowProcessModel<MPS_PettyCashTable>
    {

    }

    public class PettyCashProcess : MpsWorkflowProcessModel<MPS_PettyCashTable>, IPettyCashProcess
    {
        public override string ProcessFullPathNew(MPS_PettyCashTable workflowTable)
        {
            return "MPS\\Processes\\PettyCash";
        }

        public override string AttachmentLibraryNew(MPS_PettyCashTable workflowTable)
        {
            return "PettyCash";
        }
    }
}
