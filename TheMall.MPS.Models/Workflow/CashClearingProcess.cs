﻿using System;
using TheMall.MPS.Models.CashClearing;
using TheMall.MPS.Models.Workflow.Abstracts;
using TheMall.MPS.Models.Workflow.Interfaces;


namespace TheMall.MPS.Models.Workflow
{
    public interface ICashClearingProcess : IMpsWorkflowProcessModel<MPS_CashClearingTable>
    {

    }

    public class CashClearingProcess : MpsWorkflowProcessModel<MPS_CashClearingTable>, ICashClearingProcess
    {
        public override string ProcessFullPathNew(MPS_CashClearingTable workflowTable)
        {
            return "MPS\\Processes\\CashClearing";
        }

        public override string AttachmentLibraryNew(MPS_CashClearingTable workflowTable)
        {
            return "CashClearing";
        }
    }
}
