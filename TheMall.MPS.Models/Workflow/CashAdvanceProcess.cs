﻿using System;
using TheMall.MPS.Models.CashAdvance;
using TheMall.MPS.Models.Workflow.Abstracts;
using TheMall.MPS.Models.Workflow.Interfaces;


namespace TheMall.MPS.Models.Workflow
{
    public interface ICashAdvanceProcess : IMpsWorkflowProcessModel<MPS_CashAdvanceTable>
    {

    }

    public class CashAdvanceProcess : MpsWorkflowProcessModel<MPS_CashAdvanceTable>, ICashAdvanceProcess
    {
        public override string ProcessFullPathNew(MPS_CashAdvanceTable workflowTable)
        {
            return "MPS\\Processes\\CashAdvance";
        }

        public override string AttachmentLibraryNew(MPS_CashAdvanceTable workflowTable)
        {
            return "CashAdvance";
        }

      
    }
}
