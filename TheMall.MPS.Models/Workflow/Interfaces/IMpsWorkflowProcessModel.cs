﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrossingSoft.Framework.Models.Interfaces;
using TheMall.MPS.Models.Interfaces;
using TheMall.MPS.Models.K2;


namespace TheMall.MPS.Models.Workflow.Interfaces
{
    public interface IMpsWorkflowProcessModel<in TWorkflowTable> : IWorkflowProcessModel
        where TWorkflowTable : class, IMpsWorkflowTable
    {
        string ProcessFullPathNew(TWorkflowTable workflowTable);
        string AttachmentLibraryNew(TWorkflowTable workflowTable);
    }
}
