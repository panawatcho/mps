﻿using System;
using TheMall.MPS.Models.Interfaces;
using TheMall.MPS.Models.K2;
using TheMall.MPS.Models.Workflow.Interfaces;


namespace TheMall.MPS.Models.Workflow.Abstracts
{
    public abstract class MpsWorkflowProcessModel<TWorkflowTable> : BaseWorkflowProcessModel, IMpsWorkflowProcessModel<TWorkflowTable>
        where TWorkflowTable : class, IMpsWorkflowTable
    {
        
        public override string ProcessCode
        {
            get { return ProcessModelHelper.ProcessCode(this.GetType()); }
            //get { return this.GetType().Name.Replace("Process", ""); }
        }

        public override string ProcessFullPath
        {
            get { throw new NotImplementedException(); }
        }

        public abstract string ProcessFullPathNew(TWorkflowTable workflowTable);

        public abstract string AttachmentLibraryNew(TWorkflowTable workflowTable);
    }

    public static class ProcessModelHelper
    {
        public static string ProcessCode(Type processModelType)
        {
            return processModelType.Name.Replace("Process", "");
        }
    }
}
