﻿using System;
using TheMall.MPS.Models.Memo;
using TheMall.MPS.Models.MemoIncome;
using TheMall.MPS.Models.Workflow.Abstracts;
using TheMall.MPS.Models.Workflow.Interfaces;


namespace TheMall.MPS.Models.Workflow
{
    public interface IMemoIncomeProcess : IMpsWorkflowProcessModel<MPS_MemoIncomeTable>
    {

    }

    public class MemoIncomeProcess : MpsWorkflowProcessModel<MPS_MemoIncomeTable>, IMemoIncomeProcess
    {
        public override string ProcessFullPathNew(MPS_MemoIncomeTable workflowTable)
        {
            return "MPS\\Processes\\MemoIncome"; //k2***
        }

        public override string AttachmentLibraryNew(MPS_MemoIncomeTable workflowTable)
        {
            return "MemoIncome"; // ไม่ได้ใช้ แต่ต้องมีให้ครบ
        }
    }
}
