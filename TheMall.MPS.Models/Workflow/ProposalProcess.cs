﻿using System;
using TheMall.MPS.Models.Proposal;
using TheMall.MPS.Models.Workflow.Abstracts;
using TheMall.MPS.Models.Workflow.Interfaces;


namespace TheMall.MPS.Models.Workflow
{
    public interface IProposalProcess : IMpsWorkflowProcessModel<MPS_ProposalTable>
    {
       
    }

    public class ProposalProcess : MpsWorkflowProcessModel<MPS_ProposalTable>, IProposalProcess
    {
        public override string ProcessFullPathNew(MPS_ProposalTable workflowTable)
        {
            if (workflowTable.StatusFlag == 8)
            {
                return "MPS\\Processes\\ChangeProposal";
            }
            else
            {
                return "MPS\\Processes\\Proposal";
            }
           // return "MPS\\Processes\\Proposal";
        }
       
       
        public override string AttachmentLibraryNew(MPS_ProposalTable workflowTable)
        {
            return "Proposal";
        }
    }
}
