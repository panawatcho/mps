﻿using System;
using TheMall.MPS.Models.Memo;
using TheMall.MPS.Models.Workflow.Abstracts;
using TheMall.MPS.Models.Workflow.Interfaces;


namespace TheMall.MPS.Models.Workflow
{
    public interface IMemoProcess : IMpsWorkflowProcessModel<MPS_MemoTable>
    {

    }

    public class MemoProcess : MpsWorkflowProcessModel<MPS_MemoTable>, IMemoProcess
    {
        public override string ProcessFullPathNew(MPS_MemoTable workflowTable)
        {
            return "MPS\\Processes\\Memo";
        }

        public override string AttachmentLibraryNew(MPS_MemoTable workflowTable)
        {
            return "Memo";
        }
    }
}
