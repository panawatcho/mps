﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrossingSoft.Framework.Models.Interfaces;
using TheMall.MPS.Models.Abstracts;


namespace TheMall.MPS.Models.SubDeposit
{
    public class MPS_SubDepositComment : BaseCommentTable
    {
        [ForeignKey("ParentId")]
        public virtual MPS_SubDepositTable SubDepositTable { get; set; }
    }
}
