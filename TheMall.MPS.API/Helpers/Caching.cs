﻿using System;
using System.Runtime.Caching;
using System.Threading;

namespace TheMall.MPS.API.Helpers
{
    public class Caching
    {
        private static MemoryCache _cache = MemoryCache.Default;

        public static void AddToCache<T>(string key, T obj, CacheItemPolicy policy = null)
        {
            AddToCache(_cache, key, obj, policy);
        }

        public static void AddToCache<T>(MemoryCache cache, string key, T obj, CacheItemPolicy policy = null)
        {
            if (policy == null)
            {
                policy = new CacheItemPolicy();
                policy.AbsoluteExpiration =
                    DateTimeOffset.Now.AddHours(8.0);
            }
            cache.AddOrGetExisting(key, obj, policy);
        }

        public static T GetFromCache<T>(string key)
        {
            return GetFromCache<T>(_cache, key);
        }

        public static T GetFromCache<T>(MemoryCache cache, string key)
        {
            return (T)cache[key];
        }

        public static T GetFromCache<T>(string key, Func<T> valueFactory, CacheItemPolicy policy = null)
        {
            return GetFromCache(_cache, key, valueFactory, policy);
        }

        public static T GetFromCache<T>(MemoryCache cache, string key, Func<T> valueFactory, CacheItemPolicy policy = null)
        {
            //var newValue = new Lazy<T>(valueFactory, LazyThreadSafetyMode.PublicationOnly);
            var newValue = new Lazy<T>(valueFactory, LazyThreadSafetyMode.ExecutionAndPublication);
            var value = (Lazy<T>)cache.AddOrGetExisting(key, newValue, policy);
            return (value ?? newValue).Value; // Lazy<T> handles the locking itself
        }

        public static void RemoveFromCache(string key)
        {
            RemoveFromCache(_cache, key);
        }
        public static void RemoveFromCache(MemoryCache cache, string key)
        {
            cache.Remove(key);
        }

        public static void ClearAll()
        {
            ClearAll(_cache);
        }

        public static void ClearAll(MemoryCache cache)
        {
            foreach (var c in cache)
            {
                cache.Remove(c.Key);
            }
        }
    }
}