﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Routing;
using SourceCode.Workflow.Client;
using TheMall.MPS.API.Facades.Abstracts;
using TheMall.MPS.API.Facades.Interfaces;
using TheMall.MPS.API.Mappers;
using TheMall.MPS.API.Services;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Models.CashAdvance;
using TheMall.MPS.Models.CashClearing;
using TheMall.MPS.Models.Enums;
using TheMall.MPS.Models.K2;
using TheMall.MPS.Models.Workflow;
using TheMall.MPS.ViewModels.CashAdvance;
using TheMall.MPS.ViewModels.CashClearing;

namespace TheMall.MPS.API.Facades
{
    public interface ICashClearingFacade : IMpsWorkflowFacade<MPS_CashClearingTable, MPS_CashClearingAttachment, MPS_CashClearingComment, CashClearingViewModel>
    {
        Task<CashClearingViewModel> GetViewModelByCashAdvanceId(UrlHelper url, int id, string sn = null);
        CashClearingViewModel CashClearingFromCashAdvance(int id);
        CashClearingViewModel CashClearingFromDocumentNumber(string documentNumber);
        void SetFinalApproverDeleted(int id);
        string GetEmailInformAll(int workflowTableId);
    }
    public class CashClearingFacade : BaseMpsWorkflowFacade<MPS_CashClearingTable, MPS_CashClearingAttachment, MPS_CashClearingComment, CashClearingViewModel>, ICashClearingFacade
    {
        private readonly IMPSSession _session;
        private readonly ICashClearingService _workflowTableService;
        private readonly ICashClearingLineService _lineService;
        private readonly IAttachmentService<MPS_CashClearingAttachment, MPS_CashClearingTable> _attachmentService;
        private readonly ICommentService<MPS_CashClearingComment> _commentService;
        private readonly IWorkflowService<MPS_CashClearingTable> _workflowService;
        private readonly ICashClearingMapper _mapper;
        private readonly ICashClearingLineMapper _lineMapper;
        private readonly ICashClearingProcess _definition;
        private readonly ICashClearingApprovalService _approvalService;
        private readonly ICashClearingApprovalMapper _approveMapper;
        private readonly ICashAdvanceService _cashAdvanceService;
        private readonly IEmployeeTableService _employeeTableService;
        private readonly ICompanyService _companyService;
        
        public CashClearingFacade(
            IMPSSession session,
            ICashClearingService workflowTableService, 
            IAttachmentService<MPS_CashClearingAttachment, MPS_CashClearingTable> attachmentService, 
            ICommentService<MPS_CashClearingComment> commentService,
            IWorkflowService<MPS_CashClearingTable> workflowService, 
            INumberSequenceService numberSequenceService,
            ICashClearingProcess definition, 
            ICashClearingLineService lineService, 
            ICashClearingMapper mapper, 
            ICashClearingLineMapper lineMapper,
            ICashClearingApprovalService approvalService, 
            ICashClearingApprovalMapper approveMapper, 
            ICashAdvanceService cashAdvanceService, 
            IEmployeeTableService employeeTableService, 
            ICompanyService companyService)
            : base(session, workflowTableService, attachmentService, commentService, workflowService, numberSequenceService, definition)
        {
            _session = session;
            _workflowTableService = workflowTableService;
            _lineService = lineService;
            _mapper = mapper;
            _lineMapper = lineMapper;
            _approvalService = approvalService;
            _approveMapper = approveMapper;
            _cashAdvanceService = cashAdvanceService;
            _employeeTableService = employeeTableService;
            _companyService = companyService;
            _definition = definition;
            _workflowService = workflowService;
            _commentService = commentService;
            _attachmentService = attachmentService;
        }

        public override async Task<CashClearingViewModel> GetViewModel(UrlHelper url, int id, string sn = null)
        {
            var model = await _workflowTableService.GetAsync(id);
            var viewModel =
                await this.InitWorkflowViewModel(url,id, sn, _mapper.ToViewModel(model));

            return viewModel;
        }

        public override IEnumerable<ValidationResult> ValidateStartProcess(MPS_CashClearingTable workflowTable)
        {
            if (workflowTable.CashAdvanceTable.ProposalLine.ProposalTable.CloseFlag == true)
            {
                yield return new ValidationResult("Cannot submit because your proposal was closed!!!");
            }
            //if (string.IsNullOrEmpty(workflowTable.AccountingUsername))
            if (workflowTable.AccountingBranch == null)
            {
                yield return new ValidationResult("Please select accounting!!!");
            }

            //if (!workflowTable.CashClearingApproval.Any(m => m.ApproverLevel.Equals(Approval.ApproveLevel.Propose) && !m.Deleted))
            //{
            //    yield return new ValidationResult("Please select approver level propose!!!");
            //}

            //if (!workflowTable.CashClearingApproval.Any(m => m.ApproverLevel.Equals(Approval.ApproveLevel.Accept) && !m.Deleted))
            //{
            //    yield return new ValidationResult("Please select approver level accept!!!");
            //}

            //if (!workflowTable.CashClearingApproval.Any(m => m.ApproverLevel.Equals(Approval.ApproveLevel.Approval) && !m.Deleted))
            //{
            //    yield return new ValidationResult("Please select a approver!!!");
            //}

            if (!workflowTable.CashClearingApproval.Any(m => m.ApproverLevel.Equals(Approval.ApproveLevel.FinalApproval) && !m.Deleted && m.LockEditable))
            {
                yield return new ValidationResult("Please select a final apporver!!!");
            }

            //// checkActual = 0 ได้ คืนเงินได้ไหม เช็กจาก Remainning (proposal)++
            //if (workflowTable.Actual <= 0)
            //{
            //    yield return new ValidationResult("Actual must not be 0");
            //}
        }

        public override IEnumerable<ValidationResult> ValidateSubmit(MPS_CashClearingTable workflowTable, WorklistItem worklistItem, string action,
            CashClearingViewModel viewModel)
        {
            if (action.ToUpper().Equals("RESUBMIT"))
            {

                if (viewModel.ProposalRef.CloseFlag == true)
                {
                    yield return new ValidationResult("Cannot submit because your proposal was closed!!!");
                }

                //if (string.IsNullOrEmpty(workflowTable.AccountingUsername))
                if (viewModel.Accounting == null)
                {
                    yield return new ValidationResult("Please select accounting!!!");
                }

                //if (
                //    !viewModel.CashClearingApproval.Any(
                //        m => m.ApproverLevel.Equals(Approval.ApproveLevel.Propose) && !m.Deleted))
                //{
                //    yield return new ValidationResult("Please select approver level propose!!!");
                //}

                //if (
                //    !viewModel.CashClearingApproval.Any(
                //        m => m.ApproverLevel.Equals(Approval.ApproveLevel.Accept) && !m.Deleted))
                //{
                //    yield return new ValidationResult("Please select approver level accept!!!");
                //}

                //if (
                //    !viewModel.CashClearingApproval.Any(
                //        m => m.ApproverLevel.Equals(Approval.ApproveLevel.Approval) && !m.Deleted))
                //{
                //    yield return new ValidationResult("Please select a approver!!!");
                //}

                if (
                    !viewModel.CashClearingApproval.Any(
                        m => m.ApproverLevel.Equals(Approval.ApproveLevel.FinalApproval) && !m.Deleted && m.LockEditable))
                {
                    yield return new ValidationResult("Please select a final apporver!!!");
                }

                if (viewModel.Actual <= 0)
                {
                    yield return new ValidationResult("Actual must not be 0");
                }


                if (viewModel.Actual > viewModel.Budget)
                {
                    yield return new ValidationResult("The actual value must not be more than the budget!!!");
                }
            }
        }

        public override async Task<MPS_CashClearingComment> Submit(string sn, string action, string comment, CashClearingViewModel viewModel)
        {
            var result = await base.Submit(sn, action, comment, viewModel);
            if (result != null)
            {
                if (result.Action.ToUpper().Equals("RESUBMIT", StringComparison.CurrentCultureIgnoreCase))
                {
                      var approval = _approvalService.GetWhere(m => m.ParentId == viewModel.Id && !m.Deleted);
                    foreach (var a in approval)
                    {
                        a.ApproveStatus = false;
                        _approvalService.Update(a, a.Id);
                    }
                    await Session.SaveChangesAsync();
                }
                else
                {
                    var activity = result.Activity.Replace("Approval", "").Replace("-", "").Replace(" ", "");
                        var modelApprove1 = _approvalService.GetWhere(m => activity.Equals(m.ApproverLevel)
                                                                           && m.ParentId == result.ParentId).ToList();

                        var modelApprove =
                            modelApprove1.LastOrDefault(m => m.ApproverUserName.ToLower().Equals(result.CreatedBy.ToLower()));
                        if (modelApprove != null)
                        {
                            modelApprove.ApproveStatus = true;
                            _approvalService.Update(modelApprove, modelApprove.Id);
                            Session.SaveChanges();
                        }  
                }
            }

            return result;
        }

        public async Task<CashClearingViewModel> GetViewModelByCashAdvanceId(UrlHelper url, int id, string sn = null)
        {
            try
            {
                var model = _workflowTableService.Find(m => m.CashAdvanceID == id);
                if (model != null)
                {
                    var viewModel =
                    await this.InitWorkflowViewModel(url,id, sn, _mapper.ToViewModel(model));
                    return viewModel;
                }
                else
                {
                    return CashClearingFromCashAdvance(id);
                }
            }
            catch (Exception ex)
            {
                throw new HttpException(ex.Message);
            }
        }

        public override async Task<CashClearingViewModel> SaveAsync(CashClearingViewModel viewModel, string action = null)
        {
            if (viewModel == null)
                throw new ArgumentNullException("viewModel");
            
            if (viewModel.BudgetSum > viewModel.Budget)
               throw new System.InvalidOperationException("The budget sum value must not be more than the butget plan!!!");

            //foreach (var data in viewModel.CashClearingLines)
            //{
            //    if(data.Actual == null || data.Actual == 0)
            //    throw new System.InvalidOperationException("Actual must not be 0 or Empty!!!");
            //}

            if (viewModel.Company == null)
            {
                var vendorCode = _companyService.Find(m => m.VendorCode.Equals(viewModel.Branch.PlaceCode));
                if (vendorCode != null)
                {
                    viewModel.Company = vendorCode.ToViewModel();
                }
                else
                {
                    viewModel.Company = null;
                }
            }

            var model = _mapper.ToModel(viewModel);
            try
            {
                if (viewModel.Id <= 0)
                {
                    model = _workflowTableService.Insert(model);
                }
                else
                {
                    model = _workflowTableService.Update(model);
                }
                await _session.SaveChangesAsync();
                var existingModel = await _workflowTableService.FirstOrDefaultAsync(m => m.Id == model.Id);

                var cashClearingLines =
                    _lineMapper.ToModels(viewModel.CashClearingLines, model.CashClearingLine).ToListSafe();
                foreach (var line in cashClearingLines)
                {
                    if (line.Id <= 0 || line.ParentId <= 0)
                    {
                        if (model.Id <= 0)
                        {
                            line.CashClearingTable = model;
                        }
                        else
                        {
                            line.ParentId = existingModel.Id;
                        }
                        _lineService.Insert(line);
                    }
                    else
                    {
                        if (line.Deleted)
                        {
                            _lineService.Delete(line);
                        }
                        else
                        {
                            _lineService.Update(line, line.Id);
                        }
                    }
                }
                await Session.SaveChangesAsync();

                var approval = _approveMapper.ToModels(viewModel.CashClearingApproval, model.CashClearingApproval).ToListSafe();
                foreach (var approve in approval)
                {
                    if (approve.Id <= 0 || approve.ParentId <= 0)
                    {
                        if (model.Id <= 0)
                        {
                            approve.CashClearingTable = model;
                        }
                        else
                        {
                            approve.ParentId = model.Id;
                        }
                        _approvalService.Insert(approve);
                    }
                    else
                    {
                        if (approve.Deleted)
                        {
                            _approvalService.Delete(approve);
                        }
                        else
                        {
                            _approvalService.Update(approve, approve.Id);
                        }
                    }
                }

                await Session.SaveChangesAsync();

                return _mapper.ToViewModel(model);
            }
            catch (Exception ex)
            {
                throw new HttpException(ex.Message);
            }
        }

        public CashClearingViewModel CashClearingFromCashAdvance(int id)
        {
            try
            {
                var cashAdvanceModel = _cashAdvanceService.Find(m => m.Id == id);
                var viewModel = _mapper.CashClearingFromCashAdvanceViewModel(cashAdvanceModel);
                return viewModel;
            }
            catch (Exception ex)
            {
                throw new HttpException(ex.Message);
                //return null;
                //throw new HttpException("555+");
            }
        }

        public CashClearingViewModel CashClearingFromDocumentNumber(string documentNumber)
        {
            var model = _workflowTableService.FirstOrDefault(
               m => m.DocumentNumber.Equals(documentNumber, StringComparison.InvariantCultureIgnoreCase));
            var viewModel = _mapper.ToViewModel(model);

            return viewModel;
        }
        public void SetFinalApproverDeleted(int id)
        {
            try
            {
                var finalApprovers =
                    _approvalService.GetWhere(m => m.ParentId == id && m.ApproverLevel == "อนุมัติขั้นสุดท้าย");
                foreach (var finalApprover in finalApprovers)
                {
                    _approvalService.Delete(finalApprover);
                }
                Session.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new HttpException(ex.Message);
            }
        }

        public string GetEmailInformAll(int workflowTableId)
        {
            string email = string.Empty;
            var model = _workflowTableService.Find(m => m.Id == workflowTableId);
            if (model != null)
            {
                email += string.IsNullOrEmpty(email) ? model.CCEmail : string.Format(";{0}", model.CCEmail).TrimEnd(';');
                email += string.IsNullOrEmpty(email) ? model.InformEmail : string.Format(";{0}", model.InformEmail).TrimEnd(';');
                email += string.IsNullOrEmpty(email) ? model.AccountingEmail : string.Format(";{0}", model.AccountingEmail).TrimEnd(';');
                if (model.CashClearingApproval != null)
                {
                    foreach (var userapprove in model.CashClearingApproval)
                    {
                        if (!string.IsNullOrEmpty(userapprove.ApproverEmail))
                        {
                            email += string.IsNullOrEmpty(email)
                                   ? userapprove.ApproverEmail
                                   : string.Format(";{0}", userapprove.ApproverEmail).TrimEnd(';');
                        }
                        else
                        {
                            var employee = _employeeTableService.FindByUsername(userapprove.ApproverUserName);
                            if (employee != null && !string.IsNullOrEmpty(employee.Email))
                            {
                                email += string.IsNullOrEmpty(email)
                                    ? employee.Email
                                    : string.Format(";{0}", employee.Email).TrimEnd(';');
                            }
                        }
                    }
                }
            }
            if (!string.IsNullOrEmpty(email))
            {
                string[] listEmail = email.TrimEnd(';').Split(';');
                string[] newlistEmail = listEmail.Distinct().ToArray();
                email = newlistEmail.Aggregate(string.Empty, (current, mail) => current + (string.IsNullOrEmpty(current) ? mail : string.Format(";{0}", mail)));
            }

            return email;
        }
    }
}