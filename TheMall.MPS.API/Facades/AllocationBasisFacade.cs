﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using TheMall.MPS.API.Facades.Abstracts;
using TheMall.MPS.API.Mappers;
using TheMall.MPS.API.Services;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Models.Master;
using TheMall.MPS.ViewModels.Master;

namespace TheMall.MPS.API.Facades
{
    public interface IAllocationBasisFacade : IBaseMasterFacade<MPS_M_AllocationBasis, AllocationBasisViewModel>
    {
       // IEnumerable<AllocationBasisViewModel> Search(string allocCode = null);
        AllocationBasisViewModel Search(string allocCode = null);
    }
    public class AllocationBasisFacade : BaseMasterFacade<MPS_M_AllocationBasis, AllocationBasisViewModel>, IAllocationBasisFacade
    {
        private readonly IMPSSession _session;
        private readonly IAllocationBasisService _allocationBasisService;
        public AllocationBasisFacade(
            IMPSSession session, 
            IAllocationBasisService service) 
            : base(session, service)
        {
            _allocationBasisService = service;
            _session = session;
        }

        public override async Task<AllocationBasisViewModel> Create(AllocationBasisViewModel viewModel)
        {
            try
            {
                var model = _allocationBasisService.Insert(viewModel.ToModel());
                await _session.SaveChangesAsync();
                return model.ToViewModel();
            }
            catch (Exception exception)
            {

                throw new Exception("Cannot insert duplicate allocation code !!!");
            }
        }

        public override async Task<AllocationBasisViewModel> Update(AllocationBasisViewModel viewModel)
        {
            try
            {
                var model = _allocationBasisService.Update(viewModel.ToModel());
                await _session.SaveChangesAsync();
                return model.ToViewModel();
            }
            catch (Exception exception)
            {

                throw new Exception("Cannot update !!!");
            }
        }

        public override async Task<AllocationBasisViewModel> Delete(AllocationBasisViewModel viewModel)
        {
            try
            {
                _allocationBasisService.Delete(viewModel.ToModel());
                await _session.SaveChangesAsync();
                return viewModel;
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
        }

        public AllocationBasisViewModel Search(string allocCode = null)
        {
            try
            {
                var viewModel =
                    _allocationBasisService.FirstOrDefault(
                        m => m.AllocCode.Equals(allocCode, StringComparison.InvariantCultureIgnoreCase)).ToViewModel();
                return viewModel;
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
        }
    }
}