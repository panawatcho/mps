﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using CrossingSoft.Framework.Infrastructure.Session;
using TheMall.MPS.API.Facades.Abstracts;
using TheMall.MPS.API.Mappers;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.API.Services;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Models.Master;
using TheMall.MPS.ViewModels.Master;

namespace TheMall.MPS.API.Facades
{
    public interface IAllocateBasisFacade : IBaseMasterFacade<MPS_M_AllocateBasis, AllocateBasisViewModel>
    {

        Task<AllocateBasisViewModel> SaveAsync(AllocateBasisViewModel viewModel);
        IEnumerable<AllocateBasisViewModel> Search(string code = null, string name = null);
        Task<IEnumerable<MPS_M_AllocateBasis>> GetAllAsyns();
        MPS_M_AllocateBasis GetAsync(string code);
    }

    public class AllocateBasisFacade : BaseMasterFacade<MPS_M_AllocateBasis, AllocateBasisViewModel>, IAllocateBasisFacade
    {
        private readonly IMPSSession _session;
        private readonly IAllocateBasisService _service;
        private readonly ISharedPercentService _sharedService;

        public AllocateBasisFacade(IMPSSession session, IAllocateBasisService service,ISharedPercentService sharedService) 
            : base(session, service)
        {
            _session = session;
            _service = service;
            _sharedService = sharedService;
        }

        public override async Task<AllocateBasisViewModel> Create(AllocateBasisViewModel viewModel)
        {
            try
            {
                var model = _service.Insert(viewModel.ToModel());
                await _session.SaveChangesAsync();
                return  model.ToViewModel();
            }
            catch (Exception exception)
            {

                throw new Exception("Cannot insert duplicate code");
            }
        }

        public override async Task<AllocateBasisViewModel> Update(AllocateBasisViewModel viewModel)
        {
            var model = _service.Update(viewModel.ToModel());
            await _session.SaveChangesAsync();
            return model.ToViewModel();
        }

        public override async Task<AllocateBasisViewModel> Delete(AllocateBasisViewModel viewModel)
        {
            try
            {
                _service.Delete(viewModel.ToModel());
                await _session.SaveChangesAsync();
                return viewModel;
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
        }
         public Task<IEnumerable<MPS_M_AllocateBasis>> GetAllAsyns()
         {
             return _service.GetAllAsyns();
         }
         public MPS_M_AllocateBasis GetAsync(string code)
         {
             return _service.Find(m=>m.AllocateType.ToLower() == code.ToLower());
         }
          public async Task<AllocateBasisViewModel> SaveAsync(AllocateBasisViewModel viewModel)
          {
          
                  if (viewModel == null)
                      throw new ArgumentNullException("viewModel");

                  var model = viewModel.ToModel();
                  try
                  {
                      var already = _service.Find(m => m.AllocateType == model.AllocateType);
                      if (already == null)
                      {
                          model = _service.Insert(model);
                      }
                      else
                      {
                          model = _service.Update(model,model.AllocateType);
                      }

                      var shared = viewModel.SharedPercents.ToModels();
                 
                      foreach (var sh in shared)
                      {
                          if (sh.Id <= 0 || string.IsNullOrEmpty(sh.AllocateType))
                          {
                              if (string.IsNullOrEmpty(sh.AllocateType))
                              {
                                  sh.Allocate = model;
                              }
                              else
                              {
                                  sh.AllocateType = model.AllocateType;
                              }
                            _sharedService.Insert(sh);
                          }
                          else
                          {
                              if (sh.Deleted)
                              {
                                  _sharedService.Delete(sh);
                              }
                              else
                              {
                                  _sharedService.Update(sh, sh.Id);
                              }
                          }
                      }
                      await _session.SaveChangesAsync();

                      return model.ToViewModel();
                  }
                  catch (Exception ex)
                  {
                      throw new HttpException(ex.Message);
                  }
          
          }
      public IEnumerable<AllocateBasisViewModel> Search(string code = null, string name = null)
      {
          var viewModel = _service.Search(code, name).ToViewModels();
          return viewModel;
      }

    }
}