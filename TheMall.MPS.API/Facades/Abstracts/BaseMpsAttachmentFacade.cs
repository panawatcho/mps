﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using CrossingSoft.Framework.Infrastructure.Session;
using TheMall.MPS.API.Services;
using TheMall.MPS.Models.Abstracts;
using TheMall.MPS.Resources;
using TheMall.MPS.ViewModels;


namespace TheMall.MPS.API.Facades.Abstracts
{
    public abstract class BaseMpsAttachmentFacade<TAttachmentTable> : BaseFacade
    
        where TAttachmentTable : BaseAttachmentTable, new()
    {
        protected readonly ISessionAsync Session;
        private readonly IAttachmentService<TAttachmentTable> _attachmentService;
        protected BaseMpsAttachmentFacade(
            ISessionAsync session,
            IAttachmentService<TAttachmentTable> attachmentService)
        {
            Session = session;
            _attachmentService = attachmentService;
        }



        public TAttachmentTable GetAttachment(string id, string fileName, int documentType)
        {
            int i = 0;
            if (Int32.TryParse(id, out i))
            {
                var result = _attachmentService.GetDocTypeFiles(id, i, fileName, documentType);

                var attachments = result as TAttachmentTable[] ?? result.ToArray();

                return attachments.LastOrDefault();
            }
            else
            {
                var result = _attachmentService.GetDocTypeFiles(id, fileName, documentType);

                var attachments = result as TAttachmentTable[] ?? result.ToArray();

                return attachments.LastOrDefault();
            }
        }

        public TAttachmentTable GetAttachment(string id, string fileName)
        {
            int i = 0;
            if (Int32.TryParse(id, out i))
            {
                var result = _attachmentService.GetAttachments(i, fileName);

                var attachments = result as TAttachmentTable[] ?? result.ToArray();

                return attachments.LastOrDefault();
            }
            else
            {
                var result = _attachmentService.GetAttachments(id, fileName);

                var attachments = result as TAttachmentTable[] ?? result.ToArray();

                return attachments.LastOrDefault();

            }
        }
        //public async Task<TAttachmentTable> SaveAttachmentAsync(HttpContentHeaders headers, string contentType, Stream stream, int id, string username, string name, string title, int? documentType, string sn, string documentNumber)

        public async virtual Task<TAttachmentTable> SaveAttachmentAsync<TPostedAttachment>(
            HttpContentHeaders headers,
            string contentType,
            Stream stream,
            string username,
            TPostedAttachment viewModel)
            where TPostedAttachment : IPostedAttachmentViewModel
        {
            //if (!_attachmentService.ValidateFileExtension(name))
            if (!_attachmentService.ValidateFileExtension(viewModel))
            {
                return null;
            }
            //var result = await _attachmentService.SaveAttachmentAsync(headers, contentType, stream, name, title,
            //    id, username, documentType, sn, documentNumber);
            var result = await _attachmentService.SaveAttachmentAsync(headers, contentType, stream, username, viewModel);
            if (result != null)
            {
                await Session.SaveChangesAsync();
                return result;
            }
            else
            {
                throw new Exception(Error.UnknownError);
            }
        }


        public async Task<bool> RemoveAttachments(int id, int? documentType, string sn, string documentNumber = null, params string[] fileNames)
        {
            try
            {
                var result = await _attachmentService.RemoveAttachments(id, documentType, sn, documentNumber, fileNames);

                var attachments = result as TAttachmentTable[] ?? result.ToArray();
                if (result != null && attachments.Any())
                {
                    await Session.SaveChangesAsync();
                }

                return true;
            }
            catch (Exception exception)
            {
                throw;
            }

        }

        ////public NetworkCredential AdminNetworkCredential()
        ////{
        ////    return _attachmentService.AdminNetworkCredential();
        ////}

        public Task<StreamContent> GetStreamContent(TAttachmentTable attachment)
        {
            return _attachmentService.GetStreamContent(attachment);
        }
        //#endregion
    }
}