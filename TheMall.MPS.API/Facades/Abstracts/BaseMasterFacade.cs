﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using CrossingSoft.Framework.Infrastructure;
using CrossingSoft.Framework.Infrastructure.Session;

using TheMall.MPS.API.Facades.Abstracts;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Models.Interfaces;
using TheMall.MPS.ViewModels.Interfaces;

namespace TheMall.MPS.API.Facades.Abstracts
{
    public interface IBaseMasterFacade<TMasterTable, TMasterViewModel> where TMasterViewModel : IMasterViewModel
    {
        Task<TMasterViewModel> Create(TMasterViewModel viewModel);
        Task<TMasterViewModel> Update(TMasterViewModel viewModel);
        Task<TMasterViewModel> Delete(TMasterViewModel viewModel);
    }
    public abstract class BaseMasterFacade<TMasterTable, TMasterViewModel> : BaseFacade, IBaseMasterFacade<TMasterTable, TMasterViewModel> where TMasterViewModel : IMasterViewModel where TMasterTable : IObjectState

    {
        protected readonly IMPSSession Session;
        protected readonly IBaseMasterService<TMasterTable> Service;
        protected BaseMasterFacade(
            IMPSSession session,
            IBaseMasterService<TMasterTable> service
        )
        {
            Session = session;
            Service = service;
        }

        public abstract Task<TMasterViewModel> Create(TMasterViewModel viewModel);
        public abstract Task<TMasterViewModel> Update(TMasterViewModel viewModel);
        public abstract Task<TMasterViewModel> Delete(TMasterViewModel viewModel);
    }
}