﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Routing;
using CrossingSoft.Framework.Models.Enums;
using SourceCode.Workflow.Client;
using TheMall.MPS.API.Exceptions;
using TheMall.MPS.API.Facades.Interfaces;
using TheMall.MPS.API.Mappers;
using TheMall.MPS.API.Services;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Models.Abstracts;
using TheMall.MPS.Models.Enums;
using TheMall.MPS.Models.K2;
using TheMall.MPS.Resources;
using TheMall.MPS.ViewModels.Abstracts;
using TheMall.MPS.ViewModels.Interfaces;

namespace TheMall.MPS.API.Facades.Abstracts
{
    public abstract class BaseMpsWorkflowFacade<TWorkflowTable, TAttachmentTable, TCommentTable, TWorkflowViewModel>
          : BaseMpsAttachmentFacade<TAttachmentTable>, 
        IMpsWorkflowFacade<TWorkflowTable, TAttachmentTable, TCommentTable, TWorkflowViewModel>
        where TWorkflowTable : MpsWorkflowTable, new()
        where TAttachmentTable : BaseAttachmentTable, new()
        where TCommentTable : BaseCommentTable, new()
        where TWorkflowViewModel : BaseWorkflowViewModel, IMpsWorkflowViewModel 
    { 
        private readonly IMpsWorkflowTableService<TWorkflowTable> _workflowTableService;
        private readonly IAttachmentService<TAttachmentTable, TWorkflowTable> _attachmentService;
        private readonly ICommentService<TCommentTable> _commentService;
        private readonly IWorkflowService<TWorkflowTable> _workflowService;
        private readonly INumberSequenceService _numberSequenceService;
        private readonly IWorkflowProcessModel _definition;
        //Instantiate a Singleton of the Semaphore with a value of 1. This means that only 1 thread can be granted access at a time.
        static SemaphoreSlim semaphoreSlim = new SemaphoreSlim(1, 1);

        protected BaseMpsWorkflowFacade(
            IMPSSession session,
            IMpsWorkflowTableService<TWorkflowTable> workflowTableService,
            IAttachmentService<TAttachmentTable, TWorkflowTable> attachmentService,
            ICommentService<TCommentTable> commentService,
            IWorkflowService<TWorkflowTable> workflowService,
            INumberSequenceService numberSequenceService,
            IWorkflowProcessModel definition)
            : base(session, attachmentService)
        {
            _workflowTableService = workflowTableService;
            _attachmentService = attachmentService;
            _commentService = commentService;
            _workflowService = workflowService;
            _numberSequenceService = numberSequenceService;
            _definition = definition;
        }

        //public const string Action_SentNext = "Send Next";
        //public const string Action_SentBack = "Send Back";
        //public const string Action_Revise = "Revise";
        public abstract Task<TWorkflowViewModel> GetViewModel(UrlHelper url,int id, string sn = null);
        public TAttachmentTable GetAttachment(int id, int documentType, string fileName)
        {
            var result = _attachmentService.GetDocTypeFiles(fileName, documentType);

            var attachments = result as TAttachmentTable[] ?? result.ToArray();

            return attachments.LastOrDefault();
        }

        //
        public TAttachmentTable GetAttachment(int id, string fileName)
        {
            var result = _attachmentService.GetAttachments(fileName);

            var attachments = result as TAttachmentTable[] ?? result.ToArray();

            return attachments.LastOrDefault();
        }
        #region View Model
        protected async Task<TWorkflowViewModel> InitWorkflowViewModel(System.Web.Http.Routing.UrlHelper url, int id, string sn, TWorkflowViewModel viewModel)
        {
            if (viewModel == null)
            {
                throw new HttpException(id.ToString());
                //throw HttpExceptionHelper.DataWithIDNotExists(id);
                //throw new Exception(Error.UnknownError);
            }

            var tmp = await _commentService.GetCommentsAsync(id);
            viewModel.Comments = tmp.ToViewModel();
            viewModel.OtherFiles = (await _attachmentService.GetAttachmentsAsync(id)).ToViewModel(url);
            var commentFiles = (await _attachmentService.GetAttachmentsWithSNAsync(id)).ToCommentViewModel(url);
            if (!string.IsNullOrEmpty(sn) && commentFiles.All(m => m.Key != sn))
            {
                commentFiles.Add(sn, Enumerable.Empty<ViewModels.AttachmentViewModel>());
            }
            viewModel.CommentFiles = commentFiles;
            viewModel.DocTypeFiles = (await _attachmentService.GetDocTypeFilesAsync(id)).ToDocTypeViewModel(url);
          
            if (string.IsNullOrEmpty(viewModel.DocumentNumber))
                viewModel.DocumentStatus = DocumentStatus.Draft.ToString();
            return viewModel;
        }
        #endregion
        #region K2

        //
        private System.Object lockThis = new System.Object();
        public async virtual Task<int> StartProcess(int id)
        {
           
                TCommentTable createdComment = null;

                var workflowTable = await _workflowTableService.GetWorkflowTableAsync(id);

                int? procInstId = null;

                var validateStartProcessResult1 = this.ValidateStartProcess(workflowTable).ToListSafe();
                var validateStartProcessResult2 = _workflowService.ValidateStartProcess(workflowTable).ToListSafe();
                //if (await this.ValidateStartProcess(workflowTable) && await _workflowService.ValidateStartProcess(workflowTable))
                if ((validateStartProcessResult1 == null || !validateStartProcessResult1.Any())
                    && (validateStartProcessResult2 == null || !validateStartProcessResult2.Any()))
                {
                   
                    if (await AtomicUpdateBeforeStartProcess(workflowTable))
                    {
                        workflowTable = _workflowTableService.Update(workflowTable);
                        await Session.SaveChangesAsync();
                    }
                    //Asynchronously wait to enter the Semaphore. If no-one has been granted access to the Semaphore, code execution will proceed, otherwise this thread waits here until the semaphore is released 
                    await semaphoreSlim.WaitAsync();
                    try
                    {
                        if (await UpdateBeforeStartProcessAsync(workflowTable))
                        {
                            workflowTable = _workflowTableService.Update(workflowTable);
                        }
                        if (AllowStartK2Process())
                        {
                            var dataFields = await _workflowService.InitDataFields(workflowTable);
                            var folio = _workflowService.InitFolio(workflowTable, dataFields);
                            if (procInstId == null)
                            {
                                procInstId = _workflowService.StartProcess(workflowTable, folio, dataFields);
                            }

                            if (await UpdateAfterStartProcess(workflowTable, procInstId))
                            {
                                workflowTable = _workflowTableService.Update(workflowTable);
                            }
                            if (createdComment == null)
                            {
                                createdComment = _commentService.CreateCommentStart(workflowTable, procInstId.Value);
                            }

                            await Session.SaveChangesAsync();
                            // scope.Complete();
                            //return procInstId.Value;
                        }
                        else
                        {
                            if (await UpdateAfterStartProcess(workflowTable, 0))
                            {
                                workflowTable = _workflowTableService.Update(workflowTable);
                            }
                            _commentService.CreateCommentStart(workflowTable, procInstId ?? 0);
                            await Session.SaveChangesAsync();
                            //scope.Complete();
                            procInstId = 0;
                        }
                    }
                    catch (Exception ex)
                    {
                        if (procInstId != null && procInstId != 0)
                        {
                            try
                            {
                                //using (var k2AdminService = Resolver.GetInstance<IWorkflowK2AdminService>())
                                //{
                                //    k2AdminService.DeleteK2Process(procInstId.Value, true);
                                //    procInstId = null;
                                //}
                            }
                            catch
                            {
                            }
                        }
                        throw;
                    }
                    finally
                    {
                        //When the task is ready, release the semaphore. It is vital to ALWAYS release the semaphore when we are ready, or else we will end up with a Semaphore that is forever locked.
                        //This is why it is important to do the Release within a try...finally clause; program execution may crash or take a different path, this way you are guaranteed execution
                        semaphoreSlim.Release();
                    }
                
                //} while (saveFailed);
                    //}
                    return procInstId ?? 0;
                }
                else
                {
                    var failedValidate = new List<ValidationResult>();
                    if (validateStartProcessResult1 != null && validateStartProcessResult1.Any())
                    {
                        failedValidate.AddRange(validateStartProcessResult1);
                    }
                    if (validateStartProcessResult2 != null && validateStartProcessResult2.Any())
                    {
                        failedValidate.AddRange(validateStartProcessResult2);
                    }
                    throw failedValidate.ToAggregateException("Process start failed on these validation.");
                }
            

        }

        public async virtual Task<TCommentTable> Submit(string sn, string action, string comment, TWorkflowViewModel viewModel)
        {
            //using (var scope = TransactionUtils.CreateTransactionScope())
            //{
            if (String.IsNullOrEmpty(sn))
            {
                throw new ArgumentNullException("sn", Error.SNCannotBeEmpty);
            }
            if (String.IsNullOrEmpty(action))
            {
                throw new ArgumentNullException("action", Error.ActionCannotBeEmpty);
            }
            if (viewModel == null)
            {
                throw new ArgumentNullException("viewModel", Error.ViewModelNullWhileSubmitting);
            }

            var id = _workflowService.GetWorkflowTablePrimaryKey(sn);
            var model = await _workflowTableService.GetWorkflowTableAsync(id);

            if (model == null)
            {
                throw new Exception(Error.InvalidPrimaryKey);
            }

            var worklistItem = _workflowService.GetWorklistItem(sn);
            var actions = _workflowService.GetActions(worklistItem);
            if (!actions.Contains(action, StringComparer.InvariantCultureIgnoreCase))
            {
                throw new Exception(String.Format(Error.InvalidAction, action, sn));
            }
            var validateSubmitResult = ValidateSubmit(model, worklistItem, action, viewModel).ToListSafe();
            if (validateSubmitResult == null || !validateSubmitResult.Any())
            {
                if (AllowInProcessEdit(sn, action, viewModel, worklistItem))
                {
                    await SaveAsync(viewModel, action);
                }
                var commentTable = _commentService.CreateCommentSubmit(id,
                    worklistItem.ProcessInstance.ID,
                    worklistItem.ProcessInstance.Name,
                    worklistItem.ActivityInstanceDestination.Description,
                    sn,
                    action,
                    comment);

                await Session.SaveChangesAsync();
                // scope.Complete();
                _workflowService.Submit(sn, action);
                return commentTable;
            }
            else
            {
                throw validateSubmitResult.ToAggregateException();
            }
            // }
        }

        public abstract Task<TWorkflowViewModel> SaveAsync(TWorkflowViewModel viewModel, string action = null);

        public virtual IEnumerable<ValidationResult> ValidateSubmit(TWorkflowTable workflowTable, WorklistItem worklistItem, string action, TWorkflowViewModel viewModel)
        {
            if (String.IsNullOrEmpty(action))
            {
                yield return new ValidationResult("Please choose an action");
            }
        }

      

        public virtual bool AllowInProcessEdit(string sn, string action,
            TWorkflowViewModel viewModel, WorklistItem worklistItem)
        {
            return true;
            //if (worklistItem.ProcessInstance.Originator.Name.Equals(DTGO.Infrastructure.UserHelper.CurrentUsername(), StringComparison.InvariantCultureIgnoreCase))
            //{
            //    if (action.Equals("Resubmit", StringComparison.InvariantCultureIgnoreCase))
            //    {
            //        return true;
            //    }
            //}
            //return false;
        }

        public async virtual Task<TWorkflowViewModel> GetWorklist(UrlHelper url, string sn)
        {
            int id = 0;
            try
            {
                id = _workflowService.GetWorkflowTablePrimaryKey(sn);
            }
            catch (WorklistItemNotFoundException worklistItemNotFoundException)
            {
                var parentId = _commentService.GetParentIdFromSerialNumber(sn);
                if (parentId != null)
                {
                    //if (_workflowTableService.Exists(id))
                    {
                        worklistItemNotFoundException.WorkflowTableId = parentId;
                    }
                }
                else
                {
                    try
                    {
                        worklistItemNotFoundException.WorkflowTableId = _workflowTableService.GetWorkflowTableIdFromSN(sn);
                    }
                    catch (Exception)
                    {
                    }
                }
                throw;
            }
            if (id == 0)
            {
                throw new Exception(String.Format("You are not authorized to access data associated with this serial number. ({0})", sn));
            }
            var viewModel = await this.GetViewModel(url, id, sn);

            var worklistItem = _workflowService.GetWorklistItem(sn);
            viewModel.Actions = _workflowService.GetActions(worklistItem);
            viewModel.SN = sn;
            var dfException = _workflowService.GetDataFieldSafe<string>(sn, _definition.ExceptionDataFieldName);
            if (dfException != null)
            {
                var length = "System.AggregateException:".Length;
                var start = dfException.IndexOf("System.AggregateException:", StringComparison.OrdinalIgnoreCase);
                if (start >= 0)
                {
                    dfException = dfException.Substring(start + length + 1);
                    var end = dfException.IndexOf("---&gt", StringComparison.OrdinalIgnoreCase);
                    if (end >= 0)
                    {
                        dfException = dfException.Substring(0, end);
                    }
                }
                //TODO: Huh? Get error from K2 and put it here.
                //viewModel.DFException = dfException;
            }

            return viewModel;
        }

        public virtual IEnumerable<ValidationResult> ValidateStartProcess(TWorkflowTable workflowTable)
        {
            //if (workflowTable.StatusFlag > 0)
            //{
            //    yield return new ValidationResult(Resources.Error.ProcessAlreadyStarted);
            //}
            return Enumerable.Empty<ValidationResult>();
        }

        public virtual async Task<bool> AtomicUpdateBeforeStartProcess(TWorkflowTable workflowTable)
        {
            return await Task.FromResult(false);
        }

        protected string GetNewDocumentNumber(TWorkflowTable workflowTable)
        {
            if (string.IsNullOrEmpty(workflowTable.DocumentNumber))
            {
                var manualDocumentNumber = ManualDocumentNumber(workflowTable);
                if (string.IsNullOrEmpty(manualDocumentNumber))
                {
                    return _numberSequenceService.GetNextNumber(_definition.ProcessCode,
                            (workflowTable.CreatedDate ?? workflowTable.DocumentDate) ?? DateTime.UtcNow, _definition.Dimension1, _definition.Dimension2, _definition.Dimension3);
                }
                else
                {
                    return manualDocumentNumber;
                }
            }
            return workflowTable.DocumentNumber;
        }

        public virtual async Task<bool> UpdateBeforeStartProcessAsync(TWorkflowTable workflowTable)
        {
           
                if (string.IsNullOrEmpty(workflowTable.DocumentNumber))
                {
                    workflowTable.DocumentNumber = GetNewDocumentNumber(workflowTable);
                    return true;
                }
            
            return false;
        }

        public virtual bool UpdateBeforeStartProcess(TWorkflowTable workflowTable)
        {
            if (string.IsNullOrEmpty(workflowTable.DocumentNumber))
            {
                workflowTable.DocumentNumber = GetNewDocumentNumber(workflowTable);
                return true;
            }
            return false;
        }

        public virtual async Task<string> ManualDocumentNumberAsync(TWorkflowTable workflowTable)
        {
            return null;
        }
        public virtual string ManualDocumentNumber(TWorkflowTable workflowTable)
        {
            return null;
        }

        public virtual bool AllowStartK2Process()
        {
            return true;
        }

        public virtual async Task<bool> UpdateAfterStartProcess(TWorkflowTable workflowTable, int? procInstId)
        {
            if (procInstId != null)
            {
                workflowTable.ProcInstId = procInstId;
            }
            workflowTable.StatusFlag = (int)DocumentStatusFlag.Active;
            workflowTable.DocumentStatus = DocumentStatusFlag.Active.ToString();
            workflowTable.SubmittedDate = DateTime.UtcNow;

            return true;
        }

        public virtual WorklistItem GetWorklistItem(string sn)
        {
            return _workflowService.GetWorklistItem(sn);
        }
        #endregion

       
    }
}