﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TheMall.MPS.API.Mappers;
using TheMall.MPS.Models.Proposal;
using TheMall.MPS.Reports.Proposal;
using TheMall.MPS.Reports.Shared;
using TheMall.MPS.ViewModels.Proposal;

namespace TheMall.MPS.API.Facades
{
    public interface IReportProposalFacade
    {
        IEnumerable<ProposalDataSource> ReportData(MPS_ProposalTable proposalhModel);
        IEnumerable<ApproverDataSource> ApproversData(IEnumerable<MPS_ProposalApproval> proposalApprovalsModel);

        IEnumerable<ProposalLineDataSource> ProposalLineData(ProposalViewModel proposalLineViewModel);

        IEnumerable<ProposalDepositDataSource> ProposalDepositData(
            ProposalViewModel proposalViewModel);
    }
    public class ReportProposalFacade : IReportProposalFacade
    {
        private readonly IProposalMapper _proposalMapper;
        private readonly IReportProposalMapper _reportMapper;

        public ReportProposalFacade(
            IProposalMapper proposalMapper, 
            IReportProposalMapper reportMapper)
        {
            _proposalMapper = proposalMapper;
            _reportMapper = reportMapper;
        }

        public IEnumerable<ProposalDataSource> ReportData(MPS_ProposalTable proposalModel)
        {
            try
            {
                if (proposalModel != null)
                {
                    return _reportMapper.ReportDataMapper(proposalModel);
                }
                else
                {
                    return null;
                }
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
        }

        public IEnumerable<ApproverDataSource> ApproversData(IEnumerable<MPS_ProposalApproval> proposalApproverModel)
        {
            try
            {
                var approversDataList = _reportMapper.ApproversDataMapper(proposalApproverModel);
                return approversDataList;
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
        }

        public IEnumerable<ProposalLineDataSource> ProposalLineData(
            ProposalViewModel proposalViewModel)
        {
            try
            {
                var proposalLineDataList = _reportMapper.ProposalLineDataMapper(proposalViewModel);
                return proposalLineDataList;
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
        }

        public IEnumerable<ProposalDepositDataSource> ProposalDepositData(
            ProposalViewModel proposalViewModel)
        {
            try
            {
                var proposalDepositDataList = _reportMapper.ProposalDepositDataMapper(proposalViewModel);
                return proposalDepositDataList;
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
        }
    }
}