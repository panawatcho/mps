﻿using TheMall.MPS.API.Facades.Interfaces;
using TheMall.MPS.API.Services;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Models.Example;

namespace TheMall.MPS.API.Facades
{
    public interface IExampleLineFacade : IHaveAttachmentFacade<ExampleLineAttachment>
    {
    }

    //public class ExampleLineFacade : Abstracts.BaseUploadFacade<ExampleLineAttachment>, IExampleLineFacade
    //{
    //    public ExampleLineFacade(IMPSSession session,
    //        IAttachmentService<ExampleLineAttachment> attachmentService)
    //        : base(session, attachmentService)
    //    {
    //    }
    //}
}