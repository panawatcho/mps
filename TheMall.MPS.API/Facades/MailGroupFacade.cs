﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using TheMall.MPS.API.Facades.Abstracts;
using TheMall.MPS.API.Mappers;
using TheMall.MPS.API.Services;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Models.Master;
using TheMall.MPS.ViewModels.Master;

namespace TheMall.MPS.API.Facades
{
    public interface IMailGroupFacade : IBaseMasterFacade<MPS_M_MailGroup, MailGroupViewModel>
    {

    }
    public class MailGroupFacade : BaseMasterFacade<MPS_M_MailGroup, MailGroupViewModel>, IMailGroupFacade
    {
        private readonly IMPSSession _session;
        private readonly IMailGroupService _service;
        public MailGroupFacade(IMPSSession session,  
            IMailGroupService service) : base(session, service)
        {
            _session = session;
            _service = service;
        }

        public override async Task<MailGroupViewModel> Create(MailGroupViewModel viewModel)
        {
            try
            {
                var model = _service.Insert(viewModel.ToModel());
                await _session.SaveChangesAsync();
                return model.ToViewModel();
            }
            catch (Exception exception)
            {
                throw new Exception("Cannot insert duplicate code");
            }
        }

        public override async Task<MailGroupViewModel> Update(MailGroupViewModel viewModel)
        {
            try
            {
                var model = _service.Update(viewModel.ToModel());
                await _session.SaveChangesAsync();
                return model.ToViewModel();
            }
            catch (Exception exception)
            {
                throw new Exception("Cannot insert duplicate code");
            }
        }

        public override async Task<MailGroupViewModel> Delete(MailGroupViewModel viewModel)
        {
            try
            {
                _service.Delete(viewModel.ToModel());
                await _session.SaveChangesAsync();
                return viewModel;
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
        }
    }
}