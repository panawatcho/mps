﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TheMall.MPS.API.Mappers;
using TheMall.MPS.API.Services;
using TheMall.MPS.ViewModels.Task;

namespace TheMall.MPS.API.Facades
{
    public interface ITaskFacade
    {
        Task<IEnumerable<ProcInstStatusViewModel>> GetFolioAsync(int? procInstId, string folio);
    }

    public class TaskFacade : ITaskFacade
    {
        private readonly IK2DbService _k2DbService;

        public TaskFacade(IK2DbService k2DbService)
        {
            _k2DbService = k2DbService;
        }

        public async Task<IEnumerable<ProcInstStatusViewModel>> GetFolioAsync(int? procInstId, string folio)
        {
            IReadOnlyList<Models.K2.ProcInstStatus> result = null;
            //todo: ?????
            if (procInstId.HasValue && !string.IsNullOrEmpty(folio))
            {
                result = await _k2DbService.SearchFromFolioProcInstAsync(
                    new[]
                    {
                        procInstId.Value
                    },
                    new[]
                    {
                        folio
                    }
                );
            }
            else if (!procInstId.HasValue && !string.IsNullOrEmpty(folio))
            {
                result = await _k2DbService.SearchFromFolioAsync(new[]
                {
                    folio
                });
            }
            else if (procInstId.HasValue && string.IsNullOrEmpty(folio))
            {
                result = await _k2DbService.SearchFromProcInstAsync(new[]
                {
                    procInstId.Value
                });
            }
            else
            {
                result = await _k2DbService.SearchAsync();
            }

            return result.ToViewModel();
        }
    }
}