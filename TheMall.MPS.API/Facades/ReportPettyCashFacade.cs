﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TheMall.MPS.API.Mappers;
using TheMall.MPS.API.Services;
using TheMall.MPS.Models.PettyCash;
using TheMall.MPS.Reports.PettyCash;
using TheMall.MPS.Reports.Shared;

namespace TheMall.MPS.API.Facades
{
    public interface IReportPettyCashFacade
    {
        IEnumerable<PettyCashDataSource> ReportData(MPS_PettyCashTable pettyCashModel);
        IEnumerable<ApproverDataSource> ApproversData(IEnumerable<MPS_PettyCashApproval> pettyCashApproverModel);
    }
    public class ReportPettyCashFacade : IReportPettyCashFacade
    {
        private readonly IPettyCashService _pettyCashService;
        private readonly IPettyCashMapper _pettyCashMapper;
        private readonly IReportPettyCashMapper _reportMapper;
        private readonly IPettyCashApprovalService _pettyCashApproveService;

        public ReportPettyCashFacade(IPettyCashService pettyCashService, 
            IPettyCashMapper pettyCashMapper,
            IReportPettyCashMapper reportMapper, 
            IPettyCashApprovalService pettyCashApproveService)
        {
            _pettyCashService = pettyCashService;
            _pettyCashMapper = pettyCashMapper;
            _reportMapper = reportMapper;
            _pettyCashApproveService = pettyCashApproveService;
        }

        public IEnumerable<PettyCashDataSource> ReportData(MPS_PettyCashTable pettyCashModel)
        {
           // svar pettyCashModel = _pettyCashService.FirstOrDefault(m => m.DocumentNumber.Equals(documentNumber, StringComparison.InvariantCultureIgnoreCase));
            var pettyCashViewModel = _pettyCashMapper.ToViewModel(pettyCashModel);
            var list = _reportMapper.ReportDataMapper(pettyCashViewModel);
            return list;
        }

        public IEnumerable<ApproverDataSource> ApproversData(IEnumerable<MPS_PettyCashApproval> pettyCashApproverModel)
        {
            //var pettyCashModel = _pettyCashService.FirstOrDefault(m => m.DocumentNumber.Equals(documentNumber, StringComparison.InvariantCultureIgnoreCase));
           // var pettyCashApproverModel = _pettyCashApproveService.GetWhere(m => m.ParentId == pettyCashModel.Id);
            var approversDataList = _reportMapper.ApproversDataMapper(pettyCashApproverModel);
            return approversDataList;
        }
    }
}