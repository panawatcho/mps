﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using CrossingSoft.Framework.Infrastructure.Session;
using TheMall.MPS.API.Facades.Abstracts;
using TheMall.MPS.API.Mappers;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.API.Services;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Models.Master;
using TheMall.MPS.ViewModels.Master;

namespace TheMall.MPS.API.Facades
{
    public interface IUnitCodeFacade : IBaseMasterFacade<MPS_M_UnitCode, UnitCodeViewModel>
    {
        IEnumerable<UnitCodeViewModel> Search(string code = null, string name = null);

    }

    public class UnitCodeFacade : BaseMasterFacade<MPS_M_UnitCode, UnitCodeViewModel>, IUnitCodeFacade
    {
        private readonly IMPSSession _session;
        private readonly IUnitCodeService _service;

        public UnitCodeFacade(IMPSSession session, IUnitCodeService service) 
            : base(session, service)
        {
            _session = session;
            _service = service;
        }

        public override async Task<UnitCodeViewModel> Create(UnitCodeViewModel viewModel)
        {
            try
            {
                var model = _service.Insert(viewModel.ToModel());
                await _session.SaveChangesAsync();
                return  model.ToViewModel();
            }
            catch (Exception exception)
            {

                throw new Exception("Cannot insert duplicate code");
            }
        }

        public override async Task<UnitCodeViewModel> Update(UnitCodeViewModel viewModel)
        {
            var model = _service.Update(viewModel.ToModel());
            await _session.SaveChangesAsync();
            return model.ToViewModel();
        }

        public override async Task<UnitCodeViewModel> Delete(UnitCodeViewModel viewModel)
        {
            try
            {
                _service.Delete(viewModel.ToModel());
                await _session.SaveChangesAsync();
                return viewModel;
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
        }

        public  IEnumerable<UnitCodeViewModel> Search(string code = null, string name = null)
        {
            var model = _service.Search(code, name);
            return model.Result.ToViewModels();
        }

    }
}