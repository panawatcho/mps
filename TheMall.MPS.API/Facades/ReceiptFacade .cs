﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using CrossingSoft.Framework.Models.Enums;
using TheMall.MPS.API.Facades.Abstracts;
using TheMall.MPS.API.Mappers;
using TheMall.MPS.API.Services;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Models.Enums;
using TheMall.MPS.Models.Master;
using TheMall.MPS.Models.Receipt;
using TheMall.MPS.ViewModels.Master;
using TheMall.MPS.ViewModels.Receipt;

namespace TheMall.MPS.API.Facades
{
    public interface IReceiptFacade : IBaseMasterFacade<MPS_ReceiptTable, ReceiptViewModel>
    {
        IEnumerable<UnitCodeForEmployeeViewModel> Search(string empName = null);
        ReceiptViewModel GetViewModel(int id);
        ReceiptViewModel DeleteBy(int id, string username);
    }

    public class ReceiptFacade : BaseMasterFacade<MPS_ReceiptTable, ReceiptViewModel>, IReceiptFacade
    {
        // import
        private readonly IMPSSession _session;
        private readonly IReceiptService _receiptService;
        private readonly IReceiptLineService _receiptLineService;
        private readonly IReceiptMapper _receiptMapper;
        private readonly IReceiptLineMapper _receiptlinemapper;
        private readonly INumberSequenceService _numberSequenceService; // document number running
        private readonly IUnitCodeForEmployeeService _unitCodeForEmployeeService;
        private readonly IEmployeeTableService _employeeTableService;

        public ReceiptFacade(
            IMPSSession session,
            IReceiptService service,
            IReceiptLineService receiptLineService,
            IReceiptMapper receiptMapper,
            IReceiptLineMapper receiptlinemapper,
            INumberSequenceService numberSequenceService, 
            IUnitCodeForEmployeeFacade unitCodeForEmployeeFacade, 
            IUnitCodeForEmployeeService unitCodeForEmployeeService, 
            IEmployeeTableService employeeTableService)
            : base(session, service)
        {
            _receiptService = service;
            _receiptLineService = receiptLineService;
            _session = session;
            _receiptMapper = receiptMapper;
            _receiptlinemapper = receiptlinemapper;
            _numberSequenceService = numberSequenceService;
            _unitCodeForEmployeeService = unitCodeForEmployeeService;
            _employeeTableService = employeeTableService;
        }

        // create (viewmodel to model)
        // past mapper
        public override async Task<ReceiptViewModel> Create(ReceiptViewModel viewModel)
        {
            if (viewModel == null)
                throw new ArgumentNullException("viewModel = null");

            // receipt table
            var model = _receiptMapper.ToModel(viewModel);

            try
            {
                if (viewModel.Id <= 0)
                {
                    if (viewModel.StatusFlag == 9)
                    {
                        model.CompletedDate = DateTime.Now;
                        model.Status = DocumentStatus.Completed.ToString();
                        var document = ManualDocumentNumber(model);
                        model.DocumentNumber = document;
                    }
                    else
                    {
                        model.Status = DocumentStatus.Active.ToString();
                    }
                  
                    model = _receiptService.Insert(model);
                }
                else
                {
                    if (viewModel.StatusFlag == 9)
                    {
                        model.CompletedDate = DateTime.Now;
                        model.Status = DocumentStatus.Completed.ToString();
                        var document = ManualDocumentNumber(model);
                        model.DocumentNumber = document;
                    }
                    else
                    {
                        model.Status = DocumentStatus.Active.ToString();
                    }
                    model = _receiptService.Update(model);
                }


                // receipt line
                var receiptLines = _receiptlinemapper.ToModels(viewModel.ReceiptLines, model.ReceiptLine).ToListSafe();
                foreach (var line in receiptLines)
                {
                    if (line.Id <= 0 || line.ReceiptTableId <= 0)
                    {
                        if (model.Id <= 0)
                        {
                            line.ReceiptTable = model;
                        }
                        else
                        {
                            line.ReceiptTableId = model.Id;
                        }
                        _receiptLineService.Insert(line);
                    }
                    else
                    {
                        if (line.Deleted)
                        {
                            _receiptLineService.Delete(line);
                        }
                        else
                        {
                            _receiptLineService.Update(line, line.Id);
                        }
                    }
                }

                await _session.SaveChangesAsync();
                return GetViewModel(model.Id);
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
        }

        public override Task<ReceiptViewModel> Update(ReceiptViewModel viewModel)
        {
            throw new NotImplementedException();
        }

        public override Task<ReceiptViewModel> Delete(ReceiptViewModel viewModel)
        {
            throw new NotImplementedException();
        }

        public ReceiptViewModel DeleteBy(int id, string username)
        {
            try
            {
                if (id > 0)
                {
                    var model = _receiptService.FirstOrDefault(m => m.Id == id);
                    var employeeViewModel = _employeeTableService.FindByUsername(username).ToViewModel();

                    if (model != null)
                    {
                        model.Deleted = true;
                        model.DeletedDate = DateTime.Now;
                        model.DeletedBy = username;
                        model.DeletedByName = employeeViewModel.FullName;
                        model.StatusFlag = DocumentStatusFlagId.Deleted;
                        model.Status = DocumentStatus.Deleted.ToString();
                        _receiptService.Update(model, model.Id);
                        _session.SaveChanges();
                    }                     
                }
                return null;
            }
            catch (Exception ex)
            {
                throw new HttpException(ex.Message);
            }
        }


        // document number running
        public string ManualDocumentNumber(MPS_ReceiptTable receiptTable)
        {
            string documentNumber = receiptTable.DocumentNumber;
            string processCode = _numberSequenceService.GetProcessCode("RECEIPT"+DateTime.Now.Year.ToString());

            if (string.IsNullOrEmpty(documentNumber))
            {
                var nextdocumentNumber = _numberSequenceService.GetNextNumber(processCode,
                    receiptTable.CompletedDate, "", "", "");
                return nextdocumentNumber;
            }
            else
            {
                return documentNumber;
            }

        }

        // after save get model to view model
        public ReceiptViewModel GetViewModel(int id)
        {
            try
            {
                var model = _receiptService.Load(m => m.Id == id);
                if (model == null)
                    return null;

                var viewModel = _receiptMapper.ToViewModel(model);
                return viewModel;
            }
            catch (Exception ex)
            {
                throw new HttpException(ex.Message);
            }
        }

        public IEnumerable<UnitCodeForEmployeeViewModel> Search(string empName = null)
        {
            var model = _unitCodeForEmployeeService.GetUnitCodeCurrentUser();
            //foreach (var lineUnitCodeForEmployee in model)
            //{
            //    var unitCode = lineUnitCodeForEmployee.UnitCode;
            //}

            return model.ToViewModels();
        }


    }
}