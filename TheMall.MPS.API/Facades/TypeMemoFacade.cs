﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using CrossingSoft.Framework.Infrastructure.Session;
using TheMall.MPS.API.Facades.Abstracts;
using TheMall.MPS.API.Mappers;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.API.Services;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Models.Master;
using TheMall.MPS.ViewModels.Master;

namespace TheMall.MPS.API.Facades
{
    public interface ITypeMemoFacade : IBaseMasterFacade<MPS_M_TypeMemo, TypeMemoViewModel>
    {
       

    }

    public class TypeMemoFacade : BaseMasterFacade<MPS_M_TypeMemo, TypeMemoViewModel>, ITypeMemoFacade
    {
        private readonly IMPSSession _session;
        private readonly ITypeMemoService _service;

        public TypeMemoFacade(IMPSSession session, ITypeMemoService service) 
            : base(session, service)
        {
            _session = session;
            _service = service;
        }

        public override async Task<TypeMemoViewModel> Create(TypeMemoViewModel viewModel)
        {
            try
            {
                var model = _service.Insert(viewModel.ToModel());
                await _session.SaveChangesAsync();
                return  model.ToViewModel();
            }
            catch (Exception exception)
            {

                throw new Exception("Cannot insert duplicate code");
            }
        }

        public override async Task<TypeMemoViewModel> Update(TypeMemoViewModel viewModel)
        {
            var model = _service.Update(viewModel.ToModel());
            await _session.SaveChangesAsync();
            return model.ToViewModel();
        }

        public override async Task<TypeMemoViewModel> Delete(TypeMemoViewModel viewModel)
        {
            try
            {
                _service.Delete(viewModel.ToModel());
                await _session.SaveChangesAsync();
                return viewModel;
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
        }

      

    }
}