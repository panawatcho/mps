﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using CrossingSoft.Framework.Infrastructure.Session;
using TheMall.MPS.API.Facades.Abstracts;
using TheMall.MPS.API.Mappers;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.API.Services;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Models;
using TheMall.MPS.Models.Master;
using TheMall.MPS.ViewModels.Master;

namespace TheMall.MPS.API.Facades
{
    public interface ISharedBranchFacade : IBaseMasterFacade<MPS_M_SharedBranch, SharedBranchViewModel>
    {
        Task<IEnumerable<SharedBranchViewModel>> GetViewModel();
        SharedBranchViewModel GetByBranchCode(string branchCode = null);
    }
    public class SharedBranchFacade : BaseMasterFacade<MPS_M_SharedBranch, SharedBranchViewModel>, ISharedBranchFacade
       
    {
        private readonly IMPSSession _session;
        private readonly ISharedBranchService _service;
        private readonly ITotalSaleLastYearService _lastYearService;
     

        public SharedBranchFacade(IMPSSession session, 
            ISharedBranchService service,
            ITotalSaleLastYearService lastYearService) 
            : base(session, service)
        {
            _session = session;
            _service = service;
            _lastYearService = lastYearService;
        }

        public override async Task<SharedBranchViewModel> Create(SharedBranchViewModel viewModel)
        {
            try
            {
                var model = _service.Insert(viewModel.ToModel());
                await _session.SaveChangesAsync();
                return  model.ToViewModel();
            }
            catch (Exception exception)
            {

                throw new Exception("Cannot insert duplicate code");
            }
        }

        public override async Task<SharedBranchViewModel> Update(SharedBranchViewModel viewModel)
        {
            var model = _service.Update(viewModel.ToModel());
            await _session.SaveChangesAsync();
            return model.ToViewModel();
        }

        public override async Task<SharedBranchViewModel> Delete(SharedBranchViewModel viewModel)
        {
            try
            {
                _service.Delete(viewModel.ToModel());
                await _session.SaveChangesAsync();
                return viewModel;
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
        }

        public async Task<IEnumerable<SharedBranchViewModel>> GetViewModel()
        {
            var listViewModel = new List<SharedBranchViewModel>();
           // var model = _service.GetWhere(m => !m.Inactive);
            var model = _service.GetAll();
            var totalsaltmodel = _lastYearService.GetWhere(m => m.Year.Equals((DateTime.Now.Year-1).ToString()));
            var result = (from shb in model
                          join totalsal in totalsaltmodel on shb.BranchCode equals totalsal.BranchCode into ts
                          from totalsal in ts.DefaultIfEmpty()
                             select
                               new
                               {
                                   shb,
                                   totalsal = (totalsal ?? new MPS_M_TotalSaleLastYear())
                               }
                               ).ToListSafe();
            if (!result.Any()) return listViewModel;
            listViewModel.AddRange(
                result.Select(data => data.shb.ToViewModelPercent(data.totalsal)));
            return listViewModel;
        }

        public SharedBranchViewModel GetByBranchCode(string branchCode = null)
        {
           var viewModel = _service.FirstOrDefault(m => m.BranchCode.
               Equals(branchCode, StringComparison.InvariantCultureIgnoreCase)).ToViewModel();
            return viewModel;
        }

    }
}