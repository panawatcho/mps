﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Routing;
using TheMall.MPS.API.Facades.Interfaces;
using TheMall.MPS.API.Mappers;
using TheMall.MPS.API.Services;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Models.FileManager;
using TheMall.MPS.ViewModels;
using TheMall.MPS.ViewModels.FileManager;

namespace TheMall.MPS.API.Facades
{
    public interface IFileManagerFacade : IHaveAttachmentFacade<FileManagerFile>
    {
        IEnumerable<DirectoryViewModel> GetAllDirectories(string username);
        IEnumerable<DocumentViewModel> GetAllDocuments(int directoryId, string username);
        IEnumerable<AttachmentViewModel> GetAllFiles(int documentId, string username, UrlHelper urlHelper);
    }
    //public class FileManagerFacade : Abstracts.BaseUploadFacade<FileManagerFile>, IFileManagerFacade
    //{
    //    private readonly IAttachmentService<FileManagerFile> _attachmentService;
    //    private readonly IFileManagerDirectoryService _directoryService;
    //    private readonly IFileManagerDocumentService _documentService;

    //    public FileManagerFacade(IMPSSession session, 
    //        IAttachmentService<FileManagerFile> attachmentService,
    //        IFileManagerDirectoryService directoryService,
    //        IFileManagerDocumentService documentService)
    //        : base(session, attachmentService)
    //    {
    //        _attachmentService = attachmentService;
    //        _directoryService = directoryService;
    //        _documentService = documentService;
    //    }

    //    public IEnumerable<DirectoryViewModel> GetAllDirectories(string username)
    //    {
    //        var getAll = _directoryService.GetAll(username);

    //        var viewModel = new List<DirectoryViewModel>();
    //        viewModel.Add(new DirectoryViewModel()
    //        {
    //            Id = 0,
    //            Name = username,
    //            Subfolders = getAll.ToViewModel().OrderBy(m => m.Name) // We should not sort it here
    //        });

    //        return viewModel;
    //    }

    //    public IEnumerable<DocumentViewModel> GetAllDocuments(int directoryId, string username)
    //    {
    //        var getAll = _documentService.GetAll(_directoryService.Get(directoryId), username);

    //        return getAll.ToViewModel().OrderBy(m => m.Name);
    //    }

    //    public IEnumerable<AttachmentViewModel> GetAllFiles(int documentId, string username, UrlHelper urlHelper)
    //    {
    //        var getAll = _attachmentService.GetAllAttachments(documentId.ToString());

    //        return getAll.ToViewModels(urlHelper);
    //    }
    //}
}