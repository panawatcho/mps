﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Routing;
using CrossingSoft.Framework.Infrastructure.K2;
using Microsoft.AspNet.Identity;
using SourceCode.Workflow.Client;
using TheMall.MPS.API.Facades.Abstracts;
using TheMall.MPS.API.Facades.Interfaces;
using TheMall.MPS.API.Mappers;
using TheMall.MPS.API.Services;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Models.Enums;
using TheMall.MPS.Models.MemoIncome;
using TheMall.MPS.Models.Proposal;
using TheMall.MPS.Models.Workflow;
using TheMall.MPS.ViewModels;
using TheMall.MPS.ViewModels.MemoIncome;
using TheMall.MPS.ViewModels.Task;

namespace TheMall.MPS.API.Facades
{
    public interface IMemoIncomeFacade : IMpsWorkflowFacade<MPS_MemoIncomeTable, MPS_MemoIncomeAttachment, MPS_MemoIncomeComment, MemoIncomeViewModel>
    {
        void SetFinalApproverDeleted(int id);
        MemoIncomeViewModel DeleteBy(int id, string username);
        List<ProposalRefViewModel> GetProposalRef(string code, string name, string unitcode, string category);
        string GetEmailInformAll(int workflowTableId);
        List<MemoIncomeAccViewModel> GetMemoIncomeforAccount(string username);
    }

    public class MemoIncomeFacade : BaseMpsWorkflowFacade<MPS_MemoIncomeTable, MPS_MemoIncomeAttachment, MPS_MemoIncomeComment, MemoIncomeViewModel>, IMemoIncomeFacade
    {
        private readonly IMPSSession _session;
        private readonly IMemoIncomeService _workflowTableService;
        private readonly IMemoIncomeMapper _mapper;
        private readonly IMemoIncomeLineMapper _lineMapper;
        private readonly IMemoIncomeLineService _lineService;
        private readonly IMemoIncomeApprovalService _approvalService;
        private readonly IMemoIncomeApprovalMapper _approvalMapper;
        private readonly IUnitCodeForEmployeeService _unitCodeForEmployeeService;
        private readonly IEmployeeTableService _employeeTableService;
        private readonly IProposalService _proposalService;
        private readonly IProposalMapper _proposalMapper;
        private readonly IMemoIncomeInvoiceService _incomeInvoiceService;
        private readonly IMemoIncomeInvoiceMapper _incomeInvoiceMapper;
        private readonly IAccountTransService _accountTransService;
        private readonly IProposalIncomeOtherService _proposalIncomeOtherService;
        private readonly IDepositAccountTransService _depositAccountTransService;
        private readonly IProposalDepositLineService _proposalDepositLineService;
        private readonly IAccountingService _accountingService;
        private readonly IWorkspaceService _workSpaceService;
        private readonly ICommentService<MPS_ProposalComment> _proposalCommentService;

        public MemoIncomeFacade(IMPSSession session,
            IMemoIncomeService workflowTableService, 
            IAttachmentService<MPS_MemoIncomeAttachment, MPS_MemoIncomeTable> attachmentService, 
            ICommentService<MPS_MemoIncomeComment> commentService, 
            IWorkflowService<MPS_MemoIncomeTable> workflowService, 
            INumberSequenceService numberSequenceService, 
            IMemoIncomeProcess definition, //k2
            IMemoIncomeMapper mapper,
            IMemoIncomeLineMapper lineMapper,
            IMemoIncomeLineService lineService,      
            IMemoIncomeApprovalService approvalService,
            IMemoIncomeApprovalMapper approvalMapper,
            IUnitCodeForEmployeeService unitCodeForEmployeeService, 
            IEmployeeTableService employeeTableService, 
            IProposalService proposalService, 
            IProposalMapper proposalMapper, 
            IMemoIncomeInvoiceMapper incomeInvoiceMapper, 
            IMemoIncomeInvoiceService incomeInvoiceService, 
            IAccountTransService accountTransService, 
            IProposalIncomeOtherService proposalIncomeOtherService,
            IDepositAccountTransService depositAccountTransService,
            IProposalDepositLineService proposalDepositLineService,
            IAccountingService accountingService,
            IWorkspaceService workSpaceService,
            ICommentService<MPS_ProposalComment> proposalCommentService)
            : base(session, 
                workflowTableService, 
                attachmentService, 
                commentService, 
                workflowService, 
                numberSequenceService, 
                definition)
        {
            _session = session;
            _workflowTableService = workflowTableService;
            _mapper = mapper;
            _lineMapper = lineMapper;
            _lineService = lineService;
            _approvalService = approvalService;
            _approvalMapper = approvalMapper;
            _unitCodeForEmployeeService = unitCodeForEmployeeService;
            _employeeTableService = employeeTableService;
            _proposalService = proposalService;
            _proposalMapper = proposalMapper;
            _incomeInvoiceMapper = incomeInvoiceMapper;
            _incomeInvoiceService = incomeInvoiceService;
            _accountTransService = accountTransService;
            _proposalIncomeOtherService = proposalIncomeOtherService;
            _depositAccountTransService = depositAccountTransService;
            _proposalDepositLineService = proposalDepositLineService;
            _accountingService = accountingService;
            _workSpaceService = workSpaceService;
            _proposalCommentService = proposalCommentService;
        }

        public override async Task<MemoIncomeViewModel> GetViewModel(UrlHelper url, int id, string sn = null)
        {
            try
            {
                var model = await _workflowTableService.GetAsync(id);
                var viewModel =
                    await this.InitWorkflowViewModel(url, id, sn, _mapper.ToViewModel(model));

                return viewModel;
            }
            catch (Exception ex)
            {
                throw new HttpException(ex.Message);
            }  
        }

        #region SaveAsync
        public override async Task<MemoIncomeViewModel> SaveAsync(MemoIncomeViewModel viewModel, string action = null)
        {
            if (viewModel == null)
                throw new ArgumentNullException("viewModel");

            var model = _mapper.ToModel(viewModel);
            try
            {
                if (viewModel.Id <= 0)
                {
                    model = _workflowTableService.Insert(model);
                }
                else
                {
                    if (model.Category == "otherincome")
                    {
                        model.DepositLineId = null;
                    }
                    else if (model.Category == "depositproposal")
                    {
                        model.OtherIncomeRefId = null;
                        model.Branch = null;
                    } 
                    model = _workflowTableService.Update(model, model.Id);
                }

                // line
                var memoLines = _lineMapper.ToModels(viewModel.MemoIncomeLines, model.MemoIncomeLine).ToListSafe();
                foreach (var line in memoLines)
                {
                    if (line.Id <= 0 || line.ParentId <= 0)
                    {
                        if (model.Id <= 0)
                        {
                            line.MemoIncomeTable = model;
                        }
                        else
                        {
                            line.ParentId = model.Id;
                        }
                        _lineService.Insert(line);
                    }
                    else
                    {
                        if (line.Deleted)
                        {
                            _lineService.Delete(line);
                        }
                        else
                        {
                            _lineService.Update(line, line.Id);
                        }
                    }
                }

                // invoice
                if (viewModel.MemoIncomeInvoice != null)
                {
                    var memoIncomeInvoice = _incomeInvoiceMapper.ToModels(viewModel.MemoIncomeInvoice, model.MemoIncomeInvoice).ToListSafe();
                    foreach (var line in memoIncomeInvoice)
                    {
                        if (line.Id <= 0 || line.ParentId <= 0)
                        {
                            if (model.Id <= 0)
                            {
                                line.MemoIncomeTable = model;
                            }
                            else
                            {
                                line.ParentId = model.Id;
                            }
                            _incomeInvoiceService.Insert(line);
                        }
                        else
                        {
                            if (line.Deleted)
                            {
                                _incomeInvoiceService.Delete(line);
                            }
                            else
                            {
                                _incomeInvoiceService.Update(line, line.Id);
                            }
                        }
                    }
                }
                
                // approve
                var approval = _approvalMapper.ToModels(viewModel.MemoIncomeApproval, model.MemoIncomeApproval).ToListSafe();
                foreach (var approve in approval)
                {
                    if (approve.Id <= 0 || approve.ParentId <= 0)
                    {
                        if (model.Id <= 0)
                        {
                            approve.MemoIncomeTable = model;
                        }
                        else
                        {
                            approve.ParentId = model.Id;
                        }
                        _approvalService.Insert(approve);
                    }
                    else
                    {
                        if (approve.Deleted)
                        {
                            _approvalService.Delete(approve);
                        }
                        else
                        {
                           
                            _approvalService.Update(approve, approve.Id);
                        }
                    }
                }
                await Session.SaveChangesAsync();
                return _mapper.ToViewModel(model);
            }
            catch (Exception ex)
            {
                throw new HttpException(ex.Message);
            }
        }
        #endregion

        public override IEnumerable<ValidationResult> ValidateStartProcess(MPS_MemoIncomeTable workflowTable)
        {
            if (string.IsNullOrEmpty(workflowTable.AccountingBranch))
            {
                yield return new ValidationResult("Please select accounting!!!");
            }
        }

        // check validate ก่อนกดปุ่ม complete
        public override IEnumerable<ValidationResult> ValidateSubmit(MPS_MemoIncomeTable workflowTable, WorklistItem worklistItem, string action,
            MemoIncomeViewModel viewModel)
        {
            if (action.ToUpper().Equals("COMPLETE", StringComparison.CurrentCultureIgnoreCase))
            {
                if (!viewModel.MemoIncomeInvoice.Any()  || viewModel.MemoIncomeInvoice.Any(m => !m.Deleted && (string.IsNullOrEmpty(m.InvoiceNo) || string.IsNullOrWhiteSpace(m.InvoiceNo))))
                {
                    yield return new ValidationResult("Please input invoice number!!!");

                }
            }
        }

        // จับ action flow
        public override async Task<MPS_MemoIncomeComment> Submit(string sn, string action, string comment, MemoIncomeViewModel viewModel)
        {
            var result = await base.Submit(sn, action, comment, viewModel);
            if (result != null)
            {
                if (result.Action.ToUpper().Equals("RESUBMIT", StringComparison.CurrentCultureIgnoreCase))
                {

                    var approval = _approvalService.GetWhere(m => m.ParentId == viewModel.Id && !m.Deleted);
                    foreach (var a in approval)
                    {
                        a.ApproveStatus = false;
                        _approvalService.Update(a, a.Id);
                    }
                    await Session.SaveChangesAsync();
                }
                else if (result.Action.ToUpper().Equals("ONPROCESS", StringComparison.CurrentCultureIgnoreCase))
                {
                    var account = _workflowTableService.Find(m => m.Id == result.ParentId);
                    account.AccountingStatus = DocumentStatusFlag.OnProcess.ToString();
                    account.AccOnProcessUsername = HttpContext.Current.User.Identity.GetUserName();
                    _workflowTableService.Update(account, account.Id);
                    Session.SaveChanges();
                }
                else if (result.Action.ToUpper().Equals("COMPLETE", StringComparison.CurrentCultureIgnoreCase))
                {
                    var account = _workflowTableService.Find(m => m.Id == result.ParentId);
                    account.AccountingStatus = DocumentStatusFlag.Completed.ToString();
                    // get username
                    account.AccCompleteUsername = HttpContext.Current.User.Identity.GetUserName();
                    _workflowTableService.Update(account, account.Id);
                    Session.SaveChanges();

                    if (result.MpsMemoIncomeTable.OtherIncomeRefId != null && viewModel.MemoIncomeInvoice != null)
                    {
                        // update invoice ลง MPS_MemoIncomeInvoice
                        var invoince = _incomeInvoiceService.GetWhere(m => m.ParentId == result.ParentId).ToListSafe();

                        UpdateAccountTransToProposal(invoince);

                        var incomeOther = _proposalIncomeOtherService.Find(m => m.Id == result.MpsMemoIncomeTable.OtherIncomeRefId);
                        if (incomeOther != null)
                        {
                            //incomeOther.Actual = viewModel.MemoIncomeInvoice.Where(m => !m.Deleted).Sum(m => m.Actual);
                            var actualAccount = _accountTransService.GetWhere(m => m.IncomeOtherId == incomeOther.Id).ToListSafe();
                            incomeOther.Actual = actualAccount.Sum(m => m.Actual);
                            incomeOther = _proposalIncomeOtherService.Update(incomeOther, incomeOther.Id);
                        }
                        
                        Session.SaveChanges();
                    }
                    else if (result.MpsMemoIncomeTable.DepositLineId != null && viewModel.MemoIncomeInvoice != null)
                    {
                        var invoinceDeposit = _incomeInvoiceService.GetWhere(m => m.ParentId == result.ParentId).ToListSafe();

                        UpdateDepositAccountTransToProposal(invoinceDeposit);

                        var DepositLine = _proposalDepositLineService.Find(m => m.Id == result.MpsMemoIncomeTable.DepositLineId);
                        //DepositLine.Actual = viewModel.MemoIncomeInvoice.Where(m => !m.Deleted).Sum(m => m.Actual);
                        //DepositLine = _proposalDepositLineService.Update(DepositLine, DepositLine.Id);
                        if (DepositLine != null)
                        {
                            var depositActual = _depositAccountTransService.GetWhere(m => m.DepositLineId == DepositLine.Id).ToListSafe();
                            DepositLine.Actual = depositActual.Sum(m => m.Actual);
                            DepositLine = _proposalDepositLineService.Update(DepositLine, DepositLine.Id);
                        }
                        
                        Session.SaveChanges();
                    }
                } else {
                    var activity = result.Activity.Replace("Approval", "").Replace("-", "").Replace(" ", "");
                    var modelApprove1 = _approvalService.GetWhere(m => activity.Equals(m.ApproverLevel)
                    && m.ParentId == result.ParentId).ToList();
                    if (modelApprove1.Any())
                    {
                        var modelApprove = modelApprove1.LastOrDefault(m => m.ApproverUserName.ToLower().Equals(result.CreatedBy.ToLower()));
                        if (modelApprove != null)
                        {
                            modelApprove.ApproveStatus = true;
                            _approvalService.Update(modelApprove, modelApprove.Id);
                            Session.SaveChanges();
                        }
                    }

                }
            }
            return result;
        }

        public List<MPS_AccountTrans> MapperInvoiceToAccTrans(IEnumerable<MemoIncomeInvoiceViewModel> invoice )
        {
            var listAcc = new List<MPS_AccountTrans>();

            if (invoice != null)
            {
                //var IncomeOtherId = invoice.Last().MemoIncomeTable.OtherIncomeRefId;
                foreach (var line in invoice)
                {
                    var AccTrans = new MPS_AccountTrans();
                    AccTrans.Actual = line.Actual;
                    AccTrans.Source = line.InvoiceNo;
                    AccTrans.ActualDate = line.InvoiceDate;
                    AccTrans.Remark = line.Remark;

                    listAcc.Add(AccTrans);
                }
            }
            return listAcc;
        }

        public bool UpdateAccountTransToProposal(List<MPS_MemoIncomeInvoice> invoice)
        {
            var result = false;
            if (invoice != null)
            {
                var IncomeOtherId = invoice.Last().MemoIncomeTable.OtherIncomeRefId;
                foreach (var line in invoice)
                {
                    var AccTrans = new MPS_AccountTrans();

                    AccTrans.Actual = line.Actual;
                    AccTrans.Source = line.InvoiceNo;
                    AccTrans.ActualDate = line.InvoiceDate;
                    AccTrans.Remark = line.Remark;
                    AccTrans.FlagMemoInvoice = true;
                    AccTrans.IncomeOtherId = (int) IncomeOtherId;

                    _accountTransService.Insert(AccTrans);
                    Session.SaveChanges();
                }
                result = true;
            }
            return result;
        }

        public bool UpdateDepositAccountTransToProposal(List<MPS_MemoIncomeInvoice> invoice)
        {
            var result = false;
            if (invoice != null)
            {
                var DepositLineId = invoice.Last().MemoIncomeTable.DepositLineId;
                foreach (var line in invoice)
                {
                    var DepositAccTrans = new MPS_DepositAccountTrans();

                    DepositAccTrans.Actual = line.Actual;
                    DepositAccTrans.Remark = line.Remark;
                    DepositAccTrans.DepositLineId = (int)DepositLineId;
                    DepositAccTrans.InvoiceNo = line.InvoiceNo;
                    DepositAccTrans.MemoIncomeId = line.ParentId;

                    _depositAccountTransService.Insert(DepositAccTrans);
                    Session.SaveChanges();
                }
                result = true;
            }
            return result;
        }

        public void SetFinalApproverDeleted(int id)
        {
            try
            {
                var finalApprovers =
                    _approvalService.GetWhere(m => m.ParentId == id && m.ApproverLevel == "อนุมัติขั้นสุดท้าย");
                foreach (var finalApprover in finalApprovers)
                {
                    _approvalService.Delete(finalApprover);
                }
                Session.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new HttpException(ex.Message);
            }
        }

        public string GetEmailInformAll(int workflowTableId)
        {
            string email = string.Empty;
            var memoincomeModel = _workflowTableService.Find(m => m.Id == workflowTableId);
            if (memoincomeModel != null)
            {
                email += string.IsNullOrEmpty(email) ? memoincomeModel.CCEmail : string.Format(";{0}", memoincomeModel.CCEmail).TrimEnd(';');
                email += string.IsNullOrEmpty(email) ? memoincomeModel.InformEmail : string.Format(";{0}", memoincomeModel.InformEmail).TrimEnd(';');
                email += string.IsNullOrEmpty(email) ? memoincomeModel.AccountingEmail : string.Format(";{0}", memoincomeModel.AccountingEmail).TrimEnd(';');

                if (memoincomeModel.MemoIncomeApproval != null)
                {
                    foreach (var userapprove in memoincomeModel.MemoIncomeApproval)
                    {
                        if (!string.IsNullOrEmpty(userapprove.ApproverEmail))
                        {
                            email += string.IsNullOrEmpty(email)
                                   ? userapprove.ApproverEmail
                                   : string.Format(";{0}", userapprove.ApproverEmail).TrimEnd(';');
                        }
                        else
                        {
                            var employee = _employeeTableService.FindByUsername(userapprove.ApproverUserName);
                            if (employee != null && !string.IsNullOrEmpty(employee.Email))
                            {
                                email += string.IsNullOrEmpty(email)
                                    ? employee.Email
                                    : string.Format(";{0}", employee.Email).TrimEnd(';');
                            }
                        }
                    }
                }
            }
            if (!string.IsNullOrEmpty(email))
            {
                string[] listEmail = email.TrimEnd(';').Split(';');
                string[] newlistEmail = listEmail.Distinct().ToArray();
                email = newlistEmail.Aggregate(string.Empty, (current, mail) => current + (string.IsNullOrEmpty(current) ? mail : string.Format(";{0}", mail)));
            }

            return email;
        }

        public MemoIncomeViewModel DeleteBy(int id, string username)
        {
            try
            {
                if (id > 0)
                {
                    var model = _workflowTableService.FirstOrDefault(m => m.Id == id);
                    var employeeViewModel = _employeeTableService.FindByUsername(username).ToViewModel();
                    if (model != null && employeeViewModel != null)
                    {
                        if (model.StatusFlag == 0)
                        {
                            var modelDelete = _mapper.DeleteBy(model, employeeViewModel);
                            if (modelDelete != null)
                            {
                                modelDelete = _workflowTableService.Update(modelDelete);
                                var value = Session.SaveChanges();
                                if (value > 0)
                                {
                                    var viewModel = _mapper.ToViewModel(modelDelete);
                                    return viewModel;
                                }
                            }
                        }
                        //else if (model.StatusFlag == 9)
                        //{
                        //    model.StatusFlag = 5;
                        //    model.CancelledDate = DateTime.Now.ToLocalTime();

                        //    model.Status = DocumentStatusFlag.Cancelled.ToString();
                        //    model.DocumentStatus = DocumentStatusFlag.Cancelled.ToString();
                        //    var cancelModel = _workflowTableService.Update(model, model.Id);
                        //    var result = _budgetProposalTransFacade.CancelBudgetPropTrans(model.Id, "memo");
                        //    if (result)
                        //    {
                        //        Session.SaveChanges();
                        //    }
                        //    else
                        //    {
                        //        throw new Exception("ไม่สามารถลบรายการได้ กรุณาติดต่อผู้ดูแลระบบ");
                        //    }
                        //    return _mapper.ToViewModel(cancelModel);
                        //}
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                throw new HttpException(ex.Message);
            }
        }

        // ค้นหา ไม่ได้กรองวันที่
        public List<ProposalRefViewModel> GetProposalRef(string code, string name, string unitcode, string category)
        {
            var proposalViewModel = new List<ProposalRefViewModel>();
           // var propoNoActual = new List<MPS_ProposalTable>();

            try
            {
                // all proposal
                var proposalModel = _proposalService.SearchDocument(code, name).ToListSafe();

                // กรอง
                if (!string.IsNullOrEmpty(category) && proposalModel != null)
                {
                    if (category.Equals("depositproposal"))
                    {
                        proposalModel = proposalModel.Where(m => m.ProposalTypeCode.Equals(ProposalType.ProposalTypeCode.Deposit) && m.StatusFlag == DocumentStatusFlagId.Completed && !m.Deleted).ToListSafe();

                        //foreach (var dataProp in proposalModel) {
                        //    var lastComment = _proposalCommentService.LastOrDefault(r => r.ParentId == dataProp.Id);
                        //    if (lastComment.Action != "PostActual") {
                        //        propoNoActual.Add(dataProp);
                        //    }                           
                        //}

                        var newViewmodel = new List<ProposalRefViewModel>();

                        if (proposalModel.Any())
                        {
                            // loop prop
                            newViewmodel.AddRange(from view in proposalModel
                                                  where view.UnitCode == unitcode && !view.Deleted && !view.CloseFlag 
                                                  select _proposalMapper.ToProposalRefViewModel(view));

                            proposalViewModel.AddRange(from mm in newViewmodel
                                                       let 
                                                       line = mm.DepositLine.Where(m => m.ProcessStatus != "completed" && !m.Deleted)
                                                       where line.Any()
                                                       select mm);
                        }
                    }
                    else if (category.Equals("otherincome"))
                    {
                        proposalModel = proposalModel.Where(m => !(m.ProposalTypeCode.ToUpper() == ProposalType.ProposalTypeCode.Deposit || m.ProposalTypeCode.ToUpper() == ProposalType.ProposalTypeCode.DepositInternal) && m.StatusFlag == DocumentStatusFlagId.Completed && !m.Deleted && m.IncomeOther.Any(r => !r.Deleted && r.PostActual != true)).ToListSafe();
                        var newViewmodel = new List<ProposalRefViewModel>();
                        if (proposalModel.Any())
                        {
                            newViewmodel.AddRange(from view in proposalModel
                                                  where view.UnitCode == unitcode && !view.Deleted && !view.CloseFlag
                                                  select _proposalMapper.ToProposalRefViewModel(view));

                            proposalViewModel.AddRange(from mm in newViewmodel
                                                       let
                                                       line = mm.IncomeOther.Where(m => !m.PostActual && !m.Deleted)
                                                       where line.Any()
                                                       select mm);
                        }
                    }
                }
                return proposalViewModel;
            }
            catch (Exception ex)
            {
                throw new HttpException(ex.Message);
            }
        }

        //// document number running
        //public string ManualDocumentNumber(MPS_MemoIncomeTable memoincomeTable)
        //{
        //    string documentNumber = memoincomeTable.DocumentNumber;
        //    string processCode = _numberSequenceService.GetProcessCode("MEMOINCOME"+DateTime.Now.Year.ToString());

        //    if (string.IsNullOrEmpty(documentNumber))
        //    {
        //        var nextdocumentNumber = _numberSequenceService.GetNextNumber(processCode,
        //            memoincomeTable.CompletedDate, "", "", "");
        //        return nextdocumentNumber;
        //    }
        //    else
        //    {
        //        return documentNumber;
        //    }
        //}

        public List<MemoIncomeAccViewModel> GetMemoIncomeforAccount(string username)
        {
            var viewModel = new List<MemoIncomeAccViewModel>();
            var accModel = new List<MPS_MemoIncomeTable>();
            var account = _accountingService.GetWhere(r => r.Username.Equals(username, StringComparison.InvariantCultureIgnoreCase) && !r.InActive);
            var model = _workflowTableService.GetWhere(m => !m.Deleted && m.Status == DocumentStatusFlag.Completed.ToString() && m.AccountingBranch != null);

            foreach (var dataAcc in account)
            {
                foreach (var dataModels in model)
                {
                    if (dataAcc.BranchCode == dataModels.AccountingBranch)
                    {
                        accModel.Add(dataModels);
                    }
                }
            }

            viewModel = _mapper.ToAccViewModel(accModel).ToList();   
            return viewModel;
        }
    }
}
