﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using TheMall.MPS.API.Facades.Abstracts;
using TheMall.MPS.API.Mappers;
using TheMall.MPS.API.Services;
using TheMall.MPS.API.Services;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Infrastructure.Identity;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.ViewModels.AspNet;
using TheMall.MPS.ViewModels.Master;

namespace TheMall.MPS.API.Facades
{
    public interface IAspNetRolesFacade : IBaseMasterFacade<ApplicationRole, AspNetRolesViewModel>
    {
        AspNetRolesViewModel SearchById(string id = null);
        IEnumerable<AspNetRolesViewModel> GetAll();

    }
    public class AspNetRolesFacade : BaseMasterFacade<ApplicationRole, AspNetRolesViewModel>, IAspNetRolesFacade
    {
        private readonly IAspNetRolesService _service;
        private readonly IMPSSession _session;
        public AspNetRolesFacade(IMPSSession session,
           IAspNetRolesService service)
            : base(session, service)
        {
            _service = service;
            _session = session;
        }

        public override async Task<AspNetRolesViewModel> Create(AspNetRolesViewModel viewModel)
        {
            try
            {
                var model = _service.Insert(viewModel.ToModel());
                await _session.SaveChangesAsync();
                return model.ToViewModel();
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
        }

        public override async Task<AspNetRolesViewModel> Update(AspNetRolesViewModel viewModel)
        {
            try
            {
                var model = _service.Update(viewModel.ToModel());
                await _session.SaveChangesAsync();
                return model.ToViewModel();
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
        }

        public override async Task<AspNetRolesViewModel> Delete(AspNetRolesViewModel viewModel)
        {
            try
            {
                _service.Delete(viewModel.ToModel());
                await _session.SaveChangesAsync();
                return viewModel;
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
        }

        public AspNetRolesViewModel SearchById(string id = null)
        {
            try
            {
                var model = _service.FirstOrDefault(m => m.Id.Equals(id, StringComparison.InvariantCultureIgnoreCase));
                if (model != null)
                {
                    var viewModel = model.ToViewModel();
                    return viewModel;
                }
                return null;
            }
            catch (Exception exception)
            {

                throw new Exception(exception.Message);
            }
        }

        public IEnumerable<AspNetRolesViewModel> GetAll()
        {
            try
            {
                var models = _service.GetAll();
                if (models != null)
                {
                    var viewModels = models.ToViewModels();
                    return viewModels;
                }
                return null;
            }
            catch (Exception exception)
            {

                throw new Exception(exception.Message);
            }
        }
    }
}