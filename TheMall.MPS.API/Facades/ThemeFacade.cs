﻿using System;
using System.Threading.Tasks;
using TheMall.MPS.API.Facades.Abstracts;
using TheMall.MPS.API.Mappers;
using TheMall.MPS.API.Services;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Models.Master;
using TheMall.MPS.ViewModels.Master;

namespace TheMall.MPS.API.Facades
{
    public interface IThemeFacade : IBaseMasterFacade<MPS_M_Themes, ThemeViewModel>
    {

    }
    public class ThemeFacade : BaseMasterFacade<MPS_M_Themes, ThemeViewModel>, IThemeFacade  
    {
        private readonly IMPSSession _session;
        private readonly IThemeService _themeService;

        public ThemeFacade(IMPSSession session,
            IThemeService themeService) 
            : base(session, 
                  themeService)
        {
            _session = session;
            _themeService = themeService;
        }

        public override async Task<ThemeViewModel> Create(ThemeViewModel viewModel)
        {
            try
            {
                var model = _themeService.Insert(viewModel.ToModel());
                await _session.SaveChangesAsync();
                return  model.ToViewModel();
            }
            catch (Exception exception)
            {
                throw new Exception("Cannot insert duplicate code");
            }
        }

        public override async Task<ThemeViewModel> Update(ThemeViewModel viewModel)
        {
            var model = _themeService.Update(viewModel.ToModel());
            await _session.SaveChangesAsync();
            return model.ToViewModel();
        }

        public override async Task<ThemeViewModel> Delete(ThemeViewModel viewModel)
        {
            try
            {
                _themeService.Delete(viewModel.ToModel());
                await _session.SaveChangesAsync();
                return viewModel;
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
        }

    }
}