﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Threading.Tasks;
using TheMall.MPS.API.Mappers;
using TheMall.MPS.API.Services;
using TheMall.MPS.ViewModels.Menu;

namespace TheMall.MPS.API.Facades
{
    public interface IMenuFacade
    {
        Task<IEnumerable<MenuItemViewModel>> GetAuthorized(IIdentity user);
    }
    public class MenuFacade : IMenuFacade
    {
        private readonly IMenuService _service;

        public MenuFacade(
            IMenuService service)
        {
            _service = service;
        }

        public async Task<IEnumerable<MenuItemViewModel>> GetAuthorized(IIdentity user)
        {
            var roleClaims = Infrastructure.UserHelper.GetRolesId(user);
            var menuItems = await _service.GetAuthorizedAsync(user.Name, roleClaims.Select(m => m.Value));
            return menuItems.OrderBy(m=>m.Order).ToReadOnlyListSafe().ToViewModel();
        }
    }
}