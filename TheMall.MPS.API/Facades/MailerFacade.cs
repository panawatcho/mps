﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using CrossingSoft.Framework.Infrastructure.K2;
using CrossingSoft.Framework.Models.K2;
using TheMall.MPS.API.Services;


namespace TheMall.MPS.API.Facades
{
    public interface IMailerFacade
    {
        string GetNotificationSubject(
            string mailContentId,
            int procInstId,
            //string originatorUser,
            int actiInstDestId,
            string items = null);

        string GetNotificationBody(
            string mailContentId,
            int procInstId,
            //string originatorUser,
            string worklistUrl,
            string items = null);

        string GetEscalationSubject(
            string mailContentId,
            int procInstId,
            //string originatorUser, 
            string detinationUser,
            int actiInstId,
            string items = null);
        string GetEscalationBody(
            string mailContentId,
            int procInstId,
            //string originatorUser,
            string detinationUser,
            int actiInstId,
            string worklistUrl,
            string items = null);

        string GetMailEventSubject(
            string mailContentId,
            int procInstId,
            //string originatorUser,
            int actiInstId,
            string items = null);
        string GetMailEventBody(
            string mailContentId,
            int procInstId,
            //string originatorUser,
            string detinationUser,
            int actiInstId,
            string items = null);
    }

    public class MailerFacade : IMailerFacade
    {
        private readonly IMailTemplateService _mailTemplateService;
        private readonly IMailContentService _mailContentService;
        private readonly IWorkflowProcessAdminRepository _processAdminRepository;
       

        private readonly IK2DbService _k2DbService;
        private readonly IUMUserService _umUserService;

      
        private const string MAILCONTENT = "{MAILCONTENT}";

        public MailerFacade(
            IMailTemplateService mailTemplateService,
            IMailContentService mailContentService,
            IWorkflowProcessAdminRepository processAdminRepository,
           
            IK2DbService k2DbService, IUMUserService umUserService)
        {
            _mailTemplateService = mailTemplateService;
            _mailContentService = mailContentService;
            _processAdminRepository = processAdminRepository;
          
            _k2DbService = k2DbService;
            _umUserService = umUserService;
        }

        private string Execute(
            string template,
            int procInstId,
            //string originatorUser,
            string destinationUser,
            string worklistUrl,
            string mailContentId = null,
            bool isGetSubject = false,
            string items = null,
            int? actiInstId = null,
            int? activityInstanceDestinationId = null)
        {
            //if (!string.IsNullOrEmpty(originatorUser) && !originatorUser.Contains(":"))
            //{
            //    originatorUser = AppSettings.Instance.SecurityLabelName() + ":" + originatorUser;
            //}
            string originatorUser = null;
            var coreContextItems = new Dictionary<string, string>();
            var smoHasError = false;
            //SourceCode.Workflow.Management.EventActions eventButtonActions = null;
            SourceCode.Workflow.Client.ProcessInstance procinst = null;
            //PRUKSA.Models.K2.ProcInstCustom procinst = null;
            string sn = null;
            //int? activityInstanceDestinationId = null;
            var worklistIsUrl = false;

            var log = new List<string>();
            var error = new List<string>();
            var trace = new List<string>();

            log.Add("Log> --Start Mail--");
            log.Add("Log> Start at " + DateTime.Now);
            trace.Add("Trace> --Start Mail--");
            trace.Add("Trace> Start at " + DateTime.Now);

            if (!string.IsNullOrEmpty(worklistUrl))
            {
                var indexSn = worklistUrl.IndexOf("SN", System.StringComparison.OrdinalIgnoreCase);
                if (indexSn >= 0)
                {
                    // Worklist is URL
                    worklistIsUrl = true;
                    coreContextItems.Add("WorklistItemInstance.WorklistItem", worklistUrl);
                    coreContextItems.Add("WorklistItemURL", worklistUrl);

                    //now get the Serial Number from the worklist item
                    sn = worklistUrl.Substring(worklistUrl.IndexOf("SN", System.StringComparison.OrdinalIgnoreCase));

                    //if it contains an & then remove it.
                    if (sn.Contains("&"))
                    {
                        sn = sn.Substring(0, sn.IndexOf("&", System.StringComparison.OrdinalIgnoreCase));
                    }

                    //remove SN= and add to context
                    sn = sn.Replace("SN=", string.Empty);
                    coreContextItems.Add("SN", sn);
                    coreContextItems.Add("SN=", "SN=" + sn);

                    if (activityInstanceDestinationId == null)
                    {
                        activityInstanceDestinationId = int.Parse(sn.Substring(sn.IndexOf('_') + 1));
                    }
                }
                else
                {
                    // Worklist is Not URL, presumably was composed manually.
                    sn = worklistUrl;
                    coreContextItems.Add("SN", sn);
                    coreContextItems.Add("SN=", "SN=" + sn);

                    try
                    {
                        if (activityInstanceDestinationId == null)
                        {
                            var i = sn.IndexOf('_');
                            if (i >= 0)
                            {
                                activityInstanceDestinationId = int.Parse(sn.Substring(i + 1));
                            }
                        }
                        //var worklistItem = _processAdminRepository.GetWorkListItem(sn, destinationUser, false);
                        var worklistItem = _k2DbService.GetWorkListItem(sn);
                        //var worklistItem = _processAdminRepository.GetWorkListItem(sn, false);
                        if (worklistItem != null)
                        {
                            coreContextItems.Add("WorklistItemInstance.WorklistItem", worklistItem.Data);
                            coreContextItems.Add("WorklistItemURL", worklistItem.Data);
                        }
                    }
                    catch (Exception exception)
                    {
                    }
                }
            }

            try
            {
                procinst = _processAdminRepository.GetProcessInstanceAdmin(procInstId);
                //procinst = _k2DbService.GetProcInstCustom(procInstId);
            }
            catch (Exception ex)
            {
                //debug.Add(string.Format("Error getting ProcInst with ID '{0}'", procInstId));
                error.Add(string.Format("Error getting ProcInst with ID '{0}' : Error : {1} ", procInstId,
                    ex));
                trace.Add(string.Format("Error getting ProcInst with ID '{0}' : Error : {1} ", procInstId,
                    ex));
            }
            //try
            //{
            // Modified by VRS
            // If empty template and has a mailContentId
            if (string.IsNullOrEmpty(template) && mailContentId != null)
            {
                try
                {
                    var mailContent = _mailContentService.Find(m => m.MailContentId == mailContentId);

                    if (mailContent != null)
                    {
                        if (isGetSubject)
                        {
                            template = mailContent.Subject;
                        }
                        else
                        {
                            template = mailContent.Body;
                        }
                        var mailTemplateId = mailContent.MailTemplateId;
                        if (!isGetSubject && !string.IsNullOrEmpty(mailTemplateId))
                        {
                            var mailTemplate = _mailTemplateService.GetMailTemplate(mailTemplateId);

                            if (mailTemplate != null)
                            {
                                string templateHtml = null;
                                var templatePath = mailTemplate.TemplatePath;
                                if (!string.IsNullOrEmpty(templatePath))
                                {
                                    if (!templatePath.Contains(":"))
                                    {
                                        templatePath = System.Web.Hosting.HostingEnvironment.MapPath("~/" + templatePath);
                                    }
                                    if (System.IO.File.Exists(templatePath))
                                    {
                                        using (var sr =
                                            new System.IO.StreamReader(templatePath))
                                        {
                                            templateHtml = sr.ReadToEnd();
                                        }
                                    }
                                    else
                                    {
                                        templateHtml = mailTemplate.Template;
                                    }
                                }

                                if (!string.IsNullOrEmpty(templateHtml) && !string.IsNullOrEmpty(template))
                                {
                                    var mailContentIndex = templateHtml.IndexOf(MAILCONTENT,
                                        StringComparison.OrdinalIgnoreCase);
                                    if (mailContentIndex > -1)
                                    {
                                        template = Regex.Replace(templateHtml, MAILCONTENT, template,
                                            RegexOptions.IgnoreCase);
                                    }
                                    else
                                    {
                                        // Insert just before body end tag
                                        var bodyIndex = templateHtml.IndexOf("</body>",
                                            StringComparison.OrdinalIgnoreCase);
                                        if (bodyIndex > -1)
                                        {
                                            template = templateHtml.Insert(bodyIndex, template);
                                        }
                                    }
                                }
                                else if (!string.IsNullOrEmpty(templateHtml) && string.IsNullOrEmpty(template))
                                {
                                    // When content is empty, use just html template.
                                    template = templateHtml;
                                }
                            }
                        }
                    }
                    else
                    {
                        throw new Exception(string.Format("Cannot find Mail content with ID '{0}'", mailContentId));
                    }
                }
                catch (Exception exception)
                {
                    error.Add(string.Format("Error trying to Load MailContent: {0}",
                        mailContentId));
                    trace.Add(string.Format("Error trying to Load MailContent: {0} : {1}",
                        mailContentId, exception));
                }

                // Obsolete
                // Replace user-defined tokens
                //try
                //{
                //    var mailtokens = SMOServer.GetSmartObject(GetMailToken(root));
                //    mailtokens.MethodToExecute = GetMailToken_ListMethod(root);
                //    mailtokens.Properties[MailToken_ProcessName(root)].Value = procinst.Name;
                //    mailtokens.Properties[MailToken_ReferenceOnly(root)].Value = "false";
                //    var dt = SMOServer.ExecuteListDataTable(mailtokens);
                //    var token = MailToken_Token(root);
                //    var value = MailToken_Value(root);
                //    foreach (DataRow mailToken in dt.Rows)
                //    {
                //        template = template.Replace(mailToken[token].ToString(), mailToken[value].ToString());
                //    }
                //}
                //catch (Exception ex)
                //{
                //    debug.AppendLine(string.Format("Error trying to resolve user tokens"));
                //    trace.AppendLine(string.Format("Error trying to resolve user tokens: {0}",
                //                           ex.ToString()));
                //    //debug += string.Format("Error trying to resolve user tokens: {0}",
                //    //                       ex.ToString() + Environment.NewLine);
                //}
            }
            // Modified by VRS

            var activityFound = false;
            //get ADI
            if (activityInstanceDestinationId != null && activityInstanceDestinationId > 0)
            {

                for (int i = 0; i < 10; i++)
                {
                    try
                    {
                        var actInstDest = _k2DbService.GetActInstDest(procInstId,
                            activityInstanceDestinationId.Value);
                        if (actInstDest != null)
                        {
                            if (string.IsNullOrEmpty(destinationUser) && !string.IsNullOrEmpty(actInstDest.User))
                            {
                                destinationUser = actInstDest.User;
                            }
                            try
                            {
                                var act = _k2DbService.GetActByActInstId(actInstDest.ProcInstID,
                                    actInstDest.ActInstID);
                                if (act != null)
                                {
                                    SetActivityItems(act, coreContextItems);
                                    activityFound = true;
                                    break;
                                }
                            }
                            catch (Exception exception)
                            {
                                smoHasError = true;
                                error.Add(string.Format("Error getting Process Instance Activities : {0}",
                                    activityInstanceDestinationId));
                                trace.Add(string.Format("Error getting Process Instance Activities : {0} : {1}",
                                    activityInstanceDestinationId, exception));
                            }
                        }
                        Thread.Sleep(1000);

                        //for (int i = 0; i < 10; i++)
                        ////while (true)
                        //{
                        //    //var list = _context.ActInstDests.Where(
                        //    //    m => m.ProcInstID == procInstId && m.ID == activityInstanceDestinationId).ToReadOnlyListSafe();
                        //    var results =
                        //        _actiInstDestRepository.Query(
                        //            m =>
                        //                m.ProcessInstanceID == procInstId &&
                        //                m.ActivityInstanceDestinationID == activityInstanceDestinationId)
                        //            .Select()
                        //            .ToListSafe();
                        //    if (results != null && results.Any())
                        //    {
                        //        var activityInstanceDestination = results.First();
                        //        if (string.IsNullOrEmpty(destinationUser) && !string.IsNullOrEmpty(activityInstanceDestination.Destination))
                        //        {
                        //            destinationUser = activityInstanceDestination.Destination;
                        //        }
                        //        try
                        //        {
                        //            var act = _k2DbService.GetAct(activityInstanceDestination.ActivityID.Value);
                        //            if (act != null)
                        //            {
                        //                SetActivityItems(act, coreContextItems);
                        //                activityFound = true;
                        //            }
                        //            //SetActivityItems(activityInstanceDestination, coreContextItems);
                        //            //activityFound = true;
                        //            //for (int j = 0; j < 10 && !activityFound; j++)
                        //            //while (!activityFound)
                        //            //{
                        //            //    var activities = _workflowK2AdminService.GetProcInstActivities(procInstId);

                        //            //    foreach (SourceCode.Workflow.Management.Activity activity in activities)
                        //            //    {
                        //            //        if (activity.ID == activityInstanceDestination.ActivityID)
                        //            //        {
                        //            //            activityFound = true;
                        //            //            SetActivityItems(activity, coreContextItems);
                        //            //            break;
                        //            //        }

                        //            //        Thread.Sleep(1000);
                        //            //    }
                        //            //}
                        //            //if (!activityFound && activityInstanceDestination != null)
                        //            //{
                        //            //    SetActivityItems(activityInstanceDestination, coreContextItems);
                        //            //    //break;
                        //            //}
                        //        }
                        //        catch (Exception exception)
                        //        {
                        //            smoHasError = true;
                        //            debug.Add(string.Format("Error getting Process Instance Activities : {0}",
                        //                activityInstanceDestinationId));
                        //            trace.Add(string.Format("Error getting Process Instance Activities : {0} : {1}",
                        //                activityInstanceDestinationId, exception));
                        //        }
                        //        //try
                        //        //{
                        //        //    if (template.Contains("{Start{Actions}}"))
                        //        //    {
                        //        //        int procId = 0;
                        //        //        foreach (
                        //        //            SourceCode.Workflow.Management.ProcessInstance proc in
                        //        //                _workflowK2AdminService.GetProcessInstances(procinst.Folio))
                        //        //        {
                        //        //            if (proc.ID == procinst.ID)
                        //        //            {
                        //        //                procId = proc.ExecutingProcID;
                        //        //            }
                        //        //        }

                        //        //        var events = _workflowK2AdminService.GetProcessEvents(procId);

                        //        //        int? eventId = null;

                        //        //        //get activity info as well.

                        //        //        foreach (SourceCode.Workflow.Management.Event e in events)
                        //        //        {
                        //        //            if (e.ActID == activityInstanceDestination.ActivityID)
                        //        //            {
                        //        //                eventId = e.ID;
                        //        //                SetEventItems(e, coreContextItems);
                        //        //            }
                        //        //        }

                        //        //        //should be one result.
                        //        //        eventButtonActions = _workflowK2AdminService.GetEventActions(eventId.Value);
                        //        //    }
                        //        //}
                        //        //catch (Exception ex)
                        //        //{
                        //        //    smoHasError = true;
                        //        //    debug.Add(string.Format("Error getting event actions, ADI: {0}",
                        //        //        activityInstanceDestinationId));
                        //        //    trace.Add(string.Format("Error getting event actions, ADI : {0} : {1}",
                        //        //        activityInstanceDestinationId, ex));
                        //        //}
                        //        break;
                        //    }
                        //    else
                        //    {
                        //        Thread.Sleep(1000);
                        //    }
                        //}
                    }
                    catch (Exception exception)
                    {
                        error.Add(string.Format("Error getting activity instance destination : {0}",
                            activityInstanceDestinationId));
                        trace.Add(string.Format("Error getting activity instance destination : {0} : {1}",
                            activityInstanceDestinationId, exception));
                    }
                }
            }

            // Modified by VRS
            if (actiInstId != null && actiInstId != 0 && !activityFound)
            {
                for (int i = 0; i < 10; i++)
                //while(true)
                {
                    //var results =
                    //    _actiInstRepository.Query(
                    //        m => m.ProcessInstanceID == procInstId && m.ActivityInstanceID == actiInstId)
                    //        .Select().ToListSafe();

                    //if (results != null && results.Count > 0)
                    //var actInst = _k2DbService.GetActInst(procInstId, actiInstId.Value);
                    var act = _k2DbService.GetActByActInstId(procInstId, actiInstId.Value);
                    if (act == null)
                    {
                        try
                        {
                            //var act = _k2DbService.GetActByActInstId(procInstId ,results.First().ActivityInstanceID.Value);

                            if (act != null)
                            {
                                SetActivityItems(act, coreContextItems);
                                activityFound = true;
                            }
                            //activityFound = true;
                            //SetActivityItems(results.First(), coreContextItems);
                            //for (int j = 0; j < 5 && !activityFound; j++)
                            //while (true)
                            //{
                            //    var activities = _workflowK2AdminService.GetProcInstActivities(procInstId);
                            //    foreach (
                            //        SourceCode.Workflow.Management.Activity activity in activities)
                            //    {
                            //        if (activity.Name == results[0].ActivityName)
                            //        {
                            //            activityFound = true;
                            //            SetActivityItems(activity, coreContextItems);
                            //            break;
                            //        }
                            //    }
                            //    if (activityFound)
                            //    {
                            //        break;
                            //    }
                            //    else
                            //    {
                            //        Thread.Sleep(1000);
                            //    }
                            //}
                        }
                        catch (Exception ex)
                        {
                            smoHasError = true;
                            error.Add(string.Format("Error getting event actions, ProcInstId : {0}", procinst.ID));
                            trace.Add(string.Format("Error getting event actions, ProcInstId : {0} : {1}", procinst.ID,
                                ex));
                            //debug += string.Format("Error getting event actions : {0}", ex.ToString() + Environment.NewLine);
                        }
                        break;
                    }
                    else
                    {
                        Thread.Sleep(1000);
                    }
                }
            }
            // Modified by VRS

            // Modified by VRS 2014/08/08
            // Enable mail event name
            //else
            //{
            //    var activityFound = false;
            //    for (int j = 0; j < 5; j++)
            //    {
            //        if (!activityFound)
            //        {
            //            foreach (
            //                SourceCode.Workflow.Management.Activity act in
            //                    helper.WorkflowServer().GetProcInstActivities(procinst.ID))
            //            {
            //                if (act.ID == results[0].ActivityID)
            //                {
            //                    activityFound = true;
            //                    SetActivityItems(act, coreContextItems);
            //                    break;
            //                }
            //                Thread.Sleep(1000);
            //            }
            //        }
            //    }

            //}

            //add process context items.
            if (procinst != null)
            {
                coreContextItems = SetProcItems(procinst, coreContextItems);

                originatorUser = procinst.Originator.FQN;
                //originatorUser = procinst.Originator.ActionerName;
            }

            // Only do this if they require environment fields to be resolved.
            //if (!string.IsNullOrEmpty(template) && template.Contains("Environment."))
            //{
            //    try
            //    {
            //        SourceCode.Workflow.Management.ProcessSets procset = _workflowK2AdminService.GetProcessSets();
            //        foreach (SourceCode.Workflow.Management.ProcessSet proc in procset)
            //        {
            //            if (proc.FullName == procinst.FullName)
            //            {

            //                SourceCode.Workflow.Management.StringTable stringtab =
            //                    _workflowK2AdminService.GetStringTable(proc.StringTable);

            //                for (int i = 0; i < stringtab.Count; i++)
            //                {
            //                    coreContextItems.Add("Environment." + stringtab[i].Name, stringtab[i].Value);
            //                }

            //                break;
            //            }
            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        debug.Add(string.Format("Error getting string table fields"));
            //        trace.Add(string.Format("Error getting string table fields : {0}", ex));
            //    }
            //}
            //}
            //catch (Exception ex)
            //{
            //    debug.Add(string.Format("Error getting ProcInst with ID '{0}'", procInstId));
            //    trace.Add(string.Format("Error getting ProcInst with ID '{0}' : Error : {1} ", procInstId,
            //        ex));
            //}

            try
            {
                // Get Originator details
                if (!string.IsNullOrEmpty(originatorUser))
                {
                    var originator = procinst.Originator;

                    if (originator != null)
                    {
                        coreContextItems.Add("ProcessInstance.Originator.FQN", originator.FQN ?? string.Empty);
                        coreContextItems.Add("ProcessInstance.Originator.Name", originator.Name ?? string.Empty);
                        coreContextItems.Add("ProcessInstance.Originator.Description", originator.Description ?? string.Empty);
                        coreContextItems.Add("ProcessInstance.Originator.Email", originator.Email ?? string.Empty);
                        coreContextItems.Add("ProcessInstance.Originator.Manager", originator.Manager ?? string.Empty);
                        coreContextItems.Add("ProcessInstance.Originator.DisplayName", originator.DisplayName ?? string.Empty);

                        coreContextItems.Add("PI.Originator.FQN", originator.FQN ?? string.Empty);
                        coreContextItems.Add("PI.Originator.Name", originator.Name ?? string.Empty);
                        coreContextItems.Add("PI.Originator.Description", originator.Description ?? string.Empty);
                        coreContextItems.Add("PI.Originator.Email", originator.Email ?? string.Empty);
                        coreContextItems.Add("PI.Originator.Manager", originator.Manager ?? string.Empty);
                        coreContextItems.Add("PI.Originator.DisplayName", originator.DisplayName ?? string.Empty);
                    }
                }
                else if (string.IsNullOrEmpty(originatorUser) && procinst != null)
                {
                    var originator = procinst.Originator;
                    if (originator != null)
                    {
                        coreContextItems.Add("ProcessInstance.Originator.FQN", originator.FQN ?? string.Empty);
                        coreContextItems.Add("ProcessInstance.Originator.Name", originator.Name ?? string.Empty);
                        coreContextItems.Add("ProcessInstance.Originator.Description", originator.Description ?? string.Empty);
                        coreContextItems.Add("ProcessInstance.Originator.Email", originator.Email ?? string.Empty);
                        coreContextItems.Add("ProcessInstance.Originator.Manager", originator.Manager ?? string.Empty);
                        coreContextItems.Add("ProcessInstance.Originator.DisplayName", originator.DisplayName ?? string.Empty);

                        coreContextItems.Add("PI.Originator.FQN", originator.FQN ?? string.Empty);
                        coreContextItems.Add("PI.Originator.Name", originator.Name ?? string.Empty);
                        coreContextItems.Add("PI.Originator.Description", originator.Description ?? string.Empty);
                        coreContextItems.Add("PI.Originator.Email", originator.Email ?? string.Empty);
                        coreContextItems.Add("PI.Originator.Manager", originator.Manager ?? string.Empty);
                        coreContextItems.Add("PI.Originator.DisplayName", originator.DisplayName ?? string.Empty);
                    }
                }
            }
            catch (Exception ex)
            {
                error.Add(string.Format("Error getting details for OriginatorUser {0}", originatorUser));
                trace.Add(string.Format("Error getting details for OriginatorUser {0} : {1} ", originatorUser, ex));
            }

            try
            {
                if (!string.IsNullOrEmpty(destinationUser))
                {
                    //if (destinationUser.Contains("\\") && !destinationUser.Contains(":"))
                    //{
                    //    destinationUser = AppSettings.Instance.SecurityLabelName() + ":" + destinationUser;
                    //}
                    var destination = _umUserService.GetUser(destinationUser);

                    if (destination != null)
                    {
                        coreContextItems.Add("ProcessInstance.ActivityInstance.User.FQN", destination.FQN ?? string.Empty);
                        coreContextItems.Add("ProcessInstance.ActivityInstance.User.Name", destination.Name ?? string.Empty);
                        coreContextItems.Add("ProcessInstance.ActivityInstance.User.Description", destination.Description ?? string.Empty);
                        coreContextItems.Add("ProcessInstance.ActivityInstance.User.Email", destination.Email ?? string.Empty);
                        coreContextItems.Add("ProcessInstance.ActivityInstance.User.Manager", destination.Manager ?? string.Empty);
                        coreContextItems.Add("ProcessInstance.ActivityInstance.User.DisplayName", destination.DisplayName ?? string.Empty);

                        coreContextItems.Add("PI.ADI.User.FQN", destination.FQN ?? string.Empty);
                        coreContextItems.Add("PI.ADI.User.Name", destination.Name ?? string.Empty);
                        coreContextItems.Add("PI.ADI.User.Description", destination.Description ?? string.Empty);
                        coreContextItems.Add("PI.ADI.User.Email", destination.Email ?? string.Empty);
                        coreContextItems.Add("PI.ADI.User.Manager", destination.Manager ?? string.Empty);
                        coreContextItems.Add("PI.ADI.User.DisplayName", destination.DisplayName ?? string.Empty);
                    }
                }
            }
            catch (Exception ex)
            {
                error.Add(string.Format("Error getting details for DetinationUser {0} ", destinationUser));
                trace.Add(string.Format("Error getting details for DetinationUser {0} : {1} ", destinationUser,
                    ex));
            }

            // Modified by VRS
            if (string.IsNullOrEmpty(template))
            {
                template = "";
            }
            // Modified by VRS
            //replace context items
            template = ReplaceContext(template, coreContextItems);
            //resolve {HyperLink}|{/HyperLink}

            #region HyperLinks

            //while (!string.IsNullOrEmpty(template) && template.Contains("{HyperLink}") && !smoHasError)
            //{
            //    string hyperlink = string.Empty;

            //    try
            //    {
            //        string starttag = "{HyperLink}";
            //        string endtag = "{/HyperLink}";

            //        int posA = template.IndexOf(starttag);
            //        int posB = template.IndexOf(endtag, posA);

            //        hyperlink = template.Substring(posA, (posB - posA) + endtag.Length);

            //        string details = hyperlink.Replace(starttag, string.Empty);
            //        details = details.Replace(endtag, string.Empty);
            //        string[] obj = details.Split('|');

            //        string link = obj[0];
            //        string display = obj[1];

            //        hyperlink = template.Substring(posA, (posB - posA) + endtag.Length);

            //        string actionTemplate = hyperlink.Replace(starttag, string.Empty);
            //        actionTemplate = actionTemplate.Replace(endtag, string.Empty);

            //        string linkTemplate = string.Empty;

            //        linkTemplate = string.Format("<a href='{0}'>{1}</a>", link, display);

            //        //now replace the original string.
            //        template = template.Replace(hyperlink, linkTemplate);
            //    }
            //    catch (Exception ex)
            //    {
            //        debug.Add(string.Format("Error with hyperlink '{0}' ", hyperlink));
            //        trace.Add(string.Format("Error with hyperlink '{0}' : {1} ", hyperlink, ex));
            //        smoHasError = true;
            //    }
            //}

            #endregion

            //#region smoload - old method

            ////Call Smartobject methods here
            //while (template.Contains("{SmartObjectLoad:") && !SMOHasError)
            //{
            //    string strToReplace = string.Empty;

            //    try
            //    {
            //        //Lets run this smartobject.
            //        int posA = template.IndexOf("{SmartObjectLoad:");
            //        int posB = template.IndexOf("}", posA) + 1;

            //        strToReplace = template.Substring(posA, (posB - posA));

            //        string SMO = template.Substring(posA + 17, (posB - posA) - 18);
            //        string[] obj = SMO.Split(',');
            //        string resultStr = string.Empty;

            //        Dictionary<string, object> criteria = new Dictionary<string, object>();
            //        criteria.Add(obj[1], obj[2]);
            //        Dictionary<string, object> DRresults = helper.SmartObjectClient().SmartObjectLoad(criteria, obj[0]);

            //        try
            //        {
            //            resultStr = (DRresults[obj[3]] == null) ? string.Empty : DRresults[obj[3]].ToString();
            //        }
            //        catch
            //        {
            //            throw new Exception("return Field '" + obj[3].ToString() + "' cannot be found");
            //        }

            //        template = template.Replace(strToReplace, resultStr);
            //    }
            //    catch (Exception ex)
            //    {
            //        debug.AppendLine(string.Format("Error with smartoject load call '{0}' ", strToReplace));
            //        trace.AppendLine(string.Format("Error with smartoject load call '{0}' : {1} ", strToReplace, ex.ToString()));
            //        //debug += string.Format("Error with smartoject load call '{0}' : {1} ", strToReplace, ex.ToString() + Environment.NewLine);
            //        SMOHasError = true;
            //    }
            //}

            //#endregion

            //#region smoload - new way

            //while (template.Contains("{SMOLoad}") && !SMOHasError)
            //{
            //    string strToReplace = string.Empty;

            //    try
            //    {
            //        //Lets run this smartobject.

            //        string starttag = "{SMOLoad}";
            //        string endtag = "{/SMOLoad}";

            //        int posA = template.IndexOf(starttag);
            //        int posB = template.IndexOf(endtag, posA);

            //        strToReplace = template.Substring(posA, (posB - posA) + endtag.Length);

            //        string SMO = strToReplace.Replace(starttag, string.Empty);
            //        SMO = SMO.Replace(endtag, string.Empty);
            //        string[] obj = SMO.Split(',');

            //        string alias = RemoveLineBreaks(obj[0]);
            //        string smoName = RemoveLineBreaks(obj[1]);
            //        string smoMethod = RemoveLineBreaks(obj[2]);
            //        string filterfield = RemoveLineBreaks(obj[3]);
            //        string filtervalue = RemoveLineBreaks(obj[4]);

            //        //string resultStr = string.Empty;

            //        Dictionary<string, object> criteria = new Dictionary<string, object>();
            //        criteria.Add(filterfield, filtervalue);
            //        var DRresults = helper.SmartObjectClient().SmartObjectLoad(criteria, smoName, smoMethod);

            //        template = ReplaceDataContext(template, alias, DRresults);
            //        //now replace the original string.
            //        template = template.Replace(strToReplace, string.Empty);
            //    }
            //    catch (Exception ex)
            //    {
            //        debug.AppendLine(string.Format("Error with smartoject load call '{0}' ", strToReplace));
            //        trace.AppendLine(string.Format("Error with smartoject load call '{0}' : {1} ", strToReplace,
            //            ex.ToString()));
            //        //debug.AppendLine(string.Format("Error with smartoject load call '{0}' : {1} ", strToReplace,
            //        //    ex.ToString()));
            //        //debug += string.Format("Error with smartoject load call '{0}' : {1} ", strToReplace, ex.ToString() + Environment.NewLine);
            //        SMOHasError = true;
            //    }
            //}

            //#endregion

            //#region smolist

            //while (template.Contains("{SMOList}") && !SMOHasError)
            //{
            //    string strToReplace = string.Empty;

            //    try
            //    {
            //        string starttag = "{SMOList}";
            //        string endtag = "{/SMOList}";

            //        int posA = template.IndexOf(starttag);
            //        int posB = template.IndexOf(endtag, posA);

            //        strToReplace = template.Substring(posA, (posB - posA) + endtag.Length);

            //        string SMO = strToReplace.Replace(starttag, string.Empty);
            //        SMO = SMO.Replace(endtag, string.Empty);
            //        string[] obj = SMO.Split(',');

            //        string alias = obj[0];
            //        string smoName = obj[1];
            //        string smoMethod = obj[2];
            //        string filterfields = obj[3];
            //        string filtervalues = obj[4];

            //        string resultStr = string.Empty;

            //        Dictionary<string, object> criteria = new Dictionary<string, object>();

            //        var filters = obj[3].Split('|');
            //        var values = obj[4].Split('|');

            //        int count = 0;
            //        foreach (string str in filters)
            //        {
            //            criteria.Add(str, values[count]);
            //            count++;
            //        }

            //        var DTresults = helper.SmartObjectClient().SmartObjectGetList(criteria, smoName, smoMethod);

            //        template = ReplaceDataRepeaterContext(template, alias, DTresults);
            //        //now replace the original string.
            //        template = template.Replace(strToReplace, string.Empty);
            //    }
            //    catch (Exception ex)
            //    {
            //        debug.AppendLine(string.Format("Error with smartoject list call '{0}' ", strToReplace));
            //        trace.AppendLine(string.Format("Error with smartoject list call '{0}' : {1} ", strToReplace, ex.ToString()));
            //        //debug += string.Format("Error with smartoject list call '{0}' : {1} ", strToReplace, ex.ToString() + Environment.NewLine);
            //        SMOHasError = true;
            //    }
            //}

            //#endregion

            #region SmartButtons

            //while (template.Contains("{Start{Actions}}") && !smoHasError)
            //{
            //    string strToReplace = string.Empty;

            //    try
            //    {
            //        string starttag = "{Start{Actions}}";
            //        string endtag = "{End{Actions}}";

            //        int posA = template.IndexOf(starttag, StringComparison.OrdinalIgnoreCase);
            //        int posB = template.IndexOf(endtag, posA, StringComparison.OrdinalIgnoreCase);

            //        strToReplace = template.Substring(posA, (posB - posA) + endtag.Length);

            //        string actionTemplate = strToReplace.Replace(starttag, string.Empty);
            //        actionTemplate = actionTemplate.Replace(endtag, string.Empty);

            //        string buttonsTemplate = string.Empty;

            //        //resolve tokens.
            //        //for each action loop through
            //        foreach (SourceCode.Workflow.Management.EventAction action in eventButtonActions)
            //        {
            //            string temp = actionTemplate.Replace("{Actions{Name}}", action.Name);
            //            temp = temp.Replace("{Actions{Description}}", action.MetaData);
            //            buttonsTemplate += temp;
            //        }

            //        //now replace the original string.
            //        template = template.Replace(strToReplace, buttonsTemplate);
            //    }
            //    catch (Exception ex)
            //    {
            //        debug.Add(string.Format("Error with smartbuttons actions '{0}' ", strToReplace));
            //        trace.Add(string.Format("Error with smartbuttons actions '{0}' : {1} ", strToReplace,
            //            ex));
            //        smoHasError = true;
            //    }
            //}

            #endregion

            // Modified by VRS
            // Replace placeholders with inputs
            if (!string.IsNullOrEmpty(items))
            {
                string[] array = items.Split('~');

                for (int i = 0; i < array.Length; i++)
                {
                    template = template.Replace("[" + i + "]", array[i]);
                }
            }
            // Modified by VRS

            //Show debug info
            var traceEnabled = TraceEnable();
            var logEnabled = LoggingEnable();
            if (error.Any())
            {
                log.Add("Log> -- start template --");
                log.Add(template);
                log.Add("Log> -- end template --");

                log.Add("Log> -- start input --");
                log.Add("procInstId: " + procInstId);
                log.Add("destinationUser: " + destinationUser);
                log.Add("worklistUrl: " + worklistUrl);
                log.Add("mailContentId: " + mailContentId);
                log.Add("isGetSubject: " + isGetSubject);
                log.Add("items: " + items);
                log.Add("actiInstId: " + actiInstId);
                log.Add("activityInstanceDestinationId: " + activityInstanceDestinationId);
                log.Add("Log> -- end input --");

                var contextData = new StringBuilder();
                contextData.AppendLine(Environment.NewLine + "Current Context Data " + Environment.NewLine);
                foreach (var keyValue in coreContextItems)
                {
                    contextData.AppendLine(string.Format("{0}: {1}, ", keyValue.Key,
                        string.IsNullOrEmpty(keyValue.Value) ? "null" : keyValue.Value));
                }
                error.Add(contextData.ToString());
                log.Add("Log> Error at " + DateTime.Now);
                log.Add("Log> --End Mail--");
                log.Add(Environment.NewLine);
            }
            else
            {
                log.Add("Log> Complete at " + DateTime.Now);
                log.Add("Log> --End Mail--");
                log.Add(Environment.NewLine);
            }
            if (logEnabled)
            {
                WriteFile(log, GetLoggingLocation(), "MAILLOG_");
            }
            if (traceEnabled)
            {
                trace.Add("Trace> -- start template --");
                trace.Add(template);
                trace.Add("Trace> -- end template --");

                trace.Add("Trace> -- start input --");
                trace.Add("procInstId: " + procInstId);
                trace.Add("destinationUser: " + destinationUser);
                trace.Add("worklistUrl: " + worklistUrl);
                trace.Add("mailContentId: " + mailContentId);
                trace.Add("isGetSubject: " + isGetSubject);
                trace.Add("items: " + items);
                trace.Add("actiInstId: " + actiInstId);
                trace.Add("activityInstanceDestinationId: " + activityInstanceDestinationId);
                trace.Add("Trace> -- end input --");

                var contextData = new StringBuilder();
                contextData.AppendLine(Environment.NewLine + "Current Context Data " + Environment.NewLine);
                foreach (var keyValue in coreContextItems)
                {
                    contextData.AppendLine(string.Format("{0}: {1}, ", keyValue.Key, string.IsNullOrEmpty(keyValue.Value) ? "null" : keyValue.Value));
                }
                trace.Add(contextData.ToString());

                trace.Add("Trace> Complete at " + DateTime.Now);
                trace.Add("Trace> --End Mail--");
                trace.Add("-----------------------------------");
                trace.Add(Environment.NewLine);

                WriteFile(trace, GetTraceLocation(), "MAILTRACE_");
            }
            if (error.Any())
            {
#if DEBUG
                throw new AggregateException(trace.Select(m => new Exception(m)));
#else
                throw new AggregateException(error.Select(m => new Exception(m)));
#endif
            }
            return template;
        }

        private static Dictionary<string, string> SetProcItems(SourceCode.Workflow.Client.ProcessInstance procinst,
            Dictionary<string, string> contextItems)
        {
            contextItems.Add("ProcessInstance.ID", procinst.ID.ToString());
            contextItems.Add("PI.ID", procinst.ID.ToString());
            contextItems.Add("ProcessInstance.Name", procinst.Name.ToString());
            contextItems.Add("PI.Name", procinst.Name.ToString());
            contextItems.Add("ProcessInstance.Description", procinst.Description.ToString());
            contextItems.Add("PI.Description", procinst.Description.ToString());
            contextItems.Add("ProcessInstance.Guid", procinst.Guid.ToString());
            contextItems.Add("PI.Guid", procinst.Guid.ToString());
            contextItems.Add("ProcessInstance.ExpectedDuration", procinst.ExpectedDuration.ToString());
            contextItems.Add("PI.ExpectedDuration", procinst.ExpectedDuration.ToString());
            contextItems.Add("ProcessInstance.Folio", procinst.Folio.ToString());
            contextItems.Add("PI.Folio", procinst.Folio.ToString());
            contextItems.Add("ProcessInstance.Priority", procinst.Priority.ToString());
            contextItems.Add("PI.Priority", procinst.Priority.ToString());
            contextItems.Add("ProcessInstance.StartDate", procinst.StartDate.ToString());
            contextItems.Add("PI.StartDate", procinst.StartDate.ToString());

            //Get process Data Fields.
            foreach (SourceCode.Workflow.Client.DataField pdf in procinst.DataFields)
            {
                contextItems.Add("ProcessInstance.DataFields." + pdf.Name, pdf.Value.ToString());
                contextItems.Add("PI.DataFields." + pdf.Name, pdf.Value.ToString());
            }

            //Get process Data Fields.
            foreach (SourceCode.Workflow.Client.XmlField pxdf in procinst.XmlFields)
            {
                contextItems.Add("ProcessInstance.XmlFields." + pxdf.Name, pxdf.Value.ToString());
                contextItems.Add("PI.XmlFields." + pxdf.Name, pxdf.Value.ToString());
            }

            return contextItems;
        }

        //private static Dictionary<string, string> SetProcItems(PRUKSA.Models.K2.ProcInstCustom procinst,
        //    Dictionary<string, string> contextItems)
        //{
        //    contextItems.Add("ProcessInstance.ID", procinst.ID.ToString());
        //    contextItems.Add("PI.ID", procinst.ID.ToString());
        //    contextItems.Add("ProcessInstance.Name", procinst.Name.ToString());
        //    contextItems.Add("PI.Name", procinst.Name.ToString());
        //    contextItems.Add("ProcessInstance.Description", procinst.Description.ToString());
        //    contextItems.Add("PI.Description", procinst.Description.ToString());
        //    contextItems.Add("ProcessInstance.Guid", procinst.Guid.ToString());
        //    contextItems.Add("PI.Guid", procinst.Guid.ToString());
        //    contextItems.Add("ProcessInstance.ExpectedDuration", procinst.ExpectedDuration.ToString());
        //    contextItems.Add("PI.ExpectedDuration", procinst.ExpectedDuration.ToString());
        //    contextItems.Add("ProcessInstance.Folio", procinst.Folio.ToString());
        //    contextItems.Add("PI.Folio", procinst.Folio.ToString());
        //    contextItems.Add("ProcessInstance.Priority", procinst.Priority.ToString());
        //    contextItems.Add("PI.Priority", procinst.Priority.ToString());
        //    contextItems.Add("ProcessInstance.StartDate", procinst.StartDate.ToString());
        //    contextItems.Add("PI.StartDate", procinst.StartDate.ToString());

        //    //Get process Data Fields.
        //    foreach (PRUKSA.Models.K2.ProcInstData pdf in procinst.DataFields)
        //    {
        //        contextItems.Add("ProcessInstance.DataFields." + pdf.Name, pdf.Value.ToString());
        //        contextItems.Add("PI.DataFields." + pdf.Name, pdf.Value.ToString());
        //    }

        //    //Get process Data Fields.
        //    foreach (SourceCode.Workflow.Client.XmlField pxdf in procinst.XmlFields)
        //    {
        //        contextItems.Add("ProcessInstance.XmlFields." + pxdf.Name, pxdf.Value.ToString());
        //        contextItems.Add("PI.XmlFields." + pxdf.Name, pxdf.Value.ToString());
        //    }

        //    return contextItems;
        //}

        //private static Dictionary<string, string> SetActivityItems(SourceCode.Workflow.Management.Activity activity,
        //    Dictionary<string, string> contextItems)
        //{
        //    contextItems.Add("PI.AI.Description", activity.Description);
        //    contextItems.Add("PI.AI.Name", activity.Name);
        //    contextItems.Add("PI.AI.Priority", activity.Priority.ToString());
        //    contextItems.Add("PI.AI.MetaData", activity.MetaData.ToString());
        //    return contextItems;
        //}

        //private static Dictionary<string, string> SetActivityItems(Activity_Instance_Destination activity,
        //    Dictionary<string, string> contextItems)
        //{
        //    //contextItems.Add("PI.AI.Description", activity.Description);
        //    contextItems.Add("PI.AI.Name", activity.ActivityName);
        //    //contextItems.Add("PI.AI.Priority", activity.Priority.ToString());
        //    //contextItems.Add("PI.AI.MetaData", activity.MetaData.ToString());
        //    return contextItems;
        //}

        //private static Dictionary<string, string> SetActivityItems(Activity_Instance activity,
        //    Dictionary<string, string> contextItems)
        //{
        //    //contextItems.Add("PI.AI.Description", activity.Description);
        //    contextItems.Add("PI.AI.Name", activity.ActivityName);
        //    //contextItems.Add("PI.AI.Priority", activity.Priority.ToString());
        //    //contextItems.Add("PI.AI.MetaData", activity.MetaData.ToString());
        //    return contextItems;
        //}

        private static Dictionary<string, string> SetActivityItems(Models.K2.Act activity,
            Dictionary<string, string> contextItems)
        {
            contextItems.Add("PI.AI.Description", activity.Descr);
            contextItems.Add("PI.AI.Name", activity.Name);
            //contextItems.Add("PI.AI.Priority", activity.Priority.ToString());
            //contextItems.Add("PI.AI.MetaData", activity.MetaData.ToString());
            return contextItems;
        }

        //private static Dictionary<string, string> SetEventItems(SourceCode.Workflow.Management.Event Event,
        //    Dictionary<string, string> contextItems)
        //{
        //    contextItems.Add("PI.E.Description", Event.Description);
        //    contextItems.Add("PI.E.CredentialUser", Event.CredentialUser);
        //    contextItems.Add("PI.E.MetaData", Event.MetaData);
        //    contextItems.Add("PI.E.Name", Event.Name);
        //    contextItems.Add("PI.E.Priority", Event.Priority.ToString());
        //    return contextItems;
        //}

        private static string ReplaceContext(string template, Dictionary<string, string> contextItems)
        {
            //Replace tokens here.
            foreach (var keyValue in contextItems)
            {
                template = template.Replace("[" + keyValue.Key + "]", keyValue.Value);
            }
            return template;
        }

        private static string RemoveLineBreaks(string str)
        {
            Regex re = new Regex("[\r\n\t]", RegexOptions.Compiled);
            return re.Replace(str, string.Empty);
        }

        private bool TraceEnable()
        {
            var enable = false;
            bool.TryParse(System.Configuration.ConfigurationManager.AppSettings["MailTraceEnable"], out enable);
            return enable;
            //return false;
            //return AppSettings.Instance.Settings.MailTraceEnable();
        }

        private bool LoggingEnable()
        {
            var enable = false;
            bool.TryParse(System.Configuration.ConfigurationManager.AppSettings["MailLogEnable"], out enable);
            return enable;
            //return false;
            //return AppSettings.Instance.MailLogEnable();
        }

        private string GetLoggingLocation()
        {
            return System.Configuration.ConfigurationManager.AppSettings["MailLogLocation"];
            //return String.Empty;
            //return AppSettings.Instance.MailLogLocation();
        }

        private string GetTraceLocation()
        {
            return System.Configuration.ConfigurationManager.AppSettings["MailTraceLocation"];
            //return String.Empty;
            //return AppSettings.Instance.MailTraceLocation();
        }

        private static bool WriteFile(List<string> list, string location, string prefix)
        {
            if (list == null
                || !list.Any()
                || string.IsNullOrEmpty(location))
            {
                return false;
            }

            // do it
            try
            {
                if (!location.Contains(":"))
                {
                    location = System.Web.Hosting.HostingEnvironment.MapPath("~/" + location);
                }
                if (!System.IO.Directory.Exists(location))
                {
                    System.IO.Directory.CreateDirectory(location);
                }
                // Generate file name
                var fileName = prefix + DateTime.Now.ToString("yyyyMMdd") + ".txt";
                var fullPath = System.IO.Path.Combine(location, fileName);

                if (System.IO.File.Exists(fullPath))
                {
                    using (System.IO.StreamWriter writer = System.IO.File.AppendText(fullPath))
                    {
                        writer.Write(string.Join(" " + Environment.NewLine, list));
                    }
                }
                else
                {
                    using (System.IO.StreamWriter writer = System.IO.File.CreateText(fullPath))
                    {
                        writer.Write(string.Join(" " + Environment.NewLine, list));
                    }
                }

                return true;
            }
            catch (Exception exception)
            {
                return false;
            }
        }

        public string GetNotificationSubject(string mailContentId, int procInstId,
            //string originatorUser,
            int actiInstDestId, string items = null)
        {
            return Execute(template: string.Empty, procInstId: procInstId, /*originatorUser: originatorUser,*/ destinationUser: null, worklistUrl: "SN=" + procInstId + "_" + actiInstDestId, mailContentId: mailContentId, isGetSubject: true, items: items, activityInstanceDestinationId: actiInstDestId);
        }

        public string GetNotificationBody(string mailContentId, int procInstId,
            //string originatorUser, 
            string worklistUrl, string items = null)
        {
            return Execute(template: string.Empty, procInstId: procInstId, /*originatorUser: originatorUser,*/ destinationUser: null, worklistUrl: worklistUrl, mailContentId: mailContentId, isGetSubject: false, items: items);
        }

        public string GetEscalationSubject(string mailContentId, int procInstId,
            //string originatorUser, 
            string detinationUser, int actiInstDestId, string items = null)
        {
            return Execute(template: string.Empty, procInstId: procInstId, /*originatorUser: originatorUser,*/ destinationUser: detinationUser, worklistUrl: null, mailContentId: mailContentId, isGetSubject: true, items: items, activityInstanceDestinationId: actiInstDestId);
        }

        public string GetEscalationBody(string mailContentId, int procInstId,
            //string originatorUser, 
            string detinationUser, int actiInstDestId, string worklistUrl, string items = null)
        {
            return Execute(template: null, procInstId: procInstId, /*originatorUser: originatorUser,*/ destinationUser: detinationUser, worklistUrl: worklistUrl, mailContentId: mailContentId, isGetSubject: false, items: items, activityInstanceDestinationId: actiInstDestId);
        }

        public string GetMailEventSubject(string mailContentId, int procInstId,
            //string originatorUser, 
            int actiInstId,
            string items = null)
        {
            return Execute(template: string.Empty, procInstId: procInstId, /*originatorUser: originatorUser,*/ destinationUser: null, worklistUrl: null, mailContentId: mailContentId, isGetSubject: true, items: items, activityInstanceDestinationId: actiInstId);
        }

        public string GetMailEventBody(string mailContentId, int procInstId,
            //string originatorUser, 
            string detinationUser,
            int actiInstId, string items = null)
        {
            return Execute(template: string.Empty, procInstId: procInstId, /*originatorUser: originatorUser,*/ destinationUser: detinationUser, worklistUrl: null, mailContentId: mailContentId, isGetSubject: false, items: items, activityInstanceDestinationId: actiInstId);
        }
    }
}