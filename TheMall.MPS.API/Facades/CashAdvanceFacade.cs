﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Routing;
using SourceCode.Workflow.Client;
using CrossingSoft.Framework.Models.Enums;
using TheMall.MPS.API.Facades.Abstracts;
using TheMall.MPS.API.Facades.Interfaces;
using TheMall.MPS.API.Mappers;
using TheMall.MPS.API.Services;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.API.Services.Workflow;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Models.CashAdvance;
using TheMall.MPS.Models.Enums;
using TheMall.MPS.Models.K2;
using TheMall.MPS.Models.Proposal;
using TheMall.MPS.Models.Workflow;
using TheMall.MPS.ViewModels;
using TheMall.MPS.ViewModels.CashAdvance;
using TheMall.MPS.ViewModels.CashClearing;
using TheMall.MPS.ViewModels.Memo;
using TheMall.MPS.ViewModels.Proposal;


namespace TheMall.MPS.API.Facades
{
    public interface ICashAdvanceFacade : IMpsWorkflowFacade<MPS_CashAdvanceTable, MPS_CashAdvanceAttachment, MPS_CashAdvanceComment, CashAdvanceViewModel>
    {
        Task<CashAdvanceViewModel> GetNoLineId(UrlHelper url, int id, string sn = null);
        IEnumerable<CashAdvanceIndexDataViewModel> GetIndexData(string username);
        IEnumerable<ProposalLineViewModel> GetProposalLineData(string unitCode, int parentId);
        ProposalInfoViewModel GetProposalOneLineFromId(int id);
        CashAdvanceViewModel GetViewModelFromProposalLineId(int id);
        CashAdvanceViewModel GetViewModelFromDocumentNumber(string documentNumber);
        void SetFinalApproverDeleted(int id);
        string GetEmailInformAll(int workflowTableId);
         CashAdvanceViewModel DeleteBy(int id, string username);
        Task<CashAdvanceViewModel> GetDataBindAdvance(int propLineid);
        IEnumerable<CashAdvanceIndexDataViewModel> GetAccIndex(string username);
    }
    public class CashAdvanceFacade : BaseMpsWorkflowFacade<MPS_CashAdvanceTable, MPS_CashAdvanceAttachment, MPS_CashAdvanceComment, CashAdvanceViewModel>, ICashAdvanceFacade
    {
        private readonly IMPSSession _session;
        private readonly ICashAdvanceService _workflowTableService;
        private readonly IAttachmentService<MPS_CashAdvanceAttachment, MPS_CashAdvanceTable> _attachmentService;
        private readonly ICommentService<MPS_CashAdvanceComment> _commentService;
        private readonly IWorkflowService<MPS_CashAdvanceTable> _workflowService;
        private readonly INumberSequenceService _numberSequenceService;
        private readonly ICashAdvanceProcess _definition;
        private readonly ICashAdvanceMapper _mapper;
        private readonly ICashAdvanceLineMapper _lineMapper;
        private readonly ICashAdvanceLineService _lineService;
        private readonly ICashAdvanceApproveService _approvalService;
        private readonly ICashAdvanceApproveMapper _approveMapper;
        private readonly IProposalLineService _proposalLineService;
        private readonly IProposalLineMapper _proposalLineMapper;
        private readonly IProposalMapper _proposalMapper;
        private readonly IBudgetProposalTransService _budgetProposalTransService;
        private readonly IBudgetProposalTransFacade _budgetProposalTransFacade;
        private readonly IEmployeeTableService _employeeTableService;
        private readonly IProposalBudgetPlanService _budgetPlanService;
        private readonly IAccountingService _accountingService;
        private readonly ICompanyService _companyService;

        public CashAdvanceFacade(
            IMPSSession session,
            ICashAdvanceService workflowTableService, 
            IAttachmentService<MPS_CashAdvanceAttachment, MPS_CashAdvanceTable> attachmentService, 
            ICommentService<MPS_CashAdvanceComment> commentService, 
            IWorkflowService<MPS_CashAdvanceTable> workflowService, 
            INumberSequenceService numberSequenceService, 
            ICashAdvanceProcess definition, 
            ICashAdvanceMapper mapper, 
            ICashAdvanceLineMapper lineMapper,
            ICashAdvanceLineService lineService, 
            ICashAdvanceApproveService approvalService, 
            ICashAdvanceApproveMapper approveMapper, 
            IProposalLineService proposalLineService, 
            IProposalLineMapper proposalLineMapper, 
            IProposalMapper proposalMapper, 
            IBudgetProposalTransService budgetProposalTransService,
            IBudgetProposalTransFacade budgetProposalTransFacade, 
            IEmployeeTableService employeeTableService, 
            IProposalBudgetPlanService budgetPlanService,
            IAccountingService accountingService, 
            ICompanyService companyService)
            : base(session, workflowTableService, attachmentService, commentService, workflowService, numberSequenceService, definition)
        {
            _workflowTableService = workflowTableService;
            _attachmentService = attachmentService;
            _commentService = commentService;
            _workflowService = workflowService;
            _numberSequenceService = numberSequenceService;
            _definition = definition;
            _mapper = mapper;
            _lineMapper = lineMapper;
            _lineService = lineService;
            _approvalService = approvalService;
            _approveMapper = approveMapper;
            _proposalLineService = proposalLineService;
            _proposalLineMapper = proposalLineMapper;
            _proposalMapper = proposalMapper;
            _budgetProposalTransService = budgetProposalTransService;
            _budgetProposalTransFacade = budgetProposalTransFacade;
            _employeeTableService = employeeTableService;
            _budgetPlanService = budgetPlanService;
            _accountingService = accountingService;
            _companyService = companyService;
            _session = session;
        }

        public override async Task<CashAdvanceViewModel> GetViewModel(UrlHelper url, int id, string sn = null)
        {
            var model = await _workflowTableService.GetAsync(id);
            var viewModel =
                await this.InitWorkflowViewModel(url,id, sn, _mapper.ToViewModel(model));

            return viewModel;
        }

        public async Task<CashAdvanceViewModel> GetNoLineId(UrlHelper url, int id, string sn = null)
        {
            var model = await _workflowTableService.GetAsync(id);
            var viewModel = await this.InitWorkflowViewModel(url,id, sn, _mapper.ToViewModelNoLineId(model));

            return viewModel;
        }

        public override async Task<CashAdvanceViewModel> SaveAsync(CashAdvanceViewModel viewModel, string action = null)
        {
            if (viewModel == null)
                throw new ArgumentNullException("viewModel");

            //if (viewModel.Budget > viewModel.BudgetPlan)
            //    throw new System.InvalidOperationException("The budget sum value must not be more than the butget plan!!!");

            if (viewModel.Company == null)
            {
                var vendorCode = _companyService.Find(m => m.VendorCode.Equals(viewModel.Branch.PlaceCode));
                if (vendorCode != null)
                {
                    viewModel.Company = vendorCode.ToViewModel();
                }
                else
                {
                    viewModel.Company = null;
                }
            }

            try
            {
                var model = _mapper.ToModel(viewModel);
                if (viewModel.Id <= 0)
                {
                    model = _workflowTableService.Insert(model);
                }
                else
                {
                    model = _workflowTableService.Update(model);
                }

                await _session.SaveChangesAsync();

                var existingModel = await _workflowTableService.FirstOrDefaultAsync(m => m.Id == model.Id);

                var cashAdvanceLines =  _lineMapper.ToModels(viewModel.CashAdvanceLines, model.CashAdvanceLine).ToListSafe();
                foreach (var line in cashAdvanceLines)
                {
                    if (line.Id <= 0 || line.ParentId <= 0)
                    {
                        if (model.Id <= 0)
                        {
                            line.CashAdvanceTable = model;
                        }
                        else
                        {
                            line.ParentId = existingModel.Id;
                        }
                        _lineService.Insert(line);
                    }
                    else
                    {
                        if (line.Deleted)
                        {
                            _lineService.Delete(line);
                        }
                        else
                        {
                            _lineService.Update(line, line.Id);
                        }
                    }
                }

                await Session.SaveChangesAsync();

                var approval = _approveMapper.ToModels(viewModel.CashAdvanceApproval, model.CashAdvanceApproval).ToListSafe();
                foreach (var approve in approval)
                {
                    if (approve.Id <= 0 || approve.ParentId <= 0)
                    {
                        if (model.Id <= 0)
                        {
                            approve.CashAdvanceTable = model;
                        }
                        else
                        {
                            approve.ParentId = model.Id;
                        }
                        _approvalService.Insert(approve);
                    }
                    else
                    {
                        if (approve.Deleted)
                        {
                            _approvalService.Delete(approve);
                        }
                        else
                        {
                            _approvalService.Update(approve, approve.Id);
                        }
                    }
                }

                await Session.SaveChangesAsync();
                return _mapper.ToViewModel(model);
            }
            catch (Exception ex)
            {
                throw new HttpException(ex.Message);
            }
        }

        public IEnumerable<CashAdvanceIndexDataViewModel> GetIndexData(string username)
        {
            var models = _workflowTableService.GetWhere(m => !m.Deleted 
                                                             && (m.CreatedBy.Equals(username, StringComparison.InvariantCultureIgnoreCase) 
                                                                 || m.RequestForUserName.Equals(username, StringComparison.InvariantCultureIgnoreCase)));
            var viewModels = _mapper.ToViewModelsIndexData(models);
            return viewModels;
        }

        public IEnumerable<CashAdvanceIndexDataViewModel> GetAccIndex(string username) // เฉพาะสาขาตัวเอง
        {
            var account = _accountingService.GetWhere(r => r.Username.Equals(username, StringComparison.InvariantCultureIgnoreCase) && !r.InActive);
            var models = _workflowTableService.GetWhere(m => !m.Deleted && m.Status == DocumentStatus.Completed.ToString() && m.AccountingBranch != null);
            var accModel = new List<MPS_CashAdvanceTable>();

            foreach (var dataAcc in account)
            {
                foreach (var dataModels in models)
                {
                    if (dataAcc.BranchCode == dataModels.AccountingBranch)
                    {
                        accModel.Add(dataModels);
                    }
                }
            }

            var viewModels = _mapper.ToViewModelsIndexData(accModel);
            return viewModels;
        }

        public IEnumerable<ProposalLineViewModel> GetProposalLineData(string unitCode, int parentId)
        {
            var model = _proposalLineService.GetWhere(m => m.UnitCode.Equals(unitCode) && m.ParentId == parentId);
            var viewModel = _proposalLineMapper.ToViewModelsOnlyExpenseAndAp(model);
            return viewModel;
        }

        public ProposalInfoViewModel GetProposalOneLineFromId(int id)
        {
            var linemodel = _proposalLineService.FirstOrDefault(m => m.Id == id);
            //var lineViewModel = _lineMapper.ToViewModel(linemodel);

            var model = new MPS_ProposalTable();

            if (linemodel != null)
            {
                model = linemodel.ProposalTable;
            }

            var listviewModel = new List<ProposalLineViewModel>();
            listviewModel.Add(_proposalLineMapper.ToViewModel(linemodel));

            var viewModel = _proposalMapper.ToProposalInfoViewModel(model);
            viewModel.ProposalLine = listviewModel;

            return viewModel;

        }

        public CashAdvanceViewModel GetViewModelFromProposalLineId(int id)
        {
            var cashAdvanceModel = _workflowTableService.FirstOrDefault(m => m.ProposalLineID== id);
            if (cashAdvanceModel != null)
            {
                return _mapper.ToViewModel(cashAdvanceModel);
            }
            else
            {
                ProposalInfoViewModel proposalInfo = GetProposalOneLineFromId(id);
                return _mapper.ToviewModelOnlyProposalInfo(proposalInfo);
            }
            
        }

        public async Task<CashAdvanceViewModel> GetDataBindAdvance(int propLineid)
        {
            var viewModel = new CashAdvanceViewModel();
            if (propLineid != 0)
            {
                var propLine = _proposalLineService.Find(m=>m.Id==propLineid);
                if (propLine != null)
                {
                    var proposalRef = propLine.ProposalTable;
                    var propLineViewModel = _proposalLineMapper.ToViewModel(propLine);;
                    viewModel.BudgetPlan = propLineViewModel.BudgetPlan;
                    viewModel.ProposalRef = _proposalMapper.ToProposalInfoViewModel(proposalRef);
                    viewModel.UnitCode = propLineViewModel.UnitCode;
                    viewModel.APCode = viewModel.ProposalRef.APCode.LastOrDefault(m => m.ProposalLineId == propLineid);
                    viewModel.ExpenseTopics = propLineViewModel.ExpenseTopic;
                    viewModel.ProposalLineId = propLineid;
                    viewModel.CashAdvanceLines = new List<CashAdvanceLineViewModel>();
                    viewModel.CashAdvanceApproval = new List<CashAdvanceApprovalViewModel>();
                }
            }

            return viewModel;
        }
        public CashAdvanceViewModel GetViewModelFromDocumentNumber(string documentNumber)
        {
            var model =  _workflowTableService.FirstOrDefault(m => m.DocumentNumber.Equals(documentNumber, StringComparison.InvariantCultureIgnoreCase));
            var viewModel = _mapper.ToViewModel(model);
            return viewModel;
        }

        public override  IEnumerable<ValidationResult> ValidateStartProcess(MPS_CashAdvanceTable workflowTable)
        {
            if (workflowTable.ProposalLine.ProposalTable.CloseFlag == true)
            {
                yield return new ValidationResult("Cannot submit because your proposal was closed!!!");
            }

            if (string.IsNullOrEmpty(workflowTable.AccountingBranch))
            {
                yield return new ValidationResult("Please select accounting!!!");

            }

            //var budget = _budgetProposalTransService.GetBudgetTransTotal(workflowTable.ProposalLineID);
            var budget = _budgetProposalTransService.GetBudgetTransTotal(workflowTable.ProposalLine.ParentId, workflowTable.ProposalLine.APCode, workflowTable.ProposalLine.UnitCode);
           // var balance = workflowTable.ProposalLine.BudgetPlan - budget;
            var propBudgetPlan = _budgetPlanService.GetProposalBudgetPlan(workflowTable.ProposalLine.ParentId, workflowTable.ProposalLine.APCode, workflowTable.ProposalLine.UnitCode);

            var balance = (propBudgetPlan != null) ? propBudgetPlan.BudgetPlan - budget : budget;
            if (balance < workflowTable.Budget)
            {
                yield return new ValidationResult("Budget exceed: Statements remainning " + balance + " baht");
            }

            if (workflowTable.AccountingBranch == null)//string.IsNullOrEmpty(workflowTable.AccountingUsername))
            {
                yield return new ValidationResult("Please select accounting!!!");
            }

            //if (!workflowTable.CashAdvanceApproval.Any(m => m.ApproverLevel.Equals(Approval.ApproveLevel.Propose) && !m.Deleted))
            //{
            //    yield return new ValidationResult("Please select approver level propose!!!");

            //}
            //if (!workflowTable.CashAdvanceApproval.Any(m => m.ApproverLevel.Equals(Approval.ApproveLevel.Accept) && !m.Deleted))
            //{
            //    yield return new ValidationResult("Please select approver level accept!!!");

            //}
            //if (!workflowTable.CashAdvanceApproval.Any(m => m.ApproverLevel.Equals(Approval.ApproveLevel.Approval) && !m.Deleted))
            //{
            //    yield return new ValidationResult("Please select a approver!!!");

            //}

            if (!workflowTable.CashAdvanceApproval.Any(m => m.ApproverLevel.Equals(Approval.ApproveLevel.FinalApproval) && !m.Deleted && m.LockEditable))
            {
                yield return new ValidationResult("Please select a final apporver!!!");
            }

            if (workflowTable.Budget <= 0)
            {
                yield return new ValidationResult("Butget must not be 0");
            }
        }

        public override IEnumerable<ValidationResult> ValidateSubmit(MPS_CashAdvanceTable workflowTable, WorklistItem worklistItem, string action,
            CashAdvanceViewModel viewModel)
        {
            if (action.ToUpper().Equals("RESUBMIT"))
            {
                 var budgetTrans = _budgetProposalTransService.LastOrDefault(m => m.ProposalLineId == workflowTable.ProposalLine.Id
                                                                                    &&
                                                                                    m.DocumentType ==
                                                                                    Budget.DocumentType.CashAdvance
                                                                                    &&
                                                                                    m.DocumentNo ==
                                                                                    workflowTable.DocumentNumber
                                                                                    && m.Status == Budget.BudgetStatus.Reserve
                                                                                    && !m.InActive);
                if (budgetTrans == null)
                    yield return new ValidationResult(string.Format(Resources.Error.BudgetTransNotFound, workflowTable.DocumentNumber));

                var budgetPlan = _budgetPlanService.GetProposalBudgetPlan(workflowTable.ProposalLine.ParentId, workflowTable.ProposalLine.APCode, workflowTable.ProposalLine.UnitCode);
                if (budgetPlan == null)
                    yield return new ValidationResult(string.Format(Resources.Error.ProposalBudgetPlanNotFound, workflowTable.ProposalLine.ProposalTable.DocumentNumber, workflowTable.ProposalLine.APCode, workflowTable.ProposalLine.UnitCode));

                var budgetTransTotal = _budgetProposalTransService.GetBudgetTransTotal(workflowTable.ProposalLine.ParentId, workflowTable.ProposalLine.APCode, workflowTable.ProposalLine.UnitCode);

                var budgetBalance = (budgetPlan.BudgetPlan - budgetTransTotal + budgetTrans.Amount);

                if (budgetBalance < viewModel.Budget)
                {
                    yield return new ValidationResult("Budget exceed: Statements remainning " + budgetBalance
                                                      + " baht");
                }
                //var budget = _budgetProposalTransService.GetBudgetDetailAp(workflowTable.ProposalLineID, workflowTable.Id);
                //var balance = workflowTable.ProposalLine.BudgetPlan - budget;
                //if (balance < workflowTable.Budget)
                //{
                //    yield return new ValidationResult("Budget exceed: Statements remainning " + balance
                //                                      + " baht");
                //}
         
                //if (string.IsNullOrEmpty(workflowTable.AccountingUsername))

                if (viewModel.ProposalRef.CloseFlag == true)
                {
                    yield return new ValidationResult("Cannot submit because your proposal was closed!!!");
                }

                if (viewModel.Accounting == null || string.IsNullOrEmpty(viewModel.Accounting.PlaceCode))
                { 
                    yield return new ValidationResult("Please select accounting!!!");
                }
                  
                //if (!viewModel.CashAdvanceApproval.Any(
                //        m => m.ApproverLevel.Equals(Approval.ApproveLevel.Propose) && !m.Deleted))
                //{
                //    yield return new ValidationResult("Please select approver level propose!!!");
                //}
                //if (
                //    !viewModel.CashAdvanceApproval.Any(
                //        m => m.ApproverLevel.Equals(Approval.ApproveLevel.Accept) && !m.Deleted))
                //{
                //    yield return new ValidationResult("Please select approver level accept!!!");
                //}
                //if (!viewModel.CashAdvanceApproval.Any(
                //        m => m.ApproverLevel.Equals(Approval.ApproveLevel.Approval) && !m.Deleted))
                //{
                //    yield return new ValidationResult("Please select a approver!!!");
                //}

                if (!viewModel.CashAdvanceApproval.Any(
                        m => m.ApproverLevel.Equals(Approval.ApproveLevel.FinalApproval) && !m.Deleted && m.LockEditable))
                {
                    yield return new ValidationResult("Please select a final apporver!!!");
                }

                if (viewModel.Budget <= 0)
                {
                    yield return new ValidationResult("Butget must not be 0");
                }

                //if (viewModel.Budget > viewModel.BudgetPlan)
                //{
                //    yield return new ValidationResult("The budget sum value must not be more than the butget plan!!!");
                //}
            }
        }

        public override async Task<MPS_CashAdvanceComment> Submit(string sn, string action, string comment, CashAdvanceViewModel viewModel)
        {
          var result = await base.Submit(sn, action, comment, viewModel);
            if (result != null)
            {
                if (result.Action.ToUpper().Equals("RESUBMIT", StringComparison.CurrentCultureIgnoreCase)
              )
                {
                    _budgetProposalTransFacade.ChangeBudgetPropTrans(result.ParentId,"cashadvance");
                    var approval = _approvalService.GetWhere(m => m.ParentId == viewModel.Id && !m.Deleted);
                    foreach (var a in approval)
                    {
                        a.ApproveStatus = false;
                        _approvalService.Update(a, a.Id);
                    }
                    await Session.SaveChangesAsync();
                }
                else
                {
                    var activity = result.Activity.Replace("Approval", "").Replace("-", "").Replace(" ", "");
                    var modelApprove1 = _approvalService.GetWhere(m => activity.Equals(m.ApproverLevel) && m.ParentId == result.ParentId).ToList();

                    var modelApprove = modelApprove1.LastOrDefault(m => m.ApproverUserName.ToLower().Equals(result.CreatedBy.ToLower()));
                    if (modelApprove != null)
                    {
                        modelApprove.ApproveStatus = true;
                        _approvalService.Update(modelApprove, modelApprove.Id);
                        Session.SaveChanges();
                    }
                }
            }

            return result;
        }

        public override async Task<bool> UpdateAfterStartProcess(MPS_CashAdvanceTable workflowTable, int? procInstId)
        { 
            var result = await base.UpdateAfterStartProcess(workflowTable, procInstId);
             _budgetProposalTransFacade.CreateBudgetPropTrans(workflowTable.Id, "cashadvance");
            return result;
        }

        public void SetFinalApproverDeleted(int id)
        {
            try
            {
                var finalApprovers =
                    _approvalService.GetWhere(m => m.ParentId == id && m.ApproverLevel == "อนุมัติขั้นสุดท้าย");
                foreach (var finalApprover in finalApprovers)
                {
                    _approvalService.Delete(finalApprover);
                }
                Session.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new HttpException(ex.Message);
            }
        }

        public string GetEmailInformAll(int workflowTableId)
        {
            string email = string.Empty;
            var model = _workflowTableService.Find(m => m.Id == workflowTableId);
            if (model != null)
            {
                email += string.IsNullOrEmpty(email) ? model.CCEmail : string.Format(";{0}", model.CCEmail).TrimEnd(';');
                email += string.IsNullOrEmpty(email) ? model.InformEmail : string.Format(";{0}", model.InformEmail).TrimEnd(';');
                email += string.IsNullOrEmpty(email) ? model.AccountingEmail : string.Format(";{0}", model.AccountingEmail).TrimEnd(';');

                if (model.CashAdvanceApproval != null)
                {
                    foreach (var userapprove in model.CashAdvanceApproval)
                    {
                        if (!string.IsNullOrEmpty(userapprove.ApproverEmail))
                        {
                            email += string.IsNullOrEmpty(email)
                                   ? userapprove.ApproverEmail
                                   : string.Format(";{0}", userapprove.ApproverEmail).TrimEnd(';');
                        }
                        else
                        {
                            var employee = _employeeTableService.FindByUsername(userapprove.ApproverUserName);
                            if (employee != null && !string.IsNullOrEmpty(employee.Email))
                            {
                                email += string.IsNullOrEmpty(email)
                                    ? employee.Email
                                    : string.Format(";{0}", employee.Email).TrimEnd(';');

                            }

                        }
                    }
                }
            }
            if (!string.IsNullOrEmpty(email))
            {
                string[] listEmail = email.TrimEnd(';').Split(';');
                string[] newlistEmail = listEmail.Distinct().ToArray();
                email = newlistEmail.Aggregate(string.Empty, (current, mail) => current + (string.IsNullOrEmpty(current) ? mail : string.Format(";{0}", mail)));
            }

            return email;
        }

        public CashAdvanceViewModel DeleteBy(int id, string username)
        {
            try
            {
                if (id > 0)
                {
                    var model = _workflowTableService.FirstOrDefault(m => m.Id == id);
                    var employeeViewModel = _employeeTableService.FindByUsername(username).ToViewModel();
                    if (model != null && employeeViewModel != null)
                    {
                        if (model.StatusFlag == 0)
                        {
                            var modelDelete = _mapper.DeleteBy(model, employeeViewModel);
                            if (modelDelete != null)
                            {
                                modelDelete = _workflowTableService.Update(modelDelete);
                                var value = Session.SaveChanges();
                                if (value > 0)
                                {
                                    var viewModel = _mapper.ToViewModel(modelDelete);
                                    return viewModel;
                                }
                            }
                        }
                        else if (model.StatusFlag == 9)
                        {
                            model.StatusFlag = 5;
                            model.CancelledDate = DateTime.Now.ToLocalTime();
                            model.Status = DocumentStatusFlag.Cancelled.ToString();
                            model.DocumentStatus = DocumentStatusFlag.Cancelled.ToString();
                            var cancelModel = _workflowTableService.Update(model, model.Id);
                            var result = _budgetProposalTransFacade.CancelBudgetPropTrans(model.Id, "cashadvance");
                            if (result)
                            {
                                Session.SaveChanges();
                            }
                            else
                            {
                                throw new Exception("ไม่สามารถลบรายการได้ กรุณาติดต่อผู้ดูแลระบบ");
                            }
                            return _mapper.ToViewModel(cancelModel);
                        }
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                throw new HttpException(ex.Message);
            }
        }

    }
}