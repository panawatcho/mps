﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using CrossingSoft.Framework.Infrastructure.Session;
using TheMall.MPS.API.Facades.Abstracts;
using TheMall.MPS.API.Mappers;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.API.Services;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Models.Master;
using TheMall.MPS.ViewModels.Master;

namespace TheMall.MPS.API.Facades
{
    public interface IBudgetYearPlanFacade : IBaseMasterFacade<MPS_M_BudgetYearPlan, BudgetYearPlanViewModel>
    {
        BudgetYearPlanViewModel SearchByYearAndUnitcode(string year = null, string unitcode = null);

    }

    public class BudgetYearPlanFacade : BaseMasterFacade<MPS_M_BudgetYearPlan, BudgetYearPlanViewModel>, IBudgetYearPlanFacade
    {
        private readonly IMPSSession _session;
        private readonly IBudgetYearPlanService _service;
        private readonly IUnitCodeService _unitCodeServise;
        public BudgetYearPlanFacade(IMPSSession session, IBudgetYearPlanService service, 
            IUnitCodeService unitCodeServise) 
            : base(session, service)
        {
            _session = session;
            _service = service;
            _unitCodeServise = unitCodeServise;
        }

        public override async Task<BudgetYearPlanViewModel> Create(BudgetYearPlanViewModel viewModel)
        {
            try
            {
                var model = _service.Insert(viewModel.ToModel());
                await _session.SaveChangesAsync();
                return  model.ToViewModel();
            }
            catch (Exception exception)
            {

                throw new Exception("Unit code and Year are already exit!!!");
            }
        }

        public override async Task<BudgetYearPlanViewModel> Update(BudgetYearPlanViewModel viewModel)
        {
            var model = _service.Update(viewModel.ToModel());
            await _session.SaveChangesAsync();
            return model.ToViewModel();
        }

        public override async Task<BudgetYearPlanViewModel> Delete(BudgetYearPlanViewModel viewModel)
        {
            try
            {
                _service.Delete(viewModel.ToModel());
                await _session.SaveChangesAsync();
                return viewModel;
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
        }

        public BudgetYearPlanViewModel SearchByYearAndUnitcode(string year = null, string unitcode = null)
        {
            try
            {
                var viewModel = _service.FirstOrDefault(m => m.Year.Equals(year, StringComparison.CurrentCultureIgnoreCase)
                            && m.UnitCode.Equals(unitcode, StringComparison.CurrentCultureIgnoreCase)).ToViewModel();
                if (viewModel != null)
                {
                    var unitCodeViewModel = _unitCodeServise.FirstOrDefault(
                                            m => m.UnitCode.Equals(viewModel.UnitCode, StringComparison.CurrentCultureIgnoreCase)).ToViewModel();
                    if (unitCodeViewModel != null)
                    {
                        viewModel.UnitCodeViewModel = unitCodeViewModel;
                    }
                    var yearTime = "01/01/" + viewModel.Year;
                    viewModel.YearTime = Convert.ToDateTime(yearTime);
                    return viewModel;
                }
                return null;
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
        }

    }
}