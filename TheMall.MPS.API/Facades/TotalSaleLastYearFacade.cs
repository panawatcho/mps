﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using TheMall.MPS.API.Facades.Abstracts;
using TheMall.MPS.API.Mappers;
using TheMall.MPS.API.Services;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Models;
using TheMall.MPS.ViewModels.Master;

namespace TheMall.MPS.API.Facades
{
    public interface ITotalSaleLastYearFacade : IBaseMasterFacade<MPS_M_TotalSaleLastYear, TotalSaleLastYearViewModel>
    {
        TotalSaleLastYearViewModel Get(string branchCode = null, string year = null);
    }
    public class TotalSaleLastYearFacade : BaseMasterFacade<MPS_M_TotalSaleLastYear, TotalSaleLastYearViewModel>, ITotalSaleLastYearFacade
    {
        private readonly ITotalSaleLastYearMasterService _service;
        private readonly IMPSSession _session;
        public TotalSaleLastYearFacade(
            ITotalSaleLastYearMasterService service, IMPSSession session) :
            base(session, service)
        {
            _service = service;
            _session = session;
        }

        public override async Task<TotalSaleLastYearViewModel> Create(TotalSaleLastYearViewModel viewModel)
        {
            try
            {
                var model = _service.Insert(viewModel.ToModel());
                await _session.SaveChangesAsync();
                return model.ToViewModel();
            }
            catch (Exception exception)
            {

                throw new Exception("Cannot insert duplicate branch and year!!!");
            }
        }

        public override async Task<TotalSaleLastYearViewModel> Update(TotalSaleLastYearViewModel viewModel)
        {
            try
            {
                var model = _service.Update(viewModel.ToModel());
                await _session.SaveChangesAsync();
                return model.ToViewModel();
            }
            catch (Exception exception)
            {

                throw new Exception(exception.Message);
            }
        }

        public override async Task<TotalSaleLastYearViewModel> Delete(TotalSaleLastYearViewModel viewModel)
        {
            try
            {
                _service.Delete(viewModel.ToModel());
                await _session.SaveChangesAsync();
                return viewModel;
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
        }

        public TotalSaleLastYearViewModel Get(string branchCode = null, string year = null)
        {
            var model =
                _service.FirstOrDefault(
                    m => m.BranchCode.Equals(branchCode, StringComparison.InvariantCultureIgnoreCase) &&
                         m.Year.Equals(year, StringComparison.InvariantCultureIgnoreCase));
            if (model != null)
            {
                var viewModel = model.ToViewModel();
                var yearTime = "01/01/" + viewModel.Year;
                viewModel.YearTime = Convert.ToDateTime(yearTime);
                return viewModel;
            }
            else
            {
                return null;
            }
        }
    }
}