﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Routing;
using CrossingSoft.Framework.Infrastructure.EF6;
using CrossingSoft.Framework.Infrastructure.K2;
using TheMall.MPS.API.Facades.Abstracts;
using TheMall.MPS.API.Mappers;
using TheMall.MPS.API.Services;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Models.CashAdvance;
using TheMall.MPS.Models.CashClearing;
using TheMall.MPS.Models.Memo;
using TheMall.MPS.Models.MemoIncome;
using TheMall.MPS.Models.PettyCash;
using TheMall.MPS.Models.Proposal;
using TheMall.MPS.ViewModels.Proposal;

namespace TheMall.MPS.API.Facades
  { 
    public interface ISumitWorkflowFacade

    {
        bool SubmitOnLink(string process, string sn, string action);
    }

    public class SumitWorkflowFacade : BaseFacade , ISumitWorkflowFacade
    {
        private readonly IMPSSession _session;
        private readonly IWorkflowRepository _workflowRepository;
        private readonly IProposalFacade _proposalFacade;
        private readonly IProposalService _proposalService;
        private readonly IProposalApprovalService _proposalApproveService;
        private readonly ICommentService<MPS_ProposalComment> _proposalCommentService;
        private readonly ICommentService<MPS_MemoComment> _memoCommentService;
        private readonly ICommentService<MPS_PettyCashComment> _pettyCashCommentService;
        private readonly ICommentService<MPS_CashAdvanceComment> _cashAdvanceCommentService;
        private readonly ICommentService<MPS_CashClearingComment> _cashClearingCommentService; 
        private readonly IMemoFacade _memoFacade;
        private readonly IMemoService _memoService;
        private readonly IMemoApprovalService _memoApproveService;
        private readonly IPettyCashFacade _pettyCashFacade;
        private readonly IPettyCashService _pettyCashService;
        private readonly IPettyCashApprovalService _pettyCashApproveService;
        private readonly ICashAdvanceFacade _cashAdvanceFacade;
        private readonly ICashAdvanceService _cashAdvanceService;
        private readonly ICashAdvanceApproveService _cashAdvanceApproveService;
        private readonly ICashAdvanceFacade _cashClearingFacade;
        private readonly ICashClearingService _cashClearingService;
        private readonly ICashClearingApprovalService _cashClearingApproveService;
        private readonly IMemoIncomeService _memoIncomeService;
        private readonly IMemoIncomeFacade _memoIncomeFacede;
        private readonly IMemoIncomeApprovalService _memoIncomeApprovalService;
        private readonly ICommentService<MPS_MemoIncomeComment> _memoIncomeCommentService;

        public SumitWorkflowFacade(IMPSSession session,
            IProposalFacade proposalFacade, IProposalService proposalService, IProposalMapper proposalMapper,
            IMemoFacade memoFacade, 
            IPettyCashFacade pettyCashFacade, 
            ICashAdvanceFacade cashAdvanceFacade, 
            ICashAdvanceFacade cashClearingFacade,
            ICommentService<MPS_ProposalComment> proposalCommentService,
            IProposalApprovalService proposalApproveService, 
            IWorkflowRepository workflowRepository, 
            ICommentService<MPS_MemoComment> memoCommentService, 
            ICommentService<MPS_PettyCashComment> pettyCashCommentService,
            ICommentService<MPS_CashAdvanceComment> cashAdvanceCommentService,
            ICommentService<MPS_CashClearingComment> cashClearingCommentService,
            IMemoService memoService, IMemoApprovalService memoApproveService,
            IPettyCashService pettyCashService,
            IPettyCashApprovalService pettyCashApproveService, 
            ICashAdvanceService cashAdvanceService, 
            ICashAdvanceApproveService cashAdvanceApproveService, 
            ICashClearingService cashClearingService, 
            ICashClearingApprovalService cashClearingApproveService,
            IMemoIncomeService memoIncomeService,
            IMemoIncomeFacade memoIncomeFacede,
            IMemoIncomeApprovalService memoIncomeApprovalService,
            ICommentService<MPS_MemoIncomeComment> memoIncomeCommentService)
        {
             _session = session;
            _proposalFacade = proposalFacade;
            _proposalService = proposalService;
            _memoFacade = memoFacade;
            _pettyCashFacade = pettyCashFacade;
            _cashAdvanceFacade = cashAdvanceFacade;
            _cashClearingFacade = cashClearingFacade;
            _proposalCommentService = proposalCommentService;
            _proposalApproveService = proposalApproveService;
            _workflowRepository = workflowRepository;
            _memoCommentService = memoCommentService;
            _pettyCashCommentService = pettyCashCommentService;
            _cashAdvanceCommentService = cashAdvanceCommentService;
            _cashClearingCommentService = cashClearingCommentService;
            _memoService = memoService;
            _memoApproveService = memoApproveService;
            _pettyCashService = pettyCashService;
            _pettyCashApproveService = pettyCashApproveService;
            _cashAdvanceService = cashAdvanceService;
            _cashAdvanceApproveService = cashAdvanceApproveService;
            _cashClearingService = cashClearingService;
            _cashClearingApproveService = cashClearingApproveService;
            _memoIncomeService = memoIncomeService;
            _memoIncomeFacede = memoIncomeFacede;
            _memoIncomeApprovalService = memoIncomeApprovalService;
            _memoIncomeCommentService = memoIncomeCommentService;
        }

        public bool SubmitOnLink(string process,string sn , string action)
        {
            var result = false;
            if (!string.IsNullOrEmpty(sn) && !string.IsNullOrEmpty(action) && !string.IsNullOrEmpty(process))
                switch (process.ToLower())
            {
                case "proposal":
                {
                    var proposalId = _proposalService.GetWorkflowTableIdFromSN(sn);
                    if (proposalId != null)
                    {
                        var worklistItem = _proposalFacade.GetWorklistItem(sn);
                        var commentTable = _proposalCommentService.CreateCommentSubmit((int)proposalId,
                         worklistItem.ProcessInstance.ID,
                         worklistItem.ProcessInstance.Name,
                         worklistItem.ActivityInstanceDestination.Description,
                         sn,
                         action,
                         "Approve by email");
                        if (commentTable != null)
                        {
                            var activity = commentTable.Activity.Replace("Approval", "").Replace("-", "").Replace(" ", "");
                            var modelApprove1 = _proposalApproveService.GetWhere(m => activity.Equals(m.ApproverLevel)
                            && m.ParentId == commentTable.ParentId).ToList();

                            var modelApprove = modelApprove1.LastOrDefault(m => m.ApproverUserName.ToLower().Equals(commentTable.CreatedBy.ToLower()));
                            if (modelApprove != null)
                            {
                                modelApprove.ApproveStatus = true;
                                _proposalApproveService.Update(modelApprove, modelApprove.Id);
                                _session.SaveChanges();
                            }
                            _workflowRepository.ActionWorklistItem(sn, action, false);
                            _session.SaveChanges();
                            result = true;
                        }
                    }
                }
                break;
                case "memo":
                    {
                        var memoId = _memoService.GetWorkflowTableIdFromSN(sn);
                        if (memoId != null)
                        {
                            var worklistItem = _memoFacade.GetWorklistItem(sn);
                            var commentTable = _memoCommentService.CreateCommentSubmit((int)memoId,
                             worklistItem.ProcessInstance.ID,
                             worklistItem.ProcessInstance.Name,
                             worklistItem.ActivityInstanceDestination.Description,
                             sn,
                             action,
                            "Approve by email");
                            if (commentTable != null)
                            {
                                var activity = commentTable.Activity.Replace("Approval", "").Replace("-", "").Replace(" ", "");
                                var listApprove = _memoApproveService.GetWhere(m => activity.Equals(m.ApproverLevel)
                                && m.ParentId == commentTable.ParentId).ToList();

                                var modelApprove = listApprove.LastOrDefault(m => m.ApproverUserName.ToLower().Equals(commentTable.CreatedBy.ToLower()));
                                if (modelApprove != null)
                                {
                                    modelApprove.ApproveStatus = true;
                                    _memoApproveService.Update(modelApprove, modelApprove.Id);
                                    _session.SaveChanges();
                                }
                                _workflowRepository.ActionWorklistItem(sn, action, false);
                                _session.SaveChanges();
                                result = true;
                            }
                        }
                    }
                    break;
                case "pettycash":
                    {
                        var pettyCashId = _pettyCashService.GetWorkflowTableIdFromSN(sn);
                        if (pettyCashId != null)
                        {
                            var worklistItem = _pettyCashFacade.GetWorklistItem(sn);
                            var commentTable = _pettyCashCommentService.CreateCommentSubmit((int)pettyCashId,
                             worklistItem.ProcessInstance.ID,
                             worklistItem.ProcessInstance.Name,
                             worklistItem.ActivityInstanceDestination.Description,
                             sn,
                             action,
                             "Approve by email");
                            if (commentTable != null)
                            {
                                var activity = commentTable.Activity.Replace("Approval", "").Replace("-", "").Replace(" ", "");
                                var listApprove = _pettyCashApproveService.GetWhere(m => activity.Equals(m.ApproverLevel)
                                && m.ParentId == commentTable.ParentId).ToList();

                                var modelApprove = listApprove.LastOrDefault(m => m.ApproverUserName.ToLower().Equals(commentTable.CreatedBy.ToLower()));
                                if (modelApprove != null)
                                {
                                    modelApprove.ApproveStatus = true;
                                    _pettyCashApproveService.Update(modelApprove, modelApprove.Id);
                                    _session.SaveChanges();
                                }
                                _workflowRepository.ActionWorklistItem(sn, action, false);
                                _session.SaveChanges();
                                result = true;
                            }
                        }
                    }
                    break;
                case "cashadvance":
                    {
                        var advanceId = _cashAdvanceService.GetWorkflowTableIdFromSN(sn);
                        if (advanceId != null)
                        {
                            var worklistItem = _cashAdvanceFacade.GetWorklistItem(sn);
                            var commentTable = _cashAdvanceCommentService.CreateCommentSubmit((int)advanceId,
                             worklistItem.ProcessInstance.ID,
                             worklistItem.ProcessInstance.Name,
                             worklistItem.ActivityInstanceDestination.Description,
                             sn,
                             action,
                            "Approve by email");
                            if (commentTable != null)
                            {
                                var activity = commentTable.Activity.Replace("Approval", "").Replace("-", "").Replace(" ", "");
                                var listApprove = _cashAdvanceApproveService.GetWhere(m => activity.Equals(m.ApproverLevel)
                                && m.ParentId == commentTable.ParentId).ToList();

                                var modelApprove = listApprove.LastOrDefault(m => m.ApproverUserName.ToLower().Equals(commentTable.CreatedBy.ToLower()));
                                if (modelApprove != null)
                                {
                                    modelApprove.ApproveStatus = true;
                                    _cashAdvanceApproveService.Update(modelApprove, modelApprove.Id);
                                    _session.SaveChanges();
                                }
                                _workflowRepository.ActionWorklistItem(sn, action, false);
                                _session.SaveChanges();
                                result = true;
                            }
                        }
                    }
                    break;
                case "cashaclearing":
                    {
                        var clearingId = _cashClearingService.GetWorkflowTableIdFromSN(sn);
                        if (clearingId != null)
                        {
                            var worklistItem = _cashClearingFacade.GetWorklistItem(sn);
                            var commentTable = _cashClearingCommentService.CreateCommentSubmit((int)clearingId,
                             worklistItem.ProcessInstance.ID,
                             worklistItem.ProcessInstance.Name,
                             worklistItem.ActivityInstanceDestination.Description,
                             sn,
                             action,
                             "Approve by email");
                            if (commentTable != null)
                            {
                                var activity = commentTable.Activity.Replace("Approval", "").Replace("-", "").Replace(" ", "");
                                var listApprove = _cashClearingApproveService.GetWhere(m => activity.Equals(m.ApproverLevel)
                                && m.ParentId == commentTable.ParentId).ToList();

                                var modelApprove = listApprove.LastOrDefault(m => m.ApproverUserName.ToLower().Equals(commentTable.CreatedBy.ToLower()));
                                if (modelApprove != null)
                                {
                                    modelApprove.ApproveStatus = true;
                                    _cashClearingApproveService.Update(modelApprove, modelApprove.Id);
                                    _session.SaveChanges();
                                }
                                _workflowRepository.ActionWorklistItem(sn, action, false);
                                _session.SaveChanges();
                                result = true;
                            }
                        }
                    }
                    break;
                    case "memoincome":
                        {
                            var memoincomeId = _memoIncomeService.GetWorkflowTableIdFromSN(sn);
                            if (memoincomeId != null)
                            {
                                var worklistItem = _memoIncomeFacede.GetWorklistItem(sn);
                                var commentTable = _memoIncomeCommentService.CreateCommentSubmit((int)memoincomeId,
                                 worklistItem.ProcessInstance.ID,
                                 worklistItem.ProcessInstance.Name,
                                 worklistItem.ActivityInstanceDestination.Description,
                                 sn,
                                 action,
                                "Approve by email");
                                if (commentTable != null)
                                {
                                    var activity = commentTable.Activity.Replace("Approval", "").Replace("-", "").Replace(" ", "");
                                    var listApprove = _memoIncomeApprovalService.GetWhere(m => activity.Equals(m.ApproverLevel)
                                    && m.ParentId == commentTable.ParentId).ToList();

                                    var modelApprove = listApprove.LastOrDefault(m => m.ApproverUserName.ToLower().Equals(commentTable.CreatedBy.ToLower()));
                                    if (modelApprove != null)
                                    {
                                        modelApprove.ApproveStatus = true;
                                        _memoIncomeApprovalService.Update(modelApprove, modelApprove.Id);
                                        _session.SaveChanges();
                                    }
                                    _workflowRepository.ActionWorklistItem(sn, action, false);
                                    _session.SaveChanges();
                                    result = true;
                                }
                            }
                        }
                        break;
                    default :
                    result = false;
                          break;
            }
            return result;
        }

       
        
    }
}