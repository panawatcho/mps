﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using TheMall.MPS.API.Facades.Abstracts;
using TheMall.MPS.API.Mappers;
using TheMall.MPS.API.Services;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Models.Master;
using TheMall.MPS.ViewModels.Master;

namespace TheMall.MPS.API.Facades
{
    public interface IUnitCodeForEmployeeFacade : IBaseMasterFacade<MPS_M_UnitCodeForEmployee, UnitCodeForEmployeeViewModel>
    {
        IEnumerable<UnitCodeForEmployeeViewModel> Search(string username = null, string unitCode = null);
        IEnumerable<UnitCodeForEmployeeViewModel> GetInMaster(string empId = null, string unitCode = null);
        Task<UnitCodeForEmployeeViewModel> SaveAsync(UnitCodeForEmployeeViewModel viewModel);
        int DeleteInMaster(string empId = null, string unitCode = null);
    }

    public class UnitCodeForEmployeeFacade : BaseMasterFacade<MPS_M_UnitCodeForEmployee, UnitCodeForEmployeeViewModel>,
        IUnitCodeForEmployeeFacade
    {
        private readonly IMPSSession _session;
        private readonly IUnitCodeForEmployeeMasterService _service;
        public UnitCodeForEmployeeFacade(IMPSSession session, IUnitCodeForEmployeeMasterService service) : 
            base(session, service)
        {
            _session = session;
            _service = service;
        }

        public override async Task<UnitCodeForEmployeeViewModel> Create(UnitCodeForEmployeeViewModel viewModel)
        {
            try
            {
                var model = _service.Insert(viewModel.ToModel());
                await _session.SaveChangesAsync();
                return model.ToViewModel();
            }
            catch (Exception exception)
            {
                throw new Exception("Cannot insert duplicate code");
            }
        }

        public override async Task<UnitCodeForEmployeeViewModel> Update(UnitCodeForEmployeeViewModel viewModel)
        {
            var model = _service.Update(viewModel.ToModel());
            await _session.SaveChangesAsync();
            return model.ToViewModel();
        }

        public override async Task<UnitCodeForEmployeeViewModel> Delete(UnitCodeForEmployeeViewModel viewModel)
        {
            try
            {
                _service.Delete(viewModel.ToModel());
                await _session.SaveChangesAsync();
                return viewModel;
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
        }

        //public IEnumerable<UnitCodeForEmployeeViewModel> Search(string username = null, string unitCode = null)
        //{
        //     var model = _service.Search(username, unitCode);
        //    return model.Result.ToViewModels();
        //}


        public IEnumerable<UnitCodeForEmployeeViewModel> Search(string username = null, string unitCode = null)
        {
            var model = _service.Search(username, unitCode);
            return model.Result.ToViewModels();
        }


        public IEnumerable<UnitCodeForEmployeeViewModel> GetInMaster(string empId = null, string unitCode = null)
        {
            var model = _service.GetInMaster(empId, unitCode);
            return model.Result.ToViewModels();
        }

        //
        public async Task<UnitCodeForEmployeeViewModel> SaveAsync(UnitCodeForEmployeeViewModel viewModel)
        {
            if (viewModel == null)
                throw new ArgumentNullException("viewModel");
            try
            {
                var model = viewModel.ToModel();
                if (string.IsNullOrEmpty(viewModel.EmpId))
                { 
                    _service.Insert(model);
                    await _session.SaveChangesAsync();
                    return model.ToViewModel();
                }
                else
                {
                    _service.Update(model);
                    await _session.SaveChangesAsync();
                    return model.ToViewModel();
                }
            }
            catch (Exception ex)
            {
                throw new HttpException(ex.Message);
            }
        }

        //
        public int DeleteInMaster(string empId = null, string unitCode = null)
        {
            try
            {
                var modelDeleted = _service.FirstOrDefault(m => m.EmpId == empId && m.UnitCode == unitCode);
                _service.Delete(modelDeleted);
                _session.SaveChanges();
                return 1;
            }
            catch (Exception ex)
            {
                throw new HttpException(ex.Message);
            }
        }
    }
}