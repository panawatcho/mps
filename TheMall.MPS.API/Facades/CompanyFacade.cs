﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TheMall.MPS.API.Mappers;
using TheMall.MPS.API.Services;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.ViewModels.Master;

namespace TheMall.MPS.API.Facades
{
    public interface ICompanyFacade
    {
        CompanyViewModel Create(CompanyViewModel company);
        CompanyViewModel Update(CompanyViewModel company);
        int DeleteInMaster(string branchCode = null);
    }

    public class CompanyFacade : ICompanyFacade
    {
        private readonly ICompanyService _companyService;
        private readonly IMPSSession _session;

        public CompanyFacade(IMPSSession session, 
            ICompanyService companyService)
        {
            _session = session;
            _companyService = companyService;
        }

        public CompanyViewModel Create(CompanyViewModel companyViewModel)
        {
            try
            {
                var companyModel = companyViewModel.ToModel();
                if (companyModel != null)
                {
                    _companyService.Insert(companyModel);
                    _session.SaveChanges();
                    return companyModel.ToViewModel();
                }

                return null;
            }
            catch (Exception ex)
            {
                throw new HttpException(ex.Message);
            }
        }

        public CompanyViewModel Update(CompanyViewModel companyViewModel)
        {
            try
            {
                if (!string.IsNullOrEmpty(companyViewModel.BranchCode))
                {
                    var companyModel = companyViewModel.ToModel();
                    if (companyModel != null)
                    {
                        _companyService.Update(companyModel);
                        _session.SaveChanges();
                        return companyModel.ToViewModel();
                    }

                    return null;
                }

                return null;
            }
            catch (Exception ex)
            {
                throw new HttpException(ex.Message);
            }
        }

        public int DeleteInMaster(string vendorCode = null)
        {
            try
            {
                var modelDeleted = _companyService.FirstOrDefault(m =>
                    m.VendorCode.Equals(vendorCode, StringComparison.InvariantCultureIgnoreCase));
                _companyService.Delete(modelDeleted);
                _session.SaveChanges();
                return 1;
            }
            catch (Exception ex)
            {
                throw new HttpException(ex.Message);
            }
        }
    }
}