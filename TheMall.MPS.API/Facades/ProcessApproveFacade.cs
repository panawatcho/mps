﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using TheMall.MPS.API.Facades.Abstracts;
using TheMall.MPS.API.Mappers;
using TheMall.MPS.API.Services;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Models.Master;
using TheMall.MPS.ViewModels.Master;

namespace TheMall.MPS.API.Facades
{
    public interface IProcessApproveFacade : IBaseMasterFacade<MPS_M_ProcessApprove, ProcessApproveViewModel>
    {
        ProcessApproveViewModel Search(string processCode = null);
    }
    public class ProcessApproveFacade : BaseMasterFacade<MPS_M_ProcessApprove, ProcessApproveViewModel>, IProcessApproveFacade
    {
        private readonly IMPSSession _session;
        private readonly IProcessApproveService _service;
        private readonly ITypeMemoService _typeMemoService;
        public ProcessApproveFacade(
            IMPSSession session,
            IProcessApproveService service, ITypeMemoService typeMemoService) : base(session, service)
        {
            _service = service;
            _typeMemoService = typeMemoService;
            _session = session;
        }

        public override async Task<ProcessApproveViewModel> Create(ProcessApproveViewModel viewModel)
        {
            try
            {
                var model = _service.Insert(viewModel.ToModel());
                await _session.SaveChangesAsync();
                return model.ToViewModel();
            }
            catch (Exception exception)
            {

                throw new Exception("This process code is already exit!!!");
            }
        }

        public override async Task<ProcessApproveViewModel> Update(ProcessApproveViewModel viewModel)
        {
            try
            {
                var model = _service.Update(viewModel.ToModel());
                await _session.SaveChangesAsync();
                return model.ToViewModel();
            }
            catch (Exception exception)
            {

                throw new Exception("Error, cannot update!!!");
            }
        }

        public override async Task<ProcessApproveViewModel> Delete(ProcessApproveViewModel viewModel)
        {
            try
            {
                _service.Delete(viewModel.ToModel());
                await _session.SaveChangesAsync();
                return viewModel;
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
        }

        public ProcessApproveViewModel Search(string processCode = null)
        {
            try
            {
                var viewModel =
                    _service.FirstOrDefault(
                        m => m.ProcessCode.Equals(processCode, StringComparison.InvariantCultureIgnoreCase)).ToViewModel();
                if (viewModel.ProcessType != null)
                {
                    viewModel.TypeMemo =  _typeMemoService.FirstOrDefault(
                        m => m.TypeMemoCode.Equals(viewModel.ProcessType, StringComparison.InvariantCultureIgnoreCase)).ToViewModel();
                }
                return viewModel;
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
        }
    }
}