﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using TheMall.MPS.API.Facades.Abstracts;
using TheMall.MPS.API.Mappers;
using TheMall.MPS.API.Services;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Models;
using TheMall.MPS.Models.Master;
using TheMall.MPS.ViewModels;
using TheMall.MPS.ViewModels.Master;

namespace TheMall.MPS.API.Facades
{
    public interface IEmployeeFacade : IBaseMasterFacade<MPS_M_EmployeeTable, EmployeeViewModel>
    {

        EmployeeViewModel GetUserViewModel(string username);
        EmployeeViewModel GetById(string empId);
        List<EmployeeViewModel> SearchRequestFor(string code = null, string name = null);
    }
    public class EmployeeFacade :  IEmployeeFacade
    {
        private readonly IEmployeeTableService _service;
        private readonly IUnitCodeForEmployeeService _unitCodeForEmployeeService;
        private readonly IMPSSession _session;

        public EmployeeFacade(
            IEmployeeTableService service, 
            IUnitCodeForEmployeeService unitCodeForEmployeeService, 
            IMPSSession session, 
            IMPSSession session1)
        {
            _service = service;
            _unitCodeForEmployeeService = unitCodeForEmployeeService;
            _session = session1;
        }

        public EmployeeViewModel GetUserViewModel(string username)
        {
            var requesterModel = _service.FindByUsername(username).ToViewModel();
            if (requesterModel == null)
            {
                return null;
            }
            var unitCodeForEmployeeModel = _unitCodeForEmployeeService.GetWhere(m => m.Username.Equals(username , StringComparison.InvariantCultureIgnoreCase) && m.InActive == false).ToViewModels();
            var unitCode = unitCodeForEmployeeModel.ToViewModelsFromEmployee();
            if (unitCode == null)
            {
                return null;
            }
            requesterModel.UnitCode = unitCode;
            return requesterModel;
        }

        public async Task<EmployeeViewModel> Create(EmployeeViewModel viewModel)
        {
            try
            {
                var model = _service.Insert(viewModel.ToModel());
                await _session.SaveChangesAsync();
                return model.ToViewModel();
            }
            catch (Exception exception)
            {

                throw new Exception("Cannot insert duplicate code");
            }
        }

        public async Task<EmployeeViewModel> Update(EmployeeViewModel viewModel)
        {
            var model = _service.Update(viewModel.ToModel());
            await _session.SaveChangesAsync();
            return model.ToViewModel();
        }

        public async Task<EmployeeViewModel> Delete(EmployeeViewModel viewModel)
        {
            try
            {
                _service.Delete(viewModel.ToModel());
                await _session.SaveChangesAsync();
                return viewModel;
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
        }

        public EmployeeViewModel GetById(string empId)
        {
            var requesterModel = _service.SearchById(empId).ToViewModel();
            if (requesterModel == null)
            {
                return null;
            }
            return requesterModel;
        }

        public List<EmployeeViewModel> SearchRequestFor(string code = null, string name = null)
        {
            var listRequester = new List<EmployeeViewModel>();
            var listResult = _service.Search(code, name).ToViewModel();
            foreach (var emp in listResult)
            {
                var newRequester = new EmployeeViewModel();
                newRequester = emp;
                newRequester.UnitCode = GetUserViewModel(emp.Username).UnitCode;
                listRequester.Add(newRequester);
            }
            return listRequester;
        }
    }
}