﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TheMall.MPS.API.Mappers;
using TheMall.MPS.Models.View;
using TheMall.MPS.Reports.BudgetTracking;
using TheMall.MPS.API.Services;

namespace TheMall.MPS.API.Facades
{
    public interface IBudgetTrackingFacad
    {
        IEnumerable<BudgetTrackingDataSource> ReportData(string username);
    }
    public class ReportBudgetTrackingFacade : IBudgetTrackingFacad
    {
        private readonly IBudgetTrackingViewService _budgetTrackingViewService;
        private readonly IProposalService _proposalService;
        private readonly IProposalLineAPService _proposalLineTemplateService;
        private readonly IUnitCodeForEmployeeMasterService _unitCodeForEmployeeMasterService;

        public ReportBudgetTrackingFacade(
            IBudgetTrackingViewService budgetTrackingViewService, 
            IProposalService proposalService, 
            IProposalLineAPService proposalLineTemplateService,
            IUnitCodeForEmployeeMasterService unitCodeForEmployeeMasterService)
        {
            _budgetTrackingViewService = budgetTrackingViewService;
            _proposalService = proposalService;
            _proposalLineTemplateService = proposalLineTemplateService;
            _unitCodeForEmployeeMasterService = unitCodeForEmployeeMasterService;
        }

        public IEnumerable<BudgetTrackingDataSource> ReportData(string username)
        {
            var dataSource = new List<BudgetTrackingDataSource>();
            //var listData = new List<MPS_VW_BudgetTracking>();

            // map data
            var listView = _budgetTrackingViewService.GetAll();
            var unitcodeView = _unitCodeForEmployeeMasterService.GetWhere(m => m.Username == username && !m.InActive);

            foreach (var list in listView)
            {
                foreach (var unitcode in unitcodeView)
                {
                    if (list.UnitCode == unitcode.UnitCode)
                    {
                        //listData.Add(list);
                        var data = new BudgetTrackingDataSource();
                        var proposalTable = _proposalService.Find(m => m.Id == list.ProposalId);
                        var linetemple = _proposalLineTemplateService.GetWhere(m => m.ProposalLineId == list.ProposalLineId).ToList();
                        data.ProposalNo = list.ProposalNo;
                        data.ProposalTitle = proposalTable.Title;
                        data.RequesterName = proposalTable.RequesterName;


                        if (proposalTable.CreatedDate != null)
                        {
                            DateTime Create = (DateTime)proposalTable.CreatedDate;
                            data.CreateDate = Create.ToString("dd/MM/yyyy");
                        }
                        
                        if (proposalTable.ProjectStartDate != null)
                        {
                            DateTime StartDate = (DateTime)proposalTable.ProjectStartDate;
                            data.CampaignStartDate = StartDate.ToString("dd/MM/yyyy");
                        }

                        if (proposalTable.ProjectEndDate != null)
                        {
                            DateTime EndDate = (DateTime)proposalTable.ProjectEndDate;
                            data.CampaignEndDate = EndDate.ToString("dd/MM/yyyy");
                        }
                        
                        data.ProposalUnitCode = list.ProposalUnitCode;
                        data.UnitCode = list.UnitCode;
                        data.APCode = list.APCode;
                        data.Description = list.Description;
                        data.BudgetPlan = list.BudgetPlan;
                        data.BudgetUsed = list.BudgetUsed;
                        data.BudgetRemaining = list.BudgetRemaining;

                        data.M02_RE = (linetemple.LastOrDefault(m => m.BranchCode == "M02") != null) ? linetemple.LastOrDefault(m => m.BranchCode == "M02").RE_Amount : 0;
                        data.M02_TR = (linetemple.LastOrDefault(m => m.BranchCode == "M02") != null) ? linetemple.LastOrDefault(m => m.BranchCode == "M02").TR_Amount : 0;
                        data.M03_RE = (linetemple.LastOrDefault(m => m.BranchCode == "M03") != null) ? linetemple.LastOrDefault(m => m.BranchCode == "M03").RE_Amount : 0;
                        data.M03_TR = (linetemple.LastOrDefault(m => m.BranchCode == "M03") != null) ? linetemple.LastOrDefault(m => m.BranchCode == "M03").TR_Amount : 0;
                        data.M05_RE = (linetemple.LastOrDefault(m => m.BranchCode == "M05") != null) ? linetemple.LastOrDefault(m => m.BranchCode == "M05").RE_Amount : 0;
                        data.M05_TR = (linetemple.LastOrDefault(m => m.BranchCode == "M05") != null) ? linetemple.LastOrDefault(m => m.BranchCode == "M05").TR_Amount : 0;
                        data.M06_RE = (linetemple.LastOrDefault(m => m.BranchCode == "M06") != null) ? linetemple.LastOrDefault(m => m.BranchCode == "M06").RE_Amount : 0;
                        data.M06_TR = (linetemple.LastOrDefault(m => m.BranchCode == "M06") != null) ? linetemple.LastOrDefault(m => m.BranchCode == "M06").TR_Amount : 0;
                        data.M07_RE = (linetemple.LastOrDefault(m => m.BranchCode == "M07") != null) ? linetemple.LastOrDefault(m => m.BranchCode == "M07").RE_Amount : 0;
                        data.M07_TR = (linetemple.LastOrDefault(m => m.BranchCode == "M07") != null) ? linetemple.LastOrDefault(m => m.BranchCode == "M07").TR_Amount : 0;
                        data.M08_RE = (linetemple.LastOrDefault(m => m.BranchCode == "M08") != null) ? linetemple.LastOrDefault(m => m.BranchCode == "M08").RE_Amount : 0;
                        data.M08_TR = (linetemple.LastOrDefault(m => m.BranchCode == "M08") != null) ? linetemple.LastOrDefault(m => m.BranchCode == "M08").TR_Amount : 0;
                        data.M09_RE = (linetemple.LastOrDefault(m => m.BranchCode == "M09") != null) ? linetemple.LastOrDefault(m => m.BranchCode == "M09").RE_Amount : 0;
                        data.M09_TR = (linetemple.LastOrDefault(m => m.BranchCode == "M09") != null) ? linetemple.LastOrDefault(m => m.BranchCode == "M09").TR_Amount : 0;
                        data.M10_RE = (linetemple.LastOrDefault(m => m.BranchCode == "M10") != null) ? linetemple.LastOrDefault(m => m.BranchCode == "M10").RE_Amount : 0;
                        data.M10_TR = (linetemple.LastOrDefault(m => m.BranchCode == "M10") != null) ? linetemple.LastOrDefault(m => m.BranchCode == "M10").TR_Amount : 0;
                        data.M11_RE = (linetemple.LastOrDefault(m => m.BranchCode == "M11") != null) ? linetemple.LastOrDefault(m => m.BranchCode == "M11").RE_Amount : 0;
                        data.M11_TR = (linetemple.LastOrDefault(m => m.BranchCode == "M11") != null) ? linetemple.LastOrDefault(m => m.BranchCode == "M11").TR_Amount : 0;
                        data.M14_RE = (linetemple.LastOrDefault(m => m.BranchCode == "M14") != null) ? linetemple.LastOrDefault(m => m.BranchCode == "M14").RE_Amount : 0;
                        data.M14_TR = (linetemple.LastOrDefault(m => m.BranchCode == "M14") != null) ? linetemple.LastOrDefault(m => m.BranchCode == "M14").TR_Amount : 0;
                        data.M15_RE = (linetemple.LastOrDefault(m => m.BranchCode == "M15") != null) ? linetemple.LastOrDefault(m => m.BranchCode == "M15").RE_Amount : 0;
                        data.M15_TR = (linetemple.LastOrDefault(m => m.BranchCode == "M15") != null) ? linetemple.LastOrDefault(m => m.BranchCode == "M15").TR_Amount : 0;
                        data.M17_RE = (linetemple.LastOrDefault(m => m.BranchCode == "M17") != null) ? linetemple.LastOrDefault(m => m.BranchCode == "M17").RE_Amount : 0;
                        data.M17_TR = (linetemple.LastOrDefault(m => m.BranchCode == "M17") != null) ? linetemple.LastOrDefault(m => m.BranchCode == "M17").TR_Amount : 0;
                        data.B5_RE = (linetemple.LastOrDefault(m => m.BranchCode == "B5") != null) ? linetemple.LastOrDefault(m => m.BranchCode == "B5").RE_Amount : 0;
                        data.B5_TR = (linetemple.LastOrDefault(m => m.BranchCode == "B5") != null) ? linetemple.LastOrDefault(m => m.BranchCode == "B5").TR_Amount : 0;
                        data.B7_RE = (linetemple.LastOrDefault(m => m.BranchCode == "B7") != null) ? linetemple.LastOrDefault(m => m.BranchCode == "B7").RE_Amount : 0;
                        data.B7_TR = (linetemple.LastOrDefault(m => m.BranchCode == "B7") != null) ? linetemple.LastOrDefault(m => m.BranchCode == "B7").TR_Amount : 0;
                        data.V1_RE = (linetemple.LastOrDefault(m => m.BranchCode == "V1") != null) ? linetemple.LastOrDefault(m => m.BranchCode == "V1").RE_Amount : 0;
                        data.V1_TR = (linetemple.LastOrDefault(m => m.BranchCode == "V1") != null) ? linetemple.LastOrDefault(m => m.BranchCode == "V1").TR_Amount : 0;
                        data.V2_RE = (linetemple.LastOrDefault(m => m.BranchCode == "V2") != null) ? linetemple.LastOrDefault(m => m.BranchCode == "V2").RE_Amount : 0;
                        data.V2_TR = (linetemple.LastOrDefault(m => m.BranchCode == "V2") != null) ? linetemple.LastOrDefault(m => m.BranchCode == "V2").TR_Amount : 0;
                        data.V3_RE = (linetemple.LastOrDefault(m => m.BranchCode == "V3") != null) ? linetemple.LastOrDefault(m => m.BranchCode == "V3").RE_Amount : 0;
                        data.V3_TR = (linetemple.LastOrDefault(m => m.BranchCode == "V3") != null) ? linetemple.LastOrDefault(m => m.BranchCode == "V3").TR_Amount : 0;
                        data.V4_RE = (linetemple.LastOrDefault(m => m.BranchCode == "V4") != null) ? linetemple.LastOrDefault(m => m.BranchCode == "V4").RE_Amount : 0;
                        data.V4_TR = (linetemple.LastOrDefault(m => m.BranchCode == "V4") != null) ? linetemple.LastOrDefault(m => m.BranchCode == "V4").TR_Amount : 0;
                        data.V5_RE = (linetemple.LastOrDefault(m => m.BranchCode == "V5") != null) ? linetemple.LastOrDefault(m => m.BranchCode == "V5").RE_Amount : 0;
                        data.V5_TR = (linetemple.LastOrDefault(m => m.BranchCode == "V5") != null) ? linetemple.LastOrDefault(m => m.BranchCode == "V5").TR_Amount : 0;
                        data.V9_RE = (linetemple.LastOrDefault(m => m.BranchCode == "V9") != null) ? linetemple.LastOrDefault(m => m.BranchCode == "V9").RE_Amount : 0;
                        data.V9_TR = (linetemple.LastOrDefault(m => m.BranchCode == "V9") != null) ? linetemple.LastOrDefault(m => m.BranchCode == "V9").TR_Amount : 0;

                        data.Memo = list.Memo;
                        data.PettyCash = list.PettyCash;
                        data.CashAdvance = list.CashAdvance;
                        data.PR = list.PR;
                        data.PO = list.PO;
                        data.GR = list.GR;

                        dataSource.Add(data);
                    }
                }
            }
            return dataSource;

            //foreach (var list in listView)
            //{
            //    var data = new BudgetTrackingDataSource();
            //    var proposalTable = _proposalService.Find(m => m.Id == list.ProposalId);
            //    var linetemple = _proposalLineTemplateService.GetWhere(m => m.ProposalLineId == list.ProposalLineId).ToList();
            //    data.ProposalNo =list.ProposalNo;
            //    data.ProposalTitle = proposalTable.Title;
            //    data.RequesterName = proposalTable.RequesterName;
            //    data.CreateDate = proposalTable.CreatedDate.ToString();
            //    data.CampaignStartDate = proposalTable.ProjectStartDate.ToString();
            //    data.CampaignEndDate = proposalTable.ProjectEndDate.ToString();
            //    data.ProposalUnitCode = proposalTable.UnitCode;

            //    data.M02_RE = (linetemple.LastOrDefault(m => m.BranchCode == "M02") != null) ? linetemple.LastOrDefault(m => m.BranchCode == "M02").RE_Amount : 0;
            //    data.M02_TR = (linetemple.LastOrDefault(m => m.BranchCode == "M02") != null) ? linetemple.LastOrDefault(m => m.BranchCode == "M02").TR_Amount : 0;
            //    data.M03_RE = (linetemple.LastOrDefault(m => m.BranchCode == "M03") !=null)  ? linetemple.LastOrDefault(m => m.BranchCode == "M03").RE_Amount : 0;
            //    data.M03_TR = (linetemple.LastOrDefault(m => m.BranchCode == "M03") != null) ? linetemple.LastOrDefault(m => m.BranchCode == "M03").TR_Amount : 0;
            //    data.M05_RE = (linetemple.LastOrDefault(m => m.BranchCode == "M05") != null) ? linetemple.LastOrDefault(m => m.BranchCode == "M05").RE_Amount : 0;
            //    data.M05_TR = (linetemple.LastOrDefault(m => m.BranchCode == "M05") != null) ? linetemple.LastOrDefault(m => m.BranchCode == "M05").TR_Amount : 0;
            //    data.M06_RE = (linetemple.LastOrDefault(m => m.BranchCode == "M06") != null) ? linetemple.LastOrDefault(m => m.BranchCode == "M06").RE_Amount : 0;
            //    data.M06_TR = (linetemple.LastOrDefault(m => m.BranchCode == "M06") != null) ? linetemple.LastOrDefault(m => m.BranchCode == "M06").TR_Amount : 0;
            //    data.M07_RE = (linetemple.LastOrDefault(m => m.BranchCode == "M07") != null) ? linetemple.LastOrDefault(m => m.BranchCode == "M07").RE_Amount : 0;
            //    data.M07_TR = (linetemple.LastOrDefault(m => m.BranchCode == "M07") != null) ? linetemple.LastOrDefault(m => m.BranchCode == "M07").TR_Amount : 0;
            //    data.M08_RE = (linetemple.LastOrDefault(m => m.BranchCode == "M08") != null) ? linetemple.LastOrDefault(m => m.BranchCode == "M08").RE_Amount : 0;
            //    data.M08_TR = (linetemple.LastOrDefault(m => m.BranchCode == "M08") != null) ? linetemple.LastOrDefault(m => m.BranchCode == "M08").TR_Amount : 0;
            //    data.M09_RE = (linetemple.LastOrDefault(m => m.BranchCode == "M09") != null) ? linetemple.LastOrDefault(m => m.BranchCode == "M09").RE_Amount : 0;
            //    data.M09_TR = (linetemple.LastOrDefault(m => m.BranchCode == "M09") != null) ? linetemple.LastOrDefault(m => m.BranchCode == "M09").TR_Amount : 0;
            //    data.M10_RE = (linetemple.LastOrDefault(m => m.BranchCode == "M10") != null) ? linetemple.LastOrDefault(m => m.BranchCode == "M10").RE_Amount : 0;
            //    data.M10_TR = (linetemple.LastOrDefault(m => m.BranchCode == "M10") != null) ? linetemple.LastOrDefault(m => m.BranchCode == "M10").TR_Amount : 0;
            //    data.M11_RE = (linetemple.LastOrDefault(m => m.BranchCode == "M11") != null) ? linetemple.LastOrDefault(m => m.BranchCode == "M11").RE_Amount : 0;
            //    data.M11_TR = (linetemple.LastOrDefault(m => m.BranchCode == "M11") != null) ? linetemple.LastOrDefault(m => m.BranchCode == "M11").TR_Amount : 0;
            //    data.M14_RE = (linetemple.LastOrDefault(m => m.BranchCode == "M14") != null) ? linetemple.LastOrDefault(m => m.BranchCode == "M14").RE_Amount : 0;
            //    data.M14_TR = (linetemple.LastOrDefault(m => m.BranchCode == "M14") != null) ? linetemple.LastOrDefault(m => m.BranchCode == "M14").TR_Amount : 0;
            //    data.M15_RE = (linetemple.LastOrDefault(m => m.BranchCode == "M15") != null) ? linetemple.LastOrDefault(m => m.BranchCode == "M15").RE_Amount : 0;
            //    data.M15_TR = (linetemple.LastOrDefault(m => m.BranchCode == "M15") != null) ? linetemple.LastOrDefault(m => m.BranchCode == "M15").TR_Amount : 0;
            //    data.M17_RE = (linetemple.LastOrDefault(m => m.BranchCode == "M17") != null) ? linetemple.LastOrDefault(m => m.BranchCode == "M17").RE_Amount : 0;
            //    data.M17_TR = (linetemple.LastOrDefault(m => m.BranchCode == "M17") != null) ? linetemple.LastOrDefault(m => m.BranchCode == "M17").TR_Amount : 0;
            //    dataSource.Add(data);
            //}

            //return dataSource;
        }
       
    }
}