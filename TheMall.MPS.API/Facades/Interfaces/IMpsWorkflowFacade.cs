﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Routing;

using SourceCode.Workflow.Client;
using TheMall.MPS.Models.Interfaces;
using TheMall.MPS.ViewModels.Interfaces;

namespace TheMall.MPS.API.Facades.Interfaces
{
    public interface IMpsWorkflowFacade<TWorkflowTable, TAttachmentTable, TCommentTable, TWorkflowViewModel>
    : IMpsAttachmentFacade<TAttachmentTable>
        where TWorkflowTable : IMpsWorkflowTable
        where TAttachmentTable : IAttachmentTable
        where TCommentTable : ICommentTable
        where TWorkflowViewModel : IMpsWorkflowViewModel
    {

        //Task<TWorkflowViewModel> GetViewModel(int id, string sn = null);
        Task<TWorkflowViewModel> GetViewModel(UrlHelper url, int id, string sn = null);

        #region Workflow
        IEnumerable<ValidationResult> ValidateStartProcess(TWorkflowTable workflowTable);
        Task<bool> UpdateBeforeStartProcessAsync(TWorkflowTable workflowTable);
        bool UpdateBeforeStartProcess(TWorkflowTable workflowTable);
        Task<int> StartProcess(int id);
        Task<TCommentTable> Submit(string sn, string action, string comment,
            TWorkflowViewModel viewModel);

        IEnumerable<ValidationResult> ValidateSubmit(TWorkflowTable workflowTable,
            WorklistItem worklistItem, string action, TWorkflowViewModel viewModel);
        #endregion

        #region CRUD and ViewModels
        Task<TWorkflowViewModel> SaveAsync(TWorkflowViewModel landViewModel, string action = null);
      
        Task<TWorkflowViewModel> GetWorklist(UrlHelper url, string sn);
        
        WorklistItem GetWorklistItem(string sn);
        #endregion
    }
}