﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using CrossingSoft.Framework.Models.Interfaces;
using CRS.Advance.API.Facades.Interfaces;
using TheMall.MPS.ViewModels;


namespace TheMall.MPS.API.Facades.Interfaces
{
    public interface IMpsAttachmentFacade<TAttachmentTable> : IBaseFacade
     where TAttachmentTable : IAttachmentTable
    {
        TAttachmentTable GetAttachment(string id, string fileName);
        TAttachmentTable GetAttachment(string id, string fileName, int documentType);
        Task<TAttachmentTable> SaveAttachmentAsync<TPostedAttachment>(
            HttpContentHeaders headers,
            string contentType,
            Stream stream,
            string username,
            TPostedAttachment viewModel)
            where TPostedAttachment : IPostedAttachmentViewModel;

        Task<bool> RemoveAttachments(int id, int? documentType, string sn, string documentNumber = null, params string[] fileNames);
       
        Task<StreamContent> GetStreamContent(TAttachmentTable attachment);
    }
}