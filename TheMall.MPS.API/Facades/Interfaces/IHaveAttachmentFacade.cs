﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using TheMall.MPS.Models.Interfaces;
using TheMall.MPS.ViewModels;

namespace TheMall.MPS.API.Facades.Interfaces
{
    public interface IHaveAttachmentFacade<TAttachmentTable> where TAttachmentTable : IAttachmentTable
    {
        IEnumerable<AttachmentViewModel> UploadFile(HttpRequestMessage request, HttpContext context, string id, System.Web.Http.Routing.UrlHelper urlHelper);

        TAttachmentTable GetAttachment(int id);

        IEnumerable<AttachmentViewModel> GetAllAttachments(string parentId, System.Web.Http.Routing.UrlHelper url);
        IEnumerable<TAttachmentTable> GetAttachments(string parentId);
        IEnumerable<TAttachmentTable> GetAttachments(string parentId, int documentType);

        TAttachmentTable SaveAttachment<TPostedAttachment>(
            HttpRequestMessage request,
            string contentType,
            //Stream stream,
            HttpPostedFile file,
            string username,
            TPostedAttachment viewModel)
            where TPostedAttachment : ViewModels.IPostedAttachmentViewModel;

        TAttachmentTable SavePartialAttachment<TPostedAttachment>(
            HttpContext context,
            HttpRequestMessage request,
            string contentType,
            //Stream stream,
            HttpPostedFile file,
            string username,
            TPostedAttachment viewModel)
            where TPostedAttachment : ViewModels.IPostedAttachmentViewModel;

        Task<bool> RemoveAttachments(string parentId, int id);
        Task<System.Net.Http.StreamContent> GetStreamContent(TAttachmentTable attachment);
    }
}
