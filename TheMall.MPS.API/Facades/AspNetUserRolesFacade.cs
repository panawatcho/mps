﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using TheMall.MPS.API.Facades.Abstracts;
using TheMall.MPS.API.Mappers;
using TheMall.MPS.API.Services;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Infrastructure.Identity;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.ViewModels.AspNet;

namespace TheMall.MPS.API.Facades
{
    public interface IAspNetUserRolesFacade : IBaseMasterFacade<ApplicationUserRole, AspNetUserRolesViewModel>
    {
        AspNetUserRolesViewModel GetByUserIdandRoleId(string roleId = null, string username = null);
        IEnumerable<AspNetUserRolesViewModel> GetAll();
        IEnumerable<AspNetUserRolesViewModel> GetByRoleId(string roleId = null);

    }
    public class AspNetUserRolesFacade : BaseMasterFacade<ApplicationUserRole, AspNetUserRolesViewModel>, IAspNetUserRolesFacade
    {
        private readonly IAspNetUserRolesService _service;
        private readonly IMPSSession _session;
        private readonly IEmployeeTableService _employeeTableService;
        public AspNetUserRolesFacade(
            IMPSSession session,
            IAspNetUserRolesService service, 
            IEmployeeTableService employeeTableService)
            : base(session, service)
        {
            _service = service;
            _employeeTableService = employeeTableService;
            _session = session;
        }

        public override async Task<AspNetUserRolesViewModel> Create(AspNetUserRolesViewModel viewModel)
        {
            try
            {
                var model = _service.Insert(viewModel.ToModel());
                await _session.SaveChangesAsync();
                return model.ToViewModel();
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
        }

        public override async Task<AspNetUserRolesViewModel> Update(AspNetUserRolesViewModel viewModel)
        {
            try
            {
                var model = _service.Update(viewModel.ToModel());
                await _session.SaveChangesAsync();
                return model.ToViewModel();
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
        }

        public override async Task<AspNetUserRolesViewModel> Delete(AspNetUserRolesViewModel viewModel)
        {
            try
            {
                _service.Delete(viewModel.ToModel());
                await _session.SaveChangesAsync();
                return viewModel;
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
        }

        public AspNetUserRolesViewModel GetByUserIdandRoleId(string roleId = null, string username = null)
        {
            try
            {
                var model = _service.FirstOrDefault(m => m.RoleId.Equals(roleId, StringComparison.InvariantCultureIgnoreCase)
                    && m.UserId.Equals(username, StringComparison.InvariantCultureIgnoreCase));
                if (model != null)
                {
                    var viewModel = model.ToViewModel();
                    if (viewModel.Username != null)
                    {
                        viewModel.User = _employeeTableService.FindByUsername(viewModel.Username).ToViewModel();
                    }
                    return viewModel;
                }
                return null;
            }
            catch (Exception exception)
            {

                throw new Exception(exception.Message);
            }
        }

        public IEnumerable<AspNetUserRolesViewModel> GetAll()
        {
            try
            {
                var models = _service.GetAll();
                if (models != null)
                {
                    var viewModels = models.ToViewModels();
                    return viewModels;
                }
                return null;
            }
            catch (Exception exception)
            {

                throw new Exception(exception.Message);
            }
        }


        public IEnumerable<AspNetUserRolesViewModel> GetByRoleId(string roleId = null)
        {
            try
            {
                var models = _service.GetWhere(m=>m.RoleId.
                    Equals(roleId,StringComparison.InvariantCultureIgnoreCase));
                if (models != null)
                {
                    var viewModels = models.ToViewModels();
                    return viewModels;
                }
                return null;
            }
            catch (Exception exception)
            {

                throw new Exception(exception.Message);
            }
        }
    }
}