﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Routing;
using CrossingSoft.Framework.Infrastructure.K2;
using Microsoft.AspNet.Identity;
using SourceCode.Workflow.Client;
using TheMall.MPS.API.Controllers.Abstracts;
using TheMall.MPS.API.Facades.Abstracts;
using TheMall.MPS.API.Facades.Interfaces;
using TheMall.MPS.API.Mappers;
using TheMall.MPS.API.Services;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Models.Budget;
using TheMall.MPS.Models.Enums;
using TheMall.MPS.Models.K2;
using TheMall.MPS.Models.Master;
using TheMall.MPS.Models.Memo;
using TheMall.MPS.Models.Proposal;
using TheMall.MPS.Models.Workflow;
using TheMall.MPS.ViewModels;
using TheMall.MPS.ViewModels.Master;
using TheMall.MPS.ViewModels.Memo;
using TheMall.MPS.ViewModels.Proposal;
using WebApi.OutputCache.V2;

namespace TheMall.MPS.API.Facades
{
    public interface IMemoFacade : IMpsWorkflowFacade<MPS_MemoTable, MPS_MemoAttachment, MPS_MemoComment, MemoViewModel>
    {
        Task<MemoViewModel> GetDataBindMemo(int propLineid, string unitcode = null);
        Task<MemoViewModel> GetDataBindMemo(int propLineid);
        Task<IEnumerable<MemoViewModel>> GetRelatedWorkflowTable();
        void SetFinalApproverDeleted(int id);
        string GetEmailInformAll(int workflowTableId);
        MemoViewModel DeleteBy(int id, string username);
    }

    public class MemoFacade : BaseMpsWorkflowFacade<MPS_MemoTable, MPS_MemoAttachment, MPS_MemoComment, MemoViewModel>, IMemoFacade
    {
        private readonly IMPSSession _session;
        private readonly IMemoService _workflowTableService;
        private readonly IMemoMapper _mapper;
        private readonly IMemoLineMapper _lineMapper;
        private readonly IMemoLineService _lineService;
        private readonly IProposalLineService _proposallineService;
        private readonly IProposalLineMapper _proposallineMapper;
        private readonly IProposalMapper _proposalMapper;
        private readonly IUnitCodeForEmployeeService _unitCodeForEmployeeService;
        private readonly IBudgetProposalTransService _budgetProposalTransService;
        private readonly IBudgetProposalTransFacade _budgetProposalTransFacade;
        private readonly IMemoApprovalService _approvalService;
        private readonly IMemoApprovalMapper _approvalMapper;
        private readonly IEmployeeTableService _employeeTableService;
        private readonly IProposalBudgetPlanService _budgetPlanService;
        private readonly IProposalIncomeDepositService _incomeDepositService;
        private readonly IProposalIncomeDepositMapper _incomeDepositMapper;

        public MemoFacade(IMPSSession session, 
            IMemoService workflowTableService, 
            IAttachmentService<MPS_MemoAttachment, MPS_MemoTable> attachmentService, 
            ICommentService<MPS_MemoComment> commentService, 
            IWorkflowService<MPS_MemoTable> workflowService, 
            INumberSequenceService numberSequenceService, 
            IMemoProcess definition,
            IMemoMapper mapper,
            IMemoLineMapper lineMapper,
            IMemoLineService lineService,
            IProposalLineService proposallineService,
            IMemoApprovalService approvalService,
            IMemoApprovalMapper approvalMapper,
            IProposalLineMapper proposallineMapper,
            IProposalMapper proposalMapper,
            IUnitCodeForEmployeeService unitCodeForEmployeeService,
            IBudgetProposalTransService budgetProposalTransService, 
            IBudgetProposalTransFacade budgetProposalTransFacade, 
            IEmployeeTableService employeeTableService, 
            IProposalBudgetPlanService budgetPlanService, 
            IProposalIncomeDepositService incomeDepositService, 
            IProposalIncomeDepositMapper incomeDepositMapper) 
            : base(session, workflowTableService, attachmentService, commentService, workflowService, numberSequenceService, definition)
        {
            _session = session;
            _workflowTableService = workflowTableService;
            _mapper = mapper;
            _lineMapper = lineMapper;
            _lineService = lineService;
            _proposallineService = proposallineService;
            _approvalService = approvalService;
            _approvalMapper = approvalMapper;
            _proposallineMapper = proposallineMapper;
            _proposalMapper = proposalMapper;
            _unitCodeForEmployeeService = unitCodeForEmployeeService;
            _budgetProposalTransService = budgetProposalTransService;
            _budgetProposalTransFacade = budgetProposalTransFacade;
            _employeeTableService = employeeTableService;
            _budgetPlanService = budgetPlanService;
            _incomeDepositService = incomeDepositService;
            _incomeDepositMapper = incomeDepositMapper;
        }

        public override async Task<MemoViewModel> GetViewModel(UrlHelper url, int id, string sn = null)
        {
            try
            {
                var model = await _workflowTableService.GetAsync(id);
                var viewModel =
                    await this.InitWorkflowViewModel(url, id, sn, _mapper.ToViewModel(model));

                return viewModel;
            }
            catch (Exception ex)
            {
                throw new HttpException(ex.Message);
            }
            
        }

        public async Task<IEnumerable<MemoViewModel>> GetRelatedWorkflowTable()
         {
             var listModel = new List<MemoViewModel>();
             var username = HttpContext.Current.User.Identity.GetUserName();
             var model = _workflowTableService.GetWhere(m => !m.Deleted);
             var unitCode = _unitCodeForEmployeeService.GetUnitCodeCurrentUser();
             
             var result =
             (
                 from unit in unitCode 
                 join memo in model on unit.UnitCode equals memo.UnitCode into m
                 from memo in m.DefaultIfEmpty()
                 select new
                 {
                     memo = (memo)??new MPS_MemoTable()
                 }
             ).ToListSafe();
             if (!result.Any()) return listModel;
             listModel.AddRange(
                 result.Select(data => _mapper.ToViewModel(data.memo)));
             return  listModel;
         }

        #region SaveAsync
        public override async Task<MemoViewModel> SaveAsync(MemoViewModel viewModel, string action = null)
        {
            if (viewModel == null)
                throw new ArgumentNullException("viewModel");

            //if (viewModel.BudgetDetail > viewModel.BudgetAPCode)
            //    throw new System.InvalidOperationException("Budget Detail sum value must not be more than Budget A&P Code!!!");


            var model = _mapper.ToModel(viewModel);
            try
            {
                if (viewModel.Id <= 0)
                {
                   
                    model = _workflowTableService.Insert(model);
                }
                else
                {
                    model = _workflowTableService.Update(model, model.Id);
                }

                var memoLines = _lineMapper.ToModels(viewModel.MemoLines, model.MemoLine).ToListSafe();
                foreach (var line in memoLines)
                {
                    if (line.Id <= 0 || line.ParentId <= 0)
                    {
                        if (model.Id <= 0)
                        {
                            line.MemoTable = model;
                        }
                        else
                        {
                            line.ParentId = model.Id;
                        }
                        _lineService.Insert(line);
                    }
                    else
                    {
                        if (line.Deleted)
                        {
                            _lineService.Delete(line);
                        }
                        else
                        {
                            _lineService.Update(line, line.Id);
                        }
                    }
                }

                var approval = _approvalMapper.ToModels(viewModel.MemoApproval, model.MemoApproval).ToListSafe();
                foreach (var approve in approval)
                {
                    if (approve.Id <= 0 || approve.ParentId <= 0)
                    {
                        if (model.Id <= 0)
                        {
                            approve.MemoTable = model;
                        }
                        else
                        {
                            approve.ParentId = model.Id;
                        }
                        _approvalService.Insert(approve);
                    }
                    else
                    {
                        if (approve.Deleted)
                        {
                            _approvalService.Delete(approve);
                        }
                        else
                        {
                           
                            _approvalService.Update(approve, approve.Id);
                        }
                    }
                }
                await Session.SaveChangesAsync();

                return _mapper.ToViewModel(model);
            }
            catch (Exception ex)
            {
                throw new HttpException(ex.Message);
            }
        }
        #endregion      

        public override IEnumerable<ValidationResult> ValidateStartProcess(MPS_MemoTable workflowTable)
        {

            if (workflowTable.ProposalLine.ProposalTable.CloseFlag == true)
            {
                yield return new ValidationResult("Cannot submit because your proposal was closed!!!");
            }
            if (workflowTable.ProposalLine.ProposalTable.StatusFlag == 8)
            {
                yield return new ValidationResult("Its proposal is back in the process of editing(Cannot submit !)");
            }
            if (workflowTable.ActualCharge)
            {
                var budgetIncome = workflowTable.IncomeDeposit.ProposalTable.IncomeDeposit.Where(
              m => m.ProposalDepositRefID == workflowTable.IncomeDeposit.ProposalDepositRefID && m.ActualCharge).Sum(m => m.Budget);
                if (workflowTable.BudgetDetail > budgetIncome)
                {
                    yield return new ValidationResult("Amount for Income Deposit  " + budgetIncome
                                                         + " baht");
                }
            }
            else
            {
                //var budget = _budgetProposalTransService.GetBudgetTransTotal(workflowTable.ProposalLineID);
                var budget = _budgetProposalTransService.GetBudgetTransTotal(workflowTable.ProposalLine.ParentId,
                    workflowTable.ProposalLine.APCode, workflowTable.ProposalLine.UnitCode);
                var propBudgetPlan = _budgetPlanService.GetProposalBudgetPlan(workflowTable.ProposalLine.ParentId,
                    workflowTable.ProposalLine.APCode, workflowTable.ProposalLine.UnitCode);
                var balance = (propBudgetPlan != null) ? propBudgetPlan.BudgetPlan - budget : budget;
                if (balance < workflowTable.BudgetDetail)
                {
                    yield return new ValidationResult("Budget exceed: Statements remainning " + balance
                                                                                              + " baht");


                }
            }

            //if (!workflowTable.MemoApproval.Any(m => m.ApproverLevel.Equals(Approval.ApproveLevel.Propose) && !m.Deleted))
                //{
                //    yield return new ValidationResult("Please select approver level propose!!!");

                //}
                //if (!workflowTable.MemoApproval.Any(m => m.ApproverLevel.Equals(Approval.ApproveLevel.Accept) && !m.Deleted))
                //{
                //    yield return new ValidationResult("Please select approver level accept!!!");

                //}
                //if (!workflowTable.MemoApproval.Any(m => m.ApproverLevel.Equals(Approval.ApproveLevel.Approval) && !m.Deleted))
                //{
                //    yield return new ValidationResult("Please select a approver!!!");

                //}
                if (!workflowTable.MemoApproval.Any(m => m.ApproverLevel.Equals(Approval.ApproveLevel.FinalApproval) && !m.Deleted && m.LockEditable))
                {
                    yield return new ValidationResult("Please select a final apporver!!!");
                }

                if (workflowTable.BudgetDetail <= 0)
                {
                    yield return new ValidationResult("Butget must not be 0");

                }
                if (string.IsNullOrEmpty(workflowTable.AccountingBranch))
                {
                    yield return new ValidationResult("Please select accounting!!!");
                }
        }

        public override IEnumerable<ValidationResult> ValidateSubmit(MPS_MemoTable workflowTable, WorklistItem worklistItem, string action,
            MemoViewModel viewModel)
        {
            if (action.ToUpper().Equals("RESUBMIT"))
            {
                if (viewModel.ProposalRef.StatusFlag == 8)
                {
                    yield return new ValidationResult("Its proposal is back in the process of editing(Cannot submit !)");
                }
               
                if (workflowTable.ActualCharge)
                {
                    var budgetIncome = workflowTable.ProposalLine.ProposalTable.IncomeDeposit.Where(
                            m => m.ProposalDepositRefDoc == viewModel.IncomeDepositViewModel.DocNumber && m.ActualCharge).Sum(m => m.Budget);
                    if (viewModel.BudgetDetail > budgetIncome )
                    {
                        yield return new ValidationResult("Amount for Income Deposit  " + (budgetIncome)
                                                             + " baht");
                    }
                }
                else
                {
                     var budgetTrans = _budgetProposalTransService.LastOrDefault(m => m.ProposalLineId == workflowTable.ProposalLine.Id
                                                                                   &&
                                                                                   m.DocumentType ==
                                                                                   Budget.DocumentType.Memo
                                                                                   &&
                                                                                   m.DocumentNo ==
                                                                                   workflowTable.DocumentNumber
                                                                                   && m.Status == Budget.BudgetStatus.Reserve
                                                                                   && !m.InActive);
                if (budgetTrans == null)
                    yield return new ValidationResult(string.Format(Resources.Error.BudgetTransNotFound, workflowTable.DocumentNumber));

                var budgetPlan = _budgetPlanService.GetProposalBudgetPlan(workflowTable.ProposalLine.ParentId, workflowTable.ProposalLine.APCode, workflowTable.ProposalLine.UnitCode);
                if (budgetPlan == null)
                    yield return new ValidationResult(string.Format(Resources.Error.ProposalBudgetPlanNotFound, workflowTable.ProposalLine.ProposalTable.DocumentNumber, workflowTable.ProposalLine.APCode, workflowTable.ProposalLine.UnitCode));

                var budgetTransTotal = _budgetProposalTransService.GetBudgetTransTotal(workflowTable.ProposalLine.ParentId, workflowTable.ProposalLine.APCode, workflowTable.ProposalLine.UnitCode);
                var budgetBalance = (budgetPlan.BudgetPlan - budgetTransTotal + budgetTrans.Amount);

                if (budgetBalance < viewModel.BudgetDetail)
                {
                    yield return new ValidationResult("Budget exceed: Statements remainning " + budgetBalance
                                                      + " baht");
                }
                    
                }
               
                //var budget = _budgetProposalTransService.GetBudgetDetailAp(workflowTable.ProposalLineID,workflowTable.Id);
                //var balance = workflowTable.ProposalLine.BudgetPlan - budget;
                //if (balance < workflowTable.BudgetDetail)
                //{
                //    yield return new ValidationResult("Budget exceed: Statements remainning " + balance
                //                                      + " baht");
                //}

                if (viewModel.ProposalRef.CloseFlag == true)
                {
                    yield return new ValidationResult("Cannot submit because your proposal was closed!!!");
                }

                //if (
                //    !viewModel.MemoApproval.Any(
                //        m => m.ApproverLevel.Equals(Approval.ApproveLevel.Propose) && !m.Deleted))
                //{
                //    yield return new ValidationResult("Please select approver level propose!!!");

                //}
                //if (
                //    !workflowTable.MemoApproval.Any(
                //        m => m.ApproverLevel.Equals(Approval.ApproveLevel.Accept) && !m.Deleted))
                //{
                //    yield return new ValidationResult("Please select approver level accept!!!");

                //}
                //if (
                //    !viewModel.MemoApproval.Any(
                //        m => m.ApproverLevel.Equals(Approval.ApproveLevel.Approval) && !m.Deleted))
                //{
                //    yield return new ValidationResult("Please select a approver!!!");

                //}
                if (!viewModel.MemoApproval.Any(
                        m => m.ApproverLevel.Equals(Approval.ApproveLevel.FinalApproval) && !m.Deleted && m.LockEditable))
                {
                    yield return new ValidationResult("Please select a final apporver!!!");
                }
                if (viewModel.BudgetDetail <= 0)
                {
                    yield return new ValidationResult("Butget must not be 0");
                }
                if (string.IsNullOrEmpty(workflowTable.AccountingBranch))
                {
                    yield return new ValidationResult("Please select accounting!!!");
                }

                //if (viewModel.BudgetDetail > viewModel.BudgetAPCode)
                //{
                //    yield return
                //        new ValidationResult("Budget Deatil sum value must not be more than Budget A&P Code!!!");
                //}
            }
        }

        public override async Task<bool> UpdateAfterStartProcess(MPS_MemoTable workflowTable, int? procInstId)
        {
            
            if (workflowTable.ActualCharge)
            {
                if (workflowTable.IncomeDeposit != null)
                {
                    var incomeDeposit = new MPS_IncomeDeposit();

                    incomeDeposit.ProposalTable = _proposallineService.LastOrDefault(m => m.Id == workflowTable.ProposalLineID).ProposalTable;
                    incomeDeposit.ParentId = workflowTable.ProposalLine.ProposalTable.Id;

                    incomeDeposit.ActualCharge = workflowTable.IncomeDeposit.ActualCharge;
                    incomeDeposit.DepositApprover = workflowTable.IncomeDeposit.DepositApprover;
                    incomeDeposit.ProcInstId = workflowTable.IncomeDeposit.ProcInstId;
                    incomeDeposit.ProposalDepositRefDoc = workflowTable.IncomeDeposit.ProposalDepositRefDoc;
                    incomeDeposit.ProposalDepositRefID = workflowTable.IncomeDeposit.ProposalDepositRefID;
                    incomeDeposit.ProposalDepositRefTitle = workflowTable.IncomeDeposit.ProposalDepositRefTitle;

                    incomeDeposit.Status = workflowTable.IncomeDeposit.Status;
                    incomeDeposit.StatusFlag = workflowTable.IncomeDeposit.StatusFlag;
                    incomeDeposit.Budget = workflowTable.BudgetDetail * (-1) ?? 0;
                    incomeDeposit.Remark = "Actual Charge from Memo: " + workflowTable.DocumentNumber;
                    incomeDeposit.RefMemoId = workflowTable.Id;
                    _incomeDepositService.Insert(incomeDeposit);
                }

            }
            else
            {
                _budgetProposalTransFacade.CreateBudgetPropTrans(workflowTable.Id, "memo");
            }
            
            var result = await base.UpdateAfterStartProcess(workflowTable, procInstId);
            return result;
        }

        public override async Task<MPS_MemoComment> Submit(string sn, string action, string comment, MemoViewModel viewModel)
        {
           var result = await base.Submit(sn, action, comment, viewModel);
            if (result != null)
            {
                if (result.Action.ToUpper().Equals("RESUBMIT", StringComparison.CurrentCultureIgnoreCase))
                {
                   

                    if (viewModel.IncomeDepositViewModel != null && viewModel.MemoType.ActualCharge)
                    {
                        var resultIncomeDeposit = result.MpsMemoTable.ProposalLine.ProposalTable.IncomeDeposit.LastOrDefault(m => m.RefMemoId == result.ParentId);
                        // var incomeDeposit2 = viewModel.ProposalRef.IncomeDepositLine.LastOrDefault(m => m.RefMemoId==viewModel.Id && m.DocNumber == viewModel.IncomeDepositViewModel.DocNumber);
                        var cancelIncomeDeposit = new MPS_IncomeDeposit();
                       
                            cancelIncomeDeposit.ProposalTable = _proposallineService.LastOrDefault(m=>m.Id==viewModel.ProposalLineId).ProposalTable;
                            cancelIncomeDeposit.ParentId = cancelIncomeDeposit.ProposalTable.Id;
                        
                        cancelIncomeDeposit.ActualCharge = resultIncomeDeposit.ActualCharge;
                        cancelIncomeDeposit.DepositApprover = resultIncomeDeposit.DepositApprover;
                        cancelIncomeDeposit.ProcInstId = resultIncomeDeposit.ProcInstId;
                        cancelIncomeDeposit.ProposalDepositRefDoc = resultIncomeDeposit.ProposalDepositRefDoc;
                        cancelIncomeDeposit.ProposalDepositRefID = resultIncomeDeposit.ProposalDepositRefID;
                        cancelIncomeDeposit.ProposalDepositRefTitle = resultIncomeDeposit.ProposalDepositRefTitle;
                        cancelIncomeDeposit.RefMemoId = viewModel.Id;
                        cancelIncomeDeposit.Status = resultIncomeDeposit.Status;
                        cancelIncomeDeposit.StatusFlag = resultIncomeDeposit.StatusFlag;
                        cancelIncomeDeposit.Budget = resultIncomeDeposit.Budget * (-1);
                        cancelIncomeDeposit.Remark = "Revise Actual Charge from Memo:" + viewModel.DocumentNumber;
                        var revise = _incomeDepositService.Insert(cancelIncomeDeposit);

                        var newIncomeDeposit = new MPS_IncomeDeposit();
                        newIncomeDeposit.ProposalTable = _proposallineService.LastOrDefault(m => m.Id == viewModel.ProposalLineId).ProposalTable;
                        newIncomeDeposit.ParentId = cancelIncomeDeposit.ProposalTable.Id;
                        
                        newIncomeDeposit.ActualCharge = resultIncomeDeposit.ActualCharge;
                        newIncomeDeposit.DepositApprover = resultIncomeDeposit.DepositApprover;
                        newIncomeDeposit.ProcInstId = resultIncomeDeposit.ProcInstId;
                        newIncomeDeposit.ProposalDepositRefDoc = resultIncomeDeposit.ProposalDepositRefDoc;
                        newIncomeDeposit.ProposalDepositRefID = resultIncomeDeposit.ProposalDepositRefID;
                        newIncomeDeposit.ProposalDepositRefTitle = resultIncomeDeposit.ProposalDepositRefTitle;
                        newIncomeDeposit.RefMemoId = viewModel.Id;
                        newIncomeDeposit.Status = resultIncomeDeposit.Status;
                        newIncomeDeposit.StatusFlag = resultIncomeDeposit.StatusFlag;
                        newIncomeDeposit.Budget = viewModel.BudgetDetail * (-1) ?? 0;
                        newIncomeDeposit.Remark = "Actual Charge from Memo:" + viewModel.DocumentNumber;
                        var insertRevise = _incomeDepositService.Insert(newIncomeDeposit);
                     

                    }
                    else
                    {
                        var transResult = _budgetProposalTransFacade.ChangeBudgetPropTrans(result.ParentId, "memo");
                        if (!transResult)
                            throw new Exception("Cannot reserve for new budget. please try again.");


                    }
                    var approval = _approvalService.GetWhere(m => m.ParentId == viewModel.Id && !m.Deleted);
                    foreach (var a in approval)
                    {
                        a.ApproveStatus = false;
                        _approvalService.Update(a, a.Id);
                    }
                    await Session.SaveChangesAsync();
                }
                else
                {
                    var activity = result.Activity.Replace("Approval", "").Replace("-", "").Replace(" ", "");
                    var modelApprove1 = _approvalService.GetWhere(m => activity.Equals(m.ApproverLevel)
                    && m.ParentId == result.ParentId).ToList();
                    if (modelApprove1.Any())
                    {
                        var modelApprove = modelApprove1.LastOrDefault(m => m.ApproverUserName.ToLower().Equals(result.CreatedBy.ToLower()));
                        if (modelApprove != null)
                        {
                            modelApprove.ApproveStatus = true;
                            _approvalService.Update(modelApprove, modelApprove.Id);
                            Session.SaveChanges();

                        }
                    }
                    
                }
            }

            return result;
        }

      
        public async Task<MemoViewModel> GetDataBindMemo(int propLineid, string unitcode)
        {
            var viewModel = new MemoViewModel();
            if (propLineid != 0 && !string.IsNullOrEmpty(unitcode))
            {
                var propLine = _proposallineService.Find(propLineid);
               
                if (propLine != null)
                { 
                    var proposalRef = propLine.ProposalTable;
                    var propLineViewModel = _proposallineMapper.ToViewModel(propLine);
                  
                    viewModel.BudgetAPCode = propLineViewModel.BudgetPlan;
                    viewModel.ProposalLineId = propLineid;
                    viewModel.ProposalRef = _proposalMapper.ToProposalInfoViewModel(proposalRef);
                    viewModel.APCode = viewModel.ProposalRef.APCode.LastOrDefault(m => m.ProposalLineId == propLineid);
                    viewModel.ExpenseTopic = propLineViewModel.ExpenseTopic;
                }
            }
            return  viewModel;
        }
        public async Task<MemoViewModel> GetDataBindMemo(int propLineid)
        {
            var viewModel = new MemoViewModel();
            if (propLineid != 0 )
            {
                var propLine = _proposallineService.Find(propLineid);

                if (propLine != null)
                {
                    var proposalRef = propLine.ProposalTable;
                    var propLineViewModel = _proposallineMapper.ToViewModel(propLine);
                 ;
                    viewModel.BudgetAPCode = propLineViewModel.BudgetPlan;
                    viewModel.ProposalRef = _proposalMapper.ToProposalInfoViewModel(proposalRef);
                    viewModel.UnitCode = propLineViewModel.UnitCode;
                    viewModel.APCode = viewModel.ProposalRef.APCode.Find(m => m.ProposalLineId == propLineid);
                    viewModel.ExpenseTopic = propLineViewModel.ExpenseTopic;
                    viewModel.ProposalLineId = propLineid;
                    viewModel.MemoLines = new  List<MemoLineViewModel>();
                    viewModel.MemoApproval = new List<MemoApprovalViewModel>();
                   
                }
            }
            return viewModel;
        }


        public void SetFinalApproverDeleted(int id)
        {
            try
            {
                var finalApprovers =
                    _approvalService.GetWhere(m => m.ParentId == id && m.ApproverLevel == "อนุมัติขั้นสุดท้าย");
                foreach (var finalApprover in finalApprovers)
                {
                    _approvalService.Delete(finalApprover);
                }
                Session.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new HttpException(ex.Message);
            }
        }
        public string GetEmailInformAll(int workflowTableId)
        {
            string email = string.Empty;
            var memoModel = _workflowTableService.Find(m => m.Id == workflowTableId);
            if (memoModel != null)
            {

                email += string.IsNullOrEmpty(email) ? memoModel.CCEmail : string.Format(";{0}", memoModel.CCEmail).TrimEnd(';');
                email += string.IsNullOrEmpty(email) ? memoModel.InformEmail : string.Format(";{0}", memoModel.InformEmail).TrimEnd(';');
                email += string.IsNullOrEmpty(email) ? memoModel.AccountingEmail : string.Format(";{0}", memoModel.AccountingEmail).TrimEnd(';');

                if (memoModel.MemoApproval != null)
                {
                    foreach (var userapprove in memoModel.MemoApproval)
                    {
                        if (!string.IsNullOrEmpty(userapprove.ApproverEmail))
                        {
                            email += string.IsNullOrEmpty(email)
                                   ? userapprove.ApproverEmail
                                   : string.Format(";{0}", userapprove.ApproverEmail).TrimEnd(';');
                        }
                        else
                        {
                            var employee = _employeeTableService.FindByUsername(userapprove.ApproverUserName);
                            if (employee != null && !string.IsNullOrEmpty(employee.Email))
                            {
                                email += string.IsNullOrEmpty(email)
                                    ? employee.Email
                                    : string.Format(";{0}", employee.Email).TrimEnd(';');

                            }

                        }
                    }
                }
            }
            if (!string.IsNullOrEmpty(email))
            {
                string[] listEmail = email.TrimEnd(';').Split(';');
                string[] newlistEmail = listEmail.Distinct().ToArray();
                email = newlistEmail.Aggregate(string.Empty, (current, mail) => current + (string.IsNullOrEmpty(current) ? mail : string.Format(";{0}", mail)));
            }

            return email;

        }

        public MemoViewModel DeleteBy(int id, string username)
        {
            try
            {
                if (id > 0)
                {
                    var model = _workflowTableService.FirstOrDefault(m => m.Id == id);
                    var employeeViewModel = _employeeTableService.FindByUsername(username).ToViewModel();
                    if (model != null && employeeViewModel != null)
                    {
                        if (model.StatusFlag == 0)
                        {
                            var modelDelete = _mapper.DeleteBy(model, employeeViewModel);
                            if (modelDelete != null)
                            {
                                modelDelete = _workflowTableService.Update(modelDelete);
                                var value = Session.SaveChanges();
                                if (value > 0)
                                {
                                    var viewModel = _mapper.ToViewModel(modelDelete);
                                    return viewModel;
                                }
                            }
                        }
                        else if (model.StatusFlag == 9)
                        {
                            model.StatusFlag = 5;
                            model.CancelledDate = DateTime.Now.ToLocalTime();
                          
                            model.Status = DocumentStatusFlag.Cancelled.ToString();
                            model.DocumentStatus = DocumentStatusFlag.Cancelled.ToString();
                            var cancelModel =  _workflowTableService.Update(model , model.Id);
                            var result = _budgetProposalTransFacade.CancelBudgetPropTrans(model.Id, "memo");
                            if (result)
                            {
                                Session.SaveChanges();
                            }
                            else
                            {
                                throw new Exception("ไม่สามารถลบรายการได้ กรุณาติดต่อผู้ดูแลระบบ");
                            }
                           return _mapper.ToViewModel(cancelModel);
                        }
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                throw new HttpException(ex.Message);
            }
        }


    }
}