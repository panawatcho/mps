﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using CrossingSoft.Framework.Infrastructure.Session;
using TheMall.MPS.API.Facades.Abstracts;
using TheMall.MPS.API.Mappers;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.API.Services;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Models.Master;
using TheMall.MPS.ViewModels.Master;

namespace TheMall.MPS.API.Facades
{
    public interface ITypeProposalFacade : IBaseMasterFacade<MPS_M_TypeProposal, TypeProposalViewModel>
    {
       

    }

    public class TypeProposalFacade : BaseMasterFacade<MPS_M_TypeProposal, TypeProposalViewModel>, ITypeProposalFacade
    {
        private readonly IMPSSession _session;
        private readonly ITypeProposalService _service;

        public TypeProposalFacade(IMPSSession session, ITypeProposalService service) 
            : base(session, service)
        {
            _session = session;
            _service = service;
        }

        public override async Task<TypeProposalViewModel> Create(TypeProposalViewModel viewModel)
        {
            try
            {
                var model = _service.Insert(viewModel.ToModel());
                await _session.SaveChangesAsync();
                return  model.ToViewModel();
            }
            catch (Exception exception)
            {

                throw new Exception("Cannot insert duplicate code");
            }
        }

        public override async Task<TypeProposalViewModel> Update(TypeProposalViewModel viewModel)
        {
            var model = _service.Update(viewModel.ToModel());
            await _session.SaveChangesAsync();
            return model.ToViewModel();
        }

        public override async Task<TypeProposalViewModel> Delete(TypeProposalViewModel viewModel)
        {
            try
            {
                _service.Delete(viewModel.ToModel());
                await _session.SaveChangesAsync();
                return viewModel;
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
        }

      

    }
}