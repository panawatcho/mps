﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TheMall.MPS.API.Facades.Abstracts;
using TheMall.MPS.API.Services;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Models.Budget;
using TheMall.MPS.Models.Proposal;
using TheMall.MPS.ViewModels.Proposal;

namespace TheMall.MPS.API.Facades
{
    public interface IBudgetDepositTransFacade
    {

        bool CreateDepositTransStepCreateIncome(MPS_IncomeDeposit incomeDeposit);
        bool CancelDepositTransStepCreateIncome(MPS_IncomeDeposit incomeDeposit);
        bool ReviseDepositTransStepCreateIncome(MPS_IncomeDeposit incomeDeposit,MPS_BudgetDepositTrans budgetTrans);
        bool CreateDepositTransStepCreateDeposit(int proposalId);
        bool ReviseProposalDepositTrans(int proposalId);
        bool CancelProposalDepositTrans(int proposalId);
        bool ReturnDepositTransStepCloseProposal(int propId);
    }

    public class BudgetDepositTransFacade : BaseFacade, IBudgetDepositTransFacade
    {
        private readonly IMPSSession _session;
        private readonly IBudgetDepositTransService _depositTransService;
        private readonly IProposalIncomeDepositService _incomeDepositService;
        private readonly IProposalService _proposalService;
        private readonly IProposalIncomeDepositService _proposalIncomeDepositService;
        public BudgetDepositTransFacade(
             IMPSSession session,
            IBudgetDepositTransService depositTransService, 
            IProposalIncomeDepositService incomeDepositService, 
            IProposalService proposalService,
            IProposalIncomeDepositService proposalIncomeDepositService)
            {
            _session = session;
            _depositTransService = depositTransService;
                _incomeDepositService = incomeDepositService;
                _proposalService = proposalService;
            _proposalIncomeDepositService = proposalIncomeDepositService;
            }

        public bool CreateDepositTrans(MPS_BudgetDepositTrans depositTrans)
        {
            var result = false;
            var propDeposit = _proposalService.Find(m => m.Id == depositTrans.ProposalDepositId);
            if (propDeposit == null)
                throw new Exception(string.Format(Resources.Error.BudgetTransNotFound, depositTrans.DocumentNumber));
           
            var budgetTrans = _depositTransService.GetBudgetDepositTransTotal(depositTrans.ProposalDepositId);
            var budgetBalance = (propDeposit.TotalBudget - budgetTrans);
            result = budgetBalance >= depositTrans.Amount;
            if (result == false)
                throw new Exception(string.Format(Resources.Error.ReturnInterfaceErrorBudgetExceed, propDeposit.DocumentNumber, propDeposit.Title));
            var depositTransNew = depositTrans;
            depositTransNew.Status = "Reserve";
            depositTransNew.DocumentStatus = "Reserve";
            depositTransNew.TransDate = DateTime.Now;
            depositTransNew.Amount = depositTrans.Amount;
            _depositTransService.Insert(depositTransNew);
                result = true;
            

            return result;
        }
        public bool ChangeDepositTrans(MPS_BudgetDepositTrans depositTrans)
        {
            var result = false;
            if (depositTrans != null)
            {
                var propDeposit = _proposalService.Find(m => m.Id == depositTrans.ProposalDepositId);
                var budgetTrans = _depositTransService.LastOrDefault(m => m.ProposalDepositId == depositTrans.ProposalDepositId
                                                                                        &&
                                                                                        m.DocumentType == depositTrans.DocumentType
                                                                                        &&
                                                                                        m.DocumentId ==depositTrans.DocumentId
                                                                                        && m.Status == "Reserve"
                                                                                        && !m.InActive);
                if (budgetTrans == null)
                    throw new Exception(string.Format(Resources.Error.BudgetTransNotFound, depositTrans.DocumentNumber));

                var budgetTransTotal = _depositTransService.GetBudgetDepositTransTotal(depositTrans.ProposalDepositId);
                var budgetBalance = (propDeposit.TotalBudget - budgetTransTotal + budgetTrans.Amount);

                result = budgetBalance >= depositTrans.Amount;

                if (result == false)
                    throw new Exception(string.Format(Resources.Error.ReturnInterfaceErrorBudgetExceed, propDeposit.DocumentNumber, propDeposit.Title));

                var depositTransOld = new MPS_BudgetDepositTrans()
                {

                   Status = "Cancel",
                   DocumentStatus = "Revise",
                   ProposalDepositId = depositTrans.ProposalDepositId,
                   DocumentId = depositTrans.DocumentId,
                   DocumentType = depositTrans.DocumentType,
                   DocumentNumber = depositTrans.DocumentNumber,
                   TransDate = DateTime.Now,
                   Amount = budgetTrans.Amount * (-1),
                    
                };
               
                depositTransOld = _depositTransService.Insert(depositTransOld);

                if (depositTrans.Amount > 0)
                {
                    var newDepositTrans =  new MPS_BudgetDepositTrans()
                    {
                        Status = "Reserve",
                        DocumentStatus = "Revise",
                        ProposalDepositId = depositTrans.ProposalDepositId,
                        DocumentId = depositTrans.DocumentId,
                        DocumentType = depositTrans.DocumentType,
                        DocumentNumber = depositTrans.DocumentNumber,
                        TransDate = DateTime.Now,
                        Amount = depositTrans.Amount,
                    };
                   newDepositTrans = _depositTransService.Insert(newDepositTrans);
                }
                result = true;
            }

            return result;
        }
        public bool CancelDepositTrans(MPS_BudgetDepositTrans depositTrans)
        {
            var result = false;
            if (depositTrans != null)
            {
                depositTrans.Status = "Cancel";
                depositTrans.DocumentStatus = "Cancel";
                depositTrans.Amount = depositTrans.Amount*(-1);
                depositTrans.TransDate = DateTime.Now;
                _depositTransService.Insert(depositTrans);
                result = true;
            }

            return result;
        }

        #region Income Deposit
        public bool CreateDepositTransStepCreateIncome(MPS_IncomeDeposit incomeDeposit)
        {
            var result = false;
            if(incomeDeposit!=null)
                result = CreateDepositTrans(new MPS_BudgetDepositTrans()
                {
                    ProposalDepositId = incomeDeposit.ProposalDepositRefID,
                    RefDocumentId = incomeDeposit.Id,
                    DocumentType = "IncomeDeposit",
                    DocumentId = incomeDeposit.ParentId,
                    DocumentNumber = incomeDeposit.ProposalDepositRefDoc,
                    Amount = incomeDeposit.Budget,
                });
                if (result)
                    _session.SaveChanges();

            return result;
        }

        public bool ReviseDepositTransStepCreateIncome(MPS_IncomeDeposit incomeDeposit, MPS_BudgetDepositTrans budgetTrans)
        {
            var result = false;
            if (incomeDeposit != null && budgetTrans!=null)
            {
                var propDeposit = _proposalService.Find(m => m.Id == incomeDeposit.ProposalDepositRefID);
                //var budgetTrans = _depositTransService.LastOrDefault(m => m.ProposalDepositId == incomeDeposit.ProposalDepositRefID
                //                                                                        &&
                //                                                                        m.DocumentType == "IncomeDeposit"
                //                                                                        &&
                //                                                                        m.RefDocumentId == incomeDeposit.Id
                //                                                                        &&
                //                                                                        m.DocumentId == incomeDeposit.ParentId
                //                                                                        && m.Status == "Reserve"
                //                                                                        && !m.InActive);
                //if (budgetTrans == null)
                //    throw new Exception(string.Format(Resources.Error.BudgetTransNotFound, incomeDeposit.ProposalDepositRefDoc));

                var budgetTransTotal = _depositTransService.GetBudgetDepositTransTotal(incomeDeposit.ProposalDepositRefID);
                var budgetBalance = (propDeposit.TotalBudget - budgetTransTotal + budgetTrans.Amount);

                result = budgetBalance >= incomeDeposit.Budget;
                if (result == false)
                    throw new Exception(string.Format(Resources.Error.ReturnInterfaceErrorBudgetExceed, propDeposit.DocumentNumber, propDeposit.Title));

                var depositTransOld = new MPS_BudgetDepositTrans()
                {

                    Status = "Cancel",
                    DocumentStatus = "Revise",
                    ProposalDepositId = incomeDeposit.ProposalDepositRefID,
                    RefDocumentId = incomeDeposit.Id,
                    DocumentId = incomeDeposit.ParentId,
                    DocumentType = "IncomeDeposit",
                    DocumentNumber = budgetTrans.DocumentNumber,
                    TransDate = DateTime.Now,
                    Amount = budgetTrans.Amount * (-1),

                };

                depositTransOld = _depositTransService.Insert(depositTransOld);

                if (incomeDeposit.Budget > 0)
                {
                    var newDepositTrans = new MPS_BudgetDepositTrans()
                    {
                        Status = "Reserve",
                        DocumentStatus = "Revise",
                        ProposalDepositId = incomeDeposit.ProposalDepositRefID,
                        RefDocumentId = incomeDeposit.Id,
                        DocumentId = incomeDeposit.ParentId,
                        DocumentType = "IncomeDeposit",
                        DocumentNumber = budgetTrans.DocumentNumber,
                        TransDate = DateTime.Now,
                        Amount = incomeDeposit.Budget,
                    };
                    newDepositTrans = _depositTransService.Insert(newDepositTrans);
                }
                result = true;
                _session.SaveChanges();
            }

            return result;
        }

        public bool CancelDepositTransStepCreateIncome(MPS_IncomeDeposit incomeDeposit)
        {
            var result = false;
            if (incomeDeposit != null)
            {
                var budgetTrans =
                    _depositTransService.LastOrDefault(m => m.ProposalDepositId == incomeDeposit.ProposalDepositRefID
                                                            &&
                                                            m.DocumentType == "IncomeDeposit"
                                                            &&
                                                            m.RefDocumentId == incomeDeposit.Id
                                                            &&
                                                            m.DocumentId == incomeDeposit.ParentId
                                                            && m.Status == "Reserve"
                                                            && !m.InActive);
                if (budgetTrans!=null && budgetTrans.Amount > 0)
                result = CancelDepositTrans(new MPS_BudgetDepositTrans()
                {
                    ProposalDepositId = incomeDeposit.ProposalDepositRefID,
                    RefDocumentId = incomeDeposit.Id,
                    DocumentType = "IncomeDeposit",
                    DocumentId = incomeDeposit.ParentId,
                    DocumentNumber = incomeDeposit.ProposalDepositRefDoc,
                    Amount = budgetTrans.Amount,
                });
            }
            if (result)
                _session.SaveChanges();

            return result;
        }

        #endregion
        #region Proposal Deposit
   
        public bool CreateDepositTransStepCreateDeposit(int proposalId)
        {
            var result = false;
            var prosaldeposit = _proposalService.Find(m => m.Id == proposalId && !m.Deleted);
            if (prosaldeposit != null && prosaldeposit.DepositId!=null)
            {
                result = CreateDepositTrans(new MPS_BudgetDepositTrans()
                {
                    ProposalDepositId = (int)prosaldeposit.DepositId,
                    DocumentType = "DepositInternal",
                    DocumentId = prosaldeposit.Id,
                    DocumentNumber = prosaldeposit.DocumentNumber,
                    Amount = prosaldeposit.TotalBudget,
                });
                if (result)
                    _session.SaveChanges();
            }

            return result;
        }

        public bool ReviseProposalDepositTrans(int proposalId)
        {
            var result = false;
            var prosaldeposit = _proposalService.Find(m => m.Id == proposalId && !m.Deleted);
            if (prosaldeposit != null && prosaldeposit.DepositId!=null)
            {
                result = ChangeDepositTrans(new MPS_BudgetDepositTrans()
                {
                    ProposalDepositId = (int)prosaldeposit.DepositId,
                    DocumentType = "DepositInternal",
                    DocumentId = prosaldeposit.Id,
                    DocumentNumber = prosaldeposit.DocumentNumber,
                    Amount = prosaldeposit.TotalBudget,
                });
                if (result)
                    _session.SaveChanges();
            }

            return result;
        }
        
        public bool CancelProposalDepositTrans(int proposalId)
        {
            var result = false;
            var prosaldeposit = _proposalService.Find(m => m.Id == proposalId && !m.Deleted);
            if (prosaldeposit != null && prosaldeposit.DepositId!=null)
            {
                result = CancelDepositTrans(new MPS_BudgetDepositTrans()
                {
                    ProposalDepositId = (int)prosaldeposit.DepositId,
                    DocumentType = "DepositInternal",
                    DocumentId = prosaldeposit.Id,
                    DocumentNumber = prosaldeposit.DocumentNumber,
                    Amount = prosaldeposit.TotalBudget,
                });
                if (result)
                    _session.SaveChanges();
            }

            return result;
        }

        #endregion

        public bool ReturnDepositTransStepCloseProposal(int propId)
        {
            var listIncomeDeposit = _incomeDepositService.GetWhere(m => m.ParentId == propId && !m.Deleted && m.ActualCharge).ToListSafe();
            var result = false;
            if (listIncomeDeposit.Any())
            {
                var groupDeposit = listIncomeDeposit.GroupBy(m => m.ProposalDepositRefDoc).Last();
                foreach (var incomeDeposit in groupDeposit.ToList())
                {
                    if (incomeDeposit != null)
                    {
                        var budgetTrans =
                            _depositTransService.LastOrDefault(
                                m => m.ProposalDepositId == incomeDeposit.ProposalDepositRefID
                                     &&
                                     m.DocumentType == "IncomeDeposit"
                                     &&
                                     m.RefDocumentId == incomeDeposit.Id
                                     &&
                                     m.DocumentId == incomeDeposit.ParentId
                                     && m.Status == "Reserve"
                                     && !m.InActive);
                        if (budgetTrans != null && budgetTrans.Amount > 0)
                        {
                            var returnTrans = new MPS_BudgetDepositTrans()
                            {
                                Status = "Return",
                                DocumentStatus = "Return",
                                ProposalDepositId = incomeDeposit.ProposalDepositRefID,
                                RefDocumentId = incomeDeposit.Id,
                                DocumentType = "IncomeDeposit",
                                DocumentId = incomeDeposit.ParentId,
                                DocumentNumber = incomeDeposit.ProposalDepositRefDoc,
                                Amount = listIncomeDeposit
                                    .Where(m => m.ProposalDepositRefDoc == incomeDeposit.ProposalDepositRefDoc)
                                    .Sum(m => m.Budget)*(-1),
                                TransDate = DateTime.Now
                            };
                            _depositTransService.Insert(returnTrans);
                            result = true;
                           
                        }
                    }
                    if (result)
                        _session.SaveChanges();
                }
            }

            return result;
        }
    }
}