﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CrossingSoft.Framework.Infrastructure.Session;
using NLog.LayoutRenderers;
using TheMall.MPS.API.Facades.Abstracts;
using TheMall.MPS.API.Mappers;
using TheMall.MPS.API.Services;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Models.Budget;
using TheMall.MPS.Models.Enums;
using TheMall.MPS.Models.Procurement.GR;
using TheMall.MPS.Models.Procurement.PO;
using TheMall.MPS.Models.Procurement.PR;
using TheMall.MPS.Models.Proposal;
using TheMall.MPS.ViewModels.InterfaceToMprocurement;
using TheMall.MPS.ViewModels.InterfaceToMprocurement.GR;
using TheMall.MPS.ViewModels.InterfaceToMprocurement.PO;
using TheMall.MPS.ViewModels.InterfaceToMprocurement.PR;

namespace TheMall.MPS.API.Facades
{
    public interface IBudgetProposalTransFacade
    {
        //ReturnMessage CreatePR(RequestViewModel viewModel);
        //ReturnMessage CancelPR(RequestViewModel viewModel);
        //ReturnMessage ChangePR(RequestViewModel viewModel);
        bool CreateBudgetPropTrans(int workflowTableId, string docType);
        bool CancelBudgetPropTrans(int workflowTableId, string docType);
        bool ChangeBudgetPropTrans(int workflowTableId, string docType);
        bool CloseAdvanceBudgetPropTrans(int workflowTableId);

        ReturnMessage CreatePR(PRRequestViewModel viewModel);
        ReturnMessage CancelPR(PRRequestViewModel viewModel);
        ReturnMessage ChangePR(PRRequestViewModel viewModel);
        ReturnMessage CreatePO(PORequestViewModel viewModel);
        ReturnMessage CancelPO(PORequestViewModel viewModel);
        ReturnMessage ChangePO(PORequestViewModel viewModel);
        ReturnMessage GoodsReceipt(GRRequestViewModel viewModel);
    }

    public class BudgetProposalTransFacade : BaseFacade, IBudgetProposalTransFacade
    {
        private readonly IBudgetProposalTransService _budgetProposalTransService;
        private readonly IProposalLineService _proposalLineService;
        private readonly IMemoService _memoService;
        private readonly IPettyCashService _pettyCashService;
        private readonly ICashAdvanceService _cashAdvanceService;
        private readonly ICashClearingService _cashClearingService;
        private readonly IMPSSession _session;
        private readonly IPurchReqMapper _purchReqMapper;
        private readonly IAPcodeService _apCodeService;
        private readonly IPurchReqTableService _purchReqTableService;
        private readonly IPurchReqLineService _purchReqLineService;
        private readonly IPurchOrderMapper _purchOrderMapper;
        private readonly IPurchOrderTableService _purchOrderTableService;
        private readonly IPurchOrderLineService _purchOrderLineService;
        private readonly IGoodsReceiptMapper _goodsReceiptMapper;
        private readonly IGoodsReceiptTableService _goodsReceiptTableService;
        private readonly IGoodsReceiptLineService _goodsReceiptLineService;
        private readonly IProposalBudgetPlanService _proposalBudgetPlanService;
        private readonly IProposalService _proposalService;
        public BudgetProposalTransFacade(IBudgetProposalTransService budgetProposalTransService,
            IProposalLineService proposalLineService,
            IMPSSession session,
            IMemoService memoService,
            IPettyCashService pettyCashService,
            ICashAdvanceService cashAdvanceService,
            ICashClearingService cashClearingService,
            IPurchReqMapper purchReqMapper,
            IAPcodeService apCodeService,
            IPurchReqTableService purchReqTableService,
            IPurchReqLineService purchReqLineService,
            IPurchOrderMapper purchOrderMapper,
            IPurchOrderTableService purchOrderTableService,
            IPurchOrderLineService purchOrderLineService,
            IGoodsReceiptMapper goodsReceiptMapper,
            IGoodsReceiptTableService goodsReceiptTableService,
            IGoodsReceiptLineService goodsReceiptLineService,
            IProposalBudgetPlanService proposalBudgetPlanService, IProposalService proposalService)
        {
            _budgetProposalTransService = budgetProposalTransService;
            _proposalLineService = proposalLineService;
            _session = session;
            _memoService = memoService;
            _pettyCashService = pettyCashService;
            _cashAdvanceService = cashAdvanceService;
            _cashClearingService = cashClearingService;
            _purchReqMapper = purchReqMapper;
            _apCodeService = apCodeService;
            _purchReqTableService = purchReqTableService;
            _purchReqLineService = purchReqLineService;
            _purchOrderMapper = purchOrderMapper;
            _purchOrderTableService = purchOrderTableService;
            _purchOrderLineService = purchOrderLineService;
            _goodsReceiptMapper = goodsReceiptMapper;
            _goodsReceiptTableService = goodsReceiptTableService;
            _goodsReceiptLineService = goodsReceiptLineService;
            _proposalBudgetPlanService = proposalBudgetPlanService;
            _proposalService = proposalService;
        }

        #region MProcurement Interface methods

        #region PR
        public ReturnMessage CreatePR(PRRequestViewModel viewModel)
        {
            try
            {
                var result = true;
                if (viewModel == null)
                    throw new Exception(string.Format(Resources.Error.ParameterNotFound, ""));
                if (ValidatePRHeaderViewModel(viewModel.Header))
                {
                    var purchReqTable = _purchReqMapper.ToPurchReqTable(viewModel.Header);
                    var reqId = purchReqTable.REQ_ID;
                    var checkExistPR = _purchReqTableService.Find(reqId);
                    if (checkExistPR != null && checkExistPR.StatusFlag != DocumentStatusFlagId.Reject)
                        throw new Exception(
                            string.Format("Error! Cannot create duplicate Purchase Requisition number {0}, REQ_ID {1} ", purchReqTable.PR_NUM, purchReqTable.REQ_ID));

                    var purchReqLines = _purchReqMapper.ToPurchReqLines(viewModel.Items);
                    if (purchReqLines != null && purchReqLines.Any())
                    {
                        //Validate PURTYPE
                        var firstLine = purchReqLines.FirstOrDefault();
                        if (firstLine != null && firstLine.PURTYPE != Budget.PurchaseType.Proposal)
                            throw new Exception("Error! Cannot create Purchase Requisition PURTYPE : 'M' ");

                        var purchReqLineGrouping = purchReqLines.GroupBy(m => new { m.PROPOSALID, m.COSTCENTER, m.APCODE });
                        foreach (var purchReqGroup in purchReqLineGrouping)
                        {
                            var key = purchReqGroup.Key;
                            if (key != null
                                && !string.IsNullOrEmpty(key.PROPOSALID)
                                && !string.IsNullOrEmpty(key.COSTCENTER)
                                && !string.IsNullOrEmpty(key.APCODE))
                            {
                                var reserveAmount = purchReqGroup.Sum(m => m.ACTUALPRICE);

                                var apCode =
                                    _apCodeService.Find(
                                        m => m.APCode.Equals(key.APCODE, StringComparison.InvariantCultureIgnoreCase));
                                if (apCode != null)
                                {
                                    var validateBudgetResult = ValidateBudget(new RequestViewModel()
                                    {
                                        ProposalNumber = key.PROPOSALID,
                                        Amount = reserveAmount,
                                        APCode = apCode.APCode,
                                        ExpenseTopic = apCode.ExpenseTopicCode,
                                        UnitCode = key.COSTCENTER,
                                        Status = Budget.BudgetStatus.Reserve,
                                        DocumentStatus = "Create",
                                        DocumentType = Budget.DocumentType.PurchaseRequisition,
                                        DocumentId = purchReqTable.REQ_ID,
                                        DocumentNumber = purchReqTable.PR_NUM,
                                        EmployeeId = purchReqGroup.First().REQUESTFORID
                                    });
                                    if (validateBudgetResult)
                                    {
                                        var lineGroup = purchReqGroup.GroupBy(m => m.APDESC);
                                        foreach (var purchReqLineGroup in lineGroup)
                                        {
                                            var createResult = CreateTrans(new RequestViewModel()
                                            {
                                                ProposalNumber = key.PROPOSALID,
                                                Amount = purchReqLineGroup.Sum(m => m.ACTUALPRICE),
                                                APCode = apCode.APCode,
                                                APDescription = purchReqLineGroup.Key,
                                                ExpenseTopic = apCode.ExpenseTopicCode,
                                                UnitCode = key.COSTCENTER,
                                                Status = Budget.BudgetStatus.Reserve,
                                                DocumentStatus = "Create",
                                                DocumentType = Budget.DocumentType.PurchaseRequisition,
                                                DocumentId = purchReqTable.REQ_ID,
                                                DocumentNumber = purchReqTable.PR_NUM,
                                                EmployeeId = purchReqGroup.First().REQUESTFORID
                                            });

                                            if (createResult == false)
                                            {
                                                result = false;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        result = false;
                                    }
                                }
                                else
                                {
                                    result = false;
                                }
                            }
                            else
                            {
                                result = false;
                            }

                            if (!result)
                            {
                                return new ReturnMessage()
                                {
                                    Status = false,
                                    Message = "Error! please check your parameters and try again."

                                };
                            }
                        }
                    }
                    else
                    {
                        result = false;
                    }


                    if (result)
                    {
                        if (checkExistPR != null && checkExistPR.StatusFlag == DocumentStatusFlagId.Reject)
                        {
                            _purchReqTableService.Delete(checkExistPR);
                            _session.SaveChanges();
                        }

                        SaveCreatePR(purchReqTable, purchReqLines);

                        _session.SaveChanges();

                        return new ReturnMessage()
                        {
                            Status = true
                        };
                    }
                }

                return new ReturnMessage()
                {
                    Status = false,
                    Message = "Error! please check your parameters and try again."

                };
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }

        public ReturnMessage CancelPR(PRRequestViewModel viewModel)
        {
            try
            {
                var result = true;
                if (viewModel == null)
                    throw new Exception(string.Format(Resources.Error.ParameterNotFound, ""));
                if (ValidatePRHeaderViewModel(viewModel.Header))
                {
                    var reqPurchReqTable = _purchReqMapper.ToPurchReqTable(viewModel.Header);

                    //Find PR
                    var purchReqTable = _purchReqTableService.Find(reqPurchReqTable.REQ_ID);
                    if (purchReqTable == null)
                        throw new Exception(
                            string.Format("Error! Cannot cancel Purchase Requisition number {0}, REQ_ID {1}. PR could not be found.", reqPurchReqTable.PR_NUM, reqPurchReqTable.REQ_ID));
                    if (purchReqTable.StatusFlag != DocumentStatusFlagId.Active)
                        throw new Exception(
                            string.Format("Error! Cannot cancel Purchase Requisition number {0}, REQ_ID {1}. PR status is invalid", purchReqTable.PR_NUM, purchReqTable.REQ_ID));

                    var purchReqLines = purchReqTable.PurchReqLines;
                    if (purchReqLines != null && purchReqLines.Any())
                    {
                        var purchReqLineGrouping = purchReqLines.GroupBy(m => new { m.PROPOSALID, m.COSTCENTER, m.APCODE, m.APDESC });
                        foreach (var purchReqGroup in purchReqLineGrouping)
                        {
                            var key = purchReqGroup.Key;
                            if (key != null
                                && !string.IsNullOrEmpty(key.PROPOSALID)
                                && !string.IsNullOrEmpty(key.COSTCENTER)
                                && !string.IsNullOrEmpty(key.APCODE))
                            {
                                var apCode =
                                    _apCodeService.Find(
                                        m => m.APCode.Equals(key.APCODE, StringComparison.InvariantCultureIgnoreCase));
                                if (apCode != null)
                                {
                                    var cancelResult = CancelTrans(new RequestViewModel()
                                    {
                                        ProposalNumber = key.PROPOSALID,
                                        APCode = apCode.APCode,
                                        APDescription = key.APDESC,
                                        ExpenseTopic = apCode.ExpenseTopicCode,
                                        UnitCode = key.COSTCENTER,
                                        Status = Budget.BudgetStatus.Cancel,
                                        DocumentStatus = Budget.BudgetStatus.Cancel,
                                        DocumentType = Budget.DocumentType.PurchaseRequisition,
                                        DocumentId = purchReqTable.REQ_ID,
                                        DocumentNumber = purchReqTable.PR_NUM,
                                        EmployeeId = purchReqGroup.First().REQUESTFORID
                                    }, Budget.BudgetStatus.Reserve);

                                    if (cancelResult == false)
                                    {
                                        result = false;
                                    }
                                }
                                else
                                {
                                    result = false;
                                }

                            }
                            else
                            {
                                result = false;
                            }

                            if (!result)
                            {
                                return new ReturnMessage()
                                {
                                    Status = false,
                                    Message = "Error! please check your parameters and try again."

                                };
                            }
                        }
                    }
                    else
                    {
                        result = false;
                    }

                    if (result)
                    {
                        SaveCancelPR(purchReqTable, purchReqLines);
                        _session.SaveChanges();

                        return new ReturnMessage()
                        {
                            Status = true
                        };
                    }

                }

                return new ReturnMessage()
                {
                    Status = false,
                    Message = "Error! please check your parameters and try again."

                };
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }

        public ReturnMessage ChangePR(PRRequestViewModel viewModel)
        {
            try
            {
                var result = true;
                if (viewModel == null)
                    throw new Exception(string.Format(Resources.Error.ParameterNotFound, ""));
                if (ValidatePRHeaderViewModel(viewModel.Header))
                {
                    var reqPurchReqTable = _purchReqMapper.ToPurchReqTable(viewModel.Header);

                    //Find PR
                    var purchReqTable = _purchReqTableService.Find(reqPurchReqTable.REQ_ID);
                    if (purchReqTable == null)
                        throw new Exception(
                            string.Format("Error! Cannot change Purchase Requisition number {0}, REQ_ID {1}. PR could not be found.", reqPurchReqTable.PR_NUM, reqPurchReqTable.REQ_ID));
                    if (purchReqTable.StatusFlag != DocumentStatusFlagId.Active)
                        throw new Exception(
                            string.Format("Error! Cannot change Purchase Requisition number {0}, REQ_ID {1}. PR status is invalid", purchReqTable.PR_NUM, purchReqTable.REQ_ID));
                    var reqPurchReqLines = _purchReqMapper.ToPurchReqLines(viewModel.Items);

                    var purchReqLines = purchReqTable.PurchReqLines;
                    if (purchReqLines != null && purchReqLines.Any())
                    {
                        //Validate PURTYPE
                        var firstLine = purchReqLines.FirstOrDefault();
                        if (firstLine != null && firstLine.PURTYPE != Budget.PurchaseType.Proposal)
                            throw new Exception("Error! Cannot create Purchase Requisition PURTYPE : 'M' ");

                        var purchReqLineGrouping = purchReqLines.GroupBy(m => new { m.PROPOSALID, m.COSTCENTER, m.APCODE });
                        foreach (var purchReqGroup in purchReqLineGrouping)
                        {
                            var key = purchReqGroup.Key;
                            if (key != null
                                && !string.IsNullOrEmpty(key.PROPOSALID)
                                && !string.IsNullOrEmpty(key.COSTCENTER)
                                && !string.IsNullOrEmpty(key.APCODE))
                            {
                                var apCode =
                                    _apCodeService.Find(
                                        m => m.APCode.Equals(key.APCODE, StringComparison.InvariantCultureIgnoreCase));
                                if (apCode != null)
                                {
                                    var reserveAmount = new Decimal(0.00);
                                    if (reqPurchReqLines != null && reqPurchReqLines.Any())
                                    {
                                        var groupReqPurchReqLines =
                                            reqPurchReqLines.Where(
                                                m =>
                                                    m.PROPOSALID == key.PROPOSALID && m.COSTCENTER == key.COSTCENTER &&
                                                    m.APCODE == key.APCODE);
                                        if (groupReqPurchReqLines.Any())
                                        {
                                            reserveAmount = groupReqPurchReqLines.Sum(m => m.ACTUALPRICE);
                                        }
                                    }

                                    var validateBudgetResult = ValidateBudget(new RequestViewModel()
                                    {
                                        ProposalNumber = key.PROPOSALID,
                                        Amount = reserveAmount,
                                        APCode = apCode.APCode,
                                        ExpenseTopic = apCode.ExpenseTopicCode,
                                        UnitCode = key.COSTCENTER,
                                        Status = Budget.BudgetStatus.Reserve,
                                        DocumentStatus = "Revise",
                                        DocumentType = Budget.DocumentType.PurchaseRequisition,
                                        DocumentId = purchReqTable.REQ_ID,
                                        DocumentNumber = purchReqTable.PR_NUM,
                                        EmployeeId = purchReqGroup.First().REQUESTFORID
                                    }, true, Budget.BudgetStatus.Reserve);
                                    if (validateBudgetResult)
                                    {
                                        var lineGroup = purchReqGroup.GroupBy(m => m.APDESC);
                                        foreach (var purchReqLineGroup in lineGroup)
                                        {
                                            var amount = new decimal(0);
                                            if (reqPurchReqLines != null && reqPurchReqLines.Any())
                                            {
                                                var groupReqPurchReqLines =
                                                    reqPurchReqLines.Where(
                                                        m =>
                                                            m.PROPOSALID == key.PROPOSALID && m.COSTCENTER == key.COSTCENTER &&
                                                            m.APCODE == key.APCODE && m.APDESC == purchReqLineGroup.Key);
                                                if (groupReqPurchReqLines.Any())
                                                {
                                                    amount = groupReqPurchReqLines.Sum(m => m.ACTUALPRICE);
                                                }
                                            }

                                            var changeResult = ChangeTrans(new RequestViewModel()
                                            {
                                                ProposalNumber = key.PROPOSALID,
                                                Amount = amount,
                                                APCode = apCode.APCode,
                                                APDescription = purchReqLineGroup.Key,
                                                ExpenseTopic = apCode.ExpenseTopicCode,
                                                UnitCode = key.COSTCENTER,
                                                Status = Budget.BudgetStatus.Reserve,
                                                DocumentStatus = "Revise",
                                                DocumentType = Budget.DocumentType.PurchaseRequisition,
                                                DocumentId = purchReqTable.REQ_ID,
                                                DocumentNumber = purchReqTable.PR_NUM,
                                                EmployeeId = purchReqGroup.First().REQUESTFORID
                                            }, Budget.BudgetStatus.Reserve);
                                            if (changeResult == false)
                                            {
                                                result = false;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        result = false;
                                    }



                                }
                                else
                                {
                                    result = false;
                                }
                            }
                            else
                            {
                                result = false;
                            }

                            if (!result)
                            {
                                return new ReturnMessage()
                                {
                                    Status = false,
                                    Message = "Error! please check your parameters and try again."

                                };
                            }
                        }
                    }
                    else
                    {
                        result = false;
                    }

                    if (result)
                    {
                        DeletePR(purchReqTable);

                        SaveCreatePR(reqPurchReqTable, reqPurchReqLines);

                        _session.SaveChanges();

                        return new ReturnMessage()
                        {
                            Status = true
                        };
                    }

                }

                return new ReturnMessage()
                {
                    Status = false,
                    Message = "Error! please check your parameters and try again."

                };
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }
        #endregion

        #region PO
        public ReturnMessage CreatePO(PORequestViewModel viewModel)
        {
            try
            {
                var result = true;
                if (viewModel == null)
                    throw new Exception(string.Format(Resources.Error.ParameterNotFound, ""));
                if (ValidatePOHeaderViewModel(viewModel.Header))
                {
                    var purchOrderTable = _purchOrderMapper.ToPurchOrderTable(viewModel.Header);
                    //Find PO
                    var poId = purchOrderTable.PO_ID;
                    var checkExistPO = _purchOrderTableService.Find(poId);
                    if (checkExistPO != null && checkExistPO.StatusFlag != DocumentStatusFlagId.Reject)
                        throw new Exception(
                            string.Format("Error! Cannot create duplicate Purchase order number {0}, PO_ID {1} ", purchOrderTable.PO_NUM, purchOrderTable.PO_ID));
                    //Find PR
                    var purchReqTable = _purchReqTableService.Find(purchOrderTable.REQ_ID);
                    if (purchReqTable == null)
                        throw new Exception(
                            string.Format("Error! Purchase Requisition number {0}, REQ_ID {1}. PR could not be found.", purchOrderTable.PR_NUM, purchOrderTable.REQ_ID));
                    if (purchReqTable.StatusFlag != DocumentStatusFlagId.Active)
                        throw new Exception(
                            string.Format("Error! Cannot create Purchase order number {0}, PO_ID {1}. PR status is invalid", purchOrderTable.PO_NUM, purchOrderTable.PO_ID));

                    var purchOrderLines = _purchOrderMapper.ToPurchOrderLines(viewModel.Items);


                    if (purchOrderLines != null && purchOrderLines.Any())
                    {
                        var purchOrderLineGrouping =
                            purchOrderLines.GroupBy(m => new { m.PROPOSALID, m.COSTCENTER, m.APCODE });
                        foreach (var purchOrderGroup in purchOrderLineGrouping)
                        {
                            var key = purchOrderGroup.Key;
                            if (key != null
                                && !string.IsNullOrEmpty(key.PROPOSALID)
                                && !string.IsNullOrEmpty(key.COSTCENTER)
                                && !string.IsNullOrEmpty(key.APCODE))
                            {
                                var apCode =
                                    _apCodeService.Find(
                                        m => m.APCode.Equals(key.APCODE, StringComparison.InvariantCultureIgnoreCase));
                                if (apCode != null)
                                {
                                    var reserveAmount = new Decimal(0.00);
                                    var prAmount = new Decimal(0.00);
                                    if (purchOrderLines != null && purchOrderLines.Any())
                                    {
                                        var groupPurchOrderLines =
                                            purchOrderLines.Where(
                                                m =>
                                                    m.PROPOSALID == key.PROPOSALID && m.COSTCENTER == key.COSTCENTER &&
                                                    m.APCODE == key.APCODE);
                                        if (groupPurchOrderLines.Any())
                                        {
                                            reserveAmount = groupPurchOrderLines.Sum(m => m.ACTUALPRICE);
                                            foreach (var groupPurchOrderLine in groupPurchOrderLines)
                                            {
                                                var purchReqLine =
                                                    purchReqTable.PurchReqLines.LastOrDefault(
                                                        m => m.ITEM_NUM == groupPurchOrderLine.PR_LINE_NUM);
                                                if (purchReqLine == null ||
                                                    purchReqLine.StatusFlag != DocumentStatusFlagId.Active)
                                                    throw new Exception(
                                                        string.Format(
                                                            "Error! Cannot create Purchase order number {0}, PO_ID {1}. PR LINE NUM {2} status is invalid.",
                                                            purchOrderTable.PO_NUM, purchOrderTable.PO_ID,
                                                            groupPurchOrderLine.PR_LINE_NUM));

                                                prAmount += purchReqLine.ACTUALPRICE;
                                            }
                                        }
                                    }

                                    var validateBudgetResult = ValidateBudget(//FROM PR
                                            new RequestViewModel()
                                            {
                                                ProposalNumber = key.PROPOSALID,
                                                Amount = prAmount,
                                                APCode = apCode.APCode,
                                                ExpenseTopic = apCode.ExpenseTopicCode,
                                                UnitCode = key.COSTCENTER,
                                                Status = Budget.BudgetStatus.Commit,
                                                DocumentStatus = Budget.BudgetStatus.Commit,
                                                DocumentType = Budget.DocumentType.PurchaseRequisition,
                                                DocumentId = purchReqTable.REQ_ID,
                                                DocumentNumber = purchReqTable.PR_NUM,
                                                EmployeeId = purchOrderGroup.First().REQUESTFORID
                                            },
                                        //TO PO
                                            new RequestViewModel()
                                            {
                                                ProposalNumber = key.PROPOSALID,
                                                Amount = reserveAmount,
                                                APCode = apCode.APCode,
                                                ExpenseTopic = apCode.ExpenseTopicCode,
                                                UnitCode = key.COSTCENTER,
                                                Status = Budget.BudgetStatus.Commit,
                                                DocumentStatus = Budget.BudgetStatus.Commit,
                                                DocumentType = Budget.DocumentType.PurchaseOrder,
                                                DocumentId = purchOrderTable.PO_ID,
                                                DocumentNumber = purchOrderTable.PO_NUM,
                                                EmployeeId = purchOrderGroup.First().REQUESTFORID
                                            }, Budget.BudgetStatus.Reserve);

                                    if (validateBudgetResult)
                                    {
                                        var lineGroup = purchOrderGroup.GroupBy(m => m.APDESC);
                                        foreach (var purchOrderLineGroup in lineGroup)
                                        {
                                            var amount = new Decimal(0.00);
                                            var prGroupAmount = new Decimal(0.00);
                                            if (purchOrderLines != null && purchOrderLines.Any())
                                            {
                                                var groupPurchOrderLines =
                                                    purchOrderLines.Where(
                                                        m =>
                                                            m.PROPOSALID == key.PROPOSALID && m.COSTCENTER == key.COSTCENTER &&
                                                            m.APCODE == key.APCODE && m.APDESC == purchOrderLineGroup.Key);
                                                if (groupPurchOrderLines.Any())
                                                {
                                                    amount = groupPurchOrderLines.Sum(m => m.ACTUALPRICE);
                                                    foreach (var groupPurchOrderLine in groupPurchOrderLines)
                                                    {
                                                        var purchReqLine =
                                                            purchReqTable.PurchReqLines.LastOrDefault(
                                                                m => m.ITEM_NUM == groupPurchOrderLine.PR_LINE_NUM);
                                                        if (purchReqLine == null ||
                                                            purchReqLine.StatusFlag != DocumentStatusFlagId.Active)
                                                            throw new Exception(
                                                                string.Format(
                                                                    "Error! Cannot create Purchase order number {0}, PO_ID {1}. PR LINE NUM {2} status is invalid.",
                                                                    purchOrderTable.PO_NUM, purchOrderTable.PO_ID,
                                                                    groupPurchOrderLine.PR_LINE_NUM));

                                                        prGroupAmount += purchReqLine.ACTUALPRICE;
                                                    }
                                                }
                                            }
                                            var changeResult = ChangeTrans(
                                                //FROM PR
                                            new RequestViewModel()
                                            {
                                                ProposalNumber = key.PROPOSALID,
                                                Amount = prGroupAmount,
                                                APCode = apCode.APCode,
                                                APDescription = purchOrderLineGroup.Key,
                                                ExpenseTopic = apCode.ExpenseTopicCode,
                                                UnitCode = key.COSTCENTER,
                                                Status = Budget.BudgetStatus.Commit,
                                                DocumentStatus = Budget.BudgetStatus.Commit,
                                                DocumentType = Budget.DocumentType.PurchaseRequisition,
                                                DocumentId = purchReqTable.REQ_ID,
                                                DocumentNumber = purchReqTable.PR_NUM,
                                                EmployeeId = purchOrderGroup.First().REQUESTFORID
                                            },
                                                //TO PO
                                            new RequestViewModel()
                                            {
                                                ProposalNumber = key.PROPOSALID,
                                                Amount = amount,
                                                APCode = apCode.APCode,
                                                APDescription = purchOrderLineGroup.Key,
                                                ExpenseTopic = apCode.ExpenseTopicCode,
                                                UnitCode = key.COSTCENTER,
                                                Status = Budget.BudgetStatus.Commit,
                                                DocumentStatus = Budget.BudgetStatus.Commit,
                                                DocumentType = Budget.DocumentType.PurchaseOrder,
                                                DocumentId = purchOrderTable.PO_ID,
                                                DocumentNumber = purchOrderTable.PO_NUM,
                                                EmployeeId = purchOrderGroup.First().REQUESTFORID
                                            }, Budget.BudgetStatus.Reserve);

                                            if (changeResult == false)
                                            {
                                                result = false;
                                            }
                                        }
                                        
                                    }
                                    else
                                    {
                                        result = false;
                                    }
                                    
                                }
                                else
                                {
                                    result = false;
                                }
                            }
                            else
                            {
                                result = false;
                            }

                            if (!result)
                            {
                                return new ReturnMessage()
                                {
                                    Status = false,
                                    Message = "Error! please check your parameters and try again."

                                };
                            }
                        }
                    }
                    else
                    {
                        result = false;
                    }


                    if (result)
                    {
                        SaveCreatePO(purchOrderTable, purchOrderLines, purchReqTable.PurchReqLines);

                        _session.SaveChanges();

                        return new ReturnMessage()
                        {
                            Status = true
                        };
                    }

                }

                return new ReturnMessage()
                {
                    Status = false,
                    Message = "Error! please check your parameters and try again."

                };
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }

        public ReturnMessage CancelPO(PORequestViewModel viewModel)
        {
            try
            {
                var result = true;
                if (viewModel == null)
                    throw new Exception(string.Format(Resources.Error.ParameterNotFound, ""));
                if (ValidatePOHeaderViewModel(viewModel.Header, false))
                {
                    var reqPurchOrderTable = _purchOrderMapper.ToPurchOrderTable(viewModel.Header);

                    //Find PO
                    var purchOrderTable = _purchOrderTableService.Find(reqPurchOrderTable.PO_ID);
                    if (purchOrderTable == null)
                        throw new Exception(
                            string.Format("Error! Cannot cancel Purchase order number {0}, PO_ID {1}. PO could not be found.", reqPurchOrderTable.PO_NUM, reqPurchOrderTable.PO_ID));
                    if (purchOrderTable.StatusFlag != DocumentStatusFlagId.Active)
                        throw new Exception(
                            string.Format("Error! Cannot cancel Purchase order number {0}, PO_ID {1}. PO status is invalid", purchOrderTable.PO_NUM, purchOrderTable.PO_ID));

                    var purchOrderLines = purchOrderTable.PurchOrderLines;
                    if (purchOrderLines != null && purchOrderLines.Any())
                    {
                        var purchOrderLineGrouping =
                            purchOrderLines.GroupBy(m => new { m.PROPOSALID, m.COSTCENTER, m.APCODE, m.APDESC });
                        foreach (var purchOrderGroup in purchOrderLineGrouping)
                        {
                            var key = purchOrderGroup.Key;
                            if (key != null
                                && !string.IsNullOrEmpty(key.PROPOSALID)
                                && !string.IsNullOrEmpty(key.COSTCENTER)
                                && !string.IsNullOrEmpty(key.APCODE))
                            {
                                var apCode =
                                    _apCodeService.Find(
                                        m => m.APCode.Equals(key.APCODE, StringComparison.InvariantCultureIgnoreCase));
                                if (apCode != null)
                                {
                                    var cancelResult = CancelTrans(new RequestViewModel()
                                    {
                                        ProposalNumber = key.PROPOSALID,
                                        APCode = apCode.APCode,
                                        APDescription = key.APDESC,
                                        ExpenseTopic = apCode.ExpenseTopicCode,
                                        UnitCode = key.COSTCENTER,
                                        Status = Budget.BudgetStatus.Cancel,
                                        DocumentStatus = Budget.BudgetStatus.Cancel,
                                        DocumentType = Budget.DocumentType.PurchaseOrder,
                                        DocumentId = purchOrderTable.PO_ID,
                                        DocumentNumber = purchOrderTable.PO_NUM,
                                        EmployeeId = purchOrderGroup.First().REQUESTFORID
                                    }, Budget.BudgetStatus.Commit);

                                    if (cancelResult == false)
                                    {
                                        result = false;
                                    }
                                }
                                else
                                {
                                    result = false;
                                }

                            }
                            else
                            {
                                result = false;
                            }

                            if (!result)
                            {
                                return new ReturnMessage()
                                {
                                    Status = false,
                                    Message = "Error! please check your parameters and try again."

                                };
                            }
                        }
                    }
                    else
                    {
                        result = false;
                    }

                    if (result)
                    {
                        SaveCancelPO(purchOrderTable, purchOrderLines);
                        _session.SaveChanges();

                        return new ReturnMessage()
                        {
                            Status = true
                        };
                    }

                }

                return new ReturnMessage()
                {
                    Status = false,
                    Message = "Error! please check your parameters and try again."

                };
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }

        public ReturnMessage ChangePO(PORequestViewModel viewModel)
        {
            try
            {
                var result = true;
                if (viewModel == null)
                    throw new Exception(string.Format(Resources.Error.ParameterNotFound, ""));
                if (ValidatePOHeaderViewModel(viewModel.Header, false))
                {
                    var reqPurchOrderTable = _purchOrderMapper.ToPurchOrderTable(viewModel.Header);
                    var closePO = reqPurchOrderTable.CLOSE_PO;
                    if (!closePO.HasValue)
                        throw new Exception(string.Format(Resources.Error.ParameterNotFound, "CLOSEPO"));
                    //Find PO
                    var purchOrderTable = _purchOrderTableService.Find(reqPurchOrderTable.PO_ID);
                    if (purchOrderTable == null)
                        throw new Exception(
                            string.Format("Error! Cannot change Purchase order number {0}, PO_ID {1}. PO could not be found.", reqPurchOrderTable.PO_NUM, reqPurchOrderTable.PO_ID));
                    if (purchOrderTable.StatusFlag != DocumentStatusFlagId.Active)
                        throw new Exception(
                            string.Format("Error! Cannot change Purchase order number {0}, PO_ID {1}. PO status is invalid", purchOrderTable.PO_NUM, purchOrderTable.PO_ID));

                    var purchOrderLines = purchOrderTable.PurchOrderLines;
                    var returnList = new List<RequestViewModel>();
                    if (purchOrderLines != null && purchOrderLines.Any())
                    {
                        //Check PO Item delivery complete
                        foreach (var purchOrderLine in purchOrderLines)
                        {
                            var goodReceipts =
                                _goodsReceiptLineService.GetWhere(
                                    m =>
                                        m.GoodsReceiptTable.PO_ID == purchOrderLine.PO_ID &&
                                        m.ITEM_NUM == purchOrderLine.ITEM_NUM);
                            var grActualPrice = new decimal(0.00);
                            var grQuantity = new decimal(0.00);
                            if (goodReceipts != null && goodReceipts.Any())
                            {
                                grActualPrice = goodReceipts.Sum(m => m.ACTUALPRICE);
                                grQuantity = goodReceipts.Sum(m => m.QUANTITY ?? 0);
                                //Delivery Complete
                                if (grQuantity == purchOrderLine.QUANTITY)
                                {
                                    purchOrderLine.DeliveryComplete = true;
                                }
                                else
                                {
                                    returnList.Add(new RequestViewModel()
                                    {
                                        APCode = purchOrderLine.APCODE,
                                        APDescription = purchOrderLine.APDESC,
                                        ProposalNumber = purchOrderLine.PROPOSALID,
                                        UnitCode = purchOrderLine.COSTCENTER,
                                        Amount = purchOrderLine.ACTUALPRICE - grActualPrice,
                                        EmployeeId = purchOrderLine.REQUESTFORID
                                    });
                                }
                            }
                            else
                            {
                                returnList.Add(new RequestViewModel()
                                {
                                    APCode = purchOrderLine.APCODE,
                                    APDescription = purchOrderLine.APDESC,
                                    ProposalNumber = purchOrderLine.PROPOSALID,
                                    UnitCode = purchOrderLine.COSTCENTER,
                                    Amount = purchOrderLine.ACTUALPRICE - grActualPrice,
                                    EmployeeId = purchOrderLine.REQUESTFORID
                                });
                            }
                            purchOrderLine.StatusFlag = DocumentStatusFlagId.Completed;
                            //Update PurchReqLine
                            _purchOrderLineService.Update(purchOrderLine, purchOrderLine.PO_ID, purchOrderLine.ITEM_NUM);
                            var purchReqLine = _purchReqLineService.LastOrDefault(m =>
                                    m.ITEM_NUM == purchOrderLine.PR_LINE_NUM
                                    && m.REQ_ID == purchOrderTable.REQ_ID);
                            if (purchReqLine != null)
                            {
                                //gr complete or close po
                                if (closePO.Value || (grQuantity == purchOrderLine.QUANTITY))
                                    purchReqLine.StatusFlag = DocumentStatusFlagId.Completed;
                                else //Change pr status for create new po
                                    purchReqLine.StatusFlag = DocumentStatusFlagId.Active;

                                _purchReqLineService.Update(purchReqLine, purchReqLine.REQ_ID, purchReqLine.ITEM_NUM);
                            }
                        }

                        //Return Transaction group by proposalnumber,unitcode,apcode
                        var purchOrderLineGrouping = returnList.GroupBy(m => new { m.ProposalNumber, m.UnitCode, m.APCode, m.APDescription });
                        foreach (var purchOrderGroup in purchOrderLineGrouping)
                        {
                            var key = purchOrderGroup.Key;
                            var returnAmount = purchOrderGroup.Sum(m => m.Amount);
                            if (key != null
                                && !string.IsNullOrEmpty(key.ProposalNumber)
                                && !string.IsNullOrEmpty(key.UnitCode)
                                && !string.IsNullOrEmpty(key.APCode))
                            {
                                var apCode =
                                    _apCodeService.Find(
                                        m => m.APCode.Equals(key.APCode, StringComparison.InvariantCultureIgnoreCase));
                                if (apCode != null)
                                {
                                    if (closePO.Value)
                                    {
                                        var cancelResult = CloseTrans(new RequestViewModel()
                                        {
                                            ProposalNumber = key.ProposalNumber,
                                            APCode = apCode.APCode,
                                            APDescription = key.APDescription,
                                            ExpenseTopic = apCode.ExpenseTopicCode,
                                            UnitCode = key.UnitCode,
                                            Status = Budget.BudgetStatus.Return,
                                            DocumentStatus = Budget.BudgetStatus.Close,
                                            DocumentType = Budget.DocumentType.PurchaseOrder,
                                            DocumentId = purchOrderTable.PO_ID,
                                            DocumentNumber = purchOrderTable.PO_NUM,
                                            EmployeeId = purchOrderGroup.First().EmployeeId,
                                            Amount = returnAmount
                                        }, Budget.BudgetStatus.Commit);

                                        if (cancelResult == false)
                                        {
                                            result = false;
                                        }
                                    }
                                    else
                                    {
                                        var changeResult = ChangeTrans(
                                            //FROM PO
                                            new RequestViewModel()
                                            {
                                                ProposalNumber = key.ProposalNumber,
                                                APCode = apCode.APCode,
                                                APDescription = key.APDescription,
                                                ExpenseTopic = apCode.ExpenseTopicCode,
                                                UnitCode = key.UnitCode,
                                                Status = Budget.BudgetStatus.Return,
                                                DocumentStatus = Budget.BudgetStatus.Close,
                                                DocumentType = Budget.DocumentType.PurchaseOrder,
                                                DocumentId = purchOrderTable.PO_ID,
                                                DocumentNumber = purchOrderTable.PO_NUM,
                                                EmployeeId = purchOrderGroup.First().EmployeeId,
                                                Amount = returnAmount
                                            }
                                            , //TO PR
                                            new RequestViewModel()
                                            {
                                                ProposalNumber = key.ProposalNumber,
                                                APCode = apCode.APCode,
                                                APDescription = key.APDescription,
                                                ExpenseTopic = apCode.ExpenseTopicCode,
                                                UnitCode = key.UnitCode,
                                                Status = Budget.BudgetStatus.Reserve,
                                                DocumentStatus = Budget.BudgetStatus.Change,
                                                DocumentType = Budget.DocumentType.PurchaseRequisition,
                                                DocumentId = purchOrderTable.REQ_ID,
                                                DocumentNumber = purchOrderTable.PR_NUM,
                                                EmployeeId = purchOrderGroup.First().EmployeeId,
                                                Amount = returnAmount
                                            }, Budget.BudgetStatus.Commit);

                                        if (changeResult == false)
                                        {
                                            result = false;
                                        }
                                    }

                                }
                                else
                                {
                                    result = false;
                                }

                            }
                            else
                            {
                                result = false;
                            }

                            if (!result)
                            {
                                return new ReturnMessage()
                                {
                                    Status = false,
                                    Message = "Error! please check your parameters and try again."

                                };
                            }
                        }
                    }

                    if (result)
                    {
                        //UPDATE Close PO Table
                        purchOrderTable.CLOSE_PO = reqPurchOrderTable.CLOSE_PO;
                        purchOrderTable.ClosePO = true;
                        purchOrderTable.Status = "Closed";
                        purchOrderTable.StatusFlag = DocumentStatusFlagId.Completed;
                        purchOrderTable = _purchOrderTableService.Update(purchOrderTable, purchOrderTable.PO_ID);
                        _session.SaveChanges();

                        return new ReturnMessage()
                        {
                            Status = true
                        };
                    }

                }

                return new ReturnMessage()
                {
                    Status = false,
                    Message = "Error! please check your parameters and try again."

                };
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }
        #endregion

        #region GR
        public ReturnMessage GoodsReceipt(GRRequestViewModel viewModel)
        {
            try
            {
                var result = true;
                if (viewModel == null)
                    throw new Exception(string.Format(Resources.Error.ParameterNotFound, ""));
                if (ValidateGRHeaderViewModel(viewModel.Header))
                {
                    var goodsReceiptTable = _goodsReceiptMapper.ToGoodsReceiptTable(viewModel.Header);
                    //Find GR
                    var receiptId = goodsReceiptTable.RECEIPT_ID;
                    var checkExistGR = _goodsReceiptTableService.Find(receiptId);
                    if (checkExistGR != null)
                        throw new Exception(
                            string.Format("Error! Cannot create duplicate Goods Receipt {0} ", goodsReceiptTable.RECEIPT_ID));
                    //Find PO
                    var purchOrderTable = _purchOrderTableService.Find(goodsReceiptTable.PO_ID);
                    if (purchOrderTable == null)
                        throw new Exception(
                            string.Format("Error! Purchase order number {0}, PO_ID {1}. PO could not be found.", goodsReceiptTable.PO_NUM, goodsReceiptTable.PO_ID));
                    if (purchOrderTable.StatusFlag != DocumentStatusFlagId.Active)
                        throw new Exception(
                            string.Format("Error! Cannot Goods Receipt {0}. PO status is invalid", goodsReceiptTable.RECEIPT_ID));

                    var goodsReceiptLines = _goodsReceiptMapper.ToGoodsReceiptLines(viewModel.Items);
                    var movementType = string.Empty;

                    if (goodsReceiptLines != null && goodsReceiptLines.Any())
                    {
                        movementType = goodsReceiptLines.First().MOVEMENTTYPE;
                        if (movementType == MProcurementConstant.GRMovementType.CANCEL)
                        {
                            var goodsReceiptLine = goodsReceiptLines.First();
                            var cancelGoodsReceipt =
                                _goodsReceiptTableService.Find(m => m.RECEIPT_ID == goodsReceiptLine.REFERNUM);
                            if (cancelGoodsReceipt == null)
                                throw new Exception(string.Format("Error! Cannot cancel Goods Receipt. REFERNUM {0} not found", goodsReceiptLine.REFERNUM));

                            foreach (var cancelGoodsReceiptLine in cancelGoodsReceipt.GoodsReceiptLines)
                            {
                                var purchOrderLine = purchOrderTable.PurchOrderLines.LastOrDefault(
                                    m => m.ITEM_NUM == cancelGoodsReceiptLine.ITEM_NUM
                                    && m.PO_ID == purchOrderTable.PO_ID);
                                result = CancelGR(goodsReceiptTable, goodsReceiptLine, purchOrderLine, cancelGoodsReceiptLine.ACTUALPRICE);
                            }

                        }
                        else
                        {
                            foreach (var goodsReceiptLine in goodsReceiptLines)
                            {
                                var purchOrderLine =
                                    purchOrderTable.PurchOrderLines.LastOrDefault(
                                        m => m.ITEM_NUM == goodsReceiptLine.ITEM_NUM
                                            && m.PO_ID == purchOrderTable.PO_ID);
                                if (goodsReceiptLine.MOVEMENTTYPE == MProcurementConstant.GRMovementType.CREATE)
                                {
                                    result = CreateGR(goodsReceiptTable, goodsReceiptLine, purchOrderLine);
                                }
                                else if (goodsReceiptLine.MOVEMENTTYPE == MProcurementConstant.GRMovementType.RETURN)
                                {
                                    result = ReturnGR(goodsReceiptTable, goodsReceiptLine, purchOrderLine);
                                }
                                else
                                {
                                    result = false;
                                    return new ReturnMessage()
                                    {
                                        Status = false,
                                        Message = "Error! please check your parameters GR_ITEMS MOVEMENTTYPE and try again."

                                    };
                                }

                                if (result == false)
                                    return new ReturnMessage()
                                    {
                                        Status = false,
                                        Message = "Error! please check your parameters and try again."

                                    };
                            }
                        }

                    }
                    else
                    {
                        result = false;
                        return new ReturnMessage()
                        {
                            Status = false,
                            Message = "Error! please check your parameters GR_ITEMS and try again."

                        };
                    }


                    if (result)
                    {
                        SaveCreateGR(goodsReceiptTable, goodsReceiptLines, movementType);

                        _session.SaveChanges();

                        return new ReturnMessage()
                        {
                            Status = true
                        };
                    }
                }

                return new ReturnMessage()
                {
                    Status = false,
                    Message = "Error! please check your parameters and try again."

                };
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }

        private bool CreateGR(MPS_GoodsReceiptTable goodsReceiptTable, MPS_GoodsReceiptLine goodsReceiptLine, MPS_PurchOrderLine purchOrderLine)
        {
            var result = false;
            if (purchOrderLine == null)
                throw new Exception(string.Format("Error! Cannot create Goods receipt {0}. PO {1}, ITEM NUM {2} not found.", goodsReceiptTable.RECEIPT_ID, goodsReceiptTable.PO_NUM, goodsReceiptLine.ITEM_NUM));
            if (purchOrderLine.StatusFlag != DocumentStatusFlagId.Active)
                throw new Exception(string.Format("Error! Cannot create Goods receipt {0}. PO {1}, ITEM NUM {2} status is invalid.", goodsReceiptTable.RECEIPT_ID, goodsReceiptTable.PO_NUM, goodsReceiptLine.ITEM_NUM));
            var apCode = _apCodeService.Find(m => m.APCode.Equals(purchOrderLine.APCODE, StringComparison.InvariantCultureIgnoreCase));
            if (apCode != null)
            {
                result = ChangeTrans(
                    //FROM PO
                    new RequestViewModel()
                    {
                        ProposalNumber = purchOrderLine.PROPOSALID,
                        Amount = goodsReceiptLine.ACTUALPRICE,
                        APCode = apCode.APCode,
                        APDescription = purchOrderLine.APDESC,
                        ExpenseTopic = apCode.ExpenseTopicCode,
                        UnitCode = purchOrderLine.COSTCENTER,
                        Status = Budget.BudgetStatus.Actual,
                        DocumentStatus = Budget.BudgetStatus.Actual,
                        DocumentType = Budget.DocumentType.PurchaseOrder,
                        DocumentId = purchOrderLine.PO_ID,
                        DocumentNumber = purchOrderLine.PurchOrderTable.PO_NUM,
                        EmployeeId = purchOrderLine.REQUESTFORID
                    },
                    //TO GR
                    new RequestViewModel()
                    {
                        ProposalNumber = purchOrderLine.PROPOSALID,
                        Amount = goodsReceiptLine.ACTUALPRICE,
                        APCode = apCode.APCode,
                        APDescription = purchOrderLine.APDESC,
                        ExpenseTopic = apCode.ExpenseTopicCode,
                        UnitCode = purchOrderLine.COSTCENTER,
                        Status = Budget.BudgetStatus.Actual,
                        DocumentStatus = Budget.BudgetStatus.Actual,
                        DocumentType = Budget.DocumentType.GoodsReceipt,
                        DocumentId = goodsReceiptTable.RECEIPT_ID,
                        DocumentNumber = goodsReceiptTable.RECEIPT_ID.ToString(),
                        EmployeeId = purchOrderLine.REQUESTFORID
                    }, Budget.BudgetStatus.Commit);
            }
            return result;

        }
        private bool ReturnGR(MPS_GoodsReceiptTable goodsReceiptTable, MPS_GoodsReceiptLine goodsReceiptLine, MPS_PurchOrderLine purchOrderLine)
        {
            var result = false;
            if (purchOrderLine == null)
                throw new Exception(string.Format("Error! Cannot return Goods receipt {0}. PO {1}, ITEM NUM {2} not found.", goodsReceiptTable.RECEIPT_ID, goodsReceiptTable.PO_NUM, goodsReceiptLine.ITEM_NUM));
            if (purchOrderLine.StatusFlag != DocumentStatusFlagId.Active)
                throw new Exception(string.Format("Error! Cannot return Goods receipt {0}. PO {1}, ITEM NUM {2} status is invalid.", goodsReceiptTable.RECEIPT_ID, goodsReceiptTable.PO_NUM, goodsReceiptLine.ITEM_NUM));
            var apCode = _apCodeService.Find(m => m.APCode.Equals(purchOrderLine.APCODE, StringComparison.InvariantCultureIgnoreCase));
            if (apCode != null)
            {
                result = ChangeTrans(
                    //FROM GR
                    new RequestViewModel()
                    {
                        ProposalNumber = purchOrderLine.PROPOSALID,
                        Amount = goodsReceiptLine.ACTUALPRICE * (-1),
                        APCode = apCode.APCode,
                        APDescription = purchOrderLine.APDESC,
                        ExpenseTopic = apCode.ExpenseTopicCode,
                        UnitCode = purchOrderLine.COSTCENTER,
                        Status = Budget.BudgetStatus.Return,
                        DocumentStatus = Budget.BudgetStatus.Return,
                        DocumentType = Budget.DocumentType.GoodsReceipt,
                        DocumentId = goodsReceiptTable.RECEIPT_ID,
                        DocumentNumber = goodsReceiptTable.RECEIPT_ID.ToString(),
                        EmployeeId = purchOrderLine.REQUESTFORID
                    },
                    //TO PO
                    new RequestViewModel()
                    {
                        ProposalNumber = purchOrderLine.PROPOSALID,
                        Amount = goodsReceiptLine.ACTUALPRICE * (-1),
                        APCode = apCode.APCode,
                        APDescription = purchOrderLine.APDESC,
                        ExpenseTopic = apCode.ExpenseTopicCode,
                        UnitCode = purchOrderLine.COSTCENTER,
                        Status = Budget.BudgetStatus.Commit,
                        DocumentStatus = Budget.BudgetStatus.Commit,
                        DocumentType = Budget.DocumentType.PurchaseOrder,
                        DocumentId = purchOrderLine.PO_ID,
                        DocumentNumber = purchOrderLine.PurchOrderTable.PO_NUM,
                        EmployeeId = purchOrderLine.REQUESTFORID
                    }
                    , Budget.BudgetStatus.Actual, false);
            }
            return result;

        }

        private bool CancelGR(MPS_GoodsReceiptTable goodsReceiptTable, MPS_GoodsReceiptLine goodsReceiptLine, MPS_PurchOrderLine purchOrderLine, decimal cancelAmount)
        {
            var result = false;
            if (purchOrderLine == null)
                throw new Exception(string.Format("Error! Cannot cancel Goods receipt {0}. PO {1}, ITEM NUM {2} not found.", goodsReceiptTable.RECEIPT_ID, goodsReceiptTable.PO_NUM, goodsReceiptLine.ITEM_NUM));
            if (purchOrderLine.StatusFlag != DocumentStatusFlagId.Active)
                throw new Exception(string.Format("Error! Cannot cancel Goods receipt {0}. PO {1}, ITEM NUM {2} status is invalid.", goodsReceiptTable.RECEIPT_ID, goodsReceiptTable.PO_NUM, goodsReceiptLine.ITEM_NUM));
            var apCode = _apCodeService.Find(m => m.APCode.Equals(purchOrderLine.APCODE, StringComparison.InvariantCultureIgnoreCase));
            if (apCode != null)
            {
                result = ChangeTrans(
                    //FROM GR
                    new RequestViewModel()
                    {
                        ProposalNumber = purchOrderLine.PROPOSALID,
                        Amount = cancelAmount,
                        APCode = apCode.APCode,
                        APDescription = purchOrderLine.APDESC,
                        ExpenseTopic = apCode.ExpenseTopicCode,
                        UnitCode = purchOrderLine.COSTCENTER,
                        Status = Budget.BudgetStatus.Cancel,
                        DocumentStatus = Budget.BudgetStatus.Cancel,
                        DocumentType = Budget.DocumentType.GoodsReceipt,
                        DocumentId = goodsReceiptTable.RECEIPT_ID,
                        DocumentNumber = goodsReceiptTable.RECEIPT_ID.ToString(),
                        EmployeeId = purchOrderLine.REQUESTFORID
                    },
                    //TO PO
                    new RequestViewModel()
                    {
                        ProposalNumber = purchOrderLine.PROPOSALID,
                        Amount = cancelAmount,
                        APCode = apCode.APCode,
                        APDescription = purchOrderLine.APDESC,
                        ExpenseTopic = apCode.ExpenseTopicCode,
                        UnitCode = purchOrderLine.COSTCENTER,
                        Status = Budget.BudgetStatus.Commit,
                        DocumentStatus = Budget.BudgetStatus.Commit,
                        DocumentType = Budget.DocumentType.PurchaseOrder,
                        DocumentId = purchOrderLine.PO_ID,
                        DocumentNumber = purchOrderLine.PurchOrderTable.PO_NUM,
                        EmployeeId = purchOrderLine.REQUESTFORID
                    }
                    , Budget.BudgetStatus.Actual, false);
            }
            return result;

        }
        #endregion

        #endregion

        #region Manage Transaction

        public bool ValidateBudget(RequestViewModel viewModel,bool revise = false, string status = "")
        {
            var result = false;
            var lastReserveAmount = new decimal(0);
           
            var proposalTable = _proposalService.LastOrDefault(m => m.DocumentNumber == viewModel.ProposalNumber);
            if (proposalTable == null)
                throw new Exception(string.Format(Resources.Error.ProposalNotFound, viewModel.ProposalNumber));

            if (revise)
            {
                var budgetTrans = _budgetProposalTransService.GetWhere(m => m.ProposalId == proposalTable.Id
                                                                                 && m.APCode == viewModel.APCode
                                                                                 && m.UnitCode == viewModel.UnitCode
                                                                                    &&
                                                                                    m.DocumentType ==
                                                                                    viewModel.DocumentType
                                                                                    &&
                                                                                    m.DocumentNo ==
                                                                                    viewModel.DocumentNumber
                                                                                    && m.Status == status
                                                                                    && !m.InActive);
                if (budgetTrans == null || !budgetTrans.Any())
                    throw new Exception(string.Format(Resources.Error.BudgetTransNotFound, viewModel.DocumentNumber));

                var budgetTransGroupLine = budgetTrans.GroupBy(m => m.ProposalLineId);
                foreach (var budgetTransLine in budgetTransGroupLine)
                {
                    var budgetTransLast = budgetTransLine.LastOrDefault();

                    if (budgetTransLast != null) 
                        lastReserveAmount += budgetTransLast.Amount;
                }
            }

            var budgetPlan = _proposalBudgetPlanService.GetProposalBudgetPlan(proposalTable.Id, viewModel.APCode, viewModel.UnitCode);
            if (budgetPlan == null)
                throw new Exception(string.Format(Resources.Error.ProposalBudgetPlanNotFound, viewModel.ProposalNumber, viewModel.APCode, viewModel.UnitCode));
            var budgetTransTotal = _budgetProposalTransService.GetBudgetTransTotal(proposalTable.Id, viewModel.APCode, viewModel.UnitCode);
            var budgetBalance = (budgetPlan.BudgetPlan - budgetTransTotal + lastReserveAmount);
            result = budgetBalance >= viewModel.Amount;

            if (result == false)
                throw new Exception(string.Format(Resources.Error.ReturnInterfaceErrorBudgetExceed, proposalTable.DocumentNumber, proposalTable.Title));

            return result;
        }

        public bool ValidateBudget(RequestViewModel from, RequestViewModel to, string status, bool validateTrans = true)
        {
            var result = false;
            var lastReserveAmount = new decimal(0);

            var proposalTable = _proposalService.LastOrDefault(m => m.DocumentNumber == from.ProposalNumber);
            if (proposalTable == null)
                throw new Exception(string.Format(Resources.Error.ProposalNotFound, from.ProposalNumber));
            if (validateTrans)
            {
                var budgetTrans = _budgetProposalTransService.GetWhere(m => m.ProposalId == proposalTable.Id
                                                                                 && m.APCode == from.APCode
                                                                                 && m.UnitCode == from.UnitCode
                                                                                    &&
                                                                                    m.DocumentType ==
                                                                                    from.DocumentType
                                                                                    &&
                                                                                    m.DocumentNo ==
                                                                                    from.DocumentNumber
                                                                                    && m.Status == status
                                                                                    && !m.InActive);
                if (budgetTrans == null || !budgetTrans.Any())
                    throw new Exception(string.Format(Resources.Error.BudgetTransNotFound, from.DocumentNumber));

                var budgetTransGroupLine = budgetTrans.GroupBy(m => m.ProposalLineId);
                foreach (var budgetTransLine in budgetTransGroupLine)
                {
                    var budgetTransLast = budgetTransLine.LastOrDefault();

                    if (budgetTransLast != null)
                        lastReserveAmount += budgetTransLast.Amount;
                }
            }
            var budgetPlan = _proposalBudgetPlanService.GetProposalBudgetPlan(proposalTable.Id, from.APCode, from.UnitCode);
            if (budgetPlan == null)
                throw new Exception(string.Format(Resources.Error.ProposalBudgetPlanNotFound, from.ProposalNumber, from.APCode, from.UnitCode));
            var budgetTransTotal = _budgetProposalTransService.GetBudgetTransTotal(proposalTable.Id, from.APCode, from.UnitCode);
            var budgetBalance = (budgetPlan.BudgetPlan - budgetTransTotal + from.Amount);

            result = budgetBalance >= to.Amount;

            if (result == false)
                throw new Exception(string.Format(Resources.Error.ReturnInterfaceErrorBudgetExceed, proposalTable.DocumentNumber, proposalTable.Title));
            return result;
        }

        public bool CreateTrans(RequestViewModel viewModel, MPS_ProposalLine propoLine = null)
        {
            var result = false;
            //Validate parameters
            if (ValidateRequestViewModel(viewModel))
            {

                var proposalLine = propoLine ?? _proposalLineService.FindProposalLine(viewModel);
                if (proposalLine == null || proposalLine.ProposalTable == null)
                    throw new Exception(string.Format(Resources.Error.ProposalNotFound, viewModel.ProposalNumber));

                //Validate Proposal Status
                if (ValidateProposalLine(proposalLine))
                {
                    var budgetPlan = _proposalBudgetPlanService.GetProposalBudgetPlan(proposalLine.ParentId, proposalLine.APCode, proposalLine.UnitCode);
                    if (budgetPlan == null)
                        throw new Exception(string.Format(Resources.Error.ProposalBudgetPlanNotFound, viewModel.ProposalNumber, viewModel.APCode, viewModel.UnitCode));
                    var budgetTransTotal = _budgetProposalTransService.GetBudgetTransTotal(proposalLine.ParentId, proposalLine.APCode, proposalLine.UnitCode);
                    var budgetBalance = (budgetPlan.BudgetPlan - budgetTransTotal);
                    result = budgetBalance >= viewModel.Amount;

                    if (result == false)
                        throw new Exception(string.Format(Resources.Error.ReturnInterfaceErrorBudgetExceed, proposalLine.ProposalTable.DocumentNumber, proposalLine.ProposalTable.Title));

                    var budgetTransProposal = new MPS_BudgetProposalTrans()
                    {
                        ProposalId = proposalLine.ParentId,
                        ProposalLineId = proposalLine.Id,
                        APCode = proposalLine.APCode,
                        UnitCode = proposalLine.UnitCode,
                        Status = viewModel.Status,
                        DocumentStatus = viewModel.DocumentStatus,
                        DocumentType = viewModel.DocumentType,
                        DocumentNo = viewModel.DocumentNumber,
                        DocumentId = viewModel.DocumentId,
                        Amount = viewModel.Amount,
                        TransDate = DateTime.Now
                    };

                    _budgetProposalTransService.Insert(budgetTransProposal);

                    result = true;
                }

            }
            return result;
        }

        public bool CancelTrans(RequestViewModel viewModel, string status, MPS_ProposalLine propoLine = null)
        {
            var result = false;
            //Validate parameters
            if (ValidateRequestViewModel(viewModel))
            {
                var proposalLine = propoLine ?? _proposalLineService.FindProposalLine(viewModel);
                if (proposalLine == null || proposalLine.ProposalTable == null)
                    throw new Exception(string.Format(Resources.Error.ProposalNotFound, viewModel.ProposalNumber));
                //Validate Proposal Status
                if (ValidateProposalLine(proposalLine))
                {
                    var budgetTrans = _budgetProposalTransService.LastOrDefault(m => m.ProposalLineId == proposalLine.Id
                                                                                     &&
                                                                                     m.DocumentType ==
                                                                                     viewModel.DocumentType
                                                                                     &&
                                                                                     m.DocumentNo ==
                                                                                     viewModel.DocumentNumber
                                                                                     && m.Status == status
                                                                                     && !m.InActive);
                    if (budgetTrans == null)
                        throw new Exception(string.Format(Resources.Error.BudgetTransNotFound, viewModel.DocumentNumber));

                    var budgetTransProposal = new MPS_BudgetProposalTrans()
                    {
                        ProposalId = proposalLine.ParentId,
                        ProposalLineId = proposalLine.Id,
                        APCode = proposalLine.APCode,
                        UnitCode = proposalLine.UnitCode,
                        Status = viewModel.Status,
                        DocumentStatus = viewModel.DocumentStatus,
                        DocumentType = viewModel.DocumentType,
                        DocumentNo = viewModel.DocumentNumber,
                        DocumentId = viewModel.DocumentId,
                        Amount = budgetTrans.Amount * (-1),
                        TransDate = DateTime.Now
                    };

                    _budgetProposalTransService.Insert(budgetTransProposal);

                    result = true;
                }

            }
            return result;
        }

        public bool ChangeTrans(RequestViewModel viewModel, string status, MPS_ProposalLine propoLine = null)
        {
            var result = false;
            //Validate parameters
            if (ValidateRequestViewModel(viewModel))
            {
                var proposalLine = propoLine ?? _proposalLineService.FindProposalLine(viewModel);
                if (proposalLine == null || proposalLine.ProposalTable == null)
                    throw new Exception(string.Format(Resources.Error.ProposalNotFound, viewModel.ProposalNumber));
                //Validate Proposal Status
                if (ValidateProposalLine(proposalLine))
                {
                    var budgetTrans = _budgetProposalTransService.LastOrDefault(m => m.ProposalLineId == proposalLine.Id
                                                                                     &&
                                                                                     m.DocumentType ==
                                                                                     viewModel.DocumentType
                                                                                     &&
                                                                                     m.DocumentNo ==
                                                                                     viewModel.DocumentNumber
                                                                                     && m.Status == status
                                                                                     && !m.InActive);
                    if (budgetTrans == null)
                        throw new Exception(string.Format(Resources.Error.BudgetTransNotFound, viewModel.DocumentNumber));

                    var budgetPlan = _proposalBudgetPlanService.GetProposalBudgetPlan(proposalLine.ParentId, proposalLine.APCode, proposalLine.UnitCode);
                    if (budgetPlan == null)
                        throw new Exception(string.Format(Resources.Error.ProposalBudgetPlanNotFound, viewModel.ProposalNumber, viewModel.APCode, viewModel.UnitCode));
                    var budgetTransTotal = _budgetProposalTransService.GetBudgetTransTotal(proposalLine.ParentId, proposalLine.APCode, proposalLine.UnitCode);
                    var budgetBalance = (budgetPlan.BudgetPlan - budgetTransTotal + budgetTrans.Amount);

                    result = budgetBalance >= viewModel.Amount;

                    if (result == false)
                        throw new Exception(string.Format(Resources.Error.ReturnInterfaceErrorBudgetExceed, proposalLine.ProposalTable.DocumentNumber, proposalLine.ProposalTable.Title));

                    var cancelBudgetTransProposal = new MPS_BudgetProposalTrans()
                    {
                        ProposalId = proposalLine.ParentId,
                        ProposalLineId = proposalLine.Id,
                        APCode = proposalLine.APCode,
                        UnitCode = proposalLine.UnitCode,
                        Status = "Cancel",
                        DocumentStatus = "Revise",
                        DocumentType = viewModel.DocumentType,
                        DocumentNo = viewModel.DocumentNumber,
                        DocumentId = viewModel.DocumentId,
                        Amount = budgetTrans.Amount * (-1),
                        TransDate = DateTime.Now
                    };

                    cancelBudgetTransProposal = _budgetProposalTransService.Insert(cancelBudgetTransProposal);
                    if (viewModel.Amount > 0)
                    {
                        var createBudgetTransProposal = new MPS_BudgetProposalTrans()
                        {
                            ProposalId = proposalLine.ParentId,
                            ProposalLineId = proposalLine.Id,
                            APCode = proposalLine.APCode,
                            UnitCode = proposalLine.UnitCode,
                            Status = "Reserve",
                            DocumentStatus = "Revise",
                            DocumentType = viewModel.DocumentType,
                            DocumentNo = viewModel.DocumentNumber,
                            DocumentId = viewModel.DocumentId,
                            Amount = viewModel.Amount,
                            TransDate = DateTime.Now
                        };

                        createBudgetTransProposal = _budgetProposalTransService.Insert(createBudgetTransProposal);
                    }


                    result = true;
                }

            }
            return result;
        }

        public bool ChangeTrans(RequestViewModel from, RequestViewModel to, string checkStatus, bool validateTrans = true, MPS_ProposalLine propoLine = null)
        {
            var result = false;
            //Validate parameters
            if (ValidateRequestViewModel(from) && ValidateRequestViewModel(to))
            {
                var proposalLine = propoLine ?? _proposalLineService.FindProposalLine(from);
                if (proposalLine == null || proposalLine.ProposalTable == null)
                    throw new Exception(string.Format(Resources.Error.ProposalNotFound, from.ProposalNumber));
                //Validate Proposal Status
                if (ValidateProposalLine(proposalLine))
                {
                    if (validateTrans)
                    {
                        var budgetTrans = _budgetProposalTransService.LastOrDefault(m => m.ProposalLineId == proposalLine.Id
                                                                                    && m.DocumentType == from.DocumentType
                                                                                    && m.DocumentNo == from.DocumentNumber
                                                                                    && m.DocumentId == from.DocumentId
                                                                                    && m.Status == checkStatus
                                                                                    && !m.InActive);
                        if (budgetTrans == null)
                            throw new Exception(string.Format(Resources.Error.BudgetTransNotFound, from.DocumentNumber));
                    }
                    var budgetPlan = _proposalBudgetPlanService.GetProposalBudgetPlan(proposalLine.ParentId, proposalLine.APCode, proposalLine.UnitCode);
                    if (budgetPlan == null)
                        throw new Exception(string.Format(Resources.Error.ProposalBudgetPlanNotFound, from.ProposalNumber, from.APCode, from.UnitCode));
                    var budgetTransTotal = _budgetProposalTransService.GetBudgetTransTotal(proposalLine.ParentId, proposalLine.APCode, proposalLine.UnitCode);
                    var budgetBalance = (budgetPlan.BudgetPlan - budgetTransTotal + from.Amount);

                    result = budgetBalance >= to.Amount;

                    if (result == false)
                        throw new Exception(string.Format(Resources.Error.ReturnInterfaceErrorBudgetExceed, proposalLine.ProposalTable.DocumentNumber, proposalLine.ProposalTable.Title));

                    var cancelBudgetTransProposal = new MPS_BudgetProposalTrans()
                    {
                        ProposalId = proposalLine.ParentId,
                        ProposalLineId = proposalLine.Id,
                        APCode = proposalLine.APCode,
                        UnitCode = proposalLine.UnitCode,
                        Status = from.Status,
                        DocumentStatus = from.DocumentStatus,
                        DocumentType = from.DocumentType,
                        DocumentNo = from.DocumentNumber,
                        DocumentId = from.DocumentId,
                        RefDocumentType = to.DocumentType,
                        RefDocumentNumber = to.DocumentNumber,
                        RefDocumentId = to.DocumentId,
                        Amount = from.Amount * (-1),
                        TransDate = DateTime.Now
                    };

                    cancelBudgetTransProposal = _budgetProposalTransService.Insert(cancelBudgetTransProposal);
                    //if (to.Amount > 0)
                    //{
                    var createBudgetTransProposal = new MPS_BudgetProposalTrans()
                    {
                        ProposalId = proposalLine.ParentId,
                        ProposalLineId = proposalLine.Id,
                        APCode = proposalLine.APCode,
                        UnitCode = proposalLine.UnitCode,
                        Status = to.Status,
                        DocumentStatus = to.DocumentStatus,
                        DocumentType = to.DocumentType,
                        DocumentNo = to.DocumentNumber,
                        DocumentId = to.DocumentId,
                        RefDocumentType = from.DocumentType,
                        RefDocumentNumber = from.DocumentNumber,
                        RefDocumentId = from.DocumentId,
                        Amount = to.Amount,
                        TransDate = DateTime.Now
                    };

                    createBudgetTransProposal = _budgetProposalTransService.Insert(createBudgetTransProposal);
                    //}


                    result = true;
                }

            }
            return result;
        }

        public bool CloseTrans(RequestViewModel viewModel, string status, MPS_ProposalLine propoLine = null)
        {
            var result = false;
            //Validate parameters
            if (ValidateRequestViewModel(viewModel))
            {
                var proposalLine = propoLine ?? _proposalLineService.FindProposalLine(viewModel);
                if (proposalLine == null || proposalLine.ProposalTable == null)
                    throw new Exception(string.Format(Resources.Error.ProposalNotFound, viewModel.ProposalNumber));
                //Validate Proposal Status
                if (ValidateProposalLine(proposalLine))
                {
                    var budgetTrans = _budgetProposalTransService.LastOrDefault(m => m.ProposalLineId == proposalLine.Id
                                                                                     &&
                                                                                     m.DocumentType ==
                                                                                     viewModel.DocumentType
                                                                                     &&
                                                                                     m.DocumentNo ==
                                                                                     viewModel.DocumentNumber
                                                                                     && m.Status == status
                                                                                     && !m.InActive);
                    if (budgetTrans == null)
                        throw new Exception(string.Format(Resources.Error.BudgetTransNotFound, viewModel.DocumentNumber));

                    var budgetTransProposal = new MPS_BudgetProposalTrans()
                    {
                        ProposalId = proposalLine.ParentId,
                        ProposalLineId = proposalLine.Id,
                        APCode = proposalLine.APCode,
                        UnitCode = proposalLine.UnitCode,
                        Status = viewModel.Status,
                        DocumentStatus = viewModel.DocumentStatus,
                        DocumentType = viewModel.DocumentType,
                        DocumentNo = viewModel.DocumentNumber,
                        DocumentId = viewModel.DocumentId,
                        Amount = viewModel.Amount * (-1),
                        TransDate = DateTime.Now
                    };

                    _budgetProposalTransService.Insert(budgetTransProposal);

                    result = true;
                }

            }
            return result;
        }
        #endregion

        #region Validation

        public bool ValidateRequestViewModel(RequestViewModel viewModel)
        {
            if (viewModel == null)
                throw new Exception(string.Format(Resources.Error.ParameterNotFound, ""));
            if (string.IsNullOrEmpty(viewModel.ProposalNumber))
                throw new Exception(string.Format(Resources.Error.ParameterNotFound, "ProposalNumber"));
            if (string.IsNullOrEmpty(viewModel.APCode))
                throw new Exception(string.Format(Resources.Error.ParameterNotFound, "APCode"));
            if (string.IsNullOrEmpty(viewModel.UnitCode))
                throw new Exception(string.Format(Resources.Error.ParameterNotFound, "UnitCode"));
            if (string.IsNullOrEmpty(viewModel.ExpenseTopic))
                throw new Exception(string.Format(Resources.Error.ParameterNotFound, "ExpenseTopic"));
            if (string.IsNullOrEmpty(viewModel.DocumentNumber))
                throw new Exception(string.Format(Resources.Error.ParameterNotFound, "DocumentNumber"));
            if (string.IsNullOrEmpty(viewModel.EmployeeId))
                throw new Exception(string.Format(Resources.Error.ParameterNotFound, "EmployeeId"));

            return true;
        }

        public bool ValidateProposalLine(MPS_ProposalLine proposalLine)
        {
            //TODO Validate Proposal line Status
            if (proposalLine == null || proposalLine.ProposalTable == null)
                throw new Exception(string.Format(Resources.Error.ProposalNotFound, ""));
            if (proposalLine.ProposalTable.ProposalTypeCode == "DEPOSIT"
                || proposalLine.ProposalTable.ProposalTypeCode == "DEPOSITIN")
                throw new Exception(Resources.Error.ProposalTypeDepositInvalid);
            if (proposalLine.ProposalTable.StatusFlag != DocumentStatusFlagId.Completed)
                throw new Exception(string.Format(Resources.Error.ProposalStatusInvalid, "Incomplete"));
            if (proposalLine.ProposalTable.CloseFlag)
                throw new Exception(string.Format(Resources.Error.ProposalStatusInvalid, "Close"));
            //validate username
            return true;
        }

        public bool ValidatePRHeaderViewModel(PRHeaderViewModel viewModel)
        {
            if (viewModel == null)
                throw new Exception(string.Format(Resources.Error.ParameterNotFound, ""));
            if (string.IsNullOrEmpty(viewModel.REQ_ID))
                throw new Exception(string.Format(Resources.Error.ParameterNotFound, "REQ_ID"));
            if (string.IsNullOrEmpty(viewModel.SUBCOMMAND))
                throw new Exception(string.Format(Resources.Error.ParameterNotFound, "SUBCOMMAND"));
            if (string.IsNullOrEmpty(viewModel.PR_NUM))
                throw new Exception(string.Format(Resources.Error.ParameterNotFound, "PR_NUM"));

            return true;
        }

        public bool ValidatePOHeaderViewModel(POHeaderViewModel viewModel, bool validatePR = true)
        {
            if (viewModel == null)
                throw new Exception(string.Format(Resources.Error.ParameterNotFound, ""));
            if (string.IsNullOrEmpty(viewModel.PO_ID))
                throw new Exception(string.Format(Resources.Error.ParameterNotFound, "PO_ID"));
            if (string.IsNullOrEmpty(viewModel.PO_NUM))
                throw new Exception(string.Format(Resources.Error.ParameterNotFound, "PO_NUM"));
            if (validatePR)
            {
                if (string.IsNullOrEmpty(viewModel.REQ_ID))
                    throw new Exception(string.Format(Resources.Error.ParameterNotFound, "REQ_ID"));
                if (string.IsNullOrEmpty(viewModel.PR_NUM))
                    throw new Exception(string.Format(Resources.Error.ParameterNotFound, "PR_NUM"));
            }


            return true;
        }

        public bool ValidatePOLines(IEnumerable<MPS_PurchOrderLine> purchOrderLines, IEnumerable<MPS_PurchReqLine> purchReqLines)
        {
            if (purchOrderLines == null || !purchOrderLines.Any())
                return false;
            foreach (var purchOrderLine in purchOrderLines)
            {
                var purchReqLine = purchReqLines.LastOrDefault(m => m.ITEM_NUM == purchOrderLine.PR_LINE_NUM);
                if (purchReqLine == null || purchReqLine.StatusFlag != DocumentStatusFlagId.Active)
                    return false;
            }

            return true;
        }
        public bool ValidateGRHeaderViewModel(GRHeaderViewModel viewModel)
        {
            if (viewModel == null)
                throw new Exception(string.Format(Resources.Error.ParameterNotFound, ""));
            if (string.IsNullOrEmpty(viewModel.RECEIPT_ID))
                throw new Exception(string.Format(Resources.Error.ParameterNotFound, "RECEIPT_ID"));
            if (string.IsNullOrEmpty(viewModel.PO_ID))
                throw new Exception(string.Format(Resources.Error.ParameterNotFound, "PO_ID"));
            if (string.IsNullOrEmpty(viewModel.PO_NUM))
                throw new Exception(string.Format(Resources.Error.ParameterNotFound, "PO_NUM"));

            return true;
        }

        public ReturnMessage HandleException(Exception ex)
        {
            return new ReturnMessage()
            {
                Status = false,
                Message = ex.Message
            };
        }

        #endregion

        #region CRUD Procurement Service
        public MPS_PurchReqTable SaveCreatePR(MPS_PurchReqTable purchReqTable, IEnumerable<MPS_PurchReqLine> purchReqLines)
        {
            if (purchReqTable != null)
            {
                purchReqTable.Status = "Reserve";
                purchReqTable.StatusFlag = 1;

                purchReqTable = _purchReqTableService.Insert(purchReqTable);

                foreach (var purchReqLine in purchReqLines)
                {
                    purchReqLine.REQ_ID = purchReqTable.REQ_ID;
                    purchReqLine.StatusFlag = 1;

                    _purchReqLineService.Insert(purchReqLine);
                }
            }

            return purchReqTable;
        }
        public MPS_PurchReqTable SaveCancelPR(MPS_PurchReqTable purchReqTable, IEnumerable<MPS_PurchReqLine> purchReqLines)
        {
            if (purchReqTable != null)
            {
                purchReqTable.Status = "Cancel";
                purchReqTable.StatusFlag = 5;

                purchReqTable = _purchReqTableService.Update(purchReqTable, purchReqTable.REQ_ID);

                foreach (var purchReqLine in purchReqLines)
                {
                    //purchReqLine.REQ_ID = purchReqTable.REQ_ID;
                    purchReqLine.StatusFlag = 5;

                    _purchReqLineService.Update(purchReqLine, purchReqLine.REQ_ID, purchReqLine.ITEM_NUM);
                }
            }

            return purchReqTable;
        }
        public void DeletePR(MPS_PurchReqTable purchReqTable)
        {
            if (purchReqTable != null)
            {
                foreach (var purchReqLine in purchReqTable.PurchReqLines.ToListSafe())
                {
                    _purchReqLineService.Delete(purchReqLine);
                }

                _purchReqTableService.Delete(purchReqTable);

                _session.SaveChanges();
            }
        }
        public MPS_PurchOrderTable SaveCreatePO(MPS_PurchOrderTable purchOrderTable, IEnumerable<MPS_PurchOrderLine> purchOrderLines, IEnumerable<MPS_PurchReqLine> purchReqLines)
        {
            if (purchOrderTable != null)
            {
                purchOrderTable.Status = "Commit";
                purchOrderTable.StatusFlag = 1;

                purchOrderTable = _purchOrderTableService.Insert(purchOrderTable);

                foreach (var purchOrderLine in purchOrderLines)
                {
                    purchOrderLine.PO_ID = purchOrderTable.PO_ID;
                    purchOrderLine.StatusFlag = 1;

                    _purchOrderLineService.Insert(purchOrderLine);

                    var purchReqLine = purchReqLines.LastOrDefault(m => m.ITEM_NUM == purchOrderLine.PR_LINE_NUM);
                    if (purchReqLine != null)
                    {
                        purchReqLine.StatusFlag = DocumentStatusFlagId.InProgress;

                        _purchReqLineService.Update(purchReqLine, purchReqLine.REQ_ID, purchReqLine.ITEM_NUM);
                    }
                }
            }

            return purchOrderTable;
        }
        public MPS_PurchOrderTable SaveCancelPO(MPS_PurchOrderTable purchOrderTable, IEnumerable<MPS_PurchOrderLine> purchOrderLines)
        {
            if (purchOrderTable != null)
            {
                purchOrderTable.Status = "Cancel";
                purchOrderTable.StatusFlag = 5;

                purchOrderTable = _purchOrderTableService.Update(purchOrderTable, purchOrderTable.PO_ID);

                foreach (var purchOrderLine in purchOrderLines)
                {
                    purchOrderLine.StatusFlag = 5;

                    _purchOrderLineService.Update(purchOrderLine, purchOrderLine.PO_ID, purchOrderLine.ITEM_NUM);
                    if (purchOrderLine.PR_LINE_NUM.HasValue)
                    {
                        var purchReqLine =
                        _purchReqLineService.LastOrDefault(m => m.REQ_ID == purchOrderTable.REQ_ID && m.ITEM_NUM == purchOrderLine.PR_LINE_NUM.Value);
                        if (purchReqLine != null)
                        {
                            purchReqLine.StatusFlag = 5;
                            _purchReqLineService.Update(purchReqLine, purchReqLine.REQ_ID, purchReqLine.ITEM_NUM);
                        }
                    }


                }
            }

            return purchOrderTable;
        }

        public MPS_GoodsReceiptTable SaveCreateGR(MPS_GoodsReceiptTable goodsReceiptTable, IEnumerable<MPS_GoodsReceiptLine> goodsReceiptLines, string movementType)
        {
            if (goodsReceiptTable != null)
            {
                goodsReceiptTable.Status = MProcurementConstant.GRMovementType.GetStatusText(movementType);
                goodsReceiptTable.StatusFlag = MProcurementConstant.GRMovementType.GetStatusId(movementType);

                goodsReceiptTable = _goodsReceiptTableService.Insert(goodsReceiptTable);

                foreach (var goodsReceiptLine in goodsReceiptLines)
                {
                    goodsReceiptLine.RECEIPT_ID = goodsReceiptTable.RECEIPT_ID;
                    goodsReceiptLine.StatusFlag = 1;

                    _goodsReceiptLineService.Insert(goodsReceiptLine);
                }
            }

            return goodsReceiptTable;
        }

        #endregion

        public bool CreateBudgetPropTrans(int workflowTableId, string docType)
        {
            try
            {
                MPS_ProposalLine propLine;
                var result = false;
                switch (docType.ToLower())
                {
                    case "memo":
                        {
                            var memo = _memoService.Get(workflowTableId);
                            propLine = (memo != null && memo.ProposalLine != null) ? memo.ProposalLine : null;
                            if (propLine == null)
                                return false;
                            result = CreateTrans(new RequestViewModel()
                            {
                                ProposalNumber = propLine.ProposalTable.DocumentNumber,
                                Amount = memo.BudgetDetail.Value,
                                APCode = propLine.APCode,
                                APDescription = propLine.Description,
                                ExpenseTopic = propLine.ExpenseTopicCode,
                                UnitCode = propLine.UnitCode,
                                DocumentType = docType,
                                DocumentId = memo.Id,
                                DocumentStatus = "Create",
                                DocumentNumber = memo.DocumentNumber,
                                Status = "Reserve",
                                EmployeeId = memo.RequesterUserName

                            }, propLine);
                            if (result)
                            {
                                _session.SaveChanges();
                            }
                            return result;
                        }

                    case "pettycash":
                        {
                            var pettycash = _pettyCashService.Get(workflowTableId);
                            propLine = (pettycash != null && pettycash.ProposalLine != null) ? pettycash.ProposalLine : null;
                            if (propLine == null)
                                return false;
                            result = CreateTrans(new RequestViewModel()
                            {
                                ProposalNumber = propLine.ProposalTable.DocumentNumber,
                                Amount = pettycash.BudgetDetail,
                                APCode = propLine.APCode,
                                APDescription = propLine.Description,
                                ExpenseTopic = propLine.ExpenseTopicCode,
                                UnitCode = propLine.UnitCode,
                                DocumentType = docType,
                                DocumentId = pettycash.Id,
                                DocumentStatus = "Create",
                                DocumentNumber = pettycash.DocumentNumber,
                                Status = "Reserve",
                                EmployeeId = pettycash.RequesterUserName

                            }, propLine);
                            if (result)
                            {
                                _session.SaveChanges();
                            }
                            return result;
                        }

                    case "cashadvance":
                        {
                            var advance = _cashAdvanceService.Get(workflowTableId);
                            propLine = (advance != null && advance.ProposalLine != null) ? advance.ProposalLine : null;
                            if (propLine == null)
                                return false;
                            result = CreateTrans(new RequestViewModel()
                            {
                                ProposalNumber = propLine.ProposalTable.DocumentNumber,
                                Amount = advance.Budget,
                                APCode = propLine.APCode,
                                APDescription = propLine.Description,
                                ExpenseTopic = propLine.ExpenseTopicCode,
                                UnitCode = propLine.UnitCode,
                                DocumentType = docType,
                                DocumentId = advance.Id,
                                DocumentStatus = "Create",
                                DocumentNumber = advance.DocumentNumber,
                                Status = "Reserve",
                                EmployeeId = advance.RequesterUserName

                            }, propLine);
                            if (result)
                            {
                                _session.SaveChanges();
                            }
                            return result;
                        }
                    default:
                        return result;

                }

            }
            catch (Exception ex)
            {

                return false;
            }

        }

        public bool CancelBudgetPropTrans(int workflowTableId, string docType)
        {
            try
            {
                var result = false;
                MPS_ProposalLine propLine;
                switch (docType.ToLower())
                {
                    case "memo":
                        {
                            var memo = _memoService.Get(workflowTableId);
                            propLine = (memo != null && memo.ProposalLine != null) ? memo.ProposalLine : null;
                            if (propLine == null)
                                return false;
                            result = CancelTrans(new RequestViewModel()
                            {
                                ProposalNumber = propLine.ProposalTable.DocumentNumber,
                                Amount = memo.BudgetDetail.Value,
                                APCode = propLine.APCode,
                                APDescription = propLine.Description,
                                ExpenseTopic = propLine.ExpenseTopicCode,
                                UnitCode = propLine.UnitCode,
                                DocumentType = docType,
                                DocumentNumber = memo.DocumentNumber,
                                DocumentId = memo.Id,
                                Status = "Cancel",
                                DocumentStatus = "Cancel",
                                EmployeeId = memo.CreatedBy

                            }, "Reserve", propLine);
                            if (result)
                            {
                                _session.SaveChanges();
                            }
                            return result;
                        }
                    case "pettycash":
                        {
                            var pettycash = _pettyCashService.Get(workflowTableId);
                            propLine = (pettycash != null && pettycash.ProposalLine != null) ? pettycash.ProposalLine : null;
                            if (propLine == null)
                                return false;
                            result = CancelTrans(new RequestViewModel()
                            {
                                ProposalNumber = propLine.ProposalTable.DocumentNumber,
                                Amount = pettycash.BudgetDetail,
                                APCode = propLine.APCode,
                                APDescription = propLine.Description,
                                ExpenseTopic = propLine.ExpenseTopicCode,
                                UnitCode = propLine.UnitCode,
                                DocumentType = docType,
                                DocumentId = pettycash.Id,
                                DocumentNumber = pettycash.DocumentNumber,
                                Status = "Cancel",
                                DocumentStatus = "Cancel",
                                EmployeeId = pettycash.CreatedBy

                            }, "Reserve", propLine);
                            if (result)
                            {
                                _session.SaveChanges();
                            }
                            return result;
                        }
                    case "cashadvance":
                        {
                            var advance = _cashAdvanceService.Get(workflowTableId);
                            propLine = (advance != null && advance.ProposalLine != null) ? advance.ProposalLine : null;
                            if (propLine == null)
                                return false;
                            result = CancelTrans(new RequestViewModel()
                            {
                                ProposalNumber = propLine.ProposalTable.DocumentNumber,
                                Amount = advance.Budget,
                                APCode = propLine.APCode,
                                APDescription = propLine.Description,
                                ExpenseTopic = propLine.ExpenseTopicCode,
                                UnitCode = propLine.UnitCode,
                                DocumentType = docType,
                                DocumentId = advance.Id,
                                DocumentNumber = advance.DocumentNumber,
                                Status = "Cancel",
                                DocumentStatus = "Cancel",
                                EmployeeId = advance.CreatedBy

                            }, "Reserve", propLine);
                            if (result)
                            {
                                _session.SaveChanges();
                            }
                            return result;
                        }
                    default:
                        return result;

                }
            }
            catch (Exception ex)
            {

                return false;
            }
        }

        public bool ChangeBudgetPropTrans(int workflowTableId, string docType)
        {
            try
            {
                var result = false;
                MPS_ProposalLine propLine;
                switch (docType.ToLower())
                {
                    case "memo":
                        {
                            var memo = _memoService.Get(workflowTableId);
                            propLine = (memo != null && memo.ProposalLine != null) ? memo.ProposalLine : null;
                            if (propLine == null)
                                return false;
                            result = ChangeTrans(new RequestViewModel()
                            {
                                ProposalNumber = propLine.ProposalTable.DocumentNumber,
                                Amount = memo.BudgetDetail.Value,
                                APCode = propLine.APCode,
                                APDescription = propLine.Description,
                                ExpenseTopic = propLine.ExpenseTopicCode,
                                UnitCode = propLine.UnitCode,
                                DocumentType = docType,
                                DocumentId = memo.Id,
                                DocumentNumber = memo.DocumentNumber,
                                EmployeeId = memo.CreatedBy

                            }, "Reserve", propLine);
                            if (result)
                            {
                                _session.SaveChanges();
                            }
                            return result;
                        }
                    case "pettycash":
                        {
                            var pettycash = _pettyCashService.Get(workflowTableId);
                            propLine = (pettycash != null && pettycash.ProposalLine != null) ? pettycash.ProposalLine : null;
                            if (propLine == null)
                                return false;
                            result = ChangeTrans(new RequestViewModel()
                            {
                                ProposalNumber = propLine.ProposalTable.DocumentNumber,
                                Amount = pettycash.BudgetDetail,
                                APCode = propLine.APCode,
                                APDescription = propLine.Description,
                                ExpenseTopic = propLine.ExpenseTopicCode,
                                UnitCode = propLine.UnitCode,
                                DocumentType = docType,
                                DocumentId = pettycash.Id,
                                DocumentNumber = pettycash.DocumentNumber,
                                EmployeeId = pettycash.CreatedBy

                            }, "Reserve", propLine);
                            if (result)
                            {
                                _session.SaveChanges();
                            }
                            return result;
                        }
                    case "cashadvance":
                        {
                            var advance = _cashAdvanceService.Get(workflowTableId);
                            propLine = (advance != null && advance.ProposalLine != null) ? advance.ProposalLine : null;
                            if (propLine == null)
                                return false;
                            result = ChangeTrans(new RequestViewModel()
                            {
                                ProposalNumber = propLine.ProposalTable.DocumentNumber,
                                Amount = advance.Budget,
                                APCode = propLine.APCode,
                                APDescription = propLine.Description,
                                ExpenseTopic = propLine.ExpenseTopicCode,
                                UnitCode = propLine.UnitCode,
                                DocumentType = docType,
                                DocumentId = advance.Id,
                                DocumentNumber = advance.DocumentNumber,
                                EmployeeId = advance.CreatedBy

                            }, "Reserve", propLine);
                            if (result)
                            {
                                _session.SaveChanges();
                            }
                            return result;
                        }
                    default:
                        return result;

                }
            }
            catch (Exception ex)
            {

                return false;
            }
        }

        public bool CloseAdvanceBudgetPropTrans(int workflowTableId)
        {
            try
            {
                var result = false;
                var clearing = _cashClearingService.Get(workflowTableId);
                var advance = (clearing != null) ? clearing.CashAdvanceTable : null;
                var proposalLine = (advance != null) ? advance.ProposalLine : null;
                if (proposalLine == null)
                    return false;
                var amount = clearing.Budget - clearing.Actual;
                var budgetTransProposal = new MPS_BudgetProposalTrans()
                {
                    ProposalId = proposalLine.ParentId,
                    ProposalLineId = proposalLine.Id,
                    APCode = proposalLine.APCode,
                    UnitCode = proposalLine.UnitCode,
                    Status = "Return",
                    DocumentStatus = "Close",
                    DocumentId = clearing.Id,
                    DocumentType = "CashClearing",
                    DocumentNo = clearing.DocumentNumber,
                    RefDocumentId = advance.Id,
                    RefDocumentNumber = advance.DocumentNumber,
                    RefDocumentType = "CashAdvance",
                    Amount = amount * (-1),
                    TransDate = DateTime.Now
                };

                _budgetProposalTransService.Insert(budgetTransProposal);
                _session.SaveChanges();
                result = true;

                return result;


            }
            catch (Exception)
            {

                return false;
            }

        }
    }

}