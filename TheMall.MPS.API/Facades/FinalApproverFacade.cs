﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using TheMall.MPS.API.Facades.Abstracts;
using TheMall.MPS.API.Mappers;
using TheMall.MPS.API.Services;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Models.Master;
using TheMall.MPS.ViewModels.Master;

namespace TheMall.MPS.API.Facades
{
    public interface IFinalApproverFacade : IBaseMasterFacade<MPS_M_FinalApprover, FinalApproverViewModel>
    {
        IEnumerable<FinalApproverViewModel> Search(string username = null, string process = null);
        FinalApproverViewModel SearchByKeys(string processCode = null, string unitCode = null, string username = null);
    }

    public class FinalApproverFacade : BaseMasterFacade<MPS_M_FinalApprover, FinalApproverViewModel>, IFinalApproverFacade{
        private readonly IMPSSession _session;
        private readonly IFinalApproverService _service;
        private readonly IUnitCodeService _unitCodeService;
        private readonly IEmployeeTableService _employeeService;
        private readonly IProcessApproveService _processApproveService;
        public FinalApproverFacade(IMPSSession session, 
            IFinalApproverService service, 
            IUnitCodeService unitCodeService, 
            IEmployeeTableService employeeService, 
            IProcessApproveService processApproveService)
            : base(session, service)
        {
            _session = session;
            _service = service;
            _unitCodeService = unitCodeService;
            _employeeService = employeeService;
            _processApproveService = processApproveService;
        }

        public override async Task<FinalApproverViewModel> Create(FinalApproverViewModel viewModel)
        {
            try
            {
                var model = _service.Insert(viewModel.ToModel());
                await _session.SaveChangesAsync();
                return model.ToViewModel();
            }
            catch (Exception exception)
            {

                throw new Exception("Cannot insert duplicate code");
            }
        }

        public override async Task<FinalApproverViewModel> Update(FinalApproverViewModel viewModel)
        {
            var model = _service.Update(viewModel.ToModel());
            await _session.SaveChangesAsync();
            return model.ToViewModel();
        }

        public override async Task<FinalApproverViewModel> Delete(FinalApproverViewModel viewModel)
        {
            try
            {
                _service.Delete(viewModel.ToModel());
                await _session.SaveChangesAsync();
                return viewModel;
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
        }

        public IEnumerable<FinalApproverViewModel> Search(string username = null, string process = null)
        {
            var model = _service.Search(username, process);
            return model.Result.ToViewModels();
        }

        public FinalApproverViewModel SearchByKeys(string processCode = null, string unitCode = null,
            string username = null)
        {
            var model =
                _service.FirstOrDefault(
                    m => m.ProcessCode.Equals(processCode, StringComparison.InvariantCultureIgnoreCase) &&
                         m.UnitCode.Equals(unitCode, StringComparison.InvariantCultureIgnoreCase) &&
                         m.Username.Equals(username, StringComparison.InvariantCultureIgnoreCase));
            if (model != null)
            {
                var viewModel = model.ToViewModel();
                if (viewModel.UnitCode != null)
                {
                    viewModel.UnitCodeViewModel =
                        _unitCodeService.FirstOrDefault(
                            m => m.UnitCode.Equals(viewModel.UnitCode, StringComparison.InvariantCultureIgnoreCase)).ToViewModel();
                }

                if (viewModel.Username != null)
                {
                    viewModel.EmployeeViewModel = _employeeService.FirstOrDefault(
                        m=>m.Username.Equals(viewModel.Username, StringComparison.InvariantCultureIgnoreCase)).ToViewModel();
                }

                if (viewModel.ProcessCode != null)
                {
                   viewModel.ProcessApproveViewModel = _processApproveService.FirstOrDefault(
                        m => m.ProcessCode.Equals(viewModel.ProcessCode, StringComparison.InvariantCultureIgnoreCase))
                        .ToViewModel();
                }
                return viewModel;
            }
            return null;
        }
    }
}