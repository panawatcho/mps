﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TheMall.MPS.API.Mappers;
using TheMall.MPS.Models.Memo;
using TheMall.MPS.Reports.Memo;
using TheMall.MPS.Reports.Shared;

namespace TheMall.MPS.API.Facades
{
    public interface IReportMemoFacade
    {
        IEnumerable<MemoDataSource> ReportData(MPS_MemoTable memoViewModel);
        IEnumerable<MemoLinesDataSource> ReportLinesData(MPS_MemoTable memoViewModel);
        IEnumerable<ApproverDataSource> ApproversData(IEnumerable<MPS_MemoApproval> memoApproval);
    }
    public class ReportMemoFacade : IReportMemoFacade
    {
        private readonly IReportMemoMapper _reportMapper;

        public ReportMemoFacade(IReportMemoMapper reportMapper)
        {
            _reportMapper = reportMapper;
        }

        public  IEnumerable<MemoDataSource> ReportData(MPS_MemoTable memoModel)
        {
            //var cashAdvanceViewModel = _cashAdvanceFacade.GetViewModelFromDocumentNumber(documentNumber);
            var list = _reportMapper.ReportDataMapper(memoModel);
            return list;
        }
        public IEnumerable<MemoLinesDataSource> ReportLinesData(MPS_MemoTable memoModel)
        {
            //var cashAdvanceViewModel = _cashAdvanceFacade.GetViewModelFromDocumentNumber(documentNumber);
            var list = _reportMapper.ReportLinesDataMapper(memoModel);
            return list;
        }
        public IEnumerable<ApproverDataSource> ApproversData(IEnumerable<MPS_MemoApproval> memoApproval)
        {
            //var cashAdvanceViewModel = _cashAdvanceFacade.GetViewModelFromDocumentNumber(documentNumber);
            //var cashAdvanceApproverViewModel = _cashAdvanceApproveService.GetWhere(m => m.ParentId == id);
            var approversDataList = _reportMapper.ApproversDataMapper(memoApproval);
            return approversDataList;
        }
    }
}