﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TheMall.MPS.API.Mappers;
using TheMall.MPS.API.Services;
using TheMall.MPS.Models.CashAdvance;
using TheMall.MPS.Models.Proposal;
using TheMall.MPS.Reports.CashAdvance;
using TheMall.MPS.Reports.Shared;
using TheMall.MPS.ViewModels.CashAdvance;

namespace TheMall.MPS.API.Facades
{
    public interface IReportCashAdvanceFacade
    {
        IEnumerable<CashAdvanceDataSource> ReportData(MPS_CashAdvanceTable cashAdvanceViewModel);
        IEnumerable<ApproverDataSource> ApproversData( IEnumerable<MPS_CashAdvanceApproval> cashAdvanceApproval);
        IEnumerable<ProposalLineTemplateDataSource> SperatelyBranchPayPercent(IEnumerable<MPS_ProposalLineTemplate> proposalLineTemplates, int proposalRefId);
    }

    public class ReportCashAdvanceFacade : IReportCashAdvanceFacade
    {
        private readonly IReportCashAdvanceMapper _reportMapper;
        private readonly ICashAdvanceFacade _cashAdvanceFacade;
        private readonly ICashAdvanceApproveService _cashAdvanceApproveService;
        private readonly IProposalLineAPService _proposalLineTemplateService;
        private readonly IProposalLineSharedTemplateService _proposalLineSharedTemplateService;

        public ReportCashAdvanceFacade(
            IReportCashAdvanceMapper reportMapper,
            ICashAdvanceFacade cashAdvanceFacade, 
            ICashAdvanceApproveService cashAdvanceApproveService, 
            IProposalLineAPService proposalLineTemplateService, 
            IProposalLineSharedTemplateService proposalLineSharedTemplateService)
        {
            _reportMapper = reportMapper;
            _cashAdvanceFacade = cashAdvanceFacade;
            _cashAdvanceApproveService = cashAdvanceApproveService;
            _proposalLineTemplateService = proposalLineTemplateService;
            _proposalLineSharedTemplateService = proposalLineSharedTemplateService;
        }

        public IEnumerable<CashAdvanceDataSource> ReportData(MPS_CashAdvanceTable cashAdvanceViewModel)
        {
            //var cashAdvanceViewModel = _cashAdvanceFacade.GetViewModelFromDocumentNumber(documentNumber);
            var list = _reportMapper.ReportDataMapper(cashAdvanceViewModel);
            return list;
        }

        public IEnumerable<ApproverDataSource> ApproversData(IEnumerable<MPS_CashAdvanceApproval> cashAdvanceApproval)
        {
            //var cashAdvanceViewModel = _cashAdvanceFacade.GetViewModelFromDocumentNumber(documentNumber);
            //var cashAdvanceApproverViewModel = _cashAdvanceApproveService.GetWhere(m => m.ParentId == id);
            var approversDataList = _reportMapper.ApproversDataMapper(cashAdvanceApproval);
            return approversDataList;
        }

        public IEnumerable<ProposalLineTemplateDataSource> SperatelyBranchPayPercent(IEnumerable<MPS_ProposalLineTemplate> proposalLineTemplates, int proposalRefId)
        {
            var proposalLineSharedTemplates = _proposalLineSharedTemplateService.GetWhere(m => m.ParentId == proposalRefId && m.Selected);
            var proposalLineTemplatesData =  _reportMapper.ProposalLineTemplateDataMapper(proposalLineSharedTemplates, proposalLineTemplates);
          
            return proposalLineTemplatesData;
        }

    }
}