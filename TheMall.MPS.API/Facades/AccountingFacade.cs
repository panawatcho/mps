﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CrossingSoft.Framework.Infrastructure.EF6;
using TheMall.MPS.API.Mappers;
using TheMall.MPS.API.Services;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Models.Master;
using TheMall.MPS.ViewModels.Master;

namespace TheMall.MPS.API.Facades
{
    public interface IAccountingFacade
    {
        IEnumerable<PlaceViewModel> GetAccountingBranch();
        AccountingViewModel Create(AccountingViewModel accounting);
        AccountingViewModel Update(AccountingViewModel accounting);
        int DeleteInMaster(string branchCode = null, string usrname = null);
        AccountingViewModel GetByBranchCodeAndUsername(string branchCode = null, string username = null);
    }

    public class AccountingFacade : IAccountingFacade
    {
        private readonly IAccountingService _service;
        private readonly IAccountingMapper _accountingMapper;
        private readonly IMPSSession _session;
        public AccountingFacade(IAccountingService service, 
            IAccountingMapper accountingMapper, 
            IMPSSession session)
        {
            _service = service;
            _accountingMapper = accountingMapper;
            _session = session;
        }

        public IEnumerable<PlaceViewModel> GetAccountingBranch()
        {
            try
            {
                var allAccounting = _service.GetWhere(m=>!m.InActive);
                if (allAccounting != null)
                {
                    var accounting =
                        allAccounting.GroupBy(x => x.BranchCode)
                            .Select(g => g.First())
                            .ToList()
                            .ToViewModelsFromAccounting();

                    return accounting;
                }
                return null;
            }

            catch (Exception ex)
            {
                throw new HttpException(ex.Message);
            }
        }

        public AccountingViewModel Create(AccountingViewModel accountingViewModel)
        {
            try
            {
                var accountingModel = _accountingMapper.ToModel(accountingViewModel);
                if (accountingModel != null)
                {
                    _service.Insert(accountingModel);
                    _session.SaveChanges();
                    return _accountingMapper.ToViewModel(accountingModel);
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw new HttpException(ex.Message);
            }
        }

        public AccountingViewModel Update(AccountingViewModel accountingViewModel)
        {
            try
            {
                if (!String.IsNullOrEmpty(accountingViewModel.EmpId) &&
                    !String.IsNullOrEmpty(accountingViewModel.BranchCode))
                {
                    var accountingModel = _accountingMapper.ToModel(accountingViewModel);
                    if (accountingModel != null)
                    {
                        _service.Update(accountingModel);
                        _session.SaveChanges();
                        return _accountingMapper.ToViewModel(accountingModel);
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw new HttpException(ex.Message);
            }
        }
        public int DeleteInMaster(string branchCode = null, string usrname = null)
        {
            try
            {
                var modelDeleted = _service.FirstOrDefault(m => m.BranchCode.Equals(branchCode, StringComparison.InvariantCultureIgnoreCase)
                    && m.Username.Equals(usrname, StringComparison.InvariantCultureIgnoreCase));
                _service.Delete(modelDeleted);
                _session.SaveChanges();
                return 1;
            }
            catch (Exception ex)
            {
                throw new HttpException(ex.Message);
            }
        }

        public AccountingViewModel GetByBranchCodeAndUsername(string branchCode = null, string username = null)
        {
            var model =
                _service.FirstOrDefault(
                    m => m.BranchCode.Equals(branchCode, StringComparison.InvariantCultureIgnoreCase) &&
                         m.Username.Equals(username, StringComparison.InvariantCultureIgnoreCase) 
                         );
            if (model == null) return null;
            var viewModel = _accountingMapper.ToViewModel(model);
            return viewModel;
        }
    }
}