﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Routing;
using Microsoft.AspNet.Identity;
using SourceCode.Workflow.Client;
using TheMall.MPS.API.Facades.Abstracts;
using TheMall.MPS.API.Facades.Interfaces;
using TheMall.MPS.API.Mappers;
using TheMall.MPS.API.Services;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Models.Enums;
using TheMall.MPS.Models.K2;
using TheMall.MPS.Models.Memo;
using TheMall.MPS.Models.PettyCash;
using TheMall.MPS.Models.Workflow;
using TheMall.MPS.Resources;
using TheMall.MPS.ViewModels.PettyCash;
using TheMall.MPS.ViewModels.Proposal;

namespace TheMall.MPS.API.Facades
{
    public interface IPettyCashFacade : IMpsWorkflowFacade<MPS_PettyCashTable, MPS_PettyCashAttachment, MPS_PettyCashComment, PettyCashViewModel>
    {
        Task<PettyCashViewModel> GetDataBindPettyCash(int propLineid);
        PettyCashViewModel PettyCashFromDocumentNumber(string documentNumber);
        void SetFinalApproverDeleted(int id);
        Task<IEnumerable<PettyCashViewModel>> GetRelatedWorkflowTable();
        string GetEmailInformAll(int workflowTableId);
        PettyCashViewModel DeleteBy(int id, string username);
    }
    public class PettyCashFacade : BaseMpsWorkflowFacade<MPS_PettyCashTable, MPS_PettyCashAttachment, MPS_PettyCashComment, PettyCashViewModel>, IPettyCashFacade
    {
        private readonly IPettyCashService _workflowTableService;
        private readonly IPettyCashMapper _mapper;
        private readonly IPettyCashLineMapper _lineMapper;
        private readonly IPettyCashLineService _lineService;
        private readonly IPettyCashApprovalMapper _approvalMapper;
        private readonly IUnitCodeForEmployeeService _unitCodeForEmployeeService;
        private readonly IPettyCashApprovalService _approvalService;
        private readonly IProposalLineMapper _proposallineMapper;
        private readonly IProposalLineService _proposallineService;
        private readonly IProposalMapper _proposalMapper;
        private readonly IBudgetProposalTransService _budgetProposalTransService;
        private readonly IBudgetProposalTransFacade _budgetProposalTransFacade;
        private readonly IEmployeeTableService _employeeTableService;
        private readonly IProposalBudgetPlanService _budgetPlanService;
        private readonly ICompanyService _companyService;

        public PettyCashFacade(IMPSSession session,
            IPettyCashService workflowTableService, 
            IAttachmentService<MPS_PettyCashAttachment, MPS_PettyCashTable> attachmentService, 
            ICommentService<MPS_PettyCashComment> commentService, 
            IWorkflowService<MPS_PettyCashTable> workflowService, 
            INumberSequenceService numberSequenceService, 
            IPettyCashProcess definition,
            IPettyCashMapper mapper,
            IPettyCashLineMapper lineMapper,
            IPettyCashLineService lineService,            
            IProposalLineService proposallineService,
            IPettyCashApprovalMapper approvalMapper,
            IUnitCodeForEmployeeService unitCodeForEmployeeService,
            IPettyCashApprovalService approvalService, 
            IProposalLineMapper proposallineMapper,
            IProposalMapper proposalMapper, 
            IBudgetProposalTransService budgetProposalTransService,
            IBudgetProposalTransFacade budgetProposalTransFacade, 
            IEmployeeTableService employeeTableService, 
            IProposalBudgetPlanService budgetPlanService, 
            ICompanyService companyService) 
            : base(session, 
                workflowTableService, 
                attachmentService, 
                commentService, 
                workflowService, 
                numberSequenceService, 
                definition)
        {
            _workflowTableService = workflowTableService;
            _mapper = mapper;
            _lineMapper = lineMapper;
            _lineService = lineService;
            _proposallineService = proposallineService;
            _approvalMapper = approvalMapper;
            _unitCodeForEmployeeService = unitCodeForEmployeeService;
            _approvalService = approvalService;
            _proposallineMapper = proposallineMapper;
            _proposalMapper = proposalMapper;
            _budgetProposalTransService = budgetProposalTransService;
            _budgetProposalTransFacade = budgetProposalTransFacade;
            _employeeTableService = employeeTableService;
            _budgetPlanService = budgetPlanService;
            _companyService = companyService;
        }

        public override async Task<PettyCashViewModel> GetViewModel(UrlHelper url, int id, string sn = null)
        {
            var model = await _workflowTableService.GetAsync(id);
            var viewModel = await InitWorkflowViewModel(url, id, sn, _mapper.ToViewModel(model));

            return viewModel;
        }
        public async Task<IEnumerable<PettyCashViewModel>> GetRelatedWorkflowTable()
        {
            var listModel = new List<PettyCashViewModel>();
            var username = HttpContext.Current.User.Identity.GetUserName();
            var model = _workflowTableService.GetWhere(m => !m.Deleted);
            var unitCode = _unitCodeForEmployeeService.GetUnitCodeCurrentUser();

            var result =
            (
                from unit in unitCode
                join petty in model on unit.UnitCode equals petty.UnitCode into pt
                from petty in pt.DefaultIfEmpty()
                select new
                {
                    petty = petty ?? new MPS_PettyCashTable()
                }
            ).ToListSafe();

            if (!result.Any()) return listModel;
            listModel.AddRange(
                result.Select(data => _mapper.ToViewModel(data.petty)));

            return listModel;
        }

        public override async Task<PettyCashViewModel> SaveAsync(PettyCashViewModel viewModel, string action = null)
        {
             if (viewModel == null)
                throw new ArgumentNullException("viewModel");

             //if (viewModel.BudgetDetail > viewModel.BudgetAPCode)
             //    throw new System.InvalidOperationException("Budget Detail sum value must not be more than Budget A&P Code!!!");

            if (viewModel.Company == null)
            {
                var vendorCode = _companyService.Find(m => m.VendorCode.Equals(viewModel.Branch.PlaceCode));
                if (vendorCode != null)
                {
                    viewModel.Company = vendorCode.ToViewModel();
                }
                else
                {
                    viewModel.Company = null;
                }
            }
            
            var model = _mapper.ToModel(viewModel);
            try
            {
                if (viewModel.Id <= 0)
                {
                    model = _workflowTableService.Insert(model);
                }
                else
                {
                    model = _workflowTableService.Update(model);
                }
              
                var pettyLines = _lineMapper.ToModels(viewModel.PettyCashLines, model.PettyCashLine).ToListSafe();
                foreach (var line in pettyLines)
                {
                    if (line.Id <= 0 || line.ParentId <= 0)
                    {
                        if (model.Id <= 0)
                        {
                            line.PettyCashTable = model;
                        }
                        else
                        {
                            line.ParentId = model.Id;
                        }
                        _lineService.Insert(line);
                    }
                    else
                    {
                        if (line.Deleted)
                        {
                            _lineService.Delete(line);
                        }
                        else
                        {
                            _lineService.Update(line, line.Id);
                        }
                    }
                }
               
                var approval = _approvalMapper.ToModels(viewModel.PettyCashApproval, model.PettyCashApproval).ToListSafe();
                foreach (var approve in approval)
                {
                    if (approve.Id <= 0 || approve.ParentId <= 0)
                    {
                        if (model.Id <= 0)
                        {
                            approve.PettyCashTable = model;
                        }
                        else
                        {
                            approve.ParentId = model.Id;
                        }
                        _approvalService.Insert(approve);
                    }
                    else
                    {
                        if (approve.Deleted)
                        {
                            _approvalService.Delete(approve);
                        }
                        else
                        {
                            _approvalService.Update(approve, approve.Id);
                        }
                    }
                }
                await Session.SaveChangesAsync();
                return _mapper.ToViewModel(model);
            }
            catch (Exception ex)
            {
                throw new HttpException(ex.Message);
            }
        }

        public override async Task<bool> UpdateAfterStartProcess(MPS_PettyCashTable workflowTable, int? procInstId)
        { 
            _budgetProposalTransFacade.CreateBudgetPropTrans(workflowTable.Id, "pettycash");
            var result = await base.UpdateAfterStartProcess(workflowTable, procInstId);

            return result;
        }

        public override async Task<MPS_PettyCashComment> Submit(string sn, string action, string comment, PettyCashViewModel viewModel)
        {
            var result = await base.Submit(sn, action, comment, viewModel);
            if (result != null)
            {
                if (result.Action.ToUpper().Equals("RESUBMIT", StringComparison.CurrentCultureIgnoreCase))
                {
                    _budgetProposalTransFacade.ChangeBudgetPropTrans(result.ParentId, "pettycash");
                    var approval = _approvalService.GetWhere(m => m.ParentId == viewModel.Id && !m.Deleted);
                    foreach (var a in approval)
                    {
                        a.ApproveStatus = false;
                        _approvalService.Update(a, a.Id);
                    }
                    await Session.SaveChangesAsync();
                }
                else
                {
                    var activity = result.Activity.Replace("Approval", "").Replace("-", "").Replace(" ", "");
                    var modelApprove1 = _approvalService.GetWhere(m => activity.Equals(m.ApproverLevel)
                                                                       && m.ParentId == result.ParentId).ToList();
                    var modelApprove = modelApprove1.LastOrDefault(m => m.ApproverUserName.ToLower().Equals(result.CreatedBy.ToLower()));

                    if (modelApprove != null)
                    {
                        modelApprove.ApproveStatus = true;
                        _approvalService.Update(modelApprove, modelApprove.Id);
                        Session.SaveChanges();
                    }
                }
            }

            return result;
        }   

        public async Task<PettyCashViewModel> GetDataBindPettyCash(int propLineid)
        {
            var viewModel = new PettyCashViewModel();
            if (propLineid != 0)
            {
                var propLine = _proposallineService.Find(propLineid);
                if (propLine != null)
                {
                    var proposalRef = propLine.ProposalTable;
                    var propLineViewModel = _proposallineMapper.ToViewModel(propLine);
                    viewModel.BudgetAPCode = propLineViewModel.BudgetPlan;
                    viewModel.ProposalRef = _proposalMapper.ToProposalInfoViewModel(proposalRef);
                    viewModel.UnitCode = propLineViewModel.UnitCode;
                    viewModel.APCode = viewModel.ProposalRef.APCode.LastOrDefault(m => m.ProposalLineId == propLineid);
                    viewModel.ExpenseTopic = propLineViewModel.ExpenseTopic;
                    viewModel.ProposalLineId = propLineid;
                    viewModel.PettyCashLines = new List<PettyCashLineViewModel>();
                    viewModel.PettyCashApproval = new List<PettyCashApprovalViewModel>();
                }
            }

            return viewModel;
        }

        public PettyCashViewModel PettyCashFromDocumentNumber(string documentNumber)
        {
            var model = _workflowTableService.FirstOrDefault(
                m => m.DocumentNumber.Equals(documentNumber, StringComparison.InvariantCultureIgnoreCase));
            var viewModel = _mapper.ToViewModel(model);

            return viewModel;
        }
        public override IEnumerable<ValidationResult> ValidateStartProcess(MPS_PettyCashTable workflowTable)
        {
            if (workflowTable.ProposalLine.ProposalTable.CloseFlag == true)
            {
                yield return new ValidationResult("Cannot submit because your proposal was closed!!!");
            }

            //var budget = _budgetProposalTransService.GetBudgetTransTotal(workflowTable.ProposalLineID);
            var budget = _budgetProposalTransService.GetBudgetTransTotal(workflowTable.ProposalLine.ParentId,
                workflowTable.ProposalLine.APCode, workflowTable.ProposalLine.UnitCode);
            //var balance = workflowTable.ProposalLine.BudgetPlan - budget;
            var propBudgetPlan = _budgetPlanService.GetProposalBudgetPlan(workflowTable.ProposalLine.ParentId,
                workflowTable.ProposalLine.APCode, workflowTable.ProposalLine.UnitCode);
            var balance = (propBudgetPlan != null) ? propBudgetPlan.BudgetPlan - budget : budget;

            if (balance < workflowTable.BudgetDetail)
            {
                yield return new ValidationResult("Budget exceed: Statements remainning " + balance + " baht");
            }

            //if (!workflowTable.PettyCashApproval.Any(m => m.ApproverLevel.Equals(Approval.ApproveLevel.Propose) && !m.Deleted))
            //{
            //    yield return new ValidationResult("Please select approver level propose!!!");
            //}
            //if (!workflowTable.PettyCashApproval.Any(m => m.ApproverLevel.Equals(Approval.ApproveLevel.Accept) && !m.Deleted))
            //{
            //    yield return new ValidationResult("Please select approver level accept!!!");
            //}
            //if (!workflowTable.PettyCashApproval.Any(m => m.ApproverLevel.Equals(Approval.ApproveLevel.Approval) && !m.Deleted))
            //{
            //    yield return new ValidationResult("Please select a approver!!!");
            //}

            if (!workflowTable.PettyCashApproval.Any(m => m.ApproverLevel.Equals(Approval.ApproveLevel.FinalApproval) && !m.Deleted && m.LockEditable))
            {
                yield return new ValidationResult("Please select a final apporver!!!");
            }

            if (workflowTable.BudgetDetail <= 0)
            {
                yield return new ValidationResult("Butget must not be 0");
            }
        }

        public override IEnumerable<ValidationResult> ValidateSubmit(MPS_PettyCashTable workflowTable,
            WorklistItem worklistItem, string action, PettyCashViewModel viewModel)
        {
            if (action.ToUpper().Equals("RESUBMIT"))
            {
                var budgetTrans = _budgetProposalTransService.LastOrDefault(m => m.ProposalLineId == workflowTable.ProposalLine.Id 
                    && m.DocumentType == Budget.DocumentType.PettyCash
                    && m.DocumentNo == workflowTable.DocumentNumber
                    && m.Status == Budget.BudgetStatus.Reserve
                    && !m.InActive);

                if (budgetTrans == null)
                {
                    yield return new ValidationResult(string.Format(Resources.Error.BudgetTransNotFound, workflowTable.DocumentNumber));
                }

                var budgetPlan = _budgetPlanService.GetProposalBudgetPlan(workflowTable.ProposalLine.ParentId, workflowTable.ProposalLine.APCode, workflowTable.ProposalLine.UnitCode);
                if (budgetPlan == null)
                {
                    yield return new ValidationResult(string.Format(Error.ProposalBudgetPlanNotFound,
                        workflowTable.ProposalLine.ProposalTable.DocumentNumber, workflowTable.ProposalLine.APCode,
                        workflowTable.ProposalLine.UnitCode));
                }

                var budgetTransTotal = _budgetProposalTransService.GetBudgetTransTotal(workflowTable.ProposalLine.ParentId, workflowTable.ProposalLine.APCode, workflowTable.ProposalLine.UnitCode);

                var budgetBalance = (budgetPlan.BudgetPlan - budgetTransTotal + budgetTrans.Amount);
                if (budgetBalance < viewModel.BudgetDetail)
                {
                    yield return new ValidationResult("Budget exceed: Statements remainning " + budgetBalance + " baht");
                }

                //var budget = _budgetProposalTransService.GetBudgetDetailAp(workflowTable.ProposalLineID, workflowTable.Id);
                //var balance = workflowTable.ProposalLine.BudgetPlan - budget;
                //if (balance < workflowTable.BudgetDetail)
                //{
                //    yield return new ValidationResult("Budget exceed: Statements remainning " + balance
                //                                      + " baht");
                //}

                if (viewModel.ProposalRef.CloseFlag == true)
                {
                    yield return new ValidationResult("Cannot submit because your proposal was closed!!!");
                }

                //if (
                //    !viewModel.PettyCashApproval.Any(
                //        m => m.ApproverLevel.Equals(Approval.ApproveLevel.Propose) && !m.Deleted))
                //{
                //    yield return new ValidationResult("Please select approver level propose!!!");
                //}
                //if (
                //    !viewModel.PettyCashApproval.Any(
                //        m => m.ApproverLevel.Equals(Approval.ApproveLevel.Accept) && !m.Deleted))
                //{
                //    yield return new ValidationResult("Please select approver level accept!!!");
                //}
                //if (
                //    !viewModel.PettyCashApproval.Any(
                //        m => m.ApproverLevel.Equals(Approval.ApproveLevel.Approval) && !m.Deleted))
                //{
                //    yield return new ValidationResult("Please select a approver!!!");
                //}

                if (!viewModel.PettyCashApproval.Any(m => m.ApproverLevel.Equals(Approval.ApproveLevel.FinalApproval) 
                                                          && !m.Deleted 
                                                          && m.LockEditable))
                {
                    yield return new ValidationResult("Please select a final apporver!!!");
                }

                if (viewModel.BudgetDetail <= 0)
                {
                    yield return new ValidationResult("Butget must not be 0");
                }

                //if (viewModel.BudgetDetail > viewModel.BudgetAPCode)
                //{
                //    yield return new ValidationResult("Budget Deatil sum value must not be more than Budget A&P Code!!!");
                //}
            }
        }

        public void SetFinalApproverDeleted(int id)
        {
            try
            {
                var finalApprovers = _approvalService.GetWhere(m => m.ParentId == id && m.ApproverLevel == "อนุมัติขั้นสุดท้าย");
                foreach (var finalApprover in finalApprovers)
                {
                    _approvalService.Delete(finalApprover);
                }
                Session.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new HttpException(ex.Message);
            }
        }

        public string GetEmailInformAll(int workflowTableId)
        {
            string email = string.Empty;
            var model = _workflowTableService.Find(m => m.Id == workflowTableId);
            if (model != null)
            {
                email += string.IsNullOrEmpty(email) ? model.CCEmail : string.Format(";{0}", model.CCEmail).TrimEnd(';');
                email += string.IsNullOrEmpty(email) ? model.InformEmail : string.Format(";{0}", model.InformEmail).TrimEnd(';');
                email += string.IsNullOrEmpty(email) ? model.AccountingEmail : string.Format(";{0}", model.AccountingEmail).TrimEnd(';');

                if (model.PettyCashApproval != null)
                {
                    foreach (var userapprove in model.PettyCashApproval)
                    {
                        if (!string.IsNullOrEmpty(userapprove.ApproverEmail))
                        {
                            email += string.IsNullOrEmpty(email)
                                ? userapprove.ApproverEmail
                                : string.Format(";{0}", userapprove.ApproverEmail).TrimEnd(';');
                        }
                        else
                        {
                            var employee = _employeeTableService.FindByUsername(userapprove.ApproverUserName);
                            if (employee != null && !string.IsNullOrEmpty(employee.Email))
                            {
                                email += string.IsNullOrEmpty(email)
                                    ? employee.Email
                                    : string.Format(";{0}", employee.Email).TrimEnd(';');
                            }
                        }
                    }
                }
            }
            if (!string.IsNullOrEmpty(email))
            {
                string[] listEmail = email.TrimEnd(';').Split(';');
                string[] newlistEmail = listEmail.Distinct().ToArray();
                email = newlistEmail.Aggregate(string.Empty, (current, mail) => current + (string.IsNullOrEmpty(current) ? mail : string.Format(";{0}", mail)));
            }

            return email;
        }

        public PettyCashViewModel DeleteBy(int id, string username)
        {
            try
            {
                if (id > 0)
                {
                    var model = _workflowTableService.FirstOrDefault(m => m.Id == id);
                    var employeeViewModel = _employeeTableService.FindByUsername(username).ToViewModel();
                    if (model != null && employeeViewModel != null)
                    {
                        if (model.StatusFlag == 0)
                        {
                            var modelDelete = _mapper.DeleteBy(model, employeeViewModel);
                            if (modelDelete != null)
                            {
                                modelDelete = _workflowTableService.Update(modelDelete);
                                var value = Session.SaveChanges();
                                if (value > 0)
                                {
                                    var viewModel = _mapper.ToViewModel(modelDelete);
                                    return viewModel;
                                }
                            }
                        }
                        else if (model.StatusFlag == 9)
                        {
                            model.StatusFlag = 5;
                            model.CancelledDate = DateTime.Now.ToLocalTime();
                            model.Status = DocumentStatusFlag.Cancelled.ToString();
                            model.DocumentStatus = DocumentStatusFlag.Cancelled.ToString();
                            var cancelModel = _workflowTableService.Update(model, model.Id);
                            var result = _budgetProposalTransFacade.CancelBudgetPropTrans(model.Id, "pettycash");
                            if (result)
                            {
                                Session.SaveChanges();
                            }
                            else
                            {
                                throw new Exception("ไม่สามารถลบรายการได้ กรุณาติดต่อผู้ดูแลระบบ");
                            }
                            return _mapper.ToViewModel(cancelModel);
                        }
                    }
                }

                return null;
            }
            catch (Exception ex)
            {
                throw new HttpException(ex.Message);
            }
        }
    }
}