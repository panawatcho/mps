﻿using System;
using System.Threading.Tasks;
using CrossingSoft.Framework.Services;
using TheMall.MPS.API.Facades.Interfaces;
using TheMall.MPS.API.Mappers;
using TheMall.MPS.API.Services;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Models.Example;
using TheMall.MPS.ViewModels.Example;

namespace TheMall.MPS.API.Facades
{
    public interface IExampleFacade : IHaveAttachmentFacade<ExampleAttachment>
    {
        Task<ExampleViewModel> GetViewModel(System.Web.Http.Routing.UrlHelper url, string id);
        ExampleViewModel ToViewModel(System.Web.Http.Routing.UrlHelper url, ExampleTable model);
        Task<ExampleTable> Update(ExampleViewModel viewModel);
    }

    //public class ExampleFacade : Abstracts.BaseUploadFacade<ExampleAttachment>, IExampleFacade
    //{
    //    private readonly IMPSSession _session;
    //    private readonly IExampleService _service;
    //    private readonly IServiceAsync<ExampleLine> _lineService;

    //    public ExampleFacade(IMPSSession session,
    //        IAttachmentService<ExampleAttachment> attachmentService,
    //        IExampleService service, 
    //        IServiceAsync<ExampleLine> lineService)
    //        :base (session, attachmentService)
    //    {
    //        _session = session;
    //        _service = service;
    //        _lineService = lineService;
    //    }

    //    public async Task<ExampleViewModel> GetViewModel(System.Web.Http.Routing.UrlHelper url, string id)
    //    {
    //        int num;
    //        if (Int32.TryParse(id, out num))
    //        {
    //            return await GetViewModelById(url, num);
    //        }
    //        else
    //        {
    //            return await GetViewModelByDocumentNumber(url, id);
    //        }
    //    }

    //    protected virtual async Task<ExampleViewModel> GetViewModelById(System.Web.Http.Routing.UrlHelper url, int id, string sn = null)
    //    {
    //        return ToViewModel(url, await _service.FindAsync(m => m.Id == id));
    //    }

    //    protected virtual async Task<ExampleViewModel> GetViewModelByDocumentNumber(System.Web.Http.Routing.UrlHelper url, string documentNumber, string sn = null)
    //    {
    //        return ToViewModel(url, await _service.FindAsync(m => !string.IsNullOrEmpty(m.DocumentNumber) && m.DocumentNumber.Equals(documentNumber, StringComparison.InvariantCultureIgnoreCase)));
    //    }

    //    public ExampleViewModel ToViewModel(System.Web.Http.Routing.UrlHelper url, ExampleTable model)
    //    {
    //        return model.ToViewModel(url);
    //    }

    //    public async Task<ExampleTable> Update(ExampleViewModel viewModel)
    //    {
    //        if (viewModel == null)
    //            throw new ArgumentNullException("viewModel");

    //        var model = viewModel.ToModel(await _service.FindAsync(m => m.Id == viewModel.Id));

    //        _service.Update(model);

    //        var lines = viewModel.Lines.ToModel(model.Lines.ToListSafe());
    //        foreach (var line in lines)
    //        {
    //            if (line.Id <= 0)
    //            {
    //                if (model.Id <= 0)
    //                {
    //                    line.ExampleTable = model;
    //                }
    //                else
    //                {
    //                    line.ParentId = model.Id;
    //                }
    //                _lineService.Insert(line);
    //            }
    //            else
    //            {
    //                if (line.Deleted)
    //                {
    //                    _lineService.Delete(line);
    //                }
    //                else
    //                {
    //                    _lineService.Update(line, line.Id);
    //                }
    //            }
    //        }

    //        await _session.SaveChangesAsync();

    //        return model;
    //    }
    //}
}