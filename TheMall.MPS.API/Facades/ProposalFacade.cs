﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Core;
using System.Linq;
using System.Security.Policy;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Routing;
using AutoMapper.Execution;
using CrossingSoft.Framework.Models.Enums;
using SourceCode.Workflow.Client;
using TheMall.MPS.API.Facades.Abstracts;
using TheMall.MPS.API.Facades.Interfaces;
using TheMall.MPS.API.Mappers;
using TheMall.MPS.API.Services;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Models;
using TheMall.MPS.Models.Budget;
using TheMall.MPS.Models.Enums;
using TheMall.MPS.Models.K2;
using TheMall.MPS.Models.Memo;
using TheMall.MPS.Models.Proposal;
using TheMall.MPS.Models.Workflow;
using TheMall.MPS.ViewModels;
using TheMall.MPS.ViewModels.InterfaceToMprocurement;
using TheMall.MPS.ViewModels.Master;
using TheMall.MPS.ViewModels.Proposal;

namespace TheMall.MPS.API.Facades
{
    public interface IProposalFacade : IMpsWorkflowFacade<MPS_ProposalTable, MPS_ProposalAttachment, MPS_ProposalComment, ProposalViewModel>
    {
        // Task<ProposalViewModel> SaveAsyncs(UrlHelper url, ProposalViewModel viewModel, string action = null);
        // List<DocumentNumberViewModel> GetProposalNumber();
        List<DocumentNumberViewModel> GetDepositNumber(string code = null, string name = null);
        List<ProposalInfoViewModel> GetProposalNumber(string code = null, string name = null, string unitCode = "");
        IEnumerable<ProposalLineViewModel> GetProposalLineData(string unitCode, int parentId);
        ProposalInfoViewModel GetProposalOneLineFromId(int id);
        List<DepositNumberViewModel> GetIncomeDepositNumber(string code = null, string name = null);
        List<EmailForAPCodeViewModel> GetEmailInformForApList(int workflowTableId);
        ReturnMessage CloseProposal(int propId, string username); //
        void SetFinalApproverDeleted(int id);
        string GetEmailInformAll(int workflowTableId);
        string GetEmailInformForAp(int workflowTableId);
        ProposalViewModel DeleteBy(int id, string username);
        ProposalTrackingViewModel ProposalTrackingViewModel(int proposalId);
        bool UpdateReviseCompleted(int workflowTableId);
    }

    public class ProposalFacade : BaseMpsWorkflowFacade<MPS_ProposalTable, MPS_ProposalAttachment, MPS_ProposalComment, ProposalViewModel>, IProposalFacade
    {
        private readonly IProposalService _workflowTableService;
        private readonly IProposalMapper _mapper;
        private readonly IProposalLineMapper _lineMapper;
        private readonly IProposalLineService _lineService;
        private readonly IProposalDepositLineMapper _depositMapper;
        private readonly IProposalDepositLineService _depositService;
        private readonly IProposalIncomeDepositMapper _incomeDepositMapper;
        private readonly IProposalIncomeDepositService _incomeDepositService;
        private readonly IProposalIncomeOtherMapper _incomeOtherMapper;
        private readonly IProposalIncomeOtherService _incomeOtherService;
        private readonly IProposalIncomeTotalSaleMapper _incomeTotalSaleMapper;
        private readonly IProposalIncomeTotalSaleService _incomeTotalSaleService;
        private readonly IProposalIncomeTotalSaleTemplateMapper _incomeTotalSaleTemplateMapper;
        private readonly IProposalIncomeTotalSaleTemplateService _incomeTotalSaleTemplateService;
        private readonly IBudgetYearTransService _budgetYearTransService;
        private readonly IBudgetProposalTransService _budgetProposalTransService;
        private readonly IBudgetDepositTransFacade _budgetDepositTransFacade;
        private readonly IProposalEstimateTotalSaleMapper _estimateTotalSaleMapper;
        private readonly IProposalEstimateTotalSaleService _estimateTotalSaleService;
        private readonly IProposalApprovalMapper _approvalMapper;
        private readonly IProposalApprovalService _approvalService;
        private readonly ISharedBranchService _sharedBranchService;
        private readonly IProposalLineSharedTemplateService _sharedTemplateService;
        private readonly IProposalLineSharedTemplateMapper _sharedTemplateMapper;
        private readonly IProposalLineTemplateMapper _proposalLineTemplateMapper;
        private readonly IProposalLineAPService _proposalLineApService;
        private readonly INumberSequenceService _numberSequenceService;
        private readonly ITotalSaleLastYearService _lastYearService;
        private readonly IBudgetYearPlanService _yearPlanService;
        private readonly IAccountTransService _accountTransService;
        private readonly IUnitCodeForEmployeeService _unitCodeForEmployeeService;
        private readonly IBudgetYearTransFacade _budgetYearTransFacade;
        private readonly IMemoService _memoService;
        private readonly IPettyCashService _pettyCashService;
        private readonly ICashAdvanceService _advanceService;
        private readonly ICashClearingService _clearingService;
        private readonly IEmployeeTableService _employeeTableService;
        private readonly IBudgetDepositTransService _budgetDepositTransService;
        private readonly IProposalTrackingViewService _proposalTrackingViewService;
        private readonly ILogProposalService _logProposalService;
        private readonly IProposalBudgetPlanService _proposalBudgetPlanService;
        private readonly IProposalBudgetPlanMapper _budgetPlanMapper;
        private readonly IMProcurementViewService _mProcurement;
        private readonly ICommentService<MPS_ProposalComment> _commentService;
        private readonly IAllocationBasisService _allocationBasisService;
        private readonly IThemeFacade _themeFacade;
        private readonly IThemeService _themeService;

        public ProposalFacade(
            IMPSSession session,
            IProposalService workflowTableService,
            IAttachmentService<MPS_ProposalAttachment, MPS_ProposalTable> attachmentService,
            ICommentService<MPS_ProposalComment> commentService,
            IWorkflowService<MPS_ProposalTable> workflowService,
            INumberSequenceService numberSequenceService,
            IProposalProcess definition,
            IProposalMapper mapper,
            IProposalLineMapper lineMapper,
            IProposalLineService lineService,
            IProposalDepositLineMapper depositMapper,
            IProposalDepositLineService depositService,
            IProposalIncomeDepositMapper incomeDepositMapper,
            IProposalIncomeDepositService incomeDepositService,
            IProposalIncomeOtherMapper incomeOtherMapper,
            IProposalIncomeOtherService incomeOtherService,
            IProposalIncomeTotalSaleMapper incomeTotalSaleMapper,
            IProposalIncomeTotalSaleService incomeTotalSaleService,
            IProposalEstimateTotalSaleMapper estimateTotalSaleMapper,
            IProposalEstimateTotalSaleService estimateTotalSaleService,
            IProposalApprovalMapper approvalMapper,
            IProposalApprovalService approvalService,
            ISharedBranchService sharedBranchService,
            IProposalLineSharedTemplateService sharedTemplateService,
            IProposalLineTemplateMapper proposalLineTemplateMapper,
            IProposalLineAPService proposalLineApService,
            IProposalLineSharedTemplateMapper sharedTemplateMapper,
            INumberSequenceService numberSequenceService1,
            IProposalIncomeTotalSaleTemplateMapper incomeTotalSaleTemplateMapper,
            IProposalIncomeTotalSaleTemplateService incomeTotalSaleTemplateService,
            IBudgetYearTransService budgetYearTransService,
            ITotalSaleLastYearService lastYearService,
            IBudgetYearPlanService yearPlanService, 
            IAccountTransService accountTransService,
            IUnitCodeForEmployeeService unitCodeForEmployeeService,
            IBudgetYearTransFacade budgetYearTransFacade,
            IMemoService memoService,
            IPettyCashService pettyCashService, 
            ICashAdvanceService advanceService,
            ICashClearingService clearingService, 
            IEmployeeTableService employeeTableService,
            IBudgetProposalTransService budgetProposalTransService,
            IBudgetDepositTransFacade budgetDepositTransFacade,
            IBudgetDepositTransService budgetDepositTransService, 
            IProposalTrackingViewService proposalTrackingViewService,
            ILogProposalService logProposalService,
            IProposalBudgetPlanService proposalBudgetPlanService,
            IProposalBudgetPlanMapper budgetPlanMapper, 
            IMProcurementViewService mProcurement,
            IAllocationBasisService allocationBasisService,
            IThemeFacade themeFacade,
            IThemeService themeService)
            : base(session, 
                workflowTableService, 
                attachmentService, 
                commentService, 
                workflowService, 
                numberSequenceService, 
                definition)
        {
            _workflowTableService = workflowTableService;
            _mapper = mapper;
            _lineMapper = lineMapper;
            _lineService = lineService;
            _depositMapper = depositMapper;
            _depositService = depositService;
            _incomeDepositMapper = incomeDepositMapper;
            _incomeDepositService = incomeDepositService;
            _incomeOtherMapper = incomeOtherMapper;
            _incomeOtherService = incomeOtherService;
            _incomeTotalSaleMapper = incomeTotalSaleMapper;
            _incomeTotalSaleService = incomeTotalSaleService;
            _estimateTotalSaleMapper = estimateTotalSaleMapper;
            _estimateTotalSaleService = estimateTotalSaleService;
            _approvalMapper = approvalMapper;
            _approvalService = approvalService;
            _sharedBranchService = sharedBranchService;
            _sharedTemplateService = sharedTemplateService;
            _proposalLineTemplateMapper = proposalLineTemplateMapper;
            _proposalLineApService = proposalLineApService;
            _sharedTemplateMapper = sharedTemplateMapper;
            _numberSequenceService = numberSequenceService1;
            _incomeTotalSaleTemplateMapper = incomeTotalSaleTemplateMapper;
            _incomeTotalSaleTemplateService = incomeTotalSaleTemplateService;
            _budgetYearTransService = budgetYearTransService;
            _lastYearService = lastYearService;
            _yearPlanService = yearPlanService;
            _accountTransService = accountTransService;
            _unitCodeForEmployeeService = unitCodeForEmployeeService;
            _budgetYearTransFacade = budgetYearTransFacade;
            _memoService = memoService;
            _pettyCashService = pettyCashService;
            _advanceService = advanceService;
            _clearingService = clearingService;
            _employeeTableService = employeeTableService;
            _budgetProposalTransService = budgetProposalTransService;
            _budgetDepositTransFacade = budgetDepositTransFacade;
            _budgetDepositTransService = budgetDepositTransService;
            _proposalTrackingViewService = proposalTrackingViewService;
            _logProposalService = logProposalService;
            _proposalBudgetPlanService = proposalBudgetPlanService;
            _budgetPlanMapper = budgetPlanMapper;
            _mProcurement = mProcurement;
            _commentService = commentService;
            _allocationBasisService = allocationBasisService;
            _themeFacade = themeFacade;
            _themeService = themeService;
        }

        public override async Task<ProposalViewModel> GetViewModel(UrlHelper url, int id, string sn = null)
        {
            var model = await _workflowTableService.GetAsync(id);
            var viewModel = await this.InitWorkflowViewModel(url, id, sn, _mapper.ToViewModel(model));
            var templates = GetViewModel().OrderBy(m => m.Order);

            if (viewModel != null)
            {
                if (viewModel.ProposalLine.Any())
                {
                    viewModel.SharedBranchTemplateLines = _sharedTemplateMapper.ToListViewModel(model.TemplateLines.ToList(), _sharedBranchService.GetAll().ToViewModels().ToList());
                    var tempProposalLines = new List<ProposalLineViewModel>();

                    foreach (var line in viewModel.ProposalLine)
                    {
                        line.PercentShared = InitPercenSharedBranchforPropLine(viewModel.SharedBranchTemplateLines, line.ProposalLineTemplate);
                        tempProposalLines.Add(line);
                    }
                    viewModel.ProposalLine = tempProposalLines;
                }

                if (viewModel.IncomeTotalSale.Any())
                {
                    var tempIncomeTotalSale = new List<IncomeTotalSaleViewModel>();
                    foreach (var sale in viewModel.IncomeTotalSale)
                    {
                        sale.PercentShared = InitPercenSharedBranchforTotalSale(viewModel.SharedBranchTemplateLines, sale.IncomeTotalSaleTemplate);
                        tempIncomeTotalSale.Add(sale);
                    }
                    viewModel.IncomeTotalSale = tempIncomeTotalSale;
                }
                viewModel.SharedBranchTemplate = templates;
            }
            return viewModel;
        }

        public IEnumerable<SharedBranchViewModel> GetViewModel()
        {
            var listViewModel = new List<SharedBranchViewModel>();
            // var model = _sharedBranchService.GetWhere(m => !m.Inactive);
            var model = _sharedBranchService.GetAll();
            var totalsaltmodel = _lastYearService.GetWhere(m => m.Year.Equals((DateTime.Now.Year - 1).ToString()));
            var result = (from shb in model
                          join totalsal in totalsaltmodel on shb.BranchCode equals totalsal.BranchCode into ts
                          from totalsal in ts.DefaultIfEmpty()
                          select
                            new
                            {
                                shb,
                                totalsal = (totalsal ?? new MPS_M_TotalSaleLastYear())
                            }).ToListSafe();

            if (!result.Any()) return listViewModel;
            listViewModel.AddRange(
                result.Select(data => data.shb.ToViewModelPercent(data.totalsal)));

            return listViewModel;
        }

        #region SaveAsync
        public override async Task<ProposalViewModel> SaveAsync(ProposalViewModel viewModel, string action = null)
        {
            if (viewModel == null)
                throw new ArgumentNullException("viewModel");

            if (viewModel.UnitCode == null )
            {
                throw new Exception("Field UnitCode is required !");
            }
               
            //theme
            #region validate theme //panawat.cho
            if (viewModel.Theme != null)
            {
                if (viewModel.Theme.ThemesCode == "Other")
                {
                    if (!string.IsNullOrEmpty(viewModel.ThemeNewName))
                    {
                        var checkThemeDuplicate = _themeService.Find(m => m.ThemesCode == viewModel.ThemeNewName);
                        if (checkThemeDuplicate == null)
                        {
                            viewModel.Theme.ThemesCode = viewModel.ThemeNewName;
                            viewModel.Theme.ThemesName = viewModel.ThemeNewName;
                            viewModel.Theme.Order = 1;
                            var themeNew = _themeService.Insert(viewModel.Theme.ToModel());
                            viewModel.Theme.ThemesCode = themeNew.ThemesCode;
                        }
                        else
                        {
                            throw new Exception("Theme name duplicate!!!");
                        }
                    }
                    else
                    {
                        throw new Exception("Please enter a theme!!!");
                    }
                }
            }
            else
            {
                viewModel.Theme = new ThemeViewModel()
                {
                    ThemesCode = "General",
                    ThemesName = "General"
                };    
            }
            #endregion

            if (viewModel.ProposalLine.Any(m => (m.UnitCode != null && string.IsNullOrEmpty(m.UnitCode.UnitCode)) && !m.Deleted)
                || viewModel.ProposalLine.Any(m => (m.UnitCode != null && m.UnitCode.UnitName == "") && !m.Deleted)
                || viewModel.ProposalLine.Any(m=>m.UnitCode == null && !m.Deleted))
            {
                throw new Exception("Please specify a 'unit code Table A&P' in every fields that were required!!!");
            }

            if (viewModel.ProposalLine.Any(m => string.IsNullOrEmpty(m.Description) && !m.Deleted ) || viewModel.ProposalLine.Any(r => string.IsNullOrWhiteSpace(r.Description) && !r.Deleted))
            {
                throw new Exception("Please specify a 'Description Table A&P' in every fields that were required!!!");
            }

            //if (string.IsNullOrEmpty(action) || action.Contains("Submit"))
            //{
            //    if (viewModel.ProposalType.TypeProposalName.Equals("Deposit"))
            //    {
            //        var permissionProp = _yearPlanService.GetWhere(m => m.Year == viewModel.StartDate.Year.ToString()
            //                                                            && m.UnitCode == viewModel.UnitCode.UnitCode &&
            //                                                            m.TypeYearPlan ==
            //                                                            viewModel.ProposalType.TypeYearPlan);
            //        if (!permissionProp.Any())
            //        {
            //            throw new Exception("UnitCode " + viewModel.UnitCode.UnitCode +
            //                                " ไม่สามารถเปิด Proposal Deposit ได้");
            //        }
            //    }
            //}

            var model = _mapper.ToModel(viewModel);

            try
            {
                if (!viewModel.ProposalType.IsTypeDeposit())
                {
                    var proposalLines = _lineMapper.ToModels(viewModel.ProposalLine, model.ProposalLine).ToListSafe();
                    if (proposalLines.Any())
                    {
                        var budget = proposalLines.Where(m => m.CheckBudget && !m.Deleted).Sum(r => r.BudgetPlan);
                        model.Budget = budget;
                    }
                }
                else
                {
                    model.Budget = viewModel.TotalBudget;
                }

                if (viewModel.Id <= 0)
                {
                    model = _workflowTableService.Insert(model);
                }
                else
                {
                    model = _workflowTableService.Update(model);
                }

                if (!viewModel.ProposalType.IsTypeDeposit())
                {
                    //saveTemplateSharedBranch
                    #region saveTemplateSharedBranch
                    foreach (var template in viewModel.SharedBranchTemplate)
                    {
                        if (viewModel.SharedBranchTemplateLines.Count()==0 || model.TemplateLines.LastOrDefault(m => m.BranchCode == template.BranchCode) == null)
                        {
                            var templateLine = new MPS_ProposalLineSharedTemplate()
                            {
                                BranchCode = template.BranchCode,
                                Selected = template.Selected
                            };
                            if (model.Id <= 0)
                            {
                                templateLine.ProposalTable = model;
                            }
                            else
                            {
                                templateLine.ParentId = model.Id;
                            }
                            _sharedTemplateService.Insert(templateLine);
                        }
                        else
                        {
                            var templateLineViewModel = viewModel.SharedBranchTemplateLines.Where(m => m.BranchCode == template.BranchCode);
                            var templateLine = _sharedTemplateMapper.ToModel(templateLineViewModel.FirstOrDefault());
                            templateLine.Selected = template.Selected;
                            _sharedTemplateService.Update(templateLine, templateLine.Id);
                        }
                    }
                    #endregion

                    //propoline
                    #region propoline
                    var proposalLines = _lineMapper.ToModels(viewModel.ProposalLine, model.ProposalLine).ToListSafe();
                    foreach (var line in proposalLines)
                    {
                        var lineViewModel = viewModel.ProposalLine.LastOrDefault(m => m.LineNo == line.LineNo);
                        var temp = model.TemplateLines.Where(r => r.Selected).Select(r => r.BranchCode);
                        var percent = lineViewModel.PercentShared.Values.Where(m => temp.Contains(m.BranchCode));

                        if (line.Id <= 0 || line.ParentId <= 0)
                        {
                            if (!line.Deleted)
                            {
                                if (model.Id <= 0)
                                {
                                    line.ProposalTable = model;
                                }
                                else
                                {
                                    line.ParentId = model.Id;
                                }
                                var lineModel = _lineService.Insert(line);

                                if (lineViewModel != null)
                                {
                                    var lineAPs = _proposalLineTemplateMapper.ToProposalLineTemplateModel(percent, _proposalLineTemplateMapper.ToModels(lineViewModel.ProposalLineTemplate));
                                    foreach (var lineap in lineAPs)
                                    {
                                        if (lineap.Id <= 0 || lineap.ProposalLineId <= 0)
                                        {
                                            if (lineModel.Id <= 0)
                                            {
                                                lineap.ProposalLine = lineModel;
                                            }
                                            else
                                            {
                                                lineap.ProposalLineId = lineModel.Id;
                                            }
                                            _proposalLineApService.Insert(lineap);
                                        }
                                        else
                                        {
                                            _proposalLineApService.Update(lineap, lineap.Id);
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (line.Deleted)
                            {
                                _lineService.Delete(line);
                            }
                            else
                            {
                                var lineModel = _lineService.Update(line, line.Id);
                                if (lineViewModel != null)
                                {
                                    var lineAPs = _proposalLineTemplateMapper.ToProposalLineTemplateModel(percent, _proposalLineTemplateMapper.ToModels(lineViewModel.ProposalLineTemplate));
                                    foreach (var lineap in lineAPs)
                                    {
                                        if (lineap.Id <= 0 || lineap.ProposalLineId <= 0)
                                        {
                                            if (lineModel.Id <= 0)
                                            {
                                                lineap.ProposalLine = lineModel;
                                            }
                                            else
                                            {
                                                lineap.ProposalLineId = lineModel.Id;
                                            }
                                            _proposalLineApService.Insert(lineap);
                                        }
                                        else
                                        {
                                            _proposalLineApService.Update(lineap, lineap.Id);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    #endregion

                    var proposalBudget = _lineMapper.GroupProposalLine(viewModel.ProposalLine);
                    foreach (var groupLine in proposalBudget)
                    {
                        var propbudget = _proposalBudgetPlanService.Find(m => m.ProposalId == groupLine.ParentId && m.UnitCode == groupLine.UnitCode 
                                                                            && m.APCode == groupLine.APCode);
                        var newModel = _budgetPlanMapper.ToModel(groupLine);
                       
                        if ( groupLine.ParentId <= 0 || propbudget == null)
                        {
                            if (!groupLine.Deleted)
                            {
                                if (model.Id <= 0)
                                {
                                    newModel.ProposalTable = model;
                                }
                                else
                                {
                                    newModel.ProposalId = model.Id;
                                }
                                _proposalBudgetPlanService.Insert(newModel);
                            }
                        }
                        else
                        {
                            if (groupLine.Deleted)
                            {
                                _proposalBudgetPlanService.Delete(newModel);
                            }
                            else
                            {
                              newModel.ProposalId = model.Id;
                             _proposalBudgetPlanService.Update(newModel);

                            }
                        }
                    }

                    var incomeDeposit = _incomeDepositMapper.ToModels(viewModel.IncomeDeposit, model.IncomeDeposit).ToListSafe();
                    foreach (var incDeposit in incomeDeposit)
                    {
                        if (incDeposit.Id <= 0 || incDeposit.ParentId <= 0)
                        {
                            if (!incDeposit.Deleted)
                            {
                                if (model.Id <= 0)
                                {
                                    incDeposit.ProposalTable = model;
                                }
                                else
                                {
                                    incDeposit.ParentId = model.Id;
                                }
                                _incomeDepositService.Insert(incDeposit);
                            }
                        }
                        else
                        {
                            if (incDeposit.Deleted)
                            {
                                _incomeDepositService.Delete(incDeposit);
                            }
                            else
                            {
                                _incomeDepositService.Update(incDeposit, incDeposit.Id);
                            }
                        }
                    }

                    var incomeOther = _incomeOtherMapper.ToModels(viewModel.IncomeOther, model.IncomeOther);
                    var lineModels = model.ProposalLine;
                    foreach (var incOther in incomeOther)
                    {
                        var refLineNo = viewModel.IncomeOther.LastOrDefault(m => m.LineNo == incOther.LineNo).RefLineNo;
                        var line = lineModels.LastOrDefault(m => m.LineNo == refLineNo);
                        if (incOther.Id <= 0 || incOther.ParentId <= 0)
                        {
                            if (!incOther.Deleted)
                            {
                                if (model.Id <= 0)
                                {
                                    incOther.ProposalTable = model;
                                }
                                else
                                {
                                    incOther.ParentId = model.Id;
                                }
                                if (incOther.SponcorName == Budget.APCode.DO3)
                                {
                                    if (line != null && (line.Id <= 0))
                                    {
                                        incOther.ProposalLine = line;
                                    }
                                    else
                                    {
                                        incOther.ProposalLineId = line.Id;
                                    }
                                }
                                var incomeOtherModel = _incomeOtherService.Insert(incOther);
                            }
                        }
                        else
                        {
                            if (incOther.Deleted)
                            {
                                _incomeOtherService.Delete(incOther);
                            }
                            else
                            {
                                var incomeOtherModel = _incomeOtherService.Update(incOther, incOther.Id);
                                var accountView = (from ic in viewModel.IncomeOther.Where(m => m.Id == incOther.Id)
                                                   from ac in ic.AccountTrans
                                                   select ac); //viewModel.IncomeOther.Select(m => m.AccountTrans);
                                if (accountView != null)
                                {
                                    //var accountTrans = _incomeOtherMapper.ToAccountTransModels(accountView); // mapper model
                                    foreach (var accountTrans in accountView)
                                    {
                                        var acc = _incomeOtherMapper.ToAccountTransModel(accountTrans);
                                        if (acc.Id <= 0 || acc.IncomeOtherId <= 0)
                                        {
                                            if (incomeOtherModel.Id <= 0)
                                            {
                                                acc.IncomeOther = incomeOtherModel;
                                            }
                                            else
                                            {
                                                acc.IncomeOtherId = incomeOtherModel.Id;
                                            }
                                            _accountTransService.Insert(acc);
                                        }
                                        else
                                        {
                                            if (accountTrans.Deleted)
                                            {
                                                _accountTransService.Delete(acc.Id);
                                            }
                                            else
                                            {
                                                _accountTransService.Update(acc, acc.Id);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    // Total Sales
                    #region Total Sales
                    var incomeTotalSale = _incomeTotalSaleMapper.ToModels(viewModel.IncomeTotalSale, model.IncomeTotalSale).ToListSafe();
                    foreach (var incTotalSale in incomeTotalSale)
                    {
                        var incomeTotalSaleViewModel = viewModel.IncomeTotalSale.LastOrDefault(m => m.LineNo == incTotalSale.LineNo);
                        var temp = model.TemplateLines.Where(r => r.Selected).Select(r => r.BranchCode);
                        var percent =
                          incomeTotalSaleViewModel.PercentShared.Values.Where(m => temp.Contains(m.BranchCode));

                        if (incTotalSale.Id <= 0 || incTotalSale.ParentId <= 0)
                        {
                            if (model.Id <= 0)
                            {
                                incTotalSale.ProposalTable = model;
                            }
                            else
                            {
                                incTotalSale.ParentId = model.Id;
                            }
                            var incomeTotalSaleModel = _incomeTotalSaleService.Insert(incTotalSale);
                            if (incomeTotalSaleViewModel != null)
                            {
                                var saleTemplate = _incomeTotalSaleTemplateMapper.ToIncomeTotalSaleTemplateModel(percent, _incomeTotalSaleTemplateMapper.ToModels(incomeTotalSaleViewModel.IncomeTotalSaleTemplate));
                                foreach (var sale in saleTemplate)
                                {
                                    if (sale.Id <= 0 || sale.IncomeTotalSaleId <= 0)
                                    {
                                        if (incomeTotalSaleModel.Id <= 0)
                                        {
                                            sale.IncomeTotalSale = incomeTotalSaleModel;
                                        }
                                        else
                                        {
                                            sale.IncomeTotalSaleId = incomeTotalSaleModel.Id;
                                        }
                                        _incomeTotalSaleTemplateService.Insert(sale);
                                    }
                                    else
                                    {
                                        _incomeTotalSaleTemplateService.Update(sale, sale.Id);
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (incTotalSale.Deleted)
                            {
                                _incomeTotalSaleService.Delete(incTotalSale);
                            }
                            else
                            {
                                var incomeTotalSaleModel = _incomeTotalSaleService.Update(incTotalSale, incTotalSale.Id);
                                if (incomeTotalSaleViewModel != null)
                                {
                                    var saleTemplate = _incomeTotalSaleTemplateMapper.ToIncomeTotalSaleTemplateModel(percent, _incomeTotalSaleTemplateMapper.ToModels(incomeTotalSaleViewModel.IncomeTotalSaleTemplate));
                                    foreach (var sale in saleTemplate)
                                    {
                                        if (sale.Id <= 0 || sale.IncomeTotalSaleId <= 0)
                                        {
                                            if (incomeTotalSaleModel.Id <= 0)
                                            {
                                                sale.IncomeTotalSale = incomeTotalSaleModel;
                                            }
                                            else
                                            {
                                                sale.IncomeTotalSaleId = incomeTotalSaleModel.Id;
                                            }
                                            _incomeTotalSaleTemplateService.Insert(sale);
                                        }
                                        else
                                        {
                                            _incomeTotalSaleTemplateService.Update(sale, sale.Id);
                                        }
                                    }
                                }

                            }
                        }
                    }
                    #endregion

                    #region estimate
                    //var estimateTotalSale = _estimateTotalSaleMapper.ToModels(viewModel.EstimateTotalSale, model.EstimateTotalSale).ToListSafe();
                    //foreach (var estimate in estimateTotalSale)
                    //{
                    //    if (estimate.Id <= 0 || estimate.ParentId <= 0)
                    //    {
                    //        if (model.Id <= 0)
                    //        {
                    //            estimate.ProposalTable = model;
                    //        }
                    //        else
                    //        {
                    //            estimate.ParentId = model.Id;
                    //        }
                    //        _estimateTotalSaleService.Insert(estimate);
                    //    }
                    //    else
                    //    {
                    //        if (estimate.Deleted)
                    //        {
                    //            _estimateTotalSaleService.Delete(estimate);
                    //        }
                    //        else
                    //        {
                    //            _estimateTotalSaleService.Update(estimate, estimate.Id);
                    //        }
                    //    }
                    //}
                    #endregion
                }
                else
                {
                    // Deposit
                    #region Deposit        
                    var depositlLines = _depositMapper.ToModels(viewModel.DepositLines, model.DepositLine).ToListSafe();
                    foreach (var deposit in depositlLines)
                    {
                        if (deposit.Id <= 0 || deposit.ParentId <= 0)
                        {
                            if (model.Id <= 0)
                            {
                                deposit.ProposalTable = model;
                            }
                            else
                            {
                                deposit.ParentId = model.Id;
                            }
                            _depositService.Insert(deposit);
                        }
                        else
                        {
                            if (deposit.Deleted)
                            {
                                _depositService.Delete(deposit);
                            }
                            else
                            {
                                _depositService.Update(deposit, deposit.Id);
                            }
                        }
                    }
                }
                #endregion

                // apprpves
                #region apprpve
                var approval = _approvalMapper.ToModels(viewModel.ProposalApproval, model.ProposalApproval).ToListSafe();
                foreach (var approve in approval)
                {
                    if (approve.Id <= 0 || approve.ParentId <= 0)
                    {
                        if (!approve.Deleted)
                        {
                            if (model.Id <= 0)
                            {
                                approve.ProposalTable = model;
                            }
                            else
                            {
                                approve.ParentId = model.Id;
                            }
                            _approvalService.Insert(approve);
                        }
                    }
                    else
                    {
                        if (approve.Deleted)
                        {
                            _approvalService.Delete(approve);
                        }
                        else
                        {
                            _approvalService.Update(approve, approve.Id);
                        }
                    }
                }
                #endregion

                await Session.SaveChangesAsync();
                return _mapper.ToViewModel(model);
                //return await GetViewModel(url,model.Id);
            }
            catch (Exception ex)
            {
                throw new HttpException(ex.Message);
            }
        }
        #endregion

        public override async Task<bool> UpdateAfterStartProcess(MPS_ProposalTable workflowTable, int? procInstId)
        {
            var result = await base.UpdateAfterStartProcess(workflowTable, procInstId);

            if (workflowTable.Revision != 0 && workflowTable.Revision != null)
            {
            }
            else
            {
                if (result)
                {
                    _budgetYearTransFacade.CreateTrans(workflowTable.Id);
                    if (workflowTable.ProposalTypeCode == ProposalType.ProposalTypeCode.DepositInternal)
                    {
                        _budgetDepositTransFacade.CreateDepositTransStepCreateDeposit(workflowTable.Id);
                    }
                    if (!workflowTable.TypeProposal.ToViewModel().IsTypeDeposit()
                        && workflowTable.IncomeDeposit.Any(m => !m.Deleted))
                    {
                        if (workflowTable.IncomeDeposit != null)
                        {
                            foreach (var income in workflowTable.IncomeDeposit)
                            {
                                _budgetDepositTransFacade.CreateDepositTransStepCreateIncome(income);
                            }
                        }
                    }
                }
                return result;
            }
            return result;
        }

        public override IEnumerable<ValidationResult> ValidateStartProcess(MPS_ProposalTable workflowTable)
        {
            if (workflowTable.StatusFlag == 8) // revise complete
            {
                //Validation for revise proposal
                //if (workflowTable.ProposalLine.Any(m => string.IsNullOrEmpty(m.UnitCode) && !m.ExpenseTopicCode.ToLower().Contains("other")))
                //{
                //    yield return new ValidationResult("Please specify a unit code in every fields that were required!!!");
                //}

                if (workflowTable.ProposalLine.Any(m => (string.IsNullOrEmpty(m.UnitCode)) && !m.Deleted) || workflowTable.ProposalLine.Any(m => (m.UnitName == "") 
                    && !m.Deleted))
                {
                    throw new Exception("Please specify a 'unit code Table A&P' in every fields that were required!!!");
                }

                if (workflowTable.TemplateLines.All(m => !m.Selected) || (!workflowTable.RE && !workflowTable.TR))
                {
                    yield return new ValidationResult("Please select Shared Allocation!!!");
                }

                if ((workflowTable.IncomeOther.Any() || workflowTable.ProposalTypeCode == "DEPOSIT")
                    && string.IsNullOrEmpty(workflowTable.AccountingBranch))
                {
                    yield return new ValidationResult("Please select accounting!!!");
                }

                //if (workflowTable.TotalBudget <= 0)
                //{
                //    yield return new ValidationResult("Please specify a budget plan!!!");
                //}

                //Get Last Log
                var logProposalTable = _logProposalService.LastOrDefault(m => m.ProposalId == workflowTable.Id && m.Revision == (workflowTable.Revision - 1));
                if (logProposalTable == null)
                {
                    yield return new ValidationResult("Proposal Log could not be found.");
                }
                else
                {
                    if (logProposalTable.TotalBudget < workflowTable.TotalBudget)
                    {
                        yield return new ValidationResult(string.Format("Cannot adjust total budget more than approved amount : {0}", logProposalTable.TotalBudget));
                    }

                    var proposalLines = workflowTable.ProposalLine.Where(m => m.StatusFlag == DocumentStatusFlagId.Active && !m.Deleted);
                    if (proposalLines != null && proposalLines.Any())
                    {
                        foreach (var proposalLine in proposalLines)
                        {
                            var transTotal = _budgetProposalTransService.GetBudgetTransTotal(proposalLine.Id);
                            if (proposalLine.BudgetPlan < transTotal)
                            {
                                yield return new ValidationResult(string.Format("Cannot adjust budget plan (AP Code: {0},Unit Code: {1}) less than reserved amount : {2}", proposalLine.APCode, proposalLine.UnitCode, transTotal));
                            }
                        }
                    }
                }
            }
            else
            {
                // first validate
                if (workflowTable.ProposalLine.Any(m => (string.IsNullOrEmpty(m.UnitCode)) && !m.Deleted) || workflowTable.ProposalLine.Any(m => (m.UnitName == "") && !m.Deleted))
                {
                    throw new Exception("Please specify a 'unit code Table A&P' in every fields that were required!!!");
                }
               
                //if (workflowTable.ProposalLine.Any(m => string.IsNullOrEmpty(m.UnitCode)  && !m.ExpenseTopicCode.ToLower().Contains("other")))
                //{
                //    yield return new ValidationResult("Please specify a unit code in every fields that were required!!!");
                //}

                if (workflowTable.ProposalLine.Any(m => string.IsNullOrEmpty(m.Description) || workflowTable.ProposalLine.Any(r => string.IsNullOrWhiteSpace(r.Description))))
                {
                    yield return new ValidationResult("Please specify a 'Description' in every fields that were required!!!");
                }

                if (workflowTable.TotalBudget == 0)
                {
                    yield return new ValidationResult("Budget is not 0 baht!!!");
                }

                if (!workflowTable.TypeProposal.IsTypeDeposit())
                {
                    var allocationCode = _allocationBasisService.FindByProposalSharedTemplate(workflowTable.TemplateLines) != null ? _allocationBasisService.FindByProposalSharedTemplate(workflowTable.TemplateLines).AllocCode : null;
                    if (allocationCode.ToLower() == "cxx")
                    {
                        yield return new ValidationResult("การจัดแบ่งค่าโฆษณาส่วนกลางไม่ถูกต้อง!!!");
                    }
                }

                //if (
                //    workflowTable.ProposalLine.Any(
                //        m =>
                //            m.BudgetPlan !=
                //            (_proposalLineApService.GetWhere(r => r.ProposalLineId == m.Id).Sum(r => r.TR_Amount) +
                //             _proposalLineApService.GetWhere(r => r.ProposalLineId == m.Id).Sum(r => r.RE_Amount))))
                //{
                //    yield return new ValidationResult("มี Allocate บางรายการไม่เท่ากับ Budget Plan!!!");
                //}
                //if (workflowTable.TotalBudget <= 0)
                //{
                //    yield return new ValidationResult("Please specify a budget plan!!!");
                //}

                if (workflowTable.IncomeDeposit.Any(m => string.IsNullOrEmpty(m.DepositApprover) && !m.Deleted))
                {
                    yield return new ValidationResult("Please select approver for income from proposal deposit!!!");
                }

                if ((workflowTable.IncomeOther.Any() || workflowTable.ProposalTypeCode == "DEPOSIT")
                    && string.IsNullOrEmpty(workflowTable.AccountingBranch))
                {
                    yield return new ValidationResult("Please select accounting!!!");
                }

                //if (!workflowTable.ProposalApproval.Any(m => m.ApproverLevel.Equals(Approval.ApproveLevel.Propose) && !m.Deleted))
                //{
                //    yield return new ValidationResult("Please select approver level propose!!!");
                //}
                //if (!workflowTable.ProposalApproval.Any(m => m.ApproverLevel.Equals(Approval.ApproveLevel.Accept) && !m.Deleted))
                //{
                //    yield return new ValidationResult("Please select approver level accept!!!");
                //}
                //if (!workflowTable.ProposalApproval.Any(m => m.ApproverLevel.Equals(Approval.ApproveLevel.Approval) && !m.Deleted))
                //{
                //    yield return new ValidationResult("Please select a approver!!!");
                //}

                if (!workflowTable.ProposalApproval.Any(m => m.ApproverLevel.Equals(Approval.ApproveLevel.FinalApproval) && !m.Deleted && m.LockEditable))
                {
                    yield return new ValidationResult("Please select a final apporver!!!");
                }

                if (!string.IsNullOrEmpty(workflowTable.TypeProposal.TypeYearPlan))
                {
                    var budgetTrans = _budgetYearTransService.GetBudgetTransTotal(workflowTable.StartDate.Year.ToString(),
                        workflowTable.UnitCode, workflowTable.TypeProposal.TypeYearPlan);
                    var yearPlan =
                        _yearPlanService.GetWhere(
                            m =>
                                m.Year.Equals(workflowTable.StartDate.Year.ToString()) &&
                                m.UnitCode.Equals(workflowTable.UnitCode) &&
                                m.TypeYearPlan.Equals(workflowTable.TypeProposal.TypeYearPlan)).LastOrDefault();
                    var balance = (yearPlan != null) ? yearPlan.Budget - budgetTrans : 0;

                    if (balance < workflowTable.Budget || balance == 0)
                    {
                        yield return new ValidationResult(String.Format("Budget exceed: Statements remainning {0:n} baht", balance));
                    }
                }

                if (workflowTable.ProposalTypeCode == ProposalType.ProposalTypeCode.DepositInternal)
                {
                    var proposalDeposit = _workflowTableService.Find(m => m.Id == workflowTable.DepositId);
                    var budgetTransTotal = _budgetDepositTransService.GetBudgetDepositTransTotal(workflowTable.DepositId);
                    var balance = (proposalDeposit.TotalBudget - budgetTransTotal);

                    if (balance < workflowTable.TotalBudget ||  balance == 0)
                    {
                        yield return new ValidationResult(String.Format("Budget exceed: Statements remainning {0:n} baht", balance));
                    }
                }
            }
        }

        public override IEnumerable<ValidationResult> ValidateSubmit(MPS_ProposalTable workflowTable, WorklistItem worklistItem, string action,
            ProposalViewModel viewModel)
        {
            if (workflowTable.Revision != 0 && workflowTable.Revision != null) // resubmit proposal complete
            {
                if (action.ToUpper().Equals("RESUBMIT"))
                {                 
                    
                    if (viewModel.ProposalLine.Any(m => string.IsNullOrEmpty(m.UnitCode.UnitCode) && !m.ExpenseTopicCode.ToLower().Contains("other") && !m.Deleted))
                    {
                        yield return new ValidationResult("Please specify a unit code in every fields that were required!!!");
                    }

                    if (viewModel.ProposalLine.Any(m => string.IsNullOrEmpty(m.Description) && !m.Deleted) || viewModel.ProposalLine.Any(r => string.IsNullOrWhiteSpace(r.Description) && !r.Deleted))
                    {
                        yield return new ValidationResult("Please specify a 'Description' in every fields that were required!!!");
                    }

                    if (worklistItem.ProcessName().ToUpper() == "PROPOSAL_DEPOSIT")
                    {
                        if (viewModel.IncomeDeposit.Any() && !viewModel.IncomeDeposit.Any(m => string.IsNullOrEmpty(m.Status) && !m.Deleted))
                        {
                            yield return new ValidationResult("Please input for income from proposal deposit!!!");
                        }
                    }

                    if ((viewModel.IncomeOther.Any() || viewModel.ProposalType.TypeProposalCode == "DEPOSIT") && viewModel.Accounting == null)
                    {
                        yield return new ValidationResult("Please select accounting!!!");
                    }

                    //if (workflowTable.IncomeDeposit.Any() && workflowTable.IncomeDeposit.All(m => !string.IsNullOrEmpty(m.Status) && !m.Deleted))
                    //{
                    //    yield return new ValidationResult("Please input for income from proposal deposit!!!");
                    //}
                }
            }
            else
            {
                if (action.ToUpper().Equals("RESUBMIT")) // resubmit none complete
                {
                    if (viewModel.TotalBudget == 0)
                    {
                        yield return new ValidationResult("Budget is not 0 baht!!!");
                    }

                    if (!workflowTable.TypeProposal.IsTypeDeposit())
                    {
                        var sharedBranchViewmodel = new List<SharedBranchViewModel>();
                        sharedBranchViewmodel = viewModel.SharedBranchTemplate.Where(m => m.Selected).ToList();
                        var allocationCode =  _allocationBasisService.FindAllocationCode(sharedBranchViewmodel, viewModel.TR, viewModel.RE) != null ?  _allocationBasisService.FindAllocationCode(sharedBranchViewmodel, viewModel.TR, viewModel.RE).AllocCode : null;
                        //var allocationCode = _allocationBasisService.FindByProposalSharedTemplate(workflowTable.TemplateLines) != null ? _allocationBasisService.FindByProposalSharedTemplate(workflowTable.TemplateLines).AllocCode : null;
                        if (allocationCode.ToLower() == "cxx")
                        {
                            yield return new ValidationResult("การจัดแบ่งค่าโฆษณาส่วนกลางไม่ถูกต้อง!!!");
                        }
                    }

                    if (viewModel.ProposalLine.Any(m => string.IsNullOrEmpty(m.UnitCode.UnitCode) && !m.ExpenseTopicCode.ToLower().Contains("other") && !m.Deleted))
                    {
                        yield return new ValidationResult("Please specify a unit code in every fields that were required!!!");
                    }

                    if (viewModel.ProposalLine.Any(m => string.IsNullOrEmpty(m.Description) && !m.Deleted) || viewModel.ProposalLine.Any(r => string.IsNullOrWhiteSpace(r.Description) && !r.Deleted))
                    {
                        yield return new ValidationResult("Please specify a 'Description' in every fields that were required!!!");
                    }
                   
                    //if (viewModel.TotalBudget <= 0)
                    //{
                    //    yield return new ValidationResult("Please specify a budget plan");
                    //}

                    if (viewModel.IncomeDeposit.Any(m => !m.Approver.Any() && !m.Deleted))
                    {
                        yield return new ValidationResult("Please select approver for income from proposal deposit!!!");
                    }
                    if (worklistItem.ProcessName().ToUpper() == "PROPOSAL_DEPOSIT")
                    {
                         if (viewModel.IncomeDeposit.Any() && !viewModel.IncomeDeposit.Any(m => string.IsNullOrEmpty(m.Status) && !m.Deleted ))
                        {
                            yield return new ValidationResult("Please input for income from proposal deposit!!!");
                        }
                    }
                
                    if ((viewModel.IncomeOther.Any() || viewModel.ProposalType.TypeProposalCode == "DEPOSIT") && viewModel.Accounting == null)
                    {
                        yield return new ValidationResult("Please select accounting!!!");
                    }

                    //if (!viewModel.ProposalApproval.Any(m => m.ApproverLevel.Equals(Approval.ApproveLevel.Propose) && !m.Deleted))
                    //{
                    //    yield return new ValidationResult("Please select approver level propose!!!");
                    //}

                    //if (!viewModel.ProposalApproval.Any(m => m.ApproverLevel.Equals(Approval.ApproveLevel.Accept) && !m.Deleted))
                    //{
                    //    yield return new ValidationResult("Please select approver level accept!!!");
                    //}

                    //if (!viewModel.ProposalApproval.Any(m => m.ApproverLevel.Equals(Approval.ApproveLevel.Approval) && !m.Deleted))
                    //{
                    //    yield return new ValidationResult("Please select a approver!!!");
                    //}

                    if (!viewModel.ProposalApproval.Any(m => m.ApproverLevel.Equals(Approval.ApproveLevel.FinalApproval) && !m.Deleted && m.LockEditable))
                    {
                        yield return new ValidationResult("Please select a final apporver!!!");
                    }

                    if (!string.IsNullOrEmpty(viewModel.ProposalType.TypeYearPlan))
                    {
                        var budgetTrans = _budgetYearTransService.GetBudgetTransTotal(viewModel.StartDate.Year.ToString(),
                            viewModel.UnitCode.UnitCode, viewModel.ProposalType.TypeYearPlan);
                        var lastBudget = _budgetYearTransService.LastBudgetTrans("Proposal", viewModel.Id);
                        var yearPlan =
                            _yearPlanService.GetWhere(
                                m =>
                                    m.Year.Equals(viewModel.StartDate.Year.ToString()) &&
                                    m.UnitCode.Equals(viewModel.UnitCode.UnitCode) &&
                                    m.TypeYearPlan.Equals(viewModel.ProposalType.TypeYearPlan)).LastOrDefault();
                        var balance = (yearPlan != null) ? yearPlan.Budget - budgetTrans + lastBudget.Amount : 0;

                        if (balance < viewModel.Budget ||  balance == 0)
                        {
                            yield return new ValidationResult(String.Format("Budget exceed: Statements remainning {0:n} baht", balance));
                        }
                    }

                    if (viewModel.ProposalType.TypeProposalCode == ProposalType.ProposalTypeCode.DepositInternal)
                    {
                        var proposalDeposit = _workflowTableService.Find(m => m.Id == viewModel.DepositNumber.Id);
                        var budgetTrans = _budgetDepositTransService.LastOrDefault(m => m.ProposalDepositId == proposalDeposit.Id
                                                                                           &&
                                                                                           m.DocumentType == "DepositInternal"
                                                                                           &&
                                                                                           m.DocumentId == workflowTable.Id
                                                                                           && m.Status == "Reserve"
                                                                                           && !m.InActive);
                        if (budgetTrans == null)
                            throw new Exception(string.Format(Resources.Error.BudgetTransNotFound, proposalDeposit.DocumentNumber));

                        var budgetTransTotal = _budgetDepositTransService.GetBudgetDepositTransTotal(workflowTable.DepositId);
                        var balance = (proposalDeposit.TotalBudget - budgetTransTotal + budgetTrans.Amount);

                        if (balance < viewModel.TotalBudget || balance == 0)
                        {
                            yield return new ValidationResult(String.Format("Budget exceed: Statements remainning {0:n} baht", balance));
                        }
                    }
                }

                if (action.ToUpper().Equals("POSTACTUAL"))
                {
                    if (viewModel.ProposalType.IsTypeDeposit())
                    {
                        var depositline = viewModel.DepositLines.Where(m => !m.Deleted);
                        if (depositline.Any(m => m.ProcessStatus == "waiting" || string.IsNullOrEmpty(m.ProcessStatus)))
                        {
                            yield return new ValidationResult("ยังมีรายการที่อยู่ระหว่างดำเนินการ");
                        }
                    }
                    else
                    {
                        if (viewModel.IncomeOther != null && viewModel.IncomeOther.Any(m => !m.PostActual))
                        {
                            yield return new ValidationResult("กรุณาเลือกรายการที่ยังไม่ได้กด Actual");
                        }
                    }
                }
            }
        }

        public override async Task<MPS_ProposalComment> Submit(string sn, string action, string comment, ProposalViewModel viewModel)
        {
            var result = await base.Submit(sn, action, comment, viewModel);
            if (result != null)
            {
                if (result.MpsProposalTable.Revision != 0 && result.MpsProposalTable.Revision != null)
                {
                    var approval = _approvalService.GetWhere(m => m.ParentId == viewModel.Id && !m.Deleted);
                    foreach (var a in approval)
                    {
                        a.ApproveStatus = false;
                        _approvalService.Update(a, a.Id);
                    }
                    await Session.SaveChangesAsync();
                }
                else
                {
                    if (result.Action.ToUpper().Equals("RESUBMIT", StringComparison.CurrentCultureIgnoreCase)
                    && result.Activity.ToUpper().Equals("REVISE"))
                    {
                        if (result.ProcessName.ToUpper().Equals("PROPOSAL"))
                        {
                            _budgetYearTransFacade.ChangeTrans(result.ParentId);
                            var approval = _approvalService.GetWhere(m => m.ParentId == viewModel.Id && !m.Deleted);

                            foreach (var a in approval)
                            {
                                a.ApproveStatus = false;
                                _approvalService.Update(a, a.Id);
                            }
                            await Session.SaveChangesAsync();

                            if (viewModel.ProposalType.TypeProposalCode == ProposalType.ProposalTypeCode.DepositInternal)
                            {
                                _budgetDepositTransFacade.ReviseProposalDepositTrans(viewModel.Id);
                            }
                            if (viewModel.IncomeDeposit.Any() && !viewModel.ProposalType.IsTypeDeposit())
                            {
                                // เสนอ Revise
                                if (viewModel.IncomeDeposit.Any(m => m.ProcInstId == null))
                                {
                                    var incomeDelete = viewModel.IncomeDeposit.Where(m => m.Deleted && m.ProcInstId == null && string.IsNullOrEmpty(m.Status)).ToListSafe();

                                    foreach (var ind in incomeDelete)
                                    {
                                        var ind1 = ind;
                                        var budgetTransForCancel =
                                            _budgetDepositTransService.LastOrDefault(
                                                m => m.ProposalDepositId == ind1.DocumentNumber.Id
                                                     &&
                                                     m.DocumentType == "IncomeDeposit"
                                                     &&
                                                     m.RefDocumentId == ind1.Id
                                                     &&
                                                     m.DocumentId == ind1.ParentId
                                                     && m.Status == "Reserve"
                                                     && !m.InActive);
                                        if (budgetTransForCancel != null)
                                        {
                                            var model = _incomeDepositService.Find(m => m.Id == ind1.Id) ??
                                                            new MPS_IncomeDeposit()
                                                            {
                                                                ProposalDepositRefID = ind1.DocumentNumber.Id,
                                                                Id = ind1.Id,
                                                                ParentId = ind1.ParentId,
                                                                ProposalTable = _workflowTableService.Find(m => m.Id == ind1.ParentId)
                                                            };
                                            _budgetDepositTransFacade.CancelDepositTransStepCreateIncome(model);
                                        }
                                    }
                                    var listmodel = result.MpsProposalTable.IncomeDeposit.Where(m => !m.Deleted && m.ProcInstId == null && string.IsNullOrEmpty(m.Status)).ToListSafe();
                                    if (listmodel == null)
                                        return null;
                                    foreach (var income in listmodel)
                                    {
                                        var income1 = income;
                                        var budgetTrans =
                                            _budgetDepositTransService.LastOrDefault(
                                                m => m.ProposalDepositId == income1.ProposalDepositRefID
                                                     &&
                                                     m.DocumentType == "IncomeDeposit"
                                                     &&
                                                     m.RefDocumentId == income1.Id
                                                     &&
                                                     m.DocumentId == income1.ParentId
                                                     && m.Status == "Reserve"
                                                     && !m.InActive);
                                        if (budgetTrans == null)
                                        {
                                            _budgetDepositTransFacade.CreateDepositTransStepCreateIncome(income1);
                                        }
                                        else
                                        {
                                            _budgetDepositTransFacade.ReviseDepositTransStepCreateIncome(income1, budgetTrans);
                                        }
                                    }
                                }

                            }
                        }
                        else if (result.ProcessName.ToUpper().Equals("PROPOSAL_DEPOSIT"))
                        {
                            var reviseDepositViewModel = viewModel.IncomeDeposit.Where(m => m.StatusFlag == 0).ToListSafe();

                            if (reviseDepositViewModel != null)
                            {
                                var incomeDelete = reviseDepositViewModel.Where(m => m.Deleted).ToListSafe();
                                foreach (var ind in incomeDelete)
                                {
                                    var ind1 = ind;
                                    var budgetTransForCancel =
                                        _budgetDepositTransService.LastOrDefault(
                                            m => m.ProposalDepositId == ind1.DocumentNumber.Id
                                                 &&
                                                 m.DocumentType == "IncomeDeposit"
                                                 &&
                                                 m.RefDocumentId == ind1.Id
                                                 &&
                                                 m.DocumentId == ind1.ParentId
                                                 && m.Status == "Reserve"
                                                 && !m.InActive);

                                    if (budgetTransForCancel != null)
                                    {
                                        var model = _incomeDepositService.Find(m => m.Id == ind1.Id) ??
                                                        new MPS_IncomeDeposit()
                                                        {
                                                            ProposalDepositRefID = ind1.DocumentNumber.Id,
                                                            Id = ind1.Id,
                                                            ParentId = ind1.ParentId,
                                                            ProposalTable = _workflowTableService.Find(m => m.Id == ind1.ParentId)

                                                        };
                                        _budgetDepositTransFacade.CancelDepositTransStepCreateIncome(model);
                                    }
                                }

                                var listmodel = result.MpsProposalTable.IncomeDeposit.Where(m => !m.Deleted && m.StatusFlag == 0).ToListSafe();
                                if (listmodel == null)
                                    return null;
                                foreach (var income in listmodel)
                                {
                                    var income1 = income;
                                    var budgetTrans =
                                        _budgetDepositTransService.LastOrDefault(
                                            m => m.ProposalDepositId == income1.ProposalDepositRefID
                                                 &&
                                                 m.DocumentType == "IncomeDeposit"
                                                 &&
                                                 m.RefDocumentId == income1.Id
                                                 &&
                                                 m.DocumentId == income1.ParentId
                                                 && m.Status == "Reserve"
                                                 && !m.InActive);
                                    if (budgetTrans == null)
                                    {
                                        _budgetDepositTransFacade.CreateDepositTransStepCreateIncome(income1);
                                    }
                                    else
                                    {
                                        _budgetDepositTransFacade.ReviseDepositTransStepCreateIncome(income1, budgetTrans);
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        var activity = result.Activity.Replace("Approval", "").Replace("-", "").Replace(" ", "");
                        var modelApprove1 = _approvalService.GetWhere(m => activity.Equals(m.ApproverLevel)
                                                                           && m.ParentId == result.ParentId).ToList();
                        var modelApprove =
                            modelApprove1.LastOrDefault(m => m.ApproverUserName.ToLower().Equals(result.CreatedBy.ToLower()));

                        if (modelApprove != null)
                        {
                            modelApprove.ApproveStatus = true;
                            _approvalService.Update(modelApprove, modelApprove.Id);

                            Session.SaveChanges();
                        }

                        if (action.ToUpper().Equals("POSTACTUAL"))
                        {
                            if (viewModel.IncomeOther.Any() && viewModel.ProposalLine.Any(m => m.APCode.APCode == Budget.APCode.DO3))
                            {
                                foreach (var income in viewModel.IncomeOther)
                                {
                                    if (income.SponcorName == Budget.APCode.DO3)
                                    {
                                        var difference = income.Actual - income.Budget;
                                        if (difference > 0)
                                        {
                                            var lineModel = _lineService.LastOrDefault(m=>m.Id == income.ProposalLineId);
                                            lineModel.BudgetPlan = (decimal)difference;
                                            _lineService.Update(lineModel, lineModel.Id);

                                            Session.SaveChanges();
                                        }
                                    }
                                }

                                var proposalLine =
                                    _lineService.GetWhere(
                                        m => m.ParentId == viewModel.Id && m.APCode == Budget.APCode.DO3);
                                var lineViewModel = _lineMapper.ToViewModels(proposalLine);
                                var proposalBudget = _lineMapper.GroupProposalLine(lineViewModel);

                                foreach (var group in proposalBudget)
                                {
                                    var lineBudget = _budgetPlanMapper.ToModel(group);
                                    _proposalBudgetPlanService.Update(lineBudget);

                                    Session.SaveChanges();
                                }
                            }
                        }
                    }
                }
            }
            return result;
        }

        public IEnumerable<SharedBranchViewModel> GetTemplates()
        {
            var models = _sharedBranchService.GetWhere(m => !m.Inactive);
            return models.ToViewModels();
        }

        public Dictionary<string, ProposalPercentSharedViewModel> InitPercenSharedBranchforPropLine(IEnumerable<ProposalLineSharedTemplateViewModel> templates, IEnumerable<ProposalLineTemplateViewModel> lineAp = null)
        {
            var viewModels = new Dictionary<string, ProposalPercentSharedViewModel>();
            foreach (var template in templates)
            {
                var viewModel = new ProposalPercentSharedViewModel();
                viewModel.BranchCode = template.BranchCode;
                viewModel.Selected = template.Selected;

                if (lineAp != null)
                {
                    var proposalLineAp = lineAp.LastOrDefault(m => m.BranchCode == template.BranchCode);
                    if (proposalLineAp != null)
                    {
                        viewModel.lineId = proposalLineAp.Id;
                        viewModel.BranchCode = proposalLineAp.BranchCode;
                        viewModel.TR_Amount = proposalLineAp.TR_Amount;
                        viewModel.TR_Percent = proposalLineAp.TR_Percent;
                        viewModel.RE_Amount = proposalLineAp.RE_Amount;
                        viewModel.RE_Percent = proposalLineAp.RE_Percent;

                    }
                }
                viewModels.Add(template.BranchCode, viewModel);
            }
            return viewModels;
        }

        public Dictionary<string, ProposalPercentSharedViewModel> InitPercenSharedBranchforTotalSale(IEnumerable<ProposalLineSharedTemplateViewModel> templates, IEnumerable<IncomeTotalSaleTemplateViewModel> totalsaleTemplate = null)
        {
            var viewModels = new Dictionary<string, ProposalPercentSharedViewModel>();
            foreach (var template in templates)
            {
                var viewModel = new ProposalPercentSharedViewModel();
                viewModel.BranchCode = template.BranchCode;
                if (totalsaleTemplate != null)
                {
                    var TotalsaleTemplate = totalsaleTemplate.LastOrDefault(m => m.BranchCode == template.BranchCode);
                    if (TotalsaleTemplate != null)
                    {
                        viewModel.lineId = TotalsaleTemplate.Id;
                        viewModel.BranchCode = TotalsaleTemplate.BranchCode;
                        viewModel.Actual = TotalsaleTemplate.Actual;
                        viewModel.ActualAmount = TotalsaleTemplate.ActualAmount;

                    }
                }
                viewModels.Add(template.BranchCode, viewModel);
            }
            return viewModels;
        }

        public List<ProposalInfoViewModel> GetProposalNumber(string code = null, string name = null, string unitCode = "")
        {
            var model = _workflowTableService.SearchDocument(code, name).ToListSafe();
            var newModel = new List<MPS_ProposalTable>();
            var proposalNumberViewModel = new List<ProposalInfoViewModel>();

            if (model != null)
            {
                model = model.Where(m => m.CreatedDate != null && (m.CreatedDate.Value.Year == DateTime.Now.Year || m.StartDate.Year == DateTime.Now.Year)).ToList();
                newModel.AddRange(
                    from modelValue in model
                    let
                        line = modelValue.ProposalLine.Where(m => m.UnitCode == unitCode && !m.Deleted)
                    where line.Any()
                    select modelValue);
                proposalNumberViewModel.AddRange(newModel.Select(p => _mapper.ToProposalInfoViewModel(p)));
            }
            var propInfo = new List<ProposalInfoViewModel>();
            propInfo.AddRange(from view in proposalNumberViewModel
                              let
                              prop = view.ProposalLine.Where(m => m.Remainning > 0 && m.UnitCode.UnitCode == unitCode)
                              where prop.Any()
                              select view);
            return propInfo;
        }

        public IEnumerable<ProposalLineViewModel> GetProposalLineData(string unitCode, int parentId)
        {
            var model = _lineService.GetWhere(m => m.UnitCode.Equals(unitCode) && m.ParentId == parentId);
            var viewModel = _lineMapper.ToViewModelsOnlyExpenseAndAp(model);
            return viewModel;
        }

        public List<DocumentNumberViewModel> GetDepositNumber(string code = null, string name = null)
        {
            var model = _workflowTableService.SearchDocument(code, name).ToListSafe();
            var documentNumberViewModel = new List<DocumentNumberViewModel>();

            if (model != null)
            {
                model = model.Where(m => m.CreatedDate != null &&
                    ((m.CreatedDate.Value.Year == DateTime.Now.Year  || m.StartDate.Year == DateTime.Now.Year) &&
                        (m.ProposalTypeCode.ToUpper() == ProposalType.ProposalTypeCode.Deposit ||
                        m.ProposalTypeCode.ToUpper() == ProposalType.ProposalTypeCode.DepositInternal)
                    )).ToList();
                documentNumberViewModel.AddRange(model.Select(p => _mapper.ToDocNumberViewModel(p)));
            }
            return documentNumberViewModel.Where(m => m.Balance != null && (m.Balance.Value > 0)).ToListSafe(); ;
        }
       
        public List<DepositNumberViewModel> GetIncomeDepositNumber(string code = null, string name = null)
        {
            var model = _workflowTableService.SearchDocument(code, name).ToListSafe();
            var depositNumberViewModel = new List<DepositNumberViewModel>();

            if (model != null)
            {
                model = model.Where(m => m.CreatedDate != null && ((m.CreatedDate.Value.Year == DateTime.Now.Year|| m.StartDate.Year == DateTime.Now.Year) &&
                     (m.ProposalTypeCode.ToUpper() == ProposalType.ProposalTypeCode.Deposit ||
                        m.ProposalTypeCode.ToUpper() == ProposalType.ProposalTypeCode.DepositInternal)
                    )).ToList();
                depositNumberViewModel.AddRange(model.Select(p => _mapper.ToDepositNumberViewModel(p)));
            }

            return depositNumberViewModel.Where(m => m.Balance != null && (m.Balance.Value > 0)).ToListSafe();
        }

        public ProposalInfoViewModel GetProposalOneLineFromId(int id)
        {
            var linemodel = _lineService.FirstOrDefault(m => m.Id == id);
            //var lineViewModel = _lineMapper.ToViewModel(linemodel);

            var model = new MPS_ProposalTable();

            if (linemodel != null)
            {
                model = linemodel.ProposalTable;
            }

            var listviewModel = new List<ProposalLineViewModel>();
            listviewModel.Add(_lineMapper.ToViewModel(linemodel));

            var viewModel = _mapper.ToProposalInfoViewModel(model);
            viewModel.ProposalLine = listviewModel;

            return viewModel;
        }

        public override string ManualDocumentNumber(MPS_ProposalTable workflowTable)
        {
            string documentNumber = workflowTable.DocumentNumber;
            string processCode = _numberSequenceService.GetProcessCode(workflowTable.ProposalTypeCode + workflowTable.StartDate.Year);

            if (string.IsNullOrEmpty(documentNumber))
            {
                var nextdocumentNumber = _numberSequenceService.GetNextNumber(processCode,
                    workflowTable.StartDate, "", "", "");
                return nextdocumentNumber;
            }
            else
            {
                return documentNumber;
            }
        }

        #region Closproposal
        public ReturnMessage CloseProposal(int propId, string username)
        {
            var returnStatus = new ReturnMessage();
            var result = false;
            var message = string.Empty;
            var employeeViewModel = _employeeTableService.FindByUsername(username).ToViewModel();
            var model = _workflowTableService.Get(propId);
            var checkAccount = false;
            checkAccount = !model.IncomeOther.Any() || model.IncomeOther.All(m => m.PostActual);
            var checkMemo = _memoService.CheckMemoSuccess(propId);
            var checkPettyCash = _pettyCashService.CheckPettyCashSuccess(propId);
            var checkadvance = CheckCashAdvanceSuccess(propId);
            var checkMpro = _mProcurement.CheckProcurement(model.DocumentNumber);
            if (checkMemo && checkPettyCash && checkAccount && checkadvance && checkMpro)
            {

                model.CloseFlag = true;
                model.Status = DocumentStatusFlag.Close.ToString();
                model.Status = DocumentStatusFlag.Close.ToString();
                model.CloseDate = DateTime.Now; //
                model.ClosedBy = employeeViewModel.Username; // 
                model.CloseByName = employeeViewModel.FullName; //
                _commentService.CreateCommentSubmit(propId, model.ProcInstId, "NULL", "Close Proposal", "NULL", "Closed", ""); //
                _workflowTableService.Update(model, model.Id);
                _budgetYearTransFacade.ReturnTrans(propId);  //ไปเชคส่วนที่เป็น   Change Actual
                _budgetYearTransFacade.IncomeTrans(propId);   //ไปเชคส่วนที่เป็น   Change Actual
             //   _budgetDepositTransFacade.ReturnDepositTransStepCloseProposal(propId); เปิดใช้เมื่อเอา Change Actual ขึ้น
                result = true;
                Session.SaveChanges();
                message = string.Format("ปิด Proposal :{0} เรียบร้อยแล้ว", model.DocumentNumber);
            }
            else
            {
                result = false;
                message = string.Format("ยังไม่สามารถปิด Proposal :{0} ได้ ยังมีบางรายการกำลังดำเนินงานอยู่", model.DocumentNumber);
                if (!checkMemo)
                {
                    message = message + Environment.NewLine + "Memo บางรายการยังไม่เสร็จสมบูรณ์";
                }
                if (!checkPettyCash)
                {
                    message = message + Environment.NewLine + "Petty Cash บางรายการยังไม่เสร็จสมบูรณ์";
                }
                if (!checkadvance)
                {
                    message = message + Environment.NewLine + "Cash Advance บางรายการยังไม่เสร็จสมบูรณ์";
                }
                if (!checkMpro)
                {
                    message = message + Environment.NewLine  + "PR PO บางรายการยังไม่เสร็จสมบูรณ์";
                }
                if (!checkAccount)
                {
                    message = message + Environment.NewLine + "ฝ่ายบัญชียังไม่ได้ทำการ Post Actual";
                }

            }

            returnStatus.Status = result;
            returnStatus.Message = message;

            return returnStatus;
        }
        #endregion

        #region CheckCashAdvanceSuccess
        public bool CheckCashAdvanceSuccess(int parentId)
        {
            var advance = _advanceService.GetWhere(m => m.ProposalRefID == parentId && !m.Deleted && m.StatusFlag != 5 && m.StatusFlag != 0 && m.Status.ToLower() != DocumentStatusFlag.Cancelled.ToString().ToLower()).ToListSafe();
            if (advance == null || advance.Count == 0)
                return true;
            if (advance.Count > 0)
            {
                if (advance.Count > 0 && advance.All(m => m.Status == DocumentStatus.Completed.ToString()))
                {
                    var listAdvanceId = advance.Select(m => m.Id);
                    var clearing = _clearingService.GetWhere(m => listAdvanceId.Contains(m.CashAdvanceID) &&   m.Status.ToLower() != DocumentStatusFlag.Cancelled.ToString().ToLower()).ToListSafe();
                    if (clearing == null || clearing.Count == 0)
                        return false;
                    if (clearing.Count > 0 && clearing.Count == listAdvanceId.Count() && clearing.All(m => m.Status == DocumentStatus.Completed.ToString()))
                    {
                        return true;
                    }
                }
                
            }

            return false;

        }
        #endregion

        #region SetFinalApproverDeleted
        public void SetFinalApproverDeleted(int id)
        {
            try
            {
                var finalApprovers =
                    _approvalService.GetWhere(m => m.ParentId == id && m.ApproverLevel == "อนุมัติขั้นสุดท้าย");
                foreach (var finalApprover in finalApprovers)
                {
                    _approvalService.Delete(finalApprover);
                }
                Session.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new HttpException(ex.Message);
            }
        }
        #endregion

        #region GetmailInform
        public string GetEmailInformAll(int workflowTableId)
        {
            string email = string.Empty;
            var proposalModel = _workflowTableService.Find(m => m.Id == workflowTableId);
            if (proposalModel != null)
            {

                email += string.IsNullOrEmpty(email) ? proposalModel.CCEmail : string.Format(";{0}", proposalModel.CCEmail).TrimEnd(';');
                email += string.IsNullOrEmpty(email) ? proposalModel.InformEmail : string.Format(";{0}", proposalModel.InformEmail).TrimEnd(';');
                email += string.IsNullOrEmpty(email) ? proposalModel.AccountingEmail : string.Format(";{0}", proposalModel.AccountingEmail).TrimEnd(';');
                if (proposalModel.IncomeDeposit != null)
                {
                    var incomeDepositApprove = proposalModel.IncomeDeposit.Select(m => m.DepositApprover);
                    foreach (var userDeposit in incomeDepositApprove)
                    {
                        if (!string.IsNullOrEmpty(userDeposit))
                        {
                            string[] Username = userDeposit.TrimEnd(';').Split(';');
                            foreach (var user in Username)
                            {
                                var employee = _employeeTableService.FindByUsername(user);
                                if (employee != null && !string.IsNullOrEmpty(employee.Email))
                                {
                                    email += string.IsNullOrEmpty(email)
                                        ? employee.Email
                                        : string.Format(";{0}", employee.Email).TrimEnd(';');

                                }
                            }
                        }
                    }
                }
                if (proposalModel.ProposalApproval != null)
                {
                    foreach (var userapprove in proposalModel.ProposalApproval)
                    {
                        if (!string.IsNullOrEmpty(userapprove.ApproverEmail))
                        {
                            email += string.IsNullOrEmpty(email)
                                   ? userapprove.ApproverEmail
                                   : string.Format(";{0}", userapprove.ApproverEmail).TrimEnd(';');
                        }
                        else
                        {
                            var employee = _employeeTableService.FindByUsername(userapprove.ApproverUserName);
                            if (employee != null && !string.IsNullOrEmpty(employee.Email))
                            {
                                email += string.IsNullOrEmpty(email)
                                    ? employee.Email
                                    : string.Format(";{0}", employee.Email).TrimEnd(';');

                            }

                        }
                    }
                }
            }
            if (!string.IsNullOrEmpty(email))
            {
                string[] listEmail = email.TrimEnd(';').Split(';');
                string[] newlistEmail = listEmail.Distinct().ToArray();
                email = newlistEmail.Aggregate(string.Empty, (current, mail) => current + (string.IsNullOrEmpty(current) ? mail : string.Format(";{0}", mail)));
            }

            return email.TrimEnd(';');

        }
        #endregion

        public string GetEmailInformForAp(int workflowTableId)
        {
            string email = string.Empty;
            var proposalLine =
                _lineService.GetWhere(m => m.ParentId == workflowTableId && !m.Deleted && m.BudgetPlan != 0);
            if (proposalLine != null)
            {
                var vieModel = _lineMapper.ToViewModels(proposalLine);
                var unitcode = vieModel.Where(r => r.APCode.SendMail).Select(m => m.UnitCode.UnitCode).ToListSafe();
                if (unitcode != null)
                {
                    var employee = _unitCodeForEmployeeService.GetWhere(m => unitcode.Contains(m.UnitCode));
                    if (employee != null)
                    {
                        email = employee.Aggregate(email, (current, user) => current + (string.IsNullOrEmpty(current) ? user.Employee.Email : string.Format(";{0}", user.Employee.Email)));
                    }
                }
            }

            return email.TrimEnd(';');
        }

        public List<EmailForAPCodeViewModel> GetEmailInformForApList(int workflowTableId)
        {
            var listEmail = new List<EmailForAPCodeViewModel>();
            var proposalLine =
              _lineService.GetWhere(m => m.ParentId == workflowTableId && !m.Deleted && m.BudgetPlan != 0);
            if (proposalLine != null)
            {
                var vieModel = _lineMapper.ToViewModels(proposalLine);
                var listprop = vieModel.Where(r => r.APCode.SendMail).ToListSafe();

                foreach (var line in listprop)
                {
                    string username = string.Empty;
                    string email = string.Empty;
                    var data = new EmailForAPCodeViewModel();
                    data.ExpenseTopicCode = line.ExpenseTopicCode;
                    data.ExpenseTopicName = line.ExpenseTopicName;
                    data.APCode = line.APCode.APCode;
                    data.APCode = line.APCode.APCode;
                    data.APName = line.APCode.APName;
                    data.UnitCode = line.UnitCode.UnitCode;
                    data.UnitName = line.UnitCode.UnitName;
                    data.BudgetPlan = line.BudgetPlan;
                    var user = _unitCodeForEmployeeService.GetWhere(m => data.UnitCode.Contains(m.UnitCode));
                    foreach (var u in user)
                    {
                        username += (string.IsNullOrEmpty(u.Username))
                            ? u.Username
                            : string.Format(";{0}", u.Username).TrimEnd(';'); ;
                        email += (string.IsNullOrEmpty(u.Employee.Email))
                          ? u.Employee.Email
                          : string.Format(";{0}", u.Employee.Email).TrimEnd(';'); ;
                    }
                    data.UserName = username.TrimStart(';').TrimEnd(';');
                    data.Email = email.TrimStart(';').TrimEnd(';');
                    listEmail.Add(data);
                }
            }

            return listEmail;
        }

        public ProposalViewModel DeleteBy(int id, string username)
        {
            try
            {
                if (id > 0)
                {
                    var model = _workflowTableService.FirstOrDefault(m => m.Id == id);
                    var employeeViewModel = _employeeTableService.FindByUsername(username).ToViewModel();
                    if (model != null && employeeViewModel != null)
                    {
                        var modelDelete = _mapper.DeleteBy(model, employeeViewModel);
                        if (modelDelete != null)
                        {
                            modelDelete = _workflowTableService.Update(modelDelete);
                            var value = Session.SaveChanges();
                            if (value > 0)
                            {
                                var viewModel = _mapper.ToViewModel(modelDelete);
                                return viewModel;
                            }
                        }
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                throw new HttpException(ex.Message);
            }
        }

        public ProposalTrackingViewModel ProposalTrackingViewModel(int proposalId)
        {
            var proposalTracking = new ProposalTrackingViewModel();
            if (proposalId != 0)
            {
                var model = _workflowTableService.Find(m => m.Id == proposalId);
                var tracking = _proposalTrackingViewService.GetWhere(m => m.ProposalId == proposalId).ToList();
                if (model != null && tracking.Any())
                {
                    var lineViewModel = _lineMapper.ToViewModels(model.ProposalLine);
                    proposalTracking.TotalBudget = model.TotalBudget;
                    proposalTracking.Remaining = lineViewModel.Sum(m => m.Remainning);
                    proposalTracking.ProposalTracking = tracking;
                }
            }
            return proposalTracking;
        }

        public bool UpdateReviseCompleted(int workflowTableId)
        {
            var result = _budgetYearTransFacade.ChangeTrans(workflowTableId);
            if (result)
            {
                var proposalTable = _workflowTableService.Find(workflowTableId);
                if (proposalTable == null)
                    throw new Exception(string.Format(Resources.Error.DataWithIDNotExists, workflowTableId));

                proposalTable.StatusFlag = DocumentStatusFlagId.Completed;
                proposalTable.Status = "Completed";
                proposalTable.DocumentStatus = "Completed";

                proposalTable = _workflowTableService.Update(proposalTable, proposalTable.Id);
                Session.SaveChanges();
            }
            return result;
        }      
    }
}