﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TheMall.MPS.API.Mappers;
using TheMall.MPS.API.Services;
using TheMall.MPS.Models.CashClearing;
using TheMall.MPS.Reports.CashClearing;
using TheMall.MPS.Reports.Shared;

namespace TheMall.MPS.API.Facades
{
    public interface IReportCashClearingFacade
    {
        IEnumerable<CashClearingDataSource> ReportData(MPS_CashClearingTable cashClearingModel);
        IEnumerable<ApproverDataSource> ApproversData(IEnumerable<MPS_CashClearingApproval> cashAdvanceApproverModel);
    }
    public class ReportCashClearingFacade: IReportCashClearingFacade
    {
        private readonly IReportCashClearingMapper _reportMapper;
        private readonly ICashClearingApprovalService _cashClearingApproveService;
        private readonly ICashClearingService _cashClearingService;
        private readonly ICashClearingMapper _cashClearingMapper;
        public ReportCashClearingFacade(
            IReportCashClearingMapper reportMapper, 
            ICashClearingApprovalService cashClearingApproveService, 
            ICashClearingService cashClearingService, 
            ICashClearingMapper cashClearingMapper)
        {
            _reportMapper = reportMapper;
            _cashClearingApproveService = cashClearingApproveService;
            _cashClearingService = cashClearingService;
            _cashClearingMapper = cashClearingMapper;
        }

        public IEnumerable<CashClearingDataSource> ReportData(MPS_CashClearingTable cashClearingModel)
        {
            var cashClearingViewModel = _cashClearingMapper.ToViewModel(cashClearingModel);
            var list = _reportMapper.ReportDataMapper(cashClearingViewModel);
            return list;
        }

        public IEnumerable<ApproverDataSource> ApproversData(IEnumerable<MPS_CashClearingApproval> cashAdvanceApproverModel)
        {
            var approversDataList = _reportMapper.ApproversDataMapper(cashAdvanceApproverModel);
            return approversDataList;
        }

    }


}