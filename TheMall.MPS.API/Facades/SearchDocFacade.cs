﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TheMall.MPS.API.Mappers;
using TheMall.MPS.API.Services;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.ViewModels;

namespace TheMall.MPS.API.Facades
{
    public interface ISearchDocFacade
    {
        IEnumerable<SearchDocViewModel> SearchDoc(SearchDocViewModel viewModel);
        //IEnumerable<SearchDocViewModel> SearchDoc(DateTime? startDate,
        //    string process = null, string title = null, string documentNo = null,
        //    string username = null, string status = null);
    }

    public class SearchDocFacade : ISearchDocFacade
    {
        private readonly IProposalService _proposalService;
        private readonly ICashAdvanceService _cashAdvanceService;
        private readonly IPettyCashService _pettyCashService;
        private readonly ICashClearingService _cashClearingService;
        private readonly IMemoService _memoService;
        private readonly IProposalMapper _proposalMapper;
        private readonly ISearchDocMapper _searchDocMapper;
        private readonly IPettyCashMapper _pettyCashMapper;
        private readonly IMPSSession _session;
        private readonly IMemoIncomeService _memoIncomeService;

        public SearchDocFacade(
            IProposalService proposalService, 
            ICashAdvanceService cashAdvanceService, 
            IPettyCashService pettyCashService, 
            ICashClearingService cashClearingService, 
            IMemoService memoService, 
            IProposalMapper proposalMapper, 
            ISearchDocMapper searchDocMapper, 
            IMPSSession session, 
            IPettyCashMapper pettyCashMapper,
            IMemoIncomeService memoIncomeService)
        {
            _proposalService = proposalService;
            _cashAdvanceService = cashAdvanceService;
            _pettyCashService = pettyCashService;
            _cashClearingService = cashClearingService;
            _memoService = memoService;
            _proposalMapper = proposalMapper;
            _searchDocMapper = searchDocMapper;
            _session = session;
            _pettyCashMapper = pettyCashMapper;
            _memoIncomeService = memoIncomeService;
        }

        public IEnumerable<SearchDocViewModel> SearchDoc(SearchDocViewModel viewModel)
        {
            try
            {
                var checkHaveDate = false;
                if (viewModel != null)
                {
                    switch (viewModel.Process)
                    {
                        case "Proposal":
                            if (viewModel.CreateDate != null)
                            {
                                checkHaveDate = true;
                            }
                            var proposalModel = _proposalService.SearchDoc(viewModel.CreateDate, checkHaveDate, viewModel.Process, viewModel.Title,
                                viewModel.DocumentNumber, viewModel.Username, viewModel.Status);
                            var proposalViewModels = _searchDocMapper.ProposalToViewModels(proposalModel);
                            return proposalViewModels;
                            break;
                        case "CashAdvance":
                            if (viewModel.CreateDate != null)
                            {
                                checkHaveDate = true;
                            }
                            var cashAdvanceModel = _cashAdvanceService.SearchDoc(viewModel.CreateDate, checkHaveDate, viewModel.Process, viewModel.Title,
                                viewModel.DocumentNumber, viewModel.Username, viewModel.Status);
                            var cashAdvanceViewModels = _searchDocMapper.CashAdvanceToViewModels(cashAdvanceModel);
                            return cashAdvanceViewModels;
                            break;
                        case "CashClearing":
                            if (viewModel.CreateDate != null)
                            {
                                checkHaveDate = true;
                            }
                            var cashClearingModel = _cashClearingService.SearchDoc(viewModel.CreateDate, checkHaveDate, viewModel.Process, viewModel.Title,
                                viewModel.DocumentNumber, viewModel.Username, viewModel.Status);
                            var cashClearingViewModels = _searchDocMapper.CashClearingToViewModels(cashClearingModel);
                            return cashClearingViewModels;
                            break;
                        case "PettyCash":
                            if (viewModel.CreateDate != null)
                            {
                                checkHaveDate = true;
                            }
                            var pettyCashModel = _pettyCashService.SearchDoc(viewModel.CreateDate, checkHaveDate, viewModel.Process, viewModel.Title,
                                viewModel.DocumentNumber, viewModel.Username, viewModel.Status);
                            var pettyCashViewModels = _searchDocMapper.PettyCashToViewModels(pettyCashModel);
                            return pettyCashViewModels;
                            break;
                        case "Memo":
                            if (viewModel.CreateDate != null)
                            {
                                checkHaveDate = true;
                            }
                            var memoModel = _memoService.SearchDoc(viewModel.CreateDate, checkHaveDate, viewModel.Process, viewModel.Title,
                                viewModel.DocumentNumber, viewModel.Username, viewModel.Status);
                            var memoViewModels = _searchDocMapper.MemoToViewModels(memoModel);
                            return memoViewModels;
                            break;
                        case "MemoIncome":
                            if (viewModel.CreateDate != null)
                            {
                                checkHaveDate = true;
                            }
                            var memoincomeModel = _memoIncomeService.SearchDoc(viewModel.CreateDate, checkHaveDate, viewModel.Process, viewModel.Title,
                                viewModel.DocumentNumber, viewModel.Username, viewModel.Status);
                            var memoIncomeViewModels = _searchDocMapper.MemoIncomeToViewModels(memoincomeModel);
                            return memoIncomeViewModels;
                            break;
                    }
                }
               
                return null;
            }

            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
        }
    }
}