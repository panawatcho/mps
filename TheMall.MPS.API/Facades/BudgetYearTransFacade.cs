﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CrossingSoft.Framework.Infrastructure.Session;
using TheMall.MPS.API.Facades.Abstracts;
using TheMall.MPS.API.Services;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Models.Budget;
using TheMall.MPS.Models.Enums;
using TheMall.MPS.Models.Proposal;
using TheMall.MPS.ViewModels.InterfaceToMprocurement;

namespace TheMall.MPS.API.Facades
{
    public interface IBudgetYearTransFacade
    {
        bool CreateTrans(int proposalId);
        bool CancelTrans(int proposalId);
        bool ChangeTrans(int proposalId);
        bool ReturnTrans(int proposalId);
        bool IncomeTrans(int proposalId);
        bool MemoSpecialReturnTrans(int memoId);
        bool ValidateBudget(MPS_ProposalTable proposal, MPS_BudgetYearTrans lastTrans = null);
    }

    public class BudgetYearTransFacade : BaseFacade, IBudgetYearTransFacade
    {

        private readonly IMPSSession _session;
        private readonly IProposalService _proposalService;
        private readonly IBudgetYearPlanService _budgetYearPlanService;
        private readonly IBudgetYearTransService _budgetYearTransService;
        private readonly IBudgetProposalTransService _budgetProposalTransService;
        private readonly IMemoService _memoService;

        public BudgetYearTransFacade(IMPSSession session,
            IProposalService proposalService,
            IBudgetYearPlanService budgetYearPlanService,
            IBudgetYearTransService budgetYearTransService,
            IBudgetProposalTransService budgetProposalTransService,
            IMemoService memoService)
        {
            _session = session;
            _proposalService = proposalService;
            _budgetYearPlanService = budgetYearPlanService;
            _budgetYearTransService = budgetYearTransService;
            _budgetProposalTransService = budgetProposalTransService;
            _memoService = memoService;
        }

        public bool CreateTrans(int proposalId)
        {
            var result = false;
            var proposal = _proposalService.GetWorkflowTable(proposalId);
            if (proposal == null)
                throw new Exception(string.Format(Resources.Error.DataWithIDNotExists, proposalId));
            if (ValidateBudget(proposal))
            {
                if (proposal.TypeProposal != null && !string.IsNullOrEmpty(proposal.UnitCode))
                {
                    //Type Year Plan == null 
                    if (string.IsNullOrEmpty(proposal.TypeProposal.TypeYearPlan))
                        return true;
                    else
                    {
                        var year = proposal.StartDate.Year.ToString();
                        var unitCode = proposal.UnitCode;
                        var typeYearPlan = proposal.TypeProposal.TypeYearPlan;

                        var budgetYearTrans = new MPS_BudgetYearTrans()
                        {
                            Year = year,
                            UnitCode = unitCode,
                            TypeYearPlan = typeYearPlan,
                            Status = "Reserve",
                            DocumentStatus = "Create",
                            DocumentType = "Proposal",
                            DocumentId = proposal.Id,
                            DocumentNo = proposal.DocumentNumber,
                            Amount = proposal.Budget,
                            TransDate = DateTime.Now
                        };

                        budgetYearTrans = _budgetYearTransService.Insert(budgetYearTrans);
                        _session.SaveChanges();

                        result = true;
                    }
                }
            }
            return result;
        }

        public bool CancelTrans(int proposalId)
        {
            var result = false;
            var proposal = _proposalService.GetWorkflowTable(proposalId);
            if (proposal == null)
                throw new Exception(string.Format(Resources.Error.DataWithIDNotExists, proposalId));

            if (proposal.TypeProposal != null && !string.IsNullOrEmpty(proposal.UnitCode))
            {
                //Type Year Plan == null 
                if (string.IsNullOrEmpty(proposal.TypeProposal.TypeYearPlan))
                    return true;
                else
                {
                    var year = proposal.StartDate.Year.ToString();
                    var unitCode = proposal.UnitCode;
                    var typeYearPlan = proposal.TypeProposal.TypeYearPlan;

                    var budgetTrans = _budgetYearTransService.LastBudgetTrans("Proposal", proposal.Id);
                    if (budgetTrans == null)
                        throw new Exception(string.Format(Resources.Error.BudgetTransNotFound, proposal.DocumentNumber));
                    if (budgetTrans.Status != "Reserve")
                        throw new Exception("Invalid budget status for budget cancellation");
                    var budgetYearTrans = new MPS_BudgetYearTrans()
                    {
                        Year = budgetTrans.Year,
                        UnitCode = budgetTrans.UnitCode,
                        TypeYearPlan = budgetTrans.TypeYearPlan,
                        Status = "Cancel",
                        DocumentStatus = "Cancel",
                        DocumentType = "Proposal",
                        DocumentId = proposal.Id,
                        DocumentNo = proposal.DocumentNumber,
                        Amount = budgetTrans.Amount * (-1),
                        TransDate = DateTime.Now
                    };

                    budgetYearTrans = _budgetYearTransService.Insert(budgetYearTrans);
                    _session.SaveChanges();

                    result = true;
                }
            }

            return result;
        }

        public bool ChangeTrans(int proposalId)
        {
            var result = false;
            var proposal = _proposalService.GetWorkflowTable(proposalId);
            if (proposal == null)
                throw new Exception(string.Format(Resources.Error.DataWithIDNotExists, proposalId));
            if (proposal.TypeProposal != null && !string.IsNullOrEmpty(proposal.UnitCode))
            {
                //Type Year Plan == null 
                if (string.IsNullOrEmpty(proposal.TypeProposal.TypeYearPlan))
                {
                    return true;
                }
                else
                {
                    var year = proposal.StartDate.Year.ToString();
                    var unitCode = proposal.UnitCode;
                    var typeYearPlan = proposal.TypeProposal.TypeYearPlan;

                    var budgetTrans = _budgetYearTransService.LastBudgetTrans("Proposal", proposal.Id);
                    if (budgetTrans == null)
                        throw new Exception(string.Format(Resources.Error.BudgetTransNotFound, proposal.DocumentNumber));
                    if (budgetTrans.Status != "Reserve")
                        throw new Exception("Invalid budget status for budget cancellation");

                    if (ValidateBudget(proposal, budgetTrans))
                    {
                        var cancelBudgetYearTrans = new MPS_BudgetYearTrans()
                        {
                            Year = budgetTrans.Year,
                            UnitCode = budgetTrans.UnitCode,
                            TypeYearPlan = budgetTrans.TypeYearPlan,
                            Status = "Cancel",
                            DocumentStatus = "Revise",
                            DocumentType = "Proposal",
                            DocumentId = proposal.Id,
                            DocumentNo = proposal.DocumentNumber,
                            Amount = budgetTrans.Amount * (-1),
                            TransDate = DateTime.Now
                        };

                        cancelBudgetYearTrans = _budgetYearTransService.Insert(cancelBudgetYearTrans);

                        var budgetYearTrans = new MPS_BudgetYearTrans()
                        {
                            Year = year,
                            UnitCode = unitCode,
                            TypeYearPlan = typeYearPlan,
                            Status = "Reserve",
                            DocumentStatus = "Revise",
                            DocumentType = "Proposal",
                            DocumentId = proposal.Id,
                            DocumentNo = proposal.DocumentNumber,
                            Amount = proposal.Budget,
                            TransDate = DateTime.Now
                        };

                        budgetYearTrans = _budgetYearTransService.Insert(budgetYearTrans);
                        _session.SaveChanges();

                        result = true;
                    }

                }
            }

            return result;
        }

        public bool ReturnTrans(int proposalId)
        {
            var result = false;
            var proposal = _proposalService.GetWorkflowTable(proposalId);
            if (proposal == null)
                throw new Exception(string.Format(Resources.Error.DataWithIDNotExists, proposalId));

            if (proposal.TypeProposal != null && !string.IsNullOrEmpty(proposal.UnitCode))
            {
                //Type Year Plan == null 
                if (string.IsNullOrEmpty(proposal.TypeProposal.TypeYearPlan))
                    return true;
                else
                {
                    var year = proposal.StartDate.Year.ToString();
                    var unitCode = proposal.UnitCode;
                    var typeYearPlan = proposal.TypeProposal.TypeYearPlan;

                    var budgetTrans = _budgetYearTransService.LastReseveBudgetTrans("Proposal", proposal.Id);
                    if (budgetTrans == null)
                        throw new Exception(string.Format(Resources.Error.BudgetTransNotFound, proposal.DocumentNumber));
                    //if (budgetTrans.Status != "Reserve")
                    //    throw new Exception("Invalid budget status for budget returning");

                    var actual =
                        _budgetProposalTransService.GetWhere(m => !m.InActive && m.ProposalId == proposalId)
                            .Sum(m => m.Amount);
                    var budgetReturn = budgetTrans.Amount - actual;
                    var budgetYearTrans = new MPS_BudgetYearTrans()
                    {
                        Year = budgetTrans.Year,
                        UnitCode = budgetTrans.UnitCode,
                        TypeYearPlan = budgetTrans.TypeYearPlan,
                        Status = "Return",
                        DocumentStatus = "Close",
                        DocumentType = "Proposal",
                        DocumentId = proposal.Id,
                        DocumentNo = proposal.DocumentNumber,
                        Amount = budgetReturn * (-1),
                        TransDate = DateTime.Now
                    };

                    budgetYearTrans = _budgetYearTransService.Insert(budgetYearTrans);
                    _session.SaveChanges();

                    result = true;
                }
            }

            return result;
        }

        //public bool IncomeTrans(int proposalId)
        //{
        //    var result = false;
        //    var proposal = _proposalService.GetWorkflowTable(proposalId);
        //    if (proposal == null)
        //        throw new Exception(string.Format(Resources.Error.DataWithIDNotExists, proposalId));

        //    if (proposal.TypeProposal != null && !string.IsNullOrEmpty(proposal.UnitCode))
        //    {
        //        //Type Year Plan == null 
        //        if (string.IsNullOrEmpty(proposal.TypeProposal.TypeYearPlan))
        //            return true;
        //        else
        //        {
        //            var year = proposal.StartDate.Year.ToString();
        //            var unitCode = proposal.UnitCode;
        //            var typeYearPlan = proposal.TypeProposal.TypeYearPlan;

        //            var budgetTrans = _budgetYearTransService.LastReseveBudgetTrans("Proposal", proposal.Id);
        //            if (budgetTrans == null)
        //                throw new Exception(string.Format(Resources.Error.BudgetTransNotFound, proposal.DocumentNumber));
        //            //if (budgetTrans.Status != "Reserve")
        //            //    throw new Exception("Invalid budget status for budget returning");
        //            var depositAmount = proposal.IncomeDeposit.Where(m => !m.Deleted).Sum(m => m.Budget);
        //            var incomebudgetYearTrans = new MPS_BudgetYearTrans()
        //            {
        //                Year = year,
        //                UnitCode = unitCode,
        //                TypeYearPlan = typeYearPlan,
        //                Status = "IncomeDeposit",
        //                DocumentStatus = "Close",
        //                DocumentType = "Proposal",
        //                DocumentId = proposal.Id,
        //                DocumentNo = proposal.DocumentNumber,
        //                Amount = depositAmount * (-1),
        //                TransDate = DateTime.Now
        //            };

        //            incomebudgetYearTrans = _budgetYearTransService.Insert(incomebudgetYearTrans);
        //            var otherAmount = proposal.IncomeOther.Where(m => !m.Deleted).Sum(m => m.Actual);
        //            if (proposal.IncomeOther.Any(m => m.SponcorName == "DO3"))
        //            {
        //                var actualDo3 =
        //                    proposal.IncomeOther.Where(m => m.SponcorName == "DO3" && !m.Deleted).Sum(m => m.Actual);
        //                var budgetDo3 =
        //                   proposal.IncomeOther.Where(m => m.SponcorName == "DO3" && !m.Deleted).Sum(m => m.Budget);
        //                if ((actualDo3 - budgetDo3) > 0)
        //                {
        //                    otherAmount = otherAmount - (actualDo3 - budgetDo3);
        //                }
        //            }
        //            var otherbudgetYearTrans = new MPS_BudgetYearTrans()
        //            {
        //                Year = year,
        //                UnitCode = unitCode,
        //                TypeYearPlan = typeYearPlan,
        //                Status = "IncomeOther",
        //                DocumentStatus = "Close",
        //                DocumentType = "Proposal",
        //                DocumentId = proposal.Id,
        //                DocumentNo = proposal.DocumentNumber,
        //                Amount = otherAmount * (-1) ?? 0,
        //                TransDate = DateTime.Now
        //            };

        //            otherbudgetYearTrans = _budgetYearTransService.Insert(otherbudgetYearTrans);
        //            _session.SaveChanges();

        //            result = true;
        //        }
        //    }

        //    return result;
        //}

        //public bool MemoSpecialReturnTrans(int memoId)
        //{
        //    var result = false;
        //    var memo = _memoService.GetWorkflowTable(memoId);
        //    if (memo == null)
        //        throw new Exception(string.Format(Resources.Error.DataWithIDNotExists, memoId));

        //    if (memo.ProposalLine.ProposalTable.TypeProposal != null && !string.IsNullOrEmpty(memo.UnitCode) && memo.MemoType.ToLower() != "normal")
        //    {
        //        //Type Year Plan == null 
        //        if (string.IsNullOrEmpty(memo.ProposalLine.ProposalTable.TypeProposal.TypeYearPlan))
        //            return true;
        //        else
        //        {
        //            var proposal = memo.ProposalLine.ProposalTable;
        //            var year = proposal.StartDate.Year.ToString();
        //            var unitCode = memo.PayToUnitCode;
        //            var typeYearPlan = proposal.TypeProposal.TypeYearPlan;

        //            //var budgetTrans = _budgetYearTransService.LastReseveBudgetTrans("Proposal", proposal.Id);
        //            //if (budgetTrans == null)
        //            //    throw new Exception(string.Format(Resources.Error.BudgetTransNotFound, proposal.DocumentNumber));
        //            //if (budgetTrans.Status != "Reserve")
        //            //    throw new Exception("Invalid budget status for budget returning");

        //            var budgetYearTrans = new MPS_BudgetYearTrans()
        //            {
        //                Year = year,
        //                UnitCode = unitCode,
        //                TypeYearPlan = typeYearPlan,
        //                Status = "Return",
        //                DocumentStatus = "Return",
        //                DocumentType = "Memo",
        //                DocumentId = proposal.Id,
        //                DocumentNo = proposal.DocumentNumber,
        //                RefDocumentId = memoId,
        //                RefDocumentNumber = memo.DocumentNumber,
        //                RefDocumentType = memo.MemoType,
        //                Amount = memo.BudgetDetail * (-1) ?? 0,
        //                TransDate = DateTime.Now
        //            };

        //            budgetYearTrans = _budgetYearTransService.Insert(budgetYearTrans);
        //        }
        //        _session.SaveChanges();

        //        result = true;

        //    }
        //    return result;
        //}

        #region Validation
        public bool ValidateBudget(int proposalId, MPS_BudgetYearTrans lastTrans = null)
        {
            var proposal = _proposalService.GetWorkflowTable(proposalId);
            if (proposal == null)
                throw new Exception(string.Format(Resources.Error.DataWithIDNotExists, proposalId));

            return ValidateBudget(proposal, lastTrans);
        }
        public bool ValidateBudget(MPS_ProposalTable proposal, MPS_BudgetYearTrans lastTrans = null)
        {
            var result = false;
            if (proposal != null)
            {
                if (proposal.TypeProposal != null && !string.IsNullOrEmpty(proposal.UnitCode))
                {
                    //Type Year Plan == null 
                    if (string.IsNullOrEmpty(proposal.TypeProposal.TypeYearPlan))
                    {
                        result = true;
                    }
                    else
                    {
                        var year = proposal.StartDate.Year.ToString();
                        var unitCode = proposal.UnitCode;
                        var typeYearPlan = proposal.TypeProposal.TypeYearPlan;

                        var budgetYear = _budgetYearPlanService.GetBudgetYearPlan(year, unitCode, typeYearPlan);
                        if (budgetYear == null)
                            throw new Exception(string.Format(Resources.Error.BudgetYearPlanNotFound, year, unitCode, typeYearPlan));
                        var budgetTrans = _budgetYearTransService.GetBudgetTransTotal(year, unitCode, typeYearPlan);

                        var lastBudget = lastTrans != null ? lastTrans.Amount : 0;
                        if (lastTrans != null && lastTrans.Year != year)
                            lastBudget = 0;

                        var budgetBalance = (budgetYear.Budget - budgetTrans + lastBudget);
                        result = budgetBalance >= proposal.Budget;
                        if (result == false)
                            throw new Exception(string.Format(Resources.Error.BudgetExceed, budgetBalance));
                    }
                }
            }
            return result;
        }
        #endregion

        public bool MemoSpecialReturnTrans(int memoId)
        {
            var result = false;
            var memo = _memoService.GetWorkflowTable(memoId);
            if (memo == null)
                throw new Exception(string.Format(Resources.Error.DataWithIDNotExists, memoId));

            if (memo.ProposalLine.ProposalTable.TypeProposal != null && !string.IsNullOrEmpty(memo.UnitCode) && memo.MemoType.ToLower() != "normal")
            {
                //Type Year Plan == null 
            
                
                    if (string.IsNullOrEmpty(memo.ProposalLine.ProposalTable.TypeProposal.TypeYearPlan))
                        return true;
                    else
                    {
                        if (memo.ActualCharge)
                        {
                            var proposal = memo.ProposalLine.ProposalTable;
                            var year = proposal.StartDate.Year.ToString();
                            var unitCode = proposal.UnitCode;
                            var typeYearPlan = proposal.TypeProposal.TypeYearPlan;

                            //var budgetTrans = _budgetYearTransService.LastReseveBudgetTrans("Proposal", proposal.Id);
                            //if (budgetTrans == null)
                            //    throw new Exception(string.Format(Resources.Error.BudgetTransNotFound, proposal.DocumentNumber));
                            //if (budgetTrans.Status != "Reserve")
                            //    throw new Exception("Invalid budget status for budget returning");

                            var budgetYearTrans = new MPS_BudgetYearTrans()
                            {
                                Year = year,
                                UnitCode = unitCode,
                                TypeYearPlan = typeYearPlan,
                                Status = "Return",
                                DocumentStatus = "Return",
                                DocumentType = "Memo",
                                DocumentId = proposal.Id,
                                DocumentNo = proposal.DocumentNumber,
                                RefDocumentId = memoId,
                                RefDocumentNumber = memo.DocumentNumber,
                                RefDocumentType = memo.MemoType,
                                Amount = memo.BudgetDetail * (-1) ?? 0,
                                TransDate = DateTime.Now
                            };
                            budgetYearTrans = _budgetYearTransService.Insert(budgetYearTrans);
                        }
                        else
                        {
                            var proposal = memo.ProposalLine.ProposalTable;
                            var year = proposal.StartDate.Year.ToString();
                            var unitCode = memo.PayToUnitCode;
                            var typeYearPlan = proposal.TypeProposal.TypeYearPlan;

                            //var budgetTrans = _budgetYearTransService.LastReseveBudgetTrans("Proposal", proposal.Id);
                            //if (budgetTrans == null)
                            //    throw new Exception(string.Format(Resources.Error.BudgetTransNotFound, proposal.DocumentNumber));
                            //if (budgetTrans.Status != "Reserve")
                            //    throw new Exception("Invalid budget status for budget returning");

                            var budgetYearTrans = new MPS_BudgetYearTrans()
                            {
                                Year = year,
                                UnitCode = unitCode,
                                TypeYearPlan = typeYearPlan,
                                Status = "Return",
                                DocumentStatus = "Return",
                                DocumentType = "Memo",
                                DocumentId = proposal.Id,
                                DocumentNo = proposal.DocumentNumber,
                                RefDocumentId = memoId,
                                RefDocumentNumber = memo.DocumentNumber,
                                RefDocumentType = memo.MemoType,
                                Amount = memo.BudgetDetail * (-1) ?? 0,
                                TransDate = DateTime.Now
                            };
                            budgetYearTrans = _budgetYearTransService.Insert(budgetYearTrans);
                        }                    
                }
                
                _session.SaveChanges();

                result = true;

            }
            return result;
        }


        public bool IncomeTrans(int proposalId)
        {
            var result = false;
            var proposal = _proposalService.GetWorkflowTable(proposalId);
            if (proposal == null)
                throw new Exception(string.Format(Resources.Error.DataWithIDNotExists, proposalId));

            if (proposal.TypeProposal != null && !string.IsNullOrEmpty(proposal.UnitCode))
            {
                //Type Year Plan == null 
                if (string.IsNullOrEmpty(proposal.TypeProposal.TypeYearPlan))
                    return true;
                else
                {
                    var year = proposal.StartDate.Year.ToString();
                    var unitCode = proposal.UnitCode;
                    var typeYearPlan = proposal.TypeProposal.TypeYearPlan;

                    var budgetTrans = _budgetYearTransService.LastReseveBudgetTrans("Proposal", proposal.Id);
                    if (budgetTrans == null)
                        throw new Exception(string.Format(Resources.Error.BudgetTransNotFound, proposal.DocumentNumber));



                    //เปิดใช้เมื่อเอา Change ขึ้น
                    //var depositAmount = proposal.IncomeDeposit.Where(m => !m.Deleted && !m.ActualCharge).Sum(m => m.Budget);

                    //ปิดใช้เมื่อเอา Change ขึ้น
                    var depositAmount = proposal.IncomeDeposit.Where(m => !m.Deleted).Sum(m => m.Budget);

                    var incomebudgetYearTrans = new MPS_BudgetYearTrans()
                    {
                        Year = year,
                        UnitCode = unitCode,
                        TypeYearPlan = typeYearPlan,
                        Status = "IncomeDeposit",
                        DocumentStatus = "Close",
                        DocumentType = "Proposal",
                        DocumentId = proposal.Id,
                        DocumentNo = proposal.DocumentNumber,
                        Amount = depositAmount * (-1),
                        TransDate = DateTime.Now
                    };

                    incomebudgetYearTrans = _budgetYearTransService.Insert(incomebudgetYearTrans);
                    var otherAmount = proposal.IncomeOther.Where(m => !m.Deleted).Sum(m => m.Actual);
                    if (proposal.IncomeOther.Any(m => m.SponcorName == "DO3"))
                    {
                        var actualDo3 =
                            proposal.IncomeOther.Where(m => m.SponcorName == "DO3" && !m.Deleted).Sum(m => m.Actual);
                        var budgetDo3 =
                           proposal.IncomeOther.Where(m => m.SponcorName == "DO3" && !m.Deleted).Sum(m => m.Budget);
                        if ((actualDo3 - budgetDo3) > 0)
                        {
                            otherAmount = otherAmount - (actualDo3 - budgetDo3);
                        }
                    }
                    var otherbudgetYearTrans = new MPS_BudgetYearTrans()
                    {
                        Year = year,
                        UnitCode = unitCode,
                        TypeYearPlan = typeYearPlan,
                        Status = "IncomeOther",
                        DocumentStatus = "Close",
                        DocumentType = "Proposal",
                        DocumentId = proposal.Id,
                        DocumentNo = proposal.DocumentNumber,
                        Amount = otherAmount * (-1) ?? 0,
                        TransDate = DateTime.Now
                    };

                    otherbudgetYearTrans = _budgetYearTransService.Insert(otherbudgetYearTrans);
                    _session.SaveChanges();

                    result = true;
                }
            }

            return result;
        }
        //

    }
}