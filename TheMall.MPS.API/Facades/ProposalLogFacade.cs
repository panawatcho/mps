﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Reporting.WebForms;
using TheMall.MPS.API.Mappers;
using TheMall.MPS.API.Services;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Models.Enums;
using TheMall.MPS.Models.Proposal;
using TheMall.MPS.Models.ProposalLog;

namespace TheMall.MPS.API.Facades
{
    public interface IProposalLogFacade
    {
        bool ReviseProposal(int proposalId);
        bool CheckStartNewProcess(MPS_ProposalTable proposalTable);
        bool TrackChanges(int proposalId);
        bool CancelRevise(int proposalId,bool createLog = true);
    }
    public class ProposalLogFacade : IProposalLogFacade
    {
        private readonly IMPSSession _session;
        private readonly IProposalService _proposalService;
        private readonly IProposalLineService _proposalLineService;
        private readonly IProposalLineAPService _proposalLineTemplateService;
        private readonly IProposalIncomeDepositService _proposalIncomeDepositService;
        private readonly IProposalIncomeOtherService _proposalIncomeOtherService;
        private readonly IProposalApprovalService _proposalApprovalService;

        //LOG
        private readonly IProposalLogMapper _logProposalMapper;
        private readonly ILogProposalService _logProposalService;
        private readonly ILogProposalLineService _logProposalLineService;
        private readonly ILogProposalLineTemplateService _logProposalLineTemplateService;
        private readonly ILogProposalLineSharedTemplateService _logProposalLineSharedTemplateService;
        private readonly ILogProposalApprovalService _logProposalApprovalService;
        private readonly ILogProposalDepositLineService _logProposalDepositLineService;
        private readonly ILogProposalIncomeDepositService _logProposalIncomeDepositService;
        private readonly ILogProposalIncomeOtherService _logProposalIncomeOtherService;
        private readonly ILogProposalAccountTransService _logProposalAccountTransService;
        private readonly ILogProposalIncomeTotalSaleService _logProposalIncomeTotalSaleService;
        private readonly ILogProposalIncomeTotalSaleTemplateService _logProposalIncomeTotalSaleTmplService;
        private readonly ILogProposalExpenseTopicService _logProposalExpenseTopicService;
        private readonly ILogProposalTrackChangesService _logProposalTrackChangesService;
        public ProposalLogFacade(IMPSSession session,
            IProposalService proposalService,
            IProposalLineService proposalLineService,
            IProposalLineAPService proposalLineTemplateService,
            IProposalIncomeDepositService proposalIncomeDepositService,
            IProposalIncomeOtherService proposalIncomeOtherService,
            IProposalApprovalService proposalApprovalService,
            IProposalLogMapper logProposalMapper,
            ILogProposalService logProposalService,
            ILogProposalLineService logProposalLineService,
            ILogProposalLineTemplateService logProposalLineTemplateService,
            ILogProposalLineSharedTemplateService logProposalLineSharedTemplateService,
            ILogProposalApprovalService logProposalApprovalService,
            ILogProposalDepositLineService logProposalDepositLineService,
            ILogProposalIncomeDepositService logProposalIncomeDepositService,
            ILogProposalIncomeOtherService logProposalIncomeOtherService,
            ILogProposalAccountTransService logProposalAccountTransService,
            ILogProposalIncomeTotalSaleService logProposalIncomeTotalSaleService,
            ILogProposalIncomeTotalSaleTemplateService logProposalIncomeTotalSaleTmplService,
            ILogProposalExpenseTopicService logProposalExpenseTopicService, ILogProposalTrackChangesService logProposalTrackChangesService)
        {
            _session = session;
            _proposalService = proposalService;
            _proposalLineService = proposalLineService;
            _proposalLineTemplateService = proposalLineTemplateService;
            _proposalIncomeDepositService = proposalIncomeDepositService;
            _proposalIncomeOtherService = proposalIncomeOtherService;
            _proposalApprovalService = proposalApprovalService;
            _logProposalMapper = logProposalMapper;
            _logProposalService = logProposalService;
            _logProposalLineService = logProposalLineService;
            _logProposalLineTemplateService = logProposalLineTemplateService;
            _logProposalLineSharedTemplateService = logProposalLineSharedTemplateService;
            _logProposalApprovalService = logProposalApprovalService;
            _logProposalDepositLineService = logProposalDepositLineService;
            _logProposalIncomeDepositService = logProposalIncomeDepositService;
            _logProposalIncomeOtherService = logProposalIncomeOtherService;
            _logProposalAccountTransService = logProposalAccountTransService;
            _logProposalIncomeTotalSaleService = logProposalIncomeTotalSaleService;
            _logProposalIncomeTotalSaleTmplService = logProposalIncomeTotalSaleTmplService;
            _logProposalExpenseTopicService = logProposalExpenseTopicService;
            _logProposalTrackChangesService = logProposalTrackChangesService;

        }

        public bool ReviseProposal(int proposalId)
        {
            var result = false;
            var proposalTable = _proposalService.Find(proposalId);
            if (proposalTable != null)
            {
                if (proposalTable.StatusFlag == DocumentStatusFlagId.ReviseCompleted)
                    return true;
                if (proposalTable.StatusFlag != DocumentStatusFlagId.Completed
                    || proposalTable.CloseFlag == true)
                    throw new Exception("Invalid proposal status.");
                var lastRevision = _logProposalService.GetWhere(m => m.ProposalId == proposalId).Max(m => m.Revision);
                var revision = (lastRevision.HasValue && lastRevision.Value >= 1)
                    ? (lastRevision.Value + 1)
                    : 1;

                CreateLog(proposalTable, revision);

                proposalTable.Status = "Revise";
                proposalTable.DocumentStatus = DocumentStatusFlag.Draft.ToString();
                proposalTable.StatusFlag = DocumentStatusFlagId.ReviseCompleted;
                var updateRevision = proposalTable.Revision ?? 1;
                proposalTable.Revision = updateRevision + 1;
                proposalTable = _proposalService.Update(proposalTable, proposalTable.Id);
                _session.SaveChanges();

                result = true;
            }
            else
            {
                throw new Exception(string.Format(Resources.Error.DataWithIDNotExists, proposalId));
            }
            return result;
        }

        public void CreateLog(MPS_ProposalTable proposalTable, int revision, bool updateRef = true)
        {
            //Log - ProposalTable 
            var logProposalTable = _logProposalMapper.ToLogProposalTable(proposalTable);
            logProposalTable.Revision = revision;
            logProposalTable = _logProposalService.Insert(logProposalTable);

            //Log - ProposalLine
            foreach (var proposalLine in proposalTable.ProposalLine)
            {
                var logProposalLine = _logProposalMapper.ToLogProposalLine(proposalLine);
                if (!updateRef && proposalLine.StatusFlag == DocumentStatusFlagId.Draft)
                {
                    logProposalLine.ProposalLineId = null;
                }
                logProposalLine.ProposalTable = logProposalTable;
                logProposalLine = _logProposalLineService.Insert(logProposalLine);

                //Log - ProposalLineTemplate
                foreach (var proposalLineTemplate in proposalLine.ProposalLineTemplates)
                {
                    var logProposalLineTemplate = _logProposalMapper.ToLogProposalLineTemplate(proposalLineTemplate);
                    if (!updateRef && proposalLine.StatusFlag == DocumentStatusFlagId.Draft)
                    {
                        logProposalLineTemplate.ProposalLineTemplateId = null;
                    }
                    logProposalLineTemplate.ProposalLine = logProposalLine;
                    logProposalLineTemplate = _logProposalLineTemplateService.Insert(logProposalLineTemplate);
                }
                //Update ProposalLine
                if (updateRef)
                {
                    proposalLine.StatusFlag = DocumentStatusFlagId.Active;
                    _proposalLineService.Update(proposalLine, proposalLine.Id);
                }
            }
            //Log - ProposalLineSharedTemplate
            foreach (var proposalLineSharedTemplate in proposalTable.TemplateLines)
            {
                var logProposalLineSharedTemplate =
                    _logProposalMapper.ToLogProposalLineSharedTemplate(proposalLineSharedTemplate);
                logProposalLineSharedTemplate.ProposalTable = logProposalTable;
                logProposalLineSharedTemplate =
                    _logProposalLineSharedTemplateService.Insert(logProposalLineSharedTemplate);
            }
            //Log - ProposalApproval
            foreach (var proposalApproval in proposalTable.ProposalApproval)
            {
                var logProposalApproval = _logProposalMapper.ToLogProposalApproval(proposalApproval);
                if (!updateRef && proposalApproval.StatusFlag == DocumentStatusFlagId.Draft)
                {
                    logProposalApproval.ProposalApprovalId = null;
                }
                logProposalApproval.ProposalTable = logProposalTable;
                logProposalApproval = _logProposalApprovalService.Insert(logProposalApproval);
                //Update ProposalApproval
                if (updateRef)
                {
                    proposalApproval.StatusFlag = DocumentStatusFlagId.Active;
                    proposalApproval.ApproveStatus = false;
                    _proposalApprovalService.Update(proposalApproval, proposalApproval.Id);
                }
            }
            //Log - DepositLine
            foreach (var depositLine in proposalTable.DepositLine)
            {
                var logDepositLine = _logProposalMapper.ToLogDepositLine(depositLine);
                logDepositLine.ProposalTable = logProposalTable;
                logDepositLine = _logProposalDepositLineService.Insert(logDepositLine);

                //Update DepositLine
                //depositLine.StatusFlag = DocumentStatusFlagId.Active;
                //_depositLineService.Update(depositLine, depositLine.Id);
            }
            //Log - IncomeDeposit
            foreach (var incomeDeposit in proposalTable.IncomeDeposit)
            {
                var logIncomeDeposit = _logProposalMapper.ToLogIncomeDeposit(incomeDeposit);
                if (!updateRef && !_logProposalIncomeDepositService.Any(m => m.IncomeDepositId == incomeDeposit.Id))
                {
                    logIncomeDeposit.IncomeDepositId = null;
                }
                logIncomeDeposit.ProposalTable = logProposalTable;
                logIncomeDeposit = _logProposalIncomeDepositService.Insert(logIncomeDeposit);
            }
            //Log - IncomeOther
            foreach (var incomeOther in proposalTable.IncomeOther)
            {
                var logIncomeOther = _logProposalMapper.ToLogIncomeOther(incomeOther);
                if (!updateRef && incomeOther.StatusFlag == DocumentStatusFlagId.Draft)
                {
                    logIncomeOther.IncomeOtherId = null;
                }
                logIncomeOther.ProposalTable = logProposalTable;
                logIncomeOther = _logProposalIncomeOtherService.Insert(logIncomeOther);

                //Log - AccountTrans
                foreach (var accountTrans in incomeOther.AccountTrans)
                {
                    var logAccountTrans = _logProposalMapper.ToLogAccountTrans(accountTrans);
                    if (!updateRef && incomeOther.StatusFlag == DocumentStatusFlagId.Draft)
                    {
                        logAccountTrans.AccountTransId = null;
                    }
                    logAccountTrans.IncomeOther = logIncomeOther;
                    logAccountTrans = _logProposalAccountTransService.Insert(logAccountTrans);
                }
                //Update IncomeOther
                if (updateRef)
                {
                    incomeOther.StatusFlag = DocumentStatusFlagId.Active;
                    _proposalIncomeOtherService.Update(incomeOther, incomeOther.Id);
                }
            }
            //Log - IncomeTotalSale
            foreach (var incomeTotalSale in proposalTable.IncomeTotalSale)
            {
                var logIncomeTotalSale = _logProposalMapper.ToLogIncomeTotalSale(incomeTotalSale);
                logIncomeTotalSale.ProposalTable = logProposalTable;
                logIncomeTotalSale = _logProposalIncomeTotalSaleService.Insert(logIncomeTotalSale);

                //Log - IncomeTotalSaleTemplate
                foreach (var incomeTotalSaleTemplate in incomeTotalSale.IncomeTotalSaleTemplate)
                {
                    var logIncomeTotalSaleTemplate =
                        _logProposalMapper.ToLogIncomeTotalSaleTemplate(incomeTotalSaleTemplate);
                    logIncomeTotalSaleTemplate.IncomeTotalSale = logIncomeTotalSale;
                    logIncomeTotalSaleTemplate =
                        _logProposalIncomeTotalSaleTmplService.Insert(logIncomeTotalSaleTemplate);
                }
            }

            //Log - ExpenseTopic
            var proposalLineGroup = proposalTable.ProposalLine.Where(m => !m.Deleted).GroupBy(m => m.ExpenseTopicCode);
            foreach (var expenseTopic in proposalLineGroup)
            {
                var logExpenseTopic = new Models.ProposalLog.MPS_Log_ProposalExpenseTopic();
                logExpenseTopic.ProposalLog = logProposalTable;
                logExpenseTopic.ExpenseTopicCode = expenseTopic.Key;
                logExpenseTopic.Amount = expenseTopic.Sum(m => m.BudgetPlan);

                logExpenseTopic = _logProposalExpenseTopicService.Insert(logExpenseTopic);
            }
        }
        #region TrackChanges
        public bool TrackChanges(int proposalId)
        {
            var result = false;
            var proposalTable = _proposalService.Find(proposalId);
            if (proposalTable != null)
            {
                //Get Last Log
                var logProposalTable = _logProposalService.LastOrDefault(m => m.ProposalId == proposalId);
                if (logProposalTable == null)
                    throw new Exception("Proposal Log could not be found.");

                //Log - ProposalTable
                ProposalTableTrackChanges(proposalTable, logProposalTable);
                //Log - ProposalLine
                foreach (var proposalLine in proposalTable.ProposalLine)
                {
                    var logProposalLine = logProposalTable.ProposalLine.LastOrDefault(m => m.ProposalLineId == proposalLine.Id);
                    ProposalLineTrackChanges(logProposalTable, proposalLine, logProposalLine);
                }
                //Log - IncomeDeposit
                foreach (var incomeDeposit in proposalTable.IncomeDeposit)
                {
                    var logIncomeDeposit = logProposalTable.IncomeDeposit.LastOrDefault(m => m.IncomeDepositId == incomeDeposit.Id);
                    IncomeDepositTrackChanges(logProposalTable, incomeDeposit, logIncomeDeposit);
                }
                //Log - IncomeOther
                foreach (var incomeOther in proposalTable.IncomeOther)
                {
                    var logIncomeOther = logProposalTable.IncomeOther.LastOrDefault(m => m.IncomeOtherId == incomeOther.Id);
                    IncomeOtherTrackChanges(logProposalTable, incomeOther, logIncomeOther);
                }

                //Log - ProposalApproval
                foreach (var proposalApproval in proposalTable.ProposalApproval)
                {
                    var logProposalApproval = logProposalTable.ProposalApproval.LastOrDefault(m => m.ProposalApprovalId == proposalApproval.Id);
                    ProposalApprovalTrackChanges(logProposalTable, proposalApproval, logProposalApproval);
                }
                _session.SaveChanges();
                result = true;
            }
            else
            {
                throw new Exception(string.Format(Resources.Error.DataWithIDNotExists, proposalId));
            }
            return result;
        }

        public void ProposalTableTrackChanges(MPS_ProposalTable proposalTable, MPS_Log_ProposalTable logProposalTable)
        {
            var proposalId = proposalTable.Id;
            var proposalLogId = logProposalTable.Id;
            var refTableName = "ProposalTable";
            var refTableDesc = "Proposal";
            if (proposalTable.Other != logProposalTable.Other)
            {
                var proposalTrackChanges = new MPS_Log_ProposalTrackChanges()
                {
                    ProposalId = proposalId,
                    ProposalLogId = proposalLogId,
                    RefTableName = refTableName,
                    RefTableDesc = refTableDesc,
                    RefTableId = proposalId,
                    RefLogId = proposalLogId,
                    FieldName = "Other",
                    FieldDescription = Resources.Proposal.Other,
                    State = "Modified",
                    OldValue = logProposalTable.Other,
                    NewValue = proposalTable.Other
                };
                _logProposalTrackChangesService.Insert(proposalTrackChanges);
            }
            if (proposalTable.StartDate.Date != logProposalTable.StartDate.Date)
            {
                var proposalTrackChanges = new MPS_Log_ProposalTrackChanges()
                {
                    ProposalId = proposalId,
                    ProposalLogId = proposalLogId,
                    RefTableName = refTableName,
                    RefTableDesc = refTableDesc,
                    RefTableId = proposalId,
                    RefLogId = proposalLogId,
                    FieldName = "StartDate",
                    FieldDescription = Resources.Proposal.StartDate,
                    State = "Modified",
                    OldValue = logProposalTable.StartDate.ToString("dd/MM/yyyy"),
                    NewValue = proposalTable.StartDate.ToString("dd/MM/yyyy"),
                };
                _logProposalTrackChangesService.Insert(proposalTrackChanges);
            }
            if (proposalTable.EndDate.Date != logProposalTable.EndDate.Date)
            {
                var proposalTrackChanges = new MPS_Log_ProposalTrackChanges()
                {
                    ProposalId = proposalId,
                    ProposalLogId = proposalLogId,
                    RefTableName = refTableName,
                    RefTableDesc = refTableDesc,
                    RefTableId = proposalId,
                    RefLogId = proposalLogId,
                    FieldName = "EndDate",
                    FieldDescription = Resources.Proposal.EndDate,
                    State = "Modified",
                    OldValue = logProposalTable.EndDate.ToString("dd/MM/yyyy"),
                    NewValue = proposalTable.EndDate.ToString("dd/MM/yyyy"),
                };
                _logProposalTrackChangesService.Insert(proposalTrackChanges);
            }
            if (logProposalTable.ProjectStartDate != null && (proposalTable.ProjectStartDate != null && proposalTable.ProjectStartDate.Value.Date != logProposalTable.ProjectStartDate.Value.Date))
            {
                var proposalTrackChanges = new MPS_Log_ProposalTrackChanges()
                {
                    ProposalId = proposalId,
                    ProposalLogId = proposalLogId,
                    RefTableName = refTableName,
                    RefTableDesc = refTableDesc,
                    RefTableId = proposalId,
                    RefLogId = proposalLogId,
                    FieldName = "ProjectStartDate",
                    FieldDescription = Resources.Proposal.ProjectStartDate,
                    State = "Modified",
                    OldValue = logProposalTable.ProjectStartDate.HasValue ? logProposalTable.ProjectStartDate.Value.ToString("dd/MM/yyyy") : null,
                    NewValue = proposalTable.ProjectStartDate.HasValue ? proposalTable.ProjectStartDate.Value.ToString("dd/MM/yyyy") : null,
                };
                _logProposalTrackChangesService.Insert(proposalTrackChanges);
            }
            if (proposalTable.ProjectEndDate != logProposalTable.ProjectEndDate)
            {
                var proposalTrackChanges = new MPS_Log_ProposalTrackChanges()
                {
                    ProposalId = proposalId,
                    ProposalLogId = proposalLogId,
                    RefTableName = refTableName,
                    RefTableDesc = refTableDesc,
                    RefTableId = proposalId,
                    RefLogId = proposalLogId,
                    FieldName = "ProjectEndDate",
                    FieldDescription = Resources.Proposal.ProjectEndDate,
                    State = "Modified",
                    OldValue = logProposalTable.ProjectEndDate.HasValue ? logProposalTable.ProjectEndDate.Value.ToString("dd/MM/yyyy") : null,
                    NewValue = proposalTable.ProjectEndDate.HasValue ? proposalTable.ProjectEndDate.Value.ToString("dd/MM/yyyy") : null,
                };
                _logProposalTrackChangesService.Insert(proposalTrackChanges);
            }
            if (proposalTable.Objective != logProposalTable.Objective)
            {
                var proposalTrackChanges = new MPS_Log_ProposalTrackChanges()
                {
                    ProposalId = proposalId,
                    ProposalLogId = proposalLogId,
                    RefTableName = refTableName,
                    RefTableDesc = refTableDesc,
                    RefTableId = proposalId,
                    RefLogId = proposalLogId,
                    FieldName = "Objective",
                    FieldDescription = Resources.Proposal.Objective,
                    State = "Modified",
                    OldValue = null,
                    NewValue = null
                };
                _logProposalTrackChangesService.Insert(proposalTrackChanges);
            }
            if (proposalTable.DueDatePropose != logProposalTable.DueDatePropose)
            {
                var proposalTrackChanges = new MPS_Log_ProposalTrackChanges()
                {
                    ProposalId = proposalId,
                    ProposalLogId = proposalLogId,
                    RefTableName = refTableName,
                    RefTableDesc = refTableDesc,
                    RefTableId = proposalId,
                    RefLogId = proposalLogId,
                    FieldName = "DueDatePropose",
                    FieldDescription = "Due Date (เสนอ)",
                    State = "Modified",
                    OldValue = logProposalTable.DueDatePropose.HasValue ? logProposalTable.DueDatePropose.Value.ToString("dd/MM/yyyy") : null,
                    NewValue = proposalTable.DueDatePropose.HasValue ? proposalTable.DueDatePropose.Value.ToString("dd/MM/yyyy") : null,
                };
                _logProposalTrackChangesService.Insert(proposalTrackChanges);
            }
            if (proposalTable.DueDateAccept != logProposalTable.DueDateAccept)
            {
                var proposalTrackChanges = new MPS_Log_ProposalTrackChanges()
                {
                    ProposalId = proposalId,
                    ProposalLogId = proposalLogId,
                    RefTableName = refTableName,
                    RefTableDesc = refTableDesc,
                    RefTableId = proposalId,
                    RefLogId = proposalLogId,
                    FieldName = "DueDateAccept",
                    FieldDescription = "Due Date (เห็นชอบ)",
                    State = "Modified",
                    OldValue = logProposalTable.DueDateAccept.HasValue ? logProposalTable.DueDateAccept.Value.ToString("dd/MM/yyyy") : null,
                    NewValue = proposalTable.DueDateAccept.HasValue ? proposalTable.DueDateAccept.Value.ToString("dd/MM/yyyy") : null,
                };
                _logProposalTrackChangesService.Insert(proposalTrackChanges);
            }
            if (proposalTable.DueDateApprove != logProposalTable.DueDateApprove)
            {
                var proposalTrackChanges = new MPS_Log_ProposalTrackChanges()
                {
                    ProposalId = proposalId,
                    ProposalLogId = proposalLogId,
                    RefTableName = refTableName,
                    RefTableDesc = refTableDesc,
                    RefTableId = proposalId,
                    RefLogId = proposalLogId,
                    FieldName = "DueDateApprove",
                    FieldDescription = "Due Date (อนุมัติ)",
                    State = "Modified",
                    OldValue = logProposalTable.DueDateApprove.HasValue ? logProposalTable.DueDateApprove.Value.ToString("dd/MM/yyyy") : null,
                    NewValue = proposalTable.DueDateApprove.HasValue ? proposalTable.DueDateApprove.Value.ToString("dd/MM/yyyy") : null,
                };
                _logProposalTrackChangesService.Insert(proposalTrackChanges);
            }
            if (proposalTable.DueDateFinalApprove != logProposalTable.DueDateFinalApprove)
            {
                var proposalTrackChanges = new MPS_Log_ProposalTrackChanges()
                {
                    ProposalId = proposalId,
                    ProposalLogId = proposalLogId,
                    RefTableName = refTableName,
                    RefTableDesc = refTableDesc,
                    RefTableId = proposalId,
                    RefLogId = proposalLogId,
                    FieldName = "DueDateFinalApprove",
                    FieldDescription = "Due Date (อนุมัติขั้นสุดท้าย)",
                    State = "Modified",
                    OldValue = logProposalTable.DueDateFinalApprove.HasValue ? logProposalTable.DueDateFinalApprove.Value.ToString("dd/MM/yyyy") : null,
                    NewValue = proposalTable.DueDateFinalApprove.HasValue ? proposalTable.DueDateFinalApprove.Value.ToString("dd/MM/yyyy") : null,
                };
                _logProposalTrackChangesService.Insert(proposalTrackChanges);
            }
        }

        public void ProposalLineTrackChanges(MPS_Log_ProposalTable logProposalTable, MPS_ProposalLine proposalLine, MPS_Log_ProposalLine logProposalLine)
        {
            var proposalId = logProposalTable.ProposalId;
            var proposalLogId = logProposalTable.Id;
            var refTableName = "ProposalLine";
            var refTableDesc = "A&P + Allocation basis";
            if (logProposalLine == null)
            {
                //Added
                var proposalTrackChanges = new MPS_Log_ProposalTrackChanges()
                {
                    ProposalId = proposalId,
                    ProposalLogId = proposalLogId,
                    RefTableName = refTableName,
                    RefTableDesc = refTableDesc,
                    RefTableId = proposalLine.Id,
                    RefLogId = null,
                    FieldName = null,
                    FieldDescription = null,
                    State = "Added",
                    OldValue = null,
                    NewValue = string.Format("APCode: {0}, UnitCode : {1}, BudgetPlan: {2}", proposalLine.APCode, proposalLine.UnitCode, proposalLine.BudgetPlan.ToString("n2"))
                };
                _logProposalTrackChangesService.Insert(proposalTrackChanges);
            }
            else
            {
                //Modified
                if (proposalLine.BudgetPlan != logProposalLine.BudgetPlan)
                {
                    var proposalTrackChanges = new MPS_Log_ProposalTrackChanges()
                    {
                        ProposalId = proposalId,
                        ProposalLogId = proposalLogId,
                        RefTableName = refTableName,
                        RefTableDesc = refTableDesc,
                        RefTableId = proposalLine.Id,
                        RefLogId = logProposalLine.Id,
                        FieldName = "BudgetPlan",
                        FieldDescription = Resources.Proposal.BudgetPlan,
                        State = "Modified",
                        OldValue = logProposalLine.BudgetPlan.ToString("n2"),
                        NewValue = proposalLine.BudgetPlan.ToString("n2")
                    };
                    _logProposalTrackChangesService.Insert(proposalTrackChanges);
                }
            }

        }

        public void IncomeDepositTrackChanges(MPS_Log_ProposalTable logProposalTable, MPS_IncomeDeposit incomeDeposit, MPS_Log_IncomeDeposit logIncomeDeposit)
        {
            var proposalId = logProposalTable.ProposalId;
            var proposalLogId = logProposalTable.Id;
            var refTableName = "IncomeDeposit";
            var refTableDesc = "Income from deposit proposal";
            if (logIncomeDeposit == null)
            {
                //Added
                var proposalTrackChanges = new MPS_Log_ProposalTrackChanges()
                {
                    ProposalId = proposalId,
                    ProposalLogId = proposalLogId,
                    RefTableName = refTableName,
                    RefTableDesc = refTableDesc,
                    RefTableId = incomeDeposit.Id,
                    RefLogId = null,
                    FieldName = null,
                    FieldDescription = null,
                    State = "Added",
                    OldValue = null,
                    NewValue = string.Format("Deposit Income : {0} - {1}, Amount: {2}", incomeDeposit.ProposalDepositRefDoc, incomeDeposit.ProposalDepositRefTitle, incomeDeposit.Budget.ToString("n2"))
                };
                _logProposalTrackChangesService.Insert(proposalTrackChanges);
            }
            //else
            //{
            //    //Modified
            //    if (incomeDeposit.Budget != logIncomeDeposit.Budget)
            //    {
            //        var proposalTrackChanges = new MPS_Log_ProposalTrackChanges()
            //        {
            //            ProposalId = proposalId,
            //            ProposalLogId = proposalLogId,
            //            RefTableName = refTableName,
            //            RefTableDesc = refTableDesc,
            //            RefTableId = incomeDeposit.Id,
            //            RefLogId = logIncomeDeposit.Id,
            //            FieldName = "Budget",
            //            FieldDescription = "Budget",
            //            State = "Modified",
            //            OldValue = logIncomeDeposit.Budget.ToString("n2"),
            //            NewValue = incomeDeposit.Budget.ToString("n2")
            //        };
            //        _logProposalTrackChangesService.Insert(proposalTrackChanges);
            //    }
            //}

        }

        public void IncomeOtherTrackChanges(MPS_Log_ProposalTable logProposalTable, MPS_IncomeOther incomeOther, MPS_Log_IncomeOther logIncomeOther)
        {
            var proposalId = logProposalTable.ProposalId;
            var proposalLogId = logProposalTable.Id;
            var refTableName = "IncomeOther";
            var refTableDesc = "Income from other sources";
            if (logIncomeOther == null)
            {
                //Added
                var proposalTrackChanges = new MPS_Log_ProposalTrackChanges()
                {
                    ProposalId = proposalId,
                    ProposalLogId = proposalLogId,
                    RefTableName = refTableName,
                    RefTableDesc = refTableDesc,
                    RefTableId = incomeOther.Id,
                    RefLogId = null,
                    FieldName = null,
                    FieldDescription = null,
                    State = "Added",
                    OldValue = null,
                    NewValue = string.Format("Income other sources : {0} - {1}, Amount: {2}", incomeOther.SponcorName, incomeOther.Description, incomeOther.Budget.ToString("n2"))
                };
                _logProposalTrackChangesService.Insert(proposalTrackChanges);
            }
            //else
            //{
            //    //Modified
            //    if (incomeOther.Budget != logIncomeOther.Budget)
            //    {
            //        var proposalTrackChanges = new MPS_Log_ProposalTrackChanges()
            //        {
            //            ProposalId = proposalId,
            //            ProposalLogId = proposalLogId,
            //            RefTableName = refTableName,
            //            RefTableDesc = refTableDesc,
            //            RefTableId = incomeOther.Id,
            //            RefLogId = logIncomeOther.Id,
            //            FieldName = "Budget",
            //            FieldDescription = "Budget",
            //            State = "Modified",
            //            OldValue = logIncomeOther.Budget.ToString("n2"),
            //            NewValue = incomeOther.Budget.ToString("n2")
            //        };
            //        _logProposalTrackChangesService.Insert(proposalTrackChanges);
            //    }
            //}

        }

        public void ProposalApprovalTrackChanges(MPS_Log_ProposalTable logProposalTable, MPS_ProposalApproval proposalApproval, MPS_Log_ProposalApproval logProposalApproval)
        {
            var proposalId = logProposalTable.ProposalId;
            var proposalLogId = logProposalTable.Id;
            var refTableName = "ProposalApproval";
            var refTableDesc = "Approve";
            if (logProposalApproval == null)
            {
                //Added
                var proposalTrackChanges = new MPS_Log_ProposalTrackChanges()
                {
                    ProposalId = proposalId,
                    ProposalLogId = proposalLogId,
                    RefTableName = refTableName,
                    RefTableDesc = refTableDesc,
                    RefTableId = proposalApproval.Id,
                    RefLogId = null,
                    FieldName = null,
                    FieldDescription = null,
                    State = "Added",
                    OldValue = null,
                    NewValue = string.Format("Proposal Approve : {0} , {1}", proposalApproval.ApproverLevel, proposalApproval.ApproverFullName)
                };
                _logProposalTrackChangesService.Insert(proposalTrackChanges);
            }


        }

        #endregion

        public bool CheckStartNewProcess(MPS_ProposalTable proposalTable)
        {
            try
            {
                var result = false;
                if (proposalTable == null)
                    return false;
                var logProposal = _logProposalService.Find(m => m.ProposalId == proposalTable.Id);
                if (logProposal == null)
                    return false;
                if (logProposal.TotalBudget != proposalTable.TotalBudget)
                    return true;
                var logProposalLine = logProposal.ProposalLine;
                if (logProposalLine == null)
                    return false;
                var proposalLine = proposalTable.ProposalLine;
                var listExpenseTopic = proposalLine.Select(m => m.ExpenseTopicCode).Distinct();
                foreach (var expense in listExpenseTopic)
                {
                    var alreadyExpense = logProposalLine.Where(m => m.ExpenseTopicCode.Equals(expense)).ToListSafe();
                    if (alreadyExpense == null)
                        return true;

                    var logAmount = alreadyExpense.Sum(m => m.BudgetPlan);
                    var amount =
                        proposalLine.Where(m => m.ExpenseTopicCode.Equals(expense)).Sum(m => m.BudgetPlan);
                    result = amount != logAmount;
                    if (result)
                    {
                        return true;
                    }

                }
                var incomeDeoposit = proposalTable.IncomeDeposit;
                if (incomeDeoposit.Any(m => m.StatusFlag == 0))
                {
                    result = true;
                }
                var incomeOtherSource = proposalTable.IncomeOther;
                if (incomeOtherSource.Any(m => m.StatusFlag == 0))
                {
                    result = true;
                }
                if (!result)
                {
                    var proposalApprovals = proposalTable.ProposalApproval.ToList();
                    foreach (var proposalApproval in proposalApprovals)
                    {
                        var logProposalApproval = logProposal.ProposalApproval.LastOrDefault(m => m.ProposalApprovalId == proposalApproval.Id);
                        ProposalApprovalCancel(proposalApproval, logProposalApproval);
                        _session.SaveChanges();
                    }
                    ////var modelApprove = _proposalApprovalService.GetWhere(m => m.ParentId == proposalTable.Id);
                    ////foreach (var model in modelApprove)
                    ////{
                    ////    model.ApproveStatus = true;
                    ////    _proposalApprovalService.Update(model, model.Id);
                    ////    _session.SaveChanges();
                    ////} 
                }
                return result;
            }
            catch (Exception)
            {

                throw new Exception(string.Format(Resources.Error.DataWithIDNotExists, proposalTable.DocumentNumber));
            }

        }

        #region Cancel Revise
        public bool CancelRevise(int proposalId,bool createLog = true)
        {
            var result = false;
            var proposalTable = _proposalService.Find(proposalId);
            if (proposalTable != null)
            {
                if (createLog)
                {
                    proposalTable.StatusFlag = DocumentStatusFlagId.Reject;
                    proposalTable.Status = "Rejected";
                    proposalTable.DocumentStatus = "Cancelled";

                    CreateLog(proposalTable, proposalTable.Revision ?? 1, false);
                }
                

                //Get Last Log
                var logProposalTable = _logProposalService.LastOrDefault(m => m.ProposalId == proposalId);
                if (logProposalTable == null)
                    throw new Exception("Proposal Log could not be found.");

                //Log - ProposalTable
                ProposalTableCancel(proposalTable, logProposalTable);

                //Log - ProposalLine
                var proposalLines = proposalTable.ProposalLine.ToList();
                foreach (var proposalLine in proposalLines)
                {
                    var logProposalLine = logProposalTable.ProposalLine.LastOrDefault(m => m.ProposalLineId == proposalLine.Id);

                    //if (logProposalLine != null)
                    //{
                        //Log - ProposalLineTemplate
                        var proposalLineTemplates = proposalLine.ProposalLineTemplates.ToList();
                        foreach (var proposalLineTemplate in proposalLineTemplates)
                        {
                            var logProposalLineTemplate = (logProposalLine != null) ? logProposalLine.ProposalLineTemplates.LastOrDefault(m => m.ProposalLineTemplateId == proposalLineTemplate.Id) : null;
                            ProposalLineTemplateCancel(proposalLineTemplate, logProposalLineTemplate);
                        }
                    //}

                    ProposalLineCancel(proposalLine, logProposalLine);

                }

                //Log - IncomeDeposit
                var incomeDeposits = proposalTable.IncomeDeposit.ToList();
                foreach (var incomeDeposit in incomeDeposits)
                {
                    var logIncomeDeposit = logProposalTable.IncomeDeposit.LastOrDefault(m => m.IncomeDepositId == incomeDeposit.Id);
                    IncomeDepositCancel(incomeDeposit, logIncomeDeposit);
                }

                //Log - IncomeOther
                var incomeOthers = proposalTable.IncomeOther.ToList();
                foreach (var incomeOther in incomeOthers)
                {
                    var logIncomeOther = logProposalTable.IncomeOther.LastOrDefault(m => m.IncomeOtherId == incomeOther.Id);
                    IncomeOtherCancel(incomeOther, logIncomeOther);
                }

                //Log - ProposalApproval
                var proposalApprovals = proposalTable.ProposalApproval.ToList();
                foreach (var proposalApproval in proposalApprovals)
                {
                    var logProposalApproval = logProposalTable.ProposalApproval.LastOrDefault(m => m.ProposalApprovalId == proposalApproval.Id);
                    ProposalApprovalCancel(proposalApproval, logProposalApproval);
                }
                _session.SaveChanges();
                result = true;
            }
            else
            {
                throw new Exception(string.Format(Resources.Error.DataWithIDNotExists, proposalId));
            }
            return result;
        }

        public void ProposalTableCancel(MPS_ProposalTable proposalTable, MPS_Log_ProposalTable logProposalTable)
        {
            var updateProposalTable = _logProposalMapper.ToProposalTable(logProposalTable);
            var revision = proposalTable.Revision - 1;
            updateProposalTable.Revision = revision != 1 ? revision : null;
            updateProposalTable = _proposalService.Update(updateProposalTable, updateProposalTable.Id);
        }

        public void ProposalLineCancel(MPS_ProposalLine proposalLine, MPS_Log_ProposalLine logProposalLine)
        {
            if (logProposalLine == null)
            {
                //delete
                _proposalLineService.Delete(proposalLine);
            }
            else
            {
                //Modified
                var updateProposalLine = _logProposalMapper.ToProposalLine(logProposalLine);
                updateProposalLine.ParentId = proposalLine.ParentId;
                if (updateProposalLine.Id > 0)
                    updateProposalLine = _proposalLineService.Update(updateProposalLine, updateProposalLine.Id);
            }
        }

        public void ProposalLineTemplateCancel(MPS_ProposalLineTemplate proposalLineTemplate, MPS_Log_ProposalLineTemplate logProposalLineTemplate)
        {
            if (logProposalLineTemplate == null)
            {
                //delete
                _proposalLineTemplateService.Delete(proposalLineTemplate);
            }
            else
            {
                //Modified
                var updateProposalLineTemplate = _logProposalMapper.ToProposalLineTemplate(logProposalLineTemplate);
                updateProposalLineTemplate.ProposalLineId = proposalLineTemplate.ProposalLineId;
                if (updateProposalLineTemplate.Id > 0)
                    updateProposalLineTemplate = _proposalLineTemplateService.Update(updateProposalLineTemplate, updateProposalLineTemplate.Id);
            }
        }

        public void IncomeDepositCancel(MPS_IncomeDeposit incomeDeposit, MPS_Log_IncomeDeposit logIncomeDeposit)
        {
            if (logIncomeDeposit == null)
            {
                //delete
                _proposalIncomeDepositService.Delete(incomeDeposit);
            }
        }

        public void IncomeOtherCancel(MPS_IncomeOther incomeOther, MPS_Log_IncomeOther logIncomeOther)
        {
            if (logIncomeOther == null)
            {
                //delete
                _proposalIncomeOtherService.Delete(incomeOther);
            }
        }

        public void ProposalApprovalCancel(MPS_ProposalApproval proposalApproval, MPS_Log_ProposalApproval logProposalApproval)
        {
            if (logProposalApproval == null)
            {
                //delete
                _proposalApprovalService.Delete(proposalApproval);
            }
            else
            {
                //Modified
                var updateProposalApproval = _logProposalMapper.ToProposalApproval(logProposalApproval);
                updateProposalApproval.ParentId = proposalApproval.ParentId;
                if (updateProposalApproval.Id > 0)
                    updateProposalApproval = _proposalApprovalService.Update(updateProposalApproval, updateProposalApproval.Id);
            }
        }

        #endregion
    }
}