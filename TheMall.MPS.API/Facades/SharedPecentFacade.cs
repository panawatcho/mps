﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using CrossingSoft.Framework.Infrastructure.Session;
using TheMall.MPS.API.Facades.Abstracts;
using TheMall.MPS.API.Mappers;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.API.Services;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Models.Master;
using TheMall.MPS.ViewModels.Master;

namespace TheMall.MPS.API.Facades
{
    public interface ISharedPercentFacade : IBaseMasterFacade<MPS_M_SharedPercent, SharedPercentViewModel>
    {

    }
    public class SharedPercentFacade : BaseMasterFacade<MPS_M_SharedPercent, SharedPercentViewModel>, ISharedPercentFacade
    {
        private readonly IMPSSession _session;
        private readonly ISharedPercentService _service;
        private readonly IAllocateBasisService _allocateService;

        public SharedPercentFacade(IMPSSession session, ISharedPercentService service,IAllocateBasisService allocateService)
            : base(session, service)
        {
            _session = session;
            _service = service;
            _allocateService = allocateService;
        }

        public override async Task<SharedPercentViewModel> Create(SharedPercentViewModel viewModel)
        {
            try
            {
                var model = _service.Insert(viewModel.ToModel());
                await _session.SaveChangesAsync();
                return model.ToViewModel();
            }
            catch (Exception exception)
            {

                throw new Exception("Cannot insert duplicate code");
            }
        }

        public override async Task<SharedPercentViewModel> Update(SharedPercentViewModel viewModel)
        {
            var model = _service.Update(viewModel.ToModel());
            await _session.SaveChangesAsync();
            return model.ToViewModel();
        }

        public override async Task<SharedPercentViewModel> Delete(SharedPercentViewModel viewModel)
        {
            try
            {
                _service.Delete(viewModel.ToModel());
                await _session.SaveChangesAsync();
                return viewModel;
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
        }
       

    }
}