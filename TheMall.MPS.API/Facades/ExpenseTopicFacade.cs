﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using CrossingSoft.Framework.Infrastructure.Session;
using TheMall.MPS.API.Facades.Abstracts;
using TheMall.MPS.API.Mappers;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.API.Services;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Models.Master;
using TheMall.MPS.ViewModels.Master;

namespace TheMall.MPS.API.Facades
{
    public interface IExpenseTopicFacade : IBaseMasterFacade<MPS_M_ExpenseTopic, ExpenseTopicViewModel>
    {

        Task<ExpenseTopicViewModel> GetViewModel(string code);
        Task<IEnumerable<MPS_M_ExpenseTopic>> GetAllAsyns();
        Task<IEnumerable<ExpenseTopicViewModel>> Search(string code = null, string name = null);
        Task<MPS_M_ExpenseTopic> GetAsyns(string code);

        Task<ExpenseTopicViewModel> Save(ExpenseTopicViewModel viewModel);
    }

    public class ExpenseTopicFacade : BaseMasterFacade<MPS_M_ExpenseTopic, ExpenseTopicViewModel>, IExpenseTopicFacade
    {
        private readonly IMPSSession _session;
        private readonly IExpenseTopicService _service;
        private readonly IAPcodeService _serviceAPcode;

        public ExpenseTopicFacade(IMPSSession session, IExpenseTopicService service,IAPcodeService serviceAPcode) 
            : base(session, service)
        {
            _session = session;
            _service = service;
            _serviceAPcode = serviceAPcode;
        }

        public override async Task<ExpenseTopicViewModel> Create(ExpenseTopicViewModel viewModel)
        {
            try
            {
                var model = viewModel.ToModel();
        //        var model = _service.Insert(viewModel.ToModel());
                _service.Insert(model);
                if (viewModel.APCode != null)
                {
                    var apCodeModel = viewModel.APCode.ToModel();
                    foreach (var line in apCodeModel)
                    {
                        if (line.ExpenseTopicCode == null)
                        {
                            line.ExpenseTopicCode = model.ExpenseTopicCode;
                            _serviceAPcode.Insert(line);
                        }
                    }
                }
                await Session.SaveChangesAsync();
                return model.ToViewModel();
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
        }

        public override async Task<ExpenseTopicViewModel> Update(ExpenseTopicViewModel viewModel)
        {
            try
            {
                var model = viewModel.ToModel();
                _service.Update(viewModel.ToModel());
                if (viewModel.APCode != null)
                {
                    var apCodeModel = viewModel.APCode.ToModel();
                    foreach (var line in apCodeModel)
                    {
                        if (line.ExpenseTopicCode != null)
                        {
                            //line.ExpenseTopicCode = model.ExpenseTopicCode;
                            _serviceAPcode.Update(line);
                        }
                        else
                        {
                            if (line.ExpenseTopicCode == null)
                            {
                                line.ExpenseTopicCode = model.ExpenseTopicCode;
                                _serviceAPcode.Insert(line);
                            }
                        }
                    }
                }
                await Session.SaveChangesAsync();
                return model.ToViewModel();
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
        }

  

        public override async Task<ExpenseTopicViewModel> Delete(ExpenseTopicViewModel viewModel)
        {
            try
            {
                _service.Delete(viewModel.ToModel());
                await _session.SaveChangesAsync();
                return viewModel;
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
        }
        public  Task<IEnumerable<MPS_M_ExpenseTopic>> GetAllAsyns()
        {
          
            return _service.GetAllAsyns();
        }
        public Task<MPS_M_ExpenseTopic> GetAsyns(string code)
        {

            return _service.GetAsync(code);
        }

        public async Task<ExpenseTopicViewModel> Save(ExpenseTopicViewModel viewModel)
        {
            //try
            //{
                if (viewModel == null)
                    return null;
                var model = viewModel.ToModel();
                var modelExpense = _service.Find(model.ExpenseTopicCode);
                if (modelExpense != null)
                {
                    model = _service.Update(model,model.ExpenseTopicCode);
                    
                }
                else
                {
                    model = _service.Insert(model);
                }
          


                var apViewModels = new List<APCodeViewModel>();
                if (viewModel.APCode != null)
                {

                    foreach (var ap in viewModel.APCode.ToModel())
                    {
                        var apModel = _serviceAPcode.Find(ap.APCode);

                        if (apModel != null)
                        {

                            apViewModels.Add(_serviceAPcode.Update(ap,ap.APCode).ToViewModel());
                            await _session.SaveChangesAsync();

                        }
                        else
                        {
                            apViewModels.Add(_serviceAPcode.Insert(ap).ToViewModel());
                            await _session.SaveChangesAsync();
                        }

                    }
                }
                await _session.SaveChangesAsync();
                var result = model.ToViewModel();
                result.APCode = apViewModels;

            return result;

        }



        public async Task<ExpenseTopicViewModel> GetViewModel(string expenseTopicCode)
        {
            var model = _service.Find(expenseTopicCode);
            var apmodel = _serviceAPcode.GetWhere(m => m.ExpenseTopicCode == expenseTopicCode);
            var viewModel = model.ToViewModel();
            viewModel.APCode = apmodel.ToViewModels();
            return   viewModel;
        }

        public async Task<IEnumerable<ExpenseTopicViewModel>> Search(string code = null, string name = null)
        {
            var model = _service.Search(code, name).Result;

            return model.ToViewModels();
        }

    }
}