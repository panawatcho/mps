﻿using System.Web.Http;

namespace TheMall.MPS.API.Controllers
{
    [RoutePrefix("api/ping")]
    public class PingController : ApiController
    {
        [Authorize]
        public IHttpActionResult Get()
        {
            return Ok("pong");
        }

        [Authorize]
        public IHttpActionResult Post()
        {
            return Ok("pong");
        }

        [AllowAnonymous]
        [Route("anonymous")]
        public IHttpActionResult GetAnonymous()
        {
            return Ok("pong");
        }

        [AllowAnonymous]
        [Route("anonymous")]
        public IHttpActionResult PostAnonymous()
        {
            return Ok("pong");
        }
    }
}