﻿using System.Threading.Tasks;
using System.Web.Http;
using TheMall.MPS.API.Facades;

namespace TheMall.MPS.API.Controllers
{
    [RoutePrefix("api/task")]
    public class TaskController : ApiController
    {
        private readonly ITaskFacade _taskFacade;

        public TaskController(ITaskFacade taskFacade)
        {
            _taskFacade = taskFacade;
        }

        [Route("folio")]
        public async Task<IHttpActionResult> GetFolio(int? procInstId = null, string folio = null)
        {
            return Ok(await _taskFacade.GetFolioAsync(procInstId, folio));
        }
    }
}