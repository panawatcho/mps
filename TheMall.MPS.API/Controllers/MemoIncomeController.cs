﻿using System.Threading.Tasks;
using System.Web.Http;
using TheMall.MPS.API.Controllers.Abstracts;
using TheMall.MPS.API.Facades;
using TheMall.MPS.API.Mappers;
using TheMall.MPS.API.Services;
using TheMall.MPS.Models.Enums;
using TheMall.MPS.Models.MemoIncome;
using TheMall.MPS.ViewModels.MemoIncome;

namespace TheMall.MPS.API.Controllers
{
    [Attributes.AllowAuthenticated]
    [RoutePrefix("api/memoincome")]
    public class MemoIncomeController : BaseMpsWorkflowController<MPS_MemoIncomeTable, MPS_MemoIncomeAttachment, MPS_MemoIncomeComment, MemoIncomeViewModel>
    {
        private readonly IMemoIncomeFacade _facade;
        private readonly IEmployeeTableService _employee;
        private readonly IFinalApproverService _finalApproverService;
        private readonly ITaskFacade _taskFacade;

        public MemoIncomeController(IMemoIncomeFacade facade,
            IEmployeeTableService employee, 
            IFinalApproverService finalApproverService,
            ITaskFacade taskFacade)
            : base(facade)
        {
            _facade = facade;
            _employee = employee;
            _finalApproverService = finalApproverService;
            _taskFacade = taskFacade;
        }

        //
        [Route("{id}")]
        [HttpGet]
        public async Task<IHttpActionResult> Get( int id)
        {
            return Ok(await _facade.GetViewModel(Url,id));
        }

        [Route("")]
        [HttpPost]
        public async Task<IHttpActionResult> Post(MemoIncomeViewModel viewModel)
        {
            var model = await _facade.SaveAsync(viewModel);

            return Ok(model);
        }

        //
        [Route("{id}/startprocess")]
        [HttpPost]
        public async Task<IHttpActionResult> StartProcess(int id/*, string username*/)
        {
            var procInstId = await _facade.StartProcess(id);

            return Ok(procInstId);
        }

        //
        [Route("getfinalapprover/{budget}/{id}/{unitcode}")]
        [HttpGet]
        public IHttpActionResult GetFinalApprover(decimal budget, int id, string unitcode)
        {
            string process = "memoincome";
            if (id > 0)
            {
                _facade.SetFinalApproverDeleted(id);
            }
            //string memoType = type;
            //if (memoType.ToLower() != "normal")
            //{
            //    process = String.Concat(process, memoType).Trim();
            //}
            //var finalApprover = _finalApproverService.GetFinalApprover(budget, process , unitcode).ToMemoApproval() ??
            //                    _finalApproverService.GetFinalApprover(budget, "memoincome", unitcode).ToMemoApproval();
            var finalApprover = _finalApproverService.GetFinalApprover(budget, Budget.DocumentType.MemoIncome, unitcode).ToPettyCashApproval();

            return Ok(finalApprover);
        }

        //
        [Route("deleteby/{id}/{username}")]
        [HttpGet]
        public IHttpActionResult DeleteBy(int id, string username)
        {
            var result = _facade.DeleteBy(id, username);

            return Ok(result); ;
        }

        // search
        [Route("getproposalref/")]
        [HttpGet]
        public IHttpActionResult GetProposalRef(string code = "", string name = "", string unitCode = "", string category = "")
        {
            var viewModel = _facade.GetProposalRef(code, name, unitCode, category);
       
            return Ok(viewModel);
        }

        //
        [Route("accindex/{username}")]
        [HttpGet]
        public IHttpActionResult GetMemoIncomeforAccount(string username)
        {
            var viewModel = _facade.GetMemoIncomeforAccount(username);

            return Ok(viewModel);
        }
    }
}

