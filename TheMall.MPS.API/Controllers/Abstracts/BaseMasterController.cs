﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using CrossingSoft.Framework.Infrastructure;
using TheMall.MPS.API.Facades.Abstracts;
using TheMall.MPS.Models.Interfaces;
using TheMall.MPS.ViewModels.Interfaces;

namespace TheMall.MPS.API.Controllers.Abstracts
{
    public abstract class BaseMasterController<TMasterTable, TMasterViewModel> : BaseApiController
        where TMasterTable : new()
        where TMasterViewModel : IMasterViewModel, new()
    {
        protected IBaseMasterFacade<TMasterTable, TMasterViewModel> Facade;
        protected BaseMasterController(
            IBaseMasterFacade<TMasterTable, TMasterViewModel> facade
        )
        {
            Facade = facade;
        }
        [HttpPost]
        [Route("Create")]
        public async Task<IHttpActionResult> Post(TMasterViewModel viewModel)
        {
            return Ok(await Facade.Create(viewModel));
        }
        [HttpPost]
        [Route("Update")]
        public async Task<IHttpActionResult> Update(TMasterViewModel viewModel)
        {
            return Ok(await Facade.Update(viewModel));
        }
        [HttpPost]
        [Route("Delete")]
        public async Task<IHttpActionResult> Delete(TMasterViewModel viewModel)
        {
            await Facade.Delete(viewModel);
            return Ok(viewModel);
        }
    }
}
