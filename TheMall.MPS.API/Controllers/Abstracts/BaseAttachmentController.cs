﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using TheMall.MPS.API.Attributes;
using TheMall.MPS.API.Controllers.Abstracts;
using TheMall.MPS.API.Facades.Interfaces;
using TheMall.MPS.Models.Abstracts;
using TheMall.MPS.Resources;



namespace TheMall.MPS.API.Controllers.Abstracts
{
    public abstract class BaseAttachmentController<TAttchmentTable, TPostedAttachmentViewModel> : BaseApiController
        where TAttchmentTable : BaseAttachmentTable
        where TPostedAttachmentViewModel : ViewModels.IPostedAttachmentViewModel
    {
        protected IMpsAttachmentFacade<TAttchmentTable> Facade;
        protected BaseAttachmentController(IMpsAttachmentFacade<TAttchmentTable> facade)
        {
            Facade = facade;
        }

        [AllowAuthenticated]
        [Route("{id}/attachment")]
        [HttpGet]
        public async Task<HttpResponseMessage> GetAttachment(string id, string fileName, bool inline = false)
        {
            var attachment = Facade.GetAttachment(id, fileName);
            if (attachment == null) return new HttpResponseMessage(HttpStatusCode.NotFound);

            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
            //var stream = new FileStream(attachment.DownloadUrl, FileMode.Open);
            //var content = new StreamContent(stream);
            //await content.LoadIntoBufferAsync();
            var content = await Facade.GetStreamContent(attachment);
            result.Content = content;
            if (!string.IsNullOrEmpty(attachment.ContentType))
            {
                result.Content.Headers.ContentType = new MediaTypeHeaderValue(attachment.ContentType);
            }
            else
            {
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
            }
            result.Content.Headers.ContentDisposition =
                new ContentDispositionHeaderValue(inline ? "inline" : "attachment")
                {
                    FileName = attachment.FileName
                };

            return result;

        }

        [AllowAuthenticated]
        [Route("{id}/attachment/type/{documentType}")]
        [HttpGet]
        public async Task<HttpResponseMessage> GetAttachment(string id, int documentType, string fileName, bool inline = false)
        {
            var attachment = Facade.GetAttachment(id, fileName, documentType);
            var content = await Facade.GetStreamContent(attachment);

            return CreateAttachmentResponse(content, attachment, inline);
            
        }

        protected HttpResponseMessage CreateAttachmentResponse(StreamContent content, TAttchmentTable attachment, bool inline)
        {
            if (content == null || attachment == null) return new HttpResponseMessage(HttpStatusCode.NotFound);

            return CreateAttachmentResponse(content, attachment.ContentType, attachment.FileName, inline);
        }

        protected HttpResponseMessage CreateAttachmentResponse(StreamContent content, string contentType, string fileName, bool inline)
        {
            if (content == null) return new HttpResponseMessage(HttpStatusCode.NotFound);

            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
            result.Content = content;
            if (!string.IsNullOrEmpty(contentType))
            {
                result.Content.Headers.ContentType = new MediaTypeHeaderValue(contentType);
            }
            else
            {
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
            }
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue(inline ? "inline" : "attachment")
            {
                FileName = fileName
            };
            return result;
        }

        [Route("{id}/attachment")]
        [HttpPost]
        public async Task<IHttpActionResult> PostAttachment([FromUri] TPostedAttachmentViewModel viewModel)
        {
            return await this.SaveAsync(viewModel, HttpContext.Current.User.Identity.Name);
        }

        [AllowAuthenticated]
        [Route("{id}/attachment")]
        [HttpDelete]
        public async Task<IHttpActionResult> DeleteAttachment(int? id, string fileName, int? documentType = null, string sn = null, string documentNumber = null)
        {
            return await this.Remove(id, documentType, sn, documentNumber, fileName);
        }

        //protected virtual async Task<IHttpActionResult> SaveAsync(int? id, string username, string name, string title, int? documentType, string sn, string documentNumber)
        protected virtual async Task<IHttpActionResult> SaveAsync(TPostedAttachmentViewModel viewModel, string username)
        {
            if (!this.Request.Content.IsMimeMultipartContent())
            {
                return StatusCode(HttpStatusCode.UnsupportedMediaType);
            }
            var headers = Request.Content.Headers;

            string serverFilePath = null;
            try
            {
                var provider = new MultipartFormDataStreamProvider(System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data"));
                var multipart = await Request.Content.ReadAsMultipartAsync(provider);
                var file = multipart.FileData.First();
                var contentType = file.Headers.ContentType.MediaType;
                serverFilePath = file.LocalFileName;

                using (FileStream sourceStream = File.Open(serverFilePath, FileMode.Open))
                {
                    var result =
                        await
                        Facade.SaveAttachmentAsync(headers, contentType, sourceStream, username, viewModel);
                    if (result != null)
                    {
                        return Ok(result.Id);
                    }
                    else
                    {
                        return InternalServerError(new Exception(Error.UnknownError));
                    }
                    return Ok(true);
                }

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                try
                {
                    if (serverFilePath != null)
                    {
                        File.Delete(serverFilePath);
                    }
                }
                catch (Exception)
                {
                }
            }

        }

        protected virtual async Task<IHttpActionResult> Remove(int? id, int? documentType, string sn, string documentNumber, params string[] fileNames)
        {
            var result = await Facade.RemoveAttachments(id ?? 0, documentType, sn, documentNumber, fileNames);
            if (result)
            {
                return Ok(true);
            }
            else
            {
                return InternalServerError(new Exception(Error.UnknownError));
            }
        }



    }

    public abstract class BaseAttachmentController<TAttchmentTable> : BaseAttachmentController<TAttchmentTable, ViewModels.PostedAttachmentViewModel>
        where TAttchmentTable : BaseAttachmentTable
    {
        protected BaseAttachmentController(IMpsAttachmentFacade<TAttchmentTable> facade)
            : base(facade)
        {
        }
    }
}