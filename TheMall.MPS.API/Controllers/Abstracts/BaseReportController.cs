﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using TheMall.MPS.API.Services;
using TheMall.MPS.Reports.Interfaces;

namespace TheMall.MPS.API.Controllers.Abstracts
{
    public abstract class BaseReportController : ApiController
    {
        private readonly IReportService _reportService;

        protected BaseReportController(IReportService reportService)
        {
            _reportService = reportService;
        }

        protected HttpResponseMessage GenerateReportStream(IReportCriteria criteria)
        {
            MemoryStream ms = null;
            try
            {
                _reportService.Source = ReportSource.Stream;

                ms = _reportService.GetReportStream(criteria);

                var contentType = _reportService.ContentType;
                var zipping = _reportService.Zipping;
                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StreamContent(ms);
                string fileName = string.Format("{0}.{1}", _reportService.FileName, _reportService.FileExtension);
                var encodedFilename = System.Web.HttpUtility.UrlEncode(fileName, System.Text.Encoding.UTF8);
                //response.Content.Headers.Add("X-File-Name", fileName);
                response.Content.Headers.Add("X-File-Name", Uri.EscapeDataString(encodedFilename));
                if (!zipping)
                {
                    response.Content.Headers.ContentType = new MediaTypeHeaderValue(contentType);
                    response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue(criteria.Inline ? "inline" : "attachment")
                    {
                        FileName = fileName,
                        FileNameStar = Uri.EscapeDataString(fileName)
                    };
                }
                else
                {
                    
                    response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                    response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                    {
                        FileName = fileName,
                        FileNameStar = Uri.EscapeDataString(fileName)
                    };
                }
                return response;
            }
            catch (Exception)
            {
                if (ms != null)
                {
                    ms.Dispose();
                }
                throw;
            }
        }

        protected HttpResponseMessage GenerateReportStreamAndSave(IReportCriteria criteria)
        {
            MemoryStream ms = null;
            try
            {
                _reportService.Source = ReportSource.Stream;
                //
                ms = _reportService.GetReportStreamAndSave(criteria);
                //
                var contentType = _reportService.ContentType;
                var zipping = _reportService.Zipping;
                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StreamContent(ms);
                string fileName = string.Format("{0}.{1}", _reportService.FileName, _reportService.FileExtension);
                var encodedFilename = System.Web.HttpUtility.UrlEncode(fileName, System.Text.Encoding.UTF8);

                response.Content.Headers.Add("X-File-Name", Uri.EscapeDataString(encodedFilename));
                if (!zipping)
                {
                    response.Content.Headers.ContentType = new MediaTypeHeaderValue(contentType);
                    response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue(criteria.Inline ? "inline" : "attachment")
                    {
                        FileName = fileName,
                        FileNameStar = Uri.EscapeDataString(fileName)
                    };
                }
                else
                {

                    response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                    response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                    {
                        FileName = fileName,
                        FileNameStar = Uri.EscapeDataString(fileName)
                    };
                }
                return response;
            }
            catch (Exception)
            {
                if (ms != null)
                {
                    ms.Dispose();
                }
                throw;
            }
        }
    }
}