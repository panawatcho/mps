﻿using System.Web.Http;
using TheMall.MPS.ViewModels.Interfaces;

namespace TheMall.MPS.API.Controllers.Abstracts
{
    public abstract class BaseApiController : ApiController
    {
        protected System.Web.Http.Results.CreatedAtRouteNegotiatedContentResult<T> CreatedAtDefaultRoute<T>(T content)
            where T : IViewModel
        {
            var routeValues = new
            {
                //controller = this.GetType().Name.Replace("Controller", ""),
                id = content.Id
            };
            return CreatedAtRoute(WebApiConfig.DefaultApiRoute, routeValues, content);
        }
    }
}