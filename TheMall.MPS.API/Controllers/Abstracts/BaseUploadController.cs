﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;
using TheMall.MPS.API.Facades.Interfaces;
using TheMall.MPS.Models.Interfaces;
using TheMall.MPS.ViewModels;

namespace TheMall.MPS.API.Controllers.Abstracts
{
    public abstract class BaseUploadController<TAttachmentTable> : ApiController
        where TAttachmentTable : IAttachmentTable, new()
    {
        private readonly IHaveAttachmentFacade<TAttachmentTable> _attachmentFacade;
        protected BaseUploadController(IHaveAttachmentFacade<TAttachmentTable> attachmentFacade)
        {
            _attachmentFacade = attachmentFacade;
        }

        private readonly JavaScriptSerializer _js = new JavaScriptSerializer { MaxJsonLength = 41943040 };

        #region Get
        [Route("{id}/attachment")]
        public virtual IHttpActionResult Get(string id)
        {
            return Ok(_attachmentFacade.GetAllAttachments(id, Url));
        }

        //[Route("{id}/attachment/{attachmentId}", Name = "DownloadAttachmentNormal")]
        [Route("{id}/attachment/{attachmentId}")]
        public async virtual Task<HttpResponseMessage> Get(string id, int attachmentId, bool inline = false)
        {
            if (attachmentId <= 0)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
            return await DownloadFileContent(id, attachmentId, inline);
        }
        private async Task<HttpResponseMessage> DownloadFileContent(string parentId, int attachmentId, bool inline)
        {
            var attachment = _attachmentFacade.GetAttachment(attachmentId);
            if (attachment == null) return new HttpResponseMessage(HttpStatusCode.NotFound);

            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);

            var content = await _attachmentFacade.GetStreamContent(attachment);
            result.Content = content;
            if (!string.IsNullOrEmpty(attachment.ContentType))
            {
                result.Content.Headers.ContentType = new MediaTypeHeaderValue(attachment.ContentType);
            }
            else
            {
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
            }
            result.Content.Headers.ContentDisposition =
                new ContentDispositionHeaderValue(inline ? "inline" : "attachment")
                {
                    FileName = attachment.FileName
                };
            var encodedFilename = System.Web.HttpUtility.UrlEncode(attachment.FileName, System.Text.Encoding.UTF8);
            result.Content.Headers.Add("X-File-Name", Uri.EscapeDataString(encodedFilename));
            return result;
        }
        #endregion

        #region Post & Put
        [Route("{id}/attachment")]
        public virtual HttpResponseMessage Post(string id)
        {
            return UploadFile(HttpContext.Current, id);
        }

        [Route("{id}/attachment")]
        public virtual HttpResponseMessage Put(string id)
        {
            return UploadFile(HttpContext.Current, id);
        }

        private HttpResponseMessage UploadFile(HttpContext context, string id)
        {
            if (!this.Request.Content.IsMimeMultipartContent())
            {
                return new HttpResponseMessage(HttpStatusCode.UnsupportedMediaType);
            }

            var viewModels = _attachmentFacade.UploadFile(this.Request, context, id, Url);

            return WriteJsonIframeSafe(context, viewModels);
        }

        private HttpResponseMessage WriteJsonIframeSafe(HttpContext context, IEnumerable<AttachmentViewModel> statuses)
        {
            context.Response.AddHeader("Vary", "Accept");
            var response = new HttpResponseMessage()
            {
                Content = new StringContent(_js.Serialize(statuses.ToArray()))
            };
            if (context.Request["HTTP_ACCEPT"].Contains("application/json"))
            {
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            }
            else
            {
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
            }
            return response;
        }
        #endregion

        #region Delete

        //[Route("{id}/attachment/{attachmentId}", Name = "DeleteAttachmentNormal")]
        [Route("{id}/attachment/{attachmentId}")]
        public async virtual Task<HttpResponseMessage> Delete(string id, int attachmentId)
        {
            var deleted = await _attachmentFacade.RemoveAttachments(id, attachmentId);
            return ControllerContext.Request.CreateResponse(deleted ? HttpStatusCode.OK : HttpStatusCode.InternalServerError, deleted);
        }
        #endregion
    }

}