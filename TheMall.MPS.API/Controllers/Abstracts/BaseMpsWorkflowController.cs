﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using TheMall.MPS.API.Facades.Interfaces;
using TheMall.MPS.API.Mappers;
using TheMall.MPS.Models.Abstracts;
using TheMall.MPS.Models.Interfaces;
using TheMall.MPS.Resources;
using TheMall.MPS.ViewModels;
using TheMall.MPS.ViewModels.Abstracts;
using TheMall.MPS.ViewModels.Interfaces;

namespace TheMall.MPS.API.Controllers.Abstracts
{
    public abstract class BaseMpsWorkflowController<TWorkflowTable, TAttachmentTable, TCommentTable, TWorkflowViewModel,
        TPostedAttachment>
        //: Abstracts.BasePRCApiController
        : BaseAttachmentController<TAttachmentTable, TPostedAttachment>
        where TWorkflowTable : IMpsWorkflowTable
        where TAttachmentTable : BaseAttachmentTable
        where TCommentTable : ICommentTable
        where TWorkflowViewModel : IMpsWorkflowViewModel
        where TPostedAttachment : IPostedAttachmentViewModel
    {
        protected new readonly IMpsWorkflowFacade<TWorkflowTable, TAttachmentTable, TCommentTable, TWorkflowViewModel> Facade;

        protected BaseMpsWorkflowController(
            IMpsWorkflowFacade<TWorkflowTable, TAttachmentTable, TCommentTable, TWorkflowViewModel> facade
            )
            : base(facade)
        {
            Facade = facade;
        }

        [Route("Worklist/{sn}")]
        public virtual async Task<IHttpActionResult> GetWorklist(string sn)
        {
            if (string.IsNullOrEmpty(sn))
            {
                return BadRequest(Error.NoSN);
            }
            var viewModel = await Facade.GetWorklist(Url, sn);

            return Ok(viewModel);
        }

        [Route("Worklist/{sn}")]
        public virtual async Task<IHttpActionResult> PostWorklist(string sn, string action, TWorkflowViewModel viewModel)
        {
            if (string.IsNullOrEmpty(sn))
            {
                return BadRequest(Error.NoSN);
            }
            if (string.IsNullOrEmpty(action))
            {
                return BadRequest(Error.NoAction);
            }

            var commentTable = await Facade.Submit(sn, action, viewModel.Comment, viewModel);
            var commentViewModel = commentTable.ToViewModel();
            return Ok(commentViewModel);
        }

       
      
    }
    public abstract class BaseMpsWorkflowController<TWorkflowTable, TAttachmentTable, TCommentTable, TWorkflowViewModel>
           : BaseMpsWorkflowController
                   <TWorkflowTable, TAttachmentTable, TCommentTable, TWorkflowViewModel, PostedAttachmentViewModel>
        where TWorkflowTable : MpsWorkflowTable
        where TAttachmentTable : BaseAttachmentTable
        where TCommentTable : BaseCommentTable
        where TWorkflowViewModel : BaseMpsWorkflowViewModel
    {
        protected BaseMpsWorkflowController(
            IMpsWorkflowFacade<TWorkflowTable, TAttachmentTable, TCommentTable, TWorkflowViewModel> facade)
            : base(facade)
        {
        }
    }

}