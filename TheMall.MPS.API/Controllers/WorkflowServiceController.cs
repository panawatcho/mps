﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.OData;
using System.Web.Http.OData.Extensions;
using System.Web.Http.OData.Query;
using System.Web.Http.Routing;
using CrossingSoft.Framework.Infrastructure.K2;
using TheMall.MPS.API.Attributes;
using TheMall.MPS.API.Services;
using TheMall.MPS.Models.Enums;
using TheMall.MPS.ViewModels;
using TheMall.MPS.ViewModels.Workspace;
using SourceCode.Workflow.Client;
using TheMall.MPS.API.Facades;

namespace TheMall.MPS.API.Controllers
{
    [RoutePrefix("api/workflowservice")]
    public class WorkflowServiceController : ApiController
    {
        private readonly IWorkflowRepository _workflowRepository;
        private readonly ISumitWorkflowFacade _sumitWorkflowFacade;

        public WorkflowServiceController(IWorkflowRepository workflowRepository, ISumitWorkflowFacade sumitWorkflowFacade)
        {
            _workflowRepository = workflowRepository;
            _sumitWorkflowFacade = sumitWorkflowFacade;
        }

        [HttpGet]
        public bool Submit(string process , string sn, string action)
        {
            var success = false;
            try
            {
                //var user = User.Identity.Name;
              success =  _sumitWorkflowFacade.SubmitOnLink(process, sn, action);
                //_workflowRepository.ActionWorklistItem(sn, action, false);
             
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return success;
        }
    }
}
