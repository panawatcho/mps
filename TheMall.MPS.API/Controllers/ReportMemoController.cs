﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Microsoft.Reporting.WebForms;
using TheMall.MPS.API.Controllers.Abstracts;
using TheMall.MPS.API.Facades;
using TheMall.MPS.API.Mappers;
using TheMall.MPS.API.Services;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Reports.CashAdvance;

namespace TheMall.MPS.API.Controllers
{
    [RoutePrefix("api/reportmemo")]
    public class ReportMemoController : BaseReportController
    {
        private readonly IReportService _reportService;
        private readonly IMemoService _memoService;
        private readonly IReportMemoFacade _reportMemoFacade;
        private readonly IMemoMapper _memoMapper;
        private readonly IMPSSession _session;
        public ReportMemoController(
            IReportService reportService,
            IMemoService memoService,
            IReportMemoFacade reportMemoFacade, 
            IMemoMapper memoMapper, IMPSSession session)
            : base(reportService)
        {
            _reportService = reportService;
            _memoService = memoService;
            _reportMemoFacade = reportMemoFacade;
            _memoMapper = memoMapper;
            _session = session;
        }

        [Route("memo")]
        public HttpResponseMessage Post(CashAdvanceFormCriteria criteria)
        {
            try
            {
                _reportService.FileName = "Memo";
                _reportService.ReportName = "MemoForm";
                _reportService.ReportPath = "Memo";
                //var cashAdvanceModel = _cashAdvanceService.FirstOrDefault(m => m.DocumentNumber == criteria.DocumentNumber);
                var memoModel = _memoService.FirstOrDefault(m => m.Id == criteria.Id);
               
                //var cashAdvanceViewModel = cashAdvanceModel;
                if (memoModel != null)
                {
                    memoModel.RePrintNo = (memoModel.RePrintNo != null) ? memoModel.RePrintNo + 1 : 1;  
                    var head = _reportMemoFacade.ReportData(memoModel);
                    var lines = _reportMemoFacade.ReportLinesData(memoModel);

                    var approvers = _reportMemoFacade.ApproversData(memoModel.MemoApproval);
                  //  var attachment = _reportMemoFacade.ReportAttachmentsData(memoModel);
                    
                    var dataSource = new Microsoft.Reporting.WebForms.ReportDataSource("MemoDataSet", head);
                    var approversDataSource =
                        new Microsoft.Reporting.WebForms.ReportDataSource("ApproversDataSet", approvers);

                    var linesDataSource = new Microsoft.Reporting.WebForms.ReportDataSource("linesDataSet", lines);
                   // var attachmentDataSource = new Microsoft.Reporting.WebForms.ReportDataSource("AttachmentDataSet", attachment);
                    var reportParams = new List<ReportParameter>();
                    reportParams.Add(new ReportParameter { Name = "RePrintNo", Values = { memoModel.RePrintNo.ToString() } });
                    _reportService.Parameters = reportParams;
                    _reportService.DataSources = new[] {dataSource, approversDataSource, linesDataSource};//, attachmentDataSource };
                    _memoService.Update(memoModel, memoModel.Id);
                    _session.SaveChanges();

                    return GenerateReportStream(criteria);
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw new HttpException(ex.Message);
            }

        }
    }
}