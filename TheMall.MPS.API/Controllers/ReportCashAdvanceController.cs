﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Microsoft.Reporting.WebForms;
using TheMall.MPS.API.Controllers.Abstracts;
using TheMall.MPS.API.Facades;
using TheMall.MPS.API.Services;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Reports.CashAdvance;
using TheMall.MPS.Reports.Tests;

namespace TheMall.MPS.API.Controllers
{
     [RoutePrefix("api/reportcashadvance")]
    public class ReportCashAdvanceController : BaseReportController
    {
        private readonly IReportService _reportService;
        private readonly ICashAdvanceFacade _cashAdvanceFacade;
        private readonly ICashAdvanceService _cashAdvanceService;
        private readonly IPlaceService _placeService;
        private readonly IReportCashAdvanceFacade _reportCashAdvanceFacade;
        private readonly IMPSSession _session;

         public ReportCashAdvanceController(IReportService reportService, 
             IReportService reportService1, 
             ICashAdvanceFacade cashAdvanceFacade, 
             IPlaceService placeService, 
             IReportCashAdvanceFacade reportCashAdvanceFacade, 
             ICashAdvanceService cashAdvanceService, IMPSSession session) 
            : base(reportService)
         {
             _reportService = reportService1;
             _cashAdvanceFacade = cashAdvanceFacade;
             _placeService = placeService;
             _reportCashAdvanceFacade = reportCashAdvanceFacade;
             _cashAdvanceService = cashAdvanceService;
             _session = session;
         }

         [Route("cashadvance")]
        public HttpResponseMessage Post(CashAdvanceFormCriteria criteria)
        {
             try
             {
                //_reportService.FileName = "ยืมเงิน";
                _reportService.ReportName = "CashAdvanceForm";
                _reportService.ReportPath = "CashAdvance";
                //var cashAdvanceModel = _cashAdvanceService.FirstOrDefault(m => m.DocumentNumber == criteria.DocumentNumber);
                var cashAdvanceModel = _cashAdvanceService.FirstOrDefault(m => m.Id == criteria.Id);
                //var cashAdvanceViewModel = cashAdvanceModel;
                if (cashAdvanceModel != null)
                {
                    cashAdvanceModel.RePrintNo = (cashAdvanceModel.RePrintNo != null) ? cashAdvanceModel.RePrintNo + 1 : 1;  
                     var list = _reportCashAdvanceFacade.ReportData(cashAdvanceModel);
                     var approvers = _reportCashAdvanceFacade.ApproversData(cashAdvanceModel.CashAdvanceApproval);
                     var proposalLineTemplate =
                         _reportCashAdvanceFacade.SperatelyBranchPayPercent(cashAdvanceModel.ProposalLine.ProposalLineTemplates, cashAdvanceModel.ProposalRefID);
                     
                     var dataSource = new Microsoft.Reporting.WebForms.ReportDataSource("CashAdvanceDataSet", list);
                     var approversDataSource = 
                         new Microsoft.Reporting.WebForms.ReportDataSource("ApproversDataSet",approvers);                    
                     var proposalLineTemplateDataSource = 
                         new Microsoft.Reporting.WebForms.ReportDataSource("ProposalLineTemplateDataSet",proposalLineTemplate);

                     var reportParams = new List<ReportParameter>();
                     reportParams.Add(new ReportParameter { Name = "RePrintNo", Values = { cashAdvanceModel.RePrintNo.ToString() } });
                     _reportService.Parameters = reportParams;
                     _reportService.FileName = "Cash Advance-No." + cashAdvanceModel.DocumentNumber + "- Date_" + DateTime.Now.ToString("dd_MM_yy"); 
                     _reportService.DataSources = new[]
                     {dataSource, approversDataSource, proposalLineTemplateDataSource}; // <- ยังไม่ใช้จนกว่าจะแก้ปัญหาได้ว่าควรดึงค่ามาจากอะไรอีกอย่าง(ต่อบรรทัดต่อไป)
                     //ตอนนี้มีตรง Create by ที่ไม่ซ้ำกันแต่เราจะอ้าง create by จากอะไร ในเมื่อคนเปิด Proposal กับคนเปิด cash advance อาจจะเป็นคนละคนกันก็ได้ <- โอเคกันยังอะ ถ้าโอแล้วก็ลบ Comment ทิ้งให้หมด
                     _cashAdvanceService.Update(cashAdvanceModel, cashAdvanceModel.Id);
                     _session.SaveChanges();
                    return GenerateReportStream(criteria);
                 }
                 else
                 {
                     return null;
                 }
             }
             catch (Exception ex)
             {
                throw new HttpException(ex.Message);
             }          
        }
    }
}