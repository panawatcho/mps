﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using TheMall.MPS.API.Controllers.Abstracts;
using TheMall.MPS.API.Facades;
using TheMall.MPS.API.Facades.Abstracts;
using TheMall.MPS.API.Mappers;
using TheMall.MPS.API.Services;
using TheMall.MPS.Infrastructure;
using TheMall.MPS.Models.Master;
using TheMall.MPS.ViewModels.Master;
using WebApi.OutputCache.V2;

namespace TheMall.MPS.API.Controllers
{
    [Attributes.AllowAuthenticated]
    [RoutePrefix("api/mailgroup")]
    public class MailGroupController : BaseMasterController<MPS_M_MailGroup, MailGroupViewModel>
    {
        private readonly IMailGroupService _service;
        public MailGroupController(
            IMailGroupFacade facade, 
            IMailGroupService service) 
            : base(facade)
        {
            _service = service;
        }

        [CacheOutput(ClientTimeSpan = Constants.CacheTime, ServerTimeSpan = Constants.ServerCacheTime)]
        public async Task<IHttpActionResult> Get()
        {
            return Ok((await _service.GetAllAsyns()).ToViewModels());
        }
        [CacheOutput(ClientTimeSpan = Constants.CacheTime, ServerTimeSpan = Constants.ServerCacheTime)]
        public async Task<IHttpActionResult> GetById(string id)
        {
            return Ok((await _service.GetAsync(id)).ToViewModel());
        }

        [HttpGet]
        [Route("getbygroupcode/{groupCode}")]
        public async Task<IHttpActionResult> GetByGroupCode(string groupCode = null)
        {
            return Ok(_service.FirstOrDefault(m=>m.GroupCode.Equals(groupCode,StringComparison.InvariantCultureIgnoreCase)).ToViewModel());
        }
    }
}