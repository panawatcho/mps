﻿using System;
using System.Collections.Generic;
using System.EnterpriseServices;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using TheMall.MPS.API.Controllers.Abstracts;
using TheMall.MPS.API.Facades;
using TheMall.MPS.API.Facades.Abstracts;
using TheMall.MPS.API.Mappers;
using TheMall.MPS.API.Services;
using TheMall.MPS.Infrastructure;
using TheMall.MPS.Models.Master;
using TheMall.MPS.ViewModels.Master;
using WebApi.OutputCache.V2;

namespace TheMall.MPS.API.Controllers
{
    [Attributes.AllowAuthenticated]
    [RoutePrefix("api/budgetyearplan")]
    public class BudgetYearPlanController : BaseMasterController<MPS_M_BudgetYearPlan,BudgetYearPlanViewModel>
    {
        private readonly IBudgetYearPlanService _service;
        private readonly IBudgetYearPlanFacade _facade;
        public BudgetYearPlanController(IBudgetYearPlanFacade facade, IBudgetYearPlanService service) 
            : base(facade)
        {
            _service = service;
            _facade = facade;
        }

        //[CacheOutput(ClientTimeSpan = Constants.CacheTime, ServerTimeSpan = Constants.ServerCacheTime)]
        public async Task<IHttpActionResult> Get()
        {
            return Ok((await _service.GetAllAsyns()).ToViewModels());
        }
        //[CacheOutput(ClientTimeSpan = Constants.CacheTime, ServerTimeSpan = Constants.ServerCacheTime)]
        public async Task<IHttpActionResult> GetById(string id)
        {
            return Ok((await _service.GetAsync(id)).ToViewModel());
        }
       
        [HttpGet]
        [Route("search")]
        public async Task<IHttpActionResult> Search(string code = null, string name = null)
        {
            return Ok((await _service.Search(code, name)));
        }

        [HttpGet]
        [Route("searchByYearAndUnitCode/{year}/{unitcode}")]
        public async Task<IHttpActionResult> SearchByYearAndUnitCode(string year = null, string unitcode = null)
        {
          
            var viewModel = _facade.SearchByYearAndUnitcode(year, unitcode);
            return Ok(viewModel);
            //return Ok((await _service.Search(code, name)));B
        }
    
    }
}
