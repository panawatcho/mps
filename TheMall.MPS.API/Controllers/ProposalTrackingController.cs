﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using TheMall.MPS.API.Facades;
using TheMall.MPS.API.Services;

namespace TheMall.MPS.API.Controllers
{
    [Attributes.AllowAuthenticated]
    [RoutePrefix("api/proposaltracking")]
    public class ProposalTrackingController : ApiController
    {
        private readonly IProposalTrackingViewService _proposalTracking;
        private readonly IDepositTrackingViewService _depositTracking;
        private readonly IProposalFacade _proosalProposalFacade;
        public ProposalTrackingController(IProposalTrackingViewService proposalTracking, IProposalFacade proosalProposalFacade, IDepositTrackingViewService depositTracking)
        {
            _proposalTracking = proposalTracking;
            _proosalProposalFacade = proosalProposalFacade;
            _depositTracking = depositTracking;
        }

        [HttpGet]
        [Route("GetTracking/{proplineId}")]
        public IHttpActionResult GetTracking(int proplineId)
        {
            var view = _proposalTracking.GetWhere(m => m.ProposalLineId == proplineId);
            return Ok(view);
        }
        [Route("proposalTracking/{proplineId}")]
        [HttpGet]
        public IHttpActionResult Tracking(int proplineId)
        {
            var tracking = _proosalProposalFacade.ProposalTrackingViewModel(proplineId);
            return Ok(tracking);
        }
        [Route("depositTracking/{propId}")]
        [HttpGet]
        public IHttpActionResult DepositTracking(int propId)
        {
            var tracking = _depositTracking.GetWhere(m => m.DepositId == propId);
            return Ok(tracking);
        }
    }
}
