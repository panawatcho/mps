﻿using System;
using System.Collections.Generic;
using System.EnterpriseServices;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using TheMall.MPS.API.Controllers.Abstracts;
using TheMall.MPS.API.Facades;
using TheMall.MPS.API.Facades.Abstracts;
using TheMall.MPS.API.Mappers;
using TheMall.MPS.API.Services;
using TheMall.MPS.Infrastructure;
using TheMall.MPS.Models.Master;
using TheMall.MPS.ViewModels.Master;
using WebApi.OutputCache.V2;

namespace TheMall.MPS.API.Controllers
{
    [Attributes.AllowAuthenticated]
    [RoutePrefix("api/typememo")]
    public class TypeMemoController : BaseMasterController<MPS_M_TypeMemo,TypeMemoViewModel>
    {
        private readonly ITypeMemoService _service;
        public TypeMemoController(ITypeMemoFacade facade, ITypeMemoService service) 
            : base(facade)
        {
            _service = service;
        }

       // [CacheOutput(ClientTimeSpan = Constants.CacheTime, ServerTimeSpan = Constants.ServerCacheTime)]
        public async Task<IHttpActionResult> Get()
        {
            return Ok((await _service.GetAllAsyns()).ToViewModels());
        }
     
        public async Task<IHttpActionResult> GetById(string id)
        {
            return Ok((await _service.GetAsync(id)).ToViewModel());
        }
        [HttpGet]
        [Route("search")]
        public async Task<IHttpActionResult> Search(string code = null, string name = null)
        {
            return Ok((await _service.Search(code, name)));
        }

        [HttpGet]
        [Route("searchbytypecode/{typeMemoCode}")]
        public async Task<IHttpActionResult> SearchByTypeCode(string typeMemoCode = null)
        {
            return Ok(_service.FirstOrDefault(m=>m.TypeMemoCode.Equals(typeMemoCode,StringComparison.InvariantCultureIgnoreCase)).ToViewModel());
        }
    }
}
