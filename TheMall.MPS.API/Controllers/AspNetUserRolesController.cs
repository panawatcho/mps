﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using TheMall.MPS.API.Attributes;
using TheMall.MPS.API.Controllers.Abstracts;
using TheMall.MPS.API.Facades;
using TheMall.MPS.API.Facades.Abstracts;
using TheMall.MPS.Infrastructure.Identity;
using TheMall.MPS.ViewModels.AspNet;

namespace TheMall.MPS.API.Controllers
{

    [AllowAuthenticated]
    [RoutePrefix("api/aspnetuserroles")]
    public class AspNetUserRolesController : BaseMasterController<ApplicationUserRole, AspNetUserRolesViewModel>
    {
        private readonly IAspNetUserRolesFacade _facade;
        public AspNetUserRolesController(
            IAspNetUserRolesFacade facade) : base(facade)
        {
            _facade = facade;
        }

        [HttpGet]
        [Route("getbyuseridandroleid/{roleId}/{username}")]
        public async Task<IHttpActionResult> Search(string roleId = null, string username = null)
        {
            return Ok(_facade.GetByUserIdandRoleId(roleId,username));
        }

        [HttpGet]
        [Route("getall/")]
        public async Task<IHttpActionResult> GetAll()
        {
            return Ok(_facade.GetAll());
        }

        [HttpGet]
        [Route("getbyroleid/{roleId}")]
        public async Task<IHttpActionResult> GetByRoleId(string roleId = null)
        {
            return Ok(_facade.GetByRoleId(roleId));
        }
    }
}