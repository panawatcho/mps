﻿using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http;
using TheMall.MPS.API.Controllers.Abstracts;
using TheMall.MPS.API.Services;
using TheMall.MPS.Reports.Tests;

namespace TheMall.MPS.API.Controllers
{
    [RoutePrefix("api/report")]
    public class ReportController : BaseReportController
    {
        private readonly IReportService _reportService;
        public ReportController(IReportService reportService)
            : base(reportService)
        {
            _reportService = reportService;
        }

        [Route("report1")]
        public HttpResponseMessage Post(Report1Criteria criteria)
        {
            _reportService.FileName = "ทดสอบ";
            _reportService.ReportName = "Report1";
            _reportService.ReportPath = "Tests";

            var rand = new System.Random();
            var list = new List<Report1DataSource>();
            for (int i = 0; i < 100; i++)
            {
                list.Add(new Report1DataSource()
                {
                    Id = i,
                    Name = string.Format("Test {0}", i),
                    Amount = rand.NextDouble().ToString("N"),
                    Date = System.DateTime.UtcNow.ToLongDateString(),
                    Image = Reports.Helpers.Checkbox.Create(i % 2 == 0)});
            }
            var dataSource = new Microsoft.Reporting.WebForms.ReportDataSource("DataSet1", list
                );

            _reportService.DataSources = new[] { dataSource };

            return GenerateReportStream(criteria);
        }
    }
}