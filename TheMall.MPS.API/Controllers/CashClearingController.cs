﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Routing;
using TheMall.MPS.API.Controllers.Abstracts;
using TheMall.MPS.API.Facades;
using TheMall.MPS.API.Facades.Interfaces;
using TheMall.MPS.API.Mappers;
using TheMall.MPS.API.Services;
using TheMall.MPS.Models.CashClearing;
using TheMall.MPS.Models.Enums;
using TheMall.MPS.ViewModels.CashAdvance;
using TheMall.MPS.ViewModels.CashClearing;

namespace TheMall.MPS.API.Controllers
{
    [Attributes.AllowAuthenticated]
    [RoutePrefix("api/cashclearing")]
    public class CashClearingController : BaseMpsWorkflowController<MPS_CashClearingTable, MPS_CashClearingAttachment, MPS_CashClearingComment, CashClearingViewModel>
    {
        private readonly ICashClearingFacade _facade;
        private readonly IEmployeeTableService _employee;
        private readonly IFinalApproverService _finalApproverService;

        public CashClearingController(ICashClearingFacade facade,
            IEmployeeTableService employee, 
            IFinalApproverService finalApproverService) : base(facade)
        {
            _facade = facade;
            _employee = employee;
            _finalApproverService = finalApproverService;
        }


        [Route("{id}")]
        [HttpGet]
        public async Task<IHttpActionResult> Get( int id)
        {
            return Ok(await _facade.GetViewModel(Url,id));
        }


        [Route("GetByCashAdvanceId/{CashAdvanceId}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetByCashAdvanceId( int cashAdvanceId)
        {
            return Ok(await _facade.GetViewModelByCashAdvanceId(Url,cashAdvanceId));
        }


        [Route("")]
        [HttpPost]
        public async Task<IHttpActionResult> Post(CashClearingViewModel viewModel)
        {
            var model = await _facade.SaveAsync(viewModel);
            return Ok(model);
        }

        [Route("{id}/startprocess")]
        [HttpPost]
        public async Task<IHttpActionResult> StartProcess(int id)
        {
            var procInstId = await _facade.StartProcess(id);

            return Ok(procInstId);
        }
        [Route("approve")]
        [HttpGet]
        public IHttpActionResult GetApproval()
        {
            var viewModel = new CashAdvanceApprovalViewModel()
            {
                Employee = _employee.FindByUsername("Tester06").ToViewModel(),
                ApproverSequence = 1,
                ApproverLevel = "อนุมัติขั้นสุดท้าย",
                ApproverUserName = "Tester06",
                ApproverFullName = "",
                ApproverEmail = "Tester06@gmail.com",
                Position = "รองประธานการตลาด",
                Department = "ฝ่าย CORPORATE PROMOTION",
                DueDate = DateTime.Now,
                LineNo = 1

            };
            return Ok(viewModel);
        }

        [Route("approve/{budget}/{id}/{unitcode}")]
        [HttpGet]
        public IHttpActionResult GetFinalApproval(decimal budget, int id ,string unitcode)
        {
            if (id > 0)
            {
                _facade.SetFinalApproverDeleted(id);
            }
            var finalApprover = _finalApproverService.GetFinalApprover(budget, Budget.DocumentType.CashClearing, unitcode).ToCashClearingApproval();
            return Ok(finalApprover);
        }

        /*
        [Route("CashClearingFromCashAdvance/{id}")]
        [HttpGet]
        public async Task<IHttpActionResult> CashClearingFromCashAdvance(int id)
        {
            var viewModel= _facade.CashClearingFromCashAdvance(id);

            return Ok(viewModel);
        }*/
    }
}