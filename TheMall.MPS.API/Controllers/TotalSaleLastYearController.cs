﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using TheMall.MPS.API.Attributes;
using TheMall.MPS.API.Controllers.Abstracts;
using TheMall.MPS.API.Facades;
using TheMall.MPS.API.Facades.Abstracts;
using TheMall.MPS.API.Mappers;
using TheMall.MPS.API.Services;
using TheMall.MPS.Models;
using TheMall.MPS.ViewModels.Master;

namespace TheMall.MPS.API.Controllers
{
    [AllowAuthenticated]
    [RoutePrefix("api/totalsalelastyear")]
    public class TotalSaleLastYearController : BaseMasterController<MPS_M_TotalSaleLastYear,TotalSaleLastYearViewModel>
    {
        private readonly ITotalSaleLastYearFacade _facade;
        private readonly ITotalSaleLastYearMasterService _service;


        public TotalSaleLastYearController(
            ITotalSaleLastYearFacade facade, 
            ITotalSaleLastYearMasterService service)
            : base(facade)
        {
            _facade = facade;
            _service = service;
        }

        //public TotalSaleLastYearController(
        //    ITotalSaleLastYearFacade facade, 
        //    ITotalSaleLastYearMasterService service)
        //{
        //    _facade = facade;
        //    _service = service;
        //}

        [Route("GetData/{branchCode}/{year}")]
        [HttpGet]
        public IHttpActionResult GetData(string branchCode, string year)
        {
            return Ok(_facade.Get(branchCode, year));
        }

        [Route("GetAllData/")]
        [HttpGet]
        public IHttpActionResult GetAllData()
        {
            return Ok(_service.GetAll());
        }
    }

       
    }