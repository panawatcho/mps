﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using TheMall.MPS.API.Attributes;
using TheMall.MPS.API.Controllers.Abstracts;
using TheMall.MPS.API.Facades;
using TheMall.MPS.API.Mappers;
using TheMall.MPS.API.Services;
using TheMall.MPS.ViewModels;

namespace TheMall.MPS.API.Controllers
{
    [AllowAuthenticated]
    [RoutePrefix("api/searchdoc")]
    public class SearchDocController : BaseApiController
    {
        private readonly ISearchDocFacade _searchDocFacade;
        private readonly IAccountingService _accountingService;
        public SearchDocController(ISearchDocFacade searchDocFacade)
        {
            _searchDocFacade = searchDocFacade;
            //  _searchDocFacade = searchDocFacade;
        }

        //[Route("SearchDoc/{startDate}/{process}/{title}/{documentNo}/{username}/{status}")]
        [Route("SearchDoc")]
        [HttpPost]
        public async Task<IHttpActionResult> SearchDoc(SearchDocViewModel searchDocViewModel)
            //(DateTime? startDate,
            //string process = null, string title = null, string documentNo = null, 
            //string username = null, string status = null)
        {
            var viewModel = _searchDocFacade.SearchDoc(searchDocViewModel);
            return Ok(viewModel);
        }
    }
}