﻿using System;
using System.Collections.Generic;
using System.EnterpriseServices;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using TheMall.MPS.API.Controllers.Abstracts;
using TheMall.MPS.API.Facades;
using TheMall.MPS.API.Facades.Abstracts;
using TheMall.MPS.API.Mappers;
using TheMall.MPS.API.Services;
using TheMall.MPS.Infrastructure;
using TheMall.MPS.Models.Master;
using TheMall.MPS.ViewModels.Master;
using WebApi.OutputCache.V2;

namespace TheMall.MPS.API.Controllers
{
    [Attributes.AllowAuthenticated]
    [RoutePrefix("api/allocatebasis")]
    public class AllocateBasisController : BaseMasterController<MPS_M_AllocateBasis,AllocateBasisViewModel>
    {
        private readonly IAllocateBasisFacade _facade;
       
        public AllocateBasisController(IAllocateBasisFacade facade) 
            : base(facade)
        {
            _facade = facade;
            
        }

        // [CacheOutput(ClientTimeSpan = Constants.CacheTime, ServerTimeSpan = Constants.ServerCacheTime)]
        public async Task<IHttpActionResult> Get()
        {
            return Ok((await _facade.GetAllAsyns()).ToViewModels());
        }
       // [CacheOutput(ClientTimeSpan = Constants.CacheTime, ServerTimeSpan = Constants.ServerCacheTime)]
        [HttpGet]
        [Route("{id}")]
        public async Task<IHttpActionResult> GetById(string id)
        {
            return Ok(( _facade.GetAsync(id)).ToViewModel());
        }

        [HttpPost]
        [Route("")]
        public async Task<IHttpActionResult> PostSharedPercent(AllocateBasisViewModel viewModel)
        {
            return Ok((await _facade.SaveAsync(viewModel)));
        }

        [HttpGet]
        [Route("search")]
        public async Task<IHttpActionResult> Search(string code = null, string name = null)
        {
            return Ok(( _facade.Search(code, name)));
        }
    }
}
