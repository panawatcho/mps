﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Microsoft.Reporting.WebForms;
using TheMall.MPS.API.Controllers.Abstracts;
using TheMall.MPS.API.Facades;
using TheMall.MPS.API.Services;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Reports.CashClearing;
using TheMall.MPS.Reports.Tests;

namespace TheMall.MPS.API.Controllers
{
    
    [RoutePrefix("api/reportcashclearing")]
    public class ReportCashClearingController : BaseReportController
    {
        private readonly IReportService _reportService;
        private readonly ICashClearingFacade _cashClearingFacade;
        private readonly IReportCashClearingFacade _reportCashClearingFacade;
        private readonly ICashClearingService _cashClearingService;
        private readonly IMPSSession _session;
        public ReportCashClearingController(
            IReportService reportService,
            ICashClearingFacade cashClearingFacade, 
            IReportCashClearingFacade reportCashClearingFacade, ICashClearingService cashClearingService, IMPSSession session) : base(reportService)
        {
            _reportService = reportService;
            _cashClearingFacade = cashClearingFacade;
            _reportCashClearingFacade = reportCashClearingFacade;
            _cashClearingService = cashClearingService;
            _session = session;
        }

        [Route("cashclearing")]
        public HttpResponseMessage Post(CashClearingFormCriteria criteria)
        {
            try
            {
                _reportService.FileName = "ใบเคลียเงิน";
                _reportService.ReportName = "CashClearingForm";
                _reportService.ReportPath = "CashClearing";
               // var cashClearingModel = _cashClearingService.FirstOrDefault(m => m.DocumentNumber.Equals(criteria.DocumentNumber, StringComparison.InvariantCultureIgnoreCase));
                var cashClearingModel = _cashClearingService.FirstOrDefault(m => m.Id == criteria.Id);
                cashClearingModel.RePrintNo = (cashClearingModel.RePrintNo != null) ? cashClearingModel.RePrintNo + 1 : 1;  
                var list = _reportCashClearingFacade.ReportData(cashClearingModel);
                var approvers = _reportCashClearingFacade.ApproversData(cashClearingModel.CashClearingApproval);
                var dataSource = new Microsoft.Reporting.WebForms.ReportDataSource("CashClearingDataSet", list);
                var approversDataSource = new Microsoft.Reporting.WebForms.ReportDataSource("ApproversDataSet", approvers);
                var reportParams = new List<ReportParameter>();
                reportParams.Add(new ReportParameter { Name = "RePrintNo", Values = { cashClearingModel.RePrintNo.ToString() } });
                _reportService.Parameters = reportParams;
                _reportService.DataSources = new[] { dataSource, approversDataSource };
                _cashClearingService.Update(cashClearingModel, cashClearingModel.Id);
                _session.SaveChanges();
                return GenerateReportStream(criteria);
                 
            }
            catch (Exception ex)
            {
                throw new HttpException(ex.Message);
            }
        }

    }
}