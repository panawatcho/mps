﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using TheMall.MPS.API.Controllers.Abstracts;
using TheMall.MPS.API.Facades;
using TheMall.MPS.API.Facades.Interfaces;
using TheMall.MPS.Infrastructure;
using TheMall.MPS.Models.FileManager;
using TheMall.MPS.ViewModels;
using TheMall.MPS.ViewModels.FileManager;

namespace TheMall.MPS.API.Controllers
{
    [RoutePrefix("api/filemanager/directory")]
    public class FileManagerDirectoryController : Abstracts.BaseApiController
    {
        private readonly IFileManagerFacade _facade;

        public FileManagerDirectoryController(IFileManagerFacade facade)
        {
            _facade = facade;
        }

        [Route("")]
        public IHttpActionResult Get()
        {
            return Ok(_facade.GetAllDirectories(UserHelper.CurrentUserName()));
        }

        
    }
}