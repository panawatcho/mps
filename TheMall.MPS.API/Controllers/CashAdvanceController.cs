﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Routing;
using TheMall.MPS.API.Controllers.Abstracts;
using TheMall.MPS.API.Facades;
using TheMall.MPS.API.Facades.Interfaces;
using TheMall.MPS.API.Mappers;
using TheMall.MPS.API.Services;
using TheMall.MPS.Models.CashAdvance;
using TheMall.MPS.Models.Enums;
using TheMall.MPS.ViewModels.CashAdvance;
using TheMall.MPS.ViewModels.CashClearing;
using TheMall.MPS.ViewModels.PettyCash;

namespace TheMall.MPS.API.Controllers
{
    [Attributes.AllowAuthenticated]
    [RoutePrefix("api/cashadvance")]
    public class CashAdvanceController : BaseMpsWorkflowController<MPS_CashAdvanceTable, MPS_CashAdvanceAttachment, MPS_CashAdvanceComment, CashAdvanceViewModel>
    {
        private readonly ICashAdvanceFacade _facade;
        private readonly IEmployeeTableService _employee;
        private readonly IFinalApproverService _finalApproverService;
        public CashAdvanceController(
            ICashAdvanceFacade facade,
            IEmployeeTableService employee, 
            IFinalApproverService finalApproverService) : base(facade)
        {
            _facade = facade;
            _employee = employee;
            _finalApproverService = finalApproverService;
        }

        [Route("{id}")]
        [HttpGet]
        public async Task<IHttpActionResult> Get( int id)
        {
            var result = await _facade.GetViewModel(Url, id);
            return Ok(result);
        }

        [Route("NoLineId/{id}")]
        [HttpGet]
        public async Task<IHttpActionResult> NoLineId( int id)
        {
            return Ok(await _facade.GetNoLineId(Url,id));
        }
   

        [Route("")]
        [HttpPost]
        public async Task<IHttpActionResult> Post(CashAdvanceViewModel viewModel)
        {
            var model = await _facade.SaveAsync(viewModel);
            return Ok(model);
        }

        [Route("{id}/startprocess")]
        [HttpPost]
        public async Task<IHttpActionResult> StartProcess(int id)
        {
            var procInstId = await _facade.StartProcess(id);

            return Ok(procInstId);
        }

        [Route("indexdata/{username}")]
        [HttpGet]
        public async Task<IHttpActionResult> DataForIndex(string username)
        {
            var cashAdvanceIndexData = _facade.GetIndexData(username);

            return Ok(cashAdvanceIndexData);
        }

        [Route("accindex/{username}")]
        [HttpGet]
        public async Task<IHttpActionResult> DataAccIndex(string username)
        {
            var cashAdvanceIndexAcc = _facade.GetAccIndex(username);

            return Ok(cashAdvanceIndexAcc);
        }

        [Route("getProposalLineData/{UnitCode}/{parentId}")]
        [HttpGet]
        public IHttpActionResult GetProposalLineData(string UnitCode, int parentId)
        {
            var viewModel = _facade.GetProposalLineData(UnitCode, parentId);
            return Ok(viewModel);
        }


        [Route("getdatabindadvance/{propLineid}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetDataBindAdvance(int propLineid)
        {
            var viewModel = await _facade.GetDataBindAdvance(propLineid);
            return Ok(viewModel);
        }
        
        [Route("approve")]
        [HttpGet]
        public IHttpActionResult GetApproval()
        {
            var viewModel = new CashClearingApprovalViewModel()
            {
                Employee = _employee.FindByUsername("Tester06").ToViewModel(),
                ApproverSequence = 1,
                ApproverLevel = "อนุมัติขั้นสุดท้าย",
                ApproverUserName = "Tester06",
                ApproverFullName = "",
                ApproverEmail = "Tester06@gmail.com",
                Position = "รองประธานการตลาด",
                Department = "ฝ่าย CORPORATE PROMOTION",
                DueDate = DateTime.Now,
                LineNo = 1

            };
            return Ok(viewModel);
        }

        [Route("approve/{budget}/{id}/{unitcode}")]
        [HttpGet]
        public IHttpActionResult GetFinalApproval(decimal budget, int id, string unitcode)
        {
           

            if (id > 0)
            {
                _facade.SetFinalApproverDeleted(id);
            }
            var finalApprover = _finalApproverService.GetFinalApprover(budget, Budget.DocumentType.CashAdvance, unitcode).ToCashAdvanceApproval();
            return Ok(finalApprover);
        }



        [Route("deleteby/{id}/{username}")]
        [HttpGet]
        public IHttpActionResult DeleteBy(int id, string username)
        {
            var result =  _facade.DeleteBy(id, username);
            return Ok(result); ;
        }
    }
}