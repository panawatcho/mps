﻿using System.Threading.Tasks;
using System.Web.Http;
using TheMall.MPS.API.Controllers.Abstracts;
using TheMall.MPS.API.Mappers;
using TheMall.MPS.API.Services;

namespace TheMall.MPS.API.Controllers
{
    [RoutePrefix("api/documenttype")]
    public class DocumentTypeController : BaseApiController
    {

        private readonly IDocumentTypeService _documentTypeService;

        public DocumentTypeController(IDocumentTypeService documentTypeService)
        {
            _documentTypeService = documentTypeService;
        }

        [Route("{process}")]
        public async Task<IHttpActionResult> GetDocumentTypes(string process)
        {
            return Ok((await _documentTypeService.GetFromProcessAsync(process)).ToViewModel());
        }

        [Route("{process}/{activity}")]
        public async Task<IHttpActionResult> GetDocumentTypes(string process, string activity)
        {
            return Ok((await _documentTypeService.GetFromProcessAsync(process, activity)).ToViewModel());
        }
    }
}