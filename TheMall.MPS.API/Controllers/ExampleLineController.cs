﻿using System.Web.Http;
using TheMall.MPS.API.Controllers.Abstracts;
using TheMall.MPS.API.Facades;
using TheMall.MPS.Models.Example;

namespace TheMall.MPS.API.Controllers
{
    [RoutePrefix("api/exampleline")]
    public class ExampleLineController : BaseUploadController<ExampleLineAttachment>
    {
        private readonly IExampleLineFacade _facade;
        public ExampleLineController(
            IExampleLineFacade facade)
            : base(facade)
        {
            _facade = facade;
        }
    }
}