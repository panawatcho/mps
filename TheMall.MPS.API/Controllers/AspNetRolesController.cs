﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using TheMall.MPS.API.Attributes;
using TheMall.MPS.API.Controllers.Abstracts;
using TheMall.MPS.API.Facades;
using TheMall.MPS.API.Facades.Abstracts;
using TheMall.MPS.Infrastructure.Identity;
using TheMall.MPS.ViewModels.AspNet;
using TheMall.MPS.ViewModels.Master;

namespace TheMall.MPS.API.Controllers
{
    [AllowAuthenticated]
    [RoutePrefix("api/aspnetroles")]
    public class AspNetRolesController : BaseMasterController<ApplicationRole, AspNetRolesViewModel>
    {
        private readonly IAspNetRolesFacade _facade;

        public AspNetRolesController(
            IAspNetRolesFacade facade) : base(facade)
        {
            _facade = facade;
        }


        [HttpGet]
        [Route("getbyid/{id}")]
        public async Task<IHttpActionResult> Search(string id = null)
        {
            return Ok(_facade.SearchById(id));
        }

        [HttpGet]
        [Route("getall/")]
        public async Task<IHttpActionResult> GetAll()
        {
            return Ok(_facade.GetAll());
        }
    }
}