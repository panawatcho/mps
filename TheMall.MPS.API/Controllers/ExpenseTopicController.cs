﻿using System;
using System.Collections.Generic;
using System.EnterpriseServices;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using TheMall.MPS.API.Controllers.Abstracts;
using TheMall.MPS.API.Facades;
using TheMall.MPS.API.Facades.Abstracts;
using TheMall.MPS.API.Mappers;
using TheMall.MPS.API.Services;
using TheMall.MPS.Infrastructure;
using TheMall.MPS.Models.Master;
using TheMall.MPS.ViewModels.Master;
using WebApi.OutputCache.V2;

namespace TheMall.MPS.API.Controllers
{
    [Attributes.AllowAuthenticated]
    [RoutePrefix("api/expensetopic")]
    public class ExpenseTopicController : BaseMasterController<MPS_M_ExpenseTopic,ExpenseTopicViewModel>
    {
        private readonly IExpenseTopicFacade _facade;
        
        public ExpenseTopicController(IExpenseTopicFacade facade) 
            : base(facade)
        {
            _facade = facade;
         
        }

        //[CacheOutput(ClientTimeSpan = Constants.CacheTime, ServerTimeSpan = Constants.ServerCacheTime)]
         [HttpGet]
         [Route("")]
        public async Task<IHttpActionResult> Get()
        {
            return Ok((await _facade.GetAllAsyns()).ToViewModels());
        }
        //[CacheOutput(ClientTimeSpan = Constants.CacheTime, ServerTimeSpan = Constants.ServerCacheTime)]
        [HttpGet]
         
        public async Task<IHttpActionResult> GetById(string id)
        {
            return Ok((await _facade.GetAsyns(id)));
        }
        [HttpPost]
        [Route("")]
        public async Task<IHttpActionResult> PostExpense(ExpenseTopicViewModel viewModel)
        {
            return Ok(await _facade.Save(viewModel));
        }
        [HttpGet]
        [Route("search")]
        public async Task<IHttpActionResult> Search(string code = null, string name = null)
        {
            return Ok((await _facade.Search(code, name)));
        }
        [HttpGet]
        [Route("expensetopic/{expenseTopicCode}")]
        public async Task<IHttpActionResult> GetViewModel(string expenseTopicCode)
        {
            return Ok((await _facade.GetViewModel(expenseTopicCode)));
        }
    }
}
