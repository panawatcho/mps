﻿using System;
using System.Collections.Generic;
using System.EnterpriseServices;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using TheMall.MPS.API.Controllers.Abstracts;
using TheMall.MPS.API.Facades;
using TheMall.MPS.API.Facades.Abstracts;
using TheMall.MPS.API.Mappers;
using TheMall.MPS.API.Services;
using TheMall.MPS.Infrastructure;
using TheMall.MPS.Models.Master;
using TheMall.MPS.ViewModels.Master;
using WebApi.OutputCache.V2;

namespace TheMall.MPS.API.Controllers
{
    [Attributes.AllowAuthenticated]
    [RoutePrefix("api/unitcode")]
    public class UnitCodeController : BaseMasterController<MPS_M_UnitCode,UnitCodeViewModel>
    {
        private readonly IUnitCodeFacade _facade;
        private readonly IUnitCodeService _service;
        public UnitCodeController(IUnitCodeFacade facade, IUnitCodeService service)
            : base(facade)
        {
            _facade = facade;
            _service = service;
        }

        //[CacheOutput(ClientTimeSpan = Constants.CacheTime, ServerTimeSpan = Constants.ServerCacheTime)]
        public async Task<IHttpActionResult> Get()
        {
            return Ok((await _service.GetAllAsyns()).ToViewModels());
        }
       // [CacheOutput(ClientTimeSpan = Constants.CacheTime, ServerTimeSpan = Constants.ServerCacheTime)]
        public async Task<IHttpActionResult> GetById(string id)
        {
            return Ok((await _service.GetAsync(id)).ToViewModel());
        }
        [HttpGet]
        [Route("search")]
        public async Task<IHttpActionResult> Search(string code = null, string name = null)
        {
            return Ok(( _facade.Search(code, name)));
        }

        [HttpGet]
        [Route("getbyunitcode/{unitCode}")]
        public async Task<IHttpActionResult> GetByUnitCode(string unitCode = null)
        {
            return Ok(_service.FirstOrDefault(m=>m.UnitCode.Equals(unitCode, StringComparison.InvariantCultureIgnoreCase)).ToViewModel());
        }
    }
}
