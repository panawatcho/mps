﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using TheMall.MPS.API.Controllers.Abstracts;
using TheMall.MPS.API.Facades;
using TheMall.MPS.API.Mappers;
using TheMall.MPS.API.Services;
using TheMall.MPS.Models.Master;
using TheMall.MPS.ViewModels.Master;

namespace TheMall.MPS.API.Controllers
{
    [Attributes.AllowAuthenticated]
    [RoutePrefix("api/theme")]
    public class ThemeController : BaseMasterController<MPS_M_Themes, ThemeViewModel>
    {
        private readonly IThemeService _themeService;

        public ThemeController(IThemeFacade themefacade, 
            IThemeService themeService) 
            : base(themefacade)
        {
            _themeService = themeService;
        }
       
        public async Task<IHttpActionResult> Get()
        {
            return Ok((await _themeService.GetAllAsyns()).ToViewModels());
           
        }
        public async Task<IHttpActionResult> GetById(string id)
        {
            return Ok((await _themeService.GetAsync(id)).ToViewModel());
        }
    }
}