﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.OData;
using System.Web.Http.OData.Extensions;
using System.Web.Http.OData.Query;
using TheMall.MPS.API.Attributes;
using TheMall.MPS.API.Services;
using TheMall.MPS.ViewModels;
using TheMall.MPS.ViewModels.Workspace;
using SourceCode.Workflow.Client;

namespace TheMall.MPS.API.Controllers
{
    //[ApiExplorerSettings(IgnoreApi = true)]
    [AllowAuthenticated]
    public class WorkspaceController : ApiController
    {
        //private const string PROCESS_FOLDER = "Land Acquisition";
        private readonly IWorkspaceService _workspaceService;

        public WorkspaceController(IWorkspaceService workspaceService)
        {
            _workspaceService = workspaceService;
        }

        // GET: rest/Worklist
        public PageResult<MyWorklistItem> Get(ODataQueryOptions<MyWorklistItem> oDataQueryOptions)
        {
            var worklistCriteria = new WorklistCriteria();
            //worklistCriteria.AddFilterField(WCField.ProcessFolder, WCCompare.Like, TheMall.MPS.Infrastructure.Constants.PROCESS_FOLDER);
            //Exclude these processes
            //worklistCriteria.AddFilterField(WCLogical.And, WCField.ProcessFullName, WCCompare.NotEqual, _billPaymentProcess.ProcessFullPath);
            //worklistCriteria.AddFilterField(WCLogical.And, WCField.ProcessFullName, WCCompare.NotEqual, _paymentVoucherProcess.ProcessFullPath);
            var worklist = _workspaceService.GetWorklist(oDataQueryOptions, worklistCriteria);
            return
                new PageResult<MyWorklistItem>(
                    worklist.Data as IEnumerable<MyWorklistItem>, Request.ODataProperties().NextLink, worklist.TotalCount);
        }

        // filter memoincome
        [Route("api/workspaceaccount")]
        public PageResult<MyWorklistItem> GetWorkSpace(ODataQueryOptions<MyWorklistItem> oDataQueryOptions)
        {
            var worklistCriteria = new WorklistCriteria();
            //worklistCriteria.AddFilterField(WCField.ProcessFolder, WCCompare.Like, TheMall.MPS.Infrastructure.Constants.PROCESS_FOLDER);
            //Exclude these processes
            //worklistCriteria.AddFilterField(WCLogical.And, WCField.ProcessFullName, WCCompare.NotEqual, _billPaymentProcess.ProcessFullPath);
            //worklistCriteria.AddFilterField(WCLogical.And, WCField.ProcessFullName, WCCompare.NotEqual, _paymentVoucherProcess.ProcessFullPath);
            var worklist = _workspaceService.GetWorklist(oDataQueryOptions, worklistCriteria, true);
            worklist.Data = worklist.Data.Where(m => m.ProcessName.ToLower() == "memoincome").ToList(); 
            worklist.TotalCount = worklist.Data.Count(); // แบ่งหน้า
            return
                new PageResult<MyWorklistItem>(
                    worklist.Data as IEnumerable<MyWorklistItem>, Request.ODataProperties().NextLink, worklist.TotalCount);
        }

        [Route("api/workspacecashadvance")]
        public PageResult<MyWorklistItem> GetWorkSpaceCashAdvance(ODataQueryOptions<MyWorklistItem> oDataQueryOptions)
        {
            var worklistCriteria = new WorklistCriteria();
            var worklist = _workspaceService.GetWorklist(oDataQueryOptions, worklistCriteria, true);
            worklist.Data = worklist.Data.Where(m => m.ProcessName.ToLower() == "cashadvance").ToList();
            worklist.TotalCount = worklist.Data.Count(); // แบ่งหน้า
            return
                new PageResult<MyWorklistItem>(
                    worklist.Data as IEnumerable<MyWorklistItem>, Request.ODataProperties().NextLink, worklist.TotalCount);
        }

        [Route("api/workspaceaccount/proposal")]
        public PageResult<WorklistItemViewModel> GetWorkSpaceForProposal(ODataQueryOptions<MyWorklistItem> oDataQueryOptions)
        {
            var worklistCriteria = new WorklistCriteria();
          //  worklistCriteria.AddFilterField(WCField.ProcessName, WCCompare.Like, "Proposal_Account");
            
            var worklist = _workspaceService.GetWorklistForProposal(oDataQueryOptions, worklistCriteria);
            worklist.Data = worklist.Data;
            worklist.TotalCount = worklist.Data.Count(); // แบ่งหน้า

            return
                new PageResult<WorklistItemViewModel>(
                    worklist.Data as IEnumerable<WorklistItemViewModel>, Request.ODataProperties().NextLink, worklist.TotalCount);
        }

        [Route("api/workspace/delegate")]
        public IHttpActionResult Delegate(string SN, string toUserFQN)
        {
            try
            {
                _workspaceService.Delegate(SN, toUserFQN);
                return Ok(true);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        [Route("api/workspace/redirect")]
        public IHttpActionResult Redirect(string SN, string toUserFQN)
        {
            try
            {
                _workspaceService.Redirect(SN, toUserFQN);
                return Ok(true);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        [Route("api/workspace/total")]
        public IHttpActionResult GetWorklistTotal()
        {
            var worklistCriteria = new WorklistCriteria();
            var worklistTotalViewModel = new WorklistTotalViewModel();
            worklistCriteria.AddFilterField(WCField.ProcessFolder, WCCompare.Like, TheMall.MPS.Infrastructure.Constants.PROCESS_FOLDER);
            //Exclude these processes
            //worklistCriteria.AddFilterField(WCLogical.And, WCField.ProcessFullName, WCCompare.NotEqual, _billPaymentProcess.ProcessFullPath);
            //worklistCriteria.AddFilterField(WCLogical.And, WCField.ProcessFullName, WCCompare.NotEqual, _paymentVoucherProcess.ProcessFullPath);
            var worklist = _workspaceService.GetWorklist(null, worklistCriteria, true);
            if (worklist != null && worklist.Data != null)
            {
                worklistTotalViewModel.Total = worklist.TotalCount;
                worklistTotalViewModel.Early = worklist.Data.Count(m => m.DueStatus == WorklistDueStatus.Early.ToString());
                worklistTotalViewModel.Ondue = worklist.Data.Count(m => m.DueStatus == WorklistDueStatus.Due.ToString());
                worklistTotalViewModel.Overdue = worklist.Data.Count(m => m.DueStatus == WorklistDueStatus.Late.ToString());
            }
            return Ok(worklistTotalViewModel);
       
        }
    }
}
