﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using TheMall.MPS.API.Controllers.Abstracts;
using TheMall.MPS.API.Facades;
using TheMall.MPS.ViewModels;

namespace TheMall.MPS.API.Controllers
{
    [Attributes.AllowAuthenticated]
    [RoutePrefix("api/documentnumber")]
    public class DocumentNumberController : BaseApiController
    {
        private readonly IProposalFacade _proposalFacade;

        public DocumentNumberController(IProposalFacade proposalFacade)
        {
            _proposalFacade = proposalFacade;
        }

        [Route("proposal")]
        [HttpGet]
        public IHttpActionResult GetProposalNumber(string code = "", string name = "", string unitCode = "")
        {
            //var viewModel = _proposalFacade.GetProposalNumber(code, name);
            var viewModel = _proposalFacade.GetProposalNumber(code, name, unitCode);
            return Ok(viewModel);
        }

        [Route("deposit")]
        [HttpGet]
        public IHttpActionResult GetDeposit(string code = "", string name = "")
        {
            var viewModel = _proposalFacade.GetDepositNumber(code, name);
            return Ok(viewModel);
        }

        [Route("incomedeposit")]
        [HttpGet]
        public IHttpActionResult GetIncomeDeposit(string code = "", string name = "")
        {
            var viewModel = _proposalFacade.GetIncomeDepositNumber(code, name);
            return Ok(viewModel);
        }

    }
}
