﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using CrossingSoft.Framework.Services;
using TheMall.MPS.API.Facades;
using TheMall.MPS.API.Services;
using TheMall.MPS.Models.FileManager;

namespace TheMall.MPS.API.Controllers
{
    [AllowAnonymous]
    public class FileManagerFileController : Abstracts.BaseUploadController<FileManagerFile>
    {
        //private readonly IFileManagerFacade _facade;
        //private readonly IService<FileManagerFile> _attachmentService;
        //private readonly IFileManagerDocumentService _documentService;

        public FileManagerFileController(/*IService<FileManagerFile> attachmentService, IFileManagerDocumentService documentService, */IFileManagerFacade facade)
            : base(facade)
        {
            /*_attachmentService = attachmentService;
            _documentService = documentService;
            _facade = facade;*/
        }

        //public IHttpActionResult Get(int id, bool inline = false)
        //{
        //    var file = _attachmentService.Find(m => m.Id == id);
        //    var document = _documentService.Get(file.ParentId);
        //    return RedirectToRoute("FileManagerFile", new { directoryId = document.DirectoryId, documentId = document.Id, attachmentId = id, inline = inline });
        //}

        //public IHttpActionResult Delete(int id, bool inline = false)
        //{
        //    var file = _attachmentService.Find(m => m.Id == id);
        //    var document = _documentService.Get(file.ParentId);
        //    return RedirectToRoute("FileManagerFile", new { directoryId = document.DirectoryId, documentId = document.Id, attachmentId = id });
        //}
    }
}