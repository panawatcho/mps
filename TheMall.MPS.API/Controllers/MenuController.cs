﻿using System.Threading.Tasks;
using System.Web.Http;
using TheMall.MPS.API.Attributes;
using TheMall.MPS.API.Facades;

namespace TheMall.MPS.API.Controllers
{
    [AllowAuthenticated]
    [RoutePrefix("api/menu")]
    public class MenuController : ApiController
    {
        private readonly IMenuFacade _facade;

        public MenuController(IMenuFacade facade)
        {
            _facade = facade;
        }

        public async Task<IHttpActionResult> Get()
        {
            return Ok(await _facade.GetAuthorized(User.Identity));
        }
    }
}
