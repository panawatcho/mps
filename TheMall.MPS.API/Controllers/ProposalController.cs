﻿using System;
using System.Collections.Generic;
using System.EnterpriseServices;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Routing;
using TheMall.MPS.API.Controllers.Abstracts;
using TheMall.MPS.API.Facades;
using TheMall.MPS.API.Mappers;
using TheMall.MPS.API.Services;
using TheMall.MPS.Models.Enums;
using TheMall.MPS.Models.Proposal;
using TheMall.MPS.ViewModels.Master;
using TheMall.MPS.ViewModels.Proposal;
using WebApi.OutputCache.V2;

namespace TheMall.MPS.API.Controllers
{
    [Attributes.AllowAuthenticated]
    [RoutePrefix("api/proposal")]
    public class ProposalController :
        BaseMpsWorkflowController<MPS_ProposalTable, MPS_ProposalAttachment, MPS_ProposalComment, ProposalViewModel>
    {
        private readonly IProposalFacade _facade;
        private readonly IEmployeeTableService _employee;
        private readonly IFinalApproverService _finalApproverService;
        private readonly IBudgetDepositTransFacade _budgetDepositTransFacade;
        private readonly IProposalLogFacade _proposalLogFacade;
        public ProposalController(IProposalFacade facade,IEmployeeTableService employee, 
            IFinalApproverService finalApproverService, 
            IBudgetDepositTransFacade budgetDepositTransFacade,
            IProposalLogFacade proposalLogFacade)
            : base(facade)
        {
            _facade = facade;
            _employee = employee;
            _finalApproverService = finalApproverService;
            _budgetDepositTransFacade = budgetDepositTransFacade;
            _proposalLogFacade = proposalLogFacade;
        }


        [Route("{id}")]
        [HttpGet]
        public async Task<IHttpActionResult> Get(int id)
        {
            return Ok(await _facade.GetViewModel(Url, id));
        }
        
        [Route("")]
        [HttpPost]
        public async Task<IHttpActionResult> Post(ProposalViewModel viewModel)
        {
            var model = await _facade.SaveAsync(viewModel);
            var viewModels = await _facade.GetViewModel(Url, model.Id);
            return Ok(viewModels);
        }

        [Route("deposit")]
        [HttpGet]
        public IHttpActionResult GetDeposit(string code = "", string name = "")
        {
            var viewModel = _facade.GetDepositNumber(code, name);
            return Ok(viewModel);
        }


        [Route("getProposalLineData/{UnitCode}/{parentId}")]
        [HttpGet]
        public IHttpActionResult GetProposalLineData(string UnitCode, int parentId)
        {
            var viewModel = _facade.GetProposalLineData(UnitCode, parentId);
            return Ok(viewModel);
        }


        [Route("getProposalDataFromLineId/{id}")]
        [HttpGet]
        public IHttpActionResult GetProposalDataFromLineId(int id)
        {
            var viewModel = _facade.GetProposalOneLineFromId(id);
            return Ok(viewModel);
        }


        [Route("{id}/startprocess")]
        [HttpPost]
        public async Task<IHttpActionResult> StartProcess(int id)
        {
            var procInstId = await _facade.StartProcess(id);

            return Ok(procInstId);
        }


        [Route("approve/{budget}/{id}/{unitcode}")]
        [HttpGet]
        public IHttpActionResult GetFinalApproval(decimal budget, int id, string unitcode)
        {
            if (id > 0)
            {
                _facade.SetFinalApproverDeleted(id);
            }

            var finalApprover = _finalApproverService.GetFinalApprover(budget, Budget.DocumentType.Proposal, unitcode).ToProposalApproval();
            return Ok(finalApprover);
        }


        [Route("closeproposal/{id}/{username}")] //
        [HttpGet]
        public  IHttpActionResult CloseProposal(int id, string username) //
        {
            var close = _facade.CloseProposal(id, username); //
            return Ok(close);
        }


        [Route("logdep")]
        [HttpGet]
        public IHttpActionResult Logdep(int id)
        {
            var close = _budgetDepositTransFacade.CancelProposalDepositTrans(id);
            return Ok(close);
        }


        [Route("reviseproposal")]
        [HttpGet]
        public IHttpActionResult ReviseProposal(int id)
        {
            var revise = _proposalLogFacade.ReviseProposal(id);
            return Ok(revise);
        }


        [Route("deleteby/{id}/{username}")]
        [HttpGet]
        public IHttpActionResult DeleteBy(int id, string username)
        {
            var result = _facade.DeleteBy(id, username);
            return Ok(result); ;
        }

        [Route("cancelrevise")]
        [HttpGet]
        public IHttpActionResult CancelRevise(int id, bool log)
        {
            var revise = _proposalLogFacade.CancelRevise(id, log);
            return Ok(revise);
        }
    }
}
