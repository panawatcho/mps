﻿using System.Linq;
using System.Net.Http;
using System.Web.Http;
using TheMall.MPS.API.Controllers.Abstracts;
using TheMall.MPS.API.Facades;
using TheMall.MPS.API.Services;
using TheMall.MPS.Reports.BudgetTracking;

namespace TheMall.MPS.API.Controllers
{
    [RoutePrefix("api/reportbudgettracking")]
    public class ReportBudgetTrackingController : BaseReportController
    {
        private readonly IReportService _reportService;
        private readonly IBudgetTrackingFacad _budgetTrackingFacad;
        
        public ReportBudgetTrackingController(
            IReportService reportService,
            IBudgetTrackingFacad budgetTrackingFacad)
            : base(reportService)
        {
            _reportService = reportService;
            _budgetTrackingFacad = budgetTrackingFacad;
        }

        [Route("budgettracking/{username}")]
        public HttpResponseMessage Post(BudgetTrackingFormCriteria criteria, string username)
        {
            criteria.Format = "XLS";
            _reportService.FileName = "BudgetTracking";
            _reportService.ReportName = "BudgetTrackingForm";
            _reportService.ReportPath = "BudgetTracking";

            var list = _budgetTrackingFacad.ReportData(username).ToList();
            var dataSource = new Microsoft.Reporting.WebForms.ReportDataSource("BudgetTracking", list);

            _reportService.DataSources = new[] { dataSource };

            return GenerateReportStream(criteria);
        }
    }
}