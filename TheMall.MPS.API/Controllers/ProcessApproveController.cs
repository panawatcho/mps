﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using TheMall.MPS.API.Controllers.Abstracts;
using TheMall.MPS.API.Facades;
using TheMall.MPS.API.Facades.Abstracts;
using TheMall.MPS.API.Services;
using TheMall.MPS.Models.Master;
using TheMall.MPS.ViewModels.Master;

namespace TheMall.MPS.API.Controllers
{
    [Attributes.AllowAuthenticated]
    [RoutePrefix("api/processapprove")]
    public class ProcessApproveController : BaseMasterController<MPS_M_ProcessApprove, ProcessApproveViewModel>
    {
        private readonly IProcessApproveService _service;
        private readonly IProcessApproveFacade _facade;
        public ProcessApproveController(
            IProcessApproveFacade facade,
            IProcessApproveService service) : base(facade)
        {
            _service = service;
            _facade = facade;
        }

        public async Task<IHttpActionResult> Get()
        {
            return Ok(await _service.GetAllAsyns());
        }


        [HttpGet]
        [Route("search/{processCode}")]
        public async Task<IHttpActionResult> Search(string processCode = null)
        {
            var viewModel = _facade.Search(processCode);
            return Ok(viewModel);
        }

    }
}