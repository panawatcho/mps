﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Microsoft.Reporting.WebForms;
using TheMall.MPS.API.Controllers.Abstracts;
using TheMall.MPS.API.Facades;
using TheMall.MPS.API.Mappers;
using TheMall.MPS.API.Services;
using TheMall.MPS.Reports.Proposal;

namespace TheMall.MPS.API.Controllers
{
     [RoutePrefix("api/reportproposal")]
    public class ReportProposalController : BaseReportController
    {
         private readonly IReportService _reportService;
         private readonly IProposalService _proposalService;
         private readonly IReportProposalFacade _reportProposalFacade;
         private readonly IProposalMapper _proposalMapper;
         public ReportProposalController(
             IReportService reportService, 
             IProposalService proposalService, 
             IReportProposalFacade reportProposalFacade, 
             IProposalMapper proposalMapper) : 
             base(reportService)
         {
             _reportService = reportService;
             _proposalService = proposalService;
             _reportProposalFacade = reportProposalFacade;
             _proposalMapper = proposalMapper;
         }


         [Route("proposal")]
         public HttpResponseMessage Post(ProposalFormCriteria criteria)
         {
             _reportService.ReportName = "ProposalForm";
             _reportService.ReportPath = "Proposal";

             var proposalModel = _proposalService.FirstOrDefault(m => m.Id == criteria.Id);
             var proposalViewModel = _proposalMapper.ToViewModelForReport(proposalModel);
             if (proposalModel != null)
             {
                var list = _reportProposalFacade.ReportData(proposalModel);
                var approvers = _reportProposalFacade.ApproversData(proposalModel.ProposalApproval);
                var lines = _reportProposalFacade.ProposalLineData(proposalViewModel);
                var deposit = _reportProposalFacade.ProposalDepositData(proposalViewModel);
                var dataSource = new Microsoft.Reporting.WebForms.ReportDataSource("ProposalDataSet", list);
                var approversDataSource = new Microsoft.Reporting.WebForms.ReportDataSource("ApproversDataSet", approvers);
                var lineDataSource = new Microsoft.Reporting.WebForms.ReportDataSource("ProposalLineDataSet", lines);
                var depositDataSource = new Microsoft.Reporting.WebForms.ReportDataSource("DepositDataSet", deposit);

                var filePath = @"file:\"+ System.Web.Hosting.HostingEnvironment.MapPath("~/ImageApprovers/");
                var reportParams = new List<ReportParameter>();
                reportParams.Add(new ReportParameter { Name = "PathImages", Values = { filePath} });
                _reportService.Parameters = reportParams;
                _reportService.DataSources = new[] { dataSource, approversDataSource, lineDataSource, depositDataSource };
                _reportService.FileName = " Proposal-No." + proposalModel.DocumentNumber + "- Date_" + DateTime.Now.ToString("dd_MM_yy");
                _reportService.EnableExternalImages = true;
                 return GenerateReportStream(criteria);
             }
             return null;
         }
    }
}