﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using TheMall.MPS.API.Controllers.Abstracts;
using TheMall.MPS.API.Facades;
using TheMall.MPS.API.Facades.Abstracts;
using TheMall.MPS.API.Mappers;
using TheMall.MPS.API.Services;
using TheMall.MPS.Infrastructure;
using TheMall.MPS.Models.Master;
using TheMall.MPS.ViewModels.Master;
using WebApi.OutputCache.V2;

namespace TheMall.MPS.API.Controllers
{
    [Attributes.AllowAuthenticated]
    [RoutePrefix("api/sharedbranch")]
    public class SharedBranchController : BaseMasterController<MPS_M_SharedBranch,SharedBranchViewModel>
    {
        private readonly ISharedBranchFacade _facade;
        private readonly ISharedBranchService _service;

        public SharedBranchController(
            ISharedBranchFacade facade, 
            ISharedBranchService service)
            : base(facade)
        {
            _facade = facade;
            _service = service;
        }
       
        // [CacheOutput(ClientTimeSpan = Constants.CacheTime, ServerTimeSpan = Constants.ServerCacheTime)]
        public async Task<IHttpActionResult> Get()
        {
            return Ok((await _service.GetAllAsyns()).ToViewModels().OrderBy(m=>m.Order));
        }
      //  [CacheOutput(ClientTimeSpan = Constants.CacheTime, ServerTimeSpan = Constants.ServerCacheTime)]
        public async Task<IHttpActionResult> GetById(string id)
        {
            return Ok((await _service.GetAsync(id)).ToViewModel());
        }
        [HttpGet]
        [Route("search")]
        public async Task<IHttpActionResult> Search(string code = null, string name = null)
        {
            return Ok((await _service.Search(code, name)));
        }
        [HttpGet]
        [Route("getwithpercent")]
        public async Task<IHttpActionResult> GetSharedBranch()
        {
            return Ok((await _facade.GetViewModel()).OrderBy(m => m.Order));
        }

        [HttpGet]
        [Route("SearchByBranchCode/{branchCode}")]
        public async Task<IHttpActionResult> SearchByBranchCode(string branchCode = null)
        {
            return Ok((_facade.GetByBranchCode(branchCode)));
        }
    }
}