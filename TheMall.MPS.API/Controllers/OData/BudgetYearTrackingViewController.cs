﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.OData;
using TheMall.MPS.API.Controllers.Abstracts;
using TheMall.MPS.API.Services;
using TheMall.MPS.Models.View;

namespace TheMall.MPS.API.Controllers.OData
{
    [AllowAnonymous]
    public class BudgetYearTrackingViewController : BaseODataController
    {
        private readonly IBudgetYearTrackingViewService _service;
        public BudgetYearTrackingViewController(IBudgetYearTrackingViewService service)
        {
            _service = service;
        }

        [EnableQuery]
        public IQueryable<MPS_VW_BudgetYearTracking> Get()
        {
            return _service.ODataQueryable();
        }
    }
}