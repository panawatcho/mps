﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.OData;
using TheMall.MPS.API.Controllers.Abstracts;
using TheMall.MPS.API.Services;
using TheMall.MPS.Models.Master;
using TheMall.MPS.Models.Memo;
using TheMall.MPS.Models.Proposal;
using TheMall.MPS.Models.Receipt;

namespace TheMall.MPS.API.Controllers.OData
{
    // get data to index(view)
    [AllowAnonymous]
    public class ReceiptTableController : BaseODataController
    {
        private readonly IReceiptService _service;
        public ReceiptTableController(IReceiptService service)
        {
            _service = service;
        }

        [EnableQuery]
        public IQueryable<MPS_ReceiptTable> Get()
        {
            return _service.ODataQueryable();
        }
    }
}
