﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.OData;
using TheMall.MPS.API.Controllers.Abstracts;
using TheMall.MPS.API.Services;
using TheMall.MPS.Models.Master;
using TheMall.MPS.Models.MemoIncome;
using TheMall.MPS.Models.Proposal;

namespace TheMall.MPS.API.Controllers.OData
{
    [AllowAnonymous]
    public class MemoIncomeTableController : BaseODataController
    {
        private readonly IMemoIncomeService _service;

        public MemoIncomeTableController(IMemoIncomeService service)
        {
            _service = service;
        }

        [EnableQuery]
        public IQueryable<MPS_MemoIncomeTable> Get()
        {
            return _service.ODataQueryable();
        }
    }
}
