﻿using System.Linq;
using System.Web.Http;
using System.Web.Http.OData;
using TheMall.MPS.API.Services;
using TheMall.MPS.Models.Example;

namespace TheMall.MPS.API.Controllers.OData
{
    [AllowAnonymous]
    public class ExampleTableController : Abstracts.BaseODataController
    {
        private readonly IExampleService _service;

        public ExampleTableController(IExampleService service)
        {
            _service = service;
        }

        [EnableQuery]
        public IQueryable<ExampleTable> Get()
        {
            return _service.ODataQueryable();
        }

    }
}