﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.OData;
using TheMall.MPS.API.Controllers.Abstracts;
using TheMall.MPS.API.Services;
using TheMall.MPS.Models.Master;

namespace TheMall.MPS.API.Controllers.OData
{
    [AllowAnonymous]
    public class FinalApproverTableController : BaseODataController
    {
        private readonly IFinalApproverService _service;
        public FinalApproverTableController(IFinalApproverService service)
        {
            _service = service;
        }

        [EnableQuery]
        public IQueryable<MPS_M_FinalApprover> Get()
        {
            return _service.ODataQueryable();
        }
    }
}