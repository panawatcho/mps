﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.OData;
using TheMall.MPS.API.Controllers.Abstracts;
using TheMall.MPS.API.Services;
using TheMall.MPS.Models.Master;

namespace TheMall.MPS.API.Controllers.OData
{
    [AllowAnonymous]
    public class ExpenseTopicTableController : BaseODataController
    {
        private readonly IExpenseTopicService _service;
        public ExpenseTopicTableController(IExpenseTopicService service)
        {
            _service = service;
        }

        [EnableQuery]
        public IQueryable<MPS_M_ExpenseTopic> Get()
        {
            return _service.ODataQueryable().OrderByDescending(m => m.ExpenseTopicCode);
        }
    }
}
