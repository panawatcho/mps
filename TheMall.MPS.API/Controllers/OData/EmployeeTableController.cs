﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.OData;
using TheMall.MPS.API.Controllers.Abstracts;
using TheMall.MPS.API.Services;
using TheMall.MPS.Models;

namespace TheMall.MPS.API.Controllers.OData
{
    public class EmployeeTableController : BaseODataController
    {
        private readonly IEmployeeTableService _employeeTableService;

        public EmployeeTableController(IEmployeeTableService employeeTableService)
        {
            _employeeTableService = employeeTableService;
        }

        [EnableQuery]
        public IQueryable<MPS_M_EmployeeTable> Get()
        {
            return _employeeTableService.ODataQueryable().OrderByDescending(m => m.EmpId);
        }
    }
}