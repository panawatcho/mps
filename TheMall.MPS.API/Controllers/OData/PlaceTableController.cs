﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.OData;
using TheMall.MPS.API.Controllers.Abstracts;
using TheMall.MPS.API.Services;
using TheMall.MPS.Models.Master;

namespace TheMall.MPS.API.Controllers.OData
{
    [AllowAnonymous]
    public class PlaceTableController : BaseODataController
    {
        private readonly IPlaceService _service;
        public PlaceTableController(IPlaceService service)
        {
            _service = service;
        }

        [EnableQuery]
        public IQueryable<MPS_M_Place> Get()
        {
            return _service.ODataQueryable().OrderBy(m => m.Order);
        }
    }
}