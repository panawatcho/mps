﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.OData;
using TheMall.MPS.API.Controllers.Abstracts;
using TheMall.MPS.API.Services;
using TheMall.MPS.Models.Master;

namespace TheMall.MPS.API.Controllers.OData
{
    public class SharedBranchTableController : BaseODataController
    {
        private readonly ISharedBranchService _service;
        public SharedBranchTableController(ISharedBranchService service)
        {
            _service = service;
        }

        [EnableQuery]
        public IQueryable<MPS_M_SharedBranch> Get()
        {
            return _service.ODataQueryable().OrderBy(m => m.Order);
        }
    }
}