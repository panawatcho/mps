﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.OData;
using TheMall.MPS.API.Controllers.Abstracts;
using TheMall.MPS.API.Services;
using TheMall.MPS.Models.Master;

namespace TheMall.MPS.API.Controllers.OData
{
    [AllowAnonymous]
    public class APcodeTableController : BaseODataController
    {
        private readonly IAPcodeService _service;
        public APcodeTableController(IAPcodeService service)
        {
            _service = service;
        }

        [EnableQuery]
        public IQueryable<MPS_M_APCode> Get()
        {
            return _service.ODataQueryable().OrderBy(m => m.Order);
        }
    }
}
