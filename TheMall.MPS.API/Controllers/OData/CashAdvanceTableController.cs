﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.OData;
using TheMall.MPS.API.Controllers.Abstracts;
using TheMall.MPS.API.Services;
using TheMall.MPS.Models.CashAdvance;

namespace TheMall.MPS.API.Controllers.OData
{
    [AllowAnonymous]
    public class CashAdvanceTableController : BaseODataController
    {
        private readonly ICashAdvanceService _service;
        public CashAdvanceTableController(ICashAdvanceService service)
        {
            _service = service;
        }

        [EnableQuery]
        public IQueryable<MPS_CashAdvanceTable> Get()
        {
            return _service.ODataQueryable();
        }
    }
}