﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.OData;
using TheMall.MPS.API.Controllers.Abstracts;
using TheMall.MPS.API.Services;
using TheMall.MPS.Models.Master;

namespace TheMall.MPS.API.Controllers.OData
{
    public class UnitCodeForEmployeeTableController : BaseODataController
    {
         private readonly IUnitCodeForEmployeeMasterService _service;
         public UnitCodeForEmployeeTableController(IUnitCodeForEmployeeMasterService service)
        {
            _service = service;
        }

        [EnableQuery]
        public IQueryable<MPS_M_UnitCodeForEmployee> Get()
        {
            return _service.ODataQueryable().OrderByDescending(m => m.UnitCode);
        }
    }
}