﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.OData;
using TheMall.MPS.API.Controllers.Abstracts;
using TheMall.MPS.API.Services;
using TheMall.MPS.API.Services;
using TheMall.MPS.Models.View;

namespace TheMall.MPS.API.Controllers.OData
{
    [AllowAnonymous]
    public class ProposalTrackingViewController : BaseODataController
    {
        private readonly IProposalTrackingViewService _service;
        public ProposalTrackingViewController(IProposalTrackingViewService service)
        {
            _service = service;
        }

        [EnableQuery]
        public IQueryable<MPS_VW_ProposalTracking> Get()
        {
            return _service.ODataQueryable();
        }
    }
}