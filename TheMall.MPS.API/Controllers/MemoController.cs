﻿using System;
using System.Collections.Generic;
using System.EnterpriseServices;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Routing;
using CrossingSoft.Framework.Infrastructure.K2;
using TheMall.MPS.API.Controllers.Abstracts;
using TheMall.MPS.API.Facades;
using TheMall.MPS.API.Facades.Interfaces;
using TheMall.MPS.API.Mappers;
using TheMall.MPS.API.Services;
using TheMall.MPS.Models.Enums;
using TheMall.MPS.Models.Memo;
using TheMall.MPS.Models.Proposal;
using TheMall.MPS.ViewModels.Master;
using TheMall.MPS.ViewModels.Memo;
using TheMall.MPS.ViewModels.Proposal;
using WebApi.OutputCache.V2;
using Constants = TheMall.MPS.Infrastructure.Constants;

namespace TheMall.MPS.API.Controllers
{
    [Attributes.AllowAuthenticated]
    [RoutePrefix("api/memo")]
    public class MemoController : BaseMpsWorkflowController<MPS_MemoTable, MPS_MemoAttachment, MPS_MemoComment, MemoViewModel>
    {
        private readonly IMemoFacade _facade;
        private readonly IMemoService _service;
        private readonly IEmployeeTableService _employee;
        private readonly IFinalApproverService _finalApproverService;
        public MemoController(IMemoFacade facade,
            IEmployeeTableService employee, 
            IFinalApproverService finalApproverService)
            : base(facade)
        {
            _facade = facade;
            _employee = employee;
            _finalApproverService = finalApproverService;
        }

        [Route("{id}")]
        [HttpGet]
        public async Task<IHttpActionResult> Get( int id)
        {
            return Ok(await _facade.GetViewModel(Url,id));
        }

        [Route("")]
        [HttpPost]
        public async Task<IHttpActionResult> Post(MemoViewModel viewModel)
        {
            var model = await _facade.SaveAsync(viewModel);
            return Ok(model);
        }

        [Route("getdatabindmemo/{propLineid}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetDataBindMemo(int propLineid)
        {
            var viewModel = await _facade.GetDataBindMemo(propLineid);

            return Ok(viewModel);
        }
        [Route("{id}/startprocess")]
        [HttpPost]
        public async Task<IHttpActionResult> StartProcess(int id)
        {
            var procInstId = await _facade.StartProcess(id);

            return Ok(procInstId);
        }

        [Route("related")]
        [HttpGet]
        public async Task<IHttpActionResult> GetRelatedAll()
        {
            return Ok(await _facade.GetRelatedWorkflowTable());
        }

        [Route("approve")]
        [HttpGet]
        public IHttpActionResult GetApproval()
        {
            var viewModel = new MemoApprovalViewModel()
            {
                Employee = _employee.FindByUsername("Tester06").ToViewModel(),
                ApproverSequence = 1,
                ApproverLevel = "อนุมัติขั้นสุดท้าย",
                ApproverUserName = "Tester06",
                ApproverFullName = "",
                ApproverEmail = "Tester06@gmail.com",
                Position = "รองประธานการตลาด",
                Department = "ฝ่าย CORPORATE PROMOTION",
                DueDate = DateTime.Now,
                LineNo = 1

            };
            return Ok(viewModel);
        }

        [Route("getfinalapprover/{budget}/{id}/{unitcode}/{type}")]
        [HttpGet]
        public IHttpActionResult GetFinalApprover(decimal budget, int id, string unitcode,string type)
        {
            string process = "memo";
            if (id > 0)
            {
                _facade.SetFinalApproverDeleted(id);

            }
            string memoType = type;
            if (memoType.ToLower() != "normal")
            {
                process = String.Concat(process, memoType).Trim();
            }
            var finalApprover = _finalApproverService.GetFinalApprover(budget, process , unitcode).ToMemoApproval() ??
                                _finalApproverService.GetFinalApprover(budget, "memo", unitcode).ToMemoApproval();
            return Ok(finalApprover);
        }


        [Route("deleteby/{id}/{username}")]
        [HttpGet]
        public IHttpActionResult DeleteBy(int id, string username)
        {
            var result = _facade.DeleteBy(id, username);
            return Ok(result); ;
        }
    }
}

