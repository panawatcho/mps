﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using TheMall.MPS.API.Attributes;
using TheMall.MPS.API.Controllers.Abstracts;
using TheMall.MPS.API.Facades;
using TheMall.MPS.API.Services;
using TheMall.MPS.Models.Receipt;
using TheMall.MPS.ViewModels.Master;
using TheMall.MPS.ViewModels.Receipt;

namespace TheMall.MPS.API.Controllers
{
    [AllowAuthenticated]
    [RoutePrefix("api/receipt")]
    public class ReceiptController : BaseMasterController<MPS_ReceiptTable, ReceiptViewModel>
    {
        private readonly IReceiptService _receiptService;
        private readonly IReceiptFacade _receiptFacade;
        
        public ReceiptController(
            IReceiptService receiptService,
            IReceiptFacade receiptFacade)
            : base(receiptFacade)
        {
            _receiptService = receiptService;
            _receiptFacade = receiptFacade;
        }

        // view(datasource) parameter create
        [HttpPost]
        [Route("createreceipt")]
        public async Task<IHttpActionResult> SavePost(ReceiptViewModel viewModel)
        {
            return Ok(await _receiptFacade.Create(viewModel));
        }

        // view(datasource) parameter get/id
        [HttpGet]
        [Route("get/{id}")]
        public  IHttpActionResult Get(int id)
        {
            return Ok((_receiptFacade.GetViewModel(id)));
        }

        [HttpGet]
        [Route("search/{empName}")]
        public IHttpActionResult Search(string empName = null)
        {
            return Ok((_receiptFacade.Search(empName)));
        }

        [Route("deleteby/{id}/{username}")]
        [HttpGet]
        public IHttpActionResult DeleteBy(int id, string username)
        {
            var result = _receiptFacade.DeleteBy(id, username);
            return Ok(result); ;
        }

    }
}