﻿using System;
using System.Collections.Generic;
using System.EnterpriseServices;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using TheMall.MPS.API.Controllers.Abstracts;
using TheMall.MPS.API.Facades;
using TheMall.MPS.API.Facades.Abstracts;
using TheMall.MPS.API.Mappers;
using TheMall.MPS.API.Services;
using TheMall.MPS.Infrastructure;
using TheMall.MPS.Models.Master;
using TheMall.MPS.ViewModels.Master;
using WebApi.OutputCache.V2;

namespace TheMall.MPS.API.Controllers
{
    [Attributes.AllowAuthenticated]
    [RoutePrefix("api/typeproposal")]
    public class TypeProposalController : BaseMasterController<MPS_M_TypeProposal,TypeProposalViewModel>
    {
        private readonly ITypeProposalService _service;

        public TypeProposalController(ITypeProposalFacade facade, ITypeProposalService service) 
            : base(facade)
        {
            _service = service;
        }

       // [CacheOutput(ClientTimeSpan = Constants.CacheTime, ServerTimeSpan = Constants.ServerCacheTime)]
        public async Task<IHttpActionResult> Get()
        {
            return Ok((await _service.GetAllAsyns()).ToViewModels());
        }
        //[CacheOutput(ClientTimeSpan = Constants.CacheTime, ServerTimeSpan = Constants.ServerCacheTime)]
        public async Task<IHttpActionResult> GetById(string id)
        {
            return Ok((await _service.GetAsync(id)).ToViewModel());
        }
        [HttpGet]
        [Route("search")]
        public async Task<IHttpActionResult> Search(string code = null, string name = null)
        {
            return Ok((await _service.Search(code, name)));
        }

        [HttpGet]
        [Route("searchbyproposalcodetype/{proposalTypeCode}")]
        public async Task<IHttpActionResult> SearchByProposalCodeType(string proposalTypeCode = null)
        {
            return Ok(_service.FirstOrDefault(m=>m.TypeProposalCode.Equals(proposalTypeCode,StringComparison.InvariantCultureIgnoreCase)).ToViewModel());
        }
    }
}
