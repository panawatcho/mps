﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using CrossingSoft.Framework.Infrastructure.Session;
using Microsoft.Reporting.WebForms;
using TheMall.MPS.API.Controllers.Abstracts;
using TheMall.MPS.API.Facades;
using TheMall.MPS.API.Mappers;
using TheMall.MPS.API.Services;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Models.Receipt;
using TheMall.MPS.Reports.CashAdvance;
using TheMall.MPS.Reports.Receipt;
using TheMall.MPS.Reports.Tests;

namespace TheMall.MPS.API.Controllers
{
    [RoutePrefix("api/reportreceipt")]
    public class ReportReceiptController : BaseReportController
    {
        private readonly IMPSSession _session;
        private readonly IReportService _reportService;
        private readonly IReceiptService _receiptService;
        private readonly IEmployeeTableService _employeeTableService;

        public ReportReceiptController(IReportService reportService, 
            IReceiptService receiptService, 
            IEmployeeTableService employeeTableService,
            IMPSSession session) 
            : base(reportService)
        {
            _reportService = reportService;
            _receiptService = receiptService;
            _employeeTableService = employeeTableService;
            _session = session;
        }

        [Route("receipt")]
        public HttpResponseMessage Post(ReceiptFormCriteria criteria)
        {
            try{
                
                var model = _receiptService.Find(m => m.Id == criteria.Id);
                model.RePrintNo = (model.RePrintNo != null) ? model.RePrintNo + 1 : 1;
                var list = ReceiptToDataSources(model);
                var receiptlinedata = ReceiptLineToDataSources(model);
                var Receipt = new Microsoft.Reporting.WebForms.ReportDataSource("Receipt", list);
                var ReceiptLine = new Microsoft.Reporting.WebForms.ReportDataSource("ReceiptLine", receiptlinedata);
                var reportParams = new List<ReportParameter>();

                reportParams.Add(new ReportParameter { Name = "RePrintNo", Values = { model.RePrintNo.ToString() } });
             
                _reportService.ReportName = "ReceiptForm";
                _reportService.ReportPath = "Receipt";
                _reportService.Parameters = reportParams;
                _reportService.DataSources = new[] { Receipt, ReceiptLine };

                _reportService.FileName = "Receipt";

                _receiptService.Update(model, model.Id);
                _session.SaveChanges();
                return GenerateReportStream(criteria); 
             }
            catch (Exception ex)
            {
                throw new HttpException(ex.Message);
            }
        }

        public List<ReceiptDataSource> ReceiptToDataSources (MPS_ReceiptTable model)
        {
            var list = new List<ReceiptDataSource>();
            if (model != null)
            {
                var reportDataSource = new ReceiptDataSource();

                reportDataSource.Id = reportDataSource.Id;
                reportDataSource.RequesterName = model.RequesterName;
                reportDataSource.RequesterUserName = model.RequesterUserName;
                //
                var requesterPosition = _employeeTableService.FindByUsername(model.RequesterUserName);
                reportDataSource.RequesterPosition = (!string.IsNullOrEmpty(requesterPosition.PositionName))? "- " + requesterPosition.PositionName : "";
                reportDataSource.UnitCode = model.UnitCode;
                reportDataSource.UnitName = model.UnitName;
                //
                if (string.IsNullOrEmpty(model.RequestForUserName))
                {
                    reportDataSource.RequesterForName = model.RequesterName;
                    reportDataSource.RequestForUserName = model.RequesterUserName;
                    reportDataSource.RequesterForPosition = reportDataSource.RequesterPosition;
                }
                else
                {
                    reportDataSource.RequesterForName = model.RequesterForName;
                    reportDataSource.RequestForUserName = model.RequestForUserName;
                    var requesterForPosition = _employeeTableService.FindByUsername(model.RequestForUserName);
                    reportDataSource.RequesterForPosition = (!string.IsNullOrEmpty(requesterForPosition.PositionName))? "- " + requesterForPosition.PositionName : "";
                }
                //
                if (string.IsNullOrEmpty(model.UnitCodeFor))
                {
                    reportDataSource.UnitCodeFor = model.UnitCode;
                    reportDataSource.UnitNameFor = model.UnitName;
                }
                else
                {
                    reportDataSource.UnitCodeFor = model.UnitCodeFor;
                    reportDataSource.UnitNameFor = model.UnitNameFor;
                }
                //
                reportDataSource.BudgetDetail = model.BudgetDetail;
                reportDataSource.BudgetInCharacter = reportDataSource.BudgetDetail.ToWords();
                //
                reportDataSource.BudgeNoVatTax = model.BudgetNoVatTax;
                reportDataSource.BudgetNoVatTaxInCharacter = reportDataSource.BudgeNoVatTax.ToWords();
                //
                reportDataSource.DocumentNumber = model.DocumentNumber;
                reportDataSource.RePrintNo = model.RePrintNo;
                DateTime submitDate = (DateTime)model.CompletedDate;
                reportDataSource.CompletedDate = submitDate.ToString("dd/MM/yyyy");
                reportDataSource.PrintDate = DateTime.Now.ToString("dd/MM/yyyy");

                list.Add(reportDataSource);
            }

            return list;
        }

        public List<ReceiptLineDataSource> ReceiptLineToDataSources(MPS_ReceiptTable model)
        {
            var list = new List<ReceiptLineDataSource>();
            if (model != null)
            {
                foreach (var line in model.ReceiptLine)
                {
                    var reportDataSource = new ReceiptLineDataSource();
                    reportDataSource.Unit = line.Unit;
                    reportDataSource.Amount = line.Amount;
                    reportDataSource.Description = line.Description;
                    reportDataSource.NetAmount = line.NetAmount;
                    reportDataSource.NetAmountNoVatTax = line.NetAmountNoVatTax;
                    reportDataSource.Remark = line.Remark;
                    reportDataSource.Tax = line.Tax;
                    reportDataSource.Vat = line.Vat;

                    list.Add(reportDataSource);
                }
            }

            return list;
        }
    }
}