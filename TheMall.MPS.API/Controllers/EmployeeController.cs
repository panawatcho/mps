﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using TheMall.MPS.API.Attributes;
using TheMall.MPS.API.Controllers.Abstracts;
using TheMall.MPS.API.Facades;
using TheMall.MPS.API.Mappers;
using TheMall.MPS.API.Services;
using TheMall.MPS.Models;
using TheMall.MPS.ViewModels;
using WebApi.OutputCache.V2;

namespace TheMall.MPS.API.Controllers
{
    [AllowAuthenticated]
    [RoutePrefix("api/employee")]
    public class EmployeeController : BaseMasterController<MPS_M_EmployeeTable, EmployeeViewModel> //BaseApiController
    {
        private readonly IEmployeeTableService _service;
        private readonly IUnitCodeService _unitCode;
        private readonly IUnitCodeForEmployeeService _unitCodeforemp;
        private readonly IEmployeeFacade _employeeFacade;
        private readonly IEmailViewService _emailViewService;
        public EmployeeController(
            IEmployeeTableService service, IUnitCodeService unitCode, 
            IUnitCodeForEmployeeService unitCodeforemp, 
            IEmployeeFacade employeeFacade,
            IEmailViewService emailViewService)
            : base(employeeFacade)
        {
            _service = service;
            _unitCode = unitCode;
            _unitCodeforemp = unitCodeforemp;
            _employeeFacade = employeeFacade;
            _emailViewService = emailViewService;
        }


        public async Task<IHttpActionResult> Get()
        {
            return Ok(await _service.GetAllAsync());
        }

        [HttpGet]
        [Route("search")]
        public async Task<IHttpActionResult> Search(string code = null, string name = null)
        {
          
            return Ok(( _service.Search(code, name).ToViewModel()));
        }

        [HttpGet]
        [Route("searchAll")]
        public async Task<IHttpActionResult> SearchAll(string query = "")
        {
            return Ok((await _service.SearchAllAsync(query)));
        }

        [HttpGet]
        [Route("accounting")]
        public async Task<IHttpActionResult> Accounting(string code = null, string name = null)
        {
            return Ok((_service.SearchAccounting(code, name).ToViewModel()));
        }

        [HttpGet]
        [Route("getByUser")]
        public async Task<IHttpActionResult> GetByUsername(string username)
        {
            return Ok(_employeeFacade.GetUserViewModel(username));
        }

        [HttpGet]
        [Route("searchbyemail")]
        public async Task<IHttpActionResult> SearchByEmail(string email = null)
        {
            var emailList = _emailViewService.SearchByEmail(email);
            //return Ok(emailList.Take(100));
            return Ok(emailList);
        }


        [HttpGet]
        [Route("searchbyid/{empId}")]
        public async Task<IHttpActionResult> SearchById(string empId = null)
        {
            var aEmployee = _employeeFacade.GetById(empId);
            return Ok(aEmployee);
        }

        [HttpGet]
        [Route("searchFor")]
        public async Task<IHttpActionResult> SearchFor(string code = null, string name = null)
        {
            return Ok((_employeeFacade.SearchRequestFor(code, name)));
        }
    }
}
