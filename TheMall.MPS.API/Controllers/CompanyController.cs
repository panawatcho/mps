﻿using System.Threading.Tasks;
using System.Web.Http;
using TheMall.MPS.API.Attributes;
using TheMall.MPS.API.Controllers.Abstracts;
using TheMall.MPS.API.Facades;
using TheMall.MPS.API.Mappers;
using TheMall.MPS.API.Services;
using TheMall.MPS.Models.Master;
using TheMall.MPS.ViewModels.Master;

namespace TheMall.MPS.API.Controllers
{
    [AllowAuthenticated]
    [RoutePrefix("api/company")]
    public class CompanyController : BaseApiController
    {
        private readonly ICompanyService _companyService;
        private readonly ICompanyFacade _companyFacade;

        public CompanyController(ICompanyFacade companyFacade,
            ICompanyService companyService)
        {
            _companyFacade = companyFacade;
            _companyService = companyService;
        }

        [Route("")]
        [HttpGet]
        public async Task<IHttpActionResult> ShowAll()
        {
            return Ok((_companyService.GetAll()));
        }

        [Route("create")]
        [HttpPost]
        public async Task<IHttpActionResult> Create(CompanyViewModel company)
        {
            var accountingResult = _companyFacade.Create(company);
            return Ok(accountingResult);
        }

        [Route("Update")]
        [HttpPost]
        public async Task<IHttpActionResult> Update(CompanyViewModel company)
        {
            var accountingResult = _companyFacade.Update(company);
            return Ok(accountingResult);
        }

        [Route("DeleteInMaster/{branchCode}")]
        [HttpGet]
        public async Task<IHttpActionResult> Delete(string branchCode)
        {
            return Ok(_companyFacade.DeleteInMaster(branchCode));
        }

        [Route("ShowVendor/{branchCode}")]
        [HttpGet]
        public async Task<IHttpActionResult> ShowVendor(string branchCode)
        {
            return Ok((_companyService.GetWhere(m => m.BranchCode == branchCode)));
        }

    }
}