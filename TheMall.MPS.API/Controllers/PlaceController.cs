﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using TheMall.MPS.API.Controllers.Abstracts;
using TheMall.MPS.API.Facades;
using TheMall.MPS.API.Facades.Abstracts;
using TheMall.MPS.API.Mappers;
using TheMall.MPS.API.Services;
using TheMall.MPS.Infrastructure;
using TheMall.MPS.Models.Master;
using TheMall.MPS.ViewModels.Master;
using WebApi.OutputCache.V2;

namespace TheMall.MPS.API.Controllers
{
    [Attributes.AllowAuthenticated]
    [RoutePrefix("api/place")]
    public class PlaceController : BaseMasterController<MPS_M_Place,PlaceViewModel>
    {
        private readonly IPlaceService _service;
        public PlaceController(IPlaceFacade facade , IPlaceService service) : base(facade)
        {
            _service = service;
        }
       
        public async Task<IHttpActionResult> Get()
        {
            return Ok((await _service.GetAllAsyns()).ToViewModels());
        }

        public async Task<IHttpActionResult> GetById(string id)
        {
            return Ok((await _service.GetAsync(id)).ToViewModel());
        }

        [HttpGet]
        [Route("search")]
        public async Task<IHttpActionResult> Search(string code = null, string name = null)
        {
            return Ok((await _service.Search(code, name)));
        }

        [HttpGet]
        [Route("getlist")]
        public  IHttpActionResult GetList()
        {
            return Ok(( _service.GetWhere(m => !m.InActive)).ToViewModels());
        }

        [HttpGet]
        [Route("getbyplacecode/{placeCode}")]
        public IHttpActionResult GetByPlaceCode(string placeCode = null)
        {
            return Ok((_service.FirstOrDefault(m => m.PlaceCode.Equals(placeCode,StringComparison.InvariantCultureIgnoreCase))).ToViewModel());
        }
    }
}