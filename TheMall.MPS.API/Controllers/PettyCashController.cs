﻿using System;
using System.Collections.Generic;
using System.EnterpriseServices;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Routing;
using TheMall.MPS.API.Controllers.Abstracts;
using TheMall.MPS.API.Facades;
using TheMall.MPS.API.Mappers;
using TheMall.MPS.API.Services;
using TheMall.MPS.Models.Enums;
using TheMall.MPS.Models.PettyCash;
using TheMall.MPS.ViewModels.Master;
using TheMall.MPS.ViewModels.PettyCash;
using TheMall.MPS.ViewModels.Proposal;
using WebApi.OutputCache.V2;

namespace TheMall.MPS.API.Controllers
{
    [Attributes.AllowAuthenticated]
    [RoutePrefix("api/pettycash")]
    public class PettyCashController : BaseMpsWorkflowController<MPS_PettyCashTable,MPS_PettyCashAttachment,MPS_PettyCashComment,PettyCashViewModel>
    {
        private readonly IPettyCashFacade _facade;
        private readonly IEmployeeTableService _employee;
        private readonly IFinalApproverService _finalApproverService;
        public PettyCashController(IPettyCashFacade facade, 
            IEmployeeTableService employee, 
            IFinalApproverService finalApproverService) 
            : base(facade)
        {
            _facade = facade;
            _employee = employee;
            _finalApproverService = finalApproverService;
        }

        [Route("{id}")]
        [HttpGet]
        public async Task<IHttpActionResult> Get(int id)
        {
            return Ok(await _facade.GetViewModel(Url,id));
        }
        [Route("related")]
        [HttpGet]
        public async Task<IHttpActionResult> GetRelatedAll()
        {
            return Ok(await _facade.GetRelatedWorkflowTable());
        }


        [Route("")]
        [HttpPost]
        public async Task<IHttpActionResult> Post(PettyCashViewModel viewModel)
        {
            var model = await _facade.SaveAsync(viewModel);
            return Ok(model);
        }

        [Route("getdatabindpettycash/{propLineid}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetDataBindPettyCash(int propLineid)
        {
            var viewModel = await _facade.GetDataBindPettyCash(propLineid);

            return Ok(viewModel);
        }
        
        [Route("{id}/startprocess")]
        [HttpPost]
        public async Task<IHttpActionResult> StartProcess(int id)
        {
            var procInstId = await _facade.StartProcess(id);

            return Ok(procInstId);
        }

        [Route("approve")]
        [HttpGet]
        public IHttpActionResult GetApproval()
        {
            var viewModel = new PettyCashApprovalViewModel()
            {
                Employee = _employee.FindByUsername("Tester06").ToViewModel(),
                ApproverSequence = 1,
                ApproverLevel = "อนุมัติขั้นสุดท้าย",
                ApproverUserName = "Tester06",
                ApproverFullName = "",
                ApproverEmail = "Tester06@gmail.com",
                Position = "รองประธานการตลาด",
                Department = "ฝ่าย CORPORATE PROMOTION",
                DueDate = DateTime.Now,
                LineNo = 1

            };
            return Ok(viewModel);
        }

        [Route("approve/{budget}/{id}/{unitcode}")]
        [HttpGet]
        public IHttpActionResult GetFinalApproval(decimal budget, int id ,string unitcode)
        {
            if (id > 0)
            {
               _facade.SetFinalApproverDeleted(id);
            }
    
            var finalApprover = _finalApproverService.GetFinalApprover(budget, Budget.DocumentType.PettyCash, unitcode).ToPettyCashApproval();
            //if (finalApprover==null)
            //    throw new Exception("ไม่พบผู้อนุมัติคนสุดท้าย กรุณาติดต่อ Admin ระบบ");
            return Ok(finalApprover);
        }

        [Route("deleteby/{id}/{username}")]
        [HttpGet]
        public IHttpActionResult DeleteBy(int id, string username)
        {
            var result = _facade.DeleteBy(id, username);
            return Ok(result); ;
        }
    }
}
