﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using TheMall.MPS.API.Attributes;
using TheMall.MPS.API.Controllers.Abstracts;
using TheMall.MPS.API.Facades;
using TheMall.MPS.API.Mappers;
using TheMall.MPS.API.Services;
using TheMall.MPS.ViewModels.Master;

namespace TheMall.MPS.API.Controllers
{
    [AllowAuthenticated]
    [RoutePrefix("api/accounting")]
    public class AccountingController : BaseApiController
    {
        private readonly IAccountingFacade _accountingFacade;
        private readonly IAccountingService _accountingService;
        public AccountingController( 
            IAccountingFacade accountingFacade, 
            IAccountingService accountingService)
        {
            _accountingFacade = accountingFacade;
            _accountingService = accountingService;
        }

        [Route("getaccountingbranch")]
        [HttpGet]
        public async Task<IHttpActionResult> GetAccountingBranch()
        {
           var accounting =  _accountingFacade.GetAccountingBranch();
           return Ok(accounting);
        }

        [Route("create")]
        [HttpPost]
        public async Task<IHttpActionResult> Create(AccountingViewModel accounting)
        {
            var accountingResult = _accountingFacade.Create(accounting);
            return Ok(accountingResult);
        }

        [Route("Update")]
        [HttpPost]
        public async Task<IHttpActionResult> Update(AccountingViewModel accounting)
        {
            var accountingResult = _accountingFacade.Update(accounting);
            return Ok(accountingResult);
        }

        [Route("DeleteInMaster/{branchCode}/{username}")]
        [HttpGet]
        public async Task<IHttpActionResult> Delete(string branchCode, string username)
        {
            return Ok(_accountingFacade.DeleteInMaster(branchCode, username));
        }

        [Route("ShowAll")]
        [HttpGet]
        public async Task<IHttpActionResult> ShowAll()
        {
            return Ok((_accountingService.GetAll()));
        }

        [HttpGet]
        [Route("searchBranchCodeAndUsername/{branchCode}/{username}")]
        public async Task<IHttpActionResult> SearchBranchCodeAndUsername(string branchCode = null, string username = null)
        {
            var viewModel = _accountingFacade.GetByBranchCodeAndUsername(branchCode,username);
            return Ok(viewModel);
        }
    }
}