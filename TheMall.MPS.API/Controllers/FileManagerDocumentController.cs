﻿using System.Web.Http;
using TheMall.MPS.API.Facades;
using TheMall.MPS.Infrastructure;
using TheMall.MPS.Models.FileManager;

namespace TheMall.MPS.API.Controllers
{
    [AllowAnonymous]
    [RoutePrefix("api/filemanager/directory/{directoryId}/document")]
    public class FileManagerDocumentController : Abstracts.BaseUploadController<FileManagerFile>
    {
        private readonly IFileManagerFacade _facade;

        public FileManagerDocumentController(IFileManagerFacade facade) : base(facade)
        {
            _facade = facade;
        }

        [Route("")]
        public IHttpActionResult Get(int directoryId)
        {
            return Ok(_facade.GetAllDocuments(directoryId, UserHelper.CurrentUserName()));
        }
    }
}