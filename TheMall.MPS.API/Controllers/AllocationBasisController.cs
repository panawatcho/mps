﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using TheMall.MPS.API.Attributes;
using TheMall.MPS.API.Controllers.Abstracts;
using TheMall.MPS.API.Facades;
using TheMall.MPS.API.Services;
using TheMall.MPS.Models.Master;
using TheMall.MPS.ViewModels.Master;

namespace TheMall.MPS.API.Controllers
{
    [AllowAuthenticated]
    [RoutePrefix("api/allocationbasis")]
    public class AllocationBasisController : BaseMasterController<MPS_M_AllocationBasis, AllocationBasisViewModel>
    {
        private readonly IAllocationBasisService _allocationBasisService;
        private readonly IAllocationBasisFacade _allocationBasisFacade;
        public AllocationBasisController(
            IAllocationBasisService allocationBasisService, 
            IAllocationBasisFacade allocationBasisFacade)
            : base(allocationBasisFacade)
        {
            _allocationBasisService = allocationBasisService;
            _allocationBasisFacade = allocationBasisFacade;
        }

        [HttpGet]
        [Route("get")]
        public async Task<IHttpActionResult> Get()
        {
            return Ok((await _allocationBasisService.GetAllAsyns()));
        }


        [HttpGet]
        [Route("search/{allocCode}")]
        public async Task<IHttpActionResult> Search(string allocCode = null)
        {
            return Ok((_allocationBasisFacade.Search(allocCode)));
        }

        [HttpGet]
        [Route("get/{id}")]
        public async Task<IHttpActionResult> Get(int id)
        {
            return Ok(( _allocationBasisService.FirstOrDefault(m=>m.Id == id)));
        }

    }
}