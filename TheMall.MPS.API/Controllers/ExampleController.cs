﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using TheMall.MPS.API.Controllers.Abstracts;
using TheMall.MPS.API.Facades;
using TheMall.MPS.API.Facades.Interfaces;
using TheMall.MPS.Models.Example;
using TheMall.MPS.ViewModels;
using TheMall.MPS.ViewModels.Example;

namespace TheMall.MPS.API.Controllers
{
    [RoutePrefix("api/example")]
    public class ExampleController : BaseUploadController<ExampleAttachment>
    {
        private readonly IExampleFacade _facade;
        public ExampleController(
            //IHaveAttachmentFacade<ExampleAttachment> attachmentFacade, IExampleFacade facade)
            IExampleFacade facade)
            : base(facade)
        {
            _facade = facade;
        }

        [Route("{id}")]
        public virtual async Task<IHttpActionResult> Get(string id)
        {
            return Ok(await _facade.GetViewModel(Url, id));
        }

        [Route("")]
        public virtual async Task<IHttpActionResult> PostCreate(ExampleViewModel viewModel)
        {
            return Ok(true);
        }

        [Route("{id}")]
        public virtual async Task<IHttpActionResult> PostUpdate(ExampleViewModel viewModel)
        {
            return Ok(_facade.ToViewModel(Url, await _facade.Update(viewModel)));
        }


        public IHttpActionResult Delete(int id)
        {
            //Facade.Delete(id);

            return Ok(true);
        }

        [HttpGet]
        [Route("search")]
        public async Task<IHttpActionResult> Search(string code = null, string name = null)
        {
            var docFile = new List<AttachmentViewModel>()
            {
                new AttachmentViewModel()
                {
                    id = 1,
                    name = "File 1"
                },
                new AttachmentViewModel()
                {
                    id = 2,
                    name = "File 2"
                },
                new AttachmentViewModel()
                {
                    id = 3,
                    name = "File 3"
                }
            };
            return Ok(docFile);
        }
    }
}