﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Microsoft.Reporting.WebForms;
using TheMall.MPS.API.Controllers.Abstracts;
using TheMall.MPS.API.Facades;
using TheMall.MPS.API.Mappers;
using TheMall.MPS.API.Services;
using TheMall.MPS.Infrastructure.Extension;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Models.Enums;
using TheMall.MPS.Models.MemoIncome;
using TheMall.MPS.Reports.MemoIncome;
using TheMall.MPS.Reports.Shared;

namespace TheMall.MPS.API.Controllers
{
    [RoutePrefix("api/reportmemoincome")]
    public class ReportMemoIncomeController : BaseReportController
    {
        private readonly IReportService _reportService;
        private readonly IMemoIncomeService _memoIncomeService;
        private readonly IEmployeeTableService _employeeTableService;
        private readonly IMPSSession _session;
        private readonly IProposalService _proposalService;
        private readonly IMemoIncomeMapper _memoIncomeMapper;
        private readonly IProposalIncomeOtherService _proposalIncomeOtherService;
        private readonly IProposalDepositLineService _proposalDepositLineService;

        public ReportMemoIncomeController(
            IReportService reportService,
            IMemoIncomeService memoIncomeService,
            IEmployeeTableService employeeTableService,
            IMPSSession session,
            IProposalService proposalService,
            IMemoIncomeMapper memoIncomeMapper,
            IProposalIncomeOtherService proposalIncomeOtherService,
            IProposalDepositLineService proposalDepositLineService)
            : base(reportService)
        {
            _reportService = reportService;
            _employeeTableService = employeeTableService;
            _memoIncomeService = memoIncomeService;
            _session = session;
            _proposalService = proposalService;
            _memoIncomeMapper = memoIncomeMapper;
            _proposalIncomeOtherService = proposalIncomeOtherService;
            _proposalDepositLineService = proposalDepositLineService;
        }

        [Route("memoincome")]
        public HttpResponseMessage Post(MemoIncomeFormCriteria criteria)
        {
            try
            {
                var model = _memoIncomeService.Find(m => m.Id == criteria.Id);

                if (model != null)
                {
                    model.RePrintNo = (model.RePrintNo != null) ? model.RePrintNo + 1 : 1;
                    var list = MemoIncomeToDataSources(model);
                    var memoincomelinedata = MemoIncomeLineToDataSources(model);
                    var appover = ApproversToDataSources(model.MemoIncomeApproval);

                    // ชื่อ "MemoIncome" ต้องกับชื่อ Datasets ใน form report
                    var MemoIncome = new Microsoft.Reporting.WebForms.ReportDataSource("MemoIncome", list);
                    var MemoIncomeLine = new Microsoft.Reporting.WebForms.ReportDataSource("MemoIncomeLine", memoincomelinedata);
                    var MemoIncomeApprovers = new Microsoft.Reporting.WebForms.ReportDataSource("MemoIncomeApproves", appover);

                    var reportParams = new List<ReportParameter>();
                    reportParams.Add(new ReportParameter { Name = "RePrintNo", Values = { model.RePrintNo.ToString() } });

                    _reportService.ReportName = "MemoIncomeForm";
                    _reportService.ReportPath = "MemoIncome";
                    _reportService.Parameters = reportParams;
                    _reportService.DataSources = new[] { MemoIncome, MemoIncomeLine, MemoIncomeApprovers };
                    _reportService.FileName = "MemoIncome";

                    _memoIncomeService.Update(model, model.Id);
                    _session.SaveChanges();
                    return GenerateReportStream(criteria);
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw new HttpException(ex.Message);
            }
        }

        public List<MemoIncomeDataSource> MemoIncomeToDataSources(MPS_MemoIncomeTable model)
        {
            var list = new List<MemoIncomeDataSource>();

            if (model != null)
            {
                var reportDataSource = new MemoIncomeDataSource();

                reportDataSource.Id = reportDataSource.Id;
                reportDataSource.RePrintNo = model.RePrintNo;
                reportDataSource.DocumentNumber = model.DocumentNumber;
                reportDataSource.ProposalNumber = model.ProposalRefDocumentNumber;

                var proposal = _proposalService.Find(m => m.DocumentNumber == model.ProposalRefDocumentNumber);
                if (proposal != null)
                {
                    reportDataSource.ProposalName = proposal.Title;
                    reportDataSource.ProposalUnitCode = proposal.UnitCode;
                    reportDataSource.ProposalUnitName = proposal.UnitName;

                    DateTime startDate = (DateTime)proposal.StartDate;
                    reportDataSource.StartDate = startDate.ToString("dd/MM/yyyy");

                    DateTime endDate = (DateTime)proposal.EndDate;
                    reportDataSource.EndDate = endDate.ToString("dd/MM/yyyy");
                }

                reportDataSource.UnitCode = model.UnitCode;
                reportDataSource.UnitName = model.UnitName;
                reportDataSource.RequesterName = model.RequesterName;
                reportDataSource.Title = model.Title;
                reportDataSource.PrintDate = DateTime.Now.ToString("dd/MM/yyyy");

                var requesterPosition = _employeeTableService.FindByUsername(model.RequesterUserName);
                if (requesterPosition != null)
                {
                    reportDataSource.RequesterPosition = (!string.IsNullOrEmpty(requesterPosition.PositionName)) ? " - " + requesterPosition.PositionName : "";
                }

                if (model.CompletedDate != null)
                {
                    DateTime submitDate = (DateTime)model.CompletedDate;
                    reportDataSource.CompletedDate = submitDate.ToString("dd/MM/yyyy");
                }

                if (model.Category != null)
                {
                    DateTime submitDate = (DateTime)model.CreatedDate;
                    reportDataSource.CreatedDate = submitDate.ToString("dd/MM/yyyy");
                }

                if (model.Category == "otherincome")
                {
                    reportDataSource.Category = "Income Other Source";
                    var incomeOther = _proposalIncomeOtherService.Find(m => m.Id == model.OtherIncomeRefId);
                    if (incomeOther != null)
                    {
                        reportDataSource.ProposalRefName = incomeOther.SponcorName;
                        reportDataSource.ProposalRefDescription = incomeOther.Description;
                    }
                }
                else if (model.Category == "depositproposal")
                {
                    reportDataSource.Category = "Deposit Proposal";
                    var depostline = _proposalDepositLineService.Find(m => m.Id == model.DepositLineId);
                    if (depostline != null)
                    {
                        reportDataSource.ProposalRefName = depostline.PaymentNo;
                    }
                }
                if (model.Attachments != null)
                {
                    reportDataSource.AttachmentName = "";
                    foreach (var lineAttachment in model.Attachments)
                    {
                        if (reportDataSource.AttachmentName == "")
                        {
                            reportDataSource.AttachmentName = lineAttachment.FileName;
                        }
                        else
                        {
                            reportDataSource.AttachmentName = reportDataSource.AttachmentName + ", " + lineAttachment.FileName;
                        }
                    }
                }

                var memoincomeViewModel = _memoIncomeMapper.ToViewModel(model);

                if (memoincomeViewModel.CCEmail != null)
                {
                    reportDataSource.CC = "";
                    foreach (var name in memoincomeViewModel.CCEmail)
                    {
                        var emploloyee = _employeeTableService.FindByUsername(name.Name);
                        var fullName = (emploloyee != null) ? emploloyee.ThFullName : name.Name;
                        reportDataSource.CC = reportDataSource.CC == "" ? fullName : reportDataSource.CC + ", " + fullName;
                    }
                }

                if (memoincomeViewModel.InformEmail != null)
                {
                    reportDataSource.Inform = "";
                    foreach (var name in memoincomeViewModel.InformEmail)
                    {
                        var emploloyee = _employeeTableService.FindByUsername(name.Name);
                        var fullName = (emploloyee != null) ? emploloyee.ThFullName : name.Name;
                        reportDataSource.Inform = reportDataSource.Inform == "" ? fullName : reportDataSource.Inform + ", " + fullName;
                    }
                }

                if (model.Description != null)
                {
                    reportDataSource.Objective = model.Description.Replace("font-family", string.Empty);
                    var imgs = reportDataSource.Objective.Contains("img");
                    reportDataSource.ObjectiveImg = imgs ? "*ดูรูปเพิ่มเติมได้ในระบบ" : "";
                }

                list.Add(reportDataSource);
            }
            return list;
        }

        public List<MemoIncomeLinesDataSource> MemoIncomeLineToDataSources(MPS_MemoIncomeTable model)
        {
            var list = new List<MemoIncomeLinesDataSource>();
            if (model != null)
            {
                foreach (var line in model.MemoIncomeLine)
                {
                    var reportDataSource = new MemoIncomeLinesDataSource();
                    reportDataSource.Unit = line.Unit;
                    reportDataSource.Amount = line.Amount;
                    reportDataSource.Description = line.Description;
                    reportDataSource.NetAmount = line.NetAmount;
                    //reportDataSource.NetAmountNoVatTax = line.NetAmountNoVatTax;
                    reportDataSource.Remark = line.Remark;
                    reportDataSource.Tax = line.Tax;
                    reportDataSource.Vat = line.Vat;

                    list.Add(reportDataSource);
                }
            }
            return list;
        }

        public IEnumerable<ApproverDataSource> ApproversToDataSources(IEnumerable<MPS_MemoIncomeApproval> model)
        {
            if (model != null)
            {
                var list = new List<ApproverDataSource>();
                foreach (var approverData in model)
                {
                    var reportApproverDataSource = new ApproverDataSource() { };
                    reportApproverDataSource.ApproverId = approverData.Id;
                    reportApproverDataSource.ApproverSequence = approverData.ApproverSequence;
                    reportApproverDataSource.ApproverUsername = approverData.ApproverUserName ?? "";
                    reportApproverDataSource.ApproverFullName = approverData.ApproverFullName ?? "";
                    reportApproverDataSource.ApproverLevel = approverData.ApproverLevel ?? "";
                    reportApproverDataSource.ApproverPosition = approverData.Position ?? "";
                    var comment = approverData.MemoIncomeTable.Comments;
                    var approverCheck = approverData.ApproverLevel + " - Approval";
                    if (comment != null)
                    {
                        comment = comment.Where(m => m.Activity.ToLower() != "start").ToList();
                        var approve = comment.LastOrDefault(m => m.CreatedBy.Equals(approverData.ApproverUserName, StringComparison.InvariantCultureIgnoreCase) &&
                      m.Action.Equals(DocumentStatusFlag.Approve.ToString(), StringComparison.InvariantCultureIgnoreCase) &&
                      m.Activity.Equals(approverCheck, StringComparison.InvariantCultureIgnoreCase));
                        if (approve != null)
                        {
                            reportApproverDataSource.ApproveDateTime = approve.CreatedDate != null ? approve.CreatedDate.Value.ToString("dd/MM/yyyy") : "";
                        }
                        else
                        {
                            reportApproverDataSource.ApproveDateTime = "";
                        }
                    }
                    if (!string.IsNullOrEmpty(approverData.ApproverLevel))
                    {
                        if (approverData.ApproverLevel.Equals(Approval.ApproveLevelList.Propose.GetDescription()))
                            reportApproverDataSource.ApproverLevelKey = Approval.ApproveKey.Propose;
                        else if (approverData.ApproverLevel.Equals(Approval.ApproveLevelList.Accept.GetDescription()))
                            reportApproverDataSource.ApproverLevelKey = Approval.ApproveKey.Accept;
                        else if (approverData.ApproverLevel.Equals(Approval.ApproveLevelList.Approval.GetDescription()))
                            reportApproverDataSource.ApproverLevelKey = Approval.ApproveKey.Approval;
                        else if (approverData.ApproverLevel.Equals(Approval.ApproveLevelList.FinalApproval.GetDescription()))
                            reportApproverDataSource.ApproverLevelKey = Approval.ApproveKey.FinalApproval;
                        else
                        {
                            reportApproverDataSource.ApproverLevelKey = 0;
                        }
                    }
                    else
                    {
                        reportApproverDataSource.ApproverLevelKey = 0;
                    }

                    list.Add(reportApproverDataSource);
                }
                return list;
            }
            else
            {
                return null;
            }
        }
    }
}