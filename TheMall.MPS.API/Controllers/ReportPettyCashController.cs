﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using CrossingSoft.Framework.Infrastructure.Session;
using Microsoft.Reporting.WebForms;
using TheMall.MPS.API.Controllers.Abstracts;
using TheMall.MPS.API.Facades;
using TheMall.MPS.API.Services;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Reports.CashAdvance;
using TheMall.MPS.Reports.PettyCash;
using TheMall.MPS.Reports.Tests;

namespace TheMall.MPS.API.Controllers
{
    [RoutePrefix("api/reportpettycash")]
    public class ReportPettyCashController : BaseReportController
    {
        private readonly IReportService _reportService;
        private readonly IPettyCashService _pettyCashService;
        private readonly IPettyCashFacade _pettyCashFacade;
        private readonly IReportPettyCashFacade _reportPettyCashFacade;
        private readonly IMPSSession _session;
        public ReportPettyCashController(IReportService reportService, IReportService reportService1,
            IPettyCashFacade pettyCashFacade, IReportPettyCashFacade reportPettyCashFacade, IPettyCashService pettyCashService, IMPSSession session) 
            : base(reportService)
        {
            _reportService = reportService1;
            _pettyCashFacade = pettyCashFacade;
            _reportPettyCashFacade = reportPettyCashFacade;
            _pettyCashService = pettyCashService;
            _session = session;
        }

        [Route("pettycash")]
        public HttpResponseMessage Post(PettyCashFormCriteria criteria)
        {
            try{

                _reportService.ReportName = "PettyCashForm";
                _reportService.ReportPath = "PettyCash";
                var date = new DateTime();
                //var pettyCashModel = _pettyCashService.FirstOrDefault(m => m.DocumentNumber.Equals(criteria.DocumentNumber, StringComparison.InvariantCultureIgnoreCase));
                var pettyCashModel = _pettyCashService.FirstOrDefault(m => m.Id == criteria.Id);
                pettyCashModel.RePrintNo = (pettyCashModel.RePrintNo!=null)?pettyCashModel.RePrintNo+1:1;  

                var list = _reportPettyCashFacade.ReportData(pettyCashModel);
                var approvers = _reportPettyCashFacade.ApproversData(pettyCashModel.PettyCashApproval);

                var dataSource = new Microsoft.Reporting.WebForms.ReportDataSource("PettyCashDataSet", list);
                var approversDataSource = new Microsoft.Reporting.WebForms.ReportDataSource("ApproversDataSet", approvers);

                var reportParams = new List<ReportParameter>();
                reportParams.Add(new ReportParameter { Name = "RePrintNo", Values = { pettyCashModel.RePrintNo.ToString() }});
                _reportService.Parameters = reportParams;
                _reportService.DataSources = new[] { dataSource, approversDataSource };
                _reportService.FileName = " Petty Cash-No." + pettyCashModel.DocumentNumber + "- Date_" + DateTime.Now.ToString("dd_MM_yy");
                _pettyCashService.Update(pettyCashModel, pettyCashModel.Id);
                _session.SaveChanges();
                return GenerateReportStream(criteria);
                
             }
            catch (Exception ex)
            {
                throw new HttpException(ex.Message);
            }
        }
    }
}