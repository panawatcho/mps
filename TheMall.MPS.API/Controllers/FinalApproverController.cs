﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using TheMall.MPS.API.Controllers.Abstracts;
using TheMall.MPS.API.Facades;
using TheMall.MPS.API.Facades.Abstracts;
using TheMall.MPS.API.Mappers;
using TheMall.MPS.API.Services;
using TheMall.MPS.Infrastructure;
using TheMall.MPS.Models.Master;
using TheMall.MPS.ViewModels.Master;
using WebApi.OutputCache.V2;

namespace TheMall.MPS.API.Controllers
{
    [Attributes.AllowAuthenticated]
    [RoutePrefix("api/finalapprover")]
    public class FinalApproverController : BaseMasterController<MPS_M_FinalApprover, FinalApproverViewModel>
    {
        private readonly IFinalApproverFacade _facade;
        private readonly IFinalApproverService  _service;
        public FinalApproverController(IFinalApproverFacade facade, IFinalApproverService service)
            : base(facade)
        {
            _facade = facade;
            _service = service;
        }
       // [CacheOutput(ClientTimeSpan = Constants.CacheTime, ServerTimeSpan = Constants.ServerCacheTime)]
        public async Task<IHttpActionResult> Get()
        {
            return Ok((await _service.GetAllAsyns()).ToViewModels());
        }
      //  [CacheOutput(ClientTimeSpan = Constants.CacheTime, ServerTimeSpan = Constants.ServerCacheTime)]
        public async Task<IHttpActionResult> GetById(string id)
        {
            return Ok((await _service.GetAsync(id)).ToViewModel());
        }
        [HttpGet]
        [Route("search")]
        public async Task<IHttpActionResult> Search(string code = null, string name = null)
        {
            return Ok((_facade.Search(code, name)));
        }


        [HttpGet]
        [Route("searchByKeys/{processCode}/{unitCode}/{username}")]
        public async Task<IHttpActionResult> SearchByKeys(string processCode = null, string unitCode = null, string username = null)
        {
            return Ok((_facade.SearchByKeys(processCode, unitCode, username)));
        }
    }
}