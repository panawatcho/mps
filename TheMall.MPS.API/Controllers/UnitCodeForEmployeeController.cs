﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using TheMall.MPS.API.Controllers.Abstracts;
using TheMall.MPS.API.Facades;
using TheMall.MPS.API.Facades.Abstracts;
using TheMall.MPS.API.Mappers;
using TheMall.MPS.API.Services;
using TheMall.MPS.Infrastructure;
using TheMall.MPS.Models.Master;
using TheMall.MPS.ViewModels.Master;
using WebApi.OutputCache.V2;

namespace TheMall.MPS.API.Controllers
{
    [Attributes.AllowAuthenticated]
    [RoutePrefix("api/unitcodeforemployee")]
    public class UnitCodeForEmployeeController : BaseMasterController<MPS_M_UnitCodeForEmployee, UnitCodeForEmployeeViewModel>
    {
        private readonly IUnitCodeForEmployeeFacade _facade;
        private readonly IUnitCodeForEmployeeMasterService _service;
        private readonly IUnitCodeForEmployeeService _unitCodeForEmployeeService;
        public UnitCodeForEmployeeController(IUnitCodeForEmployeeFacade facade, IUnitCodeForEmployeeMasterService service, IUnitCodeForEmployeeService unitCodeForEmployeeService) : base(facade)
        {
            _facade = facade;
            _service = service;
            _unitCodeForEmployeeService = unitCodeForEmployeeService;
        }

      //  [CacheOutput(ClientTimeSpan = Constants.CacheTime, ServerTimeSpan = Constants.ServerCacheTime)]
        public async Task<IHttpActionResult> Get()
        {
            return Ok((await _service.GetAllAsyns()).ToViewModels());
        }
       // [CacheOutput(ClientTimeSpan = Constants.CacheTime, ServerTimeSpan = Constants.ServerCacheTime)]
        public async Task<IHttpActionResult> GetById(string id)
        {
            return Ok((await _service.GetAsync(id)).ToViewModel());
        }
        [HttpGet]
        [Route("search")]
        public async Task<IHttpActionResult> Search(string code = null, string name = null)
        {
            return Ok((_facade.Search(code, name)));
        }

        [HttpGet]
        [Route("GetInMaster/{empId}/{unitCode}")]
        public async Task<IHttpActionResult> GetInMaster(string empId = null, string unitCode = null)
        {
            return Ok((_facade.GetInMaster(empId, unitCode)));
        }


        [HttpGet]
        [Route("DeleteInMaster/{empId}/{unitCode}")]
        public async Task<IHttpActionResult> DeleteInMaster(string empId = null, string unitCode = null)
        {
            return Ok((_facade.DeleteInMaster(empId, unitCode)));
        }

        [Route("Save")]
        [HttpPost]
        public async Task<IHttpActionResult> Save(UnitCodeForEmployeeViewModel viewModel)
        {
            var model = await _facade.SaveAsync(viewModel);
            return Ok(model);
        }

        [HttpGet]
        [Route("GetByUserName")]
        public  IHttpActionResult GetByUserName(string username)
        {
            return Ok(_unitCodeForEmployeeService.GetUnitCodeForEmployees(username).ToViewModels());
        }
    }
}