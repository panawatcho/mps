﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http.Filters;

namespace TheMall.MPS.API.Attributes
{
    /// <summary>
    /// Converts <c>null</c> return values into an HTTP 404 return code.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public sealed class NullResponseIs404Attribute : ActionFilterAttribute
    {
        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            if ((actionExecutedContext.Response != null) && actionExecutedContext.Response.IsSuccessStatusCode
                && actionExecutedContext.Response.StatusCode != HttpStatusCode.NotModified)
            {
                object contentValue = null;
                actionExecutedContext.Response.TryGetContentValue(out contentValue);
                if (contentValue == null)
                {
                    var b = actionExecutedContext.Response.Content as System.Net.Http.ByteArrayContent;
                    if (b == null)
                    {
                        //actionExecutedContext.Response =
                        //    actionExecutedContext.Request.CreateErrorResponse(HttpStatusCode.NotFound,
                        //        Resources.Error.RequestedObjectNotFound);
                        actionExecutedContext.Response =
                          actionExecutedContext.Request.CreateErrorResponse(HttpStatusCode.NotFound,
                              "Requested object could not be found.");
                    }
                }
            }
        }

    }
}