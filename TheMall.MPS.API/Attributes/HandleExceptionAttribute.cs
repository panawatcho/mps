﻿using System;
using System.Data.SqlClient;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Web.Http.Filters;

namespace TheMall.MPS.API.Attributes
{
    public class HandleExceptionAttribute : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            //TODO: Add logging
            Exception exception = actionExecutedContext.Exception;
            //if (actionExecutedContext.Exception is AggregateException)
            //{
            //    var aggregateException = actionExecutedContext.Exception as AggregateException;
            //    if (aggregateException.InnerExceptions.Count == 1)
            //    {
            //        exception = GetInnermostException(aggregateException);
            //    }
            //    else if (aggregateException.InnerExceptions.Count > 1)
            //    {
            //        foreach (Exception e in aggregateException.InnerExceptions)
            //        {
            //            var e1 = GetInnermostException(e);
            //            if (e1 is BAPIExecutionException)
            //            {
            //                var e2 = e1 as BAPIExecutionException;
            //                if (e2.Level == "E")
            //                {
            //                    actionExecutedContext.ActionContext.ModelState.AddModelError("ValidationErrors",
            //                        string.Format("({0}) {1}",
            //                            e2.Level, e2.Message));
            //                }
            //                else if (e2.Level == "W")
            //                {
            //                    actionExecutedContext.ActionContext.ModelState.AddModelError("ValidationWarnings",
            //                        string.Format("({0}) {1}",
            //                            e2.Level, e2.Message));
            //                }
            //            }
            //            else
            //            {
            //                actionExecutedContext.ActionContext.ModelState.AddModelError("ValidationErrors", e.Message);
            //            }
            //        }
            //        actionExecutedContext.Response = actionExecutedContext.Request
            //             .CreateErrorResponse(HttpStatusCode.BadRequest, actionExecutedContext.ActionContext.ModelState);
            //        return;
            //    }
            //}

            if (exception is System.Data.Entity.Validation.DbEntityValidationException)
            {
                var dbEntityValidationException = exception as System.Data.Entity.Validation.DbEntityValidationException;
                foreach (var validationErrors in dbEntityValidationException.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        actionExecutedContext.ActionContext.ModelState.AddModelError("ValidationErrors", validationError.ErrorMessage);
                    }
                }
                actionExecutedContext.Response = actionExecutedContext.Request
                     .CreateErrorResponse(HttpStatusCode.BadRequest, actionExecutedContext.ActionContext.ModelState);

                return;
            }

            if (exception is System.Data.Entity.Infrastructure.DbUpdateException)
            {
                var dbUpdateException = exception as System.Data.Entity.Infrastructure.DbUpdateException;
                var sqlException = GetInnermostException(dbUpdateException) as SqlException;
                if (sqlException != null)
                {
                    if (sqlException.Number == 547)
                    {
                        Regex rt = new Regex("table \"([^\"]*)\"");
                        Match m = rt.Match(sqlException.Message);
                        string table = m.Groups[1].Value;

                        Regex rc = new Regex("column '([^']*)'");
                        m = rc.Match(sqlException.Message);
                        string column = m.Groups[1].Value;
//#if DEBUG
                        actionExecutedContext.Response = actionExecutedContext.Request
                                .CreateErrorResponse(HttpStatusCode.BadRequest, sqlException.Message);
//#else
//                        actionExecutedContext.Response = actionExecutedContext.Request
//                                .CreateErrorResponse(HttpStatusCode.BadRequest, string.Format(Resources.Error.ForeignKeyViolatedOnColumn, column));
//#endif

                        return;
                    }
                    else if (sqlException.Number == 2601)
                    {
                        var s = sqlException.Message.IndexOf("The duplicate key value is ", StringComparison.Ordinal);
                        if (s >= 0)
                        {
                            var o = sqlException.Message.IndexOf("(", s, System.StringComparison.Ordinal);
                            var c = sqlException.Message.IndexOf(")", o + 1, System.StringComparison.Ordinal);
                            if (o >= 0 && c > o)
                            {
                                var msg = "Cannot create duplicate value " + sqlException.Message.Substring(o, (c - o) + 1);
                                var u = sqlException.Message.IndexOf("with unique index ", StringComparison.Ordinal);
                                if (u >= 0)
                                {
                                    var o1 = sqlException.Message.IndexOf("'", u, System.StringComparison.Ordinal);
                                    var c1 = sqlException.Message.IndexOf("'", o1 + 1, System.StringComparison.Ordinal);
                                    if (o1 >= 0 && c1 > o1)
                                    {
                                        msg += ", conflicting with unique index " +
                                               sqlException.Message.Substring(o1, (c1 - o1) + 1)
                                               + ".";
                                    }
                                }
                                actionExecutedContext.Response = actionExecutedContext.Request
                                    .CreateErrorResponse(HttpStatusCode.BadRequest, msg);
                                return;
                            }
                        }
                    }
                }
            }

            if (exception is System.ArgumentNullException)
            {
                var argumentNullException = exception as ArgumentNullException;
                if (!string.IsNullOrEmpty(exception.Message))
                {
                    actionExecutedContext.Response = actionExecutedContext.Request
                        .CreateErrorResponse(HttpStatusCode.BadRequest, exception.Message);
                    return;
                }
                actionExecutedContext.Response = actionExecutedContext.Request
                               .CreateErrorResponse(HttpStatusCode.BadRequest, string.Format("Argument '{0}' cannot be blank.", argumentNullException.ParamName));
                return;
            }

            if (exception is System.IO.IOException)
            {
                //var ioException = exception as System.IO.IOException;
                var e = GetInnermostException(exception);
                actionExecutedContext.Response = actionExecutedContext.Request
                               .CreateErrorResponse(HttpStatusCode.BadRequest, e.Message);
                return;
            }

            if (exception is System.Web.HttpException)
            {
                var httpException = exception as System.Web.HttpException;
                actionExecutedContext.Response = actionExecutedContext.Request
                               .CreateErrorResponse((HttpStatusCode)httpException.GetHttpCode(), exception.Message);
                return;
            }

            //if (exception is WorklistItemNotFoundException)
            //{
            //    var worklistItemNotFound = exception as WorklistItemNotFoundException;
            //    var myError = new
            //    {
            //        Message = worklistItemNotFound.Message,
            //        SN = worklistItemNotFound.SN,
            //        WorkflowTableId = worklistItemNotFound.WorkflowTableId
            //    };
            //    actionExecutedContext.Response = actionExecutedContext.Request.CreateResponse(
            //        HttpStatusCode.BadRequest, myError,
            //        new System.Net.Http.Headers.MediaTypeHeaderValue("application/json"));
            //    return;
            //}

            // All other exception
//#if DEBUG
            var innermostException = GetInnermostException(exception);
            if (!string.IsNullOrEmpty(innermostException.Message))
            {
                actionExecutedContext.Response = actionExecutedContext.Request
                         .CreateErrorResponse(HttpStatusCode.InternalServerError, innermostException.Message);
            }
            else
            {
                actionExecutedContext.Response = actionExecutedContext.Request
                         .CreateErrorResponse(HttpStatusCode.InternalServerError, exception.Message);
            }
//#else
//            actionExecutedContext.Response = actionExecutedContext.Request
//                     .CreateErrorResponse(HttpStatusCode.InternalServerError, exception.Message);
//#endif
        }

        private Exception GetInnermostException(Exception exception, bool onlyWithMsg = false)
        {
            Exception innerException = exception;
            while (innerException.InnerException != null)
            {
                innerException = innerException.InnerException;
            }
            return innerException;
        }
    }
}