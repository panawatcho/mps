﻿using System;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace TheMall.MPS.API.Attributes
{
    public class RefererAuthorize : AuthorizeAttribute
    {
        protected override bool IsAuthorized(HttpActionContext actionContext)
        {    
           #if DEBUG
            return true;
#endif

            if (actionContext.ControllerContext.ControllerDescriptor.GetCustomAttributes<AllowAuthenticated>().Any()
                || actionContext.ActionDescriptor.GetCustomAttributes<AllowAuthenticated>().Any())
            {
                //return base.IsAuthorized(actionContext);
                return true;
            }
            return base.IsAuthorized(actionContext) && IsAuthorize(actionContext);
        }

        public bool IsAuthorize(HttpActionContext actionContext)
        {
            if (actionContext.Request.Headers.Contains("X-Referer"))
            {
                var referers = actionContext.Request.Headers.GetValues("X-Referer");
                referers = referers.Select(m => m.TrimStart('/')).Select(m => m.EndsWith("/") ? m : m + "/");
                var claims =
                    Infrastructure.UserHelper.GetReferer(actionContext.RequestContext.Principal.Identity)
                        .Select(m => m.Value).ToListSafe();
                //if (!claims.Contains("*"))
                //{
                //    claims.Add("*");
                //}
                var ret = claims.Any(claim => !string.IsNullOrEmpty(claim) && referers.Any(referer => referer.StartsWith(claim, StringComparison.InvariantCultureIgnoreCase)));
                return ret;
            }
            return false;
            
        }
    }
}