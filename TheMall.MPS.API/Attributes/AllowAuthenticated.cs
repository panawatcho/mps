﻿using System;
using System.Web.Http.Filters;

namespace TheMall.MPS.API.Attributes
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class AllowAuthenticated : Attribute, IFilter
    {
        public bool AllowMultiple { get; private set; }
    }
}