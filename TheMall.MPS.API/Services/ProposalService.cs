﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using CrossingSoft.Framework.Infrastructure.OData.Repositories;
using CrossingSoft.Framework.Models.Enums;
using CrossingSoft.Framework.Services.OData;
using LinqKit;
using SourceCode.Workflow.Client;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Infrastructure.EntityFramework;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Models.Enums;
using TheMall.MPS.Models.Proposal;

namespace TheMall.MPS.API.Services
{
    public interface IProposalService : IMpsWorkflowTableService<MPS_ProposalTable>
    {
       IEnumerable<MPS_ProposalTable> SearchDocument(string code = null, string name = null);
       IEnumerable<MPS_ProposalTable> ProposalNumber();
       IEnumerable<MPS_ProposalTable> GetOpenProposal();
       IEnumerable<MPS_ProposalTable> SearchDoc(DateTime? createDate,
            bool checkStartDate,
            string process = null, string title = null, string documentNo = null,
            string username = null, string status = null);
        
    }

    public class ProposalService : MpsWorkflowTableService<MPS_ProposalTable>, IProposalService
    {
        public ProposalService(IODataRepositoryAsync<IMPSDbContext, IMPSSession, MPS_ProposalTable> repository) 
            : base(repository)
        {
        }

        //
        public override MPS_ProposalTable GetWorkflowTable(int id)
        {
            return Get(id);
        }

        //
        public override Task<MPS_ProposalTable> GetWorkflowTableAsync(int id)
        {
            return GetAsync(id);
        }

        //
        public  IEnumerable<MPS_ProposalTable> SearchDocument(string code = null, string name = null)
        {
            var query = LinqKit.PredicateBuilder.True<MPS_ProposalTable>();
           query = query.And(
                m => m.Status == DocumentStatus.Completed.ToString() && !m.Deleted);

            if (!string.IsNullOrEmpty(code) && !string.IsNullOrEmpty(name))
            {
                return
                    GetWhere(query.And(m =>
                        !string.IsNullOrEmpty(m.DocumentNumber) && m.DocumentNumber.Contains(code) &&
                        !string.IsNullOrEmpty(m.Title) && m.Title.Contains(name)));
            }
            else if (!string.IsNullOrEmpty(code))
            {
                return GetWhere(query.And(m => !string.IsNullOrEmpty(m.DocumentNumber) && m.DocumentNumber.Contains(code)));
            }
            else if (!string.IsNullOrEmpty(name))
            {
                return
                    GetWhere(query.And(m => !string.IsNullOrEmpty(m.Title) && m.Title.Contains(name)));
            }
            else
            {
                return GetWhere(query);
            }
        }

        //
         public  IEnumerable<MPS_ProposalTable> ProposalNumber()
        {
            var query = LinqKit.PredicateBuilder.True<MPS_ProposalTable>();
            query = query.And(
                 m => m.Status == DocumentStatus.Completed.ToString() && !m.Deleted );
             query = query.And(m => !m.ProposalTypeCode.Contains("DEPOSIT"));
            
            return GetWhere(query);
        }


        //
         public IEnumerable<MPS_ProposalTable> GetOpenProposal()
         {
             return GetWhere(m => m.StatusFlag == DocumentStatusFlagId.Completed
                                  && m.CloseFlag == false
                                  && m.ProposalTypeCode != "DEPOSIT"
                                  && m.ProposalTypeCode != "DEPOSITIN");
         }


        //
         public IEnumerable<MPS_ProposalTable> SearchDoc(DateTime? createDate,
             bool checkStartDate,
             string process = null, 
             string title = null, 
             string documentNo = null,
             string username = null, 
             string status = null)
         {
             var query = LinqKit.PredicateBuilder.True<MPS_ProposalTable>();
             if (!string.IsNullOrEmpty(status))
             {
                 query = query.And(m => m.DocumentStatus.Equals(status));
             }

             if (!string.IsNullOrEmpty(title))
             {
                 query = query.And(m => !string.IsNullOrEmpty(m.Title) && m.Title.Contains(title));
             }
             if (!string.IsNullOrEmpty(documentNo))
             {
                 query = query.And(m => !string.IsNullOrEmpty(m.DocumentNumber) && m.DocumentNumber.Contains(documentNo));
             }
             if (!string.IsNullOrEmpty(username))
             {
                 query = query.And(m => !string.IsNullOrEmpty(m.CreatedBy) && m.CreatedBy.Contains(username));
             }
             var result = GetWhere(query);
             if (createDate != null)
             {
                 DateTime dateTime = (DateTime) createDate;
                 result = result.Where(m => m.CreatedDate != null && m.CreatedDate.Value.Date == dateTime.Date);
                 //query = query.And(m => m.CreatedDate.Equals(startDate));
             }
             return result; //GetWhere(query);
             //if (!string.IsNullOrEmpty(code) && !string.IsNullOrEmpty(name))
             //{
             //    return
             //        GetWhere(query.And(m =>
             //            !string.IsNullOrEmpty(m.DocumentNumber) && m.DocumentNumber.Contains(code) &&
             //            !string.IsNullOrEmpty(m.Title) && m.Title.Contains(name)));
             //}
             //else if (!string.IsNullOrEmpty(code))
             //{
             //    return GetWhere(query.And(m => !string.IsNullOrEmpty(m.DocumentNumber) && m.DocumentNumber.Contains(code)));
             //}
             //else if (!string.IsNullOrEmpty(name))
             //{
             //    return
             //        GetWhere(query.And(m => !string.IsNullOrEmpty(m.Title) && m.Title.Contains(name)));
             //}
             //else
             //{
             //    return GetWhere(query);
             //}
         }
    }
}