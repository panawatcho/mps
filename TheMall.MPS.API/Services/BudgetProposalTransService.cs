﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using CrossingSoft.Framework.Infrastructure.OData.Repositories;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.Infrastructure.Repositories;
using TheMall.MPS.Models.Budget;
using TheMall.MPS.Models.Enums;

namespace TheMall.MPS.API.Services
{
    public interface IBudgetProposalTransService : Interfaces.IODataService<MPS_BudgetProposalTrans>
    {
        decimal GetBudgetTransTotal(int lineId);
        decimal GetBudgetTransTotal(int proposalId, string apCode, string unitCode);
        decimal GetBudgetDetailAp(int lineId, int documentId);
        decimal GetBalance(int proposalId);
        decimal GetBudgetReserve(int proposalId,int lineId, string apCode, string unitCode ,string documentType);
    }
    public class BudgetProposalTransService : BaseODataService<MPS_BudgetProposalTrans>, IBudgetProposalTransService
    {
        private readonly IMpsODataRepositoryAsync<MPS_BudgetProposalTrans> _repository;

        public BudgetProposalTransService(IMpsODataRepositoryAsync<MPS_BudgetProposalTrans> repository)
            : base(repository)
        {
            _repository = repository;
        }

        public decimal GetBudgetTransTotal(int lineId)
        {
            decimal balance = 0;
            if (lineId == 0) return balance;
            var proplineModel = GetWhere(m => !m.InActive && m.ProposalLineId == lineId);
            if (proplineModel != null)
            {
                balance = proplineModel.Sum(m => m.Amount);
            }

            return balance;
        }
        public decimal GetBudgetTransTotal(int proposalId, string apCode, string unitCode)
        {
            decimal balance = 0;
            if (proposalId == 0 || string.IsNullOrEmpty(apCode) || string.IsNullOrEmpty(unitCode)) return balance;
            var proplineModel = GetWhere(m => !m.InActive && m.ProposalId == proposalId
                && m.APCode.Equals(apCode, StringComparison.InvariantCultureIgnoreCase)
                && m.UnitCode.Equals(unitCode, StringComparison.InvariantCultureIgnoreCase));
            if (proplineModel != null)
            {
                balance = proplineModel.Sum(m => m.Amount);
            }

            return balance;
        }

        public decimal GetBudgetDetailAp(int lineId, int documentId)
        {
            decimal balance = 0;
            if (lineId == 0) return balance;
            var proplineModel = GetWhere(m => !m.InActive && m.ProposalLineId == lineId && m.DocumentId != documentId);
            if (proplineModel != null)
            {
                balance = proplineModel.Sum(m => m.Amount);
            }

            return balance;
        }

        public decimal GetBalance(int proposalId)
        {
            decimal balance = 0;
            if (proposalId == 0) return balance;
            var proposal = GetWhere(m => !m.InActive && m.ProposalId == proposalId);
            if (proposal != null)
            {
                balance = proposal.Sum(m => m.Amount);
            }

            return balance;
        }

        public decimal GetBudgetReserve(int proposalId, int lineId, string apCode, string unitCode, string documentType)
        {
            decimal balance = 0;
            var budgetTrans = LastOrDefault(m => m.ProposalLineId == lineId
                                               && m.ProposalId == proposalId
                                               && m.DocumentType == documentType
                                               && m.APCode == apCode
                                               && m.UnitCode == unitCode
                                               && m.Status == Budget.BudgetStatus.Reserve
                                               && !m.InActive);
            if (budgetTrans != null)
            {
                balance = budgetTrans.Amount;
            }
            return balance;
        }

    }
}