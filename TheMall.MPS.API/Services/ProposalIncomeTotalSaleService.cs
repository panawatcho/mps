﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using CrossingSoft.Framework.Infrastructure.OData.Repositories;
using CrossingSoft.Framework.Services.OData;
using SourceCode.Workflow.Client;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Infrastructure.EntityFramework;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Models.Proposal;

namespace TheMall.MPS.API.Services
{
    public interface IProposalIncomeTotalSaleService : Interfaces.IODataService<MPS_IncomeTotalSale>
    {
    }
    public class ProposalIncomeTotalSaleService : BaseODataService<MPS_IncomeTotalSale>, IProposalIncomeTotalSaleService
    {
        public ProposalIncomeTotalSaleService(IODataRepositoryAsync<MPS_IncomeTotalSale> repository)
            : base(repository)
        {
        }
    }
}