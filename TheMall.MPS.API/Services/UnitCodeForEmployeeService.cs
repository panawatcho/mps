﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using CrossingSoft.Framework.Infrastructure.OData.Repositories;
using CrossingSoft.Framework.Services.OData;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.Models.Master;

namespace TheMall.MPS.API.Services
{
    public interface IUnitCodeForEmployeeService : IODataServiceAsync<MPS_M_UnitCodeForEmployee>
    {
        IEnumerable<MPS_M_UnitCodeForEmployee> GetUnitCodeCurrentUser();
        IEnumerable<MPS_M_UnitCodeForEmployee> GetUnitCodeForEmployees(string username);
    }

    public class UnitCodeForEmployeeService : BaseODataService<MPS_M_UnitCodeForEmployee>, IUnitCodeForEmployeeService
    {
        private readonly IODataRepositoryAsync<MPS_M_UnitCodeForEmployee> _repository;

        public UnitCodeForEmployeeService(IODataRepositoryAsync<MPS_M_UnitCodeForEmployee> repository) : base(repository)
        {
            _repository = repository;
        }
        public IEnumerable<MPS_M_UnitCodeForEmployee> GetUnitCodeCurrentUser()
        {
            var currentUsername = ((ClaimsPrincipal)System.Threading.Thread.CurrentPrincipal).Identity.Name;
            return GetUnitCodeForEmployees(currentUsername);
        }
        public IEnumerable<MPS_M_UnitCodeForEmployee> GetUnitCodeForEmployees(string username)
        {
            return
                GetWhere(
                    m => m.Username != null
                        && m.Username.Equals(username, StringComparison.InvariantCultureIgnoreCase)
                        && m.InActive == false);
        }


    }
}