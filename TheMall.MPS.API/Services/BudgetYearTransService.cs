﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using CrossingSoft.Framework.Infrastructure.OData.Repositories;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.Infrastructure.Repositories;
using TheMall.MPS.Models.Budget;

namespace TheMall.MPS.API.Services
{
    public interface IBudgetYearTransService : Interfaces.IODataService<MPS_BudgetYearTrans>
    {
        decimal GetBudgetTransTotal(string year, string unitCode,string typeYearPlan);
        MPS_BudgetYearTrans LastBudgetTrans(string documentType, int documentId);
        MPS_BudgetYearTrans LastReseveBudgetTrans(string documentType, int documentId);
    }
    public class BudgetYearTransService : BaseODataService<MPS_BudgetYearTrans>, IBudgetYearTransService
    {
        public BudgetYearTransService(IMpsODataRepositoryAsync<MPS_BudgetYearTrans> repository)
            : base(repository)
        {
        }

        public decimal GetBudgetTransTotal(string year, string unitCode, string typeYearPlan)
        {
            var budgetTrans = GetWhere(m => m.Year.Equals(year,StringComparison.InvariantCultureIgnoreCase)
                && m.UnitCode.Equals(unitCode, StringComparison.InvariantCultureIgnoreCase)
                && m.TypeYearPlan.Equals(typeYearPlan, StringComparison.InvariantCultureIgnoreCase) 
                && !m.InActive);
            if (budgetTrans == null)
                return 0;
            return budgetTrans.Sum(m => m.Amount);
        }

        public MPS_BudgetYearTrans LastBudgetTrans(string documentType, int documentId)
        {
            return LastOrDefault(m => m.DocumentType.Equals(documentType, StringComparison.InvariantCultureIgnoreCase)
                && m.DocumentId == documentId
                && !m.InActive);
        }
        public MPS_BudgetYearTrans LastReseveBudgetTrans(string documentType, int documentId)
        {
            return LastOrDefault(m => m.DocumentType.Equals(documentType, StringComparison.InvariantCultureIgnoreCase)
                && m.DocumentId == documentId
                && !m.InActive && m.Status == "Reserve");
        }
    }
}