﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CrossingSoft.Framework.Infrastructure.OData.Repositories;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Infrastructure.EntityFramework;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Models.Interfaces;
using TheMall.MPS.Models.Proposal;


namespace TheMall.MPS.API.Services
{
    public interface ICommentService<TCommentTable> : IODataService<TCommentTable>
        where TCommentTable : ICommentTable, new()
    {
        Task<IEnumerable<TCommentTable>> GetCommentsAsync(int parentId);
        IEnumerable<TCommentTable> GetComments(int parentId);
        //Task<IEnumerable<TCommentTable>> GetGOACommentsAsync(int parentId);
        TCommentTable CreateCommentStart(IMpsWorkflowTable workflowTable, int procInstId);
        TCommentTable CreateCommentSubmit(int parentId, int? procInstId, string processName, string activity, string sn, string action,
            string comment);
        int? GetParentIdFromSerialNumber(string sn);
        Task<TCommentTable> GetLastCommentsAsync(int parentId);
        
    }

    public class CommentService<TCommentTable> : BaseODataService<TCommentTable>, ICommentService<TCommentTable>
        where TCommentTable : class, ICommentTable, new()
    {
        private readonly IODataRepositoryAsync<IMPSDbContext, IMPSSession, TCommentTable> _repository;

        public CommentService(IODataRepositoryAsync<IMPSDbContext, IMPSSession, TCommentTable> repository)
            : base(repository)
        {
            _repository = repository;
        }

        public bool Exists(int id)
        {
            return this.Query(e => e.Id == id).Select().Any();
        }

        public async Task<IEnumerable<TCommentTable>> GetCommentsAsync(int parentId)
        {
            return (await GetWhereAsync(m => m.ParentId == parentId)).ToListSafe();
        }
        public IEnumerable<TCommentTable> GetComments(int parentId)
        {
            return GetWhere(m => m.ParentId == parentId).ToListSafe();
        }
        //public async Task<IEnumerable<TCommentTable>> GetGOACommentsAsync(int parentId)
        //{
        //    return (await GetWhereAsync(m => m.ParentId == parentId && m.ProcessName == "GOA")).ToListSafe();
        //}

        public TCommentTable CreateCommentStart(IMpsWorkflowTable workflowTable, int procInstId)
        {
            var model = new TCommentTable();
            model.ParentId = workflowTable.Id;
            model.Action = "Start";
            model.Activity = "Start";
            model.ProcInstId = procInstId;
            model.CreatedBy = workflowTable.CreatedBy;
            return this.Insert(model);
        }

        //
        public TCommentTable CreateCommentSubmit(int parentId, int? procInstId, string processName, string activity, string sn, string action, string comment)
        {
            var model = new TCommentTable();
            model.ParentId = parentId;
            model.Action = action;
            model.ProcessName = processName;
            model.Activity = activity;
            model.ProcInstId = procInstId;
            model.SerialNumber = sn;
            model.Comment = comment;
            return this.Insert(model);
        }

        public int? GetParentIdFromSerialNumber(string sn)
        {
            var p = Query(m => m.SerialNumber == sn).Select(m => m.ParentId).FirstOrDefault();
            if (p != 0)
            {
                return p;
            }
            var procInstId = sn.GetProcInstIdFromSn();
            p = Query(m => m.ProcInstId == procInstId).Select(m => m.ParentId).LastOrDefault();
            if (p != 0)
            {
                return p;
            }
            return null;
        }

        public async Task<TCommentTable> GetLastCommentsAsync(int parentId)
        {
            var last =
                (await
                    _repository.Query(m => m.ParentId == parentId)
                        .OrderBy(n => n.OrderByDescending(o => o.CreatedDate))
                        .SelectAsync()).FirstOrDefault();
            return last;
        }
    }
}