﻿using CrossingSoft.Framework.Infrastructure.OData.Repositories;
using TheMall.MPS.Models.Example;

namespace TheMall.MPS.API.Services
{
    public interface IExampleService : CrossingSoft.Framework.Services.OData.IODataServiceAsync<ExampleTable>
    {

    }
    public class ExampleService : CrossingSoft.Framework.Services.OData.ODataServiceAsync<ExampleTable>, IExampleService
    {
        public ExampleService(IODataRepositoryAsync<ExampleTable> repository)
            : base(repository)
        {
        }
    }
}