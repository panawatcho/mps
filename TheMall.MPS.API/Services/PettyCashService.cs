﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using CrossingSoft.Framework.Infrastructure.OData.Repositories;
using CrossingSoft.Framework.Models.Enums;
using CrossingSoft.Framework.Services.OData;
using LinqKit;
using SourceCode.Workflow.Client;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Infrastructure.EntityFramework;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Models.Enums;
using TheMall.MPS.Models.PettyCash;

namespace TheMall.MPS.API.Services
{
    public interface IPettyCashService : IMpsWorkflowTableService<MPS_PettyCashTable>
    {
        Task<IEnumerable<MPS_PettyCashTable>> SearchDocument(string code = null, string name = null);
        bool CheckPettyCashSuccess(int parentId);

        IEnumerable<MPS_PettyCashTable> SearchDoc(DateTime? createDate,
       bool checkStartDate,
       string process = null, string title = null, string documentNo = null,
       string username = null, string status = null);
    }
    public class PettyCashService : MpsWorkflowTableService<MPS_PettyCashTable>, IPettyCashService
    {
        public PettyCashService(IODataRepositoryAsync<IMPSDbContext, IMPSSession, MPS_PettyCashTable> repository) 
            : base(repository)
        {
        }

        public override MPS_PettyCashTable GetWorkflowTable(int id)
        {
            return Get(id);
        }

        public override Task<MPS_PettyCashTable> GetWorkflowTableAsync(int id)
        {
            return GetAsync(id);
        }
        public async Task<IEnumerable<MPS_PettyCashTable>> SearchDocument(string code = null, string name = null)
        {
            var query = LinqKit.PredicateBuilder.True<MPS_PettyCashTable>();

            if (!string.IsNullOrEmpty(code) && !string.IsNullOrEmpty(name))
            {
                return
                    GetWhere(query.And(m =>
                        !string.IsNullOrEmpty(m.DocumentNumber) && m.DocumentNumber.Contains(code) &&
                        !string.IsNullOrEmpty(m.Title) && m.Title.Contains(name)));
            }
            else if (!string.IsNullOrEmpty(code))
            {
                return GetWhere(query.And(m => !string.IsNullOrEmpty(m.DocumentNumber) && m.DocumentNumber.Contains(code)));
            }
            else if (!string.IsNullOrEmpty(name))
            {
                return
                    GetWhere(query.And(m => !string.IsNullOrEmpty(m.Title) && m.Title.Contains(name)));
            }
            else
            {
                return GetWhere(query);
            }
        }
        public bool CheckPettyCashSuccess(int parentId)
        {
            var model = GetWhere(m => m.ProposalRefID == parentId && !m.Deleted && m.StatusFlag != 5 && m.StatusFlag != 0).ToListSafe();
            if (model == null || model.Count == 0)
                return true;
            if (model.Count > 0)
            {
                 var countComplete = model.Where(m => m.Status == DocumentStatus.Completed.ToString()).ToListSafe();
                if (model.Count == countComplete.Count())
                
                {
                    return true;
                }
          
            }
             return false;
        }

        public IEnumerable<MPS_PettyCashTable> SearchDoc(
            DateTime? createDate,
         bool checkStartDate,
         string process = null,
         string title = null,
         string documentNo = null,
         string username = null,
         string status = null)
        {
            var query = LinqKit.PredicateBuilder.True<MPS_PettyCashTable>();
            if (!string.IsNullOrEmpty(status))
            {
                query = query.And(m => m.DocumentStatus.Equals(status));
            }

            if (!string.IsNullOrEmpty(title))
            {
                query = query.And(m => !string.IsNullOrEmpty(m.Title) && m.Title.Contains(title));
            }
            if (!string.IsNullOrEmpty(documentNo))
            {
                query = query.And(m => !string.IsNullOrEmpty(m.DocumentNumber) && m.DocumentNumber.Contains(documentNo));
            }
            if (!string.IsNullOrEmpty(username))
            {
                query = query.And(m => !string.IsNullOrEmpty(m.CreatedBy) && m.CreatedBy.Contains(username));
            }
            //if (checkStartDate)
            //{
            //    query = query.And(m => m.CreatedDate.Equals(startDate));
            //}
            var result = GetWhere(query);
            if (createDate != null)
            {
                DateTime dateTime = (DateTime) createDate;
                result = result.Where(m => m.CreatedDate != null && m.CreatedDate.Value.Date == dateTime.Date);
                //query = query.And(m => m.CreatedDate.Equals(startDate));
            }
            return result;
            //return GetWhere(query);
        }
    }
}