﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using CrossingSoft.Framework.Infrastructure.OData.Repositories;
using LinqKit;
using TheMall.MPS.API.Services;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Infrastructure.EntityFramework;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Models.Receipt;

namespace TheMall.MPS.API.Services
{
    public interface IReceiptService : IBaseMasterService<MPS_ReceiptTable>
    {

    }
    public class ReceiptService : BaseMasterService<MPS_ReceiptTable>, IReceiptService
    {
        public ReceiptService(IODataRepositoryAsync<IMPSDbContext, 
            IMPSSession, 
            MPS_ReceiptTable> repository)
            : base(repository)
        {

        }

        public override MPS_ReceiptTable DoGet(string code)
        {
            return Get(code);
        }

        public override Task<MPS_ReceiptTable> DoGetAsync(string code)
        {
            return GetAsync(code);
        }
    }
}