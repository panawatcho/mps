﻿using CrossingSoft.Framework.Infrastructure;
using CrossingSoft.Framework.Infrastructure.OData.Repositories;
using CrossingSoft.Framework.Services.OData;
using TheMall.MPS.API.Services.Interfaces;

namespace TheMall.MPS.API.Services
{
    public class BasicService<TEntity> : ODataServiceAsync<TEntity>, IService<TEntity> where TEntity : class, IObjectState
    {
        public BasicService(IODataRepositoryAsync<TEntity> repository)
            : base(repository)
        {
        }
    }
}
