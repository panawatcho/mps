﻿using System.Collections.Generic;
using CrossingSoft.Framework.Infrastructure.OData.Repositories;
using CrossingSoft.Framework.Models.Interfaces;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.Infrastructure.Repositories;
using TheMall.MPS.Models;

namespace TheMall.MPS.API.Services
{
    public interface IApplicationSettingService
    {
        IReadOnlyList<IApplicationSetting> Initialize(string environment);
        IApplicationSetting GetApplicationSetting(string key, string environment);
    }

    public class ApplicationSettingService : BaseService<ApplicationSetting>, IApplicationSettingService
    {
        private readonly IODataRepositoryAsync<ApplicationSetting> _repository;

        public ApplicationSettingService(IODataRepositoryAsync<ApplicationSetting> repository)
            : base(repository)
        {
            _repository = repository;
        }

        public IReadOnlyList<IApplicationSetting> Initialize(string environment)
        {
            //var environment = env ?? AppSettings.CurrentEnvironment;

            return GetWhere(m => m.Environment == environment
                                    && m.Preload == true).ToListSafe();
        }

        public IApplicationSetting GetApplicationSetting(string key, string environment)
        {
            return FirstOrDefault(m => m.Key == key && m.Environment == environment);
        }
    }
}