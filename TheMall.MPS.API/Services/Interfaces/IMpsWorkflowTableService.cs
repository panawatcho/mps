﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using CrossingSoft.Framework.Models.Interfaces;
using CrossingSoft.Framework.Services;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Models.Interfaces;


namespace TheMall.MPS.API.Services.Interfaces
{
    public interface IMpsWorkflowTableService
    {
        IMpsWorkflowTable GetInterfaceWorkflowTable(int id);
    }

    public interface IMpsWorkflowTableService<TWorkflowTable> : IODataService<TWorkflowTable>, IMpsWorkflowTableService
        where TWorkflowTable : IMpsWorkflowTable
    {
        TWorkflowTable GetWorkflowTable(int id);
        Task<TWorkflowTable> GetWorkflowTableAsync(int id);
        bool Exists(int id);
        TWorkflowTable Get(int id);
        Task<TWorkflowTable> GetAsync(int id);
        TWorkflowTable GetWorkflowTableFromProcInstId(int procInstid);
        int? GetWorkflowTableIdFromSN(string sn);
    }
    
}