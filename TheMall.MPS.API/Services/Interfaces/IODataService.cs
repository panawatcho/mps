﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Web;
using CrossingSoft.Framework.Infrastructure;
using CrossingSoft.Framework.Services.OData;

namespace TheMall.MPS.API.Services.Interfaces
{
    public interface IODataService<TEntity> : IService<TEntity>, IODataServiceAsync<TEntity> where TEntity : IObjectState
    {
        TEntity LastOrDefault(Expression<Func<TEntity, bool>> predicate);
        Task<TEntity> LastOrDefaultAsync(Expression<Func<TEntity, bool>> predicate);
    }

}