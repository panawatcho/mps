﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrossingSoft.Framework.Infrastructure;

namespace TheMall.MPS.API.Services.Interfaces
{
    public interface IBaseMasterService<TEntity> : CrossingSoft.Framework.Services.OData.IODataService<TEntity>
        where TEntity : IObjectState
    {
        TEntity Get(string code);
        TEntity DoGet(string code);
        Task<TEntity> GetAsync(string code);
        Task<TEntity> DoGetAsync(string code);
        //Task<IEnumerable<TEntity>> SearchAsync(string code = "", string name = "");
        Task<IEnumerable<TEntity>> GetAllAsyns();
    }

}
