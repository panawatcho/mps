﻿using CrossingSoft.Framework.Infrastructure;

namespace TheMall.MPS.API.Services.Interfaces
{
    public interface IService<TEntity> : CrossingSoft.Framework.Services.IServiceAsync<TEntity> where TEntity : IObjectState
    {
    }
    public interface IService
    {
    }
}
