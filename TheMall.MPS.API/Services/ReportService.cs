﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web.Hosting;
using TheMall.MPS.Reports.Interfaces;
using Ionic.Zip;
using Microsoft.Reporting.WebForms;

namespace TheMall.MPS.API.Services
{
    public enum ReportSource
    {
        File = 0,
        Stream = 1
    }
    public interface IReportService
    {
        // Output
        string FileExtension { get; }
        bool Zipping { get; }
        string ContentType { get; }

        // Input
        string FileName { get; set; }
        string ReportName { get; set; }
        string ReportPath { get; set; }
        ReportDataSource[] DataSources { get; set; }

        // Two ways for RDLC source
        ReportSource Source { get; set; }

        // The Embbedded Resource path
        Stream GetEmbeddedResourceStream();

        // The RDLC File path
        string GetReportResource();

        MemoryStream GetReportStream(IReportCriteria criteria);
        MemoryStream GetReportStreamAndSave(IReportCriteria criteria);
        List<ReportParameter> Parameters { get; set; }

        bool EnableExternalImages { get; set; }
    }
    public class ReportService : IReportService
    {
        public string FileName { get; set; }
        public string ReportName { get; set; }
        public string ReportPath { get; set; }
        public string FileExtension { get; private set; }
        public bool Zipping { get; private set; }
        public string ContentType { get; private set; }
        public ReportDataSource[] DataSources { get; set; }
        public ReportSource Source { get; set; }
        public List<ReportParameter> Parameters { get; set; }
        //public string AbsoluteReportPath { get; set; }
        public bool EnableExternalImages { get; set; }
        public virtual string GetReportResource()
        {
            return HostingEnvironment.MapPath(string.Format(@"{0}/{1}.rdlc", this.ReportPath, this.ReportName));
        }

        public virtual Stream GetEmbeddedResourceStream()
        {
            var reportPath = string.Format(@"{0}/{1}.rdlc", this.ReportPath, this.ReportName);
            Assembly assembly = Assembly.GetAssembly(typeof(Reports.Tests.Report1DataSource));
            List<string> resourceNames = new List<string>(assembly.GetManifestResourceNames());
            reportPath = reportPath.Replace(@"/", ".");
            reportPath = resourceNames.FirstOrDefault(r => r.Contains(reportPath));

            if (reportPath == null)
                throw new FileNotFoundException("Resource not found");

            return assembly.GetManifestResourceStream(reportPath);
        }

        //public MemoryStream GetReportStream(string format = "PDF", int page = 0, int? to = null)
        public MemoryStream GetReportStream(IReportCriteria criteria)
        {
            var format = string.IsNullOrEmpty(criteria.Format) ? "PDF" : criteria.Format;
            var page = criteria.PageFrom ?? 0;
            var to = criteria.PageTo;
            var reportName = ReportName;
            var zipping = false;

            var localReport = new LocalReport();
           
            if (Source == ReportSource.File)
            {
                localReport.ReportPath = GetReportResource();
            }
            else
            {
                localReport.LoadReportDefinition(GetEmbeddedResourceStream());
            }
            localReport.EnableExternalImages = EnableExternalImages;
            localReport.Refresh();
            if(this.Parameters!=null && this.Parameters.Any())
                localReport.SetParameters(this.Parameters);

            if (DataSources != null)
            {
                foreach (var reportDataSource in DataSources)
                {
                    localReport.DataSources.Add(reportDataSource);
                }
            }
           
            string reportType;
            string outputFormat;
            if (format.Equals("jpeg", StringComparison.InvariantCultureIgnoreCase) || format.Equals("jpg", StringComparison.InvariantCultureIgnoreCase))
            {
                reportType = "image";
                outputFormat = "jpeg";
                ContentType = "image/jpeg";
                if (page == 0 || (page != 0 && to != null && to != page))
                {
                    zipping = true;
                }
            }
            else if (format.Equals("png", StringComparison.InvariantCultureIgnoreCase))
            {
                reportType = "image";
                outputFormat = "png";
                ContentType = "image/png";
                if (page == 0 || (page != 0 && to != null && to != page))
                {
                    zipping = true;
                }
            }
            else if (format.Equals("tiff", StringComparison.InvariantCultureIgnoreCase))
            {
                reportType = "image";
                outputFormat = "tiff";
                ContentType = "image/tiff";
                page = 0;
            }
            else if (format.Equals("excel", StringComparison.InvariantCultureIgnoreCase) || format.Equals("xls", StringComparison.InvariantCultureIgnoreCase))
            {
                reportType = "Excel";
                outputFormat = "xls";
                ContentType = "application/vnd.ms-excel";
                page = 0;
            }
            else if (format.Equals("word", StringComparison.InvariantCultureIgnoreCase) || format.Equals("doc", StringComparison.InvariantCultureIgnoreCase))
            {
                reportType = "Word";
                outputFormat = "doc";
                ContentType = "application/msword";
                page = 0;
            }
            else
            {
                reportType = "PDF";
                outputFormat = "PDF";
                ContentType = "application/pdf";
                page = 0;
            }
            string mimeType = string.Empty;
            string fileNameExtension = null;

            int pageStart = 1, pageStop = 99;
            if (!zipping)
            {
                pageStart = 0;
                pageStop = 0;
            }
            if (to != null)
            {
                pageStop = to.Value;
            }
            if (page != 0)
            {
                pageStart = page;
                if (to == null)
                {
                    pageStop = page;
                }
            }

            var files = new List<byte[]>();
            for (int i = pageStart; i <= pageStop; i++)
            {
                string deviceInfo = string.Format(
                    "<DeviceInfo>" +
                    "  <OutputFormat>{0}</OutputFormat>" +
                    "  <StartPage>{1}</StartPage>" +
                    "</DeviceInfo>", outputFormat, i);

                Warning[] warnings;
                string[] streams;
                string encoding;

                //Render the report
                var renderedBytes = localReport.Render(
                    reportType,
                    deviceInfo,
                    out mimeType,
                    out encoding,
                    out fileNameExtension,
                    out streams,
                    out warnings);
                if (renderedBytes.Length == 0)
                    break;

                files.Add(renderedBytes);
            }


            if (!files.Any())
            {
                throw Exceptions.Helper.BadRequest("The report has no data.");
            }

            if (zipping == false || files.Count == 1)
            {
                FileExtension = fileNameExtension;
                Zipping = zipping;
                return new MemoryStream(files[0]);
            }
            else
            {
                using (var zip = new ZipFile())
                {
                    int currentPage = 1;
                    foreach (var file in files)
                    {
                        string fileName = string.Format("{0}_{1}.{2}", reportName, currentPage, outputFormat);
                        zip.AddEntry(fileName, file);
                        currentPage++;
                    }
                    //string zipName = string.Format("{0}.zip", fileNameStr);
                    FileExtension = ".zip";
                    Zipping = true;
                    var ms = new MemoryStream();
                    zip.Save(ms);
                    ms.Seek(0, SeekOrigin.Begin);
                    ms.Flush();

                    return ms;
                }
            }
          
        }
        public MemoryStream GetReportStreamAndSave(IReportCriteria criteria)
        {
            var format = string.IsNullOrEmpty(criteria.Format) ? "PDF" : criteria.Format;
            var page = criteria.PageFrom ?? 0;
            var to = criteria.PageTo;
            var reportName = ReportName;
            var zipping = false;

            var localReport = new LocalReport();

            if (Source == ReportSource.File)
            {
                localReport.ReportPath = GetReportResource();
            }
            else
            {
                localReport.LoadReportDefinition(GetEmbeddedResourceStream());
            }
            localReport.EnableExternalImages = EnableExternalImages;
            localReport.Refresh();
            if (this.Parameters != null && this.Parameters.Any())
                localReport.SetParameters(this.Parameters);

            if (DataSources != null)
            {
                foreach (var reportDataSource in DataSources)
                {
                    localReport.DataSources.Add(reportDataSource);
                }
            }
            
            string reportType;
            string outputFormat;
            if (format.Equals("jpeg", StringComparison.InvariantCultureIgnoreCase) || format.Equals("jpg", StringComparison.InvariantCultureIgnoreCase))
            {
                reportType = "image";
                outputFormat = "jpeg";
                ContentType = "image/jpeg";
                if (page == 0 || (page != 0 && to != null && to != page))
                {
                    zipping = true;
                }
            }
            else if (format.Equals("png", StringComparison.InvariantCultureIgnoreCase))
            {
                reportType = "image";
                outputFormat = "png";
                ContentType = "image/png";
                if (page == 0 || (page != 0 && to != null && to != page))
                {
                    zipping = true;
                }
            }
            else if (format.Equals("tiff", StringComparison.InvariantCultureIgnoreCase))
            {
                reportType = "image";
                outputFormat = "tiff";
                ContentType = "image/tiff";
                page = 0;
            }
            else if (format.Equals("excel", StringComparison.InvariantCultureIgnoreCase) || format.Equals("xls", StringComparison.InvariantCultureIgnoreCase))
            {
                reportType = "Excel";
                outputFormat = "xls";
                ContentType = "application/vnd.ms-excel";
                page = 0;
            }
            else if (format.Equals("word", StringComparison.InvariantCultureIgnoreCase) || format.Equals("doc", StringComparison.InvariantCultureIgnoreCase))
            {
                reportType = "Word";
                outputFormat = "doc";
                ContentType = "application/msword";
                page = 0;
            }
            else
            {
                reportType = "PDF";
                outputFormat = "PDF";
                ContentType = "application/pdf";
                page = 0;
            }
            string mimeType = string.Empty;
            string fileNameExtension = null;

            int pageStart = 1, pageStop = 99;
            if (!zipping)
            {
                pageStart = 0;
                pageStop = 0;
            }
            if (to != null)
            {
                pageStop = to.Value;
            }
            if (page != 0)
            {
                pageStart = page;
                if (to == null)
                {
                    pageStop = page;
                }
            }

            var files = new List<byte[]>();
            for (int i = pageStart; i <= pageStop; i++)
            {
                string deviceInfo = string.Format(
                    "<DeviceInfo>" +
                    "  <OutputFormat>{0}</OutputFormat>" +
                    "  <StartPage>{1}</StartPage>" +
                    "</DeviceInfo>", outputFormat, i);

                Warning[] warnings;
                string[] streams;
                string encoding;

                //Render the report
                var renderedBytes = localReport.Render(
                    reportType,
                    deviceInfo,
                    out mimeType,
                    out encoding,
                    out fileNameExtension,
                    out streams,
                    out warnings);
                if (renderedBytes.Length == 0)
                    break;


                //test local Path
                string root = "C:\\attachment\\";
                if (!System.IO.Directory.Exists(root))
                {
                    System.IO.Directory.CreateDirectory(root);
                }

                using (System.IO.FileStream sourceStream = System.IO.File.Open(root, System.IO.FileMode.Open))
                {
                    sourceStream.Write(renderedBytes, 0, renderedBytes.Length);
                    sourceStream.Close();
                }

                files.Add(renderedBytes);
            }


            if (!files.Any())
            {
                throw new BadReadException();
            }

            if (zipping == false || files.Count == 1)
            {
                FileExtension = fileNameExtension;
                Zipping = zipping;
                return new MemoryStream(files[0]);
            }
            else
            {
                using (var zip = new ZipFile())
                {
                    int currentPage = 1;
                    foreach (var file in files)
                    {
                        string fileName = string.Format("{0}_{1}.{2}", reportName, currentPage, outputFormat);
                        zip.AddEntry(fileName, file);
                        currentPage++;
                    }
                    //string zipName = string.Format("{0}.zip", fileNameStr);
                    FileExtension = ".zip";
                    Zipping = true;
                    var ms = new MemoryStream();
                    zip.Save(ms);
                    ms.Seek(0, SeekOrigin.Begin);
                    ms.Flush();

                    return ms;
                }
            }
        }
        
    }
}