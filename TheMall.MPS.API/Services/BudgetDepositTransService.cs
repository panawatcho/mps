﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using CrossingSoft.Framework.Infrastructure.OData.Repositories;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.Infrastructure.Repositories;
using TheMall.MPS.Models.Budget;

namespace TheMall.MPS.API.Services
{
    public interface IBudgetDepositTransService : Interfaces.IODataService<MPS_BudgetDepositTrans>
    {
        decimal GetBudgetDepositTransTotal(int lineId);
    }
    public class BudgetDepositTransService : BaseODataService<MPS_BudgetDepositTrans>, IBudgetDepositTransService
    {
        private readonly IMpsODataRepositoryAsync<MPS_BudgetDepositTrans> _repository;

        public BudgetDepositTransService(IMpsODataRepositoryAsync<MPS_BudgetDepositTrans> repository)
            : base(repository)
        {
            _repository = repository;
        }

        public decimal GetBudgetDepositTransTotal(int depositId)
        {
            decimal balance = 0;
            if (depositId == 0) return balance;
            var depositModel = GetWhere(m => !m.InActive && m.ProposalDepositId == depositId);
            if (depositModel != null)
            {
                balance = depositModel.Sum(m => m.Amount);
            }

            return balance;
        }

    }
}