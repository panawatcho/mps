﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CrossingSoft.Framework.Infrastructure.OData.Repositories;
using CrossingSoft.Framework.Services.OData;
using TheMall.MPS.Models.Mail;


namespace TheMall.MPS.API.Services
{
    public interface IMailSetupService : IODataServiceAsync<MailSetup>
    {
        MailSetup GetMailSetup(int? mailSetupId);
        Task<MailSetup> GetMailSetupAsync(int? mailSetupId);

        MailSetup GetNotificationSetup(string processCode, string activityName);
        MailSetup GetInformSetup(string processCode, string activityName);
        MailSetup GetEscalationSetup(string processCode, string activityName, string extra);
        string GetEscalationSetup_ContentId(string processCode, string activityName, string extra);
    }
    public class MailSetupService : ODataServiceAsync<MailSetup>, IMailSetupService
    {
        private readonly IODataRepositoryAsync<MailSetup> _repository;

        public MailSetupService(IODataRepositoryAsync<MailSetup> repository)
            : base(repository)
        {
            _repository = repository;
        }

        public MailSetup GetMailSetup(int? mailSetupId)
        {
            if (mailSetupId == null)
            {
                throw new ArgumentNullException("mailSetupId");
            }
            var setup = _repository.Find(m => m.Id == mailSetupId);
            if (setup == null)
                return null;
            return new List<MailSetup>(new[] {setup}).First();
        }

        public async Task<MailSetup> GetMailSetupAsync(int? mailSetupId)
        {
            if (mailSetupId == null)
            {
                throw new ArgumentNullException("mailSetupId");
            }
            var setup = await _repository.FindAsync(m => m.Id == mailSetupId);
            if (setup == null)
                return null;

            return new List<MailSetup>(new[] {setup}).First();
        }

        public MailSetup GetNotificationSetup(string processCode, string activityName)
        {
            var mailSetupId = DoGetNotificationSetup(processCode, activityName).Id;

            return GetMailSetup(mailSetupId);
        }

        private MailSetup DoGetNotificationSetup(string processName, string activityName)
        {
            var exactly = GetWhere(m =>
                !string.IsNullOrEmpty(m.ProcessName) && m.ProcessName.Equals(processName, StringComparison.InvariantCultureIgnoreCase)
                && m.MailType == Models.Enums.MailType.Notification
                && !string.IsNullOrEmpty(m.ActivityName) && m.ActivityName.Equals(activityName, StringComparison.InvariantCultureIgnoreCase)).ToListSafe();

            if (exactly.Any())
            {
                return exactly.First();
            }

            var matchProcessButEmptyActivity = GetWhere(m =>
                !string.IsNullOrEmpty(m.ProcessName) && m.ProcessName.Equals(processName, StringComparison.InvariantCultureIgnoreCase)
                && m.MailType == Models.Enums.MailType.Notification
                && string.IsNullOrEmpty(m.ActivityName)).ToListSafe();

            if (matchProcessButEmptyActivity.Any())
            {
                return matchProcessButEmptyActivity.First();
            }

            var matchActivityButEmptyProcess = GetWhere(m =>
                string.IsNullOrEmpty(m.ProcessName)
                && m.MailType == Models.Enums.MailType.Notification
                && !string.IsNullOrEmpty(m.ActivityName) && m.ActivityName.Equals(activityName, StringComparison.InvariantCultureIgnoreCase)).ToListSafe();

            if (matchActivityButEmptyProcess.Any())
            {
                return matchActivityButEmptyProcess.First();
            }

            var allEmpty = GetWhere(m =>
                string.IsNullOrEmpty(m.ProcessName)
                && m.MailType == Models.Enums.MailType.Notification
                && string.IsNullOrEmpty(m.ActivityName)).ToListSafe();

            if (allEmpty.Any())
            {
                return allEmpty.First();
            }

            throw new Exception(string.Format("No Notification Setup found for '{0}' '{1}'", processName, activityName));
        }

        public MailSetup GetInformSetup(string processCode, string activityName)
        {
            var mailSetupId = DoGetInformSetup(processCode, activityName).Id;

            return GetMailSetup(mailSetupId);
        }

        private MailSetup DoGetInformSetup(string processName, string activityName)
        {
            var exactly = GetWhere(m =>
                !string.IsNullOrEmpty(m.ProcessName) && m.ProcessName.Equals(processName, StringComparison.InvariantCultureIgnoreCase)
                && m.MailType == Models.Enums.MailType.Inform
                && !string.IsNullOrEmpty(m.ActivityName) && m.ActivityName.Equals(activityName, StringComparison.InvariantCultureIgnoreCase)).ToListSafe();

            if (exactly.Any())
            {
                return exactly.First();
            }

            var matchProcessButEmptyActivity = GetWhere(m =>
                !string.IsNullOrEmpty(m.ProcessName) && m.ProcessName.Equals(processName, StringComparison.InvariantCultureIgnoreCase)
                && m.MailType == Models.Enums.MailType.Inform
                && string.IsNullOrEmpty(m.ActivityName)).ToListSafe();

            if (matchProcessButEmptyActivity.Any())
            {
                return matchProcessButEmptyActivity.First();
            }

            var matchActivityButEmptyProcess = GetWhere(m =>
                string.IsNullOrEmpty(m.ProcessName)
                && m.MailType == Models.Enums.MailType.Inform
                && !string.IsNullOrEmpty(m.ActivityName) && m.ActivityName.Equals(activityName, StringComparison.InvariantCultureIgnoreCase)).ToListSafe();

            if (matchActivityButEmptyProcess.Any())
            {
                return matchActivityButEmptyProcess.First();
            }

            var allEmpty = GetWhere(m =>
                string.IsNullOrEmpty(m.ProcessName)
                && m.MailType == Models.Enums.MailType.Inform
                && string.IsNullOrEmpty(m.ActivityName)).ToListSafe();

            if (allEmpty.Any())
            {
                return allEmpty.First();
            }

            return null;
        }

        public string GetEscalationSetup_ContentId(string processCode, string activityName, string extra)
        {
            var setup = DoGetEscalationSetup(processCode, activityName, extra);
            return setup == null ? null : setup.MailContentId;
        }

        public MailSetup GetEscalationSetup(string processCode, string activityName, string extra)
        {
            return DoGetEscalationSetup(processCode, activityName, extra);
        }

        private MailSetup DoGetEscalationSetup(string processCode, string activityName, string extra)
        {
            if (!string.IsNullOrEmpty(extra))
            {
                var exactly = GetWhere(m =>
                    !string.IsNullOrEmpty(m.ProcessName) && m.ProcessName.Equals(processCode, StringComparison.InvariantCultureIgnoreCase)
                    && m.MailType == Models.Enums.MailType.Escalation
                    && !string.IsNullOrEmpty(m.ActivityName) && m.ActivityName.Equals(activityName, StringComparison.InvariantCultureIgnoreCase)
                    && !string.IsNullOrEmpty(m.Extra) && m.Extra.Equals(extra, StringComparison.InvariantCultureIgnoreCase)).ToListSafe();

                if (exactly.Any())
                {
                    return exactly.First();
                }

                var matchProcessButEmptyActivity = GetWhere(m =>
                    !string.IsNullOrEmpty(m.ProcessName) &&
                    m.ProcessName.Equals(processCode, StringComparison.InvariantCultureIgnoreCase)
                    && m.MailType == Models.Enums.MailType.Escalation
                    && string.IsNullOrEmpty(m.ActivityName)
                    && !string.IsNullOrEmpty(m.Extra) &&
                    m.Extra.Equals(extra, StringComparison.InvariantCultureIgnoreCase)).ToListSafe();

                if (matchProcessButEmptyActivity.Any())
                {
                    return matchProcessButEmptyActivity.First();
                }

                var matchActivityButEmptyProcess = GetWhere(m =>
                    string.IsNullOrEmpty(m.ProcessName)
                    && m.MailType == Models.Enums.MailType.Escalation
                    && !string.IsNullOrEmpty(m.ActivityName) &&
                    m.ActivityName.Equals(activityName, StringComparison.InvariantCultureIgnoreCase)
                    && !string.IsNullOrEmpty(m.Extra) &&
                    m.Extra.Equals(extra, StringComparison.InvariantCultureIgnoreCase)).ToListSafe();

                if (matchActivityButEmptyProcess.Any())
                {
                    return matchActivityButEmptyProcess.First();
                }

                var allEmpty = GetWhere(m =>
                    string.IsNullOrEmpty(m.ProcessName)
                    && m.MailType == Models.Enums.MailType.Escalation
                    && string.IsNullOrEmpty(m.ActivityName)
                    && !string.IsNullOrEmpty(m.Extra) &&
                    m.Extra.Equals(extra, StringComparison.InvariantCultureIgnoreCase)).ToListSafe();

                if (allEmpty.Any())
                {
                    return allEmpty.First();
                }
            }
            else
            {
                var exactly = GetWhere(m =>
                    !string.IsNullOrEmpty(m.ProcessName) &&
                    m.ProcessName.Equals(processCode, StringComparison.InvariantCultureIgnoreCase)
                    && m.MailType == Models.Enums.MailType.Escalation
                    && !string.IsNullOrEmpty(m.ActivityName) &&
                    m.ActivityName.Equals(activityName, StringComparison.InvariantCultureIgnoreCase)
                    && string.IsNullOrEmpty(m.Extra)).ToListSafe();

                if (exactly.Any())
                {
                    return exactly.First();
                }

                var matchProcessButEmptyActivity = GetWhere(m =>
                    !string.IsNullOrEmpty(m.ProcessName) &&
                    m.ProcessName.Equals(processCode, StringComparison.InvariantCultureIgnoreCase)
                    && m.MailType == Models.Enums.MailType.Escalation
                    && string.IsNullOrEmpty(m.ActivityName)
                    && string.IsNullOrEmpty(m.Extra)).ToListSafe();

                if (matchProcessButEmptyActivity.Any())
                {
                    return matchProcessButEmptyActivity.First();
                }

                var matchActivityButEmptyProcess = GetWhere(m =>
                    string.IsNullOrEmpty(m.ProcessName)
                    && m.MailType == Models.Enums.MailType.Escalation
                    && !string.IsNullOrEmpty(m.ActivityName) &&
                    m.ActivityName.Equals(activityName, StringComparison.InvariantCultureIgnoreCase)
                    && string.IsNullOrEmpty(m.Extra)).ToListSafe();

                if (matchActivityButEmptyProcess.Any())
                {
                    return matchActivityButEmptyProcess.First();
                }

                var allEmpty = GetWhere(m =>
                    string.IsNullOrEmpty(m.ProcessName)
                    && m.MailType == Models.Enums.MailType.Escalation
                    && string.IsNullOrEmpty(m.ActivityName)
                    && string.IsNullOrEmpty(m.Extra)).ToListSafe();

                if (allEmpty.Any())
                {
                    return allEmpty.First();
                }
            }

            return null;
        }
    }
}