﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Infrastructure.Repositories;
using TheMall.MPS.Models.View;

namespace TheMall.MPS.API.Services
{
    public interface IProposalTrackingViewService : IODataService<MPS_VW_ProposalTracking>
    {
    }
    public class ProposalTrackingViewService : BaseODataService<MPS_VW_ProposalTracking>, IProposalTrackingViewService
    {
        public ProposalTrackingViewService(IMpsODataRepositoryAsync<MPS_VW_ProposalTracking> repository) 
            : base(repository)
        {
        }
    }
}