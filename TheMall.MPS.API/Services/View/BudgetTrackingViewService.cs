﻿using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Infrastructure.Repositories;
using TheMall.MPS.Models.View;

namespace TheMall.MPS.API.Services
{
    public interface IBudgetTrackingViewService : IODataService<MPS_VW_BudgetTracking>
    {
    }
    public class BudgetTrackingViewService : BaseODataService<MPS_VW_BudgetTracking>, IBudgetTrackingViewService
    {
        public BudgetTrackingViewService(IMpsODataRepositoryAsync<MPS_VW_BudgetTracking> repository) 
            : base(repository)
        {
        }
    }
}