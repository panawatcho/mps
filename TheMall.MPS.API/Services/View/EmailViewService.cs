﻿using System.Collections.Generic;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Infrastructure.Repositories;
using TheMall.MPS.Models.View;

namespace TheMall.MPS.API.Services
{
    public interface IEmailViewService : IODataService<MPS_VW_Email>
    {
        IEnumerable<MPS_VW_Email> SearchByEmail(string email);
    }
    public class EmailViewService : BaseODataService<MPS_VW_Email>, IEmailViewService
    {
        public EmailViewService(IMpsODataRepositoryAsync<MPS_VW_Email> repository) 
            : base(repository)
        {
        }

        public IEnumerable<MPS_VW_Email> SearchByEmail(string email)
        {
            if (string.IsNullOrEmpty(email) || string.IsNullOrWhiteSpace(email))
                return GetAll();
            return GetWhere(m => m.Email.StartsWith(email));
        }
    }
}