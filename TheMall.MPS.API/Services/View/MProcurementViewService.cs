﻿using System.Collections.Generic;
using System.Linq;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Infrastructure.Repositories;
using TheMall.MPS.Models.View;

namespace TheMall.MPS.API.Services
{
    public interface IMProcurementViewService : IODataService<MPS_VW_MProcurement>
    {
       bool CheckProcurement(string proposalNumber);
    }
    public class MProcurementViewService : BaseODataService<MPS_VW_MProcurement>, IMProcurementViewService
    {
        public MProcurementViewService(IMpsODataRepositoryAsync<MPS_VW_MProcurement> repository) 
            : base(repository)
        {
        }

        public bool CheckProcurement(string proposalNumber)
        {
            var result = true;
            if (string.IsNullOrEmpty(proposalNumber))
                return false;

            var litsDocument = GetWhere(m => m.PROPOSALID == proposalNumber);
            if (litsDocument == null)
                return true;
            if (litsDocument.Any(m=>m.PRStatusFlag == 1))
            {
                return false;
            }
            if(litsDocument.Any(m=>m.PRStatusFlag == 2 && m.POStatusFlag ==9 ) || litsDocument.Any(m => m.PRStatusFlag == 2 && m.POStatusFlag == 5))
            {
                return false;
            }
            return result;
        }
        
    }
}