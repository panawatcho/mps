﻿using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Infrastructure.Repositories;
using TheMall.MPS.Models.View;

namespace TheMall.MPS.API.Services
{
    public interface IBudgetYearTrackingViewService : IODataService<MPS_VW_BudgetYearTracking>
    {
    }
    public class BudgetYearTrackingViewService : BaseODataService<MPS_VW_BudgetYearTracking>, IBudgetYearTrackingViewService
    {
        public BudgetYearTrackingViewService(IMpsODataRepositoryAsync<MPS_VW_BudgetYearTracking> repository) 
            : base(repository)
        {
        }
    }
}