﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Infrastructure.Repositories;
using TheMall.MPS.Models.View;

namespace TheMall.MPS.API.Services
{
    public interface IDepositTrackingViewService : IODataService<MPS_VW_DepositTracking>
    {
    }
    public class DepositTrackingViewService : BaseODataService<MPS_VW_DepositTracking>, IDepositTrackingViewService
    {
        public DepositTrackingViewService(IMpsODataRepositoryAsync<MPS_VW_DepositTracking> repository) 
            : base(repository)
        {
        }
    }
}