﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Infrastructure.Repositories;
using TheMall.MPS.Models.View;

namespace TheMall.MPS.API.Services
{
    public interface IProposalUnitCodeViewService : IODataService<MPS_VW_ProposalUnitCode>
    {
    }
    public class ProposalUnitCodeViewService : BaseODataService<MPS_VW_ProposalUnitCode>, IProposalUnitCodeViewService
    {
        public ProposalUnitCodeViewService(IMpsODataRepositoryAsync<MPS_VW_ProposalUnitCode> repository) 
            : base(repository)
        {
        }
    }
}