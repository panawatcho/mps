﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Infrastructure.Repositories;
using TheMall.MPS.Models.View;

namespace TheMall.MPS.API.Services
{
    public interface IDocumentRelatedViewService : IODataService<MPS_VW_DocumentRelated>
    {
    }
    public class DocumentRelatedViewService : BaseODataService<MPS_VW_DocumentRelated>, IDocumentRelatedViewService
    {
        public DocumentRelatedViewService(IMpsODataRepositoryAsync<MPS_VW_DocumentRelated> repository) 
            : base(repository)
        {
        }
    }
}