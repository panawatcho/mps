﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CrossingSoft.Framework.Infrastructure.OData.Repositories;
using CrossingSoft.Framework.Services.OData;
using TheMall.MPS.Models.Mail;


namespace TheMall.MPS.API.Services
{
    public interface IMailContentService : IODataServiceAsync<MailContent>
    {
        MailContent GetMailContent(string mailContentId);
        Task<MailContent> GetMailContentAsync(string mailContentId);
    }
    public class MailContentService : ODataServiceAsync<MailContent>, IMailContentService
    {
        private readonly IODataRepositoryAsync<MailContent> _repository;

        public MailContentService(IODataRepositoryAsync<MailContent> repository)
            : base(repository)
        {
            _repository = repository;
        }

        public MailContent GetMailContent(string mailContentId)
        {
            if (string.IsNullOrEmpty(mailContentId))
            {
                throw new ArgumentNullException("mailContentId");
            }
            var mailContent = new List<MailContent>(){this.Find(m => m.MailContentId == mailContentId)}.FirstOrDefault();

            return mailContent;
        }

        public async Task<MailContent> GetMailContentAsync(string mailContentId)
        {
            if (string.IsNullOrEmpty(mailContentId))
            {
                throw new ArgumentNullException("mailContentId");
            }
            var mailContent = this.FindAsync(m => m.MailContentId == mailContentId);

            return await mailContent;
        }
    }
}