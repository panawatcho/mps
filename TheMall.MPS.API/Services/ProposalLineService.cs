﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using CrossingSoft.Framework.Infrastructure.OData.Repositories;
using CrossingSoft.Framework.Services.OData;
using SourceCode.Workflow.Client;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Infrastructure.EntityFramework;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Models.Enums;
using TheMall.MPS.Models.Proposal;
using TheMall.MPS.ViewModels.InterfaceToMprocurement;

namespace TheMall.MPS.API.Services
{
    public interface IProposalLineService : Interfaces.IODataService<MPS_ProposalLine>
    {
        MPS_ProposalLine FindProposalLine(RequestViewModel viewModel);
        MPS_ProposalLine FindProposalLine(string proposalNo, string apCode, string unitCode, string expenseTopicCode, string apDescription);
        IEnumerable<MPS_ProposalLine> GetOpenProposalLine();
    }
    public class ProposalLineService : BaseODataService<MPS_ProposalLine>, IProposalLineService
    {
        public ProposalLineService(IODataRepositoryAsync<MPS_ProposalLine> repository)
            : base(repository)
        {
        }
        public MPS_ProposalLine FindProposalLine(RequestViewModel viewModel)
        {
            return FindProposalLine(viewModel.ProposalNumber, viewModel.APCode, viewModel.UnitCode, viewModel.ExpenseTopic,viewModel.APDescription);
        }
        public MPS_ProposalLine FindProposalLine(string proposalNumber, string apCode, string unitCode, string expenseTopicCode,string description)
        {
            return LastOrDefault(m => m.APCode.Equals(apCode, StringComparison.InvariantCultureIgnoreCase)
                                      && m.UnitCode.Equals(unitCode, StringComparison.InvariantCultureIgnoreCase)
                                      && m.ExpenseTopicCode.Equals(expenseTopicCode, StringComparison.InvariantCultureIgnoreCase)
                                      && m.ProposalTable.DocumentNumber.Equals(proposalNumber, StringComparison.InvariantCultureIgnoreCase)
                                      && m.Description.Equals(description,StringComparison.InvariantCultureIgnoreCase)
                                      && !m.Deleted);
        }

        public IEnumerable<MPS_ProposalLine> GetOpenProposalLine()
        {
            return GetWhere(m => m.ProposalTable != null
                                 && m.ProposalTable.StatusFlag == DocumentStatusFlagId.Completed
                                 && m.ProposalTable.CloseFlag == false
                                 && m.ProposalTable.ProposalTypeCode != "DEPOSIT"
                                 && m.ProposalTable.ProposalTypeCode != "DEPOSITIN"
                                 && !string.IsNullOrEmpty(m.APCode)
                                 && !string.IsNullOrEmpty(m.UnitCode));
        }
    }
}