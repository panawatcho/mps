﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CrossingSoft.Framework.Infrastructure.OData.Repositories;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.Models.CashAdvance;
using TheMall.MPS.Models.PettyCash;

namespace TheMall.MPS.API.Services
{
    public interface ICashAdvanceLineService : Interfaces.IODataService<MPS_CashAdvanceLine>
    {
    }
    public class CashAdvanceLineService : BaseODataService<MPS_CashAdvanceLine>, ICashAdvanceLineService
    {
        public CashAdvanceLineService(IODataRepositoryAsync<MPS_CashAdvanceLine> repository) 
            : base(repository)
        {
        }
    }
}