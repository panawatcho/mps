﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using CrossingSoft.Framework.Infrastructure;
using CrossingSoft.Framework.Infrastructure.OData.Repositories;
using CrossingSoft.Framework.Services.OData;
using TheMall.MPS.API.Helpers;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Models.Interfaces;


namespace TheMall.MPS.API.Services.Abstracts
{
    
    public abstract class BaseMasterService<TEntity> : BaseODataService<TEntity>, IBaseMasterService<TEntity> 
        where TEntity : class, IObjectState 
    {
         protected readonly string CacheKey = null;
         protected BaseMasterService(
            IODataRepositoryAsync<TEntity> repository)
            : base(repository)
        {
            CacheKey = typeof (TEntity).Name;
        }

        public virtual TEntity Get(string code)
        {
            return Caching.GetFromCache(CacheKey + "_" + code,
                () => DoGet(code));
          
        }
        public abstract TEntity DoGet(string code);

        public virtual async Task<TEntity> GetAsync(string code)
        {
            return await Caching.GetFromCache("Async_" + CacheKey + "_" + code,
                async () => await DoGetAsync(code));
          
        }
        public abstract Task<TEntity> DoGetAsync(string code);

       
        public  async Task<IEnumerable<TEntity>> GetAllAsyns()
        {
            return await GetAllAsync();
        }

       
    }

}