﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using CrossingSoft.Framework.Infrastructure.OData.Repositories;
using SourceCode.Workflow.Client;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Models.Abstracts;
using TheMall.MPS.Models.Interfaces;
using TheMall.MPS.Resources;


namespace TheMall.MPS.API.Services.Abstracts
{
    public abstract class MpsWorkflowTableService<TWorkflowTable> : BaseODataService<TWorkflowTable>, IMpsWorkflowTableService<TWorkflowTable>
         where TWorkflowTable : MpsWorkflowTable, IMpsWorkflowTable
    {
        protected MpsWorkflowTableService(IODataRepositoryAsync<Infrastructure.EntityFramework.IMPSDbContext, Infrastructure.Sessions.IMPSSession, TWorkflowTable> repository)
            : base(repository)
        {
        }
        public bool Exists(int id)
        {
            return Query(e => e.Id == id).Select().Any();
        }
    
        public abstract TWorkflowTable GetWorkflowTable(int id);
        public abstract Task<TWorkflowTable> GetWorkflowTableAsync(int id);

        public virtual TWorkflowTable GetWorkflowTableFromProcInstId(int procInstid)
        {
            return Query(m => m.ProcInstId == procInstid).Select().OrderByDescending(m => m).FirstOrDefault();
        }
        public virtual int? GetWorkflowTableIdFromSN(string sn)
        {
            if (string.IsNullOrEmpty(sn))
                throw new ArgumentNullException("sn");
            var procInstid = sn.GetProcInstIdFromSn();
            return Query(m => m.ProcInstId == procInstid).Select(m => m.Id).OrderByDescending(m => m).FirstOrDefault();
        }
        public TWorkflowTable Get(int id)
        {
            return this.Find(id);
        }
        public async Task<TWorkflowTable> GetAsync(int id)
        {
            return await this.FindAsync(id);
        }

        public IMpsWorkflowTable GetInterfaceWorkflowTable(int id)
        {
            var table = GetWorkflowTable(id);
            if (table == null)
            {
                throw new Exception(string.Format(Error.DataWithIDNotExists,id));
            }
            return table;
        }
    }
}