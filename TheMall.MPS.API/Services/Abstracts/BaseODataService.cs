﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Web;
using CrossingSoft.Framework.Infrastructure;
using CrossingSoft.Framework.Infrastructure.OData.Repositories;
using TheMall.MPS.API.Services.Interfaces;

namespace TheMall.MPS.API.Services.Abstracts
{
    public abstract class BaseODataService<TEntity> : CrossingSoft.Framework.Services.OData.ODataServiceAsync<TEntity>, IODataService<TEntity> where TEntity : IObjectState
    {
        private readonly IODataRepositoryAsync<TEntity> _repository;

        protected BaseODataService(IODataRepositoryAsync<TEntity> repository)
            : base(repository)
        {
            _repository = repository;
        }

        public TEntity LastOrDefault(Expression<Func<TEntity, bool>> predicate)
        {
            return GetWhere(predicate).LastOrDefault();
        }

        public async Task<TEntity> LastOrDefaultAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return (await _repository.Query(predicate).SelectAsync()).LastOrDefault();
        }
    }
}