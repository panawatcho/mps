﻿using CrossingSoft.Framework.Infrastructure;
using CrossingSoft.Framework.Infrastructure.OData.Repositories;
using TheMall.MPS.API.Services.Interfaces;

namespace TheMall.MPS.API.Services.Abstracts
{
    public abstract class BaseService<TEntity> : CrossingSoft.Framework.Services.OData.ODataServiceAsync<TEntity>, IService<TEntity> where TEntity : IObjectState
    {
        protected BaseService(IODataRepositoryAsync<TEntity> repository)
            : base(repository)
        {
        }

    }
    public abstract class BaseService : IService
    {

    }
}