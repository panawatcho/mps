﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TheMall.MPS.ViewModels.Workspace;
using SourceCode.Workflow.Client;
using Action = SourceCode.Workflow.Client.Action;
using TheMall.MPS.Models.Proposal;

namespace TheMall.MPS.API.Services.Abstracts
{
    public abstract class BaseWorkflowService : BaseService
    {
        private readonly IWorkingHourService _workingHourService;
        public const int WORKING_HOURS = 8;
        protected BaseWorkflowService(IWorkingHourService workingHourService)
        {
            _workingHourService = workingHourService;
        }
        protected MyWorklistItem ToMyWorklistItem(WorklistItem worklistItem)
        {
            //var trueStr = true.ToString();
            var item = new MyWorklistItem();
            item.SN = worklistItem.SerialNumber;
            item.Folio = worklistItem.ProcessInstance.Folio;
            item.ActivityName = worklistItem.ActivityInstanceDestination.Name;
            item.ActivityDescription = worklistItem.ActivityInstanceDestination.Description;
            item.Data = worklistItem.Data;
            item.StartDate = worklistItem.ActivityInstanceDestination.StartDate.ToUniversalTime();
            item.ProcessInstanceId = worklistItem.ProcessInstance.ID;
            item.ProcessName = worklistItem.ProcessName();
            item.Title = worklistItem.GetTitle();
            item.Status = worklistItem.Status.ToString();
            item.Actions = (from Action action in worklistItem.Actions select action.Name).ToArray();
           
            item.ExpectedDays = Math.Round(worklistItem.ExpectedDuration().TotalHours / WORKING_HOURS, 1);
            var expectedDuration = worklistItem.ExpectedDuration();
            var dueDate = _workingHourService.CalcDue(worklistItem.ActivityInstanceDestination.StartDate, expectedDuration);
            //var sla = (dueDate - DateTime.Today).TotalSeconds;
            if (dueDate.Date > DateTime.Today)
                item.DueStatus = WorklistDueStatus.Early.ToString();
            else if (dueDate.Date == DateTime.Today)
                item.DueStatus = WorklistDueStatus.Due.ToString();
            if (dueDate < DateTime.Now)
                item.DueStatus = WorklistDueStatus.Late.ToString();

            return item;
        }
        protected WorklistItemViewModel ToMyWorklistItemForProposal(WorklistItem worklistItem)
        {
            //var trueStr = true.ToString();
            var item = new WorklistItemViewModel();
            item.SN = worklistItem.SerialNumber;
            item.Folio = worklistItem.ProcessInstance.Folio;
            item.ActivityName = worklistItem.ActivityInstanceDestination.Name;
            item.ActivityDescription = worklistItem.ActivityInstanceDestination.Description;
            item.Data = worklistItem.Data;
            item.StartDate = worklistItem.ActivityInstanceDestination.StartDate.ToUniversalTime();
            item.ProcessInstanceId = worklistItem.ProcessInstance.ID;
            item.ProcessName = worklistItem.ProcessName();
            item.Title = worklistItem.GetTitle();
            item.Status = worklistItem.Status.ToString();
            item.Actions = (from Action action in worklistItem.Actions select action.Name).ToArray();

            item.ExpectedDays = Math.Round(worklistItem.ExpectedDuration().TotalHours / WORKING_HOURS, 1);
            var expectedDuration = worklistItem.ExpectedDuration();
            var dueDate = _workingHourService.CalcDue(worklistItem.ActivityInstanceDestination.StartDate, expectedDuration);
            //var sla = (dueDate - DateTime.Today).TotalSeconds;
            if (dueDate.Date > DateTime.Today)
                item.DueStatus = WorklistDueStatus.Early.ToString();
            else if (dueDate.Date == DateTime.Today)
                item.DueStatus = WorklistDueStatus.Due.ToString();
            if (dueDate < DateTime.Now)
                item.DueStatus = WorklistDueStatus.Late.ToString();

            return item;
        }
    }
}