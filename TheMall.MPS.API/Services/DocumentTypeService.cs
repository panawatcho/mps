﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CrossingSoft.Framework.Infrastructure.OData.Repositories;
using TheMall.MPS.API.Helpers;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Infrastructure.Repositories;
using TheMall.MPS.Models;

namespace TheMall.MPS.API.Services
{
    public interface IDocumentTypeService : IService<DocumentType>
    {
        DocumentType GetDocumentType(int id);
        Task<DocumentType> GetDocumentTypeAsync(int id);

        /// <summary>
        /// This is not an ideal method to use.
        /// Will return all document types regardless of their activities.
        /// </summary>
        /// <param name="process">Process code</param>
        /// <returns></returns>
        IEnumerable<DocumentType> GetAllFromProcess(string process);
        /// <summary>
        /// This is not an ideal method to use.
        /// Will return all document types regardless of their activities.
        /// </summary>
        /// <param name="process">Process code</param>
        /// <returns></returns>
        Task<IEnumerable<DocumentType>> GetAllFromProcessAsync(string process);

        /// <summary>
        /// Get document types for the specified process code AND activity.
        /// </summary>
        /// <param name="process">Process code</param>
        /// <param name="activity">Activity name. Send null or empty for document on creating.</param>
        /// <returns></returns>
        IEnumerable<DocumentType> GetFromProcess(string process, string activity = null);

        /// <summary>
        /// Get document types for the specified process code AND activity.
        /// </summary>
        /// <param name="process">Process code</param>
        /// <param name="activity">Activity name. Send null or empty for document on creating.</param>
        /// <returns></returns>
        Task<IEnumerable<DocumentType>> GetFromProcessAsync(string process, string activity = null);


        IEnumerable<DocumentType> Search(int? code = null, string name = null);
        Task<IEnumerable<DocumentType>> SearchAsync(int? code = null, string name = null);
    }
    public class DocumentTypeService : BaseService<DocumentType>, IDocumentTypeService
    {
        private readonly IODataRepositoryAsync<DocumentType> _repository;
        public DocumentTypeService(IODataRepositoryAsync<DocumentType> repository)
            : base(repository)
        {
            _repository = repository;
        }

        public DocumentType GetDocumentType(int id)
        {
            return Caching.GetFromCache("DocumentType_" + id, () => _repository.FirstOrDefault(m => m.Id == id));
        }

        public async Task<DocumentType> GetDocumentTypeAsync(int id)
        {
            return await Caching.GetFromCache("DocumentType_" + id, () => _repository.FirstOrDefaultAsync(m => m.Id == id));
        }

        public IEnumerable<DocumentType> GetAllFromProcess(string process)
        {
            if (string.IsNullOrEmpty(process))
            {
                throw new ArgumentNullException("process");
            }
            return Caching.GetFromCache("DocumentType_AllFromProcess_" + process.ToUpperInvariant(),
                () => GetWhere(m => !string.IsNullOrEmpty(m.ProcessCode)
                                  && m.ProcessCode.Equals(process, StringComparison.InvariantCultureIgnoreCase)));
        }

        public async Task<IEnumerable<DocumentType>> GetAllFromProcessAsync(string process)
        {
            if (string.IsNullOrEmpty(process))
            {
                throw new ArgumentNullException("process");
            }
            return await Caching.GetFromCache("DocumentType_AllFromProcess_" + process.ToUpperInvariant(),
                () =>GetWhereAsync(m => !string.IsNullOrEmpty(m.ProcessCode)
                                    && m.ProcessCode.Equals(process, StringComparison.InvariantCultureIgnoreCase)));
        }

        public IEnumerable<DocumentType> GetFromProcess(string process, string activity = null)
        {
            if (string.IsNullOrEmpty(process))
            {
                throw new ArgumentNullException("process");
            }
            if (string.IsNullOrEmpty(activity))
            {
                return Caching.GetFromCache("DocumentType_FromProcess_" + process.ToUpperInvariant() + "_Start",
                    () => GetWhere(m => !string.IsNullOrEmpty(m.ProcessCode)
                                        && m.ProcessCode.Equals(process, StringComparison.InvariantCultureIgnoreCase)
                                        && string.IsNullOrEmpty(m.Activity)));
            }
            else
            {
                return Caching.GetFromCache("DocumentType_FromProcess_" + process.ToUpperInvariant() + "_" + activity.ToUpperInvariant(),
                    () => GetWhere(m => !string.IsNullOrEmpty(m.ProcessCode)
                                        && m.ProcessCode.Equals(process, StringComparison.InvariantCultureIgnoreCase)
                                        && !string.IsNullOrEmpty(m.Activity)
                                        && m.Activity.Equals(activity, StringComparison.InvariantCultureIgnoreCase)));
            }
        }

        public async Task<IEnumerable<DocumentType>> GetFromProcessAsync(string process, string activity = null)
        {
            if (string.IsNullOrEmpty(process))
            {
                throw new ArgumentNullException("process");
            }
            if (string.IsNullOrEmpty(activity))
            {
                return await Caching.GetFromCache("DocumentType_FromProcess_" + process.ToUpperInvariant() + "_Start",
                    () => GetWhereAsync(m => !string.IsNullOrEmpty(m.ProcessCode)
                                        && m.ProcessCode.Equals(process, StringComparison.InvariantCultureIgnoreCase)
                                        && string.IsNullOrEmpty(m.Activity)));
            }
            else
            {
                return await Caching.GetFromCache("DocumentType_FromProcess_" + process.ToUpperInvariant() + "_" + activity.ToUpperInvariant(),
                    () => GetWhereAsync(m => !string.IsNullOrEmpty(m.ProcessCode)
                                        && m.ProcessCode.Equals(process, StringComparison.InvariantCultureIgnoreCase)
                                        && !string.IsNullOrEmpty(m.Activity)
                                        && m.Activity.Equals(activity, StringComparison.InvariantCultureIgnoreCase)));
            }
        }

        public override IEnumerable<DocumentType> GetAll()
        {
            return Caching.GetFromCache("DocumentType_GetAll", base.GetAll);
        }

        public async override Task<IEnumerable<DocumentType>> GetAllAsync()
        {
            return await Caching.GetFromCache("DocumentType_GetAll", () => base.GetAllAsync());
        }

        public IEnumerable<DocumentType> Search(int? code = null, string name = null)
        {
            if (code == null && string.IsNullOrEmpty(name))
            {
                return GetAll();
            }
            return GetWhere(m => m.Id == code ||
                m.Name.Contains(name));
        }

        public async Task<IEnumerable<DocumentType>> SearchAsync(int? code = null, string name = null)
        {
            if (code == null && string.IsNullOrEmpty(name))
            {
                return await GetAllAsync();
            }

            return await GetWhereAsync(m => m.Id == code ||
                 m.Name.Contains(name));
        }
    }
}