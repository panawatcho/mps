﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Web;
using CrossingSoft.Framework.Infrastructure.OData.Repositories;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Models.NumberSeq;
using TheMall.MPS.Resources;


namespace TheMall.MPS.API.Services
{
    public interface INumberSequenceService : IService
    {
        string GetNextNumber(string process, DateTime? documentDate, string dimension1 = "", string dimension2 = "", string dimension3 = "");
        string GetProcessCode(string proposalTypeCode);
    }
    public class NumberSequenceService : BaseService, INumberSequenceService
    {
        private readonly IODataRepositoryAsync<Infrastructure.EntityFramework.IMPSDbContext, Infrastructure.Sessions.IMPSSession, NumberSeqTable> _numberSeqTableRepo;
        private readonly IODataRepositoryAsync<Infrastructure.EntityFramework.IMPSDbContext, Infrastructure.Sessions.IMPSSession, NumberSeqSetup> _numberSeqSetupRepo;

        private static object _sync = new object();

        public NumberSequenceService(IODataRepositoryAsync<Infrastructure.EntityFramework.IMPSDbContext, Infrastructure.Sessions.IMPSSession, NumberSeqTable> numberSeqTableRepo,
            IODataRepositoryAsync<Infrastructure.EntityFramework.IMPSDbContext, Infrastructure.Sessions.IMPSSession, NumberSeqSetup> numberSeqSetupRepo)
        {
            _numberSeqTableRepo = numberSeqTableRepo;
            _numberSeqSetupRepo = numberSeqSetupRepo;
        }

        public string GetNextNumber(string process, DateTime? documentDate, string dimension1 = "", string dimension2 = "", string dimension3 = "")
        {
            lock (_sync)
            {
                var numberSeqTable = GetNumberSeqTable(process, documentDate, dimension1, dimension2, dimension3);
                if (numberSeqTable == null || string.IsNullOrEmpty(numberSeqTable.NumberSeqCode))
                {
                    var msg = string.Format("No number sequence setup found for process '{0}', '{1}'. ", process,
                        documentDate.Value.Year.ToString(CultureInfo.InvariantCulture));
                    var dim = string.Join("|", dimension1, dimension2, dimension3).Trim('|');
                    throw new Exception(string.IsNullOrEmpty(dim) ? msg : msg + " " + dim);
                }
                return GetNextNumber(numberSeqTable, documentDate);
            }
        }

        public NumberSeqTable GetNumberSeqTable(string process, DateTime? documentDate, string dimension1 = "", string dimension2 = "", string dimension3 = "")
        {
            if (documentDate == null)
            {
                throw new ArgumentNullException("documentDate");
            }
            if (string.IsNullOrEmpty(process))
            {
                throw new ArgumentNullException("process");
            }
            var year = documentDate.Value.Year.ToString(CultureInfo.InvariantCulture);

            NumberSeqSetup numberSeqSetup = null;
            //Find Setup with Condition
            ParameterExpression argParam = Expression.Parameter(typeof(NumberSeqSetup), "m");
            //Add Process Expression
            Expression nameProperty = Expression.Property(argParam, "Process");
            BinaryExpression andExp = null;
            var val = Expression.Constant(process);
            var exp = Expression.Equal(nameProperty, val);
            andExp = exp;
            //Add Year Expression
            nameProperty = Expression.Property(argParam, "Year");
            val = Expression.Constant(year);
            exp = Expression.Equal(nameProperty, val);
            andExp = Expression.And(andExp, exp);
            if (!string.IsNullOrEmpty(dimension1))
            {
                //Add Dimension1 Expression
                nameProperty = Expression.Property(argParam, "Dimension1");
                val = Expression.Constant(dimension1);
                exp = Expression.Equal(nameProperty, val);
                andExp = Expression.And(andExp, exp);
            }
            if (!string.IsNullOrEmpty(dimension2))
            {
                //Add Dimension2 Expression
                nameProperty = Expression.Property(argParam, "Dimension2");
                val = Expression.Constant(dimension2);
                exp = Expression.Equal(nameProperty, val);
                andExp = Expression.And(andExp, exp);
            }
            if (!string.IsNullOrEmpty(dimension3))
            {
                //Add Dimension3 Expression
                nameProperty = Expression.Property(argParam, "Dimension3");
                val = Expression.Constant(dimension3);
                exp = Expression.Equal(nameProperty, val);
                andExp = Expression.And(andExp, exp);
            }
            var lambda = Expression.Lambda<Func<NumberSeqSetup, bool>>(andExp, argParam);
            numberSeqSetup = _numberSeqSetupRepo.FirstOrDefault(lambda);

            if (numberSeqSetup == null || string.IsNullOrEmpty(numberSeqSetup.NumberSeqCode))
            {
                return null;
            }

            var numberSeqTable = numberSeqSetup.NumberSeqTable;

            return numberSeqTable;
        }

        public async Task<string> GetNextNumberAsync(string process, DateTime? documentDate, string dimension1 = "", string dimension2 = "", string dimension3 = "")
        {
            var numberSeqTable = await GetNumberSeqTableAsync(process, documentDate, dimension1, dimension2, dimension3);
            if (numberSeqTable == null || string.IsNullOrEmpty(numberSeqTable.NumberSeqCode))
            {
                var msg = string.Format("No number sequence setup found for process '{0}', '{1}'. ", process,
                    documentDate.Value.Year.ToString(CultureInfo.InvariantCulture));
                var dim = string.Join("|", dimension1, dimension2, dimension3).Trim('|');
                throw new Exception(string.IsNullOrEmpty(dim) ? msg : msg + " " + dim);
            }
            return GetNextNumber(numberSeqTable, documentDate);
        }

        public async Task<NumberSeqTable> GetNumberSeqTableAsync(string process, DateTime? documentDate, string dimension1 = "", string dimension2 = "", string dimension3 = "")
        {
            if (documentDate == null)
            {
                throw new ArgumentNullException("documentDate");
            }
            if (string.IsNullOrEmpty(process))
            {
                throw new ArgumentNullException("process");
            }
            var year = documentDate.Value.Year.ToString(CultureInfo.InvariantCulture);

            NumberSeqSetup numberSeqSetup = null;
            //Find Setup with Condition
            ParameterExpression argParam = Expression.Parameter(typeof(NumberSeqSetup), "m");
            //Add Process Expression
            Expression nameProperty = Expression.Property(argParam, "Process");
            BinaryExpression andExp = null;
            var val = Expression.Constant(process);
            var exp = Expression.Equal(nameProperty, val);
            andExp = exp;
            //Add Year Expression
            nameProperty = Expression.Property(argParam, "Year");
            val = Expression.Constant(year);
            exp = Expression.Equal(nameProperty, val);
            andExp = Expression.And(andExp, exp);
            if (!string.IsNullOrEmpty(dimension1))
            {
                //Add Dimension1 Expression
                nameProperty = Expression.Property(argParam, "Dimension1");
                val = Expression.Constant(dimension1);
                exp = Expression.Equal(nameProperty, val);
                andExp = Expression.And(andExp, exp);
            }
            if (!string.IsNullOrEmpty(dimension2))
            {
                //Add Dimension2 Expression
                nameProperty = Expression.Property(argParam, "Dimension2");
                val = Expression.Constant(dimension2);
                exp = Expression.Equal(nameProperty, val);
                andExp = Expression.And(andExp, exp);
            }
            if (!string.IsNullOrEmpty(dimension3))
            {
                //Add Dimension3 Expression
                nameProperty = Expression.Property(argParam, "Dimension3");
                val = Expression.Constant(dimension3);
                exp = Expression.Equal(nameProperty, val);
                andExp = Expression.And(andExp, exp);
            }
            var lambda = Expression.Lambda<Func<NumberSeqSetup, bool>>(andExp, argParam);
            numberSeqSetup =
                    await _numberSeqSetupRepo.FirstOrDefaultAsync(lambda);

            if (numberSeqSetup == null || string.IsNullOrEmpty(numberSeqSetup.NumberSeqCode))
            {
                return null;
            }

            var numberSeqTable = numberSeqSetup.NumberSeqTable;

            return numberSeqTable;
        }

        public string GetNextNumber(string numberSeqCode, DateTime? dateTime)
        {
            if (numberSeqCode == null)
            {
                throw new ArgumentNullException("numberSeqCode");
            }
            var numberSeqTable = _numberSeqTableRepo.Find(numberSeqCode);
            if (numberSeqTable == null)
            {
                throw new Exception(string.Format("Number Sequence code '{0}' could not be found.", numberSeqCode));
            }
            return GetNextNumber(numberSeqTable, dateTime);
        }

        public string GetNextNumber(NumberSeqTable numberSeqTable , DateTime? dateTime)
        {
            if (numberSeqTable == null)
            {
                throw new ArgumentNullException("numberSeqTable");
            }

            if (string.IsNullOrEmpty(numberSeqTable.Format))
            {
                throw new Exception(string.Format(Error.NumberSeqFormatNotSet, numberSeqTable.NumberSeqCode));
            }

            //find index for year format
            if (!numberSeqTable.Format.Contains("yy"))
            {
                throw new Exception(string.Format(Error.NumberSeqFormatIncorrectNeedSharp, numberSeqTable.NumberSeqCode));
            }

            if (!numberSeqTable.Format.Contains("#"))
            {
                throw new Exception(string.Format(Error.NumberSeqFormatIncorrectNeedSharp, numberSeqTable.NumberSeqCode));
            }


         
            

            var format = numberSeqTable.Format;

            //var currentYear = format.IndexOf("yy", StringComparison.Ordinal);
            var shortYearFormat = (dateTime == null) ? DateTime.Now.ToString("yy") : dateTime.Value.ToString("yy");
            
            var firstIndexOfSharp = format.IndexOf('#');

            // Format can only has 1 sequence of #
            var charArray = format.ToCharArray();
            var lastIndexOfSharp = firstIndexOfSharp;
            while (lastIndexOfSharp < charArray.Count() && charArray[lastIndexOfSharp] == '#')
            {
                lastIndexOfSharp++;
            }
            if (format.IndexOf('#', lastIndexOfSharp) > 0)
            {
                throw new Exception(string.Format(Error.NumerSeqFormatIncorrectMoreThanOneSequence, numberSeqTable.NumberSeqCode));
            }

            bool saveFailed;
            do
            {
                saveFailed = false;
                try
                {


                    // GO !!
                    var num = numberSeqTable.NextNum;
                    if (num == 0)
                    {
                        num = 1;
                    }
                    var prefix = format.Substring(0, firstIndexOfSharp);
                    var suffix = format.Substring(lastIndexOfSharp);

                    var numStr = num.ToString().PadLeft(lastIndexOfSharp - firstIndexOfSharp, '0');
                    var result = prefix + numStr + suffix;

                    //replace year
                    result = result.Replace("yy", shortYearFormat);

                    numberSeqTable.NextNum = numberSeqTable.NextNum + 1;
                    _numberSeqTableRepo.Update(numberSeqTable);
                    return result;
                }
                catch (DbUpdateConcurrencyException exception)
                {
                    saveFailed = true;
                    // Update the values of the entity that failed to save from the store 
                    exception.Entries.Single().Reload();
                }
            } while (saveFailed);
            return null;
        }

        public string GetProcessCode(string proposalTypeCode)
        {
            string processCode = String.Empty;
            
            if (!string.IsNullOrEmpty(proposalTypeCode))
            {
                var numberSeq = _numberSeqSetupRepo.Find(m => m.NumberSeqCode.Equals(proposalTypeCode));
                if (numberSeq != null)
                {
                    processCode = numberSeq.Process;
                    
                }
            }
            else
            {
                throw new System.InvalidOperationException("ProposalType Is Null Or Empty! Can not start process");
            }
            return processCode;
        }
    }
}