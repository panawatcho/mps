﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CrossingSoft.Framework.Services.OData;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.Infrastructure.Repositories;
using TheMall.MPS.Models;

namespace TheMall.MPS.API.Services
{
    public interface ITotalSaleLastYearService : IODataServiceAsync<MPS_M_TotalSaleLastYear>
    {
    }

    public class TotalSaleLastYearService : BaseODataService<MPS_M_TotalSaleLastYear>, ITotalSaleLastYearService
    {
        private readonly IMpsODataRepositoryAsync<MPS_M_TotalSaleLastYear> _repository;

        public TotalSaleLastYearService(IMpsODataRepositoryAsync<MPS_M_TotalSaleLastYear> repository)
            : base(repository)
        {
            _repository = repository;
        }
    }
}