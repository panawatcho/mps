﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CrossingSoft.Framework.Infrastructure.OData.Repositories;
using CrossingSoft.Framework.Services.OData;
using TheMall.MPS.Models.Mail;

namespace TheMall.MPS.API.Services
{
    public interface IMailTemplateService : IODataServiceAsync<MailTemplate>
    {
        MailTemplate GetMailTemplate(string mailTemplateId);
        Task<MailTemplate> GetMailTemplateAsync(string mailTemplateId);
    }
    public class MailTemplateService : ODataServiceAsync<MailTemplate>, IMailTemplateService
    {
        private readonly IODataRepositoryAsync<MailTemplate> _repository;

        public MailTemplateService(IODataRepositoryAsync<MailTemplate> repository) : base(repository)
        {
            _repository = repository;
        }

        public MailTemplate GetMailTemplate(string mailTemplateId)
        {
            if (string.IsNullOrEmpty(mailTemplateId))
            {
                throw new ArgumentNullException("mailTemplateId");
            }
            var mailTemplate =
                new List<MailTemplate>() {this.Find(m => m.MailTemplateId == mailTemplateId)}.FirstOrDefault();
            return mailTemplate;
        }

        public async Task<MailTemplate> GetMailTemplateAsync(string mailTemplateId)
        {
            if (string.IsNullOrEmpty(mailTemplateId))
            {
                throw new ArgumentNullException("mailTemplateId");
            }
            var mailTemplate = this.FindAsync(m => m.MailTemplateId == mailTemplateId);
            return await mailTemplate;
        }
    }
}