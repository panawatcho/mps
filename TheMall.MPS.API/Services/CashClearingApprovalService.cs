﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CrossingSoft.Framework.Infrastructure.OData.Repositories;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.Models.CashClearing;

namespace TheMall.MPS.API.Services
{
    public interface ICashClearingApprovalService : Interfaces.IODataService<MPS_CashClearingApproval>
    {
    }
    public class CashClearingApproveService : BaseODataService<MPS_CashClearingApproval>, ICashClearingApprovalService
    {
        public CashClearingApproveService(IODataRepositoryAsync<MPS_CashClearingApproval> repository)
            : base(repository)
        {
        }
    }
}