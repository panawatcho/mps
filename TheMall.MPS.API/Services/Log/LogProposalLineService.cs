﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using CrossingSoft.Framework.Infrastructure.OData.Repositories;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Infrastructure.EntityFramework;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Models.ProposalLog;

namespace TheMall.MPS.API.Services
{
    public interface ILogProposalLineService : Interfaces.IODataService<MPS_Log_ProposalLine>
    {
    }
    public class LogProposalLineService : BaseODataService<MPS_Log_ProposalLine>, ILogProposalLineService
    {
        public LogProposalLineService(IODataRepositoryAsync<IMPSDbContext, IMPSSession, MPS_Log_ProposalLine> repository)
            : base(repository)
        {
        }
    }
}