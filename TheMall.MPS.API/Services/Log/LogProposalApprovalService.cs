﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using CrossingSoft.Framework.Infrastructure.OData.Repositories;
using CrossingSoft.Framework.Services.OData;
using SourceCode.Workflow.Client;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Infrastructure.EntityFramework;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Models.Proposal;
using TheMall.MPS.Models.ProposalLog;

namespace TheMall.MPS.API.Services
{
    public interface ILogProposalApprovalService : Interfaces.IODataService<MPS_Log_ProposalApproval>
    {
    }
    public class LogProposalApprovalService : BaseODataService<MPS_Log_ProposalApproval>, ILogProposalApprovalService
    {
        public LogProposalApprovalService(IODataRepositoryAsync<MPS_Log_ProposalApproval> repository)
            : base(repository)
        {
        }
    }
}