﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using CrossingSoft.Framework.Infrastructure.OData.Repositories;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Infrastructure.EntityFramework;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Models.ProposalLog;

namespace TheMall.MPS.API.Services
{
    public interface ILogProposalService : IMpsWorkflowTableService<MPS_Log_ProposalTable>
    {
    }
    public class LogProposalService : MpsWorkflowTableService<MPS_Log_ProposalTable>, ILogProposalService
    {
        public LogProposalService(IODataRepositoryAsync<IMPSDbContext, IMPSSession, MPS_Log_ProposalTable> repository)
            : base(repository)
        {
        }

        public override MPS_Log_ProposalTable GetWorkflowTable(int id)
        {
            return Get(id);
        }

        public override Task<MPS_Log_ProposalTable> GetWorkflowTableAsync(int id)
        {
            return GetAsync(id);
        }
    }
}