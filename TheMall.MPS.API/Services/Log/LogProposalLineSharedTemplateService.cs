﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using CrossingSoft.Framework.Infrastructure.OData.Repositories;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.Models.Proposal;
using TheMall.MPS.Models.ProposalLog;

namespace TheMall.MPS.API.Services
{
    public interface ILogProposalLineSharedTemplateService : Interfaces.IODataService<MPS_Log_ProposalLineSharedTemplate>
    {
    }
    public class LogProposalLineSharedTemplateService : BaseODataService<MPS_Log_ProposalLineSharedTemplate>, ILogProposalLineSharedTemplateService
    {
        public LogProposalLineSharedTemplateService(IODataRepositoryAsync<MPS_Log_ProposalLineSharedTemplate> repository) 
            : base(repository)
        {
        }
    }
}