﻿using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Infrastructure.Repositories;
using TheMall.MPS.Models.ProposalLog;

namespace TheMall.MPS.API.Services
{
    public interface ILogProposalTrackChangesService : IODataService<MPS_Log_ProposalTrackChanges>
    {
    }
    public class LogProposalTrackChangesService : BaseODataService<MPS_Log_ProposalTrackChanges>, ILogProposalTrackChangesService
    {
        public LogProposalTrackChangesService(IMpsODataRepositoryAsync<MPS_Log_ProposalTrackChanges> repository) 
            : base(repository)
        {
        }
    }
}