﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using CrossingSoft.Framework.Infrastructure.OData.Repositories;
using CrossingSoft.Framework.Services.OData;
using SourceCode.Workflow.Client;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Infrastructure.EntityFramework;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Models.Proposal;
using TheMall.MPS.Models.ProposalLog;

namespace TheMall.MPS.API.Services
{
    public interface ILogProposalIncomeTotalSaleService : Interfaces.IODataService<MPS_Log_IncomeTotalSale>
    {
    }
    public class LogProposalIncomeTotalSaleService : BaseODataService<MPS_Log_IncomeTotalSale>, ILogProposalIncomeTotalSaleService
    {
        public LogProposalIncomeTotalSaleService(IODataRepositoryAsync<MPS_Log_IncomeTotalSale> repository)
            : base(repository)
        {
        }
    }
}