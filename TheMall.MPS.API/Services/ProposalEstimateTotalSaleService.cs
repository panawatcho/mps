﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using CrossingSoft.Framework.Infrastructure.OData.Repositories;
using CrossingSoft.Framework.Services.OData;
using SourceCode.Workflow.Client;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Infrastructure.EntityFramework;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Models.Proposal;

namespace TheMall.MPS.API.Services
{
    public interface IProposalEstimateTotalSaleService : Interfaces.IODataService<MPS_EstimateTotalSale>
    {
    }
    public class ProposalEstimateTotalSaleService : BaseODataService<MPS_EstimateTotalSale>, IProposalEstimateTotalSaleService
    {
        public ProposalEstimateTotalSaleService(IODataRepositoryAsync<MPS_EstimateTotalSale> repository) 
            : base(repository)
        {
        }
    }
}