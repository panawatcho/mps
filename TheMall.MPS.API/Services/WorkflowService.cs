﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using CrossingSoft.Framework.Infrastructure.K2;

using SourceCode.Workflow.Client;
using TheMall.MPS.API.Exceptions;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Models.Abstracts;
using TheMall.MPS.Models.Interfaces;
using TheMall.MPS.Models.Workflow.Interfaces;

namespace TheMall.MPS.API.Services
{
    public interface IWorkflowService
    {
        WorklistItem GetWorklistItemSafe(string sn);
    }

    public class WorkflowService : IWorkflowService
    {
        private readonly IWorkflowRepository _workflowRepository;

        public WorkflowService(IWorkflowRepository workflowRepository)
        {
            _workflowRepository = workflowRepository;
        }

        public WorklistItem GetWorklistItemSafe(string sn)
        {
            return _workflowRepository.GetWorkListItemSafe(sn);
        }

    }

    public interface IWorkflowService<TWorkflowTable> :IWorkflowService,  IService
        where TWorkflowTable : MpsWorkflowTable
    {
        Task<Dictionary<string, object>> InitDataFields(TWorkflowTable workflowTable);
        string InitFolio(TWorkflowTable workflowTable, Dictionary<string, object> dataFields = null);
        //Task<bool> ValidateStartProcess(TWorkflowTable workflowTable);
        IEnumerable<ValidationResult> ValidateStartProcess(TWorkflowTable workflowTable);
        int StartProcess(TWorkflowTable workflowTable, string folio, Dictionary<string, object> dataFields = null);

        WorklistItem GetWorklistItem(string sn);
        int GetWorkflowTablePrimaryKey(string sn);
        T GetDataField<T>(string sn, string dataFieldName, WorklistItem worklistItem = null);
        T GetDataFieldSafe<T>(string sn, string dataFieldName, WorklistItem worklistItem = null);
        bool Submit(string sn, string action);
        string[] GetActions(WorklistItem worklistItem);
    }
 
    public abstract class WorkflowService<TWorkflowTable> : BaseWorkflowService, IWorkflowService<TWorkflowTable>
        where TWorkflowTable : MpsWorkflowTable
    
    {
      
        private readonly IMpsWorkflowProcessModel<TWorkflowTable> _definition;
        private readonly IWorkflowRepository _workflowRepository;

        protected WorkflowService(
            IMpsWorkflowProcessModel<TWorkflowTable> definition,
            IWorkflowRepository workflowRepository,
            IWorkingHourService workingHourService
            )
            : base(workingHourService)
        {
            _definition = definition;
            _workflowRepository = workflowRepository;
        }

        protected int StartK2Process(string processName, string folio, Dictionary<string, object> dataFields = null)
        {
            return _workflowRepository.StartK2Process(processName, folio, dataFields);
        }

        public virtual Task<Dictionary<string, object>> InitDataFields(TWorkflowTable workflowTable)
        {
            var dataFields = new Dictionary<string, object>();

            dataFields.Add(_definition.PrimaryKeyDataFieldName, workflowTable.Id);
            dataFields.Add(_definition.TitleDataFieldName, workflowTable.Title);
            dataFields.Add(_definition.ProcessCodeDataFieldName, _definition.ProcessCode);

            return Task.FromResult(dataFields);
        }

        public abstract string InitFolio(TWorkflowTable workflowTable, Dictionary<string, object> dataFields = null);
        //public abstract Task<bool> ValidateStartProcess(TWorkflowTable workflowTable);
        public virtual IEnumerable<ValidationResult> ValidateStartProcess(TWorkflowTable workflowTable)
        {
            return Enumerable.Empty<ValidationResult>();
        }

        public virtual int StartProcess(TWorkflowTable workflowTable, string folio, Dictionary<string, object> dataFields = null)
        {
            var processName = _definition.ProcessFullPathNew(workflowTable);
            if (string.IsNullOrEmpty(processName))
            {
                throw new Exception("Process name was not provided to be started.");
            }
            return StartK2Process(processName, folio, dataFields);
        }

        public WorklistItem GetWorklistItem(string sn)
        {
            return _workflowRepository.GetWorkListItem(sn, false);
        }

        public WorklistItem GetWorklistItemSafe(string sn)
        {
            return _workflowRepository.GetWorkListItemSafe(sn);
        }

        public int GetWorkflowTablePrimaryKey(string sn)
        {
            var worklistItem = _workflowRepository.GetWorkListItemSafe(sn);
            if (worklistItem == null)
                throw new WorklistItemNotFoundException(sn, string.Format(Resources.Error.WorklistItemNotFound + " ({0})", sn));

            var pkdf = worklistItem.ProcessInstance.DataFields[_definition.PrimaryKeyDataFieldName];

            try
            {
                return (int)pkdf.Value;
            }
            catch (InvalidCastException)
            {
                return Int32.Parse(pkdf.Value.ToString());
            }
        }

        public T GetDataField<T>(string sn, string dataFieldName, WorklistItem worklistItem = null)
        {
            if (worklistItem == null)
            {
                worklistItem = _workflowRepository.GetWorkListItemSafe(sn);
            }
           
            var df = worklistItem.ProcessInstance.DataFields[dataFieldName];

            return (T)df.Value;
        }

        public T GetDataFieldSafe<T>(string sn, string dataFieldName, WorklistItem worklistItem = null)
        {
            if (worklistItem == null)
            {
                worklistItem = _workflowRepository.GetWorkListItemSafe(sn);
            }
            if (worklistItem != null)
            {
                foreach (SourceCode.Workflow.Client.DataField dataField in worklistItem.ProcessInstance.DataFields)
                {
                    if (dataField.Name == dataFieldName)
                    {
                        return (T)dataField.Value;
                    }
                }
            }
            return default(T);
        }

        public virtual bool Submit(string sn, string action)
        {
            _workflowRepository.ActionWorklistItem(sn, action, false);
            return true;
        }

        public string[] GetActions(WorklistItem worklistItem)
        {
            return _workflowRepository.GetWorklistActions(worklistItem.Actions).ToArray();
        }


    }
}