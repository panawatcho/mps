﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using TheMall.MPS.Infrastructure;
using TheMall.MPS.Infrastructure.EntityFramework;
using TheMall.MPS.Models.K2;

namespace TheMall.MPS.API.Services
{
    public interface IK2DbService
    {
        Task<IReadOnlyList<ProcInstStatus>> SearchAsync();
        Task<IReadOnlyList<ProcInstStatus>> SearchFromFolioAsync(IEnumerable<string> folios);
        Task<IReadOnlyList<ProcInstStatus>> SearchFromProcInstAsync(IEnumerable<int> procInstIds);
        Task<IReadOnlyList<ProcInstStatus>> SearchFromFolioProcInstAsync(IEnumerable<int> procInstIds, IEnumerable<string> folios);

        Models.K2.Act GetAct(int actId);
        Models.K2.Act GetActByActInstId(int procInstId, int actInstId);
        Models.K2.ActInst GetActInst(int procInstId, int actInstId);
        Models.K2.Act GetActByActInstDestId(int procInstId, int actInstDestId);
        Models.K2.ActInstDest GetActInstDest(int procInstId, int actInstDestId);
        Models.K2.WorklistHeader GetWorkListItem(string sn);
        Models.K2.ProcInst GetProcInst(int procInstId);
        Models.K2.ProcInstCustom GetProcInstCustom(int procInstId);
    }
    public class K2DbService : IK2DbService
    {
        private readonly IK2DbContext _context;
        private readonly IWorkingHourService _workingHourService;

        public K2DbService(IK2DbContext context, IWorkingHourService workingHourService)
        {
            _context = context;
            _workingHourService = workingHourService;
        }

        private static readonly Func<IK2DbContext, IQueryable<ProcInstStatusQueryResult>> ProcInstStatusQuery = context => 
            from procInsts in context.ProcInst1
            join procs in context.Proc1
                on procInsts.ProcID equals procs.ID
            join procSets in context.ProcSets
                on procs.ProcSetID equals procSets.ID
            join actInsts in context.ActInsts
                on procInsts.ID equals actInsts.ProcInstID
            join actInstDests in context.ActInstDests
                on new { actInsts.ProcInstID, actInsts.ID } equals
                new { actInstDests.ProcInstID, ID = actInstDests.ActInstID }
            join eventInsts in context.EventInsts
                on new { actInstDests.ProcInstID, actInstDests.ID } equals
                new { eventInsts.ProcInstID, ID = eventInsts.ActInstDestID }
            join act in context.Act1
                on new { ActId = actInsts.ActID, procId = procs.ID } equals new { ActId = act.ID, procId = act.ProcID }
            join evt in context.Event1
                on new { EvtId = eventInsts.EventID, procId = procs.ID, ActId = act.ID } equals
                new { EvtId = evt.ID, procId = evt.ProcID, ActId = evt.ActID }
            where
                //todo: what ???
                (procInsts.Status == 1 || procInsts.Status == 2)
                && (actInsts.Status == 2)
                && (evt.Type == 2)
                && procSets.Folder == Constants.PROCESS_FOLDER
                                                                                                                    
            select new ProcInstStatusQueryResult
            {
                ProcInstId = procInsts.ID,
                ProcDescription = procSets.Descr,
                Folio = procInsts.Folio,
                ActInstId = actInsts.ID,
                ActivityName = act.Name,
                ActivityDescription = act.Descr,
                ActInstDestId = actInstDests.ID,
                ActInstDestFqn = actInstDests.User,
                EventInstId = eventInsts.ID,
                EventName = evt.Name,
                EventStartDate = eventInsts.StartDate,
                EventExpectedDuration = eventInsts.ExpectedDuration
            };

        public async Task<IReadOnlyList<ProcInstStatus>> SearchAsync()
        {
            return await SearchProcessAsync(m => true);
        }
        public async Task<IReadOnlyList<ProcInstStatus>> SearchFromFolioAsync(IEnumerable<string> folios)
        {
            return await SearchProcessAsync(m => !string.IsNullOrEmpty(m.Folio) && folios.Any(n => m.Folio.Contains(n)));
        }
        public async Task<IReadOnlyList<ProcInstStatus>> SearchFromProcInstAsync(IEnumerable<int> procInstIds)
        {
            return await SearchProcessAsync(m => !string.IsNullOrEmpty(m.Folio) && procInstIds.Any(n => m.ProcInstId == n));
        }
        public async Task<IReadOnlyList<ProcInstStatus>> SearchFromFolioProcInstAsync(IEnumerable<int> procInstIds, IEnumerable<string> folios)
        {
            return await SearchProcessAsync(m => !string.IsNullOrEmpty(m.Folio) && folios.Any(n => m.Folio.Contains(n))
                && procInstIds.Any(n => m.ProcInstId == n));
        }

        public Act GetAct(int actId)
        {
            return _context.Acts.FirstOrDefault(m => m.ID == actId);
        }

        public Act GetActByActInstId(int procInstId, int actInstId)
        {
            var actInst = GetActInst(procInstId, actInstId);
            if (actInst == null)
            {
                return null;
            }
            return GetAct(actInst.ActID);
        }

        public Act GetActByActInstDestId(int procInstId, int actInstDestId)
        {
            var actInstDest = GetActInstDest(procInstId, actInstDestId);
            if (actInstDest == null)
            {
                return null;
            }
            return GetActByActInstId(procInstId, actInstDest.ActInstID);
        }

        public ActInstDest GetActInstDest(int procInstId, int actInstDestId)
        {
            return _context.ActInstDests.FirstOrDefault(
                                m => m.ProcInstID == procInstId && m.ID == actInstDestId);
        }

        public ActInst GetActInst(int procInstId, int actInstId)
        {
            return _context.ActInsts.FirstOrDefault(m => m.ProcInstID == procInstId && m.ID == actInstId);
        }

        public WorklistHeader GetWorkListItem(string sn)
        {
            if (string.IsNullOrEmpty(sn))
            {
                return null;
            }
            var s = sn.Split('_');
            return GetWorklistItem(Int32.Parse(s[0]), Int32.Parse(s[1]));
        }

        private WorklistHeader GetWorklistItem(int procInstId, int actInstDestId)
        {
            return
                _context.WorklistHeaders.FirstOrDefault(
                    m => m.ProcInstID == procInstId && m.ActInstDestID == actInstDestId);
        }

        public ProcInst GetProcInst(int procInstId)
        {
            return _context.ProcInsts.FirstOrDefault(m => m.ID == procInstId);
        }

        public ProcInstCustom GetProcInstCustom(int procInstId)
        {
            var result = new ProcInstCustom();
            var procInst =  _context.ProcInsts.FirstOrDefault(m => m.ID == procInstId);
            var procInstLog = _context.ProcInsts.FirstOrDefault(m => m.ID == procInstId);

            if (procInst == null || procInstLog == null)
            {
                return null;
            }
            var proc = _context.Procs.FirstOrDefault(m => m.ID == procInst.ProcID);
            if (proc == null)
            {
                return null;
            }
            var procSet = _context.ProcSets.FirstOrDefault(m => m.ID == proc.ProcSetID);
            if (procSet == null)
            {
                return null;
            }

            result.ID = procInst.ID;
            result.Name = procSet.Name;
            result.Description = procSet.Descr;
            result.Guid = proc.Guid;
            result.ExpectedDuration = proc.ExpectedDuration;
            result.Folio = procInst.Folio;
            result.Priority = proc.Priority;
            result.StartDate = procInst.StartDate;

            

            return result;
        }

        private async Task<IReadOnlyList<ProcInstStatus>> SearchProcessAsync(Expression<Func<ProcInstStatusQueryResult, bool>> expression)
        {
            var query =
                ProcInstStatusQuery.Invoke(_context)
                    .Where(expression);
            var query2 = await query.GroupBy(m => new { m.ProcInstId, m.ProcDescription, m.Folio })
                .Select(m => new ProcInstStatus
                {
                    ProcInstId = m.Key.ProcInstId,
                    ProcDescription = m.Key.ProcDescription,
                    Folio = m.Key.Folio,
                    ActInsts = m.Select(n => new ActInstStatus
                    {
                        ActInstId = n.ActInstId,
                        ActivityName = n.ActivityName,
                        ActivityDescription = n.ActivityDescription,
                        ActInstDestId = n.ActInstDestId,
                        ActInstDestFqn = n.ActInstDestFqn,
                        EventName = n.EventName,
                        EventStartDate = n.EventStartDate,
                        EventExpectedDuration = n.EventExpectedDuration,
                    })
                }).ToListAsync();
            //foreach (var procInst in query2)
            //{
            //    var acts = new List<ActInstStatus>(procInst.ActInsts);
            //    acts.Add(procInst.ActInsts.First());
            //    acts.Add(procInst.ActInsts.First());
            //    procInst.ActInsts = acts;
            //}
            foreach (var procInst in query2)
            {
                foreach (var actInst in procInst.ActInsts)
                {
                    actInst.EventDueDate = _workingHourService.CalcDue(actInst.EventStartDate,
                        actInst.EventExpectedDuration);
                }
            }
            return query2;
        }

        internal sealed class ProcInstStatusQueryResult
        {
            public int ProcInstId { get; set; }
            public string ProcDescription { get; set; }
            public string Folio { get; set; }
            public int ActInstId { get; set; }
            public string ActivityName { get; set; }
            public string ActivityDescription { get; set; }
            public int ActInstDestId { get; set; }
            public string ActInstDestFqn { get; set; }
            public string EventName { get; set; }
            public int EventInstId { get; set; }
            public DateTime EventStartDate { get; set; }
            public int EventExpectedDuration { get; set; }
        }
    }
}