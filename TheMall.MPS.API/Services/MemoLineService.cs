﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using CrossingSoft.Framework.Infrastructure.OData.Repositories;
using CrossingSoft.Framework.Services.OData;
using SourceCode.Workflow.Client;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Infrastructure.EntityFramework;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Models.Memo;
using TheMall.MPS.Models.PettyCash;

namespace TheMall.MPS.API.Services
{
    public interface IMemoLineService : Interfaces.IODataService<MPS_MemoLine>
    {
    }
    public class MemoLineService : BaseODataService<MPS_MemoLine>, IMemoLineService
    {
        public MemoLineService(IODataRepositoryAsync<MPS_MemoLine> repository) 
            : base(repository)
        {
        }
    }
}