﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using CrossingSoft.Framework.Infrastructure.OData.Repositories;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.Models.Proposal;

namespace TheMall.MPS.API.Services
{
    public interface IProposalLineSharedTemplateService : Interfaces.IODataService<MPS_ProposalLineSharedTemplate>
    {
    }
    public class ProposalLineSharedTemplateService : BaseODataService<MPS_ProposalLineSharedTemplate>, IProposalLineSharedTemplateService
    {
        public ProposalLineSharedTemplateService(IODataRepositoryAsync<MPS_ProposalLineSharedTemplate> repository) 
            : base(repository)
        {
        }
    }
}