﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using CrossingSoft.Framework.Infrastructure.OData.Repositories;
using CrossingSoft.Framework.Models.Enums;
using LinqKit;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Infrastructure.EntityFramework;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Models.CashAdvance;
using TheMall.MPS.Models.PettyCash;

namespace TheMall.MPS.API.Services
{
    public interface ICashAdvanceService : IMpsWorkflowTableService<MPS_CashAdvanceTable>
    {
        Task<IEnumerable<MPS_CashAdvanceTable>> SearchDocument(string code = null, string name = null);
        IEnumerable<MPS_CashAdvanceTable> SearchDoc(DateTime? createDate,
         bool checkStartDate,
         string process = null, string title = null, string documentNo = null,
         string username = null, string status = null);
    }
    public class CashAdvanceService : MpsWorkflowTableService<MPS_CashAdvanceTable>, ICashAdvanceService
    {
        public CashAdvanceService(IODataRepositoryAsync<IMPSDbContext, IMPSSession, MPS_CashAdvanceTable> repository)
            : base(repository)
        {
        }

        public async Task<IEnumerable<MPS_CashAdvanceTable>> SearchDocument(string code = null, string name = null)
        {
            return null;
        }

        public override MPS_CashAdvanceTable GetWorkflowTable(int id)
        {
            return Get(id);
        }

        public override Task<MPS_CashAdvanceTable> GetWorkflowTableAsync(int id)
        {
            return GetAsync(id);
        }


        public IEnumerable<MPS_CashAdvanceTable> SearchDoc(DateTime? createDate,
           bool checkStartDate,
           string process = null,
           string title = null,
           string documentNo = null,
           string username = null,
           string status = null)
        {
            var query = LinqKit.PredicateBuilder.True<MPS_CashAdvanceTable>();
            if (!string.IsNullOrEmpty(status))
            {
                query = query.And(m => m.DocumentStatus.Equals(status));
            }

            if (!string.IsNullOrEmpty(title))
            {
                query = query.And(m => !string.IsNullOrEmpty(m.Title) && m.Title.Contains(title));
            }
            if (!string.IsNullOrEmpty(documentNo))
            {
                query = query.And(m => !string.IsNullOrEmpty(m.DocumentNumber) && m.DocumentNumber.Contains(documentNo));
            }
            if (!string.IsNullOrEmpty(username))
            {
                query = query.And(m => !string.IsNullOrEmpty(m.CreatedBy) && m.CreatedBy.Contains(username));
            }
            var result = GetWhere(query);
            if (createDate != null)
            {
                DateTime dateTime = (DateTime) createDate;
                result = result.Where(m => m.CreatedDate != null && m.CreatedDate.Value.Date == dateTime.Date);
                //query = query.And(m => m.CreatedDate.Equals(startDate));
            }
            return result;
           // return GetWhere(query);
        }
    }
}