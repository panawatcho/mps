﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CrossingSoft.Framework.Infrastructure.OData.Repositories;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Models.FileManager;

namespace TheMall.MPS.API.Services
{
    public interface IFileManagerDirectoryService : IService<FileManagerDirectory>
    {
        FileManagerDirectory Get(int id);
        IEnumerable<FileManagerDirectory> GetAll(string username);
    }

    public class FileManagerDirectoryService : Abstracts.BaseService<FileManagerDirectory>, IFileManagerDirectoryService
    {
        public FileManagerDirectoryService(IODataRepositoryAsync<FileManagerDirectory> repository) : base(repository)
        {
        }

        public FileManagerDirectory Get(int id)
        {
            return Find(m => m.Id == id);
        }

        public IEnumerable<FileManagerDirectory> GetAll(string username)
        {
            return GetWhere(m => m.ParentId == null);
        }
    }
}