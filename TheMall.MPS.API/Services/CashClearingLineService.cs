﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CrossingSoft.Framework.Infrastructure.OData.Repositories;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.Models.CashClearing;

namespace TheMall.MPS.API.Services
{
    public interface ICashClearingLineService : Interfaces.IODataService<MPS_CashClearingLine>
    {
    }
    public class CashClearingLineService: BaseODataService<MPS_CashClearingLine>, ICashClearingLineService
    {
        public CashClearingLineService(IODataRepositoryAsync<MPS_CashClearingLine> repository) 
            : base(repository)
        {
        }
    }

     
}