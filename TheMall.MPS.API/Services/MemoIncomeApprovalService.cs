﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using CrossingSoft.Framework.Infrastructure.OData.Repositories;
using CrossingSoft.Framework.Services.OData;
using SourceCode.Workflow.Client;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Infrastructure.EntityFramework;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Models.MemoIncome;

namespace TheMall.MPS.API.Services
{
    public interface IMemoIncomeApprovalService : Interfaces.IODataService<MPS_MemoIncomeApproval>
    {
        void ResetStatusApproveMemoIncome(int workflowTableId);
    }
    public class MemoIncomeApprovalService : BaseODataService<MPS_MemoIncomeApproval>, IMemoIncomeApprovalService
    {
        private readonly IMPSSession _session;

        public MemoIncomeApprovalService(IODataRepositoryAsync<MPS_MemoIncomeApproval> repository, 
            IMPSSession session)
            : base(repository)
        {
            _session = session;
        }

        public void ResetStatusApproveMemoIncome(int workflowTableId)
        {
            
            var listApporve = GetWhere(m => m.ParentId == workflowTableId);
            if (listApporve != null)
            {
                foreach (var model in listApporve)
                {
                    model.ApproveStatus = false;
                    Update(model, model.Id);
                }
                _session.SaveChanges();
            }
        }
    }
}