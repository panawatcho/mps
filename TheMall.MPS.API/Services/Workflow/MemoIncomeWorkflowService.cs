﻿using System;
using System.Collections.Generic;
using CrossingSoft.Framework.Infrastructure.K2;
using TheMall.MPS.Models.Memo;
using TheMall.MPS.Models.MemoIncome;
using TheMall.MPS.Models.Workflow;


namespace TheMall.MPS.API.Services.Workflow
{
    public class MemoIncomeWorkflowService : WorkflowService<MPS_MemoIncomeTable>
    {
        public MemoIncomeWorkflowService(IMemoIncomeProcess definition, IWorkflowRepository workflowRepository, IWorkingHourService workingHourService)
            : base(definition, workflowRepository, workingHourService)
        {
        }

        public override string InitFolio(MPS_MemoIncomeTable workflowTable, Dictionary<string, object> dataFields = null)
        {
            return "MemoIncome No: " + workflowTable.DocumentNumber; //k2 Folio
        }
    }
}