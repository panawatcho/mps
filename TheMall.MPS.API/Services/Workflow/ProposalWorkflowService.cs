﻿using System;
using System.Collections.Generic;
using CrossingSoft.Framework.Infrastructure.K2;
using TheMall.MPS.Models.Proposal;
using TheMall.MPS.Models.Workflow;


namespace TheMall.MPS.API.Services.Workflow
{
    public class ProposalWorkflowService : WorkflowService<MPS_ProposalTable>
    {
        public ProposalWorkflowService(IProposalProcess definition, IWorkflowRepository workflowRepository, IWorkingHourService workingHourService)
            : base(definition, workflowRepository, workingHourService)
        {
        }

        public override string InitFolio(MPS_ProposalTable workflowTable, Dictionary<string, object> dataFields = null)
        {
            if (workflowTable.StatusFlag == 8)
            {
                return "Proposal No: " + workflowTable.DocumentNumber + " (" + workflowTable.Revision + ")";
            }
            else
            {
                return "Proposal No: " + workflowTable.DocumentNumber;
                
            }
           
        }
    }
}