﻿using System;
using System.Collections.Generic;
using CrossingSoft.Framework.Infrastructure.K2;
using TheMall.MPS.Models.PettyCash;
using TheMall.MPS.Models.Workflow;


namespace TheMall.MPS.API.Services.Workflow
{
    public class PettyCashWorkflowService : WorkflowService<MPS_PettyCashTable>
    {
        public PettyCashWorkflowService(IPettyCashProcess definition, IWorkflowRepository workflowRepository, IWorkingHourService workingHourService)
            : base(definition, workflowRepository, workingHourService)
        {
        }

        public override string InitFolio(MPS_PettyCashTable workflowTable, Dictionary<string, object> dataFields = null)
        {
            return "PettyCash No: " + workflowTable.DocumentNumber;
        }
    }
}