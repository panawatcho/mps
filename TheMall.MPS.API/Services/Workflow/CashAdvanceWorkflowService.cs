﻿using System;
using System.Collections.Generic;
using CrossingSoft.Framework.Infrastructure.K2;
using TheMall.MPS.Models.CashAdvance;
using TheMall.MPS.Models.Workflow;


namespace TheMall.MPS.API.Services.Workflow
{
    public class CashAdvanceWorkflowService : WorkflowService<MPS_CashAdvanceTable>
    {
        public CashAdvanceWorkflowService(ICashAdvanceProcess definition, IWorkflowRepository workflowRepository, IWorkingHourService workingHourService)
            : base(definition, workflowRepository, workingHourService)
        {
        }

        public override string InitFolio(MPS_CashAdvanceTable workflowTable, Dictionary<string, object> dataFields = null)
        {
            return "CashAdvance No: " + workflowTable.DocumentNumber; 
        }
    }
}