﻿using System;
using System.Collections.Generic;
using CrossingSoft.Framework.Infrastructure.K2;
using TheMall.MPS.Models.CashClearing;
using TheMall.MPS.Models.Workflow;


namespace TheMall.MPS.API.Services.Workflow
{
    public class CashClearingWorkflowService : WorkflowService<MPS_CashClearingTable>
    {
        public CashClearingWorkflowService(ICashClearingProcess definition, IWorkflowRepository workflowRepository, IWorkingHourService workingHourService)
            : base(definition, workflowRepository, workingHourService)
        {
        }

        public override string InitFolio(MPS_CashClearingTable workflowTable, Dictionary<string, object> dataFields = null)
        {
            return "CashClearing No: " + workflowTable.DocumentNumber;
        }
    }
}