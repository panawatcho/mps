﻿using System;
using System.Collections.Generic;
using CrossingSoft.Framework.Infrastructure.K2;
using TheMall.MPS.Models.Memo;
using TheMall.MPS.Models.Workflow;


namespace TheMall.MPS.API.Services.Workflow
{
    public class MemoWorkflowService : WorkflowService<MPS_MemoTable>
    {
        public MemoWorkflowService(IMemoProcess definition, IWorkflowRepository workflowRepository, IWorkingHourService workingHourService)
            : base(definition, workflowRepository, workingHourService)
        {
        }

        public override string InitFolio(MPS_MemoTable workflowTable, Dictionary<string, object> dataFields = null)
        {
            return "Memo No: " + workflowTable.DocumentNumber;
        }
    }
}