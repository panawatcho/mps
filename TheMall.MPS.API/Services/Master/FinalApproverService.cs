﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using CrossingSoft.Framework.Infrastructure.OData.Repositories;
using LinqKit;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Infrastructure.EntityFramework;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Models.Master;

namespace TheMall.MPS.API.Services
{
    public interface IFinalApproverService : IBaseMasterService<MPS_M_FinalApprover>
    {
        MPS_M_FinalApprover GetFinalApprover(decimal budget, string process);
        Task<IEnumerable<MPS_M_FinalApprover>> Search(string username = null, string process = null);
    }
    public class FinalApproverService : BaseMasterService<MPS_M_FinalApprover>, IFinalApproverService
    {
        public FinalApproverService(IODataRepositoryAsync<IMPSDbContext, IMPSSession, MPS_M_FinalApprover> repository)
            : base(repository)
        {
        }


        public override MPS_M_FinalApprover DoGet(string code)
        {
            return DoGet(code);
        }

        public override Task<MPS_M_FinalApprover> DoGetAsync(string code)
        {
            return DoGetAsync(code);
        }

        public async Task<IEnumerable<MPS_M_FinalApprover>> Search(string username = null, string process = null)
        {
            var query = LinqKit.PredicateBuilder.True<MPS_M_FinalApprover>();
            query = query.And(m => !m.InActive);
            if (!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(process))
            {
                return
                    GetWhere(query.And(m =>
                        !string.IsNullOrEmpty(m.Username) && m.Username.Contains(username) &&
                        !string.IsNullOrEmpty(m.Process) && m.Process.Contains(process)));
            }
            else if (!string.IsNullOrEmpty(username))
            {
                return GetWhere(query.And(m => !string.IsNullOrEmpty(m.Username) && m.Username.Contains(username)));
            }
            else if (!string.IsNullOrEmpty(process))
            {
                return
                    GetWhere(query.And(m => !string.IsNullOrEmpty(m.Process) && m.Process.Contains(process)));
            }
            else
            {
                return GetWhere(query);
            }
        }

        public MPS_M_FinalApprover GetFinalApprover(decimal budget ,string process)
        { 
            if(string.IsNullOrEmpty(process))
                return new MPS_M_FinalApprover();

            var query = LinqKit.PredicateBuilder.True<MPS_M_FinalApprover>();
            query = query.And(m => !m.InActive);
            query = query.And(m => m.FromBudget <= budget && m.ToBudget >= budget);
            query = query.And(m => m.Process.Equals(process, StringComparison.CurrentCultureIgnoreCase));
            var model = LastOrDefault(query);
            if (model == null)
            {
                var newQuery = LinqKit.PredicateBuilder.True<MPS_M_FinalApprover>();
                newQuery = newQuery.And(m => !m.InActive && m.Process.Equals("Default", StringComparison.CurrentCultureIgnoreCase) );
                model = LastOrDefault(newQuery);
            }
          
            return model;
        }
    }
}