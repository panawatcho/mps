﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Web;
using CrossingSoft.Framework.Infrastructure.OData.Repositories;
using LinqKit;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Infrastructure.EntityFramework;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Models.Master;

namespace TheMall.MPS.API.Services
{
    public interface IPlaceService : IBaseMasterService<MPS_M_Place>
    {
        Task<IEnumerable<MPS_M_Place>> Search(string code = null, string name = null);
    }
    public class PlaceService : BaseMasterService<MPS_M_Place>, IPlaceService
    {
        public PlaceService(IODataRepositoryAsync<IMPSDbContext, IMPSSession, MPS_M_Place> repository)
            : base(repository)
        {
        }

        public override MPS_M_Place DoGet(string code)
        {
            return Get(code);
        }

        public override Task<MPS_M_Place> DoGetAsync(string code)
        {
            return GetAsync(code);
        }

       

        public async Task<IEnumerable<MPS_M_Place>> Search(string code = null, string name = null)
        {
            var query = LinqKit.PredicateBuilder.True<MPS_M_Place>();
            query = query.And(m => !m.InActive);
            if (!string.IsNullOrEmpty(code) && !string.IsNullOrEmpty(name))
            {
                return
                    GetWhere(query.And(m =>
                        !string.IsNullOrEmpty(m.PlaceCode) && m.PlaceCode.Contains(code) &&
                        !string.IsNullOrEmpty(m.PlaceName) && m.PlaceName.Contains(name))
                        );
            }
            else if (!string.IsNullOrEmpty(code))
            {
                return GetWhere(query.And(m => !string.IsNullOrEmpty(m.PlaceCode) && m.PlaceCode.Contains(code)));
            }
            else if (!string.IsNullOrEmpty(name))
            {
                return
                    GetWhere(query.And(m => !string.IsNullOrEmpty(m.PlaceName) && m.PlaceName.Contains(name)));
            }
            else
            {
                return GetWhere(query);
            }
        }
    }
}