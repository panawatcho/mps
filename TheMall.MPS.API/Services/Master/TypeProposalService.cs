﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using CrossingSoft.Framework.Infrastructure.OData.Repositories;
using LinqKit;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Infrastructure.EntityFramework;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Models.Master;

namespace TheMall.MPS.API.Services
{
    public interface ITypeProposalService : IBaseMasterService<MPS_M_TypeProposal>
    {
        Task<IEnumerable<MPS_M_TypeProposal>> Search(string code = null, string name = null);
    }
    public class TypeProposalService : BaseMasterService<MPS_M_TypeProposal>, ITypeProposalService
    {
        public TypeProposalService(IODataRepositoryAsync<IMPSDbContext, IMPSSession, MPS_M_TypeProposal> repository)
            : base(repository)
        {
        }

        public override MPS_M_TypeProposal DoGet(string code)
        {
            return Get(code);
        }

        public override Task<MPS_M_TypeProposal> DoGetAsync(string code)
        {
            return GetAsync(code);
        }

        public async Task<IEnumerable<MPS_M_TypeProposal>> Search(string code = null, string name = null)
        {
            var query = LinqKit.PredicateBuilder.True<MPS_M_TypeProposal>();
            query = query.And(m => !m.InActive);
            if (!string.IsNullOrEmpty(code) && !string.IsNullOrEmpty(name))
            {
                return
                    GetWhere(query.And(m =>
                        !string.IsNullOrEmpty(m.TypeProposalCode) && m.TypeProposalCode.Contains(code) &&
                        !string.IsNullOrEmpty(m.TypeProposalName) && m.TypeProposalName.Contains(name)));
            }
            else if (!string.IsNullOrEmpty(code))
            {
                return GetWhere(query.And(m => !string.IsNullOrEmpty(m.TypeProposalCode) && m.TypeProposalCode.Contains(code)));
            }
            else if (!string.IsNullOrEmpty(name))
            {
                return
                    GetWhere(query.And(m => !string.IsNullOrEmpty(m.TypeProposalName) && m.TypeProposalName.Contains(name)));
            }
            else
            {
                return GetWhere(query);
            }
        }
    }
}