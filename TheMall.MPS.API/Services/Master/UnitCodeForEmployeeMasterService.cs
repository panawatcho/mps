﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using CrossingSoft.Framework.Infrastructure.OData.Repositories;
using LinqKit;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Infrastructure.EntityFramework;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Models.Master;


namespace TheMall.MPS.API.Services
{
    //For OData
    public interface IUnitCodeForEmployeeMasterService : IBaseMasterService<MPS_M_UnitCodeForEmployee>
    {
        Task<IEnumerable<MPS_M_UnitCodeForEmployee>> Search(string empId = null, string unitCode = null);
       Task<IEnumerable<MPS_M_UnitCodeForEmployee>> GetInMaster(string empId = null, string unitCode = null);
    }
    public class UnitCodeForEmployeeMasterService : BaseMasterService<MPS_M_UnitCodeForEmployee>, IUnitCodeForEmployeeMasterService
    {
        public UnitCodeForEmployeeMasterService(IODataRepositoryAsync<IMPSDbContext, IMPSSession, MPS_M_UnitCodeForEmployee> repository)
            : base(repository)
        {
        }


        public override MPS_M_UnitCodeForEmployee DoGet(string code)
        {
            return DoGet(code);
        }

        public override Task<MPS_M_UnitCodeForEmployee> DoGetAsync(string code)
        {
            return DoGetAsync(code);
        }

        //public async Task<IEnumerable<MPS_M_UnitCodeForEmployee>> Search(string username = null, string unitCode = null)
        //{
        //    var query = LinqKit.PredicateBuilder.True<MPS_M_UnitCodeForEmployee>();
        //    query = query.And(m => !m.InActive);
        //    if (!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(unitCode))
        //    {
        //        return
        //            GetWhere(query.And(m =>
        //                !string.IsNullOrEmpty(m.Username) && m.Username.Contains(username) &&
        //                !string.IsNullOrEmpty(m.UnitCode) && m.UnitCode.Contains(unitCode)));
        //    }
        //    else if (!string.IsNullOrEmpty(username))
        //    {
        //        return GetWhere(query.And(m => !string.IsNullOrEmpty(m.Username) && m.Username.Contains(username)));
        //    }
        //    else if (!string.IsNullOrEmpty(unitCode))
        //    {
        //        return
        //            GetWhere(query.And(m => !string.IsNullOrEmpty(m.UnitCode) && m.UnitCode.Contains(unitCode)));
        //    }
        //    else
        //    {
        //        return GetWhere(query);
        //    }
        //}

        public async Task<IEnumerable<MPS_M_UnitCodeForEmployee>> Search(string empId = null, string unitCode = null)
        {
            var query = LinqKit.PredicateBuilder.True<MPS_M_UnitCodeForEmployee>();
            query = query.And(m => !m.InActive);
            if (!string.IsNullOrEmpty(empId) && !string.IsNullOrEmpty(unitCode))
            {
                return
                    GetWhere(query.And(m =>
                        !string.IsNullOrEmpty(m.EmpId) && m.EmpId.Contains(empId) &&
                        !string.IsNullOrEmpty(m.UnitCode) && m.UnitCode.Contains(unitCode)));
            }
            else if (!string.IsNullOrEmpty(empId))
            {
                return GetWhere(query.And(m => !string.IsNullOrEmpty(m.EmpId) && m.EmpId.Contains(empId)));
            }
            else if (!string.IsNullOrEmpty(unitCode))
            {
                return
                    GetWhere(query.And(m => !string.IsNullOrEmpty(m.UnitCode) && m.UnitCode.Contains(unitCode)));
            }
            else
            {
                return GetWhere(query);
            }
        }



        public async Task<IEnumerable<MPS_M_UnitCodeForEmployee>> GetInMaster(string empId = null, string unitCode = null)
        {
            var query = LinqKit.PredicateBuilder.True<MPS_M_UnitCodeForEmployee>();
            //query = query.And(m => !m.InActive);
            if (!string.IsNullOrEmpty(empId) && !string.IsNullOrEmpty(unitCode))
            {
                return
                    GetWhere(query.And(m =>
                        !string.IsNullOrEmpty(m.EmpId) && m.EmpId.Contains(empId) &&
                        !string.IsNullOrEmpty(m.UnitCode) && m.UnitCode.Contains(unitCode)));
            }
            else if (!string.IsNullOrEmpty(empId))
            {
                return GetWhere(query.And(m => !string.IsNullOrEmpty(m.EmpId) && m.EmpId.Contains(empId)));
            }
            else if (!string.IsNullOrEmpty(unitCode))
            {
                return
                    GetWhere(query.And(m => !string.IsNullOrEmpty(m.UnitCode) && m.UnitCode.Contains(unitCode)));
            }
            else
            {
                return GetWhere(query);
            }
        }
    }
}