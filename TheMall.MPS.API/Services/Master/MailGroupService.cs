﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using CrossingSoft.Framework.Infrastructure.OData.Repositories;
using LinqKit;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Models.Master;

namespace TheMall.MPS.API.Services
{
    public interface IMailGroupService : IBaseMasterService<MPS_M_MailGroup>
    {
        Task<IEnumerable<MPS_M_MailGroup>> Search(string code = null, string name = null);
    }
    public class MailGroupService : BaseMasterService<MPS_M_MailGroup>, IMailGroupService
    {
        public MailGroupService(IODataRepositoryAsync<MPS_M_MailGroup> repository) 
            : base(repository)
        {
        }

        public override MPS_M_MailGroup DoGet(string code)
        {
            return Get(code);
        }

        public override Task<MPS_M_MailGroup> DoGetAsync(string code)
        {
            return GetAsync(code);
        }

        public async Task<IEnumerable<MPS_M_MailGroup>> Search(string code = null, string email = null)
        {
            var query = LinqKit.PredicateBuilder.True<MPS_M_MailGroup>();
            query = query.And(m => !m.InActive);
            if (!string.IsNullOrEmpty(code) && !string.IsNullOrEmpty(email))
            {
                return
                    GetWhere(query.And(m =>
                        !string.IsNullOrEmpty(m.GroupCode) && m.GroupCode.Contains(code) &&
                        !string.IsNullOrEmpty(m.Email) && m.Email.Contains(email))
                        );
            }
            else if (!string.IsNullOrEmpty(code))
            {
                return GetWhere(query.And(m => !string.IsNullOrEmpty(m.GroupCode) && m.GroupCode.Contains(code)));
            }
            else if (!string.IsNullOrEmpty(email))
            {
                return
                    GetWhere(query.And(m => !string.IsNullOrEmpty(m.Email) && m.Email.Contains(email)));
            }
            else
            {
                return GetWhere(query);
            }
        }
    }
}