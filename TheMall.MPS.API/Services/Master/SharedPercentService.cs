﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using CrossingSoft.Framework.Infrastructure.OData.Repositories;
using LinqKit;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Infrastructure.EntityFramework;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Models.Master;

namespace TheMall.MPS.API.Services
{
    public interface ISharedPercentService : IBaseMasterService<MPS_M_SharedPercent>
    {
        Task<IEnumerable<MPS_M_SharedPercent>> Search(string code = null, string name = null);
    }
    public class SharedPercentService : BaseMasterService<MPS_M_SharedPercent>, ISharedPercentService
    {
        public SharedPercentService(IODataRepositoryAsync<IMPSDbContext, IMPSSession, MPS_M_SharedPercent> repository)
            : base(repository)
        {
        }

        public override MPS_M_SharedPercent DoGet(string code)
        {
            throw new NotImplementedException();
        }

        public override Task<MPS_M_SharedPercent> DoGetAsync(string code)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<MPS_M_SharedPercent>> Search(string code = null, string name = null)
        {
            var query = LinqKit.PredicateBuilder.True<MPS_M_SharedPercent>();

            if (!string.IsNullOrEmpty(code) && !string.IsNullOrEmpty(name))
            {
                return
                    GetWhere(query.And(m =>
                        !string.IsNullOrEmpty(m.PlaceCode) && m.PlaceCode.Contains(code) &&
                        !string.IsNullOrEmpty(m.PlaceName) && m.PlaceName.Contains(name)));
            }
            else if (!string.IsNullOrEmpty(code))
            {
                return GetWhere(query.And(m => !string.IsNullOrEmpty(m.PlaceCode) && m.PlaceCode.Contains(code)));
            }
            else if (!string.IsNullOrEmpty(name))
            {
                return
                    GetWhere(query.And(m => !string.IsNullOrEmpty(m.PlaceName) && m.PlaceName.Contains(name)));
            }
            else
            {
                return GetWhere(query);
            }
        }
    }
}