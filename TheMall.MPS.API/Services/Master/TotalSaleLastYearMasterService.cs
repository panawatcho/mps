﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using CrossingSoft.Framework.Infrastructure.OData.Repositories;
using LinqKit;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Infrastructure.EntityFramework;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Models;

namespace TheMall.MPS.API.Services
{
    public interface ITotalSaleLastYearMasterService : IBaseMasterService<MPS_M_TotalSaleLastYear>
    {
        Task<IEnumerable<MPS_M_TotalSaleLastYear>> Search(string branchCode = null, string year = null);
    }
    public class TotalSaleLastYearMasterService : BaseMasterService<MPS_M_TotalSaleLastYear>, ITotalSaleLastYearMasterService
    {
        public TotalSaleLastYearMasterService(
            IODataRepositoryAsync<IMPSDbContext, IMPSSession, MPS_M_TotalSaleLastYear> repository) : 
            base(repository)
        {
        }

        public override MPS_M_TotalSaleLastYear DoGet(string code)
        {
            return Get(code);
        }

        public override Task<MPS_M_TotalSaleLastYear> DoGetAsync(string code)
        {
            return GetAsync(code);
        }

        public async Task<IEnumerable<MPS_M_TotalSaleLastYear>> Search(string branchCode = null, string year = null)
        {
            var query = LinqKit.PredicateBuilder.True<MPS_M_TotalSaleLastYear>();

            if (!string.IsNullOrEmpty(branchCode) && !string.IsNullOrEmpty(year))
            {
                return
                    GetWhere(query.And(m =>
                        !string.IsNullOrEmpty(m.BranchCode) && m.BranchCode.Contains(branchCode) &&
                        !string.IsNullOrEmpty(m.Year) && m.Year.Contains(year)));
            }
            else if (!string.IsNullOrEmpty(branchCode))
            {
                return GetWhere(query.And(m => !string.IsNullOrEmpty(m.BranchCode) && m.BranchCode.Contains(branchCode)));
            }
            else if (!string.IsNullOrEmpty(year))
            {
                return
                    GetWhere(query.And(m => !string.IsNullOrEmpty(m.Year) && m.Year.Contains(year)));
            }
            else
            {
                return GetWhere(query);
            }
        }
    }
}