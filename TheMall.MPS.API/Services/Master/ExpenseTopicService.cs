﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using CrossingSoft.Framework.Infrastructure.OData.Repositories;
using LinqKit;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Infrastructure.EntityFramework;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Models.Master;

namespace TheMall.MPS.API.Services
{
    public interface IExpenseTopicService : IBaseMasterService<MPS_M_ExpenseTopic>
    {
        Task<IEnumerable<MPS_M_ExpenseTopic>> Search(string code = null, string name = null);
        IEnumerable<MPS_M_ExpenseTopic> ExpenseTopic();

    }
    public class ExpenseTopicService : BaseMasterService<MPS_M_ExpenseTopic>, IExpenseTopicService
    {
        public ExpenseTopicService(IODataRepositoryAsync<IMPSDbContext, IMPSSession, MPS_M_ExpenseTopic> repository)
            : base(repository)
        {
        }

        public override MPS_M_ExpenseTopic DoGet(string code)
        {
        
            return Get(code);
        }

        public override Task<MPS_M_ExpenseTopic> DoGetAsync(string code)
        {
          
            return GetAsync(code);
        }

        public async Task<IEnumerable<MPS_M_ExpenseTopic>> Search(string code = null, string name = null)
        {
            var query = LinqKit.PredicateBuilder.True<MPS_M_ExpenseTopic>();
            query = query.And(m => !m.InActive);
            if (!string.IsNullOrEmpty(code) && !string.IsNullOrEmpty(name))
            {
                return
                    GetWhere(query.And(m =>
                        !string.IsNullOrEmpty(m.ExpenseTopicCode) && m.ExpenseTopicCode.Contains(code) &&
                        !string.IsNullOrEmpty(m.ExpenseTopicName) && m.ExpenseTopicName.Contains(name)));
            }
            else if (!string.IsNullOrEmpty(code))
            {
                return GetWhere(query.And(m => !string.IsNullOrEmpty(m.ExpenseTopicCode) && m.ExpenseTopicCode.Contains(code)));
            }
            else if (!string.IsNullOrEmpty(name))
            {
                return
                    GetWhere(query.And(m => !string.IsNullOrEmpty(m.ExpenseTopicName) && m.ExpenseTopicName.Contains(name)));
            }
            else
            {
                return GetWhere(query);
            }
        }

        public IEnumerable<MPS_M_ExpenseTopic> ExpenseTopic()
        {
            var query = LinqKit.PredicateBuilder.True<MPS_M_ExpenseTopic>();
            query = query.And(m => !m.InActive);
            return GetWhere(query);
        }
    }
}