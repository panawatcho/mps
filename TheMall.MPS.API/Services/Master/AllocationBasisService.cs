﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using CrossingSoft.Framework.Infrastructure.OData.Repositories;
using LinqKit;
using TheMall.MPS.API.Services;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Infrastructure.EntityFramework;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Models.Master;
using TheMall.MPS.Models.Proposal;
using TheMall.MPS.ViewModels;
using TheMall.MPS.ViewModels.Master;

namespace TheMall.MPS.API.Services
{
    public interface IAllocationBasisService : IBaseMasterService<MPS_M_AllocationBasis>
    {
        MPS_M_AllocationBasis FindByProposalSharedTemplate(IEnumerable<MPS_ProposalLineSharedTemplate> sharedTemplates);
        IEnumerable<MPS_M_AllocationBasis> GetAllocationBasis();
        MPS_M_AllocationBasis FindAllocationCode(IEnumerable<SharedBranchViewModel> sharedTemplates, bool TR, bool RE);
    }
    public class AllocationBasisService : BaseMasterService<MPS_M_AllocationBasis>, IAllocationBasisService
    {
        public AllocationBasisService(IODataRepositoryAsync<IMPSDbContext, 
            IMPSSession, 
            MPS_M_AllocationBasis> repository)
            : base(repository)
        {
        }

        public override MPS_M_AllocationBasis DoGet(string code)
        {

            return Get(code);
        }

        public override Task<MPS_M_AllocationBasis> DoGetAsync(string code)
        {
            return GetAsync(code);
        }

        public MPS_M_AllocationBasis FindByProposalSharedTemplate(IEnumerable<MPS_ProposalLineSharedTemplate> sharedTemplates)
        {
            //Fix Branch Code
            var MRT = sharedTemplates.Any(n => n.BranchCode == "V4" && n.Selected);
            var B5 = sharedTemplates.Any(n => n.BranchCode == "B5" && n.Selected);
            var B7 = sharedTemplates.Any(n => n.BranchCode == "B7" && n.Selected);
            var M02 = sharedTemplates.Any(n => n.BranchCode == "M02" && n.Selected);
            var M03 = sharedTemplates.Any(n => n.BranchCode == "M03" && n.Selected);
            var M05 = sharedTemplates.Any(n => n.BranchCode == "M05" && n.Selected);
            var M06 = sharedTemplates.Any(n => n.BranchCode == "M06" && n.Selected);
            var M07 = sharedTemplates.Any(n => n.BranchCode == "M07" && n.Selected);
            var M08 = sharedTemplates.Any(n => n.BranchCode == "M08" && n.Selected);
            var M09 = sharedTemplates.Any(n => n.BranchCode == "M09" && n.Selected);
            var M10 = sharedTemplates.Any(n => n.BranchCode == "M10" && n.Selected);
            var M11 = sharedTemplates.Any(n => n.BranchCode == "M11" && n.Selected);
            var M12 = sharedTemplates.Any(n => n.BranchCode == "M12" && n.Selected);
            var M13 = sharedTemplates.Any(n => n.BranchCode == "M13" && n.Selected);
            var M14 = sharedTemplates.Any(n => n.BranchCode == "M14" && n.Selected);
            var M15 = sharedTemplates.Any(n => n.BranchCode == "M15" && n.Selected);
            var M16 = sharedTemplates.Any(n => n.BranchCode == "M16" && n.Selected);
            var M17 = sharedTemplates.Any(n => n.BranchCode == "M17" && n.Selected);
            var V1 = sharedTemplates.Any(n => n.BranchCode == "V1" && n.Selected);
            var V2 = sharedTemplates.Any(n => n.BranchCode == "V2" && n.Selected);
            var V3 = sharedTemplates.Any(n => n.BranchCode == "V3" && n.Selected);
            var V5 = sharedTemplates.Any(n => n.BranchCode == "V5" && n.Selected);
            var V9 = sharedTemplates.Any(n => n.BranchCode == "V9" && n.Selected);

            var proposalModel = sharedTemplates.Last().ProposalTable;
            var allocationBasis = LastOrDefault(m => m.MRT == MRT && m.B5 == B5 && m.B7 == B7 &&
                                                     m.M02 == M02 && m.M03 == M03 && m.M05 == M05 && m.M06 == M06 && m.M07 == M07 &&
                                                     m.M08 == M08 && m.M09 == M09 && m.M10 == M10 && m.M11 == M11 && m.M12 == M12 &&
                                                     m.M13 == M13 && m.M14 == M14 && m.M15 == M15 && m.M16 == M16 && m.M17 == M17 &&
                                                     m.V1 == V1 && m.V2 == V2 && m.V3 == V3 && m.V5 == V5 && m.V9 == V9 && m.TR == proposalModel.TR && m.RE == proposalModel.RE
                                                     && !m.InActive
                                                 ) 
                                  ?? LastOrDefault(m => m.MRT == false && m.B5 == false && m.B7 == false && 
                                                    m.M02 == false && m.M03 == false && m.M05 == false && m.M06 == false && m.M07 == false &&
                                                    m.M08 == false && m.M09 == false && m.M10 == false && m.M11 == false && m.M12 == false &&
                                                    m.M13 == false && m.M14 == false && m.M15 == false && m.M16 == false && m.M17 == false &&
                                                    m.V1 == false && m.V2 == false && m.V3 == false && m.V5 == false && m.V9 == false && m.TR == false &&m.RE == false);
            return allocationBasis;
        }

        public MPS_M_AllocationBasis FindAllocationCode(IEnumerable<SharedBranchViewModel> sharedTemplates, bool TR, bool RE)
        {
            //Fix Branch Code
            var MRT = sharedTemplates.Any(n => n.BranchCode == "V4" && n.Selected);
            var B5 = sharedTemplates.Any(n => n.BranchCode == "B5" && n.Selected);
            var B7 = sharedTemplates.Any(n => n.BranchCode == "B7" && n.Selected);
            var M02 = sharedTemplates.Any(n => n.BranchCode == "M02" && n.Selected);
            var M03 = sharedTemplates.Any(n => n.BranchCode == "M03" && n.Selected);
            var M05 = sharedTemplates.Any(n => n.BranchCode == "M05" && n.Selected);
            var M06 = sharedTemplates.Any(n => n.BranchCode == "M06" && n.Selected);
            var M07 = sharedTemplates.Any(n => n.BranchCode == "M07" && n.Selected);
            var M08 = sharedTemplates.Any(n => n.BranchCode == "M08" && n.Selected);
            var M09 = sharedTemplates.Any(n => n.BranchCode == "M09" && n.Selected);
            var M10 = sharedTemplates.Any(n => n.BranchCode == "M10" && n.Selected);
            var M11 = sharedTemplates.Any(n => n.BranchCode == "M11" && n.Selected);
            var M12 = sharedTemplates.Any(n => n.BranchCode == "M12" && n.Selected);
            var M13 = sharedTemplates.Any(n => n.BranchCode == "M13" && n.Selected);
            var M14 = sharedTemplates.Any(n => n.BranchCode == "M14" && n.Selected);
            var M15 = sharedTemplates.Any(n => n.BranchCode == "M15" && n.Selected);
            var M16 = sharedTemplates.Any(n => n.BranchCode == "M16" && n.Selected);
            var M17 = sharedTemplates.Any(n => n.BranchCode == "M17" && n.Selected);
            var V1 = sharedTemplates.Any(n => n.BranchCode == "V1" && n.Selected);
            var V2 = sharedTemplates.Any(n => n.BranchCode == "V2" && n.Selected);
            var V3 = sharedTemplates.Any(n => n.BranchCode == "V3" && n.Selected);
            var V5 = sharedTemplates.Any(n => n.BranchCode == "V5" && n.Selected);
            var V9 = sharedTemplates.Any(n => n.BranchCode == "V9" && n.Selected);

            //var proposalModel = sharedTemplates.Last();
            var allocationBasis = LastOrDefault(m => m.MRT == MRT && m.B5 == B5 && m.B7 == B7 &&
                                                     m.M02 == M02 && m.M03 == M03 && m.M05 == M05 && m.M06 == M06 && m.M07 == M07 &&
                                                     m.M08 == M08 && m.M09 == M09 && m.M10 == M10 && m.M11 == M11 && m.M12 == M12 &&
                                                     m.M13 == M13 && m.M14 == M14 && m.M15 == M15 && m.M16 == M16 && m.M17 == M17 &&
                                                     m.V1 == V1 && m.V2 == V2 && m.V3 == V3 && m.V5 == V5 && m.V9 == V9 && m.TR == TR && m.RE == RE
                                                     && !m.InActive
                                                 )
                                  ?? LastOrDefault(m => m.MRT == false && m.B5 == false && m.B7 == false &&
                                                    m.M02 == false && m.M03 == false && m.M05 == false && m.M06 == false && m.M07 == false &&
                                                    m.M08 == false && m.M09 == false && m.M10 == false && m.M11 == false && m.M12 == false &&
                                                    m.M13 == false && m.M14 == false && m.M15 == false && m.M16 == false && m.M17 == false &&
                                                    m.V1 == false && m.V2 == false && m.V3 == false && m.V5 == false && m.V9 == false && m.TR == false && m.RE == false);
            return allocationBasis;
        }

        public IEnumerable<MPS_M_AllocationBasis> GetAllocationBasis()
        {
            return GetWhere(m => m.InActive == false);
        }
    }
}