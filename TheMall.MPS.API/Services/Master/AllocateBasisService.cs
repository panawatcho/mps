﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using CrossingSoft.Framework.Infrastructure.OData.Repositories;
using LinqKit;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Models.Master;

namespace TheMall.MPS.API.Services
{
    public interface IAllocateBasisService : IBaseMasterService<MPS_M_AllocationBasis>
    {
        Task<IEnumerable<MPS_M_AllocationBasis>> Search(string allocCode = null);

    }
    public class AllocateBasisService : BaseMasterService<MPS_M_AllocationBasis>, IAllocateBasisService
    {
        public AllocateBasisService(IODataRepositoryAsync<MPS_M_AllocationBasis> repository) : base(repository)
        {
        }

        public override MPS_M_AllocationBasis DoGet(string code)
        {
            return Get(code);
        }

        public override Task<MPS_M_AllocationBasis> DoGetAsync(string code)
        {
            return GetAsync(code);
        }

        public async Task<IEnumerable<MPS_M_AllocationBasis>> Search(string allocCode = null)
        {
            var query = LinqKit.PredicateBuilder.True<MPS_M_AllocationBasis>();
            query = query.And(m => !m.InActive);
            if (!string.IsNullOrEmpty(allocCode))
            {
                return GetWhere(query.And(m => !string.IsNullOrEmpty(m.AllocCode) && m.AllocCode.Contains(allocCode)));
            }
            else
            {
                return GetWhere(query);
            }
            
        }
    }
}