﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using CrossingSoft.Framework.Infrastructure.OData.Repositories;
using LinqKit;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Infrastructure.EntityFramework;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Models.Master;

namespace TheMall.MPS.API.Services
{
    public interface ISharedBranchService : IBaseMasterService<MPS_M_SharedBranch>
    {
        Task<IEnumerable<MPS_M_SharedBranch>> Search(string code = null, string name = null);
    }
    public class SharedBranchService : BaseMasterService<MPS_M_SharedBranch>, ISharedBranchService
    {
        public SharedBranchService(IODataRepositoryAsync<IMPSDbContext, IMPSSession, MPS_M_SharedBranch> repository)
            : base(repository)
        {
        }

        public override MPS_M_SharedBranch DoGet(string code)
        {
            return Get(code);
        }

        public override Task<MPS_M_SharedBranch> DoGetAsync(string code)
        {
            return GetAsync(code);
        }

        public async Task<IEnumerable<MPS_M_SharedBranch>> Search(string code = null, string name = null)
        {
            var query = LinqKit.PredicateBuilder.True<MPS_M_SharedBranch>();
            query = query.And(m => !m.Inactive);
            if (!string.IsNullOrEmpty(code) && !string.IsNullOrEmpty(name))
            {
                return
                    GetWhere(query.And(m =>
                        !string.IsNullOrEmpty(m.BranchCode) && m.BranchCode.Contains(code) &&
                        !string.IsNullOrEmpty(m.BranchName) && m.BranchName.Contains(name))).OrderBy(m => m.Order);
            }
            else if (!string.IsNullOrEmpty(code))
            {
                return GetWhere(query.And(m => !string.IsNullOrEmpty(m.BranchCode) && m.BranchCode.Contains(code))).OrderBy(m => m.Order);
            }
            else if (!string.IsNullOrEmpty(name))
            {
                return
                    GetWhere(query.And(m => !string.IsNullOrEmpty(m.BranchName) && m.BranchName.Contains(name))).OrderBy(m=>m.Order);
            }
            else
            {
                return GetWhere(query).OrderBy(m => m.Order);
            }
        }
    }
}