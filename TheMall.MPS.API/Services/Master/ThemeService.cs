﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CrossingSoft.Framework.Infrastructure.OData.Repositories;
using LinqKit;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Infrastructure.EntityFramework;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Models.Master;

namespace TheMall.MPS.API.Services
{
    public interface IThemeService : IBaseMasterService<MPS_M_Themes>
    {

    }
    public class ThemeService : BaseMasterService<MPS_M_Themes>, IThemeService
    {
        public ThemeService(IODataRepositoryAsync<IMPSDbContext, IMPSSession, MPS_M_Themes> repository)
            : base(repository)
        {
        }

        public override MPS_M_Themes DoGet(string code)
        {
            return Get(code);
        }

        public override Task<MPS_M_Themes> DoGetAsync(string code)
        {
            return GetAsync(code);
        }
    }
}