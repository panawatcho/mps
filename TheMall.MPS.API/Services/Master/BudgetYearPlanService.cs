﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using CrossingSoft.Framework.Infrastructure.OData.Repositories;
using LinqKit;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Infrastructure.EntityFramework;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Models.Master;

namespace TheMall.MPS.API.Services
{
    public interface IBudgetYearPlanService : IBaseMasterService<MPS_M_BudgetYearPlan>
    {
        Task<IEnumerable<MPS_M_BudgetYearPlan>> Search(string code = null, string name = null);

        MPS_M_BudgetYearPlan GetBudgetYearPlan(string year, string unitCode, string typeYearPlan);
    }
    public class BudgetYearPlanService : BaseMasterService<MPS_M_BudgetYearPlan>, IBudgetYearPlanService
    {
        public BudgetYearPlanService(
            IODataRepositoryAsync<IMPSDbContext, IMPSSession, MPS_M_BudgetYearPlan> repository)
            : base(repository)
        {
        }

        public override MPS_M_BudgetYearPlan DoGet(string code)
        {
            return Get(code);
        }

        public override Task<MPS_M_BudgetYearPlan> DoGetAsync(string code)
        {
            return GetAsync(code);
        }

        public async Task<IEnumerable<MPS_M_BudgetYearPlan>> Search(string code = null, string name = null)
        {
            var query = LinqKit.PredicateBuilder.True<MPS_M_BudgetYearPlan>();

            if (!string.IsNullOrEmpty(code) && !string.IsNullOrEmpty(name))
            {
                return
                    GetWhere(query.And(m =>
                        !string.IsNullOrEmpty(m.TeamName) && m.TeamName.Contains(code) &&
                        !string.IsNullOrEmpty(m.Type) && m.Type.Contains(name)));
            }
            else if (!string.IsNullOrEmpty(code))
            {
                return GetWhere(query.And(m => !string.IsNullOrEmpty(m.TeamName) && m.TeamName.Contains(code)));
            }
            else if (!string.IsNullOrEmpty(name))
            {
                return
                    GetWhere(query.And(m => !string.IsNullOrEmpty(m.Type) && m.Type.Contains(name)));
            }
            else
            {
                return GetWhere(query);
            }
        }

        public MPS_M_BudgetYearPlan GetBudgetYearPlan(string year, string unitCode, string typeYearPlan)
        {
            return LastOrDefault(m => m.Year.Equals(year, StringComparison.InvariantCultureIgnoreCase)
                                      && m.UnitCode.Equals(unitCode, StringComparison.InvariantCultureIgnoreCase)
                                      && m.TypeYearPlan.Equals(typeYearPlan, StringComparison.InvariantCultureIgnoreCase));
        }
    }
}