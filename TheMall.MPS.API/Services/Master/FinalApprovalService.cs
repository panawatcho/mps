﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using CrossingSoft.Framework.Infrastructure.OData.Repositories;
using LinqKit;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Infrastructure.EntityFramework;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Models.Enums;
using TheMall.MPS.Models.Master;

namespace TheMall.MPS.API.Services
{
    public interface IFinalApproverService : IBaseMasterService<MPS_M_FinalApprover>
    {
        Task<IEnumerable<MPS_M_FinalApprover>> Search(string username = null, string process = null);
        MPS_M_FinalApprover GetFinalApprover(decimal budget, string process, string unitcode);
    }
    public class FinalApproverService : BaseMasterService<MPS_M_FinalApprover>, IFinalApproverService
    {
        public FinalApproverService(IODataRepositoryAsync<IMPSDbContext, IMPSSession, MPS_M_FinalApprover> repository)
            : base(repository)
        {
        }


        public override MPS_M_FinalApprover DoGet(string code)
        {
            return DoGet(code);
        }

        public override Task<MPS_M_FinalApprover> DoGetAsync(string code)
        {
            return DoGetAsync(code);
        }
        public async Task<IEnumerable<MPS_M_FinalApprover>> Search(string username = null, string process = null)
        {
            var query = LinqKit.PredicateBuilder.True<MPS_M_FinalApprover>();
            query = query.And(m => !m.InActive);
            if (!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(process))
            {
                return
                       GetWhere(query.And(m =>
                       !string.IsNullOrEmpty(m.Username) && m.Username.Contains(username) &&
                       !string.IsNullOrEmpty(m.ProcessCode) && m.ProcessCode.Contains(process)));
            }
            else if (!string.IsNullOrEmpty(username))
            {
                return GetWhere(query.And(m => !string.IsNullOrEmpty(m.Username) && m.Username.Contains(username)));
            }
            else if (!string.IsNullOrEmpty(process))
            {
                return
                    GetWhere(query.And(m => !string.IsNullOrEmpty(m.ProcessCode) && m.ProcessCode.Contains(process)));
            }
            else
            {
                return GetWhere(query);
            }
        }
        public MPS_M_FinalApprover GetFinalApprover(decimal budget ,string process, string unitcode)
        { 
            if(string.IsNullOrEmpty(process))
                return new MPS_M_FinalApprover();

            var query = LinqKit.PredicateBuilder.True<MPS_M_FinalApprover>();
            query = query.And(m => !m.InActive);
            query = query.And(m => m.Amount >= budget);
            query = query.And(m => m.ProcessCode.ToLower() == process.ToLower());
            query = query.And(m => m.UnitCode == unitcode);

            //if (process.ToLower() != Budget.DocumentType.Proposal.ToLower())
            //{
            //    query = query.And(m => m.UnitCode == unitcode);
            //}

            var model = (GetWhere(query) != null)
                ? GetWhere(query).OrderByDescending(m => m.Amount).LastOrDefault()
                : null;
          

            return model;
        }
    }
}