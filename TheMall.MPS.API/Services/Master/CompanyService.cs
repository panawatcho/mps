﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CrossingSoft.Framework.Infrastructure.OData.Repositories;
using LinqKit;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Infrastructure.EntityFramework;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Models.Master;

namespace TheMall.MPS.API.Services
{
    public interface ICompanyService : IBaseMasterService<MPS_M_Company>
    {
        
    }

    public class CompanyService : BaseMasterService<MPS_M_Company>, ICompanyService
    {
        public CompanyService(IODataRepositoryAsync<IMPSDbContext, 
            IMPSSession, MPS_M_Company> repository)
            : base(repository)
        {
        }

        public override MPS_M_Company DoGet(string code)
        {
            return Get(code);
        }

        public override Task<MPS_M_Company> DoGetAsync(string code)
        {
            return GetAsync(code);
        }
    }
}