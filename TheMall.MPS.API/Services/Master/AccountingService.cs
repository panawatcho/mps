﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using CrossingSoft.Framework.Infrastructure.OData.Repositories;
using LinqKit;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Infrastructure.EntityFramework;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Models.Master;

namespace TheMall.MPS.API.Services
{
    public interface IAccountingService : IBaseMasterService<MPS_M_Accounting>
    {
        Task<IEnumerable<MPS_M_Accounting>> Search(string code = null, string name = null);
        
    }
    public class AccountingService : BaseMasterService<MPS_M_Accounting>, IAccountingService
    {
        public AccountingService(IODataRepositoryAsync<IMPSDbContext, IMPSSession, MPS_M_Accounting> repository)
            : base(repository)
        {
        }

        public override MPS_M_Accounting DoGet(string code)
        {

            return Get(code);
        }

        public override Task<MPS_M_Accounting> DoGetAsync(string code)
        {
            return GetAsync(code);
        }

        public async Task<IEnumerable<MPS_M_Accounting>> Search(string brachCode = null, string username = null)
        {
            var query = LinqKit.PredicateBuilder.True<MPS_M_Accounting>();
            query = query.And(m => !m.InActive);
            if (!string.IsNullOrEmpty(brachCode) && !string.IsNullOrEmpty(username))
            {
                return
                    GetWhere(query.And(m =>
                        !string.IsNullOrEmpty(m.BranchCode) && m.Username.Contains(brachCode) &&
                        !string.IsNullOrEmpty(m.Username) && m.BranchCode.Contains(username)));
            }
            else if (!string.IsNullOrEmpty(brachCode))
            {
                return GetWhere(query.And(m => !string.IsNullOrEmpty(m.BranchCode) && m.BranchCode.Contains(brachCode)));
            }
            else if (!string.IsNullOrEmpty(username))
            {
                return
                    GetWhere(query.And(m => !string.IsNullOrEmpty(m.Username) && m.Username.Contains(username)));
            }
            else
            {
                return GetWhere(query);
            }
            
        }

        
    }
}