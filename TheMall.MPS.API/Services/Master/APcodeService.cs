﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using CrossingSoft.Framework.Infrastructure.OData.Repositories;
using LinqKit;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Infrastructure.EntityFramework;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Models.Master;

namespace TheMall.MPS.API.Services
{
    public interface IAPcodeService : IBaseMasterService<MPS_M_APCode>
    {
        Task<IEnumerable<MPS_M_APCode>> Search(string code = null, string name = null);
        IEnumerable<MPS_M_APCode> APCode();
    }
    public class APcodeService : BaseMasterService<MPS_M_APCode>, IAPcodeService
    {
        public APcodeService(IODataRepositoryAsync<IMPSDbContext, IMPSSession, MPS_M_APCode> repository)
            : base(repository)
        {
        }

        public override MPS_M_APCode DoGet(string code)
        {

            return Get(code);
        }

        public override Task<MPS_M_APCode> DoGetAsync(string code)
        {
            return GetAsync(code);
        }

        public async Task<IEnumerable<MPS_M_APCode>> Search(string code = null, string name = null)
        {
            var query = LinqKit.PredicateBuilder.True<MPS_M_APCode>();
            query = query.And(m => !m.InActive);
            if (!string.IsNullOrEmpty(code) && !string.IsNullOrEmpty(name))
            {
                return
                    GetWhere(query.And(m =>
                        !string.IsNullOrEmpty(m.APCode) && m.APCode.Contains(code) &&
                        !string.IsNullOrEmpty(m.APName) && m.APName.Contains(name)));
            }
            else if (!string.IsNullOrEmpty(code))
            {
                return GetWhere(query.And(m => !string.IsNullOrEmpty(m.ExpenseTopicCode) && m.ExpenseTopicCode.Contains(code)));
            }
            else if (!string.IsNullOrEmpty(name))
            {
                return
                    GetWhere(query.And(m => !string.IsNullOrEmpty(m.APName) && m.APName.Contains(name)));
            }
            else
            {
                return GetWhere(query);
            }
            
        }

        public IEnumerable<MPS_M_APCode> APCode()
        {
            var query = LinqKit.PredicateBuilder.True<MPS_M_APCode>();
            query = query.And(m => !m.InActive);
            return GetWhere(query);
        }
    }
}