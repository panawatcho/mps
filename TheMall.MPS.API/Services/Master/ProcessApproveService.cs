﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using CrossingSoft.Framework.Infrastructure.OData.Repositories;
using LinqKit;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Infrastructure.EntityFramework;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Models.Master;

namespace TheMall.MPS.API.Services
{
    public interface IProcessApproveService : IBaseMasterService<MPS_M_ProcessApprove>
    {
        Task<IEnumerable<MPS_M_ProcessApprove>> Search(string processCode = null);
    }
    public class ProcessApproveService : BaseMasterService<MPS_M_ProcessApprove>, IProcessApproveService
    {
        public ProcessApproveService(
            IODataRepositoryAsync<IMPSDbContext, IMPSSession, MPS_M_ProcessApprove> repository) 
            : base(repository)
        {
        }

        public override MPS_M_ProcessApprove DoGet(string code)
        {
            return Get(code);
        }

        public override Task<MPS_M_ProcessApprove> DoGetAsync(string code)
        {
            return GetAsync(code);
        }

        public async Task<IEnumerable<MPS_M_ProcessApprove>> Search(string processCode = null)
        {
            var query = LinqKit.PredicateBuilder.True<MPS_M_ProcessApprove>();
            query = query.And(m => !m.InActive);
            if (!string.IsNullOrEmpty(processCode))
            {
                return GetWhere(query.And(m => !string.IsNullOrEmpty(m.ProcessCode) && m.ProcessCode.Contains(processCode)));
            }
            else
            {
                return GetWhere(query);
            }
        }
    }
}