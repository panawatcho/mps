﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using CrossingSoft.Framework.Infrastructure.OData.Repositories;
using TheMall.MPS.Infrastructure.Security;

namespace TheMall.MPS.API.Services
{
    public interface IMenuService
    {
        IReadOnlyList<MenuItem> GetAuthorized(string userId, IEnumerable<string> roles);
        Task<IReadOnlyList<MenuItem>> GetAuthorizedAsync(string userId, IEnumerable<string> roles);
    }
    public class MenuService : IMenuService
    {
        private readonly IODataRepositoryAsync<MenuItem> _itemRepository;
        private readonly IODataRepositoryAsync<RefererRolePermission> _refRoleRepository;
        private readonly IODataRepositoryAsync<RefererUserPermission> _refUserRepository;

        public MenuService(IODataRepositoryAsync<MenuItem> itemRepository,
            IODataRepositoryAsync<RefererRolePermission> refRoleRepository,
            IODataRepositoryAsync<RefererUserPermission> refUserRepository
            )
        {
            _itemRepository = itemRepository;
            _refRoleRepository = refRoleRepository;
            _refUserRepository = refUserRepository;
        }

        private IQueryable<MenuItem> GetAuthorizedItemsInternal(string userId, IEnumerable<string> roles)
        {
            var roleList = new List<string>(roles);
            var byUser = from item in _itemRepository.Queryable()
                         where (!item.Inactive && !item.Children.Any())
                               || (!item.Inactive && item.Children.Any(m => !m.Inactive))
                         join userPermission in _refUserRepository.Queryable()
                             on item.RefererId equals userPermission.RefererId
                         where userPermission.UserId == userId
                         //&& userPermission.Enable
                         select new { MenuItem = item, userPermission.Enable };

            var byRole = from item in _itemRepository.Queryable()
                         where (!item.Inactive && !item.Children.Any())
                               || (!item.Inactive && item.Children.Any(m => !m.Inactive))
                         join rolePermission in _refRoleRepository.Queryable()
                             on item.RefererId equals rolePermission.RefererId
                         join role in roleList on rolePermission.RoleId equals role
                         where rolePermission.Enable
                         select item;

            var all = (byUser.Where(m => m.Enable).Select(m => m.MenuItem)).Union(byRole).Distinct();
            //var exceptByUser = all.Except(byUser.Where(m => !m.Enable).Select(m => m.MenuItem));
            // Find menu item that the user has permission on a child but not exlpicitly given on parent.
            var orphan = all.Where(m => m.ParentId != null && !all.Any(n => n.Id == m.ParentId));
            var adoption = orphan.Select(m => m.Parent).Distinct().Select(m => m.Id);
            var family = all.Union(_itemRepository.Queryable().Where(m => adoption.Contains(m.Id)));

            var exceptByUser = family.Except(byUser.Where(m => !m.Enable).Select(m => m.MenuItem));

            return exceptByUser;

            
        }

        public IReadOnlyList<MenuItem> GetAuthorized(string userId, IEnumerable<string> roles)
        {
            return GetAuthorizedItemsInternal(userId, roles).ToList();
        }

        public async Task<IReadOnlyList<MenuItem>> GetAuthorizedAsync(string userId, IEnumerable<string> roles)
        {
            return await GetAuthorizedItemsInternal(userId, roles).ToListAsync();
        }
    }
}