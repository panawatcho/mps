﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using CrossingSoft.Framework.Infrastructure.OData.Repositories;
using CrossingSoft.Framework.Services.OData;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.Infrastructure.Repositories;
using TheMall.MPS.Models;


namespace TheMall.MPS.API.Services
{
    public interface IEmployeeTableService : IODataServiceAsync<MPS_M_EmployeeTable>
    {
        IEnumerable<MPS_M_EmployeeTable> Search(string username = null, string name = null);
        Task<IEnumerable<MPS_M_EmployeeTable>> SearchAsync(string username = null, string name = null);
        Task<IEnumerable<MPS_M_EmployeeTable>> SearchAllAsync(string query);
        MPS_M_EmployeeTable FindByUsername(string username);
        IEnumerable<MPS_M_EmployeeTable> SearchAccounting(string username = null, string name = null);
        IEnumerable<MPS_M_EmployeeTable> SearchByEmail(string email);

        MPS_M_EmployeeTable SearchById(string empId);
    }

    public class EmployeeTableService : BaseODataService<MPS_M_EmployeeTable>, IEmployeeTableService
    {
        //private readonly IODataRepositoryAsync<MPS_EmployeeTable> _repository;
        private readonly IMpsODataRepositoryAsync<MPS_M_EmployeeTable> _repository;
        public EmployeeTableService(IMpsODataRepositoryAsync<MPS_M_EmployeeTable> repository)
        //public EmployeeTableService(IODataRepositoryAsync<MPS_EmployeeTable> repository)
            : base(repository)
        {
            _repository = repository;
        }
        public IEnumerable<MPS_M_EmployeeTable> Search(string username = null, string name = null)

        {
            var ss = _repository.Find(m => m.Authority == "111");
            var ss2 = Find(m => m.Authority == "111");


            if (!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(name))
            {
                return GetWhere(m=>string.IsNullOrEmpty(m.Username) && m.Username.Contains(username)
                                     && ((!string.IsNullOrEmpty(m.EnFName) && m.EnFName.Contains(name))
                                         || (!string.IsNullOrEmpty(m.EnLName) && m.EnLName.Contains(name))
                                         || (!string.IsNullOrEmpty(m.ThFName) && m.ThFName.Contains(name))
                                         || (!string.IsNullOrEmpty(m.ThLName) && m.ThLName.Contains(name))));
            }
            else if (!string.IsNullOrEmpty(username))
            {
                return
                    GetWhere(m => !string.IsNullOrEmpty(m.Username) && m.Username.Contains(username));
            }
            else if (!string.IsNullOrEmpty(name))
            {
                return
                    GetWhere(m => !string.IsNullOrEmpty(m.Username)
                            && ((!string.IsNullOrEmpty(m.EnFName) && m.EnFName.Contains(name))
                                         || (!string.IsNullOrEmpty(m.EnLName) && m.EnLName.Contains(name))
                                         || (!string.IsNullOrEmpty(m.ThFName) && m.ThFName.Contains(name))
                                         || (!string.IsNullOrEmpty(m.ThLName) && m.ThLName.Contains(name))));
            }
            return GetWhere(m => !string.IsNullOrEmpty(m.Username) && m.EmpStatus != "Hold");
        }

        public async Task<IEnumerable<MPS_M_EmployeeTable>> SearchAsync(string username = null, string name = null)
        {
            if (!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(name))
            {
                return await GetWhereAsync(m => string.IsNullOrEmpty(m.Username) && m.Username.Contains(username)
                                     && ((!string.IsNullOrEmpty(m.EnFName) && m.EnFName.Contains(name))
                                         || (!string.IsNullOrEmpty(m.EnLName) && m.EnLName.Contains(name))
                                         || (!string.IsNullOrEmpty(m.ThFName) && m.ThFName.Contains(name))
                                         || (!string.IsNullOrEmpty(m.ThLName) && m.ThLName.Contains(name))));
            }
            else if (!string.IsNullOrEmpty(username))
            {
                return await 
                    GetWhereAsync(m => !string.IsNullOrEmpty(m.Username) && m.Username.Contains(username));
            }
            else if (!string.IsNullOrEmpty(name))
            {
                return await 
                    GetWhereAsync(m => !string.IsNullOrEmpty(m.Username)
                            && ((!string.IsNullOrEmpty(m.EnFName) && m.EnFName.Contains(name))
                                         || (!string.IsNullOrEmpty(m.EnLName) && m.EnLName.Contains(name))
                                         || (!string.IsNullOrEmpty(m.ThFName) && m.ThFName.Contains(name))
                                         || (!string.IsNullOrEmpty(m.ThLName) && m.ThLName.Contains(name))));
            }
            return  await GetWhereAsync(m => !string.IsNullOrEmpty(m.Username));
        }
        public async Task<IEnumerable<MPS_M_EmployeeTable>> SearchAllAsync(string query)
        {
            return await GetWhereAsync
                                 (m=>
                                     (!string.IsNullOrEmpty(m.Username) && m.Username.Contains(query))
                                     || (!string.IsNullOrEmpty(m.EnLName) && m.EnLName.Contains(query))
                                     || (!string.IsNullOrEmpty(m.EnLName) && m.EnLName.Contains(query))
                                     || (!string.IsNullOrEmpty(m.ThFName) && m.ThFName.Contains(query))
                                     || (!string.IsNullOrEmpty(m.ThLName) && m.ThLName.Contains(query)));
        }

        public MPS_M_EmployeeTable FindByUsername(string username)
        {
            if (string.IsNullOrEmpty(username))
            {
                return null;
            }
            username = RemoveDomain(username);

            return Find(m => m.EmpStatus == "Active" && !string.IsNullOrEmpty(m.Username) && m.Username.Equals(username, StringComparison.InvariantCultureIgnoreCase));
            return Find(m => !string.IsNullOrEmpty(m.Username) && m.Username.Equals(username, StringComparison.InvariantCultureIgnoreCase));
        }

        private static string RemoveDomain(string source)
        {
            if (string.IsNullOrEmpty(source))
            {
                return source;
            }
            var pos = source.IndexOf(@"\", StringComparison.Ordinal);
            if (pos >= 0)
            {
                pos = pos + 1;
                return source.Substring(pos, source.Length - pos); ;
            }
            return source;
        }

        public IEnumerable<MPS_M_EmployeeTable> SearchAccounting(string username = null, string name = null)

        {
            var employee = Search(username, name).Where(m=>m.PositionName.Contains("Accounting"));
            return employee;
        }


        public IEnumerable<MPS_M_EmployeeTable> SearchByEmail(string email)
        {


            if (!string.IsNullOrEmpty(email))
            {
                return
                    GetWhere(m => !string.IsNullOrEmpty(m.Email) && m.Email.Contains(email));
            }

            return GetWhere(m => !string.IsNullOrEmpty(m.Email));
        }

        public MPS_M_EmployeeTable SearchById(string empId)
        {
            if (!string.IsNullOrEmpty(empId))
            {
                return
                   FirstOrDefault(m => !string.IsNullOrEmpty(m.EmpId) && m.EmpId.Equals(empId, StringComparison.InvariantCultureIgnoreCase));
            }

            return null;
        }

    }
}