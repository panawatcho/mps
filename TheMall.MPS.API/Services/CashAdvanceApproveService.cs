﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CrossingSoft.Framework.Infrastructure.OData.Repositories;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.Models.CashAdvance;
using TheMall.MPS.Models.Proposal;

namespace TheMall.MPS.API.Services
{
    public interface ICashAdvanceApproveService : Interfaces.IODataService<MPS_CashAdvanceApproval>
    {
    }
    public class CashAdvanceApproveService : BaseODataService<MPS_CashAdvanceApproval>, ICashAdvanceApproveService
    {
        public CashAdvanceApproveService(IODataRepositoryAsync<MPS_CashAdvanceApproval> repository)
            : base(repository)
        {
        }
    }
}
  