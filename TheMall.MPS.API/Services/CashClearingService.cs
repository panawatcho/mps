﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using CrossingSoft.Framework.Infrastructure.OData.Repositories;
using CrossingSoft.Framework.Models.Enums;
using LinqKit;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Infrastructure.EntityFramework;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Models.CashAdvance;
using TheMall.MPS.Models.CashClearing;

namespace TheMall.MPS.API.Services
{
    public interface ICashClearingService : IMpsWorkflowTableService<MPS_CashClearingTable>
    {
        Task<IEnumerable<MPS_CashClearingTable>> SearchDocument(string code = null, string name = null);
        IEnumerable<MPS_CashClearingTable> SearchDoc(DateTime? createDate,
        bool checkStartDate,
        string process = null, string title = null, string documentNo = null,
        string username = null, string status = null);
    }
    public class CashClearingService : MpsWorkflowTableService<MPS_CashClearingTable>, ICashClearingService
    {
        public CashClearingService(IODataRepositoryAsync<IMPSDbContext, IMPSSession, MPS_CashClearingTable> repository) 
            : base(repository)
        {
        }

        public override MPS_CashClearingTable GetWorkflowTable(int id)
        {
            return Get(id);
        }

        public override Task<MPS_CashClearingTable> GetWorkflowTableAsync(int id)
        {
            return GetAsync(id);
        }

        public Task<IEnumerable<MPS_CashClearingTable>> SearchDocument(string code = null, string name = null)
        {
            return null;
        }

        public IEnumerable<MPS_CashClearingTable> SearchDoc(DateTime? createDate,
         bool checkStartDate,
         string process = null,
         string title = null,
         string documentNo = null,
         string username = null,
         string status = null)
        {
            var query = LinqKit.PredicateBuilder.True<MPS_CashClearingTable>();
            if (!string.IsNullOrEmpty(status))
            {
                query = query.And(m => m.DocumentStatus.Equals(status));
            }

            if (!string.IsNullOrEmpty(title))
            {
                query = query.And(m => !string.IsNullOrEmpty(m.Title) && m.Title.Contains(title));
            }
            if (!string.IsNullOrEmpty(documentNo))
            {
                query = query.And(m => !string.IsNullOrEmpty(m.DocumentNumber) && m.DocumentNumber.Contains(documentNo));
            }
            if (!string.IsNullOrEmpty(username))
            {
                query = query.And(m => !string.IsNullOrEmpty(m.CreatedBy) && m.CreatedBy.Contains(username));
            }
            //if (checkStartDate)
            //{
            //    query = query.And(m => m.CreatedDate.Equals(startDate));
            //}
            var result = GetWhere(query);
            if (createDate != null)
            {
                DateTime dateTime = (DateTime) createDate;
                result = result.Where(m => m.CreatedDate != null && m.CreatedDate.Value.Date == dateTime.Date);
                //query = query.And(m => m.CreatedDate.Equals(startDate));
            }
            return result;
           // return GetWhere(query);
        }
    }
}