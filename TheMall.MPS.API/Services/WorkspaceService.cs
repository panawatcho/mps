﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Configuration;
using System.Web.Http.OData.Query;
using CrossingSoft.Framework.Infrastructure.K2;
using SourceCode.Workflow.Client;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Models.K2;
using TheMall.MPS.ViewModels.Workspace;
using TheMall.MPS.Models.Workflow;
using TheMall.MPS.API.Mappers;


namespace TheMall.MPS.API.Services
{
    public interface IWorkspaceService : IService
    {
        void Delegate(string SN, string toUserFQN);
        void Redirect(string SN, string toUserFQN);
        MyWorklistItemViewModel GetWorklist(ODataQueryOptions oDataQueryOptions, WorklistCriteria worklistCriteria = null, bool getAll = false);
        AccWorklistItemViewModel GetWorklistForProposal(ODataQueryOptions oDataQueryOptions = null, SourceCode.Workflow.Client.WorklistCriteria worklistCriteria = null, bool getAll = false);
    }

    public class WorkspaceService : BaseWorkflowService, IWorkspaceService
    {
        private readonly IWorkflowRepository _workflowRepository;
        private readonly IProposalService _proposalService;
        private readonly IProposalMapper _proposalMapper;
        private readonly string _securityLabel = WebConfigurationManager.AppSettings["SecurityLabelName"];

        public WorkspaceService(IWorkflowRepository workflowRepository, 
            IWorkingHourService workingHourService, 
            IProposalService proposalService, 
            IProposalMapper proposalMapper)
            : base(workingHourService)
        {
            _workflowRepository = workflowRepository;
            _proposalService = proposalService;
            _proposalMapper = proposalMapper;
        }

        public void Redirect(string SN, string toUserFQN)
        {
            toUserFQN = FQN(toUserFQN);

            WorklistItem wli = _workflowRepository.GetWorkListItem(SN);
          
            wli.Redirect(toUserFQN);
        }

        public void Delegate(string SN, string toUserFQN)
        {

            toUserFQN = FQN(toUserFQN);

            WorklistItem wli = _workflowRepository.GetWorkListItem(SN);

            //code to delegate
            Destination _Destination = new Destination();
            //Allow actions to delegated user
            foreach (SourceCode.Workflow.Client.Action _action in wli.Actions)
            {
                _Destination.AllowedActions.Add(_action.Name);
            }
            _Destination.DestinationType = DestinationType.User;
            _Destination.Name = toUserFQN;
            wli.Redirect(toUserFQN);
        }

        public string FQN(string name)
        {
            if (!name.StartsWith(_securityLabel + ":"))
                name = _securityLabel + ":" + name;
            return name;
        }

        public MyWorklistItemViewModel GetWorklist(ODataQueryOptions oDataQueryOptions = null, SourceCode.Workflow.Client.WorklistCriteria worklistCriteria = null, bool getAll = false)
        {
            var skip = oDataQueryOptions == null || oDataQueryOptions.Skip == null ? 0 : oDataQueryOptions.Skip.Value;
            var top = oDataQueryOptions == null || oDataQueryOptions.Top == null ? 10 : oDataQueryOptions.Top.Value;
            var page = (skip / top) + 1;
            var pageSize = top;

            if (worklistCriteria == null)
            {
                worklistCriteria = new WorklistCriteria();
            }
            worklistCriteria.AddFilterField(WCLogical.AndBracket, WCField.WorklistItemStatus, WCCompare.Equal,
                                                    WorklistStatus.Available);
            worklistCriteria.AddFilterField(WCLogical.Or, WCField.WorklistItemStatus, WCCompare.Equal,
                                                    WorklistStatus.Open);
            worklistCriteria.AddFilterField(WCLogical.Or, WCField.WorklistItemStatus, WCCompare.Equal,
                                                    WorklistStatus.Allocated);

            worklistCriteria = PrepareCriteria(worklistCriteria, oDataQueryOptions);

            //worklistCriteria.AddSortField(WCField.EventStartDate, WCSortOrder.Descending);
            SourceCode.Workflow.Client.Worklist worklist = null;
            if (!getAll)
            {
                worklist = _workflowRepository.GetWorkList(worklistCriteria, pageSize, page, false);
            }
            else
            {
                worklist = _workflowRepository.GetWorkList(worklistCriteria, null, null, false);
            }

            if (worklist == null)
            {
                return null;
            }
            var worklists = (from WorklistItem worklistItem in worklist select ToMyWorklistItem(worklistItem)).ToListSafe();
            foreach (var myworklist in worklists)
            {
                myworklist.ViewFlow = GetViewFlowUrl(myworklist.ProcessInstanceId);
            }
            var myWorklistItemViewModel = new MyWorklistItemViewModel();
            myWorklistItemViewModel.Data = worklists;
            myWorklistItemViewModel.TotalCount = worklist.TotalCount;

            return myWorklistItemViewModel;
        }

        //public async Task<MyWorklistItemViewModel> GetWorklistAsync(GridDataSourceRequest gridDataSourceRequest)
        //{
        //    try
        //    {
        //        var worklistCriteria = new WorklistCriteria();
        //        worklistCriteria.AddFilterField(WCLogical.AndBracket, WCField.WorklistItemStatus, WCCompare.Equal,
        //                                               WorklistStatus.Available);
        //        worklistCriteria.AddFilterField(WCLogical.Or, WCField.WorklistItemStatus, WCCompare.Equal,
        //                                               WorklistStatus.Open);
        //        worklistCriteria.AddFilterField(WCLogical.Or, WCField.WorklistItemStatus, WCCompare.Equal,
        //                                               WorklistStatus.Allocated);

        //        worklistCriteria = PrepareCriteria(worklistCriteria, gridDataSourceRequest);

        //        worklistCriteria.AddSortField(WCField.EventStartDate, WCSortOrder.Descending);

        //        var worklist = await _workflowRepository.GetWorkListAsync(worklistCriteria, gridDataSourceRequest.PageSize, gridDataSourceRequest.Page, false);

        //        var worklists = (from WorklistItem worklistItem in worklist select ToMyWorklistItem(worklistItem)).ToListSafe();

        //        var myWorklistItemViewModel = new MyWorklistItemViewModel();
        //        myWorklistItemViewModel.Data = worklists;
        //        myWorklistItemViewModel.TotalCount = worklist.TotalCount;

        //        return myWorklistItemViewModel;
        //    }
        //    catch (Exception exception)
        //    {
        //        throw HandleException(exception);
        //    }
        //}

        #region by the mall
        public AccWorklistItemViewModel GetWorklistForProposal(ODataQueryOptions oDataQueryOptions = null, SourceCode.Workflow.Client.WorklistCriteria worklistCriteria = null, bool getAll = false)
        {
            var skip = oDataQueryOptions == null || oDataQueryOptions.Skip == null ? 0 : oDataQueryOptions.Skip.Value;
            var top = oDataQueryOptions == null || oDataQueryOptions.Top == null ? 20 : oDataQueryOptions.Top.Value;
            var page = (skip / top) + 1;
            var pageSize = top;

            if (worklistCriteria == null)
            {
                worklistCriteria = new WorklistCriteria();
            }
            worklistCriteria.AddFilterField(WCLogical.AndBracket, WCField.WorklistItemStatus, WCCompare.Equal,
                                                    WorklistStatus.Available);
            worklistCriteria.AddFilterField(WCLogical.Or, WCField.WorklistItemStatus, WCCompare.Equal,
                                                    WorklistStatus.Open);
            worklistCriteria.AddFilterField(WCLogical.Or, WCField.WorklistItemStatus, WCCompare.Equal,
                                                    WorklistStatus.Allocated);
            worklistCriteria = PrepareCriteria(worklistCriteria, oDataQueryOptions);

            //worklistCriteria.AddSortField(WCField.EventStartDate, WCSortOrder.Descending);
            SourceCode.Workflow.Client.Worklist worklist = null;
            if (!getAll)
            {
                worklist = _workflowRepository.GetWorkList(worklistCriteria, pageSize, page, false);
            }
            else
            {
                worklist = _workflowRepository.GetWorkList(worklistCriteria, null, null, false);
            }

            if (worklist == null)
            {
                return null;
            }
            // var proposalTable = _proposalService.GetWorkflowTableIdFromSN(worklist);
            var worklists = (from WorklistItem worklistItem in worklist select ToMyWorklistItemForProposal(worklistItem)).ToListSafe();
            worklists = worklists.Where(m => m.ProcessName.ToLower().Contains("proposal_account")).ToListSafe();
            foreach (var myworklist in worklists)
            {
                myworklist.ViewFlow = GetViewFlowUrl(myworklist.ProcessInstanceId);
                var ProcInt = myworklist.ProcessInstanceId - 1;
                var proposal = _proposalService.GetWorkflowTableFromProcInstId(ProcInt);
                myworklist.ProposalViewModel = (proposal != null) ? _proposalMapper.ToViewModel(proposal):new ViewModels.Proposal.ProposalViewModel();
                if (myworklist.ProposalViewModel != null)
                {
                    if (myworklist.ProposalViewModel.DepositLines != null)
                    {
                        if (myworklist.ProposalViewModel.DepositLines.Any())
                        {
                            myworklist.Amount = myworklist.ProposalViewModel.DepositLines.Sum(r => r.Amount);
                            myworklist.Actual = (decimal)myworklist.ProposalViewModel.DepositLines.Sum(r => r.Actual);
                        }
                    }
                    if (myworklist.ProposalViewModel.IncomeOther != null)
                    {
                        if (myworklist.ProposalViewModel.IncomeOther.Any())
                        {
                            myworklist.Amount = myworklist.ProposalViewModel.IncomeOther.Sum(r => r.Budget);
                            myworklist.Actual = (decimal)myworklist.ProposalViewModel.IncomeOther.Sum(r => r.Actual);
                        }
                    }
                }
            }
            var myWorklistItemViewModel = new AccWorklistItemViewModel();
            myWorklistItemViewModel.Data = worklists;
            myWorklistItemViewModel.TotalCount = worklist.TotalCount;

            return myWorklistItemViewModel;
        }
        #endregion

        //
        private SourceCode.Workflow.Client.WCField? ToWCField(string member)
        {
            switch (member.Trim().ToUpper())
            {
                case "FOLIO":
                    return WCField.ProcessFolio;
                case "TITLE":
                    return WCField.ProcessDescription;
                case "ACTIVITYNAME":
                    return WCField.ActivityName;
                case "ACTIVITYDESCRIPTION":
                    return WCField.ActivityDescription;
                case "STARTDATE":
                    return WCField.ActivityStartDate;
            }
            return null;
        }

        private MyWorklistFilterOperator ToMyWorklistFilterOperator(
            Microsoft.Data.OData.Query.BinaryOperatorKind operatorKind)
        {
            switch (operatorKind)
            {
                case Microsoft.Data.OData.Query.BinaryOperatorKind.NotEqual:
                    return MyWorklistFilterOperator.NotEquals;
                default:
                    return MyWorklistFilterOperator.Equals;
            }
        }

        public string GetViewFlowUrl(int procInstId)
        {
            var baseUrl = AppSettings.Instance.BaseUrlViewFlow + procInstId;
            return baseUrl;
        }

        //private void ToMyFilters(List<MyWorklistFilter> filters, Microsoft.Data.OData.Query.SemanticAst.BinaryOperatorNode node)
        //{
        //    var oper = node.OperatorKind;

        //    if (oper == Microsoft.Data.OData.Query.BinaryOperatorKind.And)
        //    {
        //        //ToMyFilters(filters, node.Left as Microsoft.Data.OData.Query.SemanticAst.BinaryOperatorNode);
        //        //ToMyFilters(filters, node.Right as Microsoft.Data.OData.Query.SemanticAst.BinaryOperatorNode);
        //        ToMyFilters(filters, node.Left);
        //        ToMyFilters(filters, node.Right);
        //    }
        //    else
        //    {
        //        if (node.Left is Microsoft.Data.OData.Query.SemanticAst.SingleValuePropertyAccessNode)
        //        {
        //            var filter = new MyWorklistFilter();
        //            filter.Operator = ToMyWorklistFilterOperator(oper);
        //            var left = node.Left as Microsoft.Data.OData.Query.SemanticAst.SingleValuePropertyAccessNode;
        //            var prop = Extract(left.Property as Microsoft.Data.Edm.Library.EdmStructuralProperty);
        //            if (prop != null)
        //            {
        //                filter.Prop = prop;
        //            }

        //            var right = Extract(node.Right as Microsoft.Data.OData.Query.SemanticAst.ConstantNode);
        //            if (right != null)
        //            {
        //                filter.Value = right;
        //            }
        //            filters.Add(filter);
        //        }
        //        else if (node.Left is Microsoft.Data.OData.Query.SingleValueFunctionCallNode)
        //        {
        //            var filter = ToMyFilters(node.Left as Microsoft.Data.OData.Query.SingleValueFunctionCallNode);
        //            if (filter != null)
        //            {
        //                filter.Operator = MyWorklistFilterOperator.DoesNotContain;
        //                filters.Add(filter);
        //            }
        //        }
        //    }
        //}

        //private void ToMyFilters(List<MyWorklistFilter> filters, Microsoft.Data.OData.Query.SemanticAst.SingleValueNode node)
        //{
        //    if (node is Microsoft.Data.OData.Query.SemanticAst.BinaryOperatorNode)
        //    {
        //        ToMyFilters(filters, node as Microsoft.Data.OData.Query.SemanticAst.BinaryOperatorNode);
        //    }
        //    else if (node is Microsoft.Data.OData.Query.SingleValueFunctionCallNode)
        //    {
        //        var filter = ToMyFilters(node as Microsoft.Data.OData.Query.SingleValueFunctionCallNode);
        //        if (filter != null)
        //        {
        //            filters.Add(filter);
        //        }
        //    }
        //    else if (node is Microsoft.Data.OData.Query.SemanticAst.ConvertNode)
        //    {

        //    }
        //}

        //private MyWorklistFilter ToMyFilters(
        //    Microsoft.Data.OData.Query.SingleValueFunctionCallNode node)
        //{
        //    if (node.Name == "substringof")
        //    {
        //        var filter = new MyWorklistFilter();
        //        filter.Operator = MyWorklistFilterOperator.Contains;
        //        var arguments = node.Arguments.ToList();
        //        var value = Extract(arguments[0] as Microsoft.Data.OData.Query.SemanticAst.ConstantNode);
        //        if (value != null)
        //        {
        //            filter.Value = value;
        //        }
        //        var field = arguments[1] as Microsoft.Data.OData.Query.SemanticAst.SingleValuePropertyAccessNode;
        //        if (field != null)
        //        {
        //            var prop = Extract(field.Property as Microsoft.Data.Edm.Library.EdmStructuralProperty);
        //            if (prop != null)
        //            {
        //                filter.Prop = prop;
        //            }
        //        }
        //        return filter;
        //    }

        //    return null;
        //}

        private string Extract(Microsoft.Data.Edm.Library.EdmStructuralProperty node)
        {
            if (node != null)
            {
                return node.Name;
            }
            return null;
        }

        private string Extract(Microsoft.Data.OData.Query.SemanticAst.ConstantNode node)
        {
            if (node != null)
            {
                var value = node.Value;
                return value.ToString();
            }
            return null;
        }

        private string Extract(Microsoft.Data.OData.Query.SemanticAst.SingleValuePropertyAccessNode node)
        {
            if (node != null)
            {
                return Extract(node.Property as Microsoft.Data.Edm.Library.EdmStructuralProperty);
            }
            return null;
        }

        private MyWorklistSorter ToMyWorklistSorter(Microsoft.Data.OData.Query.SemanticAst.OrderByClause orderByClause)
        {
            var sorter = new MyWorklistSorter();

            sorter.Direction = orderByClause.Direction;

            sorter.Prop = Extract(orderByClause.Expression as Microsoft.Data.OData.Query.SemanticAst.SingleValuePropertyAccessNode);

            return sorter;
        }

        private WorklistCriteria PrepareCriteria(WorklistCriteria worklistCriteria, ODataQueryOptions oDataQueryOptions)
        {
            List<MyWorklistFilter> filters = null;
            if (oDataQueryOptions != null && oDataQueryOptions.Filter != null)
            {
                filters = new List<MyWorklistFilter>();
               
                var filter = oDataQueryOptions.Filter.FilterClause;
              //  ToMyFilters(filters, filter.Expression);
              
            }

            worklistCriteria = PrepareCriteria(worklistCriteria, filters);

            MyWorklistSorter sorter = null;
            if (oDataQueryOptions != null && oDataQueryOptions.OrderBy != null)
            {
                sorter = ToMyWorklistSorter(oDataQueryOptions.OrderBy.OrderByClause);
            }

            worklistCriteria = PrepareCriteria(worklistCriteria, sorter);

            return worklistCriteria;
        }

        private WorklistCriteria PrepareCriteria(WorklistCriteria worklistCriteria,
            IList<MyWorklistFilter> filters)
        {
            if (filters != null && filters.Any())
            {
                foreach (var filter in filters)
                {
                    SourceCode.Workflow.Client.WCField? field = ToWCField(filter.Prop);
                    SourceCode.Workflow.Client.WCCompare? compare = null;
                    var value = filter.Value.Trim();

                    switch (filter.Operator)
                    {
                        case MyWorklistFilterOperator.Contains:
                            compare = WCCompare.Like;
                            value = "%" + value + "%";
                            break;
                        case MyWorklistFilterOperator.Equals:
                            compare = WCCompare.Equal;
                            break;
                        case MyWorklistFilterOperator.DoesNotContain:
                            compare = WCCompare.NotLike;
                            value = "%" + value + "%";
                            break;
                        case MyWorklistFilterOperator.NotEquals:
                            compare = WCCompare.NotEqual;
                            break;
                    }
                    if (field != null && compare != null && filter.Value != null)
                    {
                        worklistCriteria.AddFilterField(WCLogical.And, field.Value, compare.Value,
                            value);
                    }
                }
            }
            return worklistCriteria;
        }

        private WorklistCriteria PrepareCriteria(WorklistCriteria worklistCriteria,
            MyWorklistSorter sorter)
        {
            SourceCode.Workflow.Client.WCField? field = WCField.ActivityStartDate;
            SourceCode.Workflow.Client.WCSortOrder sortOrder = WCSortOrder.Descending;

            if (sorter != null)
            {
                field = ToWCField(sorter.Prop);

                switch (sorter.Direction)
                {
                    case Microsoft.Data.OData.Query.OrderByDirection.Ascending:
                        sortOrder = WCSortOrder.Ascending;
                        break;
                    case Microsoft.Data.OData.Query.OrderByDirection.Descending:
                        sortOrder = WCSortOrder.Descending;
                        break;
                }
            }

            if (field != null)
            {
                worklistCriteria.AddSortField(field.Value, sortOrder);
            }

            return worklistCriteria;
        }

        public object GetDataField(string serialNumber, string key)
        {
            var worklistCriteria = new WorklistCriteria();
            worklistCriteria.AddFilterField(WCField.SerialNumber, WCCompare.Equal,
                                                    serialNumber);
            var worklist = _workflowRepository.GetWorkList(worklistCriteria, 1, 1, false);
            if (worklist != null && worklist.Count > 0)
            {
                try
                {
                    return worklist[0].ProcessInstance.DataFields[key].Value;
                }
                catch (Exception)
                {
                    return null;
                }
            }
            return null;
        }

        public string ViewFlowUrl(string processInstanceId)
        {
            return _workflowRepository.ViewFlowUrl(processInstanceId);
        }


        public long StartK2Process(string processName, string folio, Dictionary<string, object> dataFields = null)
        {
            return _workflowRepository.StartK2Process(processName, folio, dataFields);
        }

        //public void Dispose()
        //{
        //    _workflowRepository.Dispose();
        //}
    }

        public static class WorklistItemExtensions
        {
            //    public static DateTime DueDate(this SourceCode.Workflow.Client.WorklistItem worklistItem, IWorkflowAdminRepository adminRepository)
            //    {
            //        return adminRepository.ZoneCalculateEvent(worklistItem.ActivityInstanceDestination.StartDate,
            //                                                  new TimeSpan(0, 0, 0,
            //                                                               worklistItem.ActivityInstanceDestination.
            //                                                                   ExpectedDuration));
            //        //return worklistItem.ActivityInstanceDestination.StartDate.AddSeconds(
            //        //        worklistItem.ActivityInstanceDestination.ExpectedDuration);
            //    }
            //    public static string CalculateDueStatus(this SourceCode.Workflow.Client.WorklistItem worklistItem, DateTime dueDate)
            //    {
            //        //Green: current time < start + expected duration
            //        //Yellow: start + expected duration <= current time <start + expected duration + 24 hours
            //        //Red: current time > start + expected duration + 24 hours

            //        // If no ExpectedDuration set, leave it as on-time
            //        //if (worklistItem.ActivityInstanceDestination.ExpectedDuration == 0)
            //        //    return "ontime";

            //        var now = DateTime.UtcNow;
            //        if (now < dueDate)
            //            return "ontime";
            //        else if (dueDate <= now && now < dueDate.AddHours(24))
            //            return "due";
            //        else
            //            return "overdue";
            //    }

            public static string GetTitle(this SourceCode.Workflow.Client.WorklistItem worklistItem)
            {
                try
                {
                    return worklistItem.ProcessInstance.DataFields[BaseWorkflowProcessModel.TITLE_DATAFIELD].Value.ToString();
                }
                catch (Exception)
                {
                    return "N/A";
                }
                //return worklistItem.ProcessInstance.Folio;
            }

            public static string ProcessName(this SourceCode.Workflow.Client.WorklistItem worklistItem)
            {
                return !string.IsNullOrEmpty(worklistItem.ProcessInstance.Description)
                           ? worklistItem.ProcessInstance.Description
                           : worklistItem.ProcessInstance.Name;
            }
            public static string ViewFlow(this SourceCode.Workflow.Client.WorklistItem worklistItem, string baseUrl)
            {
                return string.Format("{0}{1}", baseUrl, worklistItem.ProcessInstance.ID);
            }
            public static TimeSpan ExpectedDuration(this SourceCode.Workflow.Client.WorklistItem worklistItem)
            {
                var duration = worklistItem.EventInstance.ExpectedDuration != 0
                    ? worklistItem.EventInstance.ExpectedDuration
                    : worklistItem.ActivityInstanceDestination.ExpectedDuration;
                var expected = new TimeSpan(0, 0, duration);
                return expected;
            }
            //    public static string Module(this SourceCode.Workflow.Client.WorklistItem worklistItem)
            //    {
            //        var Module = worklistItem.ProcessInstance.Folder.Split('\\');
            //        var a = Module.Length;
            //        if (Module.Length > 1)
            //        {
            //            return Module[1];
            //        }
            //        else
            //        {
            //            return null;
            //        }
            //    }
        }

        internal sealed class MyWorklistFilter
        {
            public string Prop { get; set; }
            public MyWorklistFilterOperator Operator { get; set; }
            public string Value { get; set; }
        }

        internal enum MyWorklistFilterOperator
        {
            Equals = 1,
            NotEquals,
            Contains,
            DoesNotContain
        }

        internal sealed class MyWorklistSorter
        {
            public string Prop { get; set; }
            public Microsoft.Data.OData.Query.OrderByDirection Direction { get; set; }
        }
}