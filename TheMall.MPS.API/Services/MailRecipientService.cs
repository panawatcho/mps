﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CrossingSoft.Framework.Infrastructure.OData.Repositories;
using CrossingSoft.Framework.Services.OData;
using TheMall.MPS.Models.Enums;
using TheMall.MPS.Models.Mail;


namespace TheMall.MPS.API.Services
{
    public interface IMailRecipientService : IODataServiceAsync<MailRecipient>
    {
        MailRecipient GetMailRecipient(int? mailRecipientId);
        Task<MailRecipient> GetMailRecipientAsync(int? mailTemplateId);

        IReadOnlyList<MailRecipient> GetInformMailRecipients(string processCode, string activityName);
    }
    public class MailRecipientService : ODataServiceAsync<MailRecipient>, IMailRecipientService
    {
        private readonly IODataRepositoryAsync<MailRecipient> _repository;

        public MailRecipientService(IODataRepositoryAsync<MailRecipient> repository)
            : base(repository)
        {
            _repository = repository;
        }

        public MailRecipient GetMailRecipient(int? mailRecipientId)
        {
            if (mailRecipientId == null)
            {
                throw new ArgumentNullException("mailRecipientId");
            }
            return _repository.Find(m => m.Id == mailRecipientId);
        }

        public async Task<MailRecipient> GetMailRecipientAsync(int? mailRecipientId)
        {
            if (mailRecipientId == null)
            {
                throw new ArgumentNullException("mailRecipientId");
            }
            return await _repository.FindAsync(m => m.Id == mailRecipientId);
        }

        public IReadOnlyList<MailRecipient> GetInformMailRecipients(string processName, string activityName)
        {
            if (string.IsNullOrEmpty(processName))
            {
                throw new ArgumentNullException("processName");
            }
            if (string.IsNullOrEmpty(activityName))
            {
                throw new ArgumentNullException("activityName");
            }
            return _repository.Query(m => !string.IsNullOrEmpty(m.ProcessName) && m.ProcessName.Equals(processName, StringComparison.InvariantCultureIgnoreCase)
                    && m.MailType == MailType.Inform
                    && !string.IsNullOrEmpty(m.ActivityName) && m.ActivityName.Equals(activityName, StringComparison.InvariantCultureIgnoreCase)).Select().ToListSafe();
        }
    }
}