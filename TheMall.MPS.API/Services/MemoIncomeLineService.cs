﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using CrossingSoft.Framework.Infrastructure.OData.Repositories;
using CrossingSoft.Framework.Services.OData;
using SourceCode.Workflow.Client;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Infrastructure.EntityFramework;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Models.MemoIncome;

namespace TheMall.MPS.API.Services
{
    public interface IMemoIncomeLineService : Interfaces.IODataService<MPS_MemoIncomeLine>
    {
    }
    public class MemoIncomeLineService : BaseODataService<MPS_MemoIncomeLine>, IMemoIncomeLineService
    {
        public MemoIncomeLineService(IODataRepositoryAsync<MPS_MemoIncomeLine> repository) 
            : base(repository)
        {
        }
    }
}