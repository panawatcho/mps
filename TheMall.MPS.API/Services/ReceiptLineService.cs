﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using CrossingSoft.Framework.Infrastructure.OData.Repositories;
using LinqKit;
using TheMall.MPS.API.Services;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Infrastructure.EntityFramework;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Models.Receipt;

namespace TheMall.MPS.API.Services
{
    public interface IReceiptLineService : IBaseMasterService<MPS_ReceiptLine>
    {

    }
    public class ReceiptLineService : BaseMasterService<MPS_ReceiptLine>, IReceiptLineService
    {
        public ReceiptLineService(IODataRepositoryAsync<IMPSDbContext, 
            IMPSSession, 
            MPS_ReceiptLine> repository)
            : base(repository)
        {

        }
        public override MPS_ReceiptLine DoGet(string code)
        {
            return Get(code);
        }

        public override Task<MPS_ReceiptLine> DoGetAsync(string code)
        {
            return GetAsync(code);
        }
    }
}