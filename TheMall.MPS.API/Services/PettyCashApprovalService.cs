﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using CrossingSoft.Framework.Infrastructure.OData.Repositories;
using CrossingSoft.Framework.Services.OData;
using SourceCode.Workflow.Client;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Infrastructure.EntityFramework;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Models.Proposal;
using TheMall.MPS.Models.PettyCash;

namespace TheMall.MPS.API.Services
{
    public interface IPettyCashApprovalService : Interfaces.IODataService<MPS_PettyCashApproval>
    {
    }
    public class PettyCashApprovalService : BaseODataService<MPS_PettyCashApproval>, IPettyCashApprovalService
    {
        public PettyCashApprovalService(IODataRepositoryAsync<MPS_PettyCashApproval> repository)
            : base(repository)
        {
        }
    }
}