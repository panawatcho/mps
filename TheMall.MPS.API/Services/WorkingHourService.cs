﻿using System;
using System.Collections.Generic;
using System.Linq;
using TheMall.MPS.API.Helpers;
using TheMall.MPS.Infrastructure;
using TheMall.MPS.Infrastructure.EntityFramework;
using TheMall.MPS.Models.K2;


namespace TheMall.MPS.API.Services
{
    public interface IWorkingHourService
    {
        DateTime CalcDue(DateTime startDateTime, double secondRequested);
        DateTime CalcDue(DateTime startDateTime, int secondRequested);
        DateTime CalcDue(DateTime startDateTime, TimeSpan durationRequested);
        DateTime CalcDue(string zoneName, DateTime startDateTime, int durationRequested);
        DateTime CalcDue(string zoneName, DateTime startDateTime, TimeSpan durationRequested);

        WorklistDueStatus CalcDueStatus(DateTime today, DateTime startDateTime, double secondRequested,
            int dueThreshold = Infrastructure.Constants.DUE_DURATION_SECONDS);
        WorklistDueStatus CalcDueStatus(DateTime today, DateTime startDateTime, int secondRequested,
            int dueThreshold = Infrastructure.Constants.DUE_DURATION_SECONDS);
        WorklistDueStatus CalcDueStatus(DateTime today, DateTime startDateTime, TimeSpan durationRequested,
            int dueThreshold = Infrastructure.Constants.DUE_DURATION_SECONDS);
        WorklistDueStatus CalcDueStatus(AvailabilityZone zone, DateTime today, DateTime startDate, TimeSpan durationRequested,
            int dueThreshold = Infrastructure.Constants.DUE_DURATION_SECONDS);

        WorklistDueStatus CalcDueStatusNoZone(DateTime due, DateTime today, int dueThreshold = Infrastructure.Constants.DUE_DURATION_SECONDS);

        WorklistDueStatus CalcDueStatusNoZone(TimeSpan durationDifferent,
            int dueThreshold = Infrastructure.Constants.DUE_DURATION_SECONDS);
        
        bool IsWorkingDay(DateTime date, IReadOnlyList<AvailabilityDate> holidays,
            IReadOnlyList<AvailabilityHour> workingHours);

        double TotalWorkingSecond(DateTime pointA, DateTime pointB);
        double TotalWorkingSecond(AvailabilityZone zone, DateTime pointA, DateTime pointB);
        double TotalWorkingSecond(AvailabilityZone zone, IReadOnlyList<AvailabilityDate> holidays, IReadOnlyList<AvailabilityHour> workingHours, DateTime pointA, DateTime pointB);
    }
    public class WorkingHourService : IWorkingHourService
    {
        private readonly IK2DbContext _context;

        public WorkingHourService(IK2DbContext context)
        {
            _context = context;
        }

        private AvailabilityZone GetDefaultZone()
        {
            return Caching.GetFromCache("K2DefaultZone",
                () =>
                {
                    var list =
                    _context.AvailabilityZones.Where(m => m.DefaultZone.HasValue && m.DefaultZone.Value)
                        .ToReadOnlyListSafe();

                    return list.FirstOrDefault();
                });
        }

        private IReadOnlyList<AvailabilityHour> GetWorkingHours(Guid zone)
        {
            return Caching.GetFromCache("K2Zone_WorkingHours_" + zone,
                () =>
                {
                    var list = _context.AvailabilityHours.Where(m => m.ZoneID == zone).OrderBy(m => m.DayOfWeek).ToReadOnlyListSafe();

                    return list;
                });
        }

        private IReadOnlyList<AvailabilityDate> GetHolidays(Guid zone)
        {
            return Caching.GetFromCache("K2Zone_Holidays_" + zone,
                () =>
                {
                    var list = _context.AvailabilityDates.Where(
                    m => m.ZoneID == zone && m.IsNonWorkDate.HasValue && m.IsNonWorkDate.Value).ToListSafe();

                    return list;
                });
        }

        private DateTime ConvertUtcToLocal(AvailabilityZone zone, DateTime dateTime)
        {
            if (dateTime.Kind == DateTimeKind.Utc && zone != null)
            {
                var tmp = dateTime.AddHours(zone.TimeZoneOffset);
                return new DateTime(tmp.Year, tmp.Month, tmp.Day, tmp.Hour, tmp.Minute, tmp.Second, DateTimeKind.Local);
            }
            return dateTime;
        }

        private DateTime ConvertLocalToUtc(AvailabilityZone zone, DateTime dateTime)
        {
            if (dateTime.Kind == DateTimeKind.Local && zone != null)
            {
                var tmp = dateTime.AddHours(zone.TimeZoneOffset * -1);
                return new DateTime(tmp.Year, tmp.Month, tmp.Day, tmp.Hour, tmp.Minute, tmp.Second, DateTimeKind.Utc);
            }
            return dateTime;
        }

        public DateTime CalcDue(DateTime startDateTime, TimeSpan durationRequested)
        {
            return CalcDue(GetDefaultZone(), startDateTime, durationRequested);
        }

        public DateTime CalcDue(DateTime startDateTime, int secondRequested)
        {
            return CalcDue(GetDefaultZone(), startDateTime, new TimeSpan(0, 0, secondRequested));
        }

        public DateTime CalcDue(DateTime startDateTime, double secondRequested)
        {
            return CalcDue(startDateTime, Convert.ToInt32(Math.Round(secondRequested)));
        }

        public DateTime CalcDue(string zoneName, DateTime startDateTime, int secondRequested)
        {
            return CalcDue(_context.AvailabilityZones.FirstOrDefault(m => m.ZoneName == zoneName), startDateTime, new TimeSpan(0, 0, secondRequested));
        }

        public DateTime CalcDue(string zoneName, DateTime startDateTime, TimeSpan durationRequested)
        {
            return CalcDue(_context.AvailabilityZones.FirstOrDefault(m => m.ZoneName == zoneName), startDateTime, durationRequested);
        }

        private DateTime CalcDue(AvailabilityZone zone, DateTime startDateTime, TimeSpan durationRequested)
        {
            if (zone == null)
            {
                return startDateTime.Add(durationRequested);
            }

            var isUtc = startDateTime.Kind == DateTimeKind.Utc;
            startDateTime = ConvertUtcToLocal(zone, startDateTime);

            var secondLeft = durationRequested.TotalSeconds;
            var ongoingDateTime = startDateTime;

            var holidays = GetHolidays(zone.ZoneID);

            var workingHours = GetWorkingHours(zone.ZoneID);

            while (secondLeft > 0)
            {
                if (!IsWorkingDay(ongoingDateTime, holidays, workingHours))
                {
                    ongoingDateTime = BeginningOfTomorrow(ongoingDateTime);
                    continue;
                }
                var dow = (int)ongoingDateTime.DayOfWeek;
                var periods = workingHours.Where(m => m.DayOfWeek == dow).OrderBy(m => m.TimeOfDay);

                foreach (var period in periods)
                {
                    var periodStart = period.TimeOfDay;
                    var periodEnd = period.TimeOfDay.AddSeconds(period.Duration);
                    var startOfPeriod = new DateTime(ongoingDateTime.Year, ongoingDateTime.Month, ongoingDateTime.Day, periodStart.Hour, periodStart.Minute, periodStart.Second, DateTimeKind.Local);
                    var endOfPeriod = new DateTime(ongoingDateTime.Year, ongoingDateTime.Month, ongoingDateTime.Day, periodEnd.Hour, periodEnd.Minute, periodEnd.Second, DateTimeKind.Local);

                    if (ongoingDateTime < startOfPeriod && ongoingDateTime < endOfPeriod) // Time is before working hour
                    {
                        ongoingDateTime = ongoingDateTime.AddSeconds((startOfPeriod - ongoingDateTime).TotalSeconds);
                    }
                    if (startOfPeriod <= ongoingDateTime && endOfPeriod > ongoingDateTime) // Fall in this period
                    {
                        // Calculate number of second we can substract off.
                        var secondOff = (endOfPeriod - ongoingDateTime).TotalSeconds;
                        if (secondOff > secondLeft)
                        {
                            secondOff = secondLeft;
                        }
                        secondLeft -= secondOff;
                        ongoingDateTime = ongoingDateTime.AddSeconds(secondOff);
                    }
                    if (ongoingDateTime > startOfPeriod && ongoingDateTime >= endOfPeriod && secondLeft > 0) // Time is well passed working hours.
                    {
                        var lastPeriodOfToday = periods.Last();
                        var lastPeriodEnd = lastPeriodOfToday.TimeOfDay.AddSeconds(lastPeriodOfToday.Duration);
                        var endOfLastPeriod = new DateTime(ongoingDateTime.Year, ongoingDateTime.Month, ongoingDateTime.Day, lastPeriodEnd.Hour, lastPeriodEnd.Minute, lastPeriodEnd.Second, DateTimeKind.Local);
                        if (ongoingDateTime >= endOfLastPeriod)
                        {
                            ongoingDateTime = BeginningOfTomorrow(ongoingDateTime);
                            break;
                        }
                    }
                    if (secondLeft <= 0)
                    {
                        break;
                    }
                }
            } 
            if (isUtc)
            {
                return ConvertLocalToUtc(zone, ongoingDateTime);
            }
            return ongoingDateTime;
        }

        public WorklistDueStatus CalcDueStatus(DateTime today, DateTime startDateTime, double secondRequested,
            int dueThreshold = Constants.DUE_DURATION_SECONDS)
        {
            return CalcDueStatus(GetDefaultZone(), today, startDateTime, new TimeSpan(0, 0, Convert.ToInt32(Math.Round(secondRequested))), dueThreshold);
        }

        public WorklistDueStatus CalcDueStatus(DateTime today, DateTime startDateTime, int secondRequested,
            int dueThreshold = Constants.DUE_DURATION_SECONDS)
        {
            return CalcDueStatus(GetDefaultZone(), today, startDateTime, new TimeSpan(0, 0, secondRequested), dueThreshold);
        }

        public WorklistDueStatus CalcDueStatus(DateTime today, DateTime startDateTime, TimeSpan durationRequested,
            int dueThreshold = Constants.DUE_DURATION_SECONDS)
        {
            return CalcDueStatus(GetDefaultZone(), today, startDateTime, durationRequested, dueThreshold);
        }

        public WorklistDueStatus CalcDueStatus(AvailabilityZone zone, DateTime today, DateTime startDate, TimeSpan durationRequested, int dueThreshold = Infrastructure.Constants.DUE_DURATION_SECONDS)
        {
            if (Math.Abs(durationRequested.TotalSeconds) < 1)
            {
                // Duration not set.
                return WorklistDueStatus.Due;
            }
            var isUtc = startDate.Kind == DateTimeKind.Utc;
            if (isUtc)
            {
                var tmp = startDate.AddHours(zone.TimeZoneOffset);
                today = new DateTime(tmp.Year, tmp.Month, tmp.Day, tmp.Hour, tmp.Minute, tmp.Second, DateTimeKind.Local);
            }

            var due = CalcDue(zone, startDate, durationRequested);

            if (zone == null)
            {
                return CalcDueStatusNoZone(due, today, dueThreshold);
            }

            var holidays = GetHolidays(zone.ZoneID);
            var workingHours = GetWorkingHours(zone.ZoneID);

            var durationBetweenDueAndToday = due - today;

            // Facts !
            // F1 - due will always be a working day according to zone

            // Then the first condition is still intact becase of (F1)
            if (due.Date == today.Date && durationBetweenDueAndToday.TotalSeconds >= 0) // 1. is within same day.
            {
                return WorklistDueStatus.Due;
            }

            // Find startDate's working hour within 24 hours
            var workingSecond = TotalWorkingSecond(zone, holidays, workingHours, startDate, startDate.AddSeconds(dueThreshold));

            // if duration is less than a day's working hour then it's Due or Late
            if (durationRequested.TotalSeconds <= workingSecond)
            {
                if (durationBetweenDueAndToday.TotalSeconds >= 0)
                {
                    return WorklistDueStatus.Due;
                }
                return WorklistDueStatus.Late;
            }

            // F2 - duration will always longer than a working day

            if (durationBetweenDueAndToday.TotalSeconds >= 0) // 2. Early, but is it within 24 hours?
            {
                // Let's see the actual due start
                DateTime supposedDueStart = due.AddSeconds(-dueThreshold);

                while (!IsWorkingDay(supposedDueStart, holidays, workingHours))
                {
                    supposedDueStart = supposedDueStart.AddSeconds(-dueThreshold);
                }

                if (durationBetweenDueAndToday.TotalSeconds > (due - supposedDueStart).TotalSeconds)
                {
                    return WorklistDueStatus.Early;
                }
                return WorklistDueStatus.Due;
            }
            return WorklistDueStatus.Late;

            //// Or if duration is less than or equals to working hours in a day
            //    //|| Math.Abs(durationRequested.TotalSeconds) <= dueThreshold
            //var dueDateTime = Calc(zone, startDateTime, durationRequested);
            //if (zone == null)
            //{
            //    return CalcDueStatusNoZone(dueDateTime - today, dueThreshold);
            //}

            //var holidays = GetHolidays(zone.ZoneID);

            //var workingHours = GetWorkingHours(zone.ZoneID);

            ////DateTime supposedDueStart = dueDateTime.AddSeconds(-dueThreshold);
            //DateTime supposedDueStart = dueDateTime.AddSeconds(-dueThreshold);

            //while (!IsWorkingDay(supposedDueStart, holidays, workingHours))
            //{
            //    supposedDueStart = supposedDueStart.AddSeconds(-dueThreshold);
            //}
            //// from due start to today
            //var durationDifferent = supposedDueStart - today;
            //// if duration is less than 0, that means today
            //// then check if it's more than 24 hours (dueThreshold) to see if it's Late or Due
            //return durationDifferent.TotalSeconds <= 0
            //    ? Math.Abs(durationDifferent.TotalSeconds) > Math.Abs(dueThreshold) ? WorklistDueStatus.Late : WorklistDueStatus.Due
            //    : WorklistDueStatus.Early;
        }

        public WorklistDueStatus CalcDueStatusNoZone(DateTime due, DateTime today,
            int dueThreshold = Infrastructure.Constants.DUE_DURATION_SECONDS)
        {
            var durationDifferent = due - today;
            if (due.Date == today.Date && durationDifferent.TotalSeconds >= 0) // 1. is within same day.
            {
                return WorklistDueStatus.Due;
            }

            return CalcDueStatusNoZone(durationDifferent);
        }

        public WorklistDueStatus CalcDueStatusNoZone(TimeSpan durationDifferent, int dueThreshold = Infrastructure.Constants.DUE_DURATION_SECONDS)
        {
            if (durationDifferent.TotalSeconds >= 0) // 2. Early, but is it within 24 hours?
            {
                if (durationDifferent.TotalSeconds > dueThreshold)
                {
                    return WorklistDueStatus.Early;
                }
                return WorklistDueStatus.Due;
            }

            return WorklistDueStatus.Late; // 3. Late case.
        }

        public bool IsWorkingDay(DateTime date, IReadOnlyList<AvailabilityDate> holidays, IReadOnlyList<AvailabilityHour> workingHours)
        {
            if (holidays == null)
            {
                holidays = new List<AvailabilityDate>();
            }
            if (workingHours == null)
            {
                workingHours = new List<AvailabilityHour>();
            }
            var dow = (int)date.DayOfWeek;

            if (workingHours.All(m => m.DayOfWeek != dow))
                return false;

            if (holidays.Any(m => m.TimeOfDay.Date == date.Date))
                return false;

            return true;
        }

        public double TotalWorkingSecond(DateTime pointA, DateTime pointB)
        {
            return TotalWorkingSecond(GetDefaultZone(), pointA, pointB);
        }

        public double TotalWorkingSecond(AvailabilityZone zone, DateTime pointA, DateTime pointB)
        {
            return TotalWorkingSecond(zone, GetHolidays(zone.ZoneID), GetWorkingHours(zone.ZoneID), pointA, pointB);
        }

        public double TotalWorkingSecond(AvailabilityZone zone, IReadOnlyList<AvailabilityDate> holidays, IReadOnlyList<AvailabilityHour> workingHours, DateTime pointA, DateTime pointB)
        {
            var ongoingDateTime = pointA;
            double totalSecond = 0;

            while (ongoingDateTime < pointB)
            {
                if (!IsWorkingDay(ongoingDateTime, holidays, workingHours))
                {
                    ongoingDateTime = BeginningOfTomorrow(ongoingDateTime);
                    continue;
                }
                var dow = (int)ongoingDateTime.DayOfWeek;
                var periods = workingHours.Where(m => m.DayOfWeek == dow).OrderBy(m => m.TimeOfDay);

                foreach (var period in periods)
                {
                    var periodStart = period.TimeOfDay;
                    var periodEnd = period.TimeOfDay.AddSeconds(period.Duration);
                    var startOfPeriod = new DateTime(ongoingDateTime.Year, ongoingDateTime.Month, ongoingDateTime.Day, periodStart.Hour, periodStart.Minute, periodStart.Second, DateTimeKind.Local);
                    var endOfPeriod = new DateTime(ongoingDateTime.Year, ongoingDateTime.Month, ongoingDateTime.Day, periodEnd.Hour, periodEnd.Minute, periodEnd.Second, DateTimeKind.Local);

                    if (ongoingDateTime < startOfPeriod && ongoingDateTime < endOfPeriod) // Time is before working hour
                    {
                        ongoingDateTime = ongoingDateTime.AddSeconds((startOfPeriod - ongoingDateTime).TotalSeconds);
                    }
                    if (startOfPeriod <= ongoingDateTime && endOfPeriod > ongoingDateTime && ongoingDateTime < pointB) // Fall in this period
                    {
                        // Calculate number of second we can substract off.
                        if (endOfPeriod <= pointB)
                        {
                            var secondOff = (endOfPeriod - ongoingDateTime).TotalSeconds;
                            totalSecond += secondOff;
                            ongoingDateTime = ongoingDateTime.AddSeconds(secondOff);
                        }
                        else
                        {
                            var secondOff = (pointB - ongoingDateTime).TotalSeconds;
                            totalSecond += secondOff;
                            ongoingDateTime = ongoingDateTime.AddSeconds(secondOff);
                        }
                    }
                    if (ongoingDateTime > startOfPeriod && ongoingDateTime >= endOfPeriod && ongoingDateTime < pointB) // Time is well passed working hours.
                    {
                        var lastPeriodOfToday = periods.Last();
                        var lastPeriodEnd = lastPeriodOfToday.TimeOfDay.AddSeconds(lastPeriodOfToday.Duration);
                        var endOfLastPeriod = new DateTime(ongoingDateTime.Year, ongoingDateTime.Month, ongoingDateTime.Day, lastPeriodEnd.Hour, lastPeriodEnd.Minute, lastPeriodEnd.Second, DateTimeKind.Local);
                        if (ongoingDateTime >= endOfLastPeriod)
                        {
                            ongoingDateTime = BeginningOfTomorrow(ongoingDateTime);
                            break;
                        }
                    }
                }
            }

            return totalSecond;
        }

        private DateTime BeginningOfTomorrow(DateTime dateTime)
        {
            var tmp = dateTime.AddDays(1);
            return new DateTime(tmp.Year, tmp.Month, tmp.Day, 0, 0, 0, tmp.Kind);
        }
    }

    public enum WorklistDueStatus
    {
        Early = 0,
        Due = 1,
        Late = 2
    }
}