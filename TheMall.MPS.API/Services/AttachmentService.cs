﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using CrossingSoft.Framework.Infrastructure.OData.Repositories;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Infrastructure.EntityFramework;
using TheMall.MPS.Infrastructure.Repositories;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Models.Abstracts;
using TheMall.MPS.Models.Interfaces;
using TheMall.MPS.Resources;
using TheMall.MPS.ViewModels;

namespace TheMall.MPS.API.Services
{
    public interface IAttachmentService<TAttachmentTable> : IODataService<TAttachmentTable>
        where TAttachmentTable : IAttachmentTable, new()
    {
        IEnumerable<TAttachmentTable> GetAttachments(int parentId);
        Task<IEnumerable<TAttachmentTable>> GetAttachmentsAsync(int parentId);
        IEnumerable<TAttachmentTable> GetAttachments(int parentId, string fileName);
        IEnumerable<TAttachmentTable> GetDocTypeFiles(string documentNumber, int parentId, string fileName = null, params int[] docTypeIds);
        Task<IEnumerable<TAttachmentTable>> GetDocTypeFilesAsync(int parentId, string fileName = null, params int[] docTypeIds);

        IEnumerable<TAttachmentTable> GetAttachments(string documentNumber);
        Task<IEnumerable<TAttachmentTable>> GetAttachmentsAsync(string documentNumber);
        IEnumerable<TAttachmentTable> GetAttachments(string documentNumber, string fileName);
        IEnumerable<TAttachmentTable> GetDocTypeFiles(string documentNumber, string fileName = null, params int[] docTypeIds);
        Task<IEnumerable<TAttachmentTable>> GetDocTypeFilesAsync(string documentNumber, string fileName = null, params int[] docTypeIds);

        TAttachmentTable NewAttachmentTable<TPostedAttachment>(
            CreatedFileInfo<TPostedAttachment> createdFileInfo)
            where TPostedAttachment : IPostedAttachmentViewModel;

        bool ValidateFileExtension<TPostedAttachment>(TPostedAttachment viewModel)
            where TPostedAttachment : IPostedAttachmentViewModel;

        Task<TAttachmentTable> SaveAttachmentAsync<TPostedAttachment>(HttpContentHeaders headers, string contentType,
            Stream stream, string username, TPostedAttachment viewModel)
            where TPostedAttachment : IPostedAttachmentViewModel;

        Task<IEnumerable<TAttachmentTable>> RemoveAttachments(int parentId, int? documentType, string sn, string documentNumber = null,
            params string[] fileNames);

        //NetworkCredential AdminNetworkCredential();
        Task<StreamContent> GetStreamContent(TAttachmentTable attachment);
        Task<IEnumerable<TAttachmentTable>> GetAttachmentsWithSNAsync(int parentId);
    }

    public interface IAttachmentService<TAttachmentTable, TWorkflowTable> : IAttachmentService<TAttachmentTable>
        where TAttachmentTable : BaseAttachmentTable, new()
        where TWorkflowTable : TheMall.MPS.Models.Interfaces.IMpsWorkflowTable
    {
    }

    //internal static class Helper
    //{
    //    private static readonly Infrastructure.LimitedConcurrencyLevelTaskScheduler Lcts = new Infrastructure.LimitedConcurrencyLevelTaskScheduler(1);
    //    public static TaskFactory Factory = new TaskFactory(Lcts);

    //}
    public class AttachmentService<TAttachmentTable> : BaseODataService<TAttachmentTable>,
        IAttachmentService<TAttachmentTable>
        where TAttachmentTable : BaseAttachmentTable, new()
    {
        public AttachmentService(IODataRepositoryAsync<IMPSDbContext, IMPSSession, TAttachmentTable> repository)
            : base(repository)
        {
        }

        public IEnumerable<TAttachmentTable> GetAttachments(int parentId)
        {
            IEnumerable<TAttachmentTable> attachments = (GetWhere(m => m.ParentId == parentId && m.DocumentTypeId == null
                    && m.SerialNumber == null)).ToListSafe()
                        .OrderBy(m => m.CreatedDate);
            return attachments;
        }

        public async Task<IEnumerable<TAttachmentTable>> GetAttachmentsAsync(int parentId)
        {
            //IEnumerable<TAttachmentTable> attachments = (await GetWhereAsync(m => m.ParentId == parentId && m.DocumentTypeId == null
            //       && m.SerialNumber == null)).ToListSafe()
            //           .OrderBy(m => m.CreatedDate);
            IEnumerable<TAttachmentTable> attachments = (await GetWhereAsync(m => m.ParentId == parentId && m.DocumentTypeId == null
                   )).ToListSafe()
                       .OrderBy(m => m.CreatedDate);
            return attachments;
        }

        public IEnumerable<TAttachmentTable> GetAttachments(int parentId, string fileName)
        {
            IEnumerable<TAttachmentTable> attachments = (GetWhere(m => m.ParentId == parentId && m.DocumentTypeId == null
                //&& m.SerialNumber == null
                       && !string.IsNullOrEmpty(m.FileName)
                       && m.FileName.Equals(fileName, StringComparison.InvariantCultureIgnoreCase)
                       )).ToListSafe()
                           .OrderBy(m => m.CreatedDate);
            return attachments;
        }

        public IEnumerable<TAttachmentTable> GetDocTypeFiles(string documentNumber, int parentId, string fileName = null, params int[] docTypeIds)
        {
            IEnumerable<TAttachmentTable> attachments = null;
            if (docTypeIds == null || !docTypeIds.Any())
            {
                var tmp = GetWhere(m => m.ParentId == parentId && m.DocumentTypeId != null);
                attachments = tmp.ToListSafe().OrderBy(m => m.CreatedDate);
            }
            else
            {
                if (string.IsNullOrEmpty(fileName))
                {
                    var tmp = GetWhere(m => m.ParentId == parentId && m.DocumentTypeId != null && docTypeIds.Contains(m.DocumentTypeId.Value));
                    attachments = tmp.ToListSafe().OrderBy(m => m.CreatedDate);
                }
                else
                {
                    var tmp = GetWhere(m => m.ParentId == parentId && m.DocumentTypeId != null && docTypeIds.Contains(m.DocumentTypeId.Value) && m.FileName == fileName);
                    attachments = tmp.ToListSafe().OrderBy(m => m.CreatedDate);
                }
            }
            return attachments;
        }

        public IEnumerable<TAttachmentTable> GetAttachments(string documentNumber)
        {
            IEnumerable<TAttachmentTable> attachments = (GetWhere(m => m.DocumentNumber == documentNumber && m.DocumentTypeId == null
                    && m.SerialNumber == null)).ToListSafe()
                        .OrderBy(m => m.CreatedDate);
            return attachments;
        }

        public async Task<IEnumerable<TAttachmentTable>> GetAttachmentsAsync(string documentNumber)
        {
            IEnumerable<TAttachmentTable> attachments = (await GetWhereAsync(m => m.DocumentNumber == documentNumber && m.DocumentTypeId == null
                    && m.SerialNumber == null)).ToListSafe()
                        .OrderBy(m => m.CreatedDate);
            return attachments;
        }

        public IEnumerable<TAttachmentTable> GetAttachments(string documentNumber, string fileName)
        {
            IEnumerable<TAttachmentTable> attachments = (GetWhere(m => m.DocumentNumber == documentNumber && m.DocumentTypeId == null
                       && m.SerialNumber == null
                          && !string.IsNullOrEmpty(m.FileName)
                          && m.FileName.Equals(fileName, StringComparison.InvariantCultureIgnoreCase))).ToListSafe()
                           .OrderBy(m => m.CreatedDate);
            return attachments;
        }

        public IEnumerable<TAttachmentTable> GetDocTypeFiles(string documentNumber, string fileName = null, params int[] docTypeIds)
        {
            IEnumerable<TAttachmentTable> attachments = null;
            if (docTypeIds == null || !docTypeIds.Any())
            {
                var tmp = GetWhere(m => m.DocumentNumber == documentNumber && m.DocumentTypeId != null);
                attachments = tmp.ToListSafe().OrderBy(m => m.CreatedDate);
            }
            else
            {
                if (string.IsNullOrEmpty(fileName))
                {
                    var tmp = GetWhere(m => m.DocumentNumber == documentNumber && m.DocumentTypeId != null && docTypeIds.Contains(m.DocumentTypeId.Value));
                    attachments = tmp.ToListSafe().OrderBy(m => m.CreatedDate);
                }
                else
                {
                    var tmp = GetWhere(m => m.DocumentNumber == documentNumber && m.DocumentTypeId != null && docTypeIds.Contains(m.DocumentTypeId.Value) && m.FileName == fileName);
                    attachments = tmp.ToListSafe().OrderBy(m => m.CreatedDate);
                }
            }
            return attachments;
        }

        public async Task<IEnumerable<TAttachmentTable>> GetDocTypeFilesAsync(string documentNumber, string fileName = null, params int[] docTypeIds)
        {
            IEnumerable<TAttachmentTable> attachments = null;
            if (docTypeIds == null || !docTypeIds.Any())
            {
                var tmp = await GetWhereAsync(m => m.DocumentNumber == documentNumber && m.DocumentTypeId != null);
                attachments = tmp.ToListSafe().OrderBy(m => m.CreatedDate);
            }
            else
            {
                if (string.IsNullOrEmpty(fileName))
                {
                    var tmp = await GetWhereAsync(m => m.DocumentNumber == documentNumber && m.DocumentTypeId != null && docTypeIds.Contains(m.DocumentTypeId.Value));
                    attachments = tmp.ToListSafe().OrderBy(m => m.CreatedDate);
                }
                else
                {
                    var tmp = await GetWhereAsync(m => m.DocumentNumber == documentNumber && m.DocumentTypeId != null && docTypeIds.Contains(m.DocumentTypeId.Value) && m.FileName == fileName);
                    attachments = tmp.ToListSafe().OrderBy(m => m.CreatedDate);
                }
            }
            return attachments;
        }

        public async Task<IEnumerable<TAttachmentTable>> GetDocTypeFilesAsync(int parentId, string fileName = null, params int[] docTypeIds)
        {
            IEnumerable<TAttachmentTable> attachments = null;
            if (docTypeIds == null || !docTypeIds.Any())
            {
                var tmp = await GetWhereAsync(m => m.ParentId == parentId && m.DocumentTypeId != null);
                attachments = tmp.ToListSafe().OrderBy(m => m.CreatedDate);
            }
            else
            {
                if (string.IsNullOrEmpty(fileName))
                {
                    var tmp = await GetWhereAsync(m => m.ParentId == parentId && m.DocumentTypeId != null && docTypeIds.Contains(m.DocumentTypeId.Value));
                    attachments = tmp.ToListSafe().OrderBy(m => m.CreatedDate);
                }
                else
                {
                    var tmp = await GetWhereAsync(m => m.ParentId == parentId && m.DocumentTypeId != null && docTypeIds.Contains(m.DocumentTypeId.Value) && m.FileName == fileName);
                    attachments = tmp.ToListSafe().OrderBy(m => m.CreatedDate);
                }
            }
            return attachments;
        }

        public bool ValidateFileExtension<TPostedAttachment>(TPostedAttachment viewModel)
            where TPostedAttachment : IPostedAttachmentViewModel
        {
            var fileName = viewModel.Name;
            if (!string.IsNullOrEmpty(fileName))
            {
                if (Regex.IsMatch(Path.GetFileNameWithoutExtension(fileName), "^[^%~#&*{}\\\\:<>?/+|\"\\.]+$",
                    RegexOptions.IgnoreCase))
                {
                    var x = Path.GetExtension(fileName);
                    var regFormat = ConfigurationManager.AppSettings["FileUploadChecker"].ToString();
                    if (Regex.IsMatch(x, regFormat,
                        RegexOptions.IgnoreCase))
                    {
                        return true;
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(x))
                            throw new Exception("Invalid file type. " + x.ToUpper() + " is not allowed to be uploaded.");
                        throw new Exception("Invalid file type. Empty extension file is not allowed to be uploaded.");
                    }
                }
                else
                {
                    throw new Exception("Invalid file name. ~ % # & * { } \\ : < > / + | \" . are not allowed.");
                }
            }
            else
            {
                throw new Exception("File name cannot be empty.");
            }
        }

        public async virtual Task<TAttachmentTable> SaveAttachmentAsync<TPostedAttachment>(
            HttpContentHeaders headers,
            string contentType,
            Stream stream,
            string username,
            TPostedAttachment viewModel)
            where TPostedAttachment : IPostedAttachmentViewModel
        {
            var id = viewModel.Id ?? 0;
            var documentType = viewModel.DocumentType;

            var root = AppSettings.Instance.AttachmentRootPath;
            if (root.StartsWith("~"))
            {
                root = System.Web.Hosting.HostingEnvironment.MapPath(root);
            }
            var folder = typeof(TAttachmentTable).Name;
            string pathString = Path.Combine(root, folder, id.ToString(), documentType.HasValue ? documentType.ToString() : "Others", Guid.NewGuid().ToString());

           
            if (!Directory.Exists(pathString))
            {
                Directory.CreateDirectory(pathString);
            }
            string fileName = viewModel.Name;
            pathString = Path.Combine(pathString, fileName);
            if (!File.Exists(pathString))
            {
                using (FileStream destinationStream = File.Create(pathString))
                {
                    await stream.CopyToAsync(destinationStream);
                }
                long fileLength = headers.ContentLength ?? 0;

                var attachment = NewAttachmentTable(
                    new CreatedFileInfo<TPostedAttachment>(
                        viewModel,
                        username,
                        fileLength,
                        contentType,
                        pathString
                        )
                    );

                attachment = Insert(attachment);

                return attachment;

            }

            throw new Exception(Error.UnknownError);
        }

        public async Task<IEnumerable<TAttachmentTable>> RemoveAttachments(int parentId, int? documentType, string sn, string documentNumber = null, params string[] fileNames)
        {
            var attachmentDelete = new List<TAttachmentTable>();
            foreach (var fileName in fileNames)
            {
                var name = fileName;
                TAttachmentTable attachment = null;
                if (parentId != 0)
                {
                    attachment =
                        this.Load(
                            m => m.ParentId == parentId && m.DocumentTypeId == documentType && m.FileName == name);
                }
                else
                {
                    attachment = this.Load(m => m.DocumentNumber == documentNumber && m.DocumentTypeId == documentType && m.FileName == name);
                }
                Delete(attachment);

                attachmentDelete.Add(attachment);

                try
                {
                    
                    if (File.Exists(attachment.DownloadUrl))
                    {
                        File.Delete(attachment.DownloadUrl);
                        Directory.Delete(attachment.DownloadUrl.Replace(attachment.FileName, string.Empty));
                    }

                }
                catch
                {
                    // ignored
                }
            }

            return attachmentDelete;
        }

        public async Task<StreamContent> GetStreamContent(TAttachmentTable attachment)
        {


            //var stream = new FileStream(attachment.DownloadUrl, FileMode.Open);
            var stream = new FileStream(attachment.Location, FileMode.Open);
            var content = new StreamContent(stream);
            await content.LoadIntoBufferAsync();

            return content;

        }

        private NetworkCredential AdminNetworkCredential()
        {
            
            return new NetworkCredential(ConfigurationManager.AppSettings["K2AdminUserName"]
                 , ConfigurationManager.AppSettings["K2AdminPassword"]
                 , ConfigurationManager.AppSettings["K2AdminDomain"]);
        }

        // You will want to override this.
        public virtual TAttachmentTable NewAttachmentTable<TPostedAttachment>(
            CreatedFileInfo<TPostedAttachment> createdFileInfo)
            where TPostedAttachment : IPostedAttachmentViewModel
        {
            return NewAttachmentTable(createdFileInfo, null);
        }

        protected TAttachmentTable NewAttachmentTable<TPostedAttachment>(
            CreatedFileInfo<TPostedAttachment> createdFileInfo, TAttachmentTable attachment)
            where TPostedAttachment : IPostedAttachmentViewModel
        {
            if (attachment == null)
            {
                attachment = new TAttachmentTable();
            }
            attachment.ParentId = createdFileInfo.PostedAttachment.Id ?? 0;

            attachment.CreatedBy = createdFileInfo.CreatedBy;
            attachment.CreatedByName = createdFileInfo.CreatedBy;
            attachment.CreatedDate = DateTime.UtcNow;
            attachment.FileName = createdFileInfo.PostedAttachment.Name;
            attachment.FileDescription = createdFileInfo.PostedAttachment.Title;
            attachment.Activity = createdFileInfo.PostedAttachment.Activity;
            attachment.DocumentNumber = createdFileInfo.PostedAttachment.DocumentNumber;
            attachment.FileSize = createdFileInfo.FileSize;
            attachment.FileSizeText = createdFileInfo.FileSizeText;
            attachment.DocumentTypeId = createdFileInfo.PostedAttachment.DocumentType;
            //attachment.DownloadUrl = createdFileInfo.CreatedLocation;
            attachment.Location = createdFileInfo.CreatedLocation;
            attachment.ContentType = createdFileInfo.ContentType;
            attachment.SerialNumber = createdFileInfo.PostedAttachment.SN;

            return attachment;
        }
        public async Task<IEnumerable<TAttachmentTable>> GetAttachmentsWithSNAsync(int parentId)
        {
            var tmp = (await GetWhereAsync(m => m.ParentId == parentId && !string.IsNullOrEmpty(m.SerialNumber) && m.DocumentTypeId == null)).ToListSafe().OrderBy(m => m.CreatedDate);
            var attachments = new List<TAttachmentTable>();
            foreach (var attachment in tmp)
            {
                //attachment.DownloadUrl = ComposeDownloadUrl(attachment);
                attachments.Add(attachment);
            }
            return attachments;
        }
    }

    public class AttachmentService<TAttachmentTable, TWorkflowTable> : AttachmentService<TAttachmentTable>,
        IAttachmentService<TAttachmentTable, TWorkflowTable>
        where TAttachmentTable : BaseAttachmentTable, new()
        where TWorkflowTable : IMpsWorkflowTable
    {
        public AttachmentService(IODataRepositoryAsync<IMPSDbContext, IMPSSession, TAttachmentTable> repository)
            : base(repository)
        {
        }
    }
}