﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using CrossingSoft.Framework.Infrastructure.OData.Repositories;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Infrastructure.EntityFramework;
using TheMall.MPS.Infrastructure.Identity;
using TheMall.MPS.Infrastructure.Sessions;

namespace TheMall.MPS.API.Services
{
    public interface IAspNetUserRolesService : IBaseMasterService<ApplicationUserRole>
    {
    }
    public class AspNetUserRolesService : BaseMasterService<ApplicationUserRole>, IAspNetUserRolesService
    {
        public AspNetUserRolesService(
            IODataRepositoryAsync<
            IMPSDbContext, IMPSSession, ApplicationUserRole> repository)
            : base(repository)
        {
        }

        public override ApplicationUserRole DoGet(string code)
        {
            return Get(code);
        }

        public override Task<ApplicationUserRole> DoGetAsync(string code)
        {
            return GetAsync(code);
        }
    }
}