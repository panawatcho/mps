﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using CrossingSoft.Framework.Infrastructure.OData.Repositories;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Infrastructure.EntityFramework;
using TheMall.MPS.Infrastructure.Identity;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Models.Master;

namespace TheMall.MPS.API.Services
{
    public interface IAspNetRolesService : IBaseMasterService<ApplicationRole>
    {
    }
    public class AspNetRolesService : BaseMasterService<ApplicationRole>, IAspNetRolesService
    {
        public AspNetRolesService(
            IODataRepositoryAsync<IMPSDbContext, IMPSSession, ApplicationRole> repository)
            : base(repository)
        {
        }

        public override ApplicationRole DoGet(string code)
        {
            return Get(code);
        }

        public override Task<ApplicationRole> DoGetAsync(string code)
        {
            return GetAsync(code);
        }
    }
}