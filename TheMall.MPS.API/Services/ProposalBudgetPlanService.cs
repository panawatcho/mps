﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using CrossingSoft.Framework.Infrastructure.OData.Repositories;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.Models.Proposal;

namespace TheMall.MPS.API.Services
{
    public interface IProposalBudgetPlanService : Interfaces.IODataService<MPS_ProposalBudgetPlan>
    {
        MPS_ProposalBudgetPlan GetProposalBudgetPlan(int proposalId, string apCode, string unitCode);
    }
    public class ProposalBudgetPlanService : BaseODataService<MPS_ProposalBudgetPlan>, IProposalBudgetPlanService
    {
        public ProposalBudgetPlanService(IODataRepositoryAsync<MPS_ProposalBudgetPlan> repository)
            : base(repository)
        {
        }

        public MPS_ProposalBudgetPlan GetProposalBudgetPlan(int proposalId, string apCode, string unitCode)
        {
            return LastOrDefault(m => m.ProposalId == proposalId
                                      && m.APCode.Equals(apCode, StringComparison.InvariantCultureIgnoreCase)
                                      && m.UnitCode.Equals(unitCode, StringComparison.InvariantCultureIgnoreCase));
        }
    }
}