﻿using System.Collections.Generic;
using System.Runtime.Caching;
using CrossingSoft.Framework.Infrastructure.K2;
using CrossingSoft.Framework.Models.K2;
using TheMall.MPS.API.Helpers;


namespace TheMall.MPS.API.Services
{
    public interface IUMUserService
    {
        UMUser GetUser(string fqn);
        IReadOnlyList<UMUser> GetRoleUsers(string role);
    }

    public class UMUserService : IUMUserService
    {
        private static readonly MemoryCache UMUserCache = new MemoryCache("UMUserCache");
        private readonly IK2Repository<CrossingSoft.Framework.Models.K2.UMUser> _repository;
        public UMUserService(IK2Repository<UMUser> repository)
        {
            _repository = repository;
        }

        public UMUser GetUser(string fqn)
        {
            if (string.IsNullOrEmpty(fqn) || (!fqn.Contains("\\") && !fqn.Contains(":")))
            {
                return null;
            }
            if (fqn.Contains("\\") && !fqn.Contains(":"))
            {
                //fqn = AppSettings.Instance.SecurityLabelName() + ":" + fqn;
                fqn = Infrastructure.Constants.SecurityLabelName + ":" + fqn;
            }
            return Caching.GetFromCache(UMUserCache, "UMUser_" + fqn,
                () => DoGetUser(fqn));
        }

        private UMUser DoGetUser(string fqn)
        {
            return _repository.Find(m => m.FQN == fqn);
        }

        public IReadOnlyList<UMUser> GetRoleUsers(string role)
        {
            var properties = new Dictionary<string, object>();
            properties.Add("Role_Name", role);
            return _repository.ExecuteListMethod("Get_Role_Users", properties).ToListSafe();
        }
    }
}