﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.Infrastructure.Repositories;
using TheMall.MPS.Models.Procurement.PO;

namespace TheMall.MPS.API.Services
{
    public interface IPurchOrderTableService : Interfaces.IODataService<MPS_PurchOrderTable>
    {
    }
    public class PurchOrderTableService : BaseODataService<MPS_PurchOrderTable>, IPurchOrderTableService
    {
        public PurchOrderTableService(IMpsODataRepositoryAsync<MPS_PurchOrderTable> repository) 
            : base(repository)
        {
        }
    }
}