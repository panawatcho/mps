﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.Infrastructure.Repositories;
using TheMall.MPS.Models.Procurement.GR;

namespace TheMall.MPS.API.Services
{
    public interface IGoodsReceiptTableService : Interfaces.IODataService<MPS_GoodsReceiptTable>
    {
    }
    public class GoodsReceiptTableService : BaseODataService<MPS_GoodsReceiptTable>, IGoodsReceiptTableService
    {
        public GoodsReceiptTableService(IMpsODataRepositoryAsync<MPS_GoodsReceiptTable> repository) 
            : base(repository)
        {
        }
    }
}