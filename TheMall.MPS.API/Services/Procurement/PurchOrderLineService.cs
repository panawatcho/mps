﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.Infrastructure.Repositories;
using TheMall.MPS.Models.Procurement.PO;

namespace TheMall.MPS.API.Services
{
    public interface IPurchOrderLineService : Interfaces.IODataService<MPS_PurchOrderLine>
    {
    }
    public class PurchOrderLineService : BaseODataService<MPS_PurchOrderLine>, IPurchOrderLineService
    {
        public PurchOrderLineService(IMpsODataRepositoryAsync<MPS_PurchOrderLine> repository) 
            : base(repository)
        {
        }
    }
}