﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.Infrastructure.Repositories;
using TheMall.MPS.Models.Procurement.PR;

namespace TheMall.MPS.API.Services
{
    public interface IPurchReqTableService : Interfaces.IODataService<MPS_PurchReqTable>
    {
    }
    public class PurchReqTableService : BaseODataService<MPS_PurchReqTable>, IPurchReqTableService
    {
        public PurchReqTableService(IMpsODataRepositoryAsync<MPS_PurchReqTable> repository) 
            : base(repository)
        {
        }
    }
}