﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.Infrastructure.Repositories;
using TheMall.MPS.Models.Procurement.PR;

namespace TheMall.MPS.API.Services
{
    public interface IPurchReqLineService : Interfaces.IODataService<MPS_PurchReqLine>
    {
    }
    public class PurchReqLineService : BaseODataService<MPS_PurchReqLine>, IPurchReqLineService
    {
        public PurchReqLineService(IMpsODataRepositoryAsync<MPS_PurchReqLine> repository) 
            : base(repository)
        {
        }
    }
}