﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.Infrastructure.Repositories;
using TheMall.MPS.Models.Procurement.GR;

namespace TheMall.MPS.API.Services
{
    public interface IGoodsReceiptLineService : Interfaces.IODataService<MPS_GoodsReceiptLine>
    {
    }
    public class GoodsReceiptLineService : BaseODataService<MPS_GoodsReceiptLine>, IGoodsReceiptLineService
    {
        public GoodsReceiptLineService(IMpsODataRepositoryAsync<MPS_GoodsReceiptLine> repository) 
            : base(repository)
        {
        }
    }
}