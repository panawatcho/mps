﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CrossingSoft.Framework.Infrastructure.OData.Repositories;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Models.FileManager;

namespace TheMall.MPS.API.Services
{
    public interface IFileManagerDocumentService : IService<FileManagerDocument>
    {
        FileManagerDocument Get(int id);
        IEnumerable<FileManagerDocument> GetAll(FileManagerDirectory directory, string username);
    }

    public class FileManagerDocumentService : Abstracts.BaseService<FileManagerDocument>, IFileManagerDocumentService
    {
        public FileManagerDocumentService(IODataRepositoryAsync<FileManagerDocument> repository)
            : base(repository)
        {
        }
        public FileManagerDocument Get(int id)
        {
            return Find(m => m.Id == id);
        }
        public IEnumerable<FileManagerDocument> GetAll(FileManagerDirectory directory, string username)
        {
            return GetWhere(m => m.DirectoryId == directory.Id);
        }
    }
}