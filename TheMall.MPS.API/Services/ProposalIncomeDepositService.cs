﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using CrossingSoft.Framework.Infrastructure.OData.Repositories;
using CrossingSoft.Framework.Services.OData;
using SourceCode.Workflow.Client;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Infrastructure.EntityFramework;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Models.Proposal;

namespace TheMall.MPS.API.Services
{
    public interface IProposalIncomeDepositService : Interfaces.IODataService<MPS_IncomeDeposit>
    {
    }
    public class ProposalIncomeDepositService : BaseODataService<MPS_IncomeDeposit>, IProposalIncomeDepositService
    {
        public ProposalIncomeDepositService(IODataRepositoryAsync<MPS_IncomeDeposit> repository) : base(repository)
        {
        }
    }
}