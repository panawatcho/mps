﻿using System;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml;
using System.Xml.Serialization;
using TheMall.MPS.API.Facades;
using TheMall.MPS.API.Mappers;
using TheMall.MPS.API.Services;
using TheMall.MPS.Resources;
using TheMall.MPS.ViewModels.InterfaceToMprocurement;
using TheMall.MPS.ViewModels.InterfaceToMprocurement.GR;
using TheMall.MPS.ViewModels.InterfaceToMprocurement.PO;
using TheMall.MPS.ViewModels.InterfaceToMprocurement.PR;

namespace TheMall.MPS.API.WebServices
{
    /// <summary>
    /// Summary description for Service1
    /// </summary>

#if PROD
     [WebService(Namespace = "http://10.90.231.23/mps.api/webservices/mprocurement/")]
#else
    [WebService(Namespace = "http://10.90.234.40/mps.api/webservices/mprocurement/")]
#endif

    [WebServiceBinding(ConformsTo = WsiProfiles.None)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class InterfaceToMprocurement : WebService
    {
        //// ลองใส่ ถ้า IP ไม่เปลี่ยน
        //[WebMethod]
        //[SoapRpcMethod]
        //public string TestCRS(string DataInput)
        //{
        //    using (Resolver.BeginScope())
        //    {
        //        try
        //        {
        //            return "Test 2435";
        //        }
        //        catch (Exception ex)
        //        {
        //            return XMLHelper.SerializeToXmlDocument(XMLHelper.InitReplyViewModel(false, "Error", null, null), null);
        //        }
        //    }
        //}

        [WebMethod]
        [SoapRpcMethod]
        public string DATA_SYNCH(string DataInput)
        {
            using (Resolver.BeginScope())
            {
                try
                {

                    if (string.IsNullOrEmpty(DataInput))
                        throw new Exception("Parameter not found");
#if PROD || DEV
                    File.AppendAllText("D:/mps_log/log" + DateTime.Now.ToString("yyyyMMdd") + ".txt",
                        DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + @" --- DATA_SYNCH XML : " + DataInput +
                        Environment.NewLine);
#endif
                    var request = XMLHelper.DeserializeToRequestViewModel(DataInput);
                    if (request == null || request.Param == null)
                        throw new Exception("Parameter not found");

                    var tableName = request.Param.Keyword;
                    object result = null;
                    switch (tableName)
                    {
                        case "APCODE":
                        {
                            var _apcodeService = Resolver.GetInstance<IAPcodeService>();
                            var apCodeList = _apcodeService.APCode();
                            if (apCodeList != null && apCodeList.Any())
                                result = apCodeList.M_ToAPCodeViewModels().ToList();
                            break;

                        }
                        case "PROPOSALID":
                        {
                            var _proposalService = Resolver.GetInstance<IProposalService>();
                            var proposalList = _proposalService.GetOpenProposal();
                            if (proposalList != null && proposalList.Any())
                                result = proposalList.M_ToProposalNumbers().ToList();

                            break;
                        }
                        case "BU_PROB_AP":
                        {
                            var _proposalLineService = Resolver.GetInstance<IProposalLineService>();
                            var _proposalLineMapper = Resolver.GetInstance<IProposalLineMapper>();
                            var proposalLineList = _proposalLineService.GetOpenProposalLine();
                            if (proposalLineList != null && proposalLineList.Any())
                                result = _proposalLineMapper.M_ToProposalLineViewModels(proposalLineList).ToList();
                            break;
                        }
                        case "ALLOCATIONCODE":
                        {
                            var _allocationBasisService = Resolver.GetInstance<IAllocationBasisService>();
                            var allocationBasisList = _allocationBasisService.GetAllocationBasis();
                            if (allocationBasisList != null && allocationBasisList.Any())
                                result = allocationBasisList.M_ToAllocationBasisViewModels().ToList();
                            break;
                        }
                    }

                    return XMLHelper.SerializeToXmlDocument(XMLHelper.InitReplyViewModel(true, "Successful", tableName, result),
                        tableName);
                }
                catch (Exception ex)
                {
                    return XMLHelper.SerializeToXmlDocument(XMLHelper.InitReplyViewModel(false, "Error", null, null), null);
                }
            }
        }



        [WebMethod]
        [SoapRpcMethod]
        public string CHECKBUDGET(string DataInput)
        {
            using (Resolver.BeginScope())
            {
                try
                {
                    if (string.IsNullOrEmpty(DataInput))
                        throw new Exception("Parameter not found");
#if PROD || DEV
                    File.AppendAllText("D:/mps_log/log" + DateTime.Now.ToString("yyyyMMdd") + ".txt",
                        DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + @" --- CHECKBUDGET XML : " + DataInput +
                        Environment.NewLine);
#endif
                    var request = XMLHelper.DeserializeToPRRequestViewModel(DataInput);
                    if (request == null || request.Header == null)
                        throw new Exception("Parameter not found");
                    var _budgetProposalTransFacade = Resolver.GetInstance<IBudgetProposalTransFacade>();
                    var subCommand = request.Header.SUBCOMMAND;
                    ReturnMessage result = null;
                    switch (subCommand.ToUpper())
                    {
                        case "CREATEPR":
                            {
                                result = _budgetProposalTransFacade.CreatePR(request);
                                break;
                            }
                        case "CANCELPR":
                            {
                                result = _budgetProposalTransFacade.CancelPR(request);
                                break;
                            }
                        case "CHANGEPR":
                            {
                                result = _budgetProposalTransFacade.ChangePR(request);
                                break;
                            }
                    }
                    if (result != null && result.Status)
                    {
                        return XMLHelper.SerializeToXmlDocument(XMLHelper.InitReplyViewModel(true, string.Format("{0} Information! Purchase requisition number {0} check budget pass", request.Header.PR_NUM), null, null, new ReplyParamViewModel() { REQ_NUM = request.Header.PR_NUM }));
                    }

                    return XMLHelper.SerializeToXmlDocument(XMLHelper.InitReplyViewModel(false, result != null ? result.Message : "", null));
                }
                catch (Exception ex)
                {
                    return XMLHelper.SerializeToXmlDocument(XMLHelper.InitReplyViewModel(false, "Error XML Format", null));
                }
            }
        }

        [WebMethod]
        [SoapRpcMethod]
        public string CREATE_PO(string DataInput)
        {
            using (Resolver.BeginScope())
            {
                try
                {
                    if (string.IsNullOrEmpty(DataInput))
                        throw new Exception("Parameter not found");
#if PROD || DEV
                    File.AppendAllText("D:/mps_log/log" + DateTime.Now.ToString("yyyyMMdd") + ".txt",
                        DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + @" --- CREATE_PO XML : " + DataInput +
                        Environment.NewLine);
#endif
                    var request = XMLHelper.DeserializeToPORequestViewModel(DataInput);
                    if (request == null || request.Header == null)
                        throw new Exception("Parameter not found");
                    var _budgetProposalTransFacade = Resolver.GetInstance<IBudgetProposalTransFacade>();
                    //var subCommand = request.Header.SUBCOMMAND;
                    ReturnMessage result = null;
                    result = _budgetProposalTransFacade.CreatePO(request);
                    if (result != null && result.Status)
                    {
                        return XMLHelper.SerializeToXmlDocument(XMLHelper.InitReplyViewModel(true, string.Format("{0} Information! Success! create purchase order number : {0}", request.Header.PO_NUM), null, null, new ReplyParamViewModel() { PO_NUM = request.Header.PO_NUM }));
                    }

                    return XMLHelper.SerializeToXmlDocument(XMLHelper.InitReplyViewModel(false, result != null ? result.Message : "", null));
                }
                catch (Exception ex)
                {
                    return XMLHelper.SerializeToXmlDocument(XMLHelper.InitReplyViewModel(false, "Error XML Format", null));
                }
            }
        }

        [WebMethod]
        [SoapRpcMethod]
        public string CANCEL_PO(string DataInput)
        {
            using (Resolver.BeginScope())
            {
                try
                {
                    if (string.IsNullOrEmpty(DataInput))
                        throw new Exception("Parameter not found");
#if PROD || DEV
                    File.AppendAllText("D:/mps_log/log" + DateTime.Now.ToString("yyyyMMdd") + ".txt",
                        DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + @" --- CANCEL_PO XML : " + DataInput +
                        Environment.NewLine);
#endif
                    var request = XMLHelper.DeserializeToPORequestViewModel(DataInput);
                    if (request == null || request.Header == null)
                        throw new Exception("Parameter not found");
                    var _budgetProposalTransFacade = Resolver.GetInstance<IBudgetProposalTransFacade>();
                    //var subCommand = request.Header.SUBCOMMAND;
                    ReturnMessage result = null;
                    result = _budgetProposalTransFacade.CancelPO(request);
                    if (result != null && result.Status)
                    {
                        return XMLHelper.SerializeToXmlDocument(XMLHelper.InitReplyViewModel(true, string.Format("{0} Information! Success! cancel purchase order number : {0}", request.Header.PO_NUM), null));
                    }

                    return XMLHelper.SerializeToXmlDocument(XMLHelper.InitReplyViewModel(false, result != null ? result.Message : "", null));
                }
                catch (Exception ex)
                {
                    return XMLHelper.SerializeToXmlDocument(XMLHelper.InitReplyViewModel(false, "Error XML Format", null));
                }
            }
        }

        [WebMethod]
        [SoapRpcMethod]
        public string CHANGE_PO(string DataInput)
        {
            using (Resolver.BeginScope())
            {
                try
                {
                    if (string.IsNullOrEmpty(DataInput))
                        throw new Exception("Parameter not found");
#if PROD || DEV
                    File.AppendAllText("D:/mps_log/log" + DateTime.Now.ToString("yyyyMMdd") + ".txt",
                        DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + @" --- CHANGE_PO XML : " + DataInput +
                        Environment.NewLine);
#endif
                    var request = XMLHelper.DeserializeToPORequestViewModel(DataInput);
                    if (request == null || request.Header == null)
                        throw new Exception("Parameter not found");
                    var _budgetProposalTransFacade = Resolver.GetInstance<IBudgetProposalTransFacade>();
                    //var subCommand = request.Header.SUBCOMMAND;
                    ReturnMessage result = null;
                    result = _budgetProposalTransFacade.ChangePO(request);
                    if (result != null && result.Status)
                    {
                        return XMLHelper.SerializeToXmlDocument(XMLHelper.InitReplyViewModel(true, string.Format("{0} Information! Success! change purchase order number : {0}", request.Header.PO_NUM), null));
                    }

                    return XMLHelper.SerializeToXmlDocument(XMLHelper.InitReplyViewModel(false, result != null ? result.Message : "", null));
                }
                catch (Exception ex)
                {
                    return XMLHelper.SerializeToXmlDocument(XMLHelper.InitReplyViewModel(false, "Error XML Format", null));
                }
            }
        }

        [WebMethod]
        [SoapRpcMethod]
        public string GOODS_RECEIPT(string DataInput)
        {
            using (Resolver.BeginScope())
            {
                try
                {
                    if (string.IsNullOrEmpty(DataInput))
                        throw new Exception("Parameter not found");
#if PROD || DEV
                    File.AppendAllText("D:/mps_log/log" + DateTime.Now.ToString("yyyyMMdd") + ".txt",
                        DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + @" --- GOODS_RECEIPT XML : " + DataInput +
                        Environment.NewLine);
#endif
                    var request = XMLHelper.DeserializeToGRRequestViewModel(DataInput);
                    if (request == null || request.Header == null)
                        throw new Exception("Parameter not found");
                    var _budgetProposalTransFacade = Resolver.GetInstance<IBudgetProposalTransFacade>();

                    ReturnMessage result = null;
                    result = _budgetProposalTransFacade.GoodsReceipt(request);
                    if (result != null && result.Status)
                    {
                        return XMLHelper.SerializeToXmlDocument(XMLHelper.InitReplyViewModel(true, string.Format("{0} Information!  Success! create goods receipt id : {0}", request.Header.RECEIPT_ID), null, null, new ReplyParamViewModel() { GR_NUM = request.Header.RECEIPT_ID }));
                    }

                    return XMLHelper.SerializeToXmlDocument(XMLHelper.InitReplyViewModel(false, result != null ? result.Message : "", null));
                }
                catch (Exception ex)
                {
                    return XMLHelper.SerializeToXmlDocument(XMLHelper.InitReplyViewModel(false, "Error XML Format", null));
                }
            }
        }
        
    }
    
    public static class XMLHelper
    {
        public static ReplyViewModel InitReplyViewModel(bool success, string message = "", string tableName = "", object data = null, ReplyParamViewModel param = null)
        {
            var viewModel = new ReplyViewModel()
            {
                Approved = success
            };
            if (success)
            {
                viewModel.TableName = tableName;
                viewModel.Param = param ?? new ReplyParamViewModel();
                viewModel.Data = data;
                viewModel.Info = new InfoViewModel()
                {
                    Code = "",
                    Description = message,
                    Line = "",
                    Type = "Information"
                };
            }
            else
            {
                viewModel.Param = param ?? new ReplyParamViewModel();
                if (message.Contains("การขอใช้งบประมาณจาก Proposal"))
                {
                    viewModel.Info = new InfoViewModel()
                    {
                        Code = "",
                        Description = message,
                        DescriptionExt = string.Format(Error.BudgetExceed),
                        Line = "",
                        Type = "Error"
                    };
                }
                else
                {
                    viewModel.Info = new InfoViewModel()
                    {
                        Code = "",
                        Description = string.Format(Error.ReturnInterfaceError),
                        DescriptionExt = message,
                        Line = "",
                        Type = "Error"
                    };
                }

            }
            return viewModel;
        }

        public static string SerializeToXmlDocument(object input, string removeTag = "")
        {
            XmlSerializer ser = new XmlSerializer(input.GetType());
            XmlDocument xd = null;
            //Create our own namespaces for the output
            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();

            //Add an empty namespace and empty value
            ns.Add("", "");

            using (MemoryStream memStm = new MemoryStream())
            {
                ser.Serialize(memStm, input, ns);

                memStm.Position = 0;

                XmlReaderSettings settings = new XmlReaderSettings();
                settings.IgnoreWhitespace = true;

                using (var xtr = XmlReader.Create(memStm, settings))
                {
                    xd = new XmlDocument();
                    xd.Load(xtr);
                }
            }
            //Remove Tag
            if (xd != null && !string.IsNullOrEmpty(removeTag))
            {
                var xmlString = xd.OuterXml;
                xmlString = xmlString.Replace(@"<" + removeTag + @"LIST>", "");
                xmlString = xmlString.Replace(@"</" + removeTag + @"LIST>", "");
                xmlString = xmlString.Replace(@"<" + removeTag, "<Table");
                xmlString = xmlString.Replace(removeTag + @">", "Table>");

                xd = new XmlDocument();
                xd.LoadXml(xmlString);
            }

            var declarations = xd.ChildNodes.OfType<XmlNode>().Where(x => x.NodeType == XmlNodeType.XmlDeclaration).ToList();

            declarations.ForEach(x => xd.RemoveChild(x));

            return xd.InnerXml;
        }

        public static RequestParamViewModel DeserializeToRequestViewModel(string inputString)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(RequestParamViewModel));
            using (TextReader reader = new StringReader(inputString))
            {
                RequestParamViewModel result = (RequestParamViewModel)serializer.Deserialize(reader);
                return result;
            }
        }

        public static PRRequestViewModel DeserializeToPRRequestViewModel(string inputString)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(PRRequestViewModel));
            using (TextReader reader = new StringReader(inputString))
            {
                PRRequestViewModel result = (PRRequestViewModel)serializer.Deserialize(reader);
                return result;
            }
        }

        public static PORequestViewModel DeserializeToPORequestViewModel(string inputString)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(PORequestViewModel));
            using (TextReader reader = new StringReader(inputString))
            {
                PORequestViewModel result = (PORequestViewModel)serializer.Deserialize(reader);
                return result;
            }
        }

        public static GRRequestViewModel DeserializeToGRRequestViewModel(string inputString)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(GRRequestViewModel));
            using (TextReader reader = new StringReader(inputString))
            {
                GRRequestViewModel result = (GRRequestViewModel)serializer.Deserialize(reader);
                return result;
            }
        }
    }
}