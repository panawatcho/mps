﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using TheMall.MPS.ViewModels.Proposal;
using TheMall.MPS.API.Services;
using TheMall.MPS.API.Facades;
using TheMall.MPS.API.Mappers;
using TheMall.MPS.Infrastructure.Sessions;


namespace TheMall.MPS.API.WebServices
{
    /// <summary>
    /// Summary description for Service1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class BudgetProposalWebService : System.Web.Services.WebService
    {

        [WebMethod]
        public bool CreateBudgetProposalTrans(int workflowTableId, string docType)
        {
            using (Resolver.BeginScope())
            {
                var _budgetPropTransFacade = Resolver.GetInstance<IBudgetProposalTransFacade>();
                return _budgetPropTransFacade.CreateBudgetPropTrans(workflowTableId, docType);
            }
        }
        [WebMethod]
        public bool CancelBudgetProposalTrans(int workflowTableId, string docType)
        {
            using (Resolver.BeginScope())
            {
                var _budgetPropTransFacade = Resolver.GetInstance<IBudgetProposalTransFacade>();
                return _budgetPropTransFacade.CancelBudgetPropTrans(workflowTableId, docType);
            }
        }
        [WebMethod]
        public bool ChangeBudgetProposalTrans(int workflowTableId, string docType)
        {
            using (Resolver.BeginScope())
            {
                var _budgetPropTransFacade = Resolver.GetInstance<IBudgetProposalTransFacade>();
                return _budgetPropTransFacade.ChangeBudgetPropTrans(workflowTableId, docType);
            }
        }
        [WebMethod]
        public bool CloseAdvanceBudgetPropTrans(int workflowTableId)
        {
            using (Resolver.BeginScope())
            {
                var _budgetPropTransFacade = Resolver.GetInstance<IBudgetProposalTransFacade>();
                return _budgetPropTransFacade.CloseAdvanceBudgetPropTrans(workflowTableId);
            }
        }

        [WebMethod]
        public bool CancelProposalDepositTrans(int workflowTableId)
        {
            using (Resolver.BeginScope())
            {
                var _budgetDepTransFacade = Resolver.GetInstance<IBudgetDepositTransFacade>();
                return _budgetDepTransFacade.CancelProposalDepositTrans(workflowTableId);
            }
        }

        [WebMethod]
        public bool CancelDepositTransStepCreateIncome(int workflowTableId)
        {
            using (Resolver.BeginScope())
            {
                var result = false;
                var _incomeDepositService = Resolver.GetInstance<IProposalIncomeDepositService>();
                var _budgetDepTransFacade = Resolver.GetInstance<IBudgetDepositTransFacade>();
                var incomeModel = _incomeDepositService.GetWhere(m => m.ParentId == workflowTableId);
                if (incomeModel != null)
                {
                    foreach (var income in incomeModel)
                    {
                        _budgetDepTransFacade.CancelDepositTransStepCreateIncome(income);
                        result = true;
                    }
                }
                return result;
           }
        }
      
        
    }
}