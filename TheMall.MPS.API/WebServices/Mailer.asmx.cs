﻿using System;
using System.Linq;
using System.Web.Script.Services;
using System.Web.Services;
using CrossingSoft.Framework.Models.Interfaces;
using TheMall.MPS.API.Facades;
using TheMall.MPS.API.Factories;
using TheMall.MPS.API.Services;
using TheMall.MPS.Models.Enums;

//using TMS.API.Factories;


namespace TheMall.MPS.API.WebServices
{
    /// <summary>
    /// Summary description for Mailer
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Mailer : System.Web.Services.WebService
    {
        [WebMethod]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public string GetNotificationSubject(
            string processName,
            int procInstId,
            string activityName,
            int actiInstDestId,
            int workflowTableId)
        {
            using (Resolver.BeginScope())
            {
                var success = false;
               // //LogStart();

                try
                {
                    var mailSetupService = Resolver.GetInstance<IMailSetupService>();
                    var mailSetup = mailSetupService.GetNotificationSetup(processName, activityName);
                    if (mailSetup == null)
                    {
                        throw new Exception(string.Format("Cannot find notification for '{0}' '{1}'.", processName, activityName));
                    }

                    //var factory = Resolver.GetInstance<IMPSWorkflowServiceFactory>();
                    //var workflowTableService = factory.CreateNew(processName);
                    //var workflowTable = workflowTableService.GetInterfaceWorkflowTable(workflowTableId);

                    var facade = Resolver.GetInstance<IMailerFacade>();
                    var result = facade.GetNotificationSubject(mailSetup.MailContentId, procInstId,  actiInstDestId);
                    success = true;
                    return result;
                }
                finally
                {
                  //  //LogComplete(success);
                }
            }
        }

        [WebMethod]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public string GetNotificationBody(
            string processName,
            int procInstId,
            string activityName,
            string worklistItemUrl,
            int workflowTableId)
        {
            using (Resolver.BeginScope())
            {
                var success = false;
               // //LogStart();

                try
                {
                    var mailSetupService = Resolver.GetInstance<IMailSetupService>();
                    var mailSetup = mailSetupService.GetNotificationSetup(processName, activityName);
                    if (mailSetup == null)
                    {
                        throw new Exception(string.Format("Cannot find notification for '{0}' '{1}'.", processName, activityName));
                    }

                    //var factory = Resolver.GetInstance<IMPSWorkflowServiceFactory>();
                    //var workflowTableService = factory.CreateNew(processName);
                    //var workflowTable = workflowTableService.GetInterfaceWorkflowTable(workflowTableId);

                    var facade = Resolver.GetInstance<IMailerFacade>();

                    var result = facade.GetNotificationBody(mailSetup.MailContentId, procInstId,  worklistItemUrl);
                    
                    success = true;
                    return result;
                }
                finally
                {
                   // //LogComplete(success);
                }
            }
        }

        [WebMethod]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public string GetInformTo(
            string processName,
            string activityName)
        {
            using (Resolver.BeginScope())
            {
                var success = false;
                ////LogStart();

                try
                {
                    var mailRecipientService = Resolver.GetInstance<IMailRecipientService>();
                    var recipients = mailRecipientService.GetInformMailRecipients(processName, activityName);

                    if (!recipients.Any() || recipients.All(m => string.IsNullOrEmpty(m.Email)))
                    {
                        success = true;
                        return string.Empty;
                    }
                    else
                    {
                        var result = string.Join(";", recipients.Where(m => !string.IsNullOrEmpty(m.Email) && m.RecipientType == MailRecipientType.Email).Select(m => m.Email).ToList());
                        success = true;
                        return result;
                    }
                }
                finally
                {
                   // //LogComplete(success);
                }
            }
        }

        [WebMethod]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public string GetInformSubject(
            string processName,
            int procInstId,
            string activityName,
            int actiInstDestId,
            int workflowTableId)
        {
            using (Resolver.BeginScope())
            {
                var success = false;
              //  //LogStart();

                try
                {
                    var mailSetupService = Resolver.GetInstance<IMailSetupService>();
                    var mailSetup = mailSetupService.GetInformSetup(processName, activityName);
                    if (mailSetup == null)
                    {
                        throw new Exception(string.Format("Cannot find inform setup for '{0}' '{1}'.", processName, activityName));
                    }

                    //var factory = Resolver.GetInstance<IMPSWorkflowServiceFactory>();
                    //var workflowTableService = factory.CreateNew(processName);
                    //var workflowTable = workflowTableService.GetInterfaceWorkflowTable(workflowTableId);

                    var facade = Resolver.GetInstance<IMailerFacade>();
                    var result = facade.GetMailEventSubject(mailSetup.MailContentId, procInstId,  actiInstDestId);
                    success = true;
                    return result;
                }
                finally
                {
                   // //LogComplete(success);
                }
            }
        }


        [WebMethod]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public string GetInformBody(
            string processName,
            int procInstId,
            string activityName,
            int actiInstDestId,
            int workflowTableId,
            string items = null)
        {
            using (Resolver.BeginScope())
            {
                var success = false;
                //LogStart();

                try
                {
                    var mailSetupService = Resolver.GetInstance<IMailSetupService>();
                    var mailSetup = mailSetupService.GetInformSetup(processName, activityName);
                    if (mailSetup == null)
                    {
                        throw new Exception(string.Format("Cannot find inform setup for '{0}' '{1}'.", processName, activityName));
                    }

                    //var factory = Resolver.GetInstance<IMPSWorkflowServiceFactory>();
                    //var workflowTableService = factory.CreateNew(processName);
                    //var workflowTable = workflowTableService.GetInterfaceWorkflowTable(workflowTableId);

                    var facade = Resolver.GetInstance<IMailerFacade>();

                    var result = facade.GetMailEventBody(mailSetup.MailContentId, procInstId,  string.Empty, actiInstDestId, items);
                    
                    success = true;
                    return result;
                }
                finally
                {
                    //LogComplete(success);
                }
            }
        }

        [WebMethod]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public string GetEscalateTo_Overdue(
            string processName,
            string activityName,
            string destinationUser)
        {
            using (Resolver.BeginScope())
            {
                var success = false;
                //LogStart();

                try
                {
                    //var userService = Resolver.GetInstance<IUserService>();
                    //var destination = userService.GetUser(destinationUser);
                    //if (destination == null)
                    //{
                    //    throw new Exception(string.Format("Cannot find destination user '{0}'.", destinationUser));
                    //}
                    //if (string.IsNullOrEmpty(destination.ReportTo))
                    //{
                    //    return destination.Email;
                    //}

                    //var reportTo = userService.GetUser(destination.ReportTo);
                    //if (reportTo == null || string.IsNullOrEmpty(reportTo.Email))
                    //{
                    //    return destination.Email;
                    //}
                    success = true;
                    //return reportTo.Email;
                    return null;
                }
                finally
                {
                    //LogComplete(success);
                }
            }
        }

        [WebMethod]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public string GetEscalateSubject(
            string processName,
            int procInstId,
            string activityName,
            int actiInstDestId,
            int workflowTableId,
            string destinationUser,
            string extra)
        {
            using (Resolver.BeginScope())
            {
                var success = false;
                //LogStart();

                try
                {
                    var mailSetupService = Resolver.GetInstance<IMailSetupService>();
                    var mailContentId = mailSetupService.GetEscalationSetup_ContentId(processName, activityName, extra);
                    if (string.IsNullOrEmpty(mailContentId))
                    {
                        throw new Exception(string.Format("Cannot find escalation setup for '{0}' '{1}'.", processName, activityName));
                    }

                    //var factory = Resolver.GetInstance<IMPSWorkflowServiceFactory>();
                    //var workflowTableService = factory.CreateNew(processName);
                    //var workflowTable = workflowTableService.GetInterfaceWorkflowTable(workflowTableId);
                    
                    var facade = Resolver.GetInstance<MailerFacade>();
                    var result = facade.GetEscalationSubject(mailContentId, procInstId, destinationUser, actiInstDestId);
                    success = true;
                    return result;
                }
                finally
                {
                    //LogComplete(success);
                }
            }
        }


        [WebMethod]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public string GetEscalateBody(
            string processName,
            int procInstId,
            string activityName,
            int actiInstDestId,
            int workflowTableId,
            string destinationUser,
            string extra,
            string items = null)
        {
            using (Resolver.BeginScope())
            {
                var success = false;
                //LogStart();

                try
                {
                    var mailSetupService = Resolver.GetInstance<IMailSetupService>();
                    var mailContentId = mailSetupService.GetEscalationSetup_ContentId(processName, activityName, extra);
                    if (string.IsNullOrEmpty(mailContentId))
                    {
                        throw new Exception(string.Format("Cannot find escalation setup for '{0}' '{1}'.", processName, activityName));
                    }

                    //var factory = Resolver.GetInstance<IMPSWorkflowServiceFactory>();
                    //var workflowTableService = factory.CreateNew(processName);
                    //var workflowTable = workflowTableService.GetInterfaceWorkflowTable(workflowTableId);

                    var facade = Resolver.GetInstance<IMailerFacade>();

                    var result = facade.GetEscalationBody(mailContentId, procInstId, destinationUser, actiInstDestId, string.Format("{0}_{1}", procInstId, actiInstDestId), items);
                    
                    success = true;
                    return result;
                }
                finally
                {
                    //LogComplete(success);
                }
            }
        }
    }
}
