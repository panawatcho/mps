﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using CrossingSoft.Framework.Infrastructure.K2;
using TheMall.MPS.ViewModels.Proposal;
using TheMall.MPS.API.Services;
using TheMall.MPS.API.Facades;
using TheMall.MPS.API.Mappers;
using TheMall.MPS.ViewModels;


namespace TheMall.MPS.API.WebServices
{
    /// <summary>
    /// Summary description for Service1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class EmployeeWebService : System.Web.Services.WebService
    {

        [WebMethod]
        public string GetEmailInformProposal(int workflowTableId)
        {

            using (Resolver.BeginScope())
            {
               
                try
                {
                    var _proposalFacade = Resolver.GetInstance<IProposalFacade>();
                    return _proposalFacade.GetEmailInformAll(workflowTableId);
                   
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
               
            }

        }

        [WebMethod]
        public string GetEmailInformMemo(int workflowTableId)
        {

            using (Resolver.BeginScope())
            {

                try
                {
                    var _facade = Resolver.GetInstance<IMemoFacade>();
                    return _facade.GetEmailInformAll(workflowTableId);

                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }

            }

        }
        [WebMethod]
        public string GetEmailInformPettyCash(int workflowTableId)
        {

            using (Resolver.BeginScope())
            {

                try
                {
                    var _facade = Resolver.GetInstance<IPettyCashFacade>();
                    return _facade.GetEmailInformAll(workflowTableId);

                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }

            }

        }
        [WebMethod]
        public string GetEmailInformCashAdvance(int workflowTableId)
        {

            using (Resolver.BeginScope())
            {

                try
                {
                    var _facade = Resolver.GetInstance<ICashAdvanceFacade>();
                    return _facade.GetEmailInformAll(workflowTableId);

                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }

            }

        }
        [WebMethod]
        public string GetEmailInformCashClearing(int workflowTableId)
        {

            using (Resolver.BeginScope())
            {

                try
                {
                    var _facade = Resolver.GetInstance<ICashClearingFacade>();
                    return _facade.GetEmailInformAll(workflowTableId);

                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }

            }

        }
        [WebMethod]
        public string GetEmailByUsername(string username)
        {
            using (Resolver.BeginScope())
            {
                try
                {
                    var service = Resolver.GetInstance<IEmployeeTableService>();
                    var employee = service.FindByUsername(username);
                    return employee != null ? employee.Email : string.Empty;
                }
                catch(Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        [WebMethod]
        public string GetEmailInformForApCode(int workflowTableId)
        {
            using (Resolver.BeginScope())
            {
                try
                {
                    var _proposalFacade = Resolver.GetInstance<IProposalFacade>();
                    return _proposalFacade.GetEmailInformForAp(workflowTableId);

                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }

            }
        }
        [WebMethod]
        public List<EmailForAPCodeViewModel> GetEmailInformForApCodeList(int workflowTableId)
        {
            using (Resolver.BeginScope())
            {
                try
                {
                    var _proposalFacade = Resolver.GetInstance<IProposalFacade>();
                    return _proposalFacade.GetEmailInformForApList(workflowTableId);

                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }

            }
        }

        // Get Email Inform MemoIncome
        [WebMethod] 
        public string GetEmailInformMemoIncome(int workflowTableId)
        {
            using (Resolver.BeginScope())
            {
                try
                {
                    var _memoincomefacade = Resolver.GetInstance<IMemoIncomeFacade>();
                    return _memoincomefacade.GetEmailInformAll(workflowTableId);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        // Reset Status Approve MemoIncome
        [WebMethod]
        public void ResetStatusApproveMemoIncome(int workflowTableId)
        {
            using (Resolver.BeginScope())
            {
                try
                {
                    var _memoIncomeapprovalService = Resolver.GetInstance<IMemoIncomeApprovalService>();
                    _memoIncomeapprovalService.ResetStatusApproveMemoIncome(workflowTableId);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }
    }
}