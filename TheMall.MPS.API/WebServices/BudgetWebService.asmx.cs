﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using TheMall.MPS.ViewModels.Proposal;
using TheMall.MPS.API.Services;
using TheMall.MPS.API.Facades;
using TheMall.MPS.API.Mappers;
using TheMall.MPS.Models;
using TheMall.MPS.Models.Enums;


namespace TheMall.MPS.API.WebServices
{
    /// <summary>
    /// Summary description for Service1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class BudgetWebService : System.Web.Services.WebService
    {

        [WebMethod]
        public bool CreateBudgetYearTrans(int proposalId)
        {
            using (Resolver.BeginScope())
            {
                var _budgetYearTransFacade = Resolver.GetInstance<IBudgetYearTransFacade>();
                return _budgetYearTransFacade.CreateTrans(proposalId);
            }
        }

        [WebMethod]
        public bool CancelBudgetYearTrans(int proposalId)
        {
            using (Resolver.BeginScope())
            {
                var result = false;
                var _proposalService = Resolver.GetInstance<IProposalService>();
                var propType = _proposalService.Find(m => m.Id == proposalId);
                if (propType != null && !string.IsNullOrEmpty(propType.ProposalTypeCode))
                {
                    if (propType.ProposalTypeCode.ToUpper() == ProposalType.ProposalTypeCode.DepositInternal)
                    {
                        var _budgetDepositTransFacade = Resolver.GetInstance<IBudgetDepositTransFacade>();
                        result = _budgetDepositTransFacade.CancelProposalDepositTrans(proposalId);
                    }
                    else
                    {
                        if (propType.ProposalTypeCode.ToUpper() != ProposalType.ProposalTypeCode.Deposit)
                        {
                            foreach (var income in propType.IncomeDeposit)
                            {
                                var _budgetDepositTransFacade = Resolver.GetInstance<IBudgetDepositTransFacade>();
                                result = _budgetDepositTransFacade.CancelDepositTransStepCreateIncome(income);
                            }
                           
                        }
                        var _budgetYearTransFacade = Resolver.GetInstance<IBudgetYearTransFacade>();
                       result =  _budgetYearTransFacade.CancelTrans(proposalId);
                    }
                }


                return result;
            }
        }

        [WebMethod]
        public bool ChangeBudgetYearTrans(int proposalId)
        {
            using (Resolver.BeginScope())
            {
                var _budgetYearTransFacade = Resolver.GetInstance<IBudgetYearTransFacade>();
                return _budgetYearTransFacade.ChangeTrans(proposalId);
            }
        }
        [WebMethod]
        public bool MemoReturnBudgetTrans(int memoId)
        {
            using (Resolver.BeginScope())
            {
                var _budgetYearTransFacade = Resolver.GetInstance<IBudgetYearTransFacade>();
                return _budgetYearTransFacade.MemoSpecialReturnTrans(memoId);
            }
        }
       
    }
}