﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using CrossingSoft.Framework.Infrastructure.K2;
using TheMall.MPS.ViewModels.Proposal;
using TheMall.MPS.API.Services;
using TheMall.MPS.API.Facades;
using TheMall.MPS.API.Facades.Abstracts;
using TheMall.MPS.API.Facades.Interfaces;
using TheMall.MPS.API.Mappers;


namespace TheMall.MPS.API.WebServices
{
    /// <summary>
    /// Summary description for Service1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WorkflowWebService : System.Web.Services.WebService
    {

        [WebMethod]
        public bool Submit(string sn, string action)
        {

            using (Resolver.BeginScope())
            {
                var success = false;
                // //LogStart();

                try
                {
                    var _workflowRepository = Resolver.GetInstance<IWorkflowRepository>();
                   
                    _workflowRepository.ActionWorklistItem(sn, action, false);
                     success = true;
                   
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
                return success;
            }

        }

        //[WebMethod]
        //public bool Submit(string processcode, string sn)
        //{

        //    using (Resolver.BeginScope())
        //    {
        //        var success = false;
        //        // //LogStart();
        //        try
        //        {


        //            if (!string.IsNullOrEmpty(processcode))
        //            {
        //                switch (processcode.ToLower())
        //                {
        //                    case Budget.DocumentType.Proposal:
        //                        {
        //                            var _facade = Resolver.GetInstance<IProposalFacade>();
        //                            var _service = Resolver.GetInstance<IProposalService>();
        //                            var _mapper = Resolver.GetInstance<IProposalMapper>();
        //                            int id = _service.GetWorkflowTableIdFromSN(sn).GetValueOrDefault();
        //                            var viewModel = _mapper.ToViewModel(_service.GetWorkflowTable(id));
        //                            var comment = _facade.Submit(sn, "Approve", String.Empty, viewModel);
        //                            if (comment != null)
        //                                success = true;
        //                        }
        //                        break;
        //                    default:
        //                        success = false;
        //                        break;
        //                }

        //            }
        //            //var _workflowRepository = Resolver.GetInstance<IWorkflowRepository>();

        //            //_workflowRepository.ActionWorklistItem(sn, action, false);
        //            success = true;


        //        }
        //        catch (Exception ex)
        //        {
        //            throw new Exception(ex.Message);
        //        }
        //        return success;
        //    }

        //}
    }
}