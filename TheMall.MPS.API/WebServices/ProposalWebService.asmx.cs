﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using TheMall.MPS.ViewModels.Proposal;
using TheMall.MPS.API.Services;
using TheMall.MPS.API.Facades;
using TheMall.MPS.API.Mappers;


namespace TheMall.MPS.API.WebServices
{
    /// <summary>
    /// Summary description for Service1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class ProposalWebService : System.Web.Services.WebService
    {
        [WebMethod]
        public string TestWebservice(int parentId, int procInstId, string depositRefDoc)
        {
            return "hello";
        }

        [WebMethod]
        public List<string> GetDepositApprover(int parentId, int procInstId , string depositRefDoc)
        {
            var listUser = new List<string>();
            using (Resolver.BeginScope())
            {
                if (!string.IsNullOrEmpty(depositRefDoc) && parentId != 0 && procInstId != 0)
                {
                    var _incomeDepositService = Resolver.GetInstance<IProposalIncomeDepositService>();
                    var model = _incomeDepositService.LastOrDefault(m => m.ParentId == parentId
                                                                && m.ProcInstId == procInstId
                                                                &&
                                                                m.ProposalDepositRefDoc.Equals(depositRefDoc,
                                                                    StringComparison.InvariantCultureIgnoreCase)
                                                                && string.IsNullOrEmpty(m.Status)
                                                                && !m.Deleted);
                    if (model != null && !string.IsNullOrEmpty(model.DepositApprover))
                    {
                        //var lastIndex = model.DepositApprover.LastIndexOf(';');
                        string[] approver = model.DepositApprover.TrimEnd(';').Split(';');
                        listUser.AddRange(approver);
                    }
                }
                return listUser;
            }
        }

        [WebMethod]
        public string  ValidateNewDeposit( string depositRefDoc, int procInstId,int parentId)
        {
            string depositDocumentNumber = string.Empty;
             using (Resolver.BeginScope())
             {
                 try
                 {
                     if (!string.IsNullOrEmpty(depositRefDoc) && parentId != 0 && procInstId != 0)
                     {
                         var _incomeDepositService = Resolver.GetInstance<IProposalIncomeDepositService>();
                         var model = _incomeDepositService.GetWhere(m => m.ParentId == parentId
                                                                               && m.ProcInstId == procInstId
                                                                               &&
                                                                               m.ProposalDepositRefDoc.Equals(depositRefDoc,
                                                                                   StringComparison
                                                                                       .InvariantCultureIgnoreCase)
                                                                               && !m.Deleted
                             );

                         depositDocumentNumber = (model != null && model.Sum(m => m.Budget) == 0) ? depositDocumentNumber : depositRefDoc;
                     }
                     return depositDocumentNumber;
                
                 }
                 catch (Exception ex)
                 {
                     
                     throw  new Exception(ex.Message);
                 }

                 
            }
             
            }

        [WebMethod]
        public bool CheckStartNewProcess(int workflowtTableId)
        {
            using (Resolver.BeginScope())
            {
                try
                {
                    var result = false;
                    var _propservice = Resolver.GetInstance<IProposalService>();
                    var _proposalLogFacade = Resolver.GetInstance<IProposalLogFacade>();
                    var model = _propservice.Find(m => m.Id == workflowtTableId);
                    if (model != null)
                    {
                        var logResult = _proposalLogFacade.TrackChanges(workflowtTableId);
                       result =  _proposalLogFacade.CheckStartNewProcess(model);
                    }
                    return result;
                }
                catch (Exception ex)
                {

                    throw new Exception(ex.Message);
                }


            }
        }


        [WebMethod]
        public bool ProposalRevise_Completed(int workflowtTableId)
        {
            using (Resolver.BeginScope())
            {
                try
                {
                    var _proposalFacade = Resolver.GetInstance<IProposalFacade>();
                    var result = _proposalFacade.UpdateReviseCompleted(workflowtTableId);
                    return result;
                }
                catch (Exception ex)
                {

                    throw new Exception(ex.Message);
                }


            }
        }
        [WebMethod]
        public bool ProposalRevise_Rejected(int workflowtTableId)
        {
            using (Resolver.BeginScope())
            {
                try
                {
                    var _proposalLogFacade = Resolver.GetInstance<IProposalLogFacade>();
                    var result = _proposalLogFacade.CancelRevise(workflowtTableId);
                    return result;
                }
                catch (Exception ex)
                {

                    throw new Exception(ex.Message);
                }


            }
        }
        [WebMethod]
        public bool ProposalRevise_Cancelled(int workflowtTableId)
        {
            using (Resolver.BeginScope())
            {
                try
                {
                    var _proposalLogFacade = Resolver.GetInstance<IProposalLogFacade>();
                    var result = _proposalLogFacade.CancelRevise(workflowtTableId);
                    return result;
                }
                catch (Exception ex)
                {

                    throw new Exception(ex.Message);
                }


            }
        }

    }
}