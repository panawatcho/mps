﻿using System;
using System.Collections.Generic;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Models.Abstracts;

namespace TheMall.MPS.API.Factories
{
    public interface IMPSWorkflowServiceFactory
    {
        IMpsWorkflowTableService CreateNew(string processCode);
    }
    public class MPSWorkflowServiceFactory : Dictionary<string, Func<IMpsWorkflowTableService>>, IMPSWorkflowServiceFactory
    {
        public IMpsWorkflowTableService CreateNew(string processCode)
        {
            return this[processCode]();
        }
    }
}