﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TheMall.MPS.API.Services;
using TheMall.MPS.Infrastructure.Extension;
using TheMall.MPS.Models.CashAdvance;
using TheMall.MPS.Models.Enums;
using TheMall.MPS.Models.Proposal;
using TheMall.MPS.Reports.CashAdvance;
using TheMall.MPS.Reports.Shared;
using TheMall.MPS.ViewModels.CashAdvance;

namespace TheMall.MPS.API.Mappers
{
    public interface IReportCashAdvanceMapper
    {
        IEnumerable<CashAdvanceDataSource> ReportDataMapper(MPS_CashAdvanceTable cashAdvanceViewModel);
        IEnumerable<ApproverDataSource> ApproversDataMapper(IEnumerable<MPS_CashAdvanceApproval> approversData);
        IEnumerable<ProposalLineTemplateDataSource> ProposalLineTemplateDataMapper(
            IEnumerable<MPS_ProposalLineSharedTemplate>proposalLineSharedTemplates,
            IEnumerable<MPS_ProposalLineTemplate> proposalLineTemplates);
    }

    public class ReportCashAdvanceMapper : IReportCashAdvanceMapper
    {
        private readonly ICashAdvanceMapper _cashAdvanceMapper;
        private readonly ICompanyService _companyService;

        public ReportCashAdvanceMapper(ICashAdvanceMapper cashAdvanceMapper, 
            ICompanyService companyService)
        {
            _cashAdvanceMapper = cashAdvanceMapper;
            _companyService = companyService;
        }

        public IEnumerable<CashAdvanceDataSource> ReportDataMapper(MPS_CashAdvanceTable cashAdvanceModel)
        {
            var cashAdvanceViewModel = _cashAdvanceMapper.ToViewModel(cashAdvanceModel);
            var list = new List<CashAdvanceDataSource>();
            
            if (cashAdvanceViewModel != null)
            {
                foreach (var line in cashAdvanceViewModel.CashAdvanceLines)
                {
                    var reportDataSource = new CashAdvanceDataSource() {};
                    reportDataSource.Id = line.Id;
                    if (cashAdvanceViewModel.Branch != null)
                    {
                        reportDataSource.Branch = cashAdvanceViewModel.Branch.PlaceName;
                        reportDataSource.RequesterBranch = cashAdvanceViewModel.Branch.PlaceName;
                        reportDataSource.BranchDescription = cashAdvanceViewModel.Branch.Description;
                    }
                    else
                    {
                        reportDataSource.Branch = "";
                    }

                    if (cashAdvanceViewModel.Requester != null)
                    {
                        reportDataSource.RequesterName = cashAdvanceViewModel.Requester.FullName;
                        //reportDataSource.RequesterBranch = cashAdvanceViewModel.Requester.CompanyName;
                        //if (cashAdvanceViewModel.Branch != null)
                        //    reportDataSource.RequesterBranch = cashAdvanceViewModel.Branch.PlaceName;
                        reportDataSource.RequesterDepositName = cashAdvanceViewModel.Requester.PositionName;
                    }
                    else
                    {
                        reportDataSource.RequesterName = "";
                        //reportDataSource.RequesterBranch = "";
                        reportDataSource.RequesterDepositName = "";
                    }
                   
                    if (cashAdvanceViewModel.DocumentNumber != null)
                    {
                        reportDataSource.DocumentNumber = cashAdvanceViewModel.DocumentNumber;
                    }
                    else
                    {
                        reportDataSource.DocumentNumber = "";
                    }
                    //reportDataSource.BudgetInCharacter = cashAdvanceViewModel.Budget.ToWords();
                    reportDataSource.LineDescription = line.Description;
                    reportDataSource.LineAmount = line.Amount;
                    reportDataSource.LineUnit = line.Unit;
                    reportDataSource.LineNetAmount = line.NetAmount;
                    reportDataSource.LineRemark = line.Remark;
                    reportDataSource.Tax = line.Tax;
                    reportDataSource.Vat = line.Vat;
                    reportDataSource.Budget = (decimal) cashAdvanceViewModel.CashAdvanceLines.Sum(m => m.NetAmount); //cashAdvanceViewModel.Budget;
                    reportDataSource.BudgetInCharacter = reportDataSource.Budget.ToWords();
                    reportDataSource.Date = DateTime.Now.ToString("dd/MM/yyyy");
                    if (cashAdvanceViewModel.UnitCode != null)
                    {
                        reportDataSource.UnitCode = cashAdvanceViewModel.UnitCode.UnitCode;
                        reportDataSource.UnitName = cashAdvanceViewModel.UnitCode.UnitName;
                    }

                    if (cashAdvanceViewModel.ProposalRef != null)
                    {
                        if (cashAdvanceViewModel.ProposalRef.ProposalUnitCode != null)
                        {
                            reportDataSource.ProposalUnitCode = cashAdvanceViewModel.ProposalRef.ProposalUnitCode.UnitCode;
                            reportDataSource.ProposalUnitName = cashAdvanceViewModel.ProposalRef.ProposalUnitCode.UnitName;
                        }
                        reportDataSource.StartDate = cashAdvanceViewModel.ProposalRef.StartDate.ToString("dd/MM/yyyy");
                        reportDataSource.EndDate = cashAdvanceViewModel.ProposalRef.EndDate.ToString("dd/MM/yyyy");
                        reportDataSource.ProposalNumber = cashAdvanceViewModel.ProposalRef.DocumentNumber;
                        reportDataSource.ProposalName = cashAdvanceViewModel.ProposalRef.Title;
                    }

                    if (cashAdvanceViewModel.SubmittedDate != null)
                    {
                        DateTime submitDate = (DateTime)cashAdvanceViewModel.SubmittedDate;
                        reportDataSource.SubmitDate = submitDate.ToString("dd/MM/yyyy");
                    }

                    if (cashAdvanceViewModel.Description != null)
                    {
                        reportDataSource.Objective = cashAdvanceViewModel.Description.Replace("font-family", string.Empty);
                        var imgs = reportDataSource.Objective.Contains("img");
                        reportDataSource.ObjectiveImg = imgs ? "*ดูรูปเพิ่มเติมได้ในระบบ" : "";
                    }

                    if (cashAdvanceViewModel.Company != null)
                    {
                        var company = _companyService.Find(m => m.VendorCode.Equals(cashAdvanceViewModel.Company.VendorCode));
                        reportDataSource.Company = company.CompanyName;
                    }
                    else
                    {
                        var dataCompany = _companyService.Find(m => m.VendorCode.Equals(cashAdvanceViewModel.Branch.PlaceCode));
                        reportDataSource.Company = dataCompany.CompanyName;
                    }

                    if (cashAdvanceViewModel.ExpenseTopics != null)
                    {
                        reportDataSource.ExpenseTopic = cashAdvanceViewModel.ExpenseTopics.ExpenseTopicCode + " - " + cashAdvanceViewModel.ExpenseTopics.ExpenseTopicName;
                    }

                    if (cashAdvanceViewModel.APCode != null)
                    {
                        reportDataSource.APCode = cashAdvanceViewModel.APCode.APCode + " - " + cashAdvanceViewModel.APCode.APName;
                    }


                    list.Add(reportDataSource);
                }
                return list;
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<ApproverDataSource> ApproversDataMapper(IEnumerable<MPS_CashAdvanceApproval> approversData)
        {
            if (approversData != null)
            {
                var list = new List<ApproverDataSource>();
                foreach (var approverData in approversData)
                {
                    var reportApproverDataSource = new ApproverDataSource() {};
                    reportApproverDataSource.ApproverId = approverData.Id;
                    reportApproverDataSource.ApproverSequence = approverData.ApproverSequence;
                    reportApproverDataSource.ApproverUsername = approverData.ApproverUserName;
                    reportApproverDataSource.ApproverFullName = approverData.ApproverFullName;
                    reportApproverDataSource.ApproverLevel = approverData.ApproverLevel;
                    reportApproverDataSource.ApproverPosition = approverData.Position;
                    var comment = approverData.CashAdvanceTable.Comments;
                    var approverCheck = approverData.ApproverLevel + " - Approval";
                    if (comment != null)
                    {
                        comment = comment.Where(m => m.Activity.ToLower() != "start").ToList();
                        var approve = comment.LastOrDefault(m => m.CreatedBy.Equals(approverData.ApproverUserName, StringComparison.InvariantCultureIgnoreCase) &&
                      m.Action.Equals(DocumentStatusFlag.Approve.ToString(), StringComparison.InvariantCultureIgnoreCase) &&
                      m.Activity.Equals(approverCheck, StringComparison.InvariantCultureIgnoreCase));
                        if (approve != null)
                        {
                            reportApproverDataSource.ApproveDateTime = approve.CreatedDate != null ? approve.CreatedDate.Value.ToString("dd/MM/yyyy") : "";
                        }
                        else
                        {
                            reportApproverDataSource.ApproveDateTime = "";
                        }
                    }
                    if (approverData.ApproverLevel.Equals(Approval.ApproveLevelList.Propose.GetDescription()))
                        reportApproverDataSource.ApproverLevelKey = Approval.ApproveKey.Propose;
                    else if (approverData.ApproverLevel.Equals(Approval.ApproveLevelList.Accept.GetDescription()))
                        reportApproverDataSource.ApproverLevelKey = Approval.ApproveKey.Accept;
                    else if (approverData.ApproverLevel.Equals(Approval.ApproveLevelList.Approval.GetDescription()))
                        reportApproverDataSource.ApproverLevelKey = Approval.ApproveKey.Approval;
                    else if (approverData.ApproverLevel.Equals(Approval.ApproveLevelList.FinalApproval.GetDescription()))
                        reportApproverDataSource.ApproverLevelKey = Approval.ApproveKey.FinalApproval;
                    else
                    {
                        reportApproverDataSource.ApproverLevelKey = 0;
                    }

                    list.Add(reportApproverDataSource);
                }
                return list;
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<ProposalLineTemplateDataSource> ProposalLineTemplateDataMapper(IEnumerable<MPS_ProposalLineSharedTemplate> proposalLineSharedTemplates ,
            IEnumerable<MPS_ProposalLineTemplate> proposalLineTemplates)
        {
            var list = new List<ProposalLineTemplateDataSource>();
            var branchCode = proposalLineSharedTemplates.Select(m => m.BranchCode);
            var result = proposalLineTemplates.Where(m => branchCode.Contains(m.BranchCode));
            if (result != null)
            {                
                foreach (var line in result)
                {
                    var data = new ProposalLineTemplateDataSource();
                    data.BranchCode = line.BranchCode;
                    data.RE_Percent = line.RE_Percent;
                    data.TR_Percent = line.TR_Percent;
                    list.Add(data);
                }
                return list;
            }
            else
            {
                return null;
            }
        }
    }
}