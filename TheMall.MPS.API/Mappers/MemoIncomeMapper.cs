﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using TheMall.MPS.API.Mappers.Abstracts;
using TheMall.MPS.API.Services;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Models.Enums;
using TheMall.MPS.Models.Master;
using TheMall.MPS.Models.MemoIncome;
using TheMall.MPS.ViewModels;
using TheMall.MPS.ViewModels.Master;
using TheMall.MPS.ViewModels.MemoIncome;

namespace TheMall.MPS.API.Mappers
{
    public interface IMemoIncomeMapper : IWorkflowTableMapper<MPS_MemoIncomeTable>
    {
        MPS_MemoIncomeTable ToModel(MemoIncomeViewModel viewModel);
        MemoIncomeViewModel ToViewModel(MPS_MemoIncomeTable model);
        MPS_MemoIncomeTable DeleteBy(MPS_MemoIncomeTable model, EmployeeViewModel employeeViewModel);
        IEnumerable<MemoIncomeViewModel> ToViewModels(IEnumerable<MPS_MemoIncomeTable> models);
        IEnumerable<MemoIncomeAccViewModel> ToAccViewModel(IEnumerable<MPS_MemoIncomeTable> models);
    }

    public class MemoIncomeMapper : BaseWorkflowTableMapper<MPS_MemoIncomeTable>, IMemoIncomeMapper
    {
        private readonly IEmployeeTableService _employeeTableService;
        private readonly IProposalIncomeOtherService _incomeOtherService;
        private readonly IProposalIncomeOtherMapper _incomeOtherMapper;
        private readonly IMemoIncomeLineMapper _incomeLineMapper;
        private readonly IMemoIncomeApprovalMapper _incomeApprovalMapper;
        private readonly IProposalService _proposalService;
        private readonly IProposalMapper _proposalMapper;
        private readonly IWorkflowService _workflowService;
        private readonly IPlaceService _placeService;
        private readonly IMemoIncomeInvoiceMapper _incomeInvoiceMapper;
        private readonly IProposalDepositLineService _depositLineService;
        private readonly IProposalDepositLineMapper _depositLineMapper;
        private readonly IAccountingMapper _accountingMapper;

      public MemoIncomeMapper(IMemoIncomeService workflowTableService, 
          IEmployeeTableService employeeTableService, 
          IPlaceService placeService, 
          IAccountingService accountingService,
          IProposalIncomeOtherService incomeOtherService,
          IProposalIncomeOtherMapper incomeOtherMapper, 
          IMemoIncomeLineMapper incomeLineMapper,
          IMemoIncomeApprovalMapper incomeApprovalMapper, 
          IProposalService proposalService, 
          IProposalMapper proposalMapper, 
          IWorkflowService workflowService, 
          IMemoIncomeInvoiceMapper incomeInvoiceMapper,
          IProposalDepositLineService depositLineService,
          IProposalDepositLineMapper depositLineMapper,
          IAccountingMapper accountingMapper)
            : base(workflowTableService, 
                employeeTableService, 
                placeService, 
                accountingService)
        {
            _employeeTableService = employeeTableService;
            _incomeOtherService = incomeOtherService;
            _incomeOtherMapper = incomeOtherMapper;
            _incomeLineMapper = incomeLineMapper;
            _incomeApprovalMapper = incomeApprovalMapper;
            _proposalService = proposalService;
            _proposalMapper = proposalMapper;
            _workflowService = workflowService;
            _incomeInvoiceMapper = incomeInvoiceMapper;
            _placeService = placeService;
            _depositLineService = depositLineService;
            _depositLineMapper = depositLineMapper;
            _accountingMapper = accountingMapper;
        }

        public MPS_MemoIncomeTable ToModel(MemoIncomeViewModel viewModel)
        {
            if (viewModel == null)
            {
                return null;
            }

            // memoincometable viewmodel
            var model = InitWorkflowTable(viewModel);

            if (viewModel.ProposalRef != null)
            {
                model.ProposalRefID = viewModel.ProposalRef.Id;
                model.ProposalRefDocumentNumber = viewModel.ProposalRef.DocumentNumber;
                model.ProposalTypeCode = viewModel.ProposalRef.ProposalType.TypeProposalCode;
            }

            model.Category = viewModel.Category;
            model.TotalAmount = viewModel.TotalAmount;
            model.Description = viewModel.Description;
            model.AccountingStatus = viewModel.AccountingStatus;

            if (viewModel.MemoIncomeInvoice != null)
            {
                model.Actual = (decimal)viewModel.MemoIncomeInvoice.Where(m => !m.Deleted).Sum(m => m.Actual);
            }
            
            model.BudgetDetail = viewModel.BudgetDetail;
            model.DocumentNumber = viewModel.DocumentNumber;

            if (viewModel.Branch != null)
            {
                model.Branch = viewModel.Branch.PlaceCode;
            }

            if (viewModel.IncomeOther != null)
            {
                model.OtherIncomeRefId = viewModel.IncomeOther.Id;
            }

            if (viewModel.DepositLine != null)
            {
                model.DepositLineId = viewModel.DepositLine.Id;
            }

            return model;
        }

        //
        public MemoIncomeViewModel ToViewModel(MPS_MemoIncomeTable model)
        {
            if (model == null)
            {
                return null;
            }

            var viewModel = InitViewModel<MemoIncomeViewModel>(model);

            viewModel.Requester = _employeeTableService.FindByUsername(model.RequesterUserName).ToViewModel();
            viewModel.TotalAmount = model.TotalAmount;
            viewModel.Category = model.Category;
            viewModel.Description = model.Description;
            viewModel.AccountingStatus = model.AccountingStatus;
            viewModel.Actual = (model.Actual == null) ? 0 : model.Actual; //
            viewModel.Comments = model.Comments.ToViewModel();
            viewModel.BudgetDetail = model.BudgetDetail;
            viewModel.TypeProposal = model.TypeProposal.ToViewModel();
            viewModel.DocumentNumber = model.DocumentNumber;

            viewModel.IncomeOther = (model.OtherIncomeRefId != null && model.IncomeOther != null) ? _incomeOtherMapper.ToViewModel(model.IncomeOther) : null;

            viewModel.DepositLine = (model.DepositLineId != null && model.DepositLine != null) ? _depositLineMapper.ToViewModel(model.DepositLine) : null;

            if (model.Branch != null)
            {
                var branch = _placeService.FirstOrDefault(m => m.PlaceCode.Equals(model.Branch));
                viewModel.Branch = branch.ToViewModel();
            }
            if (model.MemoIncomeLine != null)
            {
                viewModel.MemoIncomeLines = _incomeLineMapper.ToViewModels(model.MemoIncomeLine);
            }
          
            viewModel.MemoIncomeInvoice = (model.MemoIncomeInvoice!=null)?_incomeInvoiceMapper.ToViewModels(model.MemoIncomeInvoice):new List<MemoIncomeInvoiceViewModel>();
          
            if (model.MemoIncomeApproval != null)
            {
                viewModel.MemoIncomeApproval = _incomeApprovalMapper.ToViewModels(model.MemoIncomeApproval);
            }

            if (model.ProposalRefID != null && model.ProposalTable != null)
            {
                viewModel.ProposalRef = _proposalMapper.ToProposalRefViewModel(model.ProposalTable);
            }

            return viewModel;
        }

        public MPS_MemoIncomeTable DeleteBy(MPS_MemoIncomeTable model, EmployeeViewModel employeeViewModel)
        {
            if (model != null)
            {
                if (model.Id > 0)
                {
                    model.Deleted = true;
                    model.DeletedDate = DateTime.Now;
                    model.DeletedBy = employeeViewModel.Username;
                    model.DeletedByName = employeeViewModel.FullName;
                    return model;
                } else {
                    return null;
                }
            } else {
                return null;
            }
        }

        public IEnumerable<MemoIncomeViewModel> ToViewModels(IEnumerable<MPS_MemoIncomeTable> models)
        {
            return models != null ? models.Select(ToViewModel) : Enumerable.Empty<MemoIncomeViewModel>();
        }

        public MemoIncomeAccViewModel ToAccViewModel(MPS_MemoIncomeTable model)
        {
            if (model == null)
            {
                return null;
            }
          
            var indexViewModel = new MemoIncomeAccViewModel();

            if (model.Id != null)
            {
                indexViewModel.Id = model.Id;
                indexViewModel.TotalAmount = model.TotalAmount;
                indexViewModel.Category = model.Category;
                indexViewModel.DocumentNumber = model.DocumentNumber;
                indexViewModel.Status = model.Status;
                indexViewModel.StatusFlag = model.StatusFlag;
                indexViewModel.CreatedDate = model.CreatedDate;
                indexViewModel.Title = model.Title;

                if (model.ProposalRefID != null && model.ProposalTable != null)
                {
                    indexViewModel.ProposalRefID = model.ProposalRefID;
                    indexViewModel.ProposalRefDocumentNumber = model.ProposalRefDocumentNumber;
                }
            }
            
            return indexViewModel;
        }

        public IEnumerable<MemoIncomeAccViewModel> ToAccViewModel(IEnumerable<MPS_MemoIncomeTable> models)
        {
            return models != null ? models.Select(ToAccViewModel) : Enumerable.Empty<MemoIncomeAccViewModel>();
        }
    }
}