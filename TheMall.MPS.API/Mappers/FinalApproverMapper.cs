﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using TheMall.MPS.Models.Master;
using TheMall.MPS.ViewModels.CashAdvance;
using TheMall.MPS.ViewModels.CashClearing;
using TheMall.MPS.ViewModels.Master;
using TheMall.MPS.ViewModels.Proposal;

namespace TheMall.MPS.API.Mappers
{

    public static class FinalApproverMapper
    {
        public static ProposalApprovalViewModel ToProposalApproval(this MPS_M_FinalApprover model)
        {
            if (model == null)
                return null;
            var viewModel = new ProposalApprovalViewModel()
            {
                Employee = model.Employee.ToViewModel(),
                ApproverSequence = 1,
                ApproverLevel = "อนุมัติขั้นสุดท้าย",
                ApproverUserName = model.Username,
                ApproverFullName = model.Employee.ThFullName,
                ApproverEmail = model.Employee.Email,
                Position = model.Employee.PositionName,
                Department = model.Employee.DepartmentName,
                DueDate = DateTime.Now,
                LockEditable = true,
                LineNo = 1
            };
            return viewModel;
        }
        public static MemoApprovalViewModel ToMemoApproval(this MPS_M_FinalApprover model)
        {
            if (model == null)
                return null;
            var viewModel = new MemoApprovalViewModel()
            {
                Employee = model.Employee.ToViewModel(),
                ApproverSequence = 1,
                ApproverLevel = "อนุมัติขั้นสุดท้าย",
                ApproverUserName = model.Username,
                ApproverFullName = model.Employee.ThFullName,
                ApproverEmail = model.Employee.Email,
                Position = model.Employee.PositionName,
                Department = model.Employee.DepartmentName,
                DueDate = DateTime.Now,
                LockEditable = true,
                LineNo = 1
            };
            return viewModel;
        }
        public static PettyCashApprovalViewModel ToPettyCashApproval(this MPS_M_FinalApprover model)
        {

             if (model == null)
                return null;
                var viewModel = new PettyCashApprovalViewModel()
                {
                    Employee = model.Employee.ToViewModel(),
                    ApproverSequence = 1,
                    ApproverLevel = "อนุมัติขั้นสุดท้าย",
                    ApproverUserName = model.Username,
                    ApproverFullName = model.Employee.ThFullName,
                    ApproverEmail = model.Employee.Email,
                    Position = model.Employee.PositionName,
                    Department = model.Employee.DepartmentName,
                    DueDate = DateTime.Now,
                    LockEditable = true,
                    LineNo = 1
                };
                return viewModel;
           

        }
        public static CashAdvanceApprovalViewModel ToCashAdvanceApproval(this MPS_M_FinalApprover model)
        {
            if (model == null)
                return null;
            var viewModel = new CashAdvanceApprovalViewModel();
            if (model.Employee != null)
            {
                viewModel.Employee = model.Employee.ToViewModel();
                viewModel.Position = model.Employee.PositionName;
                viewModel.Department = model.Employee.DepartmentName;
                viewModel.ApproverFullName = model.Employee.ThFullName;
                viewModel.ApproverEmail = model.Employee.Email;
            }


            viewModel.ApproverSequence = 1;
            viewModel.ApproverLevel = "อนุมัติขั้นสุดท้าย";
            viewModel.ApproverUserName = model.Username;
            viewModel.DueDate = DateTime.Now;
            viewModel.LockEditable = true;
            viewModel.LineNo = 1;
           
            return viewModel;
        }
        public static CashClearingApprovalViewModel ToCashClearingApproval(this MPS_M_FinalApprover model)
        {
            if (model == null)
                return null;
            var viewModel = new CashClearingApprovalViewModel()
            {
                Employee = model.Employee.ToViewModel(),
                ApproverSequence = 1,
                ApproverLevel = "อนุมัติขั้นสุดท้าย",
                ApproverUserName = model.Username,
                ApproverFullName = model.Employee.ThFullName,
                ApproverEmail = model.Employee.Email,
                Position = model.Employee.PositionName,
                Department = model.Employee.DepartmentName,
                DueDate = DateTime.Now,
                LineNo = 1
            };
            return viewModel;
        }
        public static FinalApproverViewModel ToViewModel(this MPS_M_FinalApprover model)
        {
            if (model == null)
                return null;
            var viewModel = new FinalApproverViewModel()
            {
                ProcessCode = model.ProcessCode,
                EmpId = model.EmpId,
                Username = model.Username,
                UnitCode = model.UnitCode,
                Amount = model.Amount,
                InActive = model.InActive,
               

            };
            return viewModel;

        }
        public static IEnumerable<FinalApproverViewModel> ToViewModels(this IEnumerable<MPS_M_FinalApprover> models)
        {
            return models != null ? models.Select(ToViewModel) : Enumerable.Empty<FinalApproverViewModel>();
        }
        public static MPS_M_FinalApprover ToModel(this FinalApproverViewModel viewModel)
        {
            if (viewModel == null)
                return null;
            var model = new MPS_M_FinalApprover();
            if (viewModel.UnitCodeViewModel != null)
            {
                model.UnitCode = viewModel.UnitCodeViewModel.UnitCode;
            }

            if (viewModel.EmployeeViewModel != null)
            {
                model.Username = viewModel.EmployeeViewModel.Username;
                model.EmpId = viewModel.EmployeeViewModel.EmpId;
            }

            if (viewModel.ProcessApproveViewModel != null)
            {
                model.ProcessCode = viewModel.ProcessApproveViewModel.ProcessCode;
            }
            model.Amount = viewModel.Amount;
            model.InActive = viewModel.InActive;
            //{
            //    ProcessCode = viewModel.ProcessCode,
            //    EmpId = viewModel.EmpId,
            //    Username = viewModel.Username,
            //    UnitCode = viewModel.UnitCode,
            //    Amount = viewModel.Amount,
            //    InActive = viewModel.InActive,

            //};
            return model;
        }
        public static IEnumerable<MPS_M_FinalApprover> ToModels(IEnumerable<FinalApproverViewModel> viewModels, IEnumerable<MPS_M_FinalApprover> lines)
        {
            return viewModels != null ? viewModels.Select(ToModel) : Enumerable.Empty<MPS_M_FinalApprover>();
        }
   
    }
}