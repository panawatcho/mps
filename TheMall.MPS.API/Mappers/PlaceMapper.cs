﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using TheMall.MPS.Models.Master;
using TheMall.MPS.ViewModels.Master;

namespace TheMall.MPS.API.Mappers
{
    public class PlaceMapperProfile : AutoMapper.Profile
    {
        public PlaceMapperProfile()
        {
            CreateMap<MPS_M_Place, PlaceViewModel>()
              .ReverseMap();
        }
    }
    public static class PlaceMapper
    {
        public static MPS_M_Place ToModel(this PlaceViewModel viewModel)
        {
            if (viewModel == null)
                return null;
            var model = new MPS_M_Place()
            {
                PlaceCode = viewModel.PlaceCode,
                PlaceName = viewModel.PlaceName,
                Description = viewModel.Description,
                InActive = viewModel.InActive,
                Order = viewModel.Order
            };
            return model;

            return Mapper.Map<MPS_M_Place>(viewModel);
        }

        public static PlaceViewModel ToViewModel(this MPS_M_Place model)
        {
            if (model == null)
                return null;
            var viewModel = new PlaceViewModel()
            {
                PlaceCode = model.PlaceCode,
                PlaceName = model.PlaceName,
                InActive = model.InActive,
                Description = model.Description,
                Order = model.Order
            };
            return viewModel;

            return Mapper.Map<PlaceViewModel>(model);
        }
        public static IEnumerable<PlaceViewModel> ToViewModels(this IEnumerable<MPS_M_Place> models)
        {
            return models != null ? models.Select(ToViewModel) : Enumerable.Empty<PlaceViewModel>();
        }
        public static IEnumerable<MPS_M_Place> ToModels(IEnumerable<PlaceViewModel> viewModels, IEnumerable<MPS_M_Place> lines)
        {
            return viewModels != null ? viewModels.Select(ToModel) : Enumerable.Empty<MPS_M_Place>();
        }
        
        
        ////Accounting Mapper
        public static PlaceViewModel ToViewFromAccounting(this MPS_M_Accounting model)
        {
            if (model == null)
                return null;
            if (model.SharedBranch != null)
            {
                var viewModel = new PlaceViewModel()
                {
                    PlaceCode = model.SharedBranch.BranchCode,
                    PlaceName = model.SharedBranch.BranchName,
                    InActive = model.SharedBranch.Inactive
                };
                return viewModel;
            }
            return Mapper.Map<PlaceViewModel>(model);
        }
        public static IEnumerable<PlaceViewModel> ToViewModelsFromAccounting(this IEnumerable<MPS_M_Accounting> models)
        {
            return models != null ? models.Select(ToViewFromAccounting) : Enumerable.Empty<PlaceViewModel>();
        }

    }
}