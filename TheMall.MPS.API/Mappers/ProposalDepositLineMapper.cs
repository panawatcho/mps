﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TheMall.MPS.API.Mappers.Abstracts;
using TheMall.MPS.API.Services;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Mappers.Abstracts;
using TheMall.MPS.Models.Proposal;
using TheMall.MPS.ViewModels.Proposal;

namespace TheMall.MPS.API.Mappers
{
    public interface IProposalDepositLineMapper : ILineMapper
    {
        MPS_DepositLine ToModel(DepositLineViewModel viewModel, IEnumerable<MPS_DepositLine> lines);
        DepositLineViewModel ToViewModel(MPS_DepositLine model);
        IEnumerable<DepositLineViewModel> ToViewModels(IEnumerable<MPS_DepositLine> models);
        IEnumerable<MPS_DepositLine> ToModels(IEnumerable<DepositLineViewModel> viewModels, IEnumerable<MPS_DepositLine> lines);
        IEnumerable<DepositAccountTransViewModel> ToDepositAccountTransViewModels(IEnumerable<MPS_DepositAccountTrans> models);
    }

    public class ProposalDepositLineMapper : BaseLineMapper, IProposalDepositLineMapper
    {
        private readonly IDepositAccountTransService _depositAccountTransService;

        public ProposalDepositLineMapper(
            IDepositAccountTransService depositAccountTransService)
        {
            _depositAccountTransService = depositAccountTransService;
        }

        public MPS_DepositLine ToModel(DepositLineViewModel viewModel, IEnumerable<MPS_DepositLine> lines)
        {
            if (viewModel == null)
            {
                return null;
            }
            var model = InitLineTable(viewModel, lines);
            if (model == null)
            {
                return null;
            }
            model.Amount = viewModel.Amount;
            if (viewModel.DatePeriod != null) model.DatePeriod = viewModel.DatePeriod.Value.ToLocalTime().Date;
            model.PaymentNo = viewModel.PaymentNo;
            model.ProcessStatus = viewModel.ProcessStatus;
            model.Remark = viewModel.Remark;

            return model;
        }
        public DepositLineViewModel ToViewModel(MPS_DepositLine model)
        {
            if (model == null)
            {
                return null;
            }
            var viewModel = this.InitLineViewModel<DepositLineViewModel>(model);

            //AutoMapper.Mapper.Map(model, viewModel);
            viewModel.PaymentNo = model.PaymentNo;
            viewModel.DatePeriod = (model.DatePeriod.HasValue) ? model.DatePeriod.Value.ToUniversalTime() : model.DatePeriod;
            viewModel.Amount = model.Amount;
            viewModel.ProcessStatus = model.ProcessStatus;
            viewModel.Remark = model.Remark;
            viewModel.Actual = model.Actual;

            //
            if (viewModel.Id != 0)
            {
                var modelDepositAccountTrans = _depositAccountTransService.GetWhere(m => m.DepositLineId == viewModel.Id);
                if (modelDepositAccountTrans != null) {
                    viewModel.DepositAccountTrans = ToDepositAccountTransViewModels(modelDepositAccountTrans).ToList();
                }
            }
            return viewModel;
        }

        public IEnumerable<DepositLineViewModel> ToViewModels(IEnumerable<MPS_DepositLine> models)
        {
            return models != null ? models.Select(ToViewModel) : Enumerable.Empty<DepositLineViewModel>();
        }

        public DepositAccountTransViewModel ToDepostiAccountTransViewModel(MPS_DepositAccountTrans model)
        {
            if (model == null)
            {
                return null;
            }
            var viewModel = new DepositAccountTransViewModel();

            viewModel.Id = model.Id;
            viewModel.Actual = model.Actual;
            viewModel.Remark = model.Remark;
            viewModel.DepositLineId = model.DepositLineId;
            viewModel.MemoIncomeId = model.MemoIncomeId;
            viewModel.LineNo = model.Id;
            viewModel.InvoiceNo = model.InvoiceNo;
            
            return viewModel;
        }
        public IEnumerable<DepositAccountTransViewModel> ToDepositAccountTransViewModels(IEnumerable<MPS_DepositAccountTrans> models)
        {
            return models != null ? models.Select(ToDepostiAccountTransViewModel) : Enumerable.Empty<DepositAccountTransViewModel>();
        }


        public IEnumerable<MPS_DepositLine> ToModels(IEnumerable<DepositLineViewModel> viewModels, IEnumerable<MPS_DepositLine> lines)
        {
            return viewModels != null ? viewModels.Select(m => ToModel(m, lines)).Where(m => m != null) : Enumerable.Empty<MPS_DepositLine>();
        }

       
    }
}