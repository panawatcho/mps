﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TheMall.MPS.API.Mappers.Abstracts;
using TheMall.MPS.API.Services;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Mappers.Abstracts;
using TheMall.MPS.Models.PettyCash;
using TheMall.MPS.ViewModels.PettyCash;

namespace TheMall.MPS.API.Mappers
{
    public interface IPettyCashLineMapper : ILineMapper
    {
        MPS_PettyCashLine ToModel(PettyCashLineViewModel viewModel, IEnumerable<MPS_PettyCashLine> lines);
        PettyCashLineViewModel ToViewModel(MPS_PettyCashLine model);
        IEnumerable<PettyCashLineViewModel> ToViewModels(IEnumerable<MPS_PettyCashLine> models);
        IEnumerable<MPS_PettyCashLine> ToModels(IEnumerable<PettyCashLineViewModel> viewModels, IEnumerable<MPS_PettyCashLine> lines);
    }
 
    public class PettyCashLineMapper : BaseLineMapper, IPettyCashLineMapper
    {
       
        public MPS_PettyCashLine ToModel(PettyCashLineViewModel viewModel, IEnumerable<MPS_PettyCashLine> lines)
        {
            if (viewModel == null)
            {
                return null;
            }
            var model = InitLineTable(viewModel, lines);
            if (model == null)
            {
                return null;
            }
           model.Description = viewModel.Description;
           model.Unit = viewModel.Unit;
           model.Amount = viewModel.Amount;
           model.Vat = viewModel.Vat;
           model.Tax = viewModel.Tax;
           model.NetAmount = viewModel.NetAmount;
           model.Remark = viewModel.Remark;

            return model;
        }
        public PettyCashLineViewModel ToViewModel(MPS_PettyCashLine model)
        {
            if (model == null)
            {
                return null;
            }
            var viewModel = this.InitLineViewModel<PettyCashLineViewModel>(model);

            if (model.Description != null)
            {
                viewModel.Description = model.Description;
            }
            else
            {
                viewModel.Description = "";
            }

            viewModel.Unit = model.Unit;
            viewModel.Amount = model.Amount;
            viewModel.Tax = model.Tax;
            viewModel.Vat = model.Vat;
            viewModel.NetAmount = model.NetAmount;
            viewModel.Remark = model.Remark;
            if (model.Remark != null)
            {
                viewModel.Remark = model.Remark;
            }
            else
            {
                viewModel.Remark = "";
            }
            viewModel.NetAmountNoVatTax = viewModel.Unit * viewModel.Amount;
            return viewModel;
        }

        public IEnumerable<PettyCashLineViewModel> ToViewModels(IEnumerable<MPS_PettyCashLine> models)
        {
            return models != null ? models.Select(ToViewModel) : Enumerable.Empty<PettyCashLineViewModel>();
        }
        public IEnumerable<MPS_PettyCashLine> ToModels(IEnumerable<PettyCashLineViewModel> viewModels, IEnumerable<MPS_PettyCashLine> lines)
        {
            return viewModels != null ? viewModels.Select(m => ToModel(m, lines)).Where(m => m != null) : Enumerable.Empty<MPS_PettyCashLine>();
        }

       
    }
}