﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TheMall.MPS.API.Extensions;
using TheMall.MPS.API.Mappers.Abstracts;
using TheMall.MPS.API.Services;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Infrastructure.Extension;
using TheMall.MPS.Mappers.Abstracts;
using TheMall.MPS.Models.Enums;
using TheMall.MPS.Models.Memo;
using TheMall.MPS.Models.Proposal;
using TheMall.MPS.ViewModels;
using TheMall.MPS.ViewModels.Memo;
using TheMall.MPS.ViewModels.Proposal;

namespace TheMall.MPS.API.Mappers
{
    public interface IProposalApprovalMapper : ILineMapper
    {
        MPS_ProposalApproval ToModel(ProposalApprovalViewModel viewModel, IEnumerable<MPS_ProposalApproval> lines);
        ProposalApprovalViewModel ToViewModel(MPS_ProposalApproval model);
        IEnumerable<ProposalApprovalViewModel> ToViewModels(IEnumerable<MPS_ProposalApproval> models);
        IEnumerable<MPS_ProposalApproval> ToModels(IEnumerable<ProposalApprovalViewModel> viewModels, IEnumerable<MPS_ProposalApproval> lines);
    }

    public class ProposalApprovalMapper : BaseLineMapper, IProposalApprovalMapper
    {
        private readonly IEmployeeTableService _employeeTableService;

        public ProposalApprovalMapper(IEmployeeTableService employeeTableService)
        {
            _employeeTableService = employeeTableService;
        }


        public MPS_ProposalApproval ToModel(ProposalApprovalViewModel viewModel, IEnumerable<MPS_ProposalApproval> lines)
        {
            if (viewModel == null || string.IsNullOrEmpty(viewModel.ApproverUserName))
            {
                return null;
            }

            if (string.IsNullOrEmpty(viewModel.ApproverEmail))
                throw new Exception("Please select approver in Approver Grid every levels!!!");


            if (viewModel.Employee == null)
            {
                return null;
            }
            var model = InitLineTable(viewModel, lines);
            if (model == null)
            {
                return null;
            }
             model.ApproverSequence = viewModel.ApproverSequence;
             model.ApproverLevel = viewModel.ApproverLevel;
            if (viewModel.Employee != null)
            {
                model.ApproverUserName = viewModel.Employee.Username;
                model.ApproverFullName = viewModel.Employee.FullName;
            }

             model.ApproverEmail = viewModel.ApproverEmail;
             model.Position = viewModel.Position;
             model.Department = viewModel.Department;
            model.LockEditable = viewModel.LockEditable;
            if (viewModel.DueDate != null)
                //model.DueDate = viewModel.DueDate.Value.ToLocalTime().Date.AddHours(23).AddMinutes(59).AddSeconds(59);
                model.DueDate = viewModel.DueDate.Value.DueDateEnd();
             ;

            return model;
        }

       

        public ProposalApprovalViewModel ToViewModel(MPS_ProposalApproval model)
        {
            if (model == null)
            {
                return null;
            }
            var viewModel = this.InitLineViewModel<ProposalApprovalViewModel>(model);
            if (model.ApproverUserName != null)
               viewModel.Employee = _employeeTableService.FindByUsername(model.ApproverUserName).ToViewModel();
            viewModel.ApproverSequence = model.ApproverSequence;
            viewModel.ApproverLevel = model.ApproverLevel;
            viewModel.ApproverUserName = model.ApproverUserName;
            viewModel.ApproverFullName = model.ApproverFullName;
            viewModel.ApproverEmail = model.ApproverEmail;
            viewModel.Position = model.Position;
            viewModel.Department = model.Department;
            viewModel.LockEditable = model.LockEditable;
            viewModel.ApproveStatus = model.ApproveStatus;
            if (model.DueDate != null) viewModel.DueDate = model.DueDate.Value;

            if (viewModel.ApproverLevel.Equals(Approval.ApproveLevelList.Propose.GetDescription()))
                viewModel.ApproverKey = Approval.ApproveKey.Propose;
            else if (viewModel.ApproverLevel.Equals(Approval.ApproveLevelList.Accept.GetDescription()))
                viewModel.ApproverKey = Approval.ApproveKey.Accept;
            else if (viewModel.ApproverLevel.Equals(Approval.ApproveLevelList.Approval.GetDescription()))
                viewModel.ApproverKey = Approval.ApproveKey.Approval;
            else if (viewModel.ApproverLevel.Equals(Approval.ApproveLevelList.FinalApproval.GetDescription()))
                viewModel.ApproverKey = Approval.ApproveKey.FinalApproval;
            else
            {
                viewModel.ApproverKey = 0;
            }

            return viewModel;
        }

        public IEnumerable<ProposalApprovalViewModel> ToViewModels(IEnumerable<MPS_ProposalApproval> models)
        {
            return models != null ? models.Select(ToViewModel) : Enumerable.Empty<ProposalApprovalViewModel>();
        }
        public IEnumerable<MPS_ProposalApproval> ToModels(IEnumerable<ProposalApprovalViewModel> viewModels, IEnumerable<MPS_ProposalApproval> lines)
        {
            return viewModels != null ? viewModels.Select(m => ToModel(m, lines)).Where(m => m != null) : Enumerable.Empty<MPS_ProposalApproval>();
        }

    
    }
}