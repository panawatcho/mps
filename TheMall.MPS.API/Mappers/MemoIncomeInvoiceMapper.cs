﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TheMall.MPS.API.Mappers.Abstracts;
using TheMall.MPS.API.Services;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Mappers.Abstracts;
using TheMall.MPS.Models.Memo;
using TheMall.MPS.Models.MemoIncome;
using TheMall.MPS.Models.PettyCash;
using TheMall.MPS.Models.Proposal;
using TheMall.MPS.ViewModels.Memo;
using TheMall.MPS.ViewModels.MemoIncome;


namespace TheMall.MPS.API.Mappers
{
    public interface IMemoIncomeInvoiceMapper : ILineMapper
    {
        MPS_MemoIncomeInvoice ToModel(MemoIncomeInvoiceViewModel viewModel, IEnumerable<MPS_MemoIncomeInvoice> lines);
        MemoIncomeInvoiceViewModel ToViewModel(MPS_MemoIncomeInvoice model);

        IEnumerable<MemoIncomeInvoiceViewModel> ToViewModels(IEnumerable<MPS_MemoIncomeInvoice> models);
        IEnumerable<MPS_MemoIncomeInvoice> ToModels(IEnumerable<MemoIncomeInvoiceViewModel> viewModels, IEnumerable<MPS_MemoIncomeInvoice> lines);
    }

    public class MemoIncomeInvoiceMapper : BaseLineMapper, IMemoIncomeInvoiceMapper
    {
        public MPS_MemoIncomeInvoice ToModel(MemoIncomeInvoiceViewModel viewModel, IEnumerable<MPS_MemoIncomeInvoice> lines)
        {
            if (viewModel == null)
            {
                return null;
            }

            var model = InitLineTable(viewModel, lines);

            if (model == null)
            {
                return null;
            }

            model.InvoiceNo = viewModel.InvoiceNo;
            model.Actual = viewModel.Actual;
            model.InvoiceDate = viewModel.InvoiceDate;
            model.Remark = viewModel.Remark;

            return model;
        }

        public MemoIncomeInvoiceViewModel ToViewModel(MPS_MemoIncomeInvoice model)
        {
            if (model == null)
            {
                return null;
            }
            var viewModel = this.InitLineViewModel<MemoIncomeInvoiceViewModel>(model);

           
            viewModel.InvoiceNo = model.InvoiceNo;
            viewModel.Actual = model.Actual;
            viewModel.InvoiceDate = model.InvoiceDate;
            viewModel.Remark = model.Remark;

            return viewModel;
        }

        public IEnumerable<MemoIncomeInvoiceViewModel> ToViewModels(IEnumerable<MPS_MemoIncomeInvoice> models)
        {
            return models != null ? models.Select(ToViewModel) : Enumerable.Empty<MemoIncomeInvoiceViewModel>();
        }
        public IEnumerable<MPS_MemoIncomeInvoice> ToModels(IEnumerable<MemoIncomeInvoiceViewModel> viewModels, IEnumerable<MPS_MemoIncomeInvoice> lines)
        {
            return viewModels != null ? viewModels.Select(m => ToModel(m, lines)).Where(m => m != null) : Enumerable.Empty<MPS_MemoIncomeInvoice>();
        }
       
    }
}