﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CrossingSoft.Framework.Models.Enums;
using TheMall.MPS.API.Services;
using TheMall.MPS.Mappers.Abstracts;
using TheMall.MPS.Models.CashAdvance;
using TheMall.MPS.Models.Enums;
using TheMall.MPS.Models.Proposal;
using TheMall.MPS.ViewModels.CashAdvance;
using TheMall.MPS.ViewModels.Proposal;

namespace TheMall.MPS.API.Mappers
{
    public interface ICashAdvanceApproveMapper : ILineMapper
    {
        MPS_CashAdvanceApproval ToModel(CashAdvanceApprovalViewModel viewModel, IEnumerable<MPS_CashAdvanceApproval> lines);
        CashAdvanceApprovalViewModel ToViewModel(MPS_CashAdvanceApproval model);
        IEnumerable<CashAdvanceApprovalViewModel> ToViewModels(IEnumerable<MPS_CashAdvanceApproval> models);
        IEnumerable<MPS_CashAdvanceApproval> ToModels(IEnumerable<CashAdvanceApprovalViewModel> viewModels, IEnumerable<MPS_CashAdvanceApproval> lines);
    }

    public class CashAdvanceApproveMapper : BaseLineMapper, ICashAdvanceApproveMapper
    {
        private readonly IEmployeeTableService _employeeTableService;
        public CashAdvanceApproveMapper(IEmployeeTableService employeeTableService)
        {
            _employeeTableService = employeeTableService;
        }
        public MPS_CashAdvanceApproval ToModel(CashAdvanceApprovalViewModel viewModel, IEnumerable<MPS_CashAdvanceApproval> lines)
        {
            if (viewModel == null || string.IsNullOrEmpty(viewModel.ApproverUserName))
            {
                return null;
            }

            if (string.IsNullOrEmpty(viewModel.ApproverEmail))
                throw new Exception("Please Push update or cancel button after select approver in Approver Grid every level!!!");


            var model = InitLineTable(viewModel, lines);
            if (model == null)
            {
                return null;
            }
            model.ApproverSequence = viewModel.ApproverSequence;
            model.ApproverLevel = viewModel.ApproverLevel;
            if (viewModel.Employee != null)
            {
                model.ApproverUserName = viewModel.Employee.Username;
                model.ApproverFullName = viewModel.Employee.FullName;
            }
            model.ApproverEmail = viewModel.ApproverEmail;
            model.Position = viewModel.Position;
            model.Department = viewModel.Department;
            if (viewModel.DueDate != null)
                model.DueDate = viewModel.DueDate.Value.ToLocalTime().Date.AddDays(1).AddSeconds(-1).ToUniversalTime();
            ;
            model.LockEditable = viewModel.LockEditable;
            return model;
        }

        public CashAdvanceApprovalViewModel ToViewModel(MPS_CashAdvanceApproval model)
        {
            if (model == null)
            {
                return null;
            }
            var viewModel = this.InitLineViewModel<CashAdvanceApprovalViewModel>(model);
            if (model.ApproverUserName != null)
                viewModel.Employee = _employeeTableService.FindByUsername(model.ApproverUserName).ToViewModel();
            viewModel.ApproverSequence = model.ApproverSequence;
            viewModel.ApproverLevel = model.ApproverLevel;
            viewModel.ApproverUserName = model.ApproverUserName;
            viewModel.ApproverFullName = model.ApproverFullName;
            viewModel.ApproverEmail = model.ApproverEmail;
            viewModel.Position = model.Position;
            viewModel.Department = model.Department;
            viewModel.DueDate = model.DueDate;
            viewModel.LockEditable = model.LockEditable;
            viewModel.ApproveStatus = model.ApproveStatus;
            return viewModel;
        }

        public IEnumerable<CashAdvanceApprovalViewModel> ToViewModels(IEnumerable<MPS_CashAdvanceApproval> models)
        {
            return models != null ? models.Select(ToViewModel) : Enumerable.Empty<CashAdvanceApprovalViewModel>();
        }

        public IEnumerable<MPS_CashAdvanceApproval> ToModels(IEnumerable<CashAdvanceApprovalViewModel> viewModels, IEnumerable<MPS_CashAdvanceApproval> lines)
        {
            return viewModels != null ? viewModels.Select(m => ToModel(m, lines)).Where(m => m != null) : Enumerable.Empty<MPS_CashAdvanceApproval>();
        }
    }
}