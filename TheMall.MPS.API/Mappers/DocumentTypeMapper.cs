﻿using System.Collections.Generic;
using System.Linq;
using TheMall.MPS.Models;
using TheMall.MPS.ViewModels;

namespace TheMall.MPS.API.Mappers
{
    public static class DocumentTypeMapper
    {
        public static DocumentTypeViewModel ToViewModel(this DocumentType documentType)
        {
            if (documentType == null)
                return null;

            return new DocumentTypeViewModel
            {
                Value = documentType.Id,
                Name = documentType.Name,
                IsRequired = documentType.IsRequired
            };
        }

        public static IEnumerable<DocumentTypeViewModel> ToViewModel(this IEnumerable<DocumentType> documentTypes)
        {
            if (documentTypes == null)
            {
                return Enumerable.Empty<DocumentTypeViewModel>();
            }
            return documentTypes.Select(documentType => documentType.ToViewModel());
        }
    }
}