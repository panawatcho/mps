﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TheMall.MPS.API.Mappers.Abstracts;
using TheMall.MPS.API.Services;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Mappers.Abstracts;
using TheMall.MPS.Models.Memo;
using TheMall.MPS.Models.Proposal;
using TheMall.MPS.ViewModels.Master;
using TheMall.MPS.ViewModels.Memo;
using TheMall.MPS.ViewModels.Proposal;

namespace TheMall.MPS.API.Mappers
{
    public interface IProposalLineSharedTemplateMapper 
    {
        MPS_ProposalLineSharedTemplate ToModel(ProposalLineSharedTemplateViewModel viewModel);
        ProposalLineSharedTemplateViewModel ToViewModel(MPS_ProposalLineSharedTemplate model);
        IEnumerable<ProposalLineSharedTemplateViewModel> ToViewModels(IEnumerable<MPS_ProposalLineSharedTemplate> models);
        IEnumerable<MPS_ProposalLineSharedTemplate> ToModels(IEnumerable<ProposalLineSharedTemplateViewModel> viewModels);
        IEnumerable<ProposalLineSharedTemplateViewModel> ToListViewModel(List<MPS_ProposalLineSharedTemplate> listModel,
            List<SharedBranchViewModel> sharedBranch);
    }

    public class ProposalLineSharedTemplateMapper : IProposalLineSharedTemplateMapper
    {
        private readonly IProposalLineSharedTemplateService _service;
        private readonly ISharedBranchService _sharedBranchService;

        public ProposalLineSharedTemplateMapper(IProposalLineSharedTemplateService service, ISharedBranchService sharedBranchService)
        {
            _service = service;
            _sharedBranchService = sharedBranchService;
        }

        public MPS_ProposalLineSharedTemplate ToModel(ProposalLineSharedTemplateViewModel viewModel)
        {
            if (viewModel == null)
            {
                return null;
            }

            var model = _service.Find(viewModel.Id);
            if (model != null)
            {
                model.Selected = viewModel.Selected;
                model.BranchCode = viewModel.BranchCode;
                model.ParentId = viewModel.ParentId;
            }

            return model;
        }

        public ProposalLineSharedTemplateViewModel ToViewModel(MPS_ProposalLineSharedTemplate model)
        {
            if (model == null)
            {
                return null;
            }
           
            var viewModel = new ProposalLineSharedTemplateViewModel();
            viewModel.Selected = model.Selected;
            viewModel.BranchCode = model.BranchCode;
            viewModel.ParentId = model.ParentId;
            viewModel.Id = model.Id;
            return viewModel;
        }

        public IEnumerable<ProposalLineSharedTemplateViewModel> ToListViewModel(List<MPS_ProposalLineSharedTemplate> listModel, List<SharedBranchViewModel> sharedBranch)
        {
            var listViewModel = new  List<ProposalLineSharedTemplateViewModel>();
            var lastTemplateLine = listModel.LastOrDefault();
            if (sharedBranch != null)
            {
                foreach (var shared in sharedBranch)
                {
                    var viewModel = new ProposalLineSharedTemplateViewModel();
                    var branchCode = listModel.LastOrDefault(m => m.BranchCode == shared.BranchCode);
                    if (branchCode != null)
                    {
                        viewModel.Selected = branchCode.Selected;
                        viewModel.BranchCode = branchCode.BranchCode;
                        viewModel.ParentId = branchCode.ParentId;
                        viewModel.Id = branchCode.Id;
                    }
                    else
                    {                       
                        viewModel.Selected = shared.Selected;
                        viewModel.BranchCode = shared.BranchCode;
                          
                    }

                    listViewModel.Add(viewModel);
                }
            }
            else
            {
                listViewModel = ToViewModels(listModel).ToList();
            }

            return listViewModel;
        }
        public IEnumerable<ProposalLineSharedTemplateViewModel> ToViewModels(IEnumerable<MPS_ProposalLineSharedTemplate> models)
        {
            return models != null ? models.Select(ToViewModel) : Enumerable.Empty<ProposalLineSharedTemplateViewModel>();
        }
        public IEnumerable<MPS_ProposalLineSharedTemplate> ToModels(IEnumerable<ProposalLineSharedTemplateViewModel> viewModels)
        {
            return viewModels != null ? viewModels.Select(ToModel).Where(m => m != null) : Enumerable.Empty<MPS_ProposalLineSharedTemplate>();
        }
    }
}