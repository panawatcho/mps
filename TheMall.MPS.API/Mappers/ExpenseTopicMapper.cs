﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using TheMall.MPS.Models.Master;
using TheMall.MPS.ViewModels.Master;

namespace TheMall.MPS.API.Mappers
{
    public class ExpenseTopicMapperProfile : AutoMapper.Profile
    {
        public ExpenseTopicMapperProfile()
        {
            CreateMap<MPS_M_ExpenseTopic, ExpenseTopicViewModel>()
                .ReverseMap();
        }
    }
    public static class ExpenseTopicMapper
    {
        public static MPS_M_ExpenseTopic ToModel(this ExpenseTopicViewModel viewModel)
        {
            if (viewModel == null)
                return null;
            var model = new MPS_M_ExpenseTopic()
            {
                ExpenseTopicCode = viewModel.ExpenseTopicCode,
                ExpenseTopicName = viewModel.ExpenseTopicName,
                TypeAP = viewModel.TypeAP,
                Description =viewModel.Description,
                InActive = viewModel.InActive,
                Order = viewModel.Order
                //APCode = viewModel.APCode.ToModel().ToList()
            };
            return model;
        }

        public static ExpenseTopicViewModel ToViewModel(this MPS_M_ExpenseTopic model)
        {
            if (model == null)
                return null;
            var viewModel = new ExpenseTopicViewModel()
            {
                ExpenseTopicCode = model.ExpenseTopicCode,
                ExpenseTopicName = model.ExpenseTopicName,
                Description =  model.Description,
                TypeAP = model.TypeAP,
                APCode = model.APCode.ToViewModels(),
                InActive = model.InActive,
                Order = model.Order
            };
            
            return viewModel;
        }
        public static IEnumerable<ExpenseTopicViewModel> ToViewModels(this IEnumerable<MPS_M_ExpenseTopic> models)
        {
            return models != null ? models.Select(ToViewModel) : Enumerable.Empty<ExpenseTopicViewModel>();
        }
        public static IEnumerable<MPS_M_ExpenseTopic> ToModels(IEnumerable<ExpenseTopicViewModel> viewModels, IEnumerable<MPS_M_ExpenseTopic> lines)
        {
            return viewModels != null ? viewModels.Select(ToModel) : Enumerable.Empty<MPS_M_ExpenseTopic>();
        }

    }
}