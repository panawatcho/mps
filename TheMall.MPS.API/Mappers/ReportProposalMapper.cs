﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TheMall.MPS.API.Services;
using TheMall.MPS.Infrastructure.Extension;
using TheMall.MPS.Models.Enums;
using TheMall.MPS.Models.Proposal;
using TheMall.MPS.Reports.Proposal;
using TheMall.MPS.Reports.Shared;
using TheMall.MPS.ViewModels.Master;
using TheMall.MPS.ViewModels.Proposal;

namespace TheMall.MPS.API.Mappers
{
    public interface IReportProposalMapper
    {
        IEnumerable<ProposalDataSource> ReportDataMapper(MPS_ProposalTable proposalModel);
        IEnumerable<ApproverDataSource> ApproversDataMapper(IEnumerable<MPS_ProposalApproval> approversData);
        IEnumerable<ProposalLineDataSource> ProposalLineDataMapper(ProposalViewModel proposalViewodel);
        IEnumerable<ProposalDepositDataSource> ProposalDepositDataMapper(ProposalViewModel proposalViewModel);
    }

    public class ReportProposalMapper : IReportProposalMapper
    {
        private readonly IPlaceService _placeService;
        private readonly IProposalMapper _proposalMapper;
        private readonly IEmployeeTableService _employeeTableService;
        private readonly IAllocationBasisService _allocationBasisService;
        public ReportProposalMapper(IPlaceService placeService, 
            IProposalMapper proposalMapper, 
            IEmployeeTableService employeeTableService, IAllocationBasisService allocationBasisService)
        {
            _placeService = placeService;
            _proposalMapper = proposalMapper;
            _employeeTableService = employeeTableService;
            _allocationBasisService = allocationBasisService;
        }

        public IEnumerable<ProposalDataSource> ReportDataMapper(MPS_ProposalTable proposalModel)
        {
            var viewModel = _proposalMapper.ToViewModel(proposalModel);
            var proposalDataSource = new List<ProposalDataSource>();
            if (proposalModel != null && viewModel != null)
            {
                var line = new ProposalDataSource();
                line.ProposalTitle = proposalModel.Title;

                //line.Place = proposalModel.Place != null ? 
                //    proposalModel.Place.Replace(",", ", ") : "";
                //int lastIndex = line.Place.LastIndexOf(',');
                //line.Place = lastIndex != 0? line.Place.Substring(0, lastIndex):"";
                
                var listPlaces = new List<PlaceViewModel>();
                if (!string.IsNullOrEmpty(proposalModel.Place))
                {
                    string[] placeid = proposalModel.Place.Split(',');
                    listPlaces.AddRange(from p in placeid where !string.IsNullOrEmpty(p) select _placeService.Find(m => m.PlaceCode == p).ToViewModel());
                }

                if (listPlaces.LastOrDefault() != null)
                {
                    foreach (var place in listPlaces)
                    {
                        if (string.IsNullOrEmpty(line.Place))
                        {
                            line.Place = place.PlaceName;
                        }
                        else
                        {
                            line.Place = line.Place + ", " + place.PlaceName;
                        }
                    }
                }

                if (proposalModel.CCUsername != null && viewModel.CCEmail != null)
                {
                    line.CC = "";
                    foreach (var lineCC in viewModel.CCEmail)
                    {
                        var emploloyee = _employeeTableService.FindByUsername(lineCC.Name);
                        var fullName = (emploloyee != null) ? emploloyee.ThFullName : lineCC.Name;
                        line.CC = line.CC == "" ? fullName : line.CC + ", " + fullName;
                    }
                  
                }
               


                if (proposalModel.InformUsername != null && viewModel.InformEmail != null)
                {
                    line.Inform = "";
                    foreach (var lineInform in viewModel.InformEmail)
                    {
                        var emploloyee = _employeeTableService.FindByUsername(lineInform.Name);
                        var fullName = (emploloyee != null) ? emploloyee.ThFullName : lineInform.Name;
                        line.Inform = line.Inform == "" ? fullName : line.Inform + ", " + fullName;
                    }
                    //var lastIndex = proposalModel.InformUsername.LastIndexOf(',');
                    //line.Inform = lastIndex != 0
                    //    ? proposalModel.InformUsername.Replace(";", ", ")
                    //    : proposalModel.InformUsername;
                }
                //else
                //{
                //    line.Inform = "";
                //}

                if (proposalModel.Attachments != null)
                {
                    line.AttachmentName = "";
                    foreach (var lineAttachment in proposalModel.Attachments)
                    {
                        if (line.AttachmentName == "")
                        {
                            line.AttachmentName = lineAttachment.FileName;
                        }
                        else
                        {
                            line.AttachmentName = line.AttachmentName + ", " + lineAttachment.FileName;
                        }
                    }
                }
                line.StartDate = proposalModel.StartDate.ToString("dd/MM/yyyy");
                line.EndDate = proposalModel.EndDate.ToString("dd/MM/yyyy");
                line.CreateReportDate = DateTime.Now.ToString("dd/MM/yyyy");
                line.ProposalNumber = proposalModel.DocumentNumber;
                line.ProposalType = proposalModel.ProposalTypeCode;
                line.ProposalTypeName = proposalModel.ProposalTypeName;
                line.Requester = proposalModel.RequesterName;
                line.TotalBudget = proposalModel.TotalBudget;
                line.TotalBudgetInCharacter = line.TotalBudget.ToWords();
                line.UnitCode = proposalModel.UnitCode;
                line.UnitName = proposalModel.UnitName;
                if (proposalModel.Objective != null)
                {
                    line.Objective = proposalModel.Objective.Replace("font-family", string.Empty);
                    var imgs = line.Objective.Contains("img");
                    line.ObjectiveImg = imgs ? "*ดูรูปเพิ่มเติมได้ในระบบ" : "";
                } 
                
                line.AllocationBasis = "-";
                if (proposalModel.TemplateLines != null && proposalModel.TemplateLines.Count !=0 )
                {
                    var allocationBasis = _allocationBasisService.FindByProposalSharedTemplate(proposalModel.TemplateLines);
                    if (allocationBasis != null)
                    {
                        line.AllocationBasis = allocationBasis.AllocCode;
                    }
                
                }
                
               
                proposalDataSource.Add(line);
                return proposalDataSource;
            }
            else
            {
                return null;
            }

        }

        public IEnumerable<ApproverDataSource> ApproversDataMapper(IEnumerable<MPS_ProposalApproval> approversData)
        {
            if (approversData != null)
            {
                var list = new List<ApproverDataSource>();
                foreach (var approverData in approversData)
                {
                    var reportApproverDataSource = new ApproverDataSource() { };
                    reportApproverDataSource.ApproverId = approverData.Id;
                    reportApproverDataSource.ApproverSequence = approverData.ApproverSequence;
                    reportApproverDataSource.ApproverUsername = approverData.ApproverUserName ?? "";
                    reportApproverDataSource.ApproverFullName = approverData.ApproverFullName ?? "";
                    reportApproverDataSource.ApproverLevel = approverData.ApproverLevel ?? "";
                    reportApproverDataSource.ApproverPosition = approverData.Position ?? "";
                    var comment = approverData.ProposalTable.Comments;
                    var approverCheck = approverData.ApproverLevel + " - Approval";
                    if (comment != null)
                    {
                        comment = comment.Where(m => m.Activity.ToLower() != "start").ToList();
                        var approve = comment.LastOrDefault(m => m.CreatedBy.Equals(approverData.ApproverUserName, StringComparison.InvariantCultureIgnoreCase) &&
                      m.Action.Equals(DocumentStatusFlag.Approve.ToString(), StringComparison.InvariantCultureIgnoreCase) &&
                      m.Activity.Equals(approverCheck, StringComparison.InvariantCultureIgnoreCase));
                        if (approve != null)
                        {
                            reportApproverDataSource.ApproveDateTime = approve.CreatedDate != null ? approve.CreatedDate.Value.ToString("dd/MM/yyyy") : "";
                        }
                        else
                        {
                            reportApproverDataSource.ApproveDateTime = "";
                        }
                    }
                    //if (comment != null)
                    //{
                    //    var approve = comment.LastOrDefault(m => m.CreatedBy.Equals(approverData.ApproverUserName) && m.Action.Equals(DocumentStatusFlag.Approve.ToString()) && m.Activity.StartsWith(approverData.ApproverLevel));
                    //    reportApproverDataSource.ApproveDateTime = (approve != null && approve.CreatedDate != null) ? approve.CreatedDate.Value.ToString("dd/MM/yyyy") : "";
                    //}
                    if (!string.IsNullOrEmpty(approverData.ApproverLevel))
                    {
                        if (approverData.ApproverLevel.Equals(Approval.ApproveLevelList.Propose.GetDescription()))
                            reportApproverDataSource.ApproverLevelKey = Approval.ApproveKey.Propose;
                        else if (approverData.ApproverLevel.Equals(Approval.ApproveLevelList.Accept.GetDescription()))
                            reportApproverDataSource.ApproverLevelKey = Approval.ApproveKey.Accept;
                        else if (approverData.ApproverLevel.Equals(Approval.ApproveLevelList.Approval.GetDescription()))
                            reportApproverDataSource.ApproverLevelKey = Approval.ApproveKey.Approval;
                        else if (approverData.ApproverLevel.Equals(Approval.ApproveLevelList.FinalApproval.GetDescription()))
                            reportApproverDataSource.ApproverLevelKey = Approval.ApproveKey.FinalApproval;
                        else
                        {
                            reportApproverDataSource.ApproverLevelKey = 0;
                        }
                    }
                    else
                    {
                        reportApproverDataSource.ApproverLevelKey = 0;
                    }

                    list.Add(reportApproverDataSource);
                }
                return list;
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<ProposalLineDataSource> ProposalLineDataMapper(ProposalViewModel proposalViewModel)
        {
            if (proposalViewModel != null)
            {
                var list = new List<ProposalLineDataSource>();
                //foreach (var proposalLine in proposalLineDataViewModel)
                //{
                //    if (proposalLine.TypeAP.Equals("A", StringComparison.InvariantCultureIgnoreCase))
                //    {
                //        sumTypeA = sumTypeA + proposalLine.BudgetPlan;
                //    }

                //    if (proposalLine.TypeAP.Equals("P", StringComparison.InvariantCultureIgnoreCase))
                //    {
                //        sumTypeP = sumTypeP + proposalLine.BudgetPlan;
                //    }
                //}

                decimal sumTypeA = 0;
                decimal sumTypeP = 0;
                decimal marketing = 0;
                foreach (var proposalLine in proposalViewModel.ProposalLine)
                {
                     if (proposalLine.TypeAP.Equals("A", StringComparison.InvariantCultureIgnoreCase))
                     {
                         sumTypeA = sumTypeA + proposalLine.BudgetPlan;
                     }

                     if (proposalLine.TypeAP.Equals("P", StringComparison.InvariantCultureIgnoreCase))
                     {
                         sumTypeP = sumTypeP + proposalLine.BudgetPlan;
                     }
                     marketing = marketing + proposalLine.BudgetPlan;
                }

                foreach (var proposalLine in proposalViewModel.ProposalLine)
                {
                    var proposalLineDataSource = new ProposalLineDataSource();
                    if (proposalLine.APCode != null)
                    {
                        proposalLineDataSource.APCode = proposalLine.APCode.APCode;

                    }
                    proposalLineDataSource.Description = proposalLine.Description;
                    proposalLineDataSource.ExpenseTopicCode = proposalLine.ExpenseTopicCode;
                    proposalLineDataSource.ExpenseTopicName = proposalLine.ExpenseTopicName;
                    if (proposalLine.Actual != null) 
                        proposalLineDataSource.Actual = (decimal) proposalLine.Actual;
                    proposalLineDataSource.BudgetPlan = proposalLine.BudgetPlan;
                    proposalLineDataSource.TypeAP = proposalLine.TypeAP;
                    
                    if (proposalLine.TypeAP.Equals("A", StringComparison.InvariantCultureIgnoreCase))
                    {
                        proposalLineDataSource.TotalAdvertising = proposalLine.BudgetPlan;
                        proposalLineDataSource.TypeAPSort = 1;
                    }
                    else if (proposalLine.TypeAP.Equals("P", StringComparison.InvariantCultureIgnoreCase))
                    {
                        proposalLineDataSource.TotalPromotion = proposalLine.BudgetPlan;
                        proposalLineDataSource.TypeAPSort = 2;
                    }
                    else
                    {
                        proposalLineDataSource.TypeAPSort = 3;
                    }
                    decimal calPerCent = 0;
                    if (proposalLineDataSource.Actual == 0) {
                        if (proposalLineDataSource.BudgetPlan != 0)
                            calPerCent = ((proposalLineDataSource.BudgetPlan - proposalLineDataSource.Actual) / proposalLineDataSource.BudgetPlan) * 100;
                    } else {
                        if (proposalLineDataSource.Actual != 0)
                            calPerCent = ((proposalLineDataSource.BudgetPlan - proposalLineDataSource.Actual) / proposalLineDataSource.Actual) * 100;
                    }
                    proposalLineDataSource.Different = proposalLineDataSource.BudgetPlan -
                                                       proposalLineDataSource.Actual;

                    if (proposalLine.UnitCode != null)
                    {
                        proposalLineDataSource.UnitCode = proposalLine.UnitCode.UnitCode;
                        proposalLineDataSource.UnitName = proposalLine.UnitCode.UnitName;
                    }

                    if (proposalViewModel.TotalSaleTarget != null && proposalViewModel.TotalSaleTarget != 0)
                    {
                        if (proposalViewModel.TotalSaleTarget != null)
                            proposalLineDataSource.SaleTarget = (decimal) proposalViewModel.TotalSaleTarget;
                        proposalLineDataSource.PercentAdvertingSaleTarget = (sumTypeA/proposalLineDataSource.SaleTarget)*
                                                                            100;
                        proposalLineDataSource.PercentPromotionSaleTarget = (sumTypeP/proposalLineDataSource.SaleTarget)*
                                                                            100;
                        proposalLineDataSource.PercentMarketingSaleTarget = (marketing / proposalLineDataSource.SaleTarget) *
                                                                           100;
                        //proposalLineDataSource.PercentAdvertingSaleTarget = ( / proposalLineDataSource.SaleTarget) * 100;
                    }


                    proposalLineDataSource.Percent = calPerCent;
                    if (proposalViewModel.TotalSaleTargetLastYear != null)
                        proposalLineDataSource.SaleTargetLastYear = (decimal)proposalViewModel.TotalSaleTargetLastYear;





                    list.Add(proposalLineDataSource);
                }




                return list;
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<ProposalDepositDataSource> ProposalDepositDataMapper(ProposalViewModel proposalViewModel)
        {
            if (proposalViewModel != null )
            {
                    var list = new List<ProposalDepositDataSource>();
                    foreach (var proposalDeposit in proposalViewModel.DepositLines)
                    {
                        var proposalDepositDataSource = new ProposalDepositDataSource();
                        proposalDepositDataSource.Amount = proposalDeposit.Amount;
                        proposalDepositDataSource.DatePeriod = proposalDeposit.DatePeriod;
                        proposalDepositDataSource.PaymentNo = proposalDeposit.PaymentNo;
                        proposalDepositDataSource.ProcessStatus = proposalDeposit.ProcessStatus;
                        proposalDepositDataSource.Source = proposalViewModel.Source;
                        if (proposalViewModel.DepositNumber != null)
                        {
                            proposalDepositDataSource.DepositProposalRef = proposalViewModel.DepositNumber.DocumentNumber + " - " 
                                + proposalViewModel.DepositNumber.Title; 
                        }
                       
                           list.Add(proposalDepositDataSource);
                    }
                    return list;
            }
            else
            {
                return null;
            }
        }
    }
}