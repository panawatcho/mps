﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Routing;
using TheMall.MPS.Models.Enums;
using TheMall.MPS.Models.Interfaces;
using TheMall.MPS.ViewModels;

namespace TheMall.MPS.API.Mappers
{
    public static class AttachmentMapper
    {
        public static IEnumerable<AttachmentViewModel> ToViewModel(this IEnumerable<IAttachmentTable> models, System.Web.Http.Routing.UrlHelper urlHelper)
        {
            if (models == null)
            {
                return Enumerable.Empty<AttachmentViewModel>();
            }

            var viewModels = new List<AttachmentViewModel>();

            foreach (var model in models)
            {
                var viewModel = model.ToViewModel(urlHelper);

                if (viewModel != null)
                {
                    viewModels.Add(viewModel);
                }
            }
            return viewModels;
        }


        public static Dictionary<string, IEnumerable<AttachmentViewModel>> ToDocTypeViewModel(
           this IEnumerable<IAttachmentTable> attachmentTables, UrlHelper url)
        {
            var dictionary = new Dictionary<string, IEnumerable<AttachmentViewModel>>();
            if (attachmentTables == null)
            {
                return dictionary;
            }

            var attachments = attachmentTables.Where(m => !string.IsNullOrEmpty(m.FileName)).Select(attachmentTable => attachmentTable.ToViewModel(url)).ToListSafe();

            var keys = attachments.GroupBy(m => m.documentType).Select(m => m.Key);

            foreach (var key in keys)
            {
                dictionary.Add(key.ToString(), attachments.Where(m => m.documentType == key).ToArray());
            }
            return dictionary;
        }

        public static AttachmentViewModel ToViewModel(this IAttachmentTable attachmentTable, System.Web.Http.Routing.UrlHelper urlHelper)
        {
            return SetValues(
                    attachmentTable.FileName,
                    attachmentTable.FileSize,
                    attachmentTable.Location,
                    attachmentTable.ContentType,
                    urlHelper,
                    attachmentTable);
        }

        public static AttachmentViewModel Create(FileInfo fileInfo, string fileType, System.Web.Http.Routing.UrlHelper urlHelper, IAttachmentTable attachmentTable = null)
        {
            return SetValues(fileInfo.Name, (int)fileInfo.Length, fileInfo.FullName, fileType, urlHelper, attachmentTable);
        }

        //public static AttachmentViewModel Create(string fileName, long fileLength, string fullPath, string fileType, System.Web.Http.Routing.UrlHelper urlHelper, IAttachmentTable attachmentTable = null)
        //{
        //    return SetValues(fileName, fileLength, fullPath, fileType, urlHelper, attachmentTable);
        //}

        private static AttachmentViewModel SetValues(string fileName, long fileLength, string fullPath, string fileType, System.Web.Http.Routing.UrlHelper urlHelper, IAttachmentTable attachmentTable = null)
        {
            if (AppSettings.Instance.AttachmentMode == AttachmentMode.FileSystem)
            {
                var model = new AttachmentViewModel();
                if (System.IO.File.Exists(fullPath))
                {
                    model.name = fileName;
                    model.type = fileType;
                    model.size = fileLength;
                    model.progress = "1.0";

                    //model.url = HandlerPath + "api/Upload?f=" + fileName;
                    //model.delete_url = HandlerPath + "api/Upload?f=" + fileName;
                    model.delete_type = "DELETE";
                    var ext = Path.GetExtension(fullPath);
                    var fileSize = ConvertBytesToMegabytes(fileLength);
                    if (fileSize > 3 || !IsImage(ext))
                    {
                        model.thumbnail_url = "Content/images/not_available.png";
                    }
                    else
                    {
                        model.thumbnail_url = @"data:image/png;base64," + EncodeFile(fullPath);
                    }

                    if (attachmentTable != null)
                    {
                        var controllerName = attachmentTable.ControllerName();
                        if (string.IsNullOrEmpty(controllerName))
                        {
                            var action = (HttpActionDescriptor[])urlHelper.Request.GetRouteData().Route.DataTokens["actions"];
                            controllerName = action[0].ControllerDescriptor.ControllerName;
                        }
                        //var controllerName = attachmentTable.GetType().Name.Replace("Attachment", "");

                        if (attachmentTable.DocumentTypeId != null)
                        {
                            //model.url = string.Format(@"{0}/{1}/attachment/type/{2}?inline={3}&fileName={4}", controllerName,
                            //        attachmentTable.ParentId, attachmentTable.DocumentTypeId, true, attachmentTable.FileName)
                            //;
                            model.url = urlHelper.Link("AttachmentWithDocumentTypeRoute", new { controller = controllerName, id = attachmentTable.ParentId, attachmentId = attachmentTable.Id, documentType = attachmentTable.DocumentTypeId, fileName = attachmentTable.FileName, inline = true });
                        }
                        else
                        {
                            model.url = urlHelper.Link("AttachmentRoute", new { controller = controllerName, id = attachmentTable.ParentId, attachmentId = attachmentTable.Id, fileName = attachmentTable.FileName, inline = true });
                        }

                        model.getcopy_url = urlHelper.Link("AttachmentRoute", new { controller = controllerName, id = attachmentTable.ParentId, attachmentId = attachmentTable.Id, fileName = attachmentTable.FileName, inline = false });
                        model.delete_url = urlHelper.Link("AttachmentRoute", new { controller = controllerName, id = attachmentTable.ParentId, attachmentId = attachmentTable.Id });
                        model.id = attachmentTable.Id;
                        model.parentId = attachmentTable.ParentId;
                        model.title = attachmentTable.FileDescription;
                        model.documentNumber = attachmentTable.DocumentNumber;
                        model.activity = attachmentTable.Activity;
                        //model.extension = System.IO.Path.GetExtension(attachmentTable.FileName).ToLowerInvariantSafe();
                        model.documentType = attachmentTable.DocumentTypeId;
                        model.serialNumber = attachmentTable.SerialNumber;
                    }
                    return model;
                }
                else
                {
                    return null;
                    model.name = fileName;
                    model.type = fileType;
                    model.size = fileLength;
                    model.progress = "1.0";
                    model.error = "File not found";
                    return model;

                }
            }
            else if (AppSettings.Instance.AttachmentMode == AttachmentMode.SharePoint)
            {
                var model = new AttachmentViewModel();
                model.name = fileName;
                model.type = fileType;
                model.size = fileLength;
                model.progress = "1.0";
                model.delete_type = "DELETE";
                var ext = Path.GetExtension(fullPath);
                var fileSize = ConvertBytesToMegabytes(fileLength);
                if (fileSize > 3 || !IsImage(ext))
                {
                    model.thumbnail_url = "Content/images/not_available.png";
                }
                else
                {
                    model.thumbnail_url = @"data:image/png;base64," + EncodeFile(fullPath);
                }
                if (attachmentTable != null)
                {
                    var controllerName = attachmentTable.ControllerName();
                    if (string.IsNullOrEmpty(controllerName))
                    {
                        var action = (HttpActionDescriptor[])urlHelper.Request.GetRouteData().Route.DataTokens["actions"];
                        controllerName = action[0].ControllerDescriptor.ControllerName;
                    }
                    //var controllerName = attachmentTable.GetType().Name.Replace("Attachment", "");
                    if (attachmentTable.DocumentTypeId != null)
                    {
                        model.url = urlHelper.Link("AttachmentWithDocumentTypeRoute", new { controller = controllerName, id = attachmentTable.ParentId, attachmentId = attachmentTable.Id, documentType = attachmentTable.DocumentTypeId, inline = true });
                    }
                    else
                    {
                        model.url = urlHelper.Link("AttachmentRoute", new { controller = controllerName, id = attachmentTable.ParentId, attachmentId = attachmentTable.Id, inline = true });
                    }


                    model.getcopy_url = urlHelper.Link("AttachmentRoute", new { controller = controllerName, id = attachmentTable.ParentId, attachmentId = attachmentTable.Id, inline = false });
                    model.delete_url = urlHelper.Link("AttachmentRoute", new { controller = controllerName, id = attachmentTable.ParentId, attachmentId = attachmentTable.Id });
                    model.id = attachmentTable.Id;
                    model.parentId = attachmentTable.ParentId;
                    model.title = attachmentTable.FileDescription;
                    model.documentNumber = attachmentTable.DocumentNumber;
                    model.activity = attachmentTable.Activity;
                    //model.extension = System.IO.Path.GetExtension(attachmentTable.FileName).ToLowerInvariantSafe();
                    model.documentType = attachmentTable.DocumentTypeId;
                    model.serialNumber = attachmentTable.SerialNumber;
                }
                return model;
            }
            return null;
        }

        private static bool IsImage(string ext)
        {
            if (string.IsNullOrEmpty(ext))
                return false;
            return ext.Equals(".gif", StringComparison.InvariantCultureIgnoreCase)
                || ext.Equals(".jpg", StringComparison.InvariantCultureIgnoreCase)
                || ext.Equals(".jpeg", StringComparison.InvariantCultureIgnoreCase)
                || ext.Equals(".png", StringComparison.InvariantCultureIgnoreCase);
        }

        private static string EncodeFile(string fileName)
        {
            byte[] bytes;
            using (Image image = Image.FromFile(fileName))
            {
                var ratioX = (double)80 / image.Width;
                var ratioY = (double)80 / image.Height;
                var ratio = Math.Min(ratioX, ratioY);
                var newWidth = (int)(image.Width * ratio);
                var newHeight = (int)(image.Height * ratio);
                var newImage = new Bitmap(newWidth, newHeight);
                Graphics.FromImage(newImage).DrawImage(image, 0, 0, newWidth, newHeight);
                ImageConverter converter = new ImageConverter();
                bytes = (byte[])converter.ConvertTo(newImage, typeof(byte[]));
                newImage.Dispose();
            }
            return Convert.ToBase64String(bytes);
        }

        private static double ConvertBytesToMegabytes(long bytes)
        {
            return (bytes / 1024f) / 1024f;
        }
    }
}