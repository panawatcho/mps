﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TheMall.MPS.API.Services;
using TheMall.MPS.Infrastructure.Extension;
using TheMall.MPS.Models.CashAdvance;
using TheMall.MPS.Models.Enums;
using TheMall.MPS.Models.PettyCash;
using TheMall.MPS.Reports.PettyCash;
using TheMall.MPS.Reports.Shared;
using TheMall.MPS.ViewModels.PettyCash;

namespace TheMall.MPS.API.Mappers
{
    public interface IReportPettyCashMapper
    {
        IEnumerable<PettyCashDataSource> ReportDataMapper(PettyCashViewModel cashAdvanceViewModel);
        IEnumerable<ApproverDataSource> ApproversDataMapper(IEnumerable<MPS_PettyCashApproval> approversData);
    }
    public class ReportPettyCashMapper: IReportPettyCashMapper
    {
        
        private readonly ICompanyService _companyService;

        public ReportPettyCashMapper(ICompanyService companyService) : base()
        {
            _companyService = companyService;
        }

        public IEnumerable<PettyCashDataSource> ReportDataMapper(PettyCashViewModel pettyCashViewModel)
        {
            var list = new List<PettyCashDataSource>();
            if (pettyCashViewModel != null)
            {
                foreach (var line in pettyCashViewModel.PettyCashLines)
                {
                    var reportDataSource = new PettyCashDataSource();

                    reportDataSource.Id = line.Id;

                    if (pettyCashViewModel.Branch != null)
                    {
                        //reportDataSource.Branch = pettyCashViewModel.Branch.PlaceCode;
                        reportDataSource.Branch = pettyCashViewModel.Branch.Description;
                    }
                    else
                    {
                        reportDataSource.Branch = "";
                    }

                    if (pettyCashViewModel.Company != null)
                    {
                        var company = _companyService.Find(m => m.VendorCode.Equals(pettyCashViewModel.Company.VendorCode));
                        reportDataSource.Company = company.CompanyName;
                    }
                    else
                    {
                        var dataCompany = _companyService.Find(m =>m.VendorCode.Equals(pettyCashViewModel.Branch.PlaceCode));
                        reportDataSource.Company = dataCompany.CompanyName;
                    }

                    if (pettyCashViewModel.Requester != null)
                    {
                        reportDataSource.RequesterName = pettyCashViewModel.Requester.FullName;
                        reportDataSource.RequesterDepositName = pettyCashViewModel.Requester.PositionName;
                    }
                    else
                    {
                        reportDataSource.RequesterName = "";
                        reportDataSource.RequesterDepositName = "";
                    }

                    if (pettyCashViewModel.DocumentNumber != null)
                    {
                        reportDataSource.DocumentNumber = pettyCashViewModel.DocumentNumber;
                    }
                    else
                    {
                        reportDataSource.DocumentNumber = "";
                    }

                    reportDataSource.Date = DateTime.Now.ToString("dd/MM/yyyy");                   
                    reportDataSource.LineDescription = line.Description;
                    reportDataSource.LineAmount = line.Amount;
                    reportDataSource.Tax = line.Tax;
                    reportDataSource.Vat = line.Vat;
                    reportDataSource.LineNetAmount = line.NetAmount;
                    reportDataSource.LineUnit = line.Unit;
                    reportDataSource.LineRemark = line.Remark;
                    reportDataSource.Budget = (decimal) pettyCashViewModel.PettyCashLines.Sum(m => m.NetAmount);
                    reportDataSource.BudgetInCharacter = reportDataSource.Budget.ToWords();

                    if (pettyCashViewModel.UnitCode != null)
                    {
                        reportDataSource.UnitCode = pettyCashViewModel.UnitCode.UnitCode;
                        reportDataSource.UnitName = pettyCashViewModel.UnitCode.UnitName;
                    }

                    if (pettyCashViewModel.ProposalRef != null)
                    {
                        if (pettyCashViewModel.ProposalRef.ProposalUnitCode != null)
                        {
                            reportDataSource.ProposalUnitCode = pettyCashViewModel.ProposalRef.ProposalUnitCode.UnitCode;
                            reportDataSource.ProposalUnitName = pettyCashViewModel.ProposalRef.ProposalUnitCode.UnitName;
                        }
                        reportDataSource.StartDate = pettyCashViewModel.ProposalRef.StartDate.ToString("dd/MM/yyyy");
                        reportDataSource.EndDate = pettyCashViewModel.ProposalRef.EndDate.ToString("dd/MM/yyyy");
                        reportDataSource.ProposalNumber = pettyCashViewModel.ProposalRef.DocumentNumber;
                        reportDataSource.ProposalName = pettyCashViewModel.ProposalRef.Title;
                    }

                    if (pettyCashViewModel.SubmittedDate != null)
                    {
                        DateTime submitDate = (DateTime) pettyCashViewModel.SubmittedDate;
                        reportDataSource.SubmitDate = submitDate.ToString("dd/MM/yyyy");
                    }

                    reportDataSource.GoodsIssue = pettyCashViewModel.GoodsIssue;

                    if (pettyCashViewModel.Description != null)
                    {
                        reportDataSource.Objective = pettyCashViewModel.Description.Replace("font-family", string.Empty);
                        var imgs = reportDataSource.Objective.Contains("img");
                        reportDataSource.ObjectiveImg = imgs ? "*ดูรูปเพิ่มเติมได้ในระบบ" : "";
                    }

                    if (pettyCashViewModel.ExpenseTopic != null)
                    {
                        reportDataSource.ExpenseTopic = pettyCashViewModel.ExpenseTopic.ExpenseTopicCode + " - " + pettyCashViewModel.ExpenseTopic.ExpenseTopicName;
                    }

                    if (pettyCashViewModel.APCode != null)
                    {
                        reportDataSource.APCode = pettyCashViewModel.APCode.APCode + " - " + pettyCashViewModel.APCode.APName;
                    }

                    list.Add(reportDataSource);
                }
                return list;
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<ApproverDataSource> ApproversDataMapper(IEnumerable<MPS_PettyCashApproval> approversData)
        {
            if (approversData != null)
            {
                var list = new List<ApproverDataSource>();
                foreach (var approverData in approversData)
                {
                    var reportApproverDataSource = new ApproverDataSource() { };
                    reportApproverDataSource.ApproverId = approverData.Id;
                    reportApproverDataSource.ApproverSequence = approverData.ApproverSequence;
                    reportApproverDataSource.ApproverUsername = approverData.ApproverUserName;
                    reportApproverDataSource.ApproverFullName = approverData.ApproverFullName;
                    reportApproverDataSource.ApproverLevel = approverData.ApproverLevel;
                    reportApproverDataSource.ApproverPosition = approverData.Position;
                    var comment = approverData.PettyCashTable.Comments;
                    var approverCheck = approverData.ApproverLevel + " - Approval";

                    if (comment != null)
                    {
                        comment = comment.Where(m => m.Activity.ToLower() != "start").ToList();

                        var approve = comment.LastOrDefault(m =>
                            m.CreatedBy.Equals(approverData.ApproverUserName,
                                StringComparison.InvariantCultureIgnoreCase) &&
                            m.Action.Equals(DocumentStatusFlag.Approve.ToString(),
                                StringComparison.InvariantCultureIgnoreCase) &&
                            m.Activity.Equals(approverCheck, StringComparison.InvariantCultureIgnoreCase));
                        if (approve != null)
                        {
                            reportApproverDataSource.ApproveDateTime = approve.CreatedDate != null
                                ? approve.CreatedDate.Value.ToString("dd/MM/yyyy")
                                : "";
                        }
                        else
                        {
                            reportApproverDataSource.ApproveDateTime = "";
                        }
                        //var approve = comment.LastOrDefault(m => m.CreatedBy.Equals(approverData.ApproverUserName) && m.Action.Equals(DocumentStatusFlag.Approve.ToString()));
                        //reportApproverDataSource.ApproveDateTime = (approve!=null && approve.CreatedDate!=null)?approve.CreatedDate.Value.ToString("dd/MM/yyyy"):"";
                    }
                    
                    if (approverData.ApproverLevel.Equals(Approval.ApproveLevelList.Propose.GetDescription()))
                        reportApproverDataSource.ApproverLevelKey = Approval.ApproveKey.Propose;
                    else if (approverData.ApproverLevel.Equals(Approval.ApproveLevelList.Accept.GetDescription()))
                        reportApproverDataSource.ApproverLevelKey = Approval.ApproveKey.Accept;
                    else if (approverData.ApproverLevel.Equals(Approval.ApproveLevelList.Approval.GetDescription()))
                        reportApproverDataSource.ApproverLevelKey = Approval.ApproveKey.Approval;
                    else if (approverData.ApproverLevel.Equals(Approval.ApproveLevelList.FinalApproval.GetDescription()))
                        reportApproverDataSource.ApproverLevelKey = Approval.ApproveKey.FinalApproval;
                    else
                    {
                        reportApproverDataSource.ApproverLevelKey = 0;
                    }

                    list.Add(reportApproverDataSource);
                }
                return list;
            }
            else
            {
                return null;
            }
        }
    }
}