﻿using System.Collections.Generic;
using System.Linq;
using TheMall.MPS.Models.Example;
using TheMall.MPS.ViewModels.Example;

namespace TheMall.MPS.API.Mappers
{
    public static class ExampleMapper
    {
        public static ExampleTable ToModel(this ExampleViewModel viewModel, ExampleTable model = null)
        {
            if (viewModel == null)
            {
                return null;
            }
            if (model == null)
            {
                model = new ExampleTable();
            }
            model.Id = viewModel.Id;
            model.Name = viewModel.Name;

            return model;
        }
        public static ExampleViewModel ToViewModel(this ExampleTable model, System.Web.Http.Routing.UrlHelper url)
        {
            if (model == null)
            {
                return null;
            }
            var viewModel = new ExampleViewModel();
            viewModel.Id = model.Id;
            viewModel.Name = model.Name;

            viewModel.Lines = model.Lines.ToViewModels(url);
            viewModel.Attachments = model.Attachments.ToViewModels(url);

            return viewModel;
        }

        public static IEnumerable<ExampleLine> ToModel(this IEnumerable<ExampleLineViewModel> viewModels, IReadOnlyList<ExampleLine> models = null)
        {
            if (viewModels == null)
            {
                return Enumerable.Empty<ExampleLine>();
            }

            var lines = new List<ExampleLine>();
            foreach (var viewModel in viewModels)
            {
                var line = InitLineTable(viewModel, models);

                line.Name = viewModel.Name;

                lines.Add(line);
            }
            return lines;
        }

        public static IEnumerable<ExampleLineViewModel> ToViewModels(this IEnumerable<ExampleLine> models, System.Web.Http.Routing.UrlHelper url)
        {
            if (models == null)
            {
                return Enumerable.Empty<ExampleLineViewModel>();
            }

            var lines = new List<ExampleLineViewModel>();
            foreach (var model in models)
            {
                var line = InitLineViewModel<ExampleLineViewModel>(model);

                line.Name = model.Name;

                line.Attachments = model.Attachments.ToViewModels(url);

                lines.Add(line);
            }
            return lines;
        }

        public static T InitLineTable<T>(TheMall.MPS.ViewModels.Interfaces.ILineViewModel viewModel, IEnumerable<T> lines)
           where T : class, Models.Interfaces.ILineTable, new()
        {
            T model = null;
            if (viewModel.Id > 0)
            {
                if (lines != null)
                {
                    model = lines.FirstOrDefault(m => m.Id == viewModel.Id);
                }
                if (model == null)
                {
                    throw Exceptions.Helper.DataWithIDNotExists(viewModel.Id);
                }
            }
            else
            {
                model = new T();

                model.Id = viewModel.Id;
                model.ParentId = viewModel.ParentId;
                model.LineNo = viewModel.LineNo;
                //model.StatusFlag = viewModel.StatusFlag;
            }

            model.Deleted = viewModel.Deleted;

            return model;
        }

        public static T InitLineViewModel<T>(Models.Interfaces.ILineTable lineTable)
            where T : class, MPS.ViewModels.Interfaces.ILineViewModel, new()
        {
            var viewModel = new T
            {
                Id = lineTable.Id,
                ParentId = lineTable.ParentId,
                LineNo = lineTable.LineNo,
                Deleted = lineTable.Deleted,
                //StatusFlag = lineTable.StatusFlag
            };

            return viewModel;
        }
    }
}