﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TheMall.MPS.API.Mappers.Abstracts;
using TheMall.MPS.API.Services;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Mappers.Abstracts;
using TheMall.MPS.Models.Memo;
using TheMall.MPS.Models.MemoIncome;
using TheMall.MPS.Models.PettyCash;
using TheMall.MPS.Models.Proposal;
using TheMall.MPS.ViewModels.Memo;
using TheMall.MPS.ViewModels.MemoIncome;


namespace TheMall.MPS.API.Mappers
{
    public interface IMemoIncomeLineMapper : ILineMapper
    {
        MPS_MemoIncomeLine ToModel(MemoIncomeLineViewModel viewModel, IEnumerable<MPS_MemoIncomeLine> lines);
        MemoIncomeLineViewModel ToViewModel(MPS_MemoIncomeLine model);

        IEnumerable<MemoIncomeLineViewModel> ToViewModels(IEnumerable<MPS_MemoIncomeLine> models);
        IEnumerable<MPS_MemoIncomeLine> ToModels(IEnumerable<MemoIncomeLineViewModel> viewModels, IEnumerable<MPS_MemoIncomeLine> lines);
    }

    public class MemoIncomeLineMapper : BaseLineMapper, IMemoIncomeLineMapper
    {

        public MPS_MemoIncomeLine ToModel(MemoIncomeLineViewModel viewModel, IEnumerable<MPS_MemoIncomeLine> lines)
        {
            if (viewModel == null)
            {
                return null;
            }

            var model = InitLineTable(viewModel, lines);

            if (model == null)
            {
                return null;
            }

            model.Description = viewModel.Description;
            model.Unit = viewModel.Unit;
            model.Amount = viewModel.Amount;
            model.Vat = viewModel.Vat;
            model.Tax = viewModel.Tax;
            model.NetAmount = viewModel.NetAmount;
            model.Remark = viewModel.Remark;

            return model;
        }

        public MemoIncomeLineViewModel ToViewModel(MPS_MemoIncomeLine model)
        {
            if (model == null)
            {
                return null;
            }
            var viewModel = this.InitLineViewModel<MemoIncomeLineViewModel>(model);

           
            viewModel.Description = model.Description;
            viewModel.Unit = model.Unit;
            viewModel.Amount = model.Amount;
            viewModel.Vat = model.Vat;
            viewModel.Tax = model.Tax;
            viewModel.NetAmount = model.NetAmount; 
            viewModel.Remark = model.Remark;

            viewModel.NetAmountNoVatTax = viewModel.Unit*viewModel.Amount;


            return viewModel;
        }

        public IEnumerable<MemoIncomeLineViewModel> ToViewModels(IEnumerable<MPS_MemoIncomeLine> models)
        {
            return models != null ? models.Select(ToViewModel) : Enumerable.Empty<MemoIncomeLineViewModel>();
        }
        public IEnumerable<MPS_MemoIncomeLine> ToModels(IEnumerable<MemoIncomeLineViewModel> viewModels, IEnumerable<MPS_MemoIncomeLine> lines)
        {
            return viewModels != null ? viewModels.Select(m => ToModel(m, lines)).Where(m => m != null) : Enumerable.Empty<MPS_MemoIncomeLine>();
        }
       
    }
}