﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using TheMall.MPS.Models.Master;
using TheMall.MPS.ViewModels.Master;

namespace TheMall.MPS.API.Mappers
{
    public class AllocateBasisMapperProfile : AutoMapper.Profile
    {
        public AllocateBasisMapperProfile()
        {
            CreateMap<MPS_M_AllocateBasis, AllocateBasisViewModel>()
                .ReverseMap();
        }

    }

    public static class AllocateBasisMapper
    {
         public static MPS_M_AllocateBasis ToModel(this AllocateBasisViewModel viewModel)
        {
            if (viewModel == null)
                return null;
            var model = new MPS_M_AllocateBasis()
            {
                AllocateType = viewModel.AllocateType,
                Description = viewModel.Description
            };
            return model;
           
        }

        public static AllocateBasisViewModel ToViewModel(this MPS_M_AllocateBasis model)
        {
            if (model == null)
                return null;
            var viewModel = new AllocateBasisViewModel()
            {
                AllocateType = model.AllocateType,
                Description = model.Description,
            };
            viewModel.SharedPercents = model.SharedPercent.ToViewModels();
            return viewModel;
           
        }
        public static IEnumerable<AllocateBasisViewModel> ToViewModels(this IEnumerable<MPS_M_AllocateBasis> models)
        {
            return models != null ? models.Select(ToViewModel) : Enumerable.Empty<AllocateBasisViewModel>();
        }
        public static IEnumerable<MPS_M_AllocateBasis> ToModels(IEnumerable<AllocateBasisViewModel> viewModels, IEnumerable<MPS_M_AllocateBasis> lines)
        {
            return viewModels != null ? viewModels.Select(ToModel) : Enumerable.Empty<MPS_M_AllocateBasis>();
        }
   
    }
}