﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TheMall.MPS.Mappers.Abstracts;
using TheMall.MPS.Models.CashAdvance;
using TheMall.MPS.Models.PettyCash;
using TheMall.MPS.ViewModels.CashAdvance;
using TheMall.MPS.ViewModels.PettyCash;

namespace TheMall.MPS.API.Mappers
{
    public interface ICashAdvanceLineMapper : ILineMapper
    {
        MPS_CashAdvanceLine ToModel(CashAdvanceLineViewModel viewModel, IEnumerable<MPS_CashAdvanceLine> lines);
        CashAdvanceLineViewModel ToViewModel(MPS_CashAdvanceLine model);
        IEnumerable<CashAdvanceLineViewModel> ToViewModels(IEnumerable<MPS_CashAdvanceLine> models);
        IEnumerable<MPS_CashAdvanceLine> ToModels(IEnumerable<CashAdvanceLineViewModel> viewModels, IEnumerable<MPS_CashAdvanceLine> lines);

        CashAdvanceLineViewModel ToViewModelNoLineId(MPS_CashAdvanceLine model);
        IEnumerable<CashAdvanceLineViewModel> ToViewModelsNoLineId(IEnumerable<MPS_CashAdvanceLine> models);

        IEnumerable<MPS_CashAdvanceLine> ToModelsNoLineId(IEnumerable<CashAdvanceLineViewModel> viewModels, IEnumerable<MPS_CashAdvanceLine> lines);
    }

    public class CashAdvanceLineMapper : BaseLineMapper, ICashAdvanceLineMapper
    {
        public MPS_CashAdvanceLine ToModel(CashAdvanceLineViewModel viewModel, IEnumerable<MPS_CashAdvanceLine> lines)
        {
            if (viewModel == null)
            {
                return null;
            }
            var model = InitLineTable(viewModel, lines);
            if (model == null)
            {
                return null;
            }
            model.Description = viewModel.Description;
            model.Unit = viewModel.Unit;
            model.Amount = viewModel.Amount;
            model.Vat = viewModel.Vat;
            model.Tax = viewModel.Tax;
            model.Remark = viewModel.Remark;
            model.NetAmount = viewModel.NetAmount;
            return model;
        }

        public CashAdvanceLineViewModel ToViewModel(MPS_CashAdvanceLine model)
        {
            if (model == null)
            {
                return null;
            }
            var viewModel = this.InitLineViewModel<CashAdvanceLineViewModel>(model);

            if (model.Description != null)
            {
                viewModel.Description = model.Description;
            }
            else
            {
                viewModel.Description = "";
            }

            viewModel.Unit = model.Unit;
            viewModel.Amount = model.Amount;
            viewModel.Tax = model.Tax;
            viewModel.Vat = model.Vat;
            viewModel.NetAmount = model.NetAmount;
            if (model.Remark != null)
            {
                viewModel.Remark = model.Remark;
            }
            else
            {
                viewModel.Remark = "";
            }
            viewModel.NetAmountNoVatTax = viewModel.Unit * viewModel.Amount;
            return viewModel;
        }

        public IEnumerable<MPS_CashAdvanceLine> ToModels(IEnumerable<CashAdvanceLineViewModel> viewModels, 
            IEnumerable<MPS_CashAdvanceLine> lines)
        {
            return viewModels != null ? viewModels.Select(m => ToModel(m, lines)).Where(m => m != null) : Enumerable.Empty<MPS_CashAdvanceLine>();
        }


        public IEnumerable<CashAdvanceLineViewModel> ToViewModels(IEnumerable<MPS_CashAdvanceLine> models)
        {
            return models != null ? models.Select(ToViewModel) : Enumerable.Empty<CashAdvanceLineViewModel>();
        }
        /////////////////////
        /// 
        /// 
        public CashAdvanceLineViewModel ToViewModelNoLineId(MPS_CashAdvanceLine model)
        {
            if (model == null)
            {
                return null;
            }
            var viewModel = this.InitLineViewModel<CashAdvanceLineViewModel>(model);

            viewModel.Description = model.Description;
            viewModel.Unit = model.Unit;
            viewModel.Amount = model.Amount;
            viewModel.Remark = model.Remark;
            viewModel.NetAmount = model.NetAmount;
            viewModel.Id = 0;
            return viewModel;
        }

        public IEnumerable<CashAdvanceLineViewModel> ToViewModelsNoLineId(IEnumerable<MPS_CashAdvanceLine> models)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<MPS_CashAdvanceLine> ToModelsNoLineId(IEnumerable<CashAdvanceLineViewModel> viewModels, IEnumerable<MPS_CashAdvanceLine> lines)
        {
            return viewModels != null ? viewModels.Select(m => ToModel(m, lines)).Where(m => m != null) : Enumerable.Empty<MPS_CashAdvanceLine>();
        }
    }
}