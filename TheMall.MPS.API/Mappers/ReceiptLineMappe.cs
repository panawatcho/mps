﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper.Execution;
using TheMall.MPS.API.Services;
using TheMall.MPS.Models;
using TheMall.MPS.Models.Master;
using TheMall.MPS.Models.Receipt;
using TheMall.MPS.ViewModels;
using TheMall.MPS.ViewModels.Receipt;
using TheMall.MPS.ViewModels.Master;
using TheMall.MPS.ViewModels.Interfaces;

namespace TheMall.MPS.API.Mappers
{
    // call
    public interface IReceiptLineMapper
    {
        ReceiptLineViewModel ToViewModel(MPS_ReceiptLine model);
        IEnumerable<ReceiptLineViewModel> ToViewModels(IEnumerable<MPS_ReceiptLine> models);
        MPS_ReceiptLine ToModel(ReceiptLineViewModel viewModel);
        IEnumerable<MPS_ReceiptLine> ToModels(IEnumerable<ReceiptLineViewModel> viewModels, IEnumerable<MPS_ReceiptLine> lines);
        IEnumerable<MPS_ReceiptLine> ToModel(IEnumerable<ReceiptLineViewModel> models);
    }
    public class ReceiptLineMapper : IReceiptLineMapper
    {
        // import
        private readonly IReceiptLineService _receiptLineService;

        public ReceiptLineMapper(
            IReceiptLineService receiptLineService)
        {
            _receiptLineService = receiptLineService;
        }

        // model to view model
        // display
        public ReceiptLineViewModel ToViewModel( MPS_ReceiptLine model)
        {
            if (model == null)
                return null;

            var viewModel = new ReceiptLineViewModel();

            viewModel.Id = model.Id;
            viewModel.LineNo = model.LineNo;
            viewModel.Description = model.Description;
            viewModel.Unit = model.Unit;
            viewModel.Amount = model.Amount;
            
            if (model.Remark != null)
            {
                viewModel.Remark = model.Remark;
            }
            else
            {
                viewModel.Remark = "";
            }
            
            viewModel.Vat = model.Vat;
            viewModel.Tax = model.Tax;
            viewModel.NetAmount = model.NetAmount;
            viewModel.NetAmountNoVatTax = model.NetAmountNoVatTax;
            viewModel.ReceiptTableId = model.ReceiptTableId;
            

            return viewModel;

        }

        public IEnumerable<ReceiptLineViewModel> ToViewModels( IEnumerable<MPS_ReceiptLine> models)
        {
            return models != null ? models.Select(ToViewModel) : Enumerable.Empty<ReceiptLineViewModel>();
        }

        // view model to model
        // insert
        public MPS_ReceiptLine ToModel( ReceiptLineViewModel viewModel)
        {
            if (viewModel == null)
                return null;
            var model = new MPS_ReceiptLine();

            if (viewModel.Id != 0)
            {
                // search id
                model = _receiptLineService.Find(m => m.Id == viewModel.Id);
                if (model != null)
                {
                    model.Description = viewModel.Description;
                    model.Unit = viewModel.Unit;
                    model.Amount = viewModel.Amount;
                    model.Remark = viewModel.Remark;
                    model.Vat = viewModel.Vat;
                    model.Tax = viewModel.Tax;
                    model.NetAmount = viewModel.NetAmount;
                    model.NetAmountNoVatTax = viewModel.NetAmountNoVatTax;
                    model.Deleted = viewModel.Deleted;
                    model.DeletedBy = viewModel.DeletedBy;
                    model.DeletedByName = viewModel.DeletedByName;
                }
            }
            else
            {
                model.LineNo = viewModel.LineNo;
                model.Description = viewModel.Description;
                model.Unit = viewModel.Unit;
                model.Amount = viewModel.Amount;
                model.Remark = viewModel.Remark;
                model.Vat = viewModel.Vat;
                model.Tax = viewModel.Tax;
                model.NetAmount = viewModel.NetAmount;
                model.NetAmountNoVatTax = viewModel.NetAmountNoVatTax;
                model.ReceiptTableId = viewModel.ReceiptTableId;
                model.Deleted = viewModel.Deleted;
                model.DeletedDate = viewModel.DeletedDate;
                model.DeletedBy = viewModel.DeletedBy;
                model.DeletedByName = viewModel.DeletedByName;
            }
           
            return model;
        }

        public IEnumerable<MPS_ReceiptLine> ToModels(IEnumerable<ReceiptLineViewModel> viewModels, IEnumerable<MPS_ReceiptLine> lines)
        {
            return viewModels != null ? viewModels.Select(ToModel) : Enumerable.Empty<MPS_ReceiptLine>();
        }

        public IEnumerable<MPS_ReceiptLine> ToModel( IEnumerable<ReceiptLineViewModel> models)
        {
            return models != null ? models.Select(ToModel) : Enumerable.Empty<MPS_ReceiptLine>();
        }
    }
}