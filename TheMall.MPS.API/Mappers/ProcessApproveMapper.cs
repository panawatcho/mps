﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TheMall.MPS.Models.Master;
using TheMall.MPS.ViewModels.Master;

namespace TheMall.MPS.API.Mappers
{
    public class ProcessApproveMapperProfile : AutoMapper.Profile
    {
        public ProcessApproveMapperProfile()
        {
            CreateMap<MPS_M_ProcessApprove, ProcessApproveViewModel>()
                .ReverseMap();
        }
    }

    public static class ProcessApproveMapper
    {
        public static MPS_M_ProcessApprove ToModel(this ProcessApproveViewModel viewModel)
        {
            var model = new MPS_M_ProcessApprove();
            model.ProcessCode = viewModel.ProcessCode;
            model.ProcessName = viewModel.ProcessName;
            model.ProcessType = viewModel.ProcessType;
            model.InActive = viewModel.InActive;
            return model;
        }

        public static ProcessApproveViewModel ToViewModel(this MPS_M_ProcessApprove model)
        {
            var viewModel = new ProcessApproveViewModel();
            viewModel.ProcessName = model.ProcessName;
            viewModel.ProcessCode = model.ProcessCode;
            viewModel.InActive = model.InActive;
            viewModel.ProcessType = model.ProcessType;
            return viewModel;
        }

        public static IEnumerable<ProcessApproveViewModel> ToViewModels(this IEnumerable<MPS_M_ProcessApprove> models)
        {
            return models != null ? models.Select(ToViewModel) : Enumerable.Empty<ProcessApproveViewModel>();
        }
        public static IEnumerable<MPS_M_ProcessApprove> ToModels(IEnumerable<ProcessApproveViewModel> viewModels, IEnumerable<MPS_M_ProcessApprove> lines)
        {
            return viewModels != null ? viewModels.Select(ToModel) : Enumerable.Empty<MPS_M_ProcessApprove>();
        }
    }
}