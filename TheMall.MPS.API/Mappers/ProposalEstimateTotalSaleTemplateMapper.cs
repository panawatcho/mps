﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using TheMall.MPS.API.Mappers.Abstracts;
using TheMall.MPS.API.Services;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Mappers.Abstracts;
using TheMall.MPS.Models.Proposal;
using TheMall.MPS.ViewModels.Proposal;

namespace TheMall.MPS.API.Mappers
{
    public interface IProposalEstimateTotalSaleTemplateMapper 
    {
        MPS_EstimateTotalSaleTemplate ToModel(EstimateTotalSaleTemplateViewModel viewModel);
        EstimateTotalSaleTemplateViewModel ToViewModel(MPS_EstimateTotalSaleTemplate model);
        IEnumerable<MPS_EstimateTotalSaleTemplate> ToModels(IEnumerable<EstimateTotalSaleTemplateViewModel> viewModels);
        IEnumerable<EstimateTotalSaleTemplateViewModel> ToViewModels(IEnumerable<MPS_EstimateTotalSaleTemplate> models);
        IEnumerable<MPS_EstimateTotalSaleTemplate> ToEstimateTotalSaleTemplateModel(IEnumerable<ProposalPercentSharedViewModel> percentSharedViewModels, IEnumerable<MPS_EstimateTotalSaleTemplate> lineAp);
    }

    public class ProposalEstimateTotalSaleTemplateMapper : IProposalEstimateTotalSaleTemplateMapper
    {
        private readonly IProposalEstimateTotalSaleService _service;

        public ProposalEstimateTotalSaleTemplateMapper(IProposalEstimateTotalSaleService service) 
        {
            _service = service;
        }


        public MPS_EstimateTotalSaleTemplate ToModel(EstimateTotalSaleTemplateViewModel viewModel)
        {
           if (viewModel == null)
           {
               return null;
           }
           var model = new MPS_EstimateTotalSaleTemplate();

           model.TR_Amount = viewModel.TR_Amount;
           model.TR_Percent = viewModel.TR_Percent;
           model.RE_Amount = viewModel.RE_Amount;
           model.RE_Percent = viewModel.RE_Percent;
           model.PercentOfDepartment = viewModel.PercentOfDepartment;
           model.BranchCode = viewModel.BranchCode;
           model.EstimateTotalSaleId = viewModel.EstimateTotalSaleId;


           return model;
        }

        public EstimateTotalSaleTemplateViewModel ToViewModel(MPS_EstimateTotalSaleTemplate model)
        {
           if (model == null)
           {
               return null;
           }
           var viewModel = new EstimateTotalSaleTemplateViewModel()
           {
               TR_Amount = model.TR_Amount,
               TR_Percent = model.TR_Percent,
               RE_Amount = model.RE_Amount,
               RE_Percent = model.RE_Percent,
               BranchCode = model.BranchCode,
               EstimateTotalSaleId = model.EstimateTotalSaleId,
               Id = model.Id

           };

           return viewModel;
        }

        public IEnumerable<MPS_EstimateTotalSaleTemplate> ToModels(IEnumerable<EstimateTotalSaleTemplateViewModel> viewModels)
        {
            return viewModels != null ? viewModels.Select(ToModel) : Enumerable.Empty<MPS_EstimateTotalSaleTemplate>();
        }

        public IEnumerable<EstimateTotalSaleTemplateViewModel> ToViewModels(IEnumerable<MPS_EstimateTotalSaleTemplate> models)
        {
            return models != null ? models.Select(ToViewModel) : Enumerable.Empty<EstimateTotalSaleTemplateViewModel>();
        }

     

        public IEnumerable<MPS_EstimateTotalSaleTemplate> ToEstimateTotalSaleTemplateModel(IEnumerable<ProposalPercentSharedViewModel> percentSharedViewModels, IEnumerable<MPS_EstimateTotalSaleTemplate> linetemplate)
        {
            var list = new List<MPS_EstimateTotalSaleTemplate>();
            foreach (var percentShared in percentSharedViewModels)
            {
                if (percentShared.lineId <= 0)
                {
                    var incomTotalSaleTemplate = new MPS_EstimateTotalSaleTemplate()
                    {
                        BranchCode = percentShared.BranchCode,
                        TR_Amount = percentShared.TR_Amount,
                        TR_Percent = percentShared.TR_Percent,
                        RE_Amount = percentShared.RE_Amount,
                        RE_Percent = percentShared.RE_Percent,

                    };
                    list.Add(incomTotalSaleTemplate);
                }
                else
                {
                    if (linetemplate != null)
                    {
                        var incomTotalSaleTemplate = linetemplate.LastOrDefault(m => m.Id == percentShared.lineId);
                        if (incomTotalSaleTemplate != null)
                        {
                            incomTotalSaleTemplate.BranchCode = percentShared.BranchCode;
                            incomTotalSaleTemplate.TR_Amount = percentShared.TR_Amount;
                            incomTotalSaleTemplate.TR_Percent = percentShared.TR_Percent;
                            incomTotalSaleTemplate.RE_Amount = percentShared.RE_Amount;
                            incomTotalSaleTemplate.RE_Percent = percentShared.RE_Percent;
                            list.Add(incomTotalSaleTemplate);
                        }
                        else
                        {
                            incomTotalSaleTemplate = new MPS_EstimateTotalSaleTemplate()
                            {
                                BranchCode = percentShared.BranchCode,
                                TR_Amount = percentShared.TR_Amount,
                                TR_Percent = percentShared.TR_Percent,
                                RE_Amount = percentShared.RE_Amount,
                                RE_Percent = percentShared.RE_Percent,
                            };
                            list.Add(incomTotalSaleTemplate);
                        }
                    }


                }
            }
            return list;
        }
    }
}