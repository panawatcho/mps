﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using TheMall.MPS.Models.Master;
using TheMall.MPS.ViewModels.Master;

namespace TheMall.MPS.API.Mappers
{
    public class TypeMemoMapperProfile : AutoMapper.Profile
    {
        public TypeMemoMapperProfile()
        {
            CreateMap<MPS_M_TypeMemo, TypeMemoViewModel>()
                .ReverseMap();
        }
    }
    public static class TypeMemoMapper
    {
        public static MPS_M_TypeMemo ToModel(this TypeMemoViewModel viewModel)
        {
            if (viewModel == null)
                return null;
            var model = new MPS_M_TypeMemo();
            
            model.TypeMemoCode = viewModel.TypeMemoCode;
            model.TypeMemoName = viewModel.TypeMemoName;
            model.InActive = viewModel.InActive;
            model.Order = viewModel.Order;
            model.ActualCharge = viewModel.ActualCharge;
            if (!viewModel.ActualCharge)
            {
                if (viewModel.UnitCode != null)
                {
                    model.UnitCode = viewModel.UnitCode.UnitCode;
                    model.UnitName = viewModel.UnitCode.UnitName;
                }
            }
            return model;

            return Mapper.Map<MPS_M_TypeMemo>(viewModel);
        }

        public static TypeMemoViewModel ToViewModel(this MPS_M_TypeMemo model)
        {
            if (model == null)
                return null;
            var viewModel = new TypeMemoViewModel()
            {
                TypeMemoCode = model.TypeMemoCode,
                TypeMemoName = model.TypeMemoName,
                UnitCode = new MPS_M_UnitCode()
                {
                    UnitCode = model.UnitCode,
                    UnitName = model.UnitName
                }.ToViewModel(),
                InActive = model.InActive,
                Order = model.Order,
                ActualCharge = model.ActualCharge
            };
            return viewModel;

            return Mapper.Map<TypeMemoViewModel>(model);
        }
        public static IEnumerable<TypeMemoViewModel> ToViewModels(this IEnumerable<MPS_M_TypeMemo> models)
        {
            return models != null ? models.Select(ToViewModel) : Enumerable.Empty<TypeMemoViewModel>();
        }
        public static IEnumerable<MPS_M_TypeMemo> ToModels(IEnumerable<TypeMemoViewModel> viewModels, IEnumerable<MPS_M_TypeMemo> lines)
        {
            return viewModels != null ? viewModels.Select(ToModel) : Enumerable.Empty<MPS_M_TypeMemo>();
        }

    }
}