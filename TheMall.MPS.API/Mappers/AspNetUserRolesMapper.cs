﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TheMall.MPS.Infrastructure.Identity;
using TheMall.MPS.ViewModels.AspNet;

namespace TheMall.MPS.API.Mappers
{
    public class AspNetUserRolesMapperMapperProfile : AutoMapper.Profile
    {
        public AspNetUserRolesMapperMapperProfile()
        {
            CreateMap<ApplicationUserRole, AspNetUserRolesViewModel>()
                .ReverseMap();
        }
    }
    public static class AspNetUserRolesMapper
    {
        public static ApplicationUserRole ToModel(this AspNetUserRolesViewModel viewModel)
        {
            if (viewModel == null)
                return null;
            var model = new ApplicationUserRole();
            model.Inactive = viewModel.Inactive;
            if (viewModel.User != null)
            {
                model.UserId = viewModel.User.Username;
            }
            if (viewModel.Role != null)
            {
                model.RoleId = viewModel.Role.Id;
            }
      
            return model;
        }


        public static AspNetUserRolesViewModel ToViewModel(this ApplicationUserRole model)
        {
            if (model == null)
                return null;
            var viewModel = new AspNetUserRolesViewModel()
            {
                Inactive = model.Inactive,
                Username = model.UserId,
                Role = model.Role.ToViewModel(),
                RoleId = model.RoleId
            };
            return viewModel;
        }


        public static IEnumerable<AspNetUserRolesViewModel> ToViewModels(this IEnumerable<ApplicationUserRole> models)
        {
            return models != null ? models.Select(ToViewModel) : Enumerable.Empty<AspNetUserRolesViewModel>();
        }
        public static IEnumerable<ApplicationUserRole> ToModels(IEnumerable<AspNetUserRolesViewModel> viewModels, IEnumerable<ApplicationUserRole> lines)
        {
            return viewModels != null ? viewModels.Select(ToModel) : Enumerable.Empty<ApplicationUserRole>();
        }
    }
}