﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TheMall.MPS.API.Mappers.Abstracts;
using TheMall.MPS.API.Services;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Mappers.Abstracts;
using TheMall.MPS.Models.Memo;
using TheMall.MPS.Models.PettyCash;
using TheMall.MPS.Models.Proposal;
using TheMall.MPS.ViewModels.Memo;
using TheMall.MPS.ViewModels.PettyCash;

namespace TheMall.MPS.API.Mappers
{
    public interface IMemoLineMapper : ILineMapper
    {
        MPS_MemoLine ToModel(MemoLineViewModel viewModel, IEnumerable<MPS_MemoLine> lines);
        MemoLineViewModel ToViewModel(MPS_MemoLine model);
        IEnumerable<MemoLineViewModel> ToViewModels(IEnumerable<MPS_MemoLine> models);
        IEnumerable<MPS_MemoLine> ToModels(IEnumerable<MemoLineViewModel> viewModels, IEnumerable<MPS_MemoLine> lines);
    }

    public class MemoLineMapper : BaseLineMapper, IMemoLineMapper
    {

        public MPS_MemoLine ToModel(MemoLineViewModel viewModel, IEnumerable<MPS_MemoLine> lines)
        {
            if (viewModel == null)
            {
                return null;
            }
            var model = InitLineTable(viewModel, lines);
            if (model == null)
            {
                return null;
            }
            model.Description = viewModel.Description;
            model.Unit = viewModel.Unit;
            model.Amount = viewModel.Amount;
            model.Vat = viewModel.Vat;
            model.Tax = viewModel.Tax;
            model.NetAmount = viewModel.NetAmount;
            model.Remark = viewModel.Remark;

            return model;
        }
        public MemoLineViewModel ToViewModel(MPS_MemoLine model)
        {
            if (model == null)
            {
                return null;
            }
            var viewModel = this.InitLineViewModel<MemoLineViewModel>(model);

         
            if ( model.Description!= null)
            {
                viewModel.Description = model.Description;
            }else
            {
                viewModel.Description = "";
            }
            viewModel.Unit = model.Unit;
            viewModel.Amount = model.Amount;
            viewModel.Vat = model.Vat;
            viewModel.Tax = model.Tax;
            viewModel.NetAmount = model.NetAmount;
            if (model.Remark != null)
            {
                viewModel.Remark = model.Remark;
            }
            else
            {
                viewModel.Remark = "";
            }

            viewModel.NetAmountNoVatTax = viewModel.Unit*viewModel.Amount;

            return viewModel;
        }

        public IEnumerable<MemoLineViewModel> ToViewModels(IEnumerable<MPS_MemoLine> models)
        {
            return models != null ? models.Select(ToViewModel) : Enumerable.Empty<MemoLineViewModel>();
        }
        public IEnumerable<MPS_MemoLine> ToModels(IEnumerable<MemoLineViewModel> viewModels, IEnumerable<MPS_MemoLine> lines)
        {
            return viewModels != null ? viewModels.Select(m => ToModel(m, lines)).Where(m => m != null) : Enumerable.Empty<MPS_MemoLine>();
        }
       
    }
}