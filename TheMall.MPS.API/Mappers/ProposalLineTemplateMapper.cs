﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Common.CommandTrees;
using System.Linq;
using System.Web;
using AutoMapper;
using TheMall.MPS.API.Mappers.Abstracts;
using TheMall.MPS.API.Services;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Mappers.Abstracts;
using TheMall.MPS.Models.Proposal;
using TheMall.MPS.ViewModels.Proposal;

namespace TheMall.MPS.API.Mappers
{
    public interface IProposalLineTemplateMapper
    {
        MPS_ProposalLineTemplate ToModel(ProposalLineTemplateViewModel viewModel);
        ProposalLineTemplateViewModel ToViewModel(MPS_ProposalLineTemplate model);
        IEnumerable<MPS_ProposalLineTemplate> ToModels(IEnumerable<ProposalLineTemplateViewModel> viewModels);
        IEnumerable<ProposalLineTemplateViewModel> ToViewModels(IEnumerable<MPS_ProposalLineTemplate> models);
        IEnumerable<MPS_ProposalLineTemplate> ToProposalLineTemplateModel(IEnumerable<ProposalPercentSharedViewModel> percentSharedViewModels, IEnumerable<MPS_ProposalLineTemplate> lineAp);
    }

    public class ProposalLineTemplateMapper : IProposalLineTemplateMapper
    {
        private readonly IProposalLineAPService _service;

        public ProposalLineTemplateMapper(IProposalLineAPService service) 
        {
            _service = service;
        }

        public MPS_ProposalLineTemplate ToModel(ProposalLineTemplateViewModel viewModel)
        {
            if (viewModel == null)
            {
                return null;
            }
            var model = new MPS_ProposalLineTemplate();
           
                model.TR_Amount = viewModel.TR_Amount;
                model.TR_Percent = viewModel.TR_Percent;
                model.RE_Amount = viewModel.RE_Amount;
                model.RE_Percent = viewModel.RE_Percent;
                model.PercentOfDepartment = viewModel.PercentOfDepartment;
                model.BranchCode = viewModel.BranchCode;
                model.ProposalLineId = viewModel.ProposalLineId;
                model.Id = viewModel.Id;
            
            return model;
        }
        public ProposalLineTemplateViewModel ToViewModel(MPS_ProposalLineTemplate model)
        {
            if (model == null)
            {
                return null;
            }
            var viewModel = new ProposalLineTemplateViewModel()
            {
                TR_Amount = model.TR_Amount,
                TR_Percent = model.TR_Percent,
                RE_Amount = model.RE_Amount,
                RE_Percent = model.RE_Percent,
                BranchCode = model.BranchCode,
                ProposalLineId = model.ProposalLineId,
                Id = model.Id 
              
            };

            return viewModel;
        }
        public IEnumerable<ProposalLineTemplateViewModel> ToViewModels(IEnumerable<MPS_ProposalLineTemplate> models)
        {
            return models != null ? models.Select(ToViewModel) : Enumerable.Empty<ProposalLineTemplateViewModel>();
        }

        public IEnumerable<MPS_ProposalLineTemplate> ToProposalLineTemplateModel(IEnumerable<ProposalPercentSharedViewModel> percentSharedViewModels, IEnumerable<MPS_ProposalLineTemplate> lineAp)
        {
            var list = new List<MPS_ProposalLineTemplate>();
           
            foreach (var percentShared in percentSharedViewModels)
            {
                if (percentShared.lineId <= 0)
                {
                    var proposalLineAp = new MPS_ProposalLineTemplate()
                    {
                        BranchCode = percentShared.BranchCode,
                        TR_Amount = percentShared.TR_Amount,
                        TR_Percent = percentShared.TR_Percent,
                        RE_Amount = percentShared.RE_Amount,
                        RE_Percent = percentShared.RE_Percent,
                         
                    };
                    list.Add(proposalLineAp);
                }
                else
                {
                    if (lineAp != null)
                    {
                        var proposalLineAp = lineAp.LastOrDefault(m => m.Id == percentShared.lineId);
                        if (proposalLineAp != null)
                        {
                            proposalLineAp.BranchCode = percentShared.BranchCode;
                            proposalLineAp.TR_Amount = percentShared.TR_Amount;
                            proposalLineAp.TR_Percent = percentShared.TR_Percent;
                            proposalLineAp.RE_Amount = percentShared.RE_Amount;
                            proposalLineAp.RE_Percent = percentShared.RE_Percent;
                          
                            list.Add(proposalLineAp);
                        }
                        else
                        {
                            proposalLineAp = new MPS_ProposalLineTemplate()
                            {
                                BranchCode = percentShared.BranchCode,
                                TR_Amount = percentShared.TR_Amount,
                                TR_Percent = percentShared.TR_Percent,
                                RE_Amount = percentShared.RE_Amount,
                                RE_Percent = percentShared.RE_Percent,
                            };
                            list.Add(proposalLineAp);
                        }
                    }


                }
            }
            return list;
        }

        public IEnumerable<MPS_ProposalLineTemplate> ToModels(IEnumerable<ProposalLineTemplateViewModel> viewModels)
        {
            return viewModels != null ? viewModels.Select(ToModel) : Enumerable.Empty<MPS_ProposalLineTemplate>();
        }
    }
}