﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TheMall.MPS.API.Mappers.Abstracts;
using TheMall.MPS.API.Services;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Models.CashAdvance;
using TheMall.MPS.Models.CashClearing;
using TheMall.MPS.ViewModels;
using TheMall.MPS.ViewModels.CashAdvance;
using TheMall.MPS.ViewModels.CashClearing;
using TheMall.MPS.ViewModels.Master;

namespace TheMall.MPS.API.Mappers
{
    public interface ICashClearingMapper : IWorkflowTableMapper<MPS_CashClearingTable>
    {
        MPS_CashClearingTable ToModel(CashClearingViewModel viewModel);
        CashClearingViewModel ToViewModel(MPS_CashClearingTable model);
        CashClearingViewModel CashClearingFromCashAdvanceViewModel(MPS_CashAdvanceTable model);
    }

    public class CashClearingMapper : BaseWorkflowTableMapper<MPS_CashClearingTable>, ICashClearingMapper
    {
        private readonly ICashClearingService _mpsWorkflowTableService;
        private readonly ICashAdvanceService _cashAdvanceService ;
        private readonly ICashAdvanceMapper _cashAdvanceMapper;
        private readonly IPlaceService _placeService;
        private readonly ICashClearingLineMapper _lineMapper;
        private readonly ICashClearingApprovalMapper _approvalMapper;
        private readonly IUnitCodeService _unitCodeService;
        private readonly IAPcodeService _apCodeService;
        private readonly IProposalService _proposalService;
        private readonly IProposalMapper _proposalMapper;
        private readonly IExpenseTopicService _expenseTopicService;
        private readonly IEmployeeTableService _employeeTableService;
        private readonly ICompanyService _companyService;

        public CashClearingMapper(
            ICashClearingService mpsWorkflowTableService,
            ICashAdvanceService cashAdvanceService, 
            ICashAdvanceMapper cashAdvanceMapper, 
            IPlaceService placeService, 
            ICashClearingLineMapper lineMapper, 
            ICashClearingApprovalMapper approvalMapper, 
            IUnitCodeService unitCodeService, 
            IAPcodeService apCodeService, 
            IProposalService proposalService, 
            IProposalMapper proposalMapper, 
            IExpenseTopicService expenseTopicService, 
            IEmployeeTableService employeeTableService,
            IAccountingService accountingService, 
            ICompanyService companyService)
            : base(mpsWorkflowTableService, employeeTableService, placeService, accountingService)
        {
            _cashAdvanceService = cashAdvanceService;
            _cashAdvanceMapper = cashAdvanceMapper;
            _placeService = placeService;
            _lineMapper = lineMapper;
            _approvalMapper = approvalMapper;
            _unitCodeService = unitCodeService;
            _apCodeService = apCodeService;
            _proposalService = proposalService;
            _proposalMapper = proposalMapper;
            _expenseTopicService = expenseTopicService;
            _employeeTableService = employeeTableService;
            _companyService = companyService;
            _mpsWorkflowTableService = mpsWorkflowTableService;
        }

        public MPS_CashClearingTable ToModel(CashClearingViewModel viewModel)
        {
            if (viewModel == null)
            {
                return null;
            }

            var model = InitWorkflowTable(viewModel);
            model.Budget = viewModel.Budget;
            model.ClearingDate = DateTime.Now;
            model.CashAdvanceID = viewModel.CashAdvanceID;
            model.Actual = viewModel.Actual;
            model.RefAdvanceNumber = viewModel.RefAdvanceNumber;

            if (viewModel.Branch != null)
            {
                model.Branch = viewModel.Branch.PlaceCode;
            }

            if (viewModel.Company != null)
            {
                model.VendorCode = viewModel.Company.VendorCode;
            }

            model.Description = viewModel.Description;
            model.Title = "Cash Clearing";

            return model;
        }

        public CashClearingViewModel ToViewModel(MPS_CashClearingTable model)
        {

            if (model == null)
            {
                return null;
            }

            var viewModel = InitViewModel<CashClearingViewModel>(model);
           // viewModel.Requester = _employeeTableService.FindByUsername(model.RequesterUserName).ToViewModel();
            if (model.CashAdvanceID != null)
            {
                var cashAdvanceViewModel = _cashAdvanceService.Find(m => m.Id == model.CashAdvanceID);
                viewModel.CashAdvanceViewModel = cashAdvanceViewModel != null
                    ? _cashAdvanceMapper.ToViewModel(cashAdvanceViewModel)
                    : new CashAdvanceViewModel();
                if (viewModel.CashAdvanceViewModel != null)
                {
                    viewModel.ProposalRef = viewModel.CashAdvanceViewModel.ProposalRef;
                    viewModel.UnitCode =  viewModel.CashAdvanceViewModel.UnitCode;
                    if (cashAdvanceViewModel != null)
                    {
                        viewModel.RefAdvanceNumber = cashAdvanceViewModel.DocumentNumber;
                    }
                }
                //if (viewModel.CashAdvanceViewModel != null)
                //{
                //    //viewModel.ExpenseTopic = viewModel.CashAdvanceViewModel.ExpenseTopics;
                //    //viewModel.APCode = viewModel.CashAdvanceViewModel.APCode;
                //}
            }

            if (model.CashAdvanceTable.ExpenseTopicCode != null)
            {
                viewModel.ExpenseTopic = _expenseTopicService.Find(m => m.ExpenseTopicCode == model.CashAdvanceTable.ExpenseTopicCode).ToViewModel();
            }

            if (model.CashAdvanceTable.APCode != null && viewModel.ProposalRef != null)
            {
                viewModel.APCode =
                    viewModel.ProposalRef.APCode.Find(
                        m => m.APCode == model.CashAdvanceTable.APCode && m.ProposalLineId == model.CashAdvanceTable.ProposalLineID);
            }

            if (model.Branch != null)
            {
                var branch = _placeService.Find(m => m.PlaceCode.Equals(model.Branch));
                viewModel.Branch = branch.ToViewModel();
            }

            viewModel.CashAdvanceID = model.CashAdvanceID;
            viewModel.Budget = model.Budget;
            viewModel.Actual = model.Actual;
            viewModel.Description = model.Description;
          
            if (model.CashClearingLine.Any())
            {
                viewModel.CashClearingLines = model.CashClearingLine.Select(_lineMapper.ToViewModel).ToList();
            }

            viewModel.CashClearingApproval = _approvalMapper.ToViewModels(model.CashClearingApproval);

            if (model.CashAdvanceTable.RequestForEmpId != null)
            {
                var requestForData = _employeeTableService.Find(m => m.EmpId.Equals(model.CashAdvanceTable.RequestForEmpId));
                viewModel.RequesterFor = requestForData.ToViewModel();
            }

            if (model.VendorCode != null)
            {
                var companyData = _companyService.Find(m => m.VendorCode.Equals(model.VendorCode));
                viewModel.Company = companyData.ToViewModel();
            }
            else
            {
                var vendorCode = _companyService.Find(m => m.VendorCode.Equals(viewModel.Branch.PlaceCode));
                if (vendorCode != null)
                {
                    viewModel.Company = vendorCode.ToViewModel();
                }
                else
                {
                    viewModel.Company = null;
                }
            }

            viewModel.CashAdvanceLinesNetAmount = model.CashAdvanceTable.CashAdvanceLine.Sum(m => m.NetAmount);
            return viewModel;
        }

        //Create CashClearing ครั้งแรกไม่มี Id
        public CashClearingViewModel CashClearingFromCashAdvanceViewModel(MPS_CashAdvanceTable model)
        {
            var viewModel = new CashClearingViewModel();
            if (model.UnitCode != null)
            {
                var unitCodeData = _unitCodeService.Find(m => m.UnitCode.Equals(model.UnitCode));
                viewModel.UnitCode = unitCodeData.ToViewModel();
            }

            if (model.CashAdvanceLine.Any())
            {
                viewModel.CashClearingLines = model.CashAdvanceLine.Select(_lineMapper.ToViewModelFromCashAdvance).ToList(); 
               // model.CashAdvanceLine;
                viewModel.CashAdvanceLinesNetAmount = model.CashAdvanceLine.Sum(m=>m.NetAmount);
                //foreach (var cashAdvanceLine in model.CashAdvanceLine)
                //{
                //    if (cashAdvanceLine.NetAmount != null)
                //        viewModel.CashAdvanceLinesNetAmount = viewModel.CashAdvanceLinesNetAmount + cashAdvanceLine.NetAmount;
                //}
            }

            if (model.ProposalRefID != null)
            {
                var proposalData = _proposalService.Find(m => m.Id == model.ProposalRefID);
                viewModel.ProposalRef = _proposalMapper.ToProposalInfoViewModel(proposalData);
                // viewModel.ProposalRef = proposalData;
            }

            if (model.ExpenseTopicCode != null)
            {
                viewModel.ExpenseTopic = _expenseTopicService.Find(m => m.ExpenseTopicCode == model.ExpenseTopicCode).ToViewModel();
            }

            if (model.APCode != null && viewModel.ProposalRef != null)
            {
                viewModel.APCode =
                    viewModel.ProposalRef.APCode.Find(
                        m => m.APCode == model.APCode && m.ProposalLineId == model.ProposalLineID);
            }

            viewModel.Requester = _employeeTableService.FindByUsername(model.RequesterUserName).ToViewModel();
           
            if (model.Branch != null)
            {
                var branch = _placeService.Find(m => m.PlaceCode.Equals(model.Branch));
                viewModel.Branch = branch.ToViewModel();
            }

            if (model.RequestForEmpId != null)
            {
                var requestForData = _employeeTableService.Find(m => m.EmpId.Equals(model.RequestForEmpId));
                viewModel.RequesterFor = requestForData.ToViewModel();
            }

            //viewModel.CashClearingApproval = _approvalMapper.ToViewModels(null);
            viewModel.RefAdvanceNumber = model.DocumentNumber;
            viewModel.Description = model.Description;
            viewModel.Budget = model.Budget;
           
            //var listAccounting = new List<EmployeeViewModel>();
            //if (!string.IsNullOrEmpty(model.AccountingUsername))
            //{
            //   // var lastIndex = model.AccountingUsername.LastIndexOf(';');
            //    string[] accounting = model.AccountingUsername.Trim(';').Split(';');
            //    listAccounting.AddRange(accounting.Select(acc => _employeeTableService.FindByUsername(acc).ToViewModel()));
            //}
            //viewModel.Accounting = (listAccounting.Any()) ? listAccounting : null;

            viewModel.CashAdvanceID = model.Id;
            if (model.CashAdvanceApproval != null)
            {
                viewModel.CashClearingApproval = _approvalMapper.CashAdvanceToViewModels(model.CashAdvanceApproval);
            }

            if (model.AccountingBranch != null)
            {
               var accountingBranch = _placeService.FirstOrDefault(m => m.PlaceCode.Equals(model.AccountingBranch)).ToViewModel();
               viewModel.Accounting = accountingBranch;
            }

            if (model.VendorCode != null)
            {
                var companyData = _companyService.Find(m => m.VendorCode.Equals(model.VendorCode));
                viewModel.Company = companyData.ToViewModel();
            }

            if (model.VendorCode != null)
            {
                var companyData = _companyService.Find(m => m.VendorCode.Equals(model.VendorCode));
                viewModel.Company = companyData.ToViewModel();
            }
            else
            {
                var vendorCode = _companyService.Find(m => m.VendorCode.Equals(viewModel.Branch.PlaceCode));
                if (vendorCode != null)
                {
                    viewModel.Company = vendorCode.ToViewModel();
                }
                else
                {
                    viewModel.Company = null;
                }
            }

            return viewModel;
        }
    }
}