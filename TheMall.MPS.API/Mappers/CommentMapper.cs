﻿using System;
using System.Collections.Generic;
using System.Linq;
using CrossingSoft.Framework.Models.Interfaces;
using TheMall.MPS.ViewModels;


namespace TheMall.MPS.API.Mappers
{
    public static class CommentMapper
    {
        public static CommentViewModel ToViewModel(this ICommentTable commentTable)
        {
            if (commentTable == null)
                return null;
            //Resources.Action.ResourceManager.IgnoreCase = true;
            return new CommentViewModel()
            {
                Id = commentTable.Id,
                CommentedBy = string.IsNullOrEmpty(commentTable.CreatedByName) ? commentTable.CreatedBy : commentTable.CreatedByName,
                CommentedDate = commentTable.CreatedDate == null ? DateTime.MinValue : commentTable.CreatedDate.Value,
                Action = Resources.Helper.TranslateAction(commentTable.Action),
                Activity = commentTable.Activity,
                Comment = commentTable.Comment,
                SN = commentTable.SerialNumber
            };
        }

        public static IEnumerable<CommentViewModel> ToViewModel(
            this IEnumerable<ICommentTable> commentTables)
        {
            if (commentTables == null)
                return Enumerable.Empty<CommentViewModel>();

            return commentTables.Select(commentTable => commentTable.ToViewModel()).ToListSafe();
        }
        public static Dictionary<string, IEnumerable<AttachmentViewModel>> ToCommentViewModel(
            this IEnumerable<TheMall.MPS.Models.Interfaces.IAttachmentTable> attachmentTables, System.Web.Http.Routing.UrlHelper urlHelper)
        {
            var dictionary = new Dictionary<string, IEnumerable<AttachmentViewModel>>();
            if (attachmentTables == null)
            {
                return dictionary;
            }

            var attachments = attachmentTables.Where(m => !string.IsNullOrEmpty(m.FileName)).Select(attachmentTable => attachmentTable.ToViewModel(urlHelper)).ToListSafe();

            var keys = attachments.GroupBy(m => m.serialNumber).Select(m => m.Key);

            foreach (var key in keys)
            {
                dictionary.Add(key.ToString(), attachments.Where(m => m.serialNumber == key).ToArray());
            }
            return dictionary;
        }
       
    }
}