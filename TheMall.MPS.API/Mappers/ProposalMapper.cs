﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Data.OData.Query.SemanticAst;
using TheMall.MPS.API.Mappers.Abstracts;
using TheMall.MPS.API.Services;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Models.Enums;
using TheMall.MPS.Models.Proposal;
using TheMall.MPS.ViewModels;
using TheMall.MPS.ViewModels.Master;
using TheMall.MPS.ViewModels.Proposal;
using TheMall.MPS.ViewModels.ProposalLog;
using TheMall.MPS.Models.Master;

namespace TheMall.MPS.API.Mappers
{
    public interface IProposalMapper : IWorkflowTableMapper<MPS_ProposalTable>
    {
        MPS_ProposalTable ToModel(ProposalViewModel viewModel);
        ProposalViewModel ToViewModel(MPS_ProposalTable model);
        ProposalInfoViewModel ToProposalInfoViewModel(MPS_ProposalTable model);
        DocumentNumberViewModel ToDocNumberViewModel(MPS_ProposalTable model);
        ProposalInfoViewModel ToViewModelWithOneLine(MPS_ProposalTable model, int id);
        DepositNumberViewModel ToDepositNumberViewModel(MPS_ProposalTable model);
        MPS_ProposalTable DeleteBy(MPS_ProposalTable model, EmployeeViewModel employeeViewModel);
        ProposalViewModel ToViewModelForReport(MPS_ProposalTable model);
        ProposalRefViewModel ToProposalRefViewModel(MPS_ProposalTable model);
    }
    public class ProposalMapper : BaseWorkflowTableMapper<MPS_ProposalTable>, IProposalMapper
    {
        private readonly IProposalService _mpsWorkflowTableService;
        private readonly IPlaceService _placeService;
        private readonly ISharedBranchService _sharedBranchService;
        private readonly IProposalDepositLineMapper _depositlineMapper;
        private readonly IProposalIncomeOtherMapper _incomeOtherMapper;
        private readonly IProposalIncomeDepositMapper _incomeDepositMapper;
        private readonly IProposalIncomeTotalSaleMapper _incomeTotalSaleMapper;
        private readonly IProposalApprovalMapper _approvalMapper;
        private readonly IProposalApprovalService _approvalService;
        private readonly IProposalLineMapper _lineMapper;
        private readonly IProposalLineSharedTemplateMapper _sharedTemplateMapper;
        private readonly IProposalEstimateTotalSaleMapper _estimateTotalSaleMapper;
        private readonly IEmployeeTableService _employeeTableService;
        private readonly IUnitCodeForEmployeeService _unitcodeforempService;
        private readonly IUnitCodeService _unitCode;
        private readonly ITypeProposalService _typeProposalService;
        private readonly IBudgetProposalTransService _budgetProposalTransService;
        private readonly IBudgetDepositTransService _budgetDepositTransService;
        private readonly ILogProposalTrackChangesService _logProposalTrackChangesService;
        private readonly ILogProposalTrackChangesMapper _logProposalTrackChangesMapper;
        private readonly IAccountingService _accountingService;
        private readonly ILogProposalService _logProposalService;
        private readonly IAllocationBasisService _allocationBasisService;
        private readonly IThemeService _themeService;
        private readonly ICompanyService _companyService;

        public ProposalMapper(IProposalService mpsWorkflowTableService, 
            IPlaceService placeService,
            IProposalDepositLineMapper depositlineMapper,
            IProposalIncomeOtherMapper incomeOtherMapper,
            IProposalIncomeDepositMapper incomeDepositMapper,
            IProposalApprovalMapper approvalMapper,
            IProposalLineMapper lineMapper, 
            IProposalLineSharedTemplateMapper sharedTemplateMapper, 
            ISharedBranchService sharedBranchService, 
            IProposalIncomeTotalSaleMapper incomeTotalSaleMapper, 
            IProposalEstimateTotalSaleMapper estimateTotalSaleMapper,
            IEmployeeTableService employeeTableService, 
            IUnitCodeForEmployeeService unitcodeforempService,
            IUnitCodeService unitCode,
            IAccountingService accountingService,
            IBudgetProposalTransService budgetProposalTransService, 
            ITypeProposalService typeProposalService, 
            IBudgetDepositTransService budgetDepositTransService, 
            ILogProposalTrackChangesService logProposalTrackChangesService,
            ILogProposalTrackChangesMapper logProposalTrackChangesMapper,
            IProposalApprovalService approvalService, 
            ILogProposalService logProposalService, 
            IAllocationBasisService allocationBasisService,
            IThemeService themeService,
            ICompanyService companyService)
            : base(mpsWorkflowTableService, employeeTableService, placeService, accountingService)
        {
            _mpsWorkflowTableService = mpsWorkflowTableService;
            _placeService = placeService;
            _depositlineMapper = depositlineMapper;
            _incomeOtherMapper = incomeOtherMapper;
            _incomeDepositMapper = incomeDepositMapper;
            _approvalMapper = approvalMapper;
            _lineMapper = lineMapper;
            _sharedTemplateMapper = sharedTemplateMapper;
            _sharedBranchService = sharedBranchService;
            _incomeTotalSaleMapper = incomeTotalSaleMapper;
            _estimateTotalSaleMapper = estimateTotalSaleMapper;
            _employeeTableService = employeeTableService;
            _unitcodeforempService = unitcodeforempService;
            _unitCode = unitCode;
            _budgetProposalTransService = budgetProposalTransService;
            _typeProposalService = typeProposalService;
            _budgetDepositTransService = budgetDepositTransService;
            _logProposalTrackChangesService = logProposalTrackChangesService;
            _logProposalTrackChangesMapper = logProposalTrackChangesMapper;
            _accountingService = accountingService;
            _approvalService = approvalService;
            _logProposalService = logProposalService;
            _allocationBasisService = allocationBasisService;
            _themeService = themeService;
            _companyService = companyService;
        }

        public MPS_ProposalTable ToModel(ProposalViewModel viewModel)
        {
            if (viewModel == null)
            {
                return null;
            }

            var model = InitWorkflowTable(viewModel);

            // model.DepartmentException = viewModel.DepartmentException;
            model.CheckBudget = viewModel.CheckBudget;
            model.CustomersTargetGroup = viewModel.CustomersTargetGroup;
            model.EndDate = viewModel.EndDate.ToLocalTime().Date;
            model.StartDate = viewModel.StartDate.ToLocalTime().Date;

            if (viewModel.ProjectStartDate != null)
                model.ProjectStartDate = viewModel.ProjectStartDate.Value.ToLocalTime().Date;

            if (viewModel.ProjectEndDate != null)
            {
                model.ProjectEndDate = viewModel.ProjectEndDate.Value.ToLocalTime().Date;
            }
            else
            {
                model.ProjectEndDate = null;
            }

            model.Other = viewModel.Other;
            model.TR = viewModel.TR;
            model.RE = viewModel.RE;
            model.TransferToNextYear = viewModel.TransferToNextYear;
            
            if (viewModel.ProposalType != null)
            {
                model.ProposalTypeCode = viewModel.ProposalType.TypeProposalCode;
                model.ProposalTypeName = viewModel.ProposalType.TypeProposalName;
            }

            if (viewModel.ListPlace != null)
            {
                model.Place = string.Empty;
                foreach (var p in viewModel.ListPlace)
                {
                    model.Place += p.PlaceCode + ",";
                }
            }

            if (viewModel.DepartmentException != null)
            {
                model.DepartmentException = string.Empty;
                foreach (var p in viewModel.DepartmentException)
                {
                    model.DepartmentException += p.BranchCode + ",";
                }
            }

            model.Source = viewModel.Source;

            if (viewModel.DepositNumber != null)
            {
                model.DepositId = viewModel.DepositNumber.Id;
                model.DepositNumber = viewModel.DepositNumber.DocumentNumber;
            }
           
            model.Objective = viewModel.Objective;

            if (viewModel.DepositNumber != null)
            {
                model.DepositId = viewModel.DepositNumber.Id;
                model.DepositNumber = viewModel.DepositNumber.DocumentNumber;
            }
          
            model.TotalBudget = viewModel.TotalBudget;
            model.CloseFlag = viewModel.CloseFlag;
            model.TotalIncomeDeposit = viewModel.TotalIncomeDeposit;
            model.TotalIncomeOther = viewModel.TotalIncomeOther;
            model.TotalMarketingExpense = viewModel.TotalMarketingExpense;
            model.TotalSaleTarget = viewModel.TotalSaleTarget;
            model.TotalAdvertising = viewModel.TotalAdvertising;
            model.TotalPromotion = viewModel.TotalPromotion;
            model.PecentAdvertising = viewModel.PecentAdvertising;
            model.PecentPromotion = viewModel.PecentPromotion;
            model.PecentMarketingExpense = viewModel.PecentMarketingExpense;

            var tempApprove = string.Empty;
            if (viewModel.Approver != null)
            {
                foreach (var app in viewModel.Approver.OrderBy(m => m.ApproverKey).ThenBy(m => m.ApproverSequence))
                {
                    tempApprove += string.Concat(app.ApproverUserName, ";").Trim();
                }
            }

            model.DepositApprover = (!string.IsNullOrEmpty(tempApprove)) ? tempApprove.TrimEnd(';') : null;
            model.PercentOtherExpense = viewModel.PercentOtherExpense;
            model.TotalOtherExpense = viewModel.TotalOtherExpense;
            model.GrandTotal = viewModel.GrandTotal;
            model.PercentGrandTotal = viewModel.PercentGrandTotal;

            if (viewModel.Theme != null)
            {
                if (viewModel.Theme.ThemesCode != null)
                {
                    model.ThemesCode = viewModel.Theme.ThemesCode;
                }
            }

            return model;
        }
        public ProposalViewModel ToViewModel(MPS_ProposalTable model)
        {
            if (model == null)
            {
                return null;
            }

            var viewModel = InitViewModel<ProposalViewModel>(model);

            //viewModel.Requester = _employeeTableService.FindByUsername(model.RequesterUserName).ToViewModel();
            // viewModel.Requester.UnitCode = unitCode.ToViewModel();
            if (model.TypeProposal != null)
            {
                viewModel.ProposalType = model.TypeProposal.ToViewModel();
            }
            else
            {
                if (!string.IsNullOrEmpty(model.ProposalTypeCode))
                {
                    viewModel.ProposalType =
                        _typeProposalService.Find(m => m.TypeProposalCode == model.ProposalTypeCode).ToViewModel();
                }
            }
                     
            viewModel.Budget = model.Budget;
            viewModel.CheckBudget = model.CheckBudget;
            viewModel.CustomersTargetGroup = model.CustomersTargetGroup;
            viewModel.Objective = model.Objective;
            viewModel.EndDate = model.EndDate;
            viewModel.StartDate = model.StartDate;

            if (model.ProjectStartDate != null)
            {
                viewModel.ProjectStartDate = model.ProjectStartDate.Value.ToUniversalTime();
            }


            if (model.ProjectEndDate != null)
            {
                viewModel.ProjectEndDate = model.ProjectEndDate.Value.ToUniversalTime();
            }

            viewModel.TR = model.TR;
            viewModel.RE = model.RE;
            viewModel.TransferToNextYear = model.TransferToNextYear;
            //viewModel.UnitCode = (model.Unitcode != null) ? model.Unitcode.ToViewModel() : new UnitCodeViewModel();
            viewModel.Place = model.Place;

            var listPlace = new List<PlaceViewModel>();
            if (!string.IsNullOrEmpty(model.Place))
            {
                string[] placeid = model.Place.Split(',');
                listPlace.AddRange(from p in placeid where !string.IsNullOrEmpty(p) select _placeService.Find(m => m.PlaceCode == p).ToViewModel());
            }

            viewModel.ListPlace = listPlace;

            var listDepartmentException = new List<SharedBranchViewModel>();
            if (!string.IsNullOrEmpty(model.DepartmentException))
            {
                string[] branchcode = model.DepartmentException.Split(',');
                listDepartmentException.AddRange(from p in branchcode where !string.IsNullOrEmpty(p) select _sharedBranchService.Find(m => m.BranchCode == p).ToViewModel());
            }

            viewModel.DepartmentException = listDepartmentException;
            viewModel.Source = model.Source;

            if (model.DepositId != 0)
            {
                var propDep = _mpsWorkflowTableService.LastOrDefault(m => m.Id == model.DepositId);
                viewModel.DepositNumber = ToDocNumberViewModel(propDep);
            }

            // viewModel.RequesterName = model.RequesterName;
            viewModel.Other = model.Other;
            viewModel.Comments = model.Comments.ToViewModel();

            viewModel.IncomeDeposit = model.IncomeDeposit != null
                ? _incomeDepositMapper.ToViewModels(model.IncomeDeposit)
                : new List<IncomeDepositViewModel>();

            viewModel.IncomeOther = model.IncomeOther != null
                ? _incomeOtherMapper.ToViewModels(model.IncomeOther)
                : new List<IncomeOtherViewModel>();

            viewModel.IncomeTotalSale = model.IncomeTotalSale != null
                ? _incomeTotalSaleMapper.ToViewModels(model.IncomeTotalSale)
                : new List<IncomeTotalSaleViewModel>();

            viewModel.ProposalApproval = model.ProposalApproval != null
                ? _approvalMapper.ToViewModels(model.ProposalApproval)
                : new List<ProposalApprovalViewModel>();

            viewModel.EstimateTotalSale = model.EstimateTotalSale != null
                ? _estimateTotalSaleMapper.ToViewModels(model.EstimateTotalSale)
                : new List<EstimateTotalSaleViewModel>();

            viewModel.ProposalLine = _lineMapper.ToViewModels(model.ProposalLine);

            viewModel.DepositLines = model.DepositLine != null
                ? _depositlineMapper.ToViewModels(model.DepositLine)
                : new List<DepositLineViewModel>();

            viewModel.SharedBranchTemplateLines = model.TemplateLines != null
                ? _sharedTemplateMapper.ToViewModels(model.TemplateLines)
                : new List<ProposalLineSharedTemplateViewModel>();
            ;

            viewModel.UnitCodeForEmployee = _unitcodeforempService.GetUnitCodeCurrentUser().ToViewModels();
            viewModel.CheckCreateDocStatus = (model.StatusFlag == Models.Enums.DocumentStatusFlagId.Completed
                                              && model.CloseFlag == false
                                              && model.ProposalTypeCode != "DEPOSIT"
                                              && model.ProposalTypeCode != "DEPOSITIN");
            viewModel.TotalBudget = model.TotalBudget;
            viewModel.CloseFlag = model.CloseFlag;

            var maxLogId = _logProposalTrackChangesService.LastOrDefault(m => m.ProposalId == model.Id);
            if (maxLogId != null)
            {
                var logTrackChanges =
                    _logProposalTrackChangesService
                        .GetWhere(m => m.ProposalId == model.Id && m.ProposalLogId == maxLogId.ProposalLogId)
                        .ToListSafe();
                if (logTrackChanges.Any())
                {
                    viewModel.LogProposalTrackChanges = _logProposalTrackChangesMapper.ToViewModels(logTrackChanges);
                }
                else
                {
                    viewModel.LogProposalTrackChanges = new List<LogProposalTrackChangesViewModel>();
                }

            }
            else
            {
                viewModel.LogProposalTrackChanges = new List<LogProposalTrackChangesViewModel>();
            }

            var listApprover = new List<ProposalApprovalViewModel>();
            if (!string.IsNullOrEmpty(model.DepositApprover) && model.DepositId!=0)
            {
               
                string[] approver = model.DepositApprover.TrimEnd(';').Split(';');
                listApprover.AddRange(approver
                    .Select(app =>
                        _approvalService.LastOrDefault(m =>
                            m.ParentId == model.DepositId && m.ApproverUserName.Equals(app)))
                    .Select(approval => _approvalMapper.ToViewModel(approval)));
            }

            viewModel.Approver = (listApprover.Any()) ? listApprover : null;
            viewModel.AllocationCode = !viewModel.ProposalType.IsTypeDeposit()
                ? _allocationBasisService.FindByProposalSharedTemplate(model.TemplateLines) != null
                    ?
                    _allocationBasisService.FindByProposalSharedTemplate(model.TemplateLines).AllocCode
                    : null
                : null;

            if (!string.IsNullOrEmpty(model.ThemesCode))
            {
                var theme = _themeService.Load(m => m.ThemesCode == model.ThemesCode);
                viewModel.Theme = theme.ToViewModel();
            }
            else
            {
                viewModel.Theme = new ThemeViewModel();
                viewModel.Theme.ThemesCode = "General";
                viewModel.Theme.ThemesName = "General";
            }

            return viewModel;
        }

        public ProposalInfoViewModel ToViewModelWithOneLine(MPS_ProposalTable model, int id)
        {
            if (model == null)
            {
                return null;
            }
               
            var viewModel = new ProposalInfoViewModel()
            {
                Id = model.Id,
                DocumentNumber = model.DocumentNumber,
                Title = model.Title,
            };

            viewModel.ProposalType = !string.IsNullOrEmpty(model.ProposalTypeCode)
                ? model.TypeProposal.ToViewModel()
                : new TypeProposalViewModel();
               
            var listPlace = new List<PlaceViewModel>();
            if (!string.IsNullOrEmpty(model.Place))
            {
                string[] placeid = model.Place.Split(',');
                listPlace.AddRange(from p in placeid
                    where !string.IsNullOrEmpty(p)
                    select _placeService.Find(m => m.PlaceCode == p).ToViewModel());
            }
            viewModel.ListPlace = listPlace;

            if (model.DepositId != 0)
            {
                var propDep = _mpsWorkflowTableService.LastOrDefault(m => m.Id == model.DepositId);
                viewModel.DepositNumber = ToDocNumberViewModel(propDep);

            }

            viewModel.Source = model.Source;
            viewModel.Other = model.Other;
            viewModel.EndDate = model.EndDate.ToUniversalTime();
            viewModel.StartDate = model.StartDate.ToUniversalTime();
            viewModel.Budget = model.Budget;

            if (model.ProjectStartDate != null)
            {
                viewModel.ProjectStartDate = model.ProjectStartDate.Value.ToUniversalTime();
            }

            if (model.ProjectEndDate != null)
            {
                viewModel.ProjectEndDate = model.ProjectEndDate.Value.ToUniversalTime();
            }

            viewModel.AccountingUsername = model.AccountingUsername;

            if (model.ProposalLine != null)
            {
                viewModel.ProposalLine = _lineMapper.ToViewModelOnlyOne(model.ProposalLine, id);
            }

            viewModel.CloseFlag = model.CloseFlag;

            return viewModel;
        }

        public ProposalInfoViewModel ToProposalInfoViewModel(MPS_ProposalTable model)
        {
            if (model == null)
            {
                return null;
            }
                
            var viewModel = new ProposalInfoViewModel()
            {
                Id = model.Id,
                DocumentNumber = model.DocumentNumber,
                Title = model.Title,
            };

            viewModel.ProposalType = (!string.IsNullOrEmpty(model.ProposalTypeCode))
                ? model.TypeProposal.ToViewModel()
                : new TypeProposalViewModel();
                                        
            var listPlace = new List<PlaceViewModel>();
            if (!string.IsNullOrEmpty(model.Place))
            {
                string[] placeid = model.Place.Split(',');
                listPlace.AddRange(from p in placeid where !string.IsNullOrEmpty(p)
                    select _placeService.Find(m => m.PlaceCode == p).ToViewModel());
            }
            viewModel.ListPlace = listPlace;
            if (!viewModel.ListPlace.Any(m => m.PlaceCode.Equals("M02")))
            {
                var m02 = _placeService.Find(m => m.PlaceCode == "M02").ToViewModel();
                viewModel.ListPlace.Add(m02);
            }

            var companyModel = new List<CompanyViewModel>();
            foreach (var p in viewModel.ListPlace)
            {
                var companyByPlact = _companyService.GetWhere(m => m.BranchCode == p.PlaceCode);
                var viewCompany = companyByPlact.ToViewModels();
                companyModel.AddRange(viewCompany);
            }
            viewModel.Company = companyModel;

            //if (!string.IsNullOrEmpty(model.Place))
            //{
            //    var placeid = new List<string>();
            //    placeid = model.Place.Split(',').ToListSafe();
            //    placeid.Add("M02");

            //    var companyAll = _companyService.GetAll();
            //    if (companyAll != null)
            //    {
            //        foreach (var listCompany in companyAll)
            //        {
            //            foreach (var p in placeid.Distinct())
            //            {
            //                if (listCompany.BranchCode == p)
            //                {
            //                    CompanyModel.Add(listCompany);
            //                }
            //            }
            //        }
            //    }  
            //}
            //viewModel.Company = companyModel;

            if (model.DepositId != 0)
            {
                var propDep = _mpsWorkflowTableService.LastOrDefault(m => m.Id == model.DepositId);
                viewModel.DepositNumber = ToDocNumberViewModel(propDep);
            }

            viewModel.Source = model.Source;
            viewModel.Other = model.Other;
            viewModel.EndDate = model.EndDate.ToUniversalTime();
            viewModel.StartDate = model.StartDate.ToUniversalTime();
            viewModel.Budget = model.Budget;

            if (model.ProjectStartDate != null)
            {
                viewModel.ProjectStartDate = model.ProjectStartDate.Value.ToUniversalTime();
            }

            if (model.ProjectEndDate != null)
            {
                viewModel.ProjectEndDate = model.ProjectEndDate.Value.ToUniversalTime();
            }

            viewModel.AccountingUsername = model.AccountingUsername;

            if (model.ProposalLine != null)
            {
                viewModel.ProposalLine =  _lineMapper.ToViewModels(model.ProposalLine,true);
            }
           
            viewModel.APCode = viewModel.ProposalLine.ToProposalViewModel().ToList();
            viewModel.StatusFlag = model.StatusFlag;
            viewModel.CloseFlag = model.CloseFlag;

            if (model.Unitcode != null)
            {
                viewModel.ProposalUnitCode = model.Unitcode.ToViewModel();
            }

            if (model.IncomeDeposit != null)
            {
                var incomeDeposit = model.IncomeDeposit.Where(m => m.ActualCharge).ToListSafe();
                var listIncomeDeposit = incomeDeposit.GroupBy(m => m.ProposalDepositRefDoc)
                    .Select(g => g.Last(m => m.RefMemoId == null)).ToListSafe();
                foreach (var dep in listIncomeDeposit)
                {
                    dep.Budget = incomeDeposit.Where(m => m.ProposalDepositRefID == dep.ProposalDepositRefID).Sum(m => m.Budget);
                }
                viewModel.IncomeDepositLine = _incomeDepositMapper.ToViewModels(listIncomeDeposit).ToList();
            }

            return viewModel;
        }

        public DocumentNumberViewModel ToDocNumberViewModel(MPS_ProposalTable model)
        {
            if (model == null)
                return null;

            var budgetTrans = _budgetDepositTransService.GetBudgetDepositTransTotal(model.Id);
            var balance =  model.TotalBudget - budgetTrans;
            var viewModel = new DocumentNumberViewModel
            {
                Id = model.Id,
                DocumentNumber = model.DocumentNumber,
                Title = model.Title,
                Type = model.ProposalTypeName,
                Approver =
                    _approvalMapper.ToViewModels(
                        model.ProposalApproval.GroupBy(m => m.ApproverUserName).Select(g => g.Last())).ToList(),
                Balance = balance
            };

            return viewModel;
        }

        public DepositNumberViewModel ToDepositNumberViewModel(MPS_ProposalTable model)
        {
            if (model == null)
                return null;

            //var approve = model.ProposalApproval.GroupBy(m => m.ApproverUserName).SelectMany(m => model.ProposalApproval);
            var budgetTrans = _budgetDepositTransService.GetBudgetDepositTransTotal(model.Id);
            var balance = model.TotalBudget - budgetTrans;
            var viewModel = new DepositNumberViewModel
            {
                Id = model.Id,
                DocumentNumber = model.DocumentNumber,
                Title = model.Title,
                Type = model.ProposalTypeName,
                Approver =
                    _approvalMapper.ToViewModels(
                        model.ProposalApproval.GroupBy(m => m.ApproverUserName).Select(g => g.Last())).ToList(),
                Balance = balance
            };

            return viewModel;
        }

        public MPS_ProposalTable DeleteBy(MPS_ProposalTable model, EmployeeViewModel employeeViewModel)
        {
            if (model != null)
            {
                if (model.Id > 0)
                {
                    model.Deleted = true;
                    model.DeletedDate = DateTime.Now;
                    model.DeletedBy = employeeViewModel.Username;
                    model.DeletedByName = employeeViewModel.FullName;
                    return model;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        public ProposalViewModel ToViewModelForReport(MPS_ProposalTable model)
        {
            if (model == null)
            {
                return null;
            }

            var viewModel = InitViewModel<ProposalViewModel>(model);
            //viewModel.Requester = _employeeTableService.FindByUsername(model.RequesterUserName).ToViewModel();
            // viewModel.Requester.UnitCode = unitCode.ToViewModel();

            if (model.TypeProposal != null)
            {
                viewModel.ProposalType = model.TypeProposal.ToViewModel();
            }
            else
            {
                if (!string.IsNullOrEmpty(model.ProposalTypeCode))
                {
                    viewModel.ProposalType =
                        _typeProposalService.Find(m => m.TypeProposalCode == model.ProposalTypeCode).ToViewModel();
                }
            }

            viewModel.Budget = model.Budget;
            viewModel.CheckBudget = model.CheckBudget;
            viewModel.CustomersTargetGroup = model.CustomersTargetGroup;
            viewModel.Objective = model.Objective;
            viewModel.EndDate = model.EndDate;
            viewModel.StartDate = model.StartDate;

            if (model.ProjectStartDate != null)
                viewModel.ProjectStartDate = model.ProjectStartDate.Value.ToUniversalTime();
            if (model.ProjectEndDate != null) viewModel.ProjectEndDate = model.ProjectEndDate.Value.ToUniversalTime();

            viewModel.TR = model.TR;
            viewModel.RE = model.RE;
            viewModel.TransferToNextYear = model.TransferToNextYear;
            //viewModel.UnitCode = (model.Unitcode != null) ? model.Unitcode.ToViewModel() : new UnitCodeViewModel();
            viewModel.Place = model.Place;

            var listPlace = new List<PlaceViewModel>();
            if (!string.IsNullOrEmpty(model.Place))
            {
                string[] placeid = model.Place.Split(',');
                listPlace.AddRange(from p in placeid where !string.IsNullOrEmpty(p) select _placeService.Find(m => m.PlaceCode == p).ToViewModel());
            }
            viewModel.ListPlace = listPlace;

            var listDepartmentException = new List<SharedBranchViewModel>();
            if (!string.IsNullOrEmpty(model.DepartmentException))
            {
                string[] branchcode = model.DepartmentException.Split(',');
                listDepartmentException.AddRange(from p in branchcode where !string.IsNullOrEmpty(p) select _sharedBranchService.Find(m => m.BranchCode == p).ToViewModel());
            }
            viewModel.DepartmentException = listDepartmentException;

            viewModel.Source = model.Source;

            if (model.DepositId != 0)
            {
                var propDep = _mpsWorkflowTableService.LastOrDefault(m => m.Id == model.DepositId);
                viewModel.DepositNumber = ToDocNumberViewModel(propDep);
            }

            // viewModel.RequesterName = model.RequesterName;
            viewModel.Other = model.Other;
            viewModel.Comments = model.Comments.ToViewModel();

            viewModel.IncomeDeposit = model.IncomeDeposit != null
                ? _incomeDepositMapper.ToViewModels(model.IncomeDeposit)
                : new List<IncomeDepositViewModel>();

            viewModel.IncomeOther = model.IncomeOther != null
                ? _incomeOtherMapper.ToViewModels(model.IncomeOther)
                : new List<IncomeOtherViewModel>();

            viewModel.IncomeTotalSale = model.IncomeTotalSale != null
                ? _incomeTotalSaleMapper.ToViewModels(model.IncomeTotalSale)
                : new List<IncomeTotalSaleViewModel>();

            viewModel.ProposalApproval = model.ProposalApproval != null
                ? _approvalMapper.ToViewModels(model.ProposalApproval)
                : new List<ProposalApprovalViewModel>();

            viewModel.EstimateTotalSale = model.EstimateTotalSale != null
                ? _estimateTotalSaleMapper.ToViewModels(model.EstimateTotalSale)
                : new List<EstimateTotalSaleViewModel>();

            viewModel.ProposalLine = _lineMapper.ToViewModels(model.ProposalLine);

            viewModel.DepositLines = model.DepositLine != null
                ? _depositlineMapper.ToViewModels(model.DepositLine)
                : new List<DepositLineViewModel>();

            viewModel.SharedBranchTemplateLines = model.TemplateLines != null
                ? _sharedTemplateMapper.ToViewModels(model.TemplateLines)
                : new List<ProposalLineSharedTemplateViewModel>();
            ;
            viewModel.TotalSaleTarget = model.TotalSaleTarget;
            viewModel.UnitCodeForEmployee = _unitcodeforempService.GetUnitCodeCurrentUser().ToViewModels();
            viewModel.CheckCreateDocStatus = model.StatusFlag == DocumentStatusFlagId.Completed
                                             && model.CloseFlag == false
                                             && model.ProposalTypeCode != "DEPOSIT"
                                             && model.ProposalTypeCode != "DEPOSITIN";
            viewModel.TotalBudget = model.TotalBudget;
            viewModel.CloseFlag = model.CloseFlag;

            var maxLogId = _logProposalTrackChangesService.LastOrDefault(m => m.ProposalId == model.Id);
            if (maxLogId != null)
            {
                var logTrackChanges =
                    _logProposalTrackChangesService
                        .GetWhere(m => m.ProposalId == model.Id && m.ProposalLogId == maxLogId.ProposalLogId)
                        .ToListSafe();
                if (logTrackChanges.Any())
                {
                    viewModel.LogProposalTrackChanges = _logProposalTrackChangesMapper.ToViewModels(logTrackChanges);
                }
                else
                {
                    viewModel.LogProposalTrackChanges = new List<LogProposalTrackChangesViewModel>();
                }
            }
            else
            {
                viewModel.LogProposalTrackChanges = new List<LogProposalTrackChangesViewModel>();
            }

            var listApprover = new List<ProposalApprovalViewModel>();
            if (!string.IsNullOrEmpty(model.DepositApprover) && model.DepositId != 0)
            {

                string[] approver = model.DepositApprover.TrimEnd(';').Split(';');
                listApprover.AddRange(approver
                    .Select(app =>
                        _approvalService.LastOrDefault(m =>
                            m.ParentId == model.DepositId && m.ApproverUserName.Equals(app)))
                    .Select(approval => _approvalMapper.ToViewModel(approval)));
            }
            viewModel.Approver = (listApprover.Any()) ? listApprover : null;

            viewModel.TotalSaleTargetLastYear = model.IncomeTotalSale != null
                ? model.IncomeTotalSale.Where(m => !m.Deleted).Sum(m => m.ActualLastYear)
                : null;

            return viewModel;
        }

        public ProposalRefViewModel ToProposalRefViewModel(MPS_ProposalTable model)
        {
            if (model == null)
                return null;

            var viewModel = new ProposalRefViewModel()
            {
                Id = model.Id,
                DocumentNumber = model.DocumentNumber,
                Title = model.Title,
            };

            viewModel.ProposalType = !string.IsNullOrEmpty(model.ProposalTypeCode)
                ? model.TypeProposal.ToViewModel()
                : new TypeProposalViewModel();

            var listPlace = new List<PlaceViewModel>();
            if (!string.IsNullOrEmpty(model.Place))
            {
                string[] placeid = model.Place.Split(',');
                listPlace.AddRange(from p in placeid
                    where !string.IsNullOrEmpty(p)
                    select _placeService.Find(m => m.PlaceCode == p).ToViewModel());
            }
            viewModel.ListPlace = listPlace;

            viewModel.Source = model.Source;
            viewModel.Other = model.Other;
            viewModel.EndDate = model.EndDate.ToUniversalTime();
            viewModel.StartDate = model.StartDate.ToUniversalTime();
            viewModel.Budget = model.Budget;

            if (model.ProjectStartDate != null)
                viewModel.ProjectStartDate = model.ProjectStartDate.Value.ToUniversalTime();
            if (model.ProjectEndDate != null) viewModel.ProjectEndDate = model.ProjectEndDate.Value.ToUniversalTime();

            viewModel.AccountingUsername = model.AccountingUsername;
            viewModel.StatusFlag = model.StatusFlag;
            viewModel.CloseFlag = model.CloseFlag;

            if (model.Unitcode != null)
            {
                viewModel.ProposalUnitCode = model.Unitcode.ToViewModel();
            }

            // กรอง memoincome line
            if (model.IncomeOther != null)
            {
                var incomeModel = model.IncomeOther.Where(m => !m.Deleted && !m.PostActual);
                viewModel.IncomeOther = (incomeModel!=null)? _incomeOtherMapper.ToViewModels(incomeModel).ToList():new List<IncomeOtherViewModel>();
            }

            // กรอง memoincome line
            if (model.DepositLine != null)
            {
                var deplineModel = model.DepositLine.Where(m => !m.Deleted && m.ProcessStatus != "completed");
                viewModel.DepositLine = (deplineModel != null) ? _depositlineMapper.ToViewModels(deplineModel).ToList():new List<DepositLineViewModel>();
            }

            return viewModel;
        }
    }
}