﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TheMall.MPS.API.Services;
using TheMall.MPS.Models.Master;
using TheMall.MPS.ViewModels.Master;

namespace TheMall.MPS.API.Mappers
{
    public interface IAccountingMapper
    {
        MPS_M_Accounting ToModel(AccountingViewModel viewModel);
        AccountingViewModel ToViewModel(MPS_M_Accounting model);
    }

    public class AccountingMapper : IAccountingMapper
    {
        private readonly IEmployeeTableService _employeeTableService;
        private readonly ISharedBranchService _sharedBranchService;
        public AccountingMapper(IEmployeeTableService employeeTableService,
            ISharedBranchService sharedBranchService)
        {
            _employeeTableService = employeeTableService;
            _sharedBranchService = sharedBranchService;
        }

        public MPS_M_Accounting ToModel(AccountingViewModel viewModel)
        {
            var model = new MPS_M_Accounting();
            if (viewModel.Employee != null)
            {
                model.EmpId = viewModel.Employee.EmpId;
                model.Email = viewModel.Employee.Email;
                model.Username = viewModel.Employee.Username;
            }

            if (viewModel.SharedBranch != null)
            {
                model.BranchCode = viewModel.SharedBranch.BranchCode;
            }
            model.InActive = viewModel.InActive;
            return model;
        }

        public AccountingViewModel ToViewModel(MPS_M_Accounting model)
        {
            var viewModel = new AccountingViewModel();
            if (model.BranchCode != null)
            {
                viewModel.SharedBranch =
                    _sharedBranchService.FirstOrDefault(m => m.BranchCode.Equals(model.BranchCode)).ToViewModel();
                viewModel.BranchCode = model.BranchCode;
            }
            if (model.EmpId != null)
            {
                viewModel.Employee = _employeeTableService.FirstOrDefault(m => m.EmpId.Equals(model.EmpId)).ToViewModel();
                viewModel.EmpId = model.EmpId;
                viewModel.Email = model.Email;
            }
            viewModel.InActive = model.InActive;
            return viewModel;
        }
    }
}