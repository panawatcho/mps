﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Configuration;
using System.Web;
using CrossingSoft.Framework.Models.Enums;
using TheMall.MPS.API.Mappers.Abstracts;
using TheMall.MPS.API.Services;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Models.CashAdvance;
using TheMall.MPS.Models.Enums;
using TheMall.MPS.ViewModels;
using TheMall.MPS.ViewModels.CashAdvance;
using TheMall.MPS.ViewModels.CashClearing;
using TheMall.MPS.ViewModels.Master;

namespace TheMall.MPS.API.Mappers
{
    public interface ICashAdvanceMapper : IWorkflowTableMapper<MPS_CashAdvanceTable>
    {
        MPS_CashAdvanceTable ToModel(CashAdvanceViewModel viewModel);
        CashAdvanceViewModel ToViewModel(MPS_CashAdvanceTable model);
        CashAdvanceViewModel ToViewModelNoLineId(MPS_CashAdvanceTable model);

        CashAdvanceIndexDataViewModel ToViewModelIndexData(MPS_CashAdvanceTable model);
        IEnumerable<CashAdvanceIndexDataViewModel> ToViewModelsIndexData(IEnumerable<MPS_CashAdvanceTable> models);
        CashAdvanceViewModel ToviewModelOnlyProposalInfo(ProposalInfoViewModel proposalInfo);

        MPS_CashAdvanceTable DeleteBy(MPS_CashAdvanceTable model, EmployeeViewModel employeeViewModel);
    }

    public class CashAdvanceMapper : BaseWorkflowTableMapper<MPS_CashAdvanceTable>, ICashAdvanceMapper
    {
        private readonly IAPcodeService _aPcodeService;
        private readonly ICashAdvanceService _mpsWorkflowTableService;
        private readonly ICashAdvanceLineMapper _lineMapper;
        private readonly IExpenseTopicService _expenseTopicService;
        private readonly IAPcodeService _apCodeService;
        private readonly IPlaceService _placeService;
        private readonly IUnitCodeService _unitCodeService;
        private readonly IEmployeeTableService _employeeTableService;
        private readonly IProposalService _proposalService;
        private readonly IProposalMapper _proposalMapper;
        private readonly ICashAdvanceApproveMapper _approvalMapper;
        private readonly ICashClearingService _cashClearingService;
        private readonly IProposalLineService _proposalLineService;
        private readonly IBudgetProposalTransService _budgetProposalTransService;
        private readonly IAccountingService _accountingService;
        private readonly ICompanyService _companyService;

        public CashAdvanceMapper(
            ICashAdvanceService mpsWorkflowTableService, 
            ICashAdvanceLineMapper lineMapper, 
            IExpenseTopicService expenseTopicService, 
            IAPcodeService apCodeService,
            IAPcodeService aPcodeService, 
            IPlaceService placeService, 
            IUnitCodeService unitCodeService, 
            IEmployeeTableService employeeTableService, 
            IProposalService proposalService, 
            IProposalMapper proposalMapper, 
            ICashAdvanceApproveMapper approvalMapper, 
            ICashClearingService cashClearingService, 
            IProposalLineService proposalLineService,
            IAccountingService accountingService, 
            IBudgetProposalTransService budgetProposalTransService, 
            ICompanyService companyService)
            : base(mpsWorkflowTableService, employeeTableService, placeService, accountingService)
        {
            _mpsWorkflowTableService = mpsWorkflowTableService;
            _lineMapper = lineMapper;
            _expenseTopicService = expenseTopicService;
            _apCodeService = apCodeService;
            _aPcodeService = aPcodeService;
            _placeService = placeService;
            _unitCodeService = unitCodeService;
            _employeeTableService = employeeTableService;
            _proposalService = proposalService;
            _proposalMapper = proposalMapper;
            _approvalMapper = approvalMapper;
            _cashClearingService = cashClearingService;
            _proposalLineService = proposalLineService;
            _budgetProposalTransService = budgetProposalTransService;
            _companyService = companyService;
            _accountingService = accountingService;
        }

        public MPS_CashAdvanceTable ToModel(CashAdvanceViewModel viewModel)
        {
            if (viewModel == null)
            {
                return null;
            }
            var model = InitWorkflowTable(viewModel);
            if (viewModel.ExpenseTopics != null)
            {
                model.ExpenseTopicCode = viewModel.ExpenseTopics.ExpenseTopicCode;
            }
           
            if (viewModel.APCode != null)
            {
                model.APCode = viewModel.APCode.APCode;
            }

            model.ProposalLineID = viewModel.ProposalLineID;
            model.AdvanceDate = DateTime.Now; //viewModel.AdvanceDate;
            model.AdvanceDueDate = DateTime.Now;//viewModel.AdvanceDueDate;
            model.Description = viewModel.Description;
            model.Budget = viewModel.Budget;
          
            if (viewModel.Branch != null)
            {
                model.Branch = viewModel.Branch.PlaceCode;
            }

            model.Title = "Cash Advance";
            //model.RequestFor = viewModel.RequestFor;
            model.RequestForUserName = viewModel.RequestForUserName;

            if (viewModel.RequesterFor!= null)
            {
                model.RequestForUserName = viewModel.RequesterFor.Username;
                model.RequestForEmpId = viewModel.RequesterFor.EmpId;
            }

            if (viewModel.ProposalRef != null)
            {
                var proposalLine = _proposalLineService.Find(m => m.Id == viewModel.ProposalLineId);
                model.ProposalLine = proposalLine;
                model.ProposalLineID = viewModel.ProposalLineId;
                model.ProposalRefID = viewModel.ProposalRef.Id;
                model.ProposalRefDocumentNumber = viewModel.ProposalRef.DocumentNumber;
            }

            if (viewModel.Company != null)
            {
                model.VendorCode = viewModel.Company.VendorCode;
            }

            return model;
        }

        public CashAdvanceViewModel ToViewModel(MPS_CashAdvanceTable model)
        {
            if (model == null)
            {
                return null;
            }

            var viewModel = InitViewModel<CashAdvanceViewModel>(model);
            if (model.ProposalRefID != null)
            {
                var proposalData = _proposalService.Find(m => m.Id == model.ProposalRefID);
                viewModel.ProposalRef = _proposalMapper.ToProposalInfoViewModel(proposalData);
                // viewModel.ProposalRef = proposalData;
            }

            //viewModel.Requester = _employeeTableService.FindByUsername(model.RequesterUserName).ToViewModel();
            if (model.ExpenseTopicCode != null)
            {
                var expenseTopic = _expenseTopicService.Find(m => m.ExpenseTopicCode == model.ExpenseTopicCode);
                //viewModel.ExpenseTopic = _expenseTopicService.DoGet(model.ExpenseTopicCode).ToViewModel();
                // viewModel.ExpenseTopic = _expenseTopicService.Find(m => m.ExpenseTopicCode == model.ExpenseTopicCode).ToViewModel();
                viewModel.ExpenseTopics = expenseTopic != null
                  ? expenseTopic.ToViewModel()
                  : new ExpenseTopicViewModel();
            }

            if (model.APCode != null && viewModel.ProposalRef != null)
            {
                viewModel.APCode = viewModel.ProposalRef.APCode.Find(m => m.APCode == model.APCode && m.ProposalLineId == model.ProposalLineID);
            }

            viewModel.ProposalLineID = model.ProposalLineID;
            viewModel.AdvanceDate = model.AdvanceDate;
            viewModel.AdvanceDueDate = model.AdvanceDueDate;

            //viewModel.Description = model.Description;
            if (model.Description != null)
            {
                viewModel.Description = model.Description;
            }
            else
            {
                viewModel.Description = "";
            }

            viewModel.Budget = model.Budget;
            //viewModel.Accounting = (!string.IsNullOrEmpty(model.AccountingUsername)) ? _employeeTableService.FindByUsername(model.AccountingUsername).ToViewModel() : null;

            if (model.Branch != null)
            {
                var branch = _placeService.Find(m => m.PlaceCode.Equals(model.Branch));
                viewModel.Branch = branch.ToViewModel();
            }

            viewModel.Comments = model.Comments.ToViewModel();
            //viewModel.RequestForEmpId = model.RequestForEmpId;
            viewModel.RequestForUserName = model.RequestForUserName;

            if (model.UnitCode != null)
            {
                var unitCodeData = _unitCodeService.Find(m => m.UnitCode.Equals(model.UnitCode));
                viewModel.UnitCode = unitCodeData.ToViewModel();
            }

            if (model.RequestForEmpId != null)
            {
                var requestForData = _employeeTableService.Find(m => m.EmpId.Equals(model.RequestForEmpId));
                viewModel.RequesterFor = requestForData.ToViewModel();
            }

            if (model.CashAdvanceLine != null && model.CashAdvanceLine.Any())
            {
                viewModel.CashAdvanceLines = model.CashAdvanceLine.Select(_lineMapper.ToViewModel).ToList();
            }

            viewModel.CashAdvanceApproval = _approvalMapper.ToViewModels(model.CashAdvanceApproval);
            viewModel.ProposalLineId = model.ProposalLineID;
            var budgetTrans = _budgetProposalTransService.GetBudgetReserve(model.ProposalRefID, model.ProposalLineID, model.APCode, model.UnitCode, Budget.DocumentType.CashAdvance);
            viewModel.BudgetTrans = budgetTrans;

            if (model.VendorCode != null)
            {
                var companyData = _companyService.Find(m => m.VendorCode.Equals(model.VendorCode));
                viewModel.Company = companyData.ToViewModel();
            }
            else
            {
                var vendorCode = _companyService.Find(m => m.VendorCode.Equals(viewModel.Branch.PlaceCode));
                if (vendorCode != null)
                {
                    viewModel.Company = vendorCode.ToViewModel();
                }
                else
                {
                    viewModel.Company = null;
                }
            }

            return viewModel;
        }

        public CashAdvanceViewModel ToViewModelNoLineId(MPS_CashAdvanceTable model)
        {
            if (model == null)
            {
                return null;
            }

            var viewModel = InitViewModel<CashAdvanceViewModel>(model);

            if (model.ExpenseTopicCode != null)
            {
                var expenseTopic = _expenseTopicService.Find(m => m.ExpenseTopicCode == model.ExpenseTopicCode);
                //viewModel.ExpenseTopic = _expenseTopicService.DoGet(model.ExpenseTopicCode).ToViewModel();
                //viewModel.ExpenseTopic = _expenseTopicService.Find(m => m.ExpenseTopicCode == model.ExpenseTopicCode).ToViewModel();
                viewModel.ExpenseTopics = expenseTopic != null
                  ? expenseTopic.ToViewModel()
                  : new ExpenseTopicViewModel();
            }

            if (model.APCode != null && viewModel.ProposalRef != null)
            {
                viewModel.APCode =
                    viewModel.ProposalRef.APCode.Find(
                        m => m.APCode == model.APCode && m.ProposalLineId == model.ProposalLineID);
            }

            viewModel.ProposalLineID = model.ProposalLineID;
            viewModel.AdvanceDate = model.AdvanceDate;
            viewModel.AdvanceDueDate = model.AdvanceDueDate;
            viewModel.Description = model.Description;
            viewModel.Budget = model.Budget;

            if (model.Branch != null)
            {
                var branch = _placeService.Find(m => m.PlaceCode.Equals(model.Branch));
                viewModel.Branch = branch.ToViewModel();
            }

            viewModel.Comments = model.Comments.ToViewModel();
            //viewModel.RequestForEmpId = model.RequestForEmpId;
            viewModel.RequestForUserName = model.RequestForUserName;

            if (model.UnitCode != null)
            {
                var unitCodeData = _unitCodeService.Find(m => m.UnitCode.Equals(model.UnitCode));
                viewModel.UnitCode = unitCodeData.ToViewModel();
            }

            if (model.RequestForEmpId != null)
            {
                var requestForData = _employeeTableService.Find(m => m.EmpId.Equals(model.RequestForEmpId));
                viewModel.RequesterFor = requestForData.ToViewModel();
            }

            if (model.CashAdvanceLine.Any())
            {
                viewModel.CashAdvanceLines = model.CashAdvanceLine.Select(_lineMapper.ToViewModelNoLineId).ToList();
            }

            if (model.ProposalRefID != null)
            {
                var proposalData = _proposalService.Find(m => m.Id == model.ProposalRefID);
                viewModel.ProposalRef = _proposalMapper.ToProposalInfoViewModel(proposalData);
                // viewModel.ProposalRef = proposalData;
            }

            return viewModel;
        }

        public CashAdvanceIndexDataViewModel ToViewModelIndexData(MPS_CashAdvanceTable model)
        {
            var indexViewModel = new CashAdvanceIndexDataViewModel();
            indexViewModel.Id = model.Id;
            indexViewModel.CashClearingId = 0;

            if (model.ExpenseTopicCode != null)
            {
                var expenseTopic = _expenseTopicService.Find(m => m.ExpenseTopicCode == model.ExpenseTopicCode);
                indexViewModel.ExpenseTopicCode = expenseTopic.ExpenseTopicCode;
                indexViewModel.ExpenseTopicName = expenseTopic.ExpenseTopicName;
            } 
             
            if (model.APCode != null)
            {
                var APCode = _apCodeService.Find(m => m.APCode == model.APCode);
                indexViewModel.APCodeCode = APCode.APCode;
                indexViewModel.APCodeName = APCode.APName;
            }

            if (model.ProposalRefID != null)
            {
                indexViewModel.ProposalRefID = model.ProposalRefID;
                indexViewModel.ProposalRefDocumentNumber = model.ProposalRefDocumentNumber;
            }

            indexViewModel.DocumentNumber = model.DocumentNumber;
            indexViewModel.Title = model.Title;
            indexViewModel.AdvanceDueDate = model.AdvanceDueDate;
            
            //indexViewModel.Description = model.Description;
            if (model.Description != null)
            {
                indexViewModel.Description = model.Description;
            }
            else
            {
                indexViewModel.Description = "";
            }

            indexViewModel.Budget = model.Budget;
            indexViewModel.DocumentStatus = model.DocumentStatus;
            indexViewModel.CreatedBy = model.CreatedBy;
            indexViewModel.StatusFlag = model.StatusFlag;

            var cashClearingModel = _cashClearingService.FirstOrDefault(m => m.CashAdvanceID == model.Id);
            if (cashClearingModel != null)
            {
                indexViewModel.CashClearingDocumentNumber = cashClearingModel.DocumentNumber;
                indexViewModel.CashClearingId = cashClearingModel.Id;
                indexViewModel.CashClearingStatus = cashClearingModel.Status;
                indexViewModel.CashClearingStatusFlag = cashClearingModel.StatusFlag;
            }

            indexViewModel.CreatedDate = model.CreatedDate;

            if (model.Status != null)
            {
                //if (model.Status.Equals(DocumentStatus.Completed) && model.StatusFlag == DocumentStatusFlagId.Completed)
                if (model.Status.Equals(DocumentStatus.Completed.ToString(), StringComparison.InvariantCultureIgnoreCase) && model.StatusFlag == DocumentStatusFlagId.Completed)
                {
                    indexViewModel.CheckComplete = true;
                }
                else
                {
                    indexViewModel.CheckComplete = false;
                }
            }

            indexViewModel.RequestForUserName = model.RequestForUserName;
            indexViewModel.Status = model.Status;

            return indexViewModel;
        }

        public IEnumerable<CashAdvanceIndexDataViewModel> ToViewModelsIndexData(IEnumerable<MPS_CashAdvanceTable> models)
        {
            return models != null ? models.Select(ToViewModelIndexData) : Enumerable.Empty<CashAdvanceIndexDataViewModel>();
        }

        public CashAdvanceViewModel ToviewModelOnlyProposalInfo(ProposalInfoViewModel proposalInfo)
        {
            var cashAdvanceViewModel = new CashAdvanceViewModel();
            cashAdvanceViewModel.ProposalRef = proposalInfo;
            foreach(var line in proposalInfo.ProposalLine)
            {
                cashAdvanceViewModel.UnitCode = line.UnitCode;
            }

            return cashAdvanceViewModel;
        }

        public MPS_CashAdvanceTable DeleteBy(MPS_CashAdvanceTable model, EmployeeViewModel employeeViewModel)
        {
            if (model != null)
            {
                if (model.Id > 0)
                {
                    model.Deleted = true;
                    model.DeletedDate = DateTime.Now;
                    model.DeletedBy = employeeViewModel.Username;
                    model.DeletedByName = employeeViewModel.FullName;
                    return model;
                }
                else
                {
                    return null;
                }
            } else
            {
                return null;
            }
        }
    }
}