﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TheMall.MPS.API.Services;
using TheMall.MPS.Mappers.Abstracts;
using TheMall.MPS.Models.Memo;

using TheMall.MPS.ViewModels.Proposal;

namespace TheMall.MPS.API.Mappers
{
    public interface IMemoApprovalMapper : ILineMapper
    {
        MPS_MemoApproval ToModel(MemoApprovalViewModel viewModel, IEnumerable<MPS_MemoApproval> lines);
        MemoApprovalViewModel ToViewModel(MPS_MemoApproval model);
        IEnumerable<MemoApprovalViewModel> ToViewModels(IEnumerable<MPS_MemoApproval> models);
        IEnumerable<MPS_MemoApproval> ToModels(IEnumerable<MemoApprovalViewModel> viewModels, IEnumerable<MPS_MemoApproval> lines);
    }

    public class MemoApprovalMapper : BaseLineMapper, IMemoApprovalMapper
    {
         private readonly IEmployeeTableService _employeeTableService;

         public MemoApprovalMapper(IEmployeeTableService employeeTableService)
        {
            _employeeTableService = employeeTableService;
        }
        public MPS_MemoApproval ToModel(MemoApprovalViewModel viewModel, IEnumerable<MPS_MemoApproval> lines)
        {
            if (viewModel == null || string.IsNullOrEmpty(viewModel.ApproverUserName))
            {
                return null;
            }

            if (string.IsNullOrEmpty(viewModel.ApproverEmail))
                throw new Exception("Please select approver in Approver Grid every levels!!!");


            var model = InitLineTable(viewModel, lines);
            if (model == null)
            {
                return null;
            }
             model.ApproverSequence = viewModel.ApproverSequence;
             model.ApproverLevel = viewModel.ApproverLevel;
             if (viewModel.Employee != null)
             {
                 model.ApproverUserName = viewModel.Employee.Username;
                 model.ApproverFullName = viewModel.Employee.FullName;
             }
             model.ApproverEmail = viewModel.ApproverEmail;
             model.Position = viewModel.Position;
             model.Department = viewModel.Department;
            model.LockEditable = viewModel.LockEditable;
             if (viewModel.DueDate != null)
                 model.DueDate = viewModel.DueDate.Value.ToLocalTime().Date.AddDays(1).AddSeconds(-1).ToUniversalTime(); ;
             ;
            return model;
        }

       

        public MemoApprovalViewModel ToViewModel(MPS_MemoApproval model)
        {
            if (model == null)
            {
                return null;
            }
            var viewModel = this.InitLineViewModel<MemoApprovalViewModel>(model);
            if (model.ApproverUserName != null)
                viewModel.Employee = _employeeTableService.FindByUsername(model.ApproverUserName).ToViewModel();
            viewModel.ApproverSequence = model.ApproverSequence;
            viewModel.ApproverLevel = model.ApproverLevel;
            viewModel.ApproverUserName = model.ApproverUserName;
            viewModel.ApproverFullName = model.ApproverFullName;
            viewModel.ApproverEmail = model.ApproverEmail;
            viewModel.Position = model.Position;
            viewModel.Department = model.Department;
            viewModel.DueDate = model.DueDate;
            viewModel.LockEditable = model.LockEditable;
            viewModel.ApproveStatus = model.ApproveStatus;
            return viewModel;
        }

        public IEnumerable<MemoApprovalViewModel> ToViewModels(IEnumerable<MPS_MemoApproval> models)
        {
            return models != null ? models.Select(ToViewModel) : Enumerable.Empty<MemoApprovalViewModel>();
        }
        public IEnumerable<MPS_MemoApproval> ToModels(IEnumerable<MemoApprovalViewModel> viewModels, IEnumerable<MPS_MemoApproval> lines)
        {
            return viewModels != null ? viewModels.Select(m => ToModel(m, lines)).Where(m => m != null) : Enumerable.Empty<MPS_MemoApproval>();
        }

       
    }
}