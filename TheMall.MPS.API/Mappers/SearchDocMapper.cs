﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TheMall.MPS.API.Services;
using TheMall.MPS.Models.CashAdvance;
using TheMall.MPS.Models.CashClearing;
using TheMall.MPS.Models.Memo;
using TheMall.MPS.Models.PettyCash;
using TheMall.MPS.Models.Proposal;
using TheMall.MPS.Models.MemoIncome;
using TheMall.MPS.ViewModels;
using TheMall.MPS.ViewModels.CashClearing;

namespace TheMall.MPS.API.Mappers
{
    public interface ISearchDocMapper
    {
        //Proposal
        SearchDocViewModel ProposalToViewModel(MPS_ProposalTable model);
        IEnumerable<SearchDocViewModel> ProposalToViewModels(IEnumerable<MPS_ProposalTable> models);
        //Cash advance 
        SearchDocViewModel CashAdvanceToViewModel(MPS_CashAdvanceTable model);
        IEnumerable<SearchDocViewModel> CashAdvanceToViewModels(IEnumerable<MPS_CashAdvanceTable> models);
        //Cash clearing
        SearchDocViewModel CashClearingToViewModel(MPS_CashClearingTable model);
        IEnumerable<SearchDocViewModel> CashClearingToViewModels(IEnumerable<MPS_CashClearingTable> models);
        //Petty cash
        SearchDocViewModel PettyCashToViewModel(MPS_PettyCashTable model);
        IEnumerable<SearchDocViewModel> PettyCashToViewModels(IEnumerable<MPS_PettyCashTable> models);
        //Memo
        SearchDocViewModel MemoToViewModel(MPS_MemoTable model);
        IEnumerable<SearchDocViewModel> MemoToViewModels(IEnumerable<MPS_MemoTable> models);
        //MemoIncome
        SearchDocViewModel MemoIncomeToViewModel(MPS_MemoIncomeTable model);
        IEnumerable<SearchDocViewModel> MemoIncomeToViewModels(IEnumerable<MPS_MemoIncomeTable> models);
    }

    public class SearchDocMapper : ISearchDocMapper
    {
        private readonly IAllocationBasisService _allocationBasisService;

        public SearchDocMapper(IAllocationBasisService allocationBasisService)
        {
            _allocationBasisService = allocationBasisService;
        }

        //Proposal
        public SearchDocViewModel ProposalToViewModel(MPS_ProposalTable model)
        {
            var viewModel = new SearchDocViewModel();
            if (model != null)
            {
                viewModel.Id = model.Id;
                viewModel.DocumentNumber = model.DocumentNumber;
                viewModel.Status = model.DocumentStatus;
                if (model.CreatedDate != null)
                {
                    viewModel.CreateDate = (DateTime)model.CreatedDate;
                }
                viewModel.Username = model.CreatedBy;
                viewModel.Title = model.Title;
                viewModel.UnitCode = model.UnitCode;
                viewModel.UnitName = model.UnitName;
                //viewModel.AllocationCode =
                //    _allocationBasisService.FindByProposalSharedTemplate(model.TemplateLines).AllocCode;
                return viewModel;
            }
            return null;
        }

        public IEnumerable<SearchDocViewModel> ProposalToViewModels(IEnumerable<MPS_ProposalTable> models)
        {
            return models != null ? models.Select(ProposalToViewModel) : Enumerable.Empty<SearchDocViewModel>();
        }

        //Cash advance
        public SearchDocViewModel CashAdvanceToViewModel(MPS_CashAdvanceTable model)
        {
            var viewModel = new SearchDocViewModel();
            if (model != null)
            {
                viewModel.Id = model.Id;
                viewModel.DocumentNumber = model.DocumentNumber;
                viewModel.Status = model.DocumentStatus;
                if (model.CreatedDate != null)
                {
                    viewModel.CreateDate = (DateTime)model.CreatedDate;
                }
                viewModel.Username = model.CreatedBy;
                viewModel.Title = model.Title;
                viewModel.UnitCode = model.UnitCode;
                viewModel.UnitName = model.UnitName;
                return viewModel;
            }
            return null;
        }
        public IEnumerable<SearchDocViewModel> CashAdvanceToViewModels(IEnumerable<MPS_CashAdvanceTable> models)
        {
            return models != null ? models.Select(CashAdvanceToViewModel) : Enumerable.Empty<SearchDocViewModel>();
        }

        //Cash clearing
        public SearchDocViewModel CashClearingToViewModel(MPS_CashClearingTable model)
        {
            var viewModel = new SearchDocViewModel();
            if (model != null)
            {
                viewModel.Id = model.Id;
                viewModel.DocumentNumber = model.DocumentNumber;
                viewModel.Status = model.DocumentStatus;
                if (model.CreatedDate != null)
                {
                    viewModel.CreateDate = (DateTime)model.CreatedDate;
                }
                viewModel.Username = model.CreatedBy;
                viewModel.Title = model.Title;
                viewModel.UnitCode = model.UnitCode;
                viewModel.UnitName = model.UnitName;
                return viewModel;
            }
            return null;
        }

        public IEnumerable<SearchDocViewModel> CashClearingToViewModels(IEnumerable<MPS_CashClearingTable> models)
        {
            return models != null ? models.Select(CashClearingToViewModel) : Enumerable.Empty<SearchDocViewModel>();
        }
        
        //Memo
        public SearchDocViewModel MemoToViewModel(MPS_MemoTable model)
        {
            var viewModel = new SearchDocViewModel();
            if (model != null)
            {
                viewModel.Id = model.Id;
                viewModel.DocumentNumber = model.DocumentNumber;
                viewModel.Status = model.DocumentStatus;
                if (model.CreatedDate != null)
                {
                    viewModel.CreateDate = (DateTime)model.CreatedDate;
                }
                viewModel.Username = model.CreatedBy;
                viewModel.Title = model.Title;
                viewModel.UnitCode = model.UnitCode;
                viewModel.UnitName = model.UnitName;
                return viewModel;
            }
            return null;
        }

        public IEnumerable<SearchDocViewModel> MemoToViewModels(IEnumerable<MPS_MemoTable> models)
        {
            return models != null ? models.Select(MemoToViewModel) : Enumerable.Empty<SearchDocViewModel>();
        }

        //Petty cash
        public SearchDocViewModel PettyCashToViewModel(MPS_PettyCashTable model)
        {
            var viewModel = new SearchDocViewModel();
            if (model != null)
            {
                viewModel.Id = model.Id;
                viewModel.DocumentNumber = model.DocumentNumber;
                viewModel.Status = model.DocumentStatus;
                if (model.CreatedDate != null)
                {
                    viewModel.CreateDate = (DateTime)model.CreatedDate;
                }
                viewModel.Username = model.CreatedBy;
                viewModel.Title = model.Title;
                viewModel.UnitCode = model.UnitCode;
                viewModel.UnitName = model.UnitName;
                return viewModel;
            }
            return null;
        }

        public IEnumerable<SearchDocViewModel> PettyCashToViewModels(IEnumerable<MPS_PettyCashTable> models)
        {
            return models != null ? models.Select(PettyCashToViewModel) : Enumerable.Empty<SearchDocViewModel>();
        }

        //MemoIncome
        public SearchDocViewModel MemoIncomeToViewModel(MPS_MemoIncomeTable model)
        {
            var viewModel = new SearchDocViewModel();
            if (model != null)
            {
                viewModel.Id = model.Id;
                viewModel.DocumentNumber = model.DocumentNumber;
                viewModel.Status = model.DocumentStatus;
                if (model.CreatedDate != null)
                {
                    viewModel.CreateDate = (DateTime)model.CreatedDate;
                }
                viewModel.Username = model.CreatedBy;
                viewModel.Title = model.Title;
                viewModel.UnitCode = model.UnitCode;
                viewModel.UnitName = model.UnitName;
                return viewModel;
            }
            return null;
        }

        public IEnumerable<SearchDocViewModel> MemoIncomeToViewModels(IEnumerable<MPS_MemoIncomeTable> models)
        {
            return models != null ? models.Select(MemoIncomeToViewModel) : Enumerable.Empty<SearchDocViewModel>();
        }
    }
}