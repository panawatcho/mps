﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TheMall.MPS.Mappers.Abstracts;
using TheMall.MPS.Models.Proposal;
using TheMall.MPS.ViewModels.Proposal;

namespace TheMall.MPS.API.Mappers
{
    public interface IProposalIncomeTotalSaleMapper : ILineMapper
    {
        MPS_IncomeTotalSale ToModel(IncomeTotalSaleViewModel viewModel, IEnumerable<MPS_IncomeTotalSale> lines);
        IncomeTotalSaleViewModel ToViewModel(MPS_IncomeTotalSale model);
        IEnumerable<IncomeTotalSaleViewModel> ToViewModels(IEnumerable<MPS_IncomeTotalSale> models);
        IEnumerable<MPS_IncomeTotalSale> ToModels(IEnumerable<IncomeTotalSaleViewModel> viewModels, IEnumerable<MPS_IncomeTotalSale> lines);
    }

    public class ProposalIncomeTotalSaleMapper : BaseLineMapper, IProposalIncomeTotalSaleMapper
    {
        private readonly IProposalIncomeTotalSaleTemplateMapper _totalSaleTemplateMapper;

        public ProposalIncomeTotalSaleMapper(IProposalIncomeTotalSaleTemplateMapper totalSaleTemplateMapper)
        {
            _totalSaleTemplateMapper = totalSaleTemplateMapper;
        }

        public MPS_IncomeTotalSale ToModel(IncomeTotalSaleViewModel viewModel, IEnumerable<MPS_IncomeTotalSale> lines)
        {
            if (viewModel == null)
            {
                return null;
            }
            var model = InitLineTable(viewModel, lines);
            if (model == null)
            {
                return new MPS_IncomeTotalSale();
            }
            model.Description = viewModel.Description;
            model.Budget= viewModel.Budget;
            model.Remark = viewModel.Remark;
            model.ActualLastYear = viewModel.ActualLastYear;
            model.ActualAmount = viewModel.ActualAmount;

            return model;
        }
        public IncomeTotalSaleViewModel ToViewModel(MPS_IncomeTotalSale model)
        {
            if (model == null)
            {
                return null;
            }
            var viewModel = this.InitLineViewModel<IncomeTotalSaleViewModel>(model);

            viewModel.Description = model.Description;
            viewModel.Budget = model.Budget;
            viewModel.Remark = model.Remark;
            viewModel.ActualLastYear = model.ActualLastYear;
            viewModel.ActualAmount = model.ActualAmount;
            viewModel.IncomeTotalSaleTemplate = _totalSaleTemplateMapper.ToViewModels(model.IncomeTotalSaleTemplate);

            return viewModel;
        }
        public IEnumerable<IncomeTotalSaleViewModel> ToViewModels(IEnumerable<MPS_IncomeTotalSale> models)
        {
            return models != null ? models.Select(ToViewModel) : Enumerable.Empty<IncomeTotalSaleViewModel>();
        }
        public IEnumerable<MPS_IncomeTotalSale> ToModels(IEnumerable<IncomeTotalSaleViewModel> viewModels, IEnumerable<MPS_IncomeTotalSale> lines)
        {
            return viewModels != null ? viewModels.Select(m => ToModel(m, lines)).Where(m => m != null) : Enumerable.Empty<MPS_IncomeTotalSale>();
        }
    }
}
