﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TheMall.MPS.API.Mappers.Abstracts;
using TheMall.MPS.API.Services;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Mappers.Abstracts;
using TheMall.MPS.Models.Memo;
using TheMall.MPS.Models.PettyCash;
using TheMall.MPS.Models.Proposal;
using TheMall.MPS.ViewModels.Memo;
using TheMall.MPS.ViewModels.Proposal;

namespace TheMall.MPS.API.Mappers
{
    public interface IPettyCashApprovalMapper : ILineMapper
    {
        MPS_PettyCashApproval ToModel(PettyCashApprovalViewModel viewModel, IEnumerable<MPS_PettyCashApproval> lines);
        PettyCashApprovalViewModel ToViewModel(MPS_PettyCashApproval model);
        IEnumerable<PettyCashApprovalViewModel> ToViewModels(IEnumerable<MPS_PettyCashApproval> models);
        IEnumerable<MPS_PettyCashApproval> ToModels(IEnumerable<PettyCashApprovalViewModel> viewModels, IEnumerable<MPS_PettyCashApproval> lines);
    }

    public class PettyCashApprovalMapper : BaseLineMapper, IPettyCashApprovalMapper
    {
        private readonly IEmployeeTableService _employeeTableService;

        public  PettyCashApprovalMapper(IEmployeeTableService employeeTableService)
        {
            _employeeTableService = employeeTableService;
        }

        public MPS_PettyCashApproval ToModel(PettyCashApprovalViewModel viewModel, IEnumerable<MPS_PettyCashApproval> lines)
        {
            if (viewModel == null || string.IsNullOrEmpty(viewModel.ApproverUserName))
            {
                return null;
            }

            if (string.IsNullOrEmpty(viewModel.ApproverEmail))
                throw new Exception("Please select approver in Approver Grid every levels!!!");

            var model = InitLineTable(viewModel, lines);
            if (model == null)
            {
                return null;
            }
             model.ApproverSequence = viewModel.ApproverSequence;
             model.ApproverLevel = viewModel.ApproverLevel;
             if (viewModel.Employee != null)
             {
                 model.ApproverUserName = viewModel.Employee.Username;
                 model.ApproverFullName = viewModel.Employee.FullName;
             }

             model.ApproverEmail = viewModel.ApproverEmail;
             model.Position = viewModel.Position;
             model.Department = viewModel.Department;
             if (viewModel.DueDate != null)
                 model.DueDate = viewModel.DueDate.Value.ToLocalTime().Date.AddDays(1).AddSeconds(-1).ToUniversalTime(); ;
             ;
            model.LockEditable = viewModel.LockEditable;

            return model;
        }

        public PettyCashApprovalViewModel ToViewModel(MPS_PettyCashApproval model)
        {
            if (model == null)
            {
                return null;
            }
            var viewModel = this.InitLineViewModel<PettyCashApprovalViewModel>(model);
            if (model.ApproverUserName != null)
                viewModel.Employee = _employeeTableService.FindByUsername(model.ApproverUserName).ToViewModel();
            viewModel.ApproverSequence = model.ApproverSequence;
            viewModel.ApproverLevel = model.ApproverLevel;
            viewModel.ApproverUserName = model.ApproverUserName;
            viewModel.ApproverFullName = model.ApproverFullName;
            viewModel.ApproverEmail = model.ApproverEmail;
            viewModel.Position = model.Position;
            viewModel.Department = model.Department;
            viewModel.DueDate = model.DueDate;
            viewModel.LockEditable = model.LockEditable;
            viewModel.ApproveStatus = model.ApproveStatus;
            return viewModel;
        }

        public IEnumerable<PettyCashApprovalViewModel> ToViewModels(IEnumerable<MPS_PettyCashApproval> models)
        {
            return models != null ? models.Select(ToViewModel) : Enumerable.Empty<PettyCashApprovalViewModel>();
        }
       
        public IEnumerable<MPS_PettyCashApproval> ToModels(IEnumerable<PettyCashApprovalViewModel> viewModels, IEnumerable<MPS_PettyCashApproval> lines)
        {
            return viewModels != null ? viewModels.Select(m => ToModel(m, lines)).Where(m => m != null) : Enumerable.Empty<MPS_PettyCashApproval>();
        }

       
    }
}