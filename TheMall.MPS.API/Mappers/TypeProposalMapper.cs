﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using TheMall.MPS.Models.Master;
using TheMall.MPS.ViewModels.Master;

namespace TheMall.MPS.API.Mappers
{
    public class TypeProposalMapperProfile : AutoMapper.Profile
    {
        public TypeProposalMapperProfile()
        {
            CreateMap<MPS_M_TypeProposal, TypeProposalViewModel>()
                .ReverseMap();
        }
    }

    public static class TypeProposalMapper
    {
        public static MPS_M_TypeProposal ToModel(this TypeProposalViewModel viewModel)
        {
            if (viewModel == null)
                return null;
            var model = new MPS_M_TypeProposal()
            {
                TypeProposalCode = viewModel.TypeProposalCode,
                TypeProposalName = viewModel.TypeProposalName,
                TypeYearPlan = viewModel.TypeYearPlan,
                InActive = viewModel.InActive,
                Order = viewModel.Order
            };
            return model;

            return Mapper.Map<MPS_M_TypeProposal>(viewModel);
        }

        public static TypeProposalViewModel ToViewModel(this MPS_M_TypeProposal model)
        {
            if (model == null)
                return null;
            var viewModel = new TypeProposalViewModel()
            {
                TypeProposalCode = model.TypeProposalCode,
                TypeProposalName = model.TypeProposalName,
                TypeYearPlan = model.TypeYearPlan,
                InActive = model.InActive,
                Order = model.Order
            };
            return viewModel;

            return Mapper.Map<TypeProposalViewModel>(model);
        }
        public static IEnumerable<TypeProposalViewModel> ToViewModels(this IEnumerable<MPS_M_TypeProposal> models)
        {
            return models != null ? models.Select(ToViewModel) : Enumerable.Empty<TypeProposalViewModel>();
        }
        public static IEnumerable<MPS_M_TypeProposal> ToModels(IEnumerable<TypeProposalViewModel> viewModels, IEnumerable<MPS_M_TypeProposal> lines)
        {
            return viewModels != null ? viewModels.Select(ToModel) : Enumerable.Empty<MPS_M_TypeProposal>();
        }

    }
}