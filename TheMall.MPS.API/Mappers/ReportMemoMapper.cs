﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using TheMall.MPS.API.Services;
using TheMall.MPS.Infrastructure.Extension;
using TheMall.MPS.Models.Enums;
using TheMall.MPS.Models.Memo;
using TheMall.MPS.Reports.Memo;
using TheMall.MPS.Reports.Shared;

namespace TheMall.MPS.API.Mappers
{
    public interface IReportMemoMapper
    {
        IEnumerable<ApproverDataSource> ApproversDataMapper(IEnumerable<MPS_MemoApproval> approversData);
        IEnumerable<MemoDataSource> ReportDataMapper(MPS_MemoTable memoModel);
        IEnumerable<MemoLinesDataSource> ReportLinesDataMapper(MPS_MemoTable memoModel);
    }

    public class ReportMemoMapper :IReportMemoMapper
    {
        private readonly IMemoMapper _memoMapper;
        private readonly IMemoLineMapper _memoLineMapper;
        private readonly IEmployeeTableService _employeeTableService;

        public ReportMemoMapper(IMemoMapper memoMapper, 
            IMemoLineMapper memoLineMapper, 
            IEmployeeTableService employeeTableService)
        {
            _memoMapper = memoMapper;
            _memoLineMapper = memoLineMapper;
            _employeeTableService = employeeTableService;
        }

        public  IEnumerable<MemoDataSource> ReportDataMapper(MPS_MemoTable memoModel)
        {
            var memoViewModel = _memoMapper.ToViewModel(memoModel);
            var memoDataSource = new List<MemoDataSource>();
            if (memoViewModel != null)
            {
                var line = new MemoDataSource();

                line.Date = DateTime.Now.ToString("dd/MM/yyyy");
                line.Id = memoViewModel.Id;
                line.Title = memoViewModel.Title;
                if (memoViewModel.Requester != null)
                {
                    line.Requester = memoViewModel.Requester.Username ?? "";
                    line.RequesterName = memoViewModel.Requester.FullName ?? "";
                //  line.RequesterDepartment = memoViewModel.Requester.DepartmentName ?? "";
                    line.RequesterPosition = memoViewModel.Requester.PositionName ?? "";
                }

                line.Branch = memoViewModel.Branch != null ? memoViewModel.Branch.Description : "";

                if (memoModel.Attachments != null)
                {
                    line.AttachmentName = "";
                    foreach (var lineAttachment in memoModel.Attachments)
                    {
                        if (line.AttachmentName == "")
                        {
                            line.AttachmentName = lineAttachment.FileName;
                        }
                        else
                        {
                            line.AttachmentName = line.AttachmentName + ", " + lineAttachment.FileName;
                        }
                    }
                }

                if (memoViewModel.UnitCode != null)
                {
                    line.UnitCode = memoViewModel.UnitCode.UnitCode + " - " + memoViewModel.UnitCode.UnitName;
                    //โชว์ไว้ในหน้า Report
                    line.RequesterDepartment = memoViewModel.UnitCode.UnitName ?? "";
                }

                if (memoViewModel.ExpenseTopic != null)
                {
                    line.ExpenseTopic = memoViewModel.ExpenseTopic.ExpenseTopicCode + " - " + memoViewModel.ExpenseTopic.ExpenseTopicName;
                }

                if (memoViewModel.APCode != null)
                {
                    line.APCode = memoViewModel.APCode.APCode + " - " + memoViewModel.APCode.APName;
                }

                if (memoViewModel.CCEmail != null)
                {
                    line.CC = "";
                    foreach (var name in memoViewModel.CCEmail)
                    {
                        var emploloyee = _employeeTableService.FindByUsername(name.Name);
                        var fullName = (emploloyee != null) ? emploloyee.ThFullName : name.Name;
                       line.CC = line.CC == "" ? fullName : line.CC + ", " + fullName;
                    }
                }


                if (memoViewModel.InformEmail != null)
                {
                    line.Inform = "";
                    foreach (var name in memoViewModel.InformEmail)
                    {
                        var emploloyee = _employeeTableService.FindByUsername(name.Name);
                        var fullName = (emploloyee != null) ? emploloyee.ThFullName : name.Name;
                        line.Inform = line.Inform == "" ? fullName : line.Inform + ", " + fullName;
                    }
                }

                if (memoViewModel.ProposalRef != null)
                {
                    line.ProposalNumber = memoViewModel.ProposalRef.DocumentNumber;
                    line.ProposalName = memoViewModel.ProposalRef.Title;
                    line.StartDate = memoViewModel.ProposalRef.StartDate.ToString("dd/MM/yyyy");
                    line.EndDate = memoViewModel.ProposalRef.EndDate.ToString("dd/MM/yyyy");
                    if (memoViewModel.ProposalRef.ProposalUnitCode != null)
                    {
                        line.ProposalUnitCode = memoViewModel.ProposalRef.ProposalUnitCode.UnitCode;
                        line.ProposalUnitName = memoViewModel.ProposalRef.ProposalUnitCode.UnitName;
                    }
                }
                if (memoViewModel.SubmittedDate != null)
                {
                    DateTime submitDate = (DateTime)memoViewModel.SubmittedDate;
                    line.SubmitDate = submitDate.ToString("dd/MM/yyyy");
                }

                line.MemoDocumentNumber = memoViewModel.DocumentNumber;

                if (memoViewModel.Description != null)
                {
                    //ine.Objective = HtmlToPlainText(memoViewModel.Description
                    //line.Objective = memoViewModel.Description;
                    line.Objective = memoViewModel.Description.Replace("font-family", string.Empty);
                    var imgs = line.Objective.Contains("img");
                    line.ObjectiveImg = imgs ? "*ดูรูปเพิ่มเติมได้ในระบบ" : "";
                }

                memoDataSource.Add(line);
                return memoDataSource;
            }
            else
            {
                return null;
            }
        }
       
        //private string GetPlainTextFromHtml(string htmlString)
        //{
        //    string htmlTagPattern = "<.*?>";
        //    var regexCss = new Regex("(\\<script(.+?)\\</script\\>)|(\\<style(.+?)\\</style\\>)", RegexOptions.Singleline | RegexOptions.IgnoreCase);
        //    htmlString = regexCss.Replace(htmlString, string.Empty);
        //    htmlString = Regex.Replace(htmlString, htmlTagPattern, string.Empty);
        //    htmlString = Regex.Replace(htmlString, @"^\s+$[\r\n]*", "", RegexOptions.Multiline);
          
        //    htmlString = htmlString.Replace("<p>", string.Empty);
        //    htmlString = htmlString.Replace("</p>", Environment.NewLine);
        //    htmlString = htmlString.Replace("&nbsp;", " ");

        //    return htmlString;
        //}

        public IEnumerable<MemoLinesDataSource> ReportLinesDataMapper(MPS_MemoTable memoModel)
        {
           var lineViewModels =  _memoLineMapper.ToViewModels(memoModel.MemoLine);
           var list = new List<MemoLinesDataSource>();
           if (lineViewModels != null)
            {
                foreach (var line in lineViewModels)
                {
                    var reportDataSource = new MemoLinesDataSource();
                    reportDataSource.Id = line.Id;
                    reportDataSource.Description = line.Description??"";
                    reportDataSource.Amount = line.Amount;
                    reportDataSource.Unit = line.Unit;
                    reportDataSource.NetAmount = line.NetAmount;
                    reportDataSource.Remark = line.Remark ?? "";
                    reportDataSource.Tax = line.Tax;
                    reportDataSource.Vat = line.Vat;
                    list.Add(reportDataSource);
                }
                return list;
            }
           else
           {
               return null;
           }
        }

        public IEnumerable<ApproverDataSource> ApproversDataMapper(IEnumerable<MPS_MemoApproval> approversData)
        {
            if (approversData != null)
            {
                var list = new List<ApproverDataSource>();
                foreach (var approverData in approversData)
                {
                    var reportApproverDataSource = new ApproverDataSource() { };
                    reportApproverDataSource.ApproverId = approverData.Id;
                    reportApproverDataSource.ApproverSequence = approverData.ApproverSequence;
                    reportApproverDataSource.ApproverUsername = approverData.ApproverUserName ?? "";
                    reportApproverDataSource.ApproverFullName = approverData.ApproverFullName ?? "";
                    reportApproverDataSource.ApproverLevel = approverData.ApproverLevel ?? "";
                    reportApproverDataSource.ApproverPosition = approverData.Position ?? "";
                    var comment = approverData.MemoTable.Comments;
                    var approverCheck = approverData.ApproverLevel + " - Approval";
                    if (comment != null )
                    {
                        comment = comment.Where(m => m.Activity.ToLower() != "start").ToList();
                        var approve = comment.LastOrDefault(m => m.CreatedBy.Equals(approverData.ApproverUserName, StringComparison.InvariantCultureIgnoreCase) &&
                      m.Action.Equals(DocumentStatusFlag.Approve.ToString(), StringComparison.InvariantCultureIgnoreCase) &&
                      m.Activity.Equals(approverCheck, StringComparison.InvariantCultureIgnoreCase));
                        if (approve != null)
                        {
                            reportApproverDataSource.ApproveDateTime = approve.CreatedDate != null ? approve.CreatedDate.Value.ToString("dd/MM/yyyy") : "";
                        }
                        else
                        {
                            reportApproverDataSource.ApproveDateTime = "";
                        }
                    }
                    if (!string.IsNullOrEmpty(approverData.ApproverLevel))
                    {
                        if (approverData.ApproverLevel.Equals(Approval.ApproveLevelList.Propose.GetDescription()))
                            reportApproverDataSource.ApproverLevelKey = Approval.ApproveKey.Propose;
                        else if (approverData.ApproverLevel.Equals(Approval.ApproveLevelList.Accept.GetDescription()))
                            reportApproverDataSource.ApproverLevelKey = Approval.ApproveKey.Accept;
                        else if (approverData.ApproverLevel.Equals(Approval.ApproveLevelList.Approval.GetDescription()))
                            reportApproverDataSource.ApproverLevelKey = Approval.ApproveKey.Approval;
                        else if (approverData.ApproverLevel.Equals(Approval.ApproveLevelList.FinalApproval.GetDescription()))
                            reportApproverDataSource.ApproverLevelKey = Approval.ApproveKey.FinalApproval;
                        else
                        {
                            reportApproverDataSource.ApproverLevelKey = 0;
                        }
                    }
                    else
                    {
                        reportApproverDataSource.ApproverLevelKey = 0;
                    }

                    list.Add(reportApproverDataSource);
                }
                return list;
            }
            else
            {
                return null;
            }
        }

    }
}