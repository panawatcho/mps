﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TheMall.MPS.Mappers.Abstracts;
using TheMall.MPS.Models.CashAdvance;
using TheMall.MPS.Models.CashClearing;
using TheMall.MPS.ViewModels.CashAdvance;
using TheMall.MPS.ViewModels.CashClearing;

namespace TheMall.MPS.API.Mappers
{
    public interface ICashClearingLineMapper : ILineMapper
    {
        MPS_CashClearingLine ToModel(CashClearingLineViewModel viewModel, IEnumerable<MPS_CashClearingLine> lines);
        CashClearingLineViewModel ToViewModel(MPS_CashClearingLine model);
        IEnumerable<CashClearingLineViewModel> ToViewModels(IEnumerable<MPS_CashClearingLine> models);
        IEnumerable<MPS_CashClearingLine> ToModels(IEnumerable<CashClearingLineViewModel> viewModels, IEnumerable<MPS_CashClearingLine> lines);
        CashClearingLineViewModel ToViewModelFromCashAdvance(MPS_CashAdvanceLine model);
        IEnumerable<CashClearingLineViewModel> ToViewModelsFromCashAdvance(IEnumerable<MPS_CashAdvanceLine> models);
    }
    public class CashClearingLineMapper : BaseLineMapper, ICashClearingLineMapper
    {
        public MPS_CashClearingLine ToModel(CashClearingLineViewModel viewModel, IEnumerable<MPS_CashClearingLine> lines)
        {
            if (viewModel == null)
            {
                return null;
            }
            var model = InitLineTable(viewModel, lines);
            if (model == null)
            {
                return null;
            }
            model.Actual = viewModel.Actual;
            model.Amount = viewModel.Amount;
            model.Vat = viewModel.Vat;
            model.Tax = viewModel.Tax;
            model.Description = viewModel.Description;
            model.Remark = viewModel.Remark;
            model.NetAmount = viewModel.NetAmount;
            model.Unit = viewModel.Unit;
            model.NetActual = viewModel.NetActual;
            model.VatActual = viewModel.VatActual;
            model.TaxActual= viewModel.TaxActual;
            model.UnitActual = viewModel.UnitActual;
            model.PayeeName = viewModel.PayeeName;
            return model;
        }

        public CashClearingLineViewModel ToViewModel(MPS_CashClearingLine model)
        {
            if (model == null)
            {
                return null;
            }
           var viewModel = this.InitLineViewModel<CashClearingLineViewModel>(model); ;
           if (viewModel == null)
            {
                return null;
            }
            viewModel.Amount = model.Amount;
            viewModel.Tax = model.Tax;
            viewModel.Vat = model.Vat;
            viewModel.NetAmount = model.NetAmount;
            viewModel.Actual = (model.Actual == null) ? 0 : model.Actual; //
            viewModel.NetActual = model.NetActual;
            viewModel.VatActual = model.VatActual;
            viewModel.TaxActual = model.TaxActual;
            viewModel.UnitActual = model.UnitActual;
            if (model.Description != null)
            {
                viewModel.Description = model.Description;
            }
            else
            {
                viewModel.Description = "";
            }

            if (model.Remark != null)
            {
                viewModel.Remark = model.Remark;
            }
            else
            {
                viewModel.Remark = "";
            }

            viewModel.Unit = model.Unit;
            viewModel.NetActualNoVatTax = viewModel.UnitActual * viewModel.Actual;
            viewModel.PayeeName = model.PayeeName;
            return viewModel;
        }

        public IEnumerable<MPS_CashClearingLine> ToModels(IEnumerable<CashClearingLineViewModel> viewModels, 
            IEnumerable<MPS_CashClearingLine> lines)
        {
            return viewModels != null ? viewModels.Select(m => ToModel(m, lines)).Where(m => m != null) : Enumerable.Empty<MPS_CashClearingLine>();
        }

        public IEnumerable<CashClearingLineViewModel> ToViewModels(IEnumerable<MPS_CashClearingLine> models)
        {
            return models != null ? models.Select(ToViewModel) : Enumerable.Empty<CashClearingLineViewModel>();
        }

        public CashClearingLineViewModel ToViewModelFromCashAdvance(MPS_CashAdvanceLine model)
        {
            if (model == null)
            {
                return null;
            }
            
            var viewModel = this.InitLineViewModel<CashClearingLineViewModel>(model);
            if (viewModel == null)
            {
                return null;
            }
            viewModel.Id = 0;
            viewModel.Amount = model.Amount;
            viewModel.Tax = model.Tax;
            viewModel.Vat = model.Vat;
            viewModel.NetAmount = model.NetAmount;
            viewModel.NetActual = 0;
            viewModel.VatActual = 0;
            viewModel.TaxActual = 0;
            viewModel.UnitActual = 0;
            if (model.Description != null)
            {
                viewModel.Description = model.Description;
            }
            else
            {
                viewModel.Description = "";
            }

            if (model.Remark != null)
            {
                viewModel.Remark = model.Remark;
            }
            else
            {
                viewModel.Remark = "";
            }
            viewModel.Unit = model.Unit;
            return viewModel;
        }


        public IEnumerable<CashClearingLineViewModel> ToViewModelsFromCashAdvance(IEnumerable<MPS_CashAdvanceLine> models)
        {
            return models != null ? models.Select(ToViewModelFromCashAdvance) : Enumerable.Empty<CashClearingLineViewModel>();
        }


    }
}