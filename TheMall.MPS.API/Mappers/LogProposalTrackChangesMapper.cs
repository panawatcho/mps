﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TheMall.MPS.API.Services;
using TheMall.MPS.Models.ProposalLog;
using TheMall.MPS.ViewModels.ProposalLog;

namespace TheMall.MPS.API.Mappers
{
    public  interface ILogProposalTrackChangesMapper

    {
        LogProposalTrackChangesViewModel LogProposalTrackChangesToViewModel(MPS_Log_ProposalTrackChanges logTracking);
        IEnumerable<LogProposalTrackChangesViewModel> ToViewModels(IEnumerable<MPS_Log_ProposalTrackChanges> models);
    }

    public class LogProposalTrackChangesMapper : ILogProposalTrackChangesMapper
    {

        public LogProposalTrackChangesViewModel LogProposalTrackChangesToViewModel(MPS_Log_ProposalTrackChanges logTracking)
        {
            if (logTracking == null)
                return null;
            var logTrackingViewModel = new LogProposalTrackChangesViewModel();
                logTrackingViewModel.ProposalId = logTracking.ProposalId;
                logTrackingViewModel.ProposalLogId = logTracking.ProposalLogId;
                logTrackingViewModel.RefTableName = logTracking.RefTableName;
                logTrackingViewModel.RefTableDesc = logTracking.RefTableDesc;
                logTrackingViewModel.RefTableId = logTracking.RefTableId;
                logTrackingViewModel.RefLogId = logTracking.RefLogId;
                logTrackingViewModel.FieldName = logTracking.FieldName;
                logTrackingViewModel.FieldDescription = logTracking.FieldDescription;
                logTrackingViewModel.State = logTracking.State;
                logTrackingViewModel.OldValue = logTracking.OldValue;
                logTrackingViewModel.NewValue = logTracking.NewValue;
                logTrackingViewModel.CreatedDate = logTracking.CreatedDate;
            return logTrackingViewModel;
        }
        public IEnumerable<LogProposalTrackChangesViewModel> ToViewModels(IEnumerable<MPS_Log_ProposalTrackChanges> models)
        {
            return models != null ? models.Select(LogProposalTrackChangesToViewModel) : Enumerable.Empty<LogProposalTrackChangesViewModel>();
        }
    }
}