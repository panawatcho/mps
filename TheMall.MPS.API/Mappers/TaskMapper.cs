﻿using System.Collections.Generic;
using System.Linq;
using TheMall.MPS.Models.K2;
using TheMall.MPS.ViewModels.Task;

namespace TheMall.MPS.API.Mappers
{
    public static class TaskMapper
    {
        public static ProcInstStatusViewModel ToViewModel(this ProcInstStatus model)
        {
            if (model == null)
            {
                return null;
            }
            var viewModel = new ProcInstStatusViewModel();

            viewModel.ProcInstId = model.ProcInstId;
            viewModel.Folio = model.Folio;
            viewModel.ProcDescription = model.ProcDescription;
            viewModel.ActInsts = model.ActInsts.ToViewModel();

            return viewModel;
        }

        public static IEnumerable<ProcInstStatusViewModel> ToViewModel(this IEnumerable<ProcInstStatus> models)
        {
            if (models == null)
            {
                return Enumerable.Empty<ProcInstStatusViewModel>();
            }

            return models.Select(ToViewModel);
        }

        public static ActInstStatusViewModel ToViewModel(this ActInstStatus model)
        {
            if (model == null)
            {
                return null;
            }

            var viewModel = new ActInstStatusViewModel();

            viewModel.ActInstId = model.ActInstId;
            viewModel.ActivityName = model.ActivityName;
            viewModel.ActivityDescription = model.ActivityDescription;
            viewModel.ActInstDestId = model.ActInstDestId;
            viewModel.ActInstDestFqn = model.ActInstDestFqn;
            viewModel.EventExpectedDuration = model.EventExpectedDuration;
            viewModel.EventName = model.EventName;
            viewModel.EventStartDate = model.EventStartDate;
            viewModel.EventDueDate = model.EventDueDate;

            return viewModel;
        }

        public static IEnumerable<ActInstStatusViewModel> ToViewModel(this IEnumerable<ActInstStatus> models)
        {
            if (models == null)
            {
                return Enumerable.Empty<ActInstStatusViewModel>();
            }

            return models.Select(ToViewModel);
        }
    }
}