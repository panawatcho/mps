﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using TheMall.MPS.API.Services;
using TheMall.MPS.Models.Master;
using TheMall.MPS.ViewModels;
using TheMall.MPS.ViewModels.Master;

namespace TheMall.MPS.API.Mappers
{
    public class UnitCodeForEmployeeMapperProfile : AutoMapper.Profile
    {
        public UnitCodeForEmployeeMapperProfile()
        {
            CreateMap<MPS_M_UnitCodeForEmployee, UnitCodeForEmployeeViewModel>()
                .ReverseMap();
        }
    }
    public static class UnitCodeForEmployeeMapper
    {
        public static MPS_M_UnitCodeForEmployee ToModel(this UnitCodeForEmployeeViewModel viewModel)
        {
            if (viewModel == null)
                return null;
            var model = new MPS_M_UnitCodeForEmployee();
            if (viewModel.EmpId == null)
            {
                if (viewModel.Employee != null)
                {
                    model.EmpId = viewModel.Employee.EmpId;
                }
            }
            else
            {
                model.EmpId = viewModel.EmpId;
            }

            if(viewModel.UnitCode != null)
             {
                 model.UnitCode = viewModel.UnitCode.UnitCode;
                 model.UnitName =  viewModel.UnitCode.UnitName;
             }

            if (viewModel.Employee != null)
            {
                model.Username = viewModel.Employee.Username;
            }
          
               // UnitCode = viewModel.UnitCode,
               // UnitName = viewModel.UnitName,
               // EmpId = viewModel.EmpId,
               // Username = viewModel.Username,
            model.InActive = viewModel.InActive;
            model.Order = viewModel.Order;
            return model;
            //return Mapper.Map<MPS_M_UnitCodeForEmployee>(viewModel);
        }

        public static UnitCodeForEmployeeViewModel ToViewModel(this MPS_M_UnitCodeForEmployee model,int id = 0)
        {
            if (model == null)
                return null;

            var viewModel = new UnitCodeForEmployeeViewModel
            {
                Id = id,
                EmpId = model.EmpId,
                Employee = model.Employee.ToViewModel(),
                UnitCode = new UnitCodeViewModel
                {
                    UnitCode = model.UnitCode,
                    UnitName = model.UnitName
                },
                InActive = model.InActive,
                Order = model.Order
            };


            //var username = new EmployeeViewModel();
            //viewModel.Username = new EmployeeViewModel()
            //{
            //    Username = model.Username;
            //    FullName = model. 
            //};
            //if (model.UnitCode != null)
            //{

            //}
            //{
                //UnitCode = model.UnitCode,
              //  UnitName = model.UnitName,
              //  EmpId = model.EmpId,
              ////  Username = model.Username,
              //  InActive = model.InActive
            //};
            return viewModel;
           // return Mapper.Map<UnitCodeForEmployeeViewModel>(model);
        }
        public static IEnumerable<UnitCodeForEmployeeViewModel> ToViewModels(this IEnumerable<MPS_M_UnitCodeForEmployee> models)
        {
            var i = 1;
            return models != null ? models.Select(m => ToViewModel(m, i++)) : Enumerable.Empty<UnitCodeForEmployeeViewModel>();
        }
        public static IEnumerable<MPS_M_UnitCodeForEmployee> ToModels(IEnumerable<UnitCodeForEmployeeViewModel> viewModels, IEnumerable<MPS_M_UnitCodeForEmployee> lines)
        {
            return viewModels != null ? viewModels.Select(ToModel) : Enumerable.Empty<MPS_M_UnitCodeForEmployee>();
        }

    }
}