﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using TheMall.MPS.API.Mappers.Abstracts;
using TheMall.MPS.API.Services;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Mappers.Abstracts;
using TheMall.MPS.Models.Proposal;
using TheMall.MPS.ViewModels.Proposal;

namespace TheMall.MPS.API.Mappers
{
    public interface IProposalIncomeTotalSaleTemplateMapper 
    {
        MPS_IncomeTotalSaleTemplate ToModel(IncomeTotalSaleTemplateViewModel viewModel);
        IncomeTotalSaleTemplateViewModel ToViewModel(MPS_IncomeTotalSaleTemplate model);
        IEnumerable<MPS_IncomeTotalSaleTemplate> ToModels(IEnumerable<IncomeTotalSaleTemplateViewModel> viewModels);
        IEnumerable<IncomeTotalSaleTemplateViewModel> ToViewModels(IEnumerable<MPS_IncomeTotalSaleTemplate> models);
        IEnumerable<MPS_IncomeTotalSaleTemplate> ToIncomeTotalSaleTemplateModel(IEnumerable<ProposalPercentSharedViewModel> percentSharedViewModels, IEnumerable<MPS_IncomeTotalSaleTemplate> lineAp);
    }

    public class ProposalIncomeTotalSaleTemplateMapper : IProposalIncomeTotalSaleTemplateMapper
    {
        private readonly IProposalIncomeTotalSaleService _service;

        public ProposalIncomeTotalSaleTemplateMapper(IProposalIncomeTotalSaleService service) 
        {
            _service = service;
        }

       
        public MPS_IncomeTotalSaleTemplate ToModel(IncomeTotalSaleTemplateViewModel viewModel)
        {
           if (viewModel == null)
           {
               return null;
           }
           var model = new MPS_IncomeTotalSaleTemplate();
           model.Actual = viewModel.Actual;
           model.ActualAmount = viewModel.ActualAmount;
           model.PercentOfDepartment = viewModel.PercentOfDepartment;
           model.BranchCode = viewModel.BranchCode;
           model.IncomeTotalSaleId = viewModel.IncomeTotalSaleId;
           model.Id = viewModel.Id;


           return model;
        }

        public IncomeTotalSaleTemplateViewModel ToViewModel(MPS_IncomeTotalSaleTemplate model)
        {
           if (model == null)
           {
               return null;
           }
           var viewModel = new IncomeTotalSaleTemplateViewModel()
           {
               ActualAmount = model.ActualAmount,
               Actual = model.Actual,
               BranchCode = model.BranchCode,
               IncomeTotalSaleId = model.IncomeTotalSaleId,
               Id = model.Id

           };

           return viewModel;
        }

        public IEnumerable<MPS_IncomeTotalSaleTemplate> ToModels(IEnumerable<IncomeTotalSaleTemplateViewModel> viewModels)
        {
            return viewModels != null ? viewModels.Select(ToModel) : Enumerable.Empty<MPS_IncomeTotalSaleTemplate>();
        }

        public IEnumerable<IncomeTotalSaleTemplateViewModel> ToViewModels(IEnumerable<MPS_IncomeTotalSaleTemplate> models)
        {
            return models != null ? models.Select(ToViewModel) : Enumerable.Empty<IncomeTotalSaleTemplateViewModel>();
        }

        public IEnumerable<MPS_IncomeTotalSaleTemplate> ToIncomeTotalSaleTemplateModel(IEnumerable<ProposalPercentSharedViewModel> percentSharedViewModels, IEnumerable<MPS_IncomeTotalSaleTemplate> linetemplate)
        {
            var list = new List<MPS_IncomeTotalSaleTemplate>();
            foreach (var percentShared in percentSharedViewModels)
            {
                if (percentShared.lineId <= 0)
                {
                    var incomTotalSaleTemplate = new MPS_IncomeTotalSaleTemplate()
                    {
                        BranchCode = percentShared.BranchCode,
                        Actual = percentShared.Actual,
                        ActualAmount = percentShared.ActualAmount
                    

                    };
                    list.Add(incomTotalSaleTemplate);
                }
                else
                {
                    if (linetemplate != null)
                    {
                        var incomTotalSaleTemplate = linetemplate.LastOrDefault(m => m.Id == percentShared.lineId);
                        if (incomTotalSaleTemplate != null)
                        {
                            incomTotalSaleTemplate.BranchCode = percentShared.BranchCode;
                            incomTotalSaleTemplate.Actual = percentShared.Actual;
                            incomTotalSaleTemplate.ActualAmount = percentShared.ActualAmount;
                          
                            list.Add(incomTotalSaleTemplate);
                        }
                        else
                        {
                            incomTotalSaleTemplate = new MPS_IncomeTotalSaleTemplate()
                            {
                                BranchCode = percentShared.BranchCode,
                                Actual = percentShared.Actual,
                                ActualAmount = percentShared.ActualAmount
                               
                            };
                            list.Add(incomTotalSaleTemplate);
                        }
                    }


                }
            }
            return list;
        }
    }
}