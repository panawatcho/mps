﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using TheMall.MPS.Models.Proposal;
using TheMall.MPS.Models.ProposalLog;

namespace TheMall.MPS.API.Mappers
{
    public class ProposalLogMapperProfile : AutoMapper.Profile
    {
        public ProposalLogMapperProfile()
        {
            CreateMap<MPS_ProposalTable, MPS_Log_ProposalTable>().IgnoreAllVirtual()
                .ReverseMap().IgnoreAllVirtual();

            CreateMap<MPS_ProposalLine, MPS_Log_ProposalLine>().IgnoreAllVirtual()
                .ReverseMap().IgnoreAllVirtual();

            CreateMap<MPS_ProposalLineTemplate, MPS_Log_ProposalLineTemplate>().IgnoreAllVirtual()
               .ReverseMap().IgnoreAllVirtual();

            CreateMap<MPS_ProposalLineSharedTemplate, MPS_Log_ProposalLineSharedTemplate>().IgnoreAllVirtual()
               .ReverseMap().IgnoreAllVirtual();

            CreateMap<MPS_ProposalApproval, MPS_Log_ProposalApproval>().IgnoreAllVirtual()
               .ReverseMap().IgnoreAllVirtual();

            CreateMap<MPS_DepositLine, MPS_Log_DepositLine>().IgnoreAllVirtual()
               .ReverseMap().IgnoreAllVirtual();

            CreateMap<MPS_IncomeDeposit, MPS_Log_IncomeDeposit>().IgnoreAllVirtual()
               .ReverseMap().IgnoreAllVirtual();

            CreateMap<MPS_IncomeOther, MPS_Log_IncomeOther>().IgnoreAllVirtual()
               .ReverseMap().IgnoreAllVirtual();

            CreateMap<MPS_AccountTrans, MPS_Log_AccountTrans>().IgnoreAllVirtual()
              .ReverseMap().IgnoreAllVirtual();

            CreateMap<MPS_IncomeTotalSale, MPS_Log_IncomeTotalSale>().IgnoreAllVirtual()
               .ReverseMap().IgnoreAllVirtual();

            CreateMap<MPS_IncomeTotalSaleTemplate, MPS_Log_IncomeTotalSaleTemplate>().IgnoreAllVirtual()
              .ReverseMap().IgnoreAllVirtual();
        }
    }

    public interface IProposalLogMapper
    {
        MPS_Log_ProposalTable ToLogProposalTable(MPS_ProposalTable model);
        MPS_Log_ProposalLine ToLogProposalLine(MPS_ProposalLine model);
        MPS_Log_ProposalLineTemplate ToLogProposalLineTemplate(MPS_ProposalLineTemplate model);
        MPS_Log_ProposalLineSharedTemplate ToLogProposalLineSharedTemplate(MPS_ProposalLineSharedTemplate model);
        MPS_Log_ProposalApproval ToLogProposalApproval(MPS_ProposalApproval model);
        MPS_Log_DepositLine ToLogDepositLine(MPS_DepositLine model);
        MPS_Log_IncomeDeposit ToLogIncomeDeposit(MPS_IncomeDeposit model);
        MPS_Log_IncomeOther ToLogIncomeOther(MPS_IncomeOther model);
        MPS_Log_AccountTrans ToLogAccountTrans(MPS_AccountTrans model);
        MPS_Log_IncomeTotalSale ToLogIncomeTotalSale(MPS_IncomeTotalSale model);
        MPS_Log_IncomeTotalSaleTemplate ToLogIncomeTotalSaleTemplate(MPS_IncomeTotalSaleTemplate model);
        // IEnumerable<MPS_Log_ProposalLine> ToLogProposalLines(IEnumerable<MPS_ProposalLine> models);
        MPS_ProposalTable ToProposalTable(MPS_Log_ProposalTable log);
        MPS_ProposalLine ToProposalLine(MPS_Log_ProposalLine log);
        MPS_ProposalLineTemplate ToProposalLineTemplate(MPS_Log_ProposalLineTemplate log);
        MPS_ProposalApproval ToProposalApproval(MPS_Log_ProposalApproval log);
    }

    public class ProposalLogMapper : IProposalLogMapper
    {
        public MPS_Log_ProposalTable ToLogProposalTable(MPS_ProposalTable model)
        {
            var log = new MPS_Log_ProposalTable();
            AutoMapper.Mapper.Map(model, log);
            log.ProposalId = log.Id;
            log.Id = 0;
            log.LogCreatedBy = ((ClaimsPrincipal)System.Threading.Thread.CurrentPrincipal).Identity.Name;
            log.LogCreatedDate = DateTime.UtcNow;
            return log;
        }
        public MPS_Log_ProposalLine ToLogProposalLine(MPS_ProposalLine model)
        {
            var log = new MPS_Log_ProposalLine();
            AutoMapper.Mapper.Map(model, log);
            log.ProposalLineId = log.Id;
            log.Id = 0;
            log.ParentId = 0;
            log.LogCreatedBy = ((ClaimsPrincipal)System.Threading.Thread.CurrentPrincipal).Identity.Name;
            log.LogCreatedDate = DateTime.UtcNow;
            return log;
        }
        public MPS_Log_ProposalLineTemplate ToLogProposalLineTemplate(MPS_ProposalLineTemplate model)
        {
            var log = new MPS_Log_ProposalLineTemplate();
            AutoMapper.Mapper.Map(model, log);
            log.ProposalLineTemplateId = log.Id;
            log.Id = 0;
            log.ProposalLineId = 0;
            log.LogCreatedBy = ((ClaimsPrincipal)System.Threading.Thread.CurrentPrincipal).Identity.Name;
            log.LogCreatedDate = DateTime.UtcNow;
            return log;
        }
        public MPS_Log_ProposalLineSharedTemplate ToLogProposalLineSharedTemplate(MPS_ProposalLineSharedTemplate model)
        {
            var log = new MPS_Log_ProposalLineSharedTemplate();
            AutoMapper.Mapper.Map(model, log);
            log.ProposalLineSharedTemplateId = log.Id;
            log.Id = 0;
            log.ParentId = 0;
            log.LogCreatedBy = ((ClaimsPrincipal)System.Threading.Thread.CurrentPrincipal).Identity.Name;
            log.LogCreatedDate = DateTime.UtcNow;
            return log;
        }
        public MPS_Log_ProposalApproval ToLogProposalApproval(MPS_ProposalApproval model)
        {
            var log = new MPS_Log_ProposalApproval();
            AutoMapper.Mapper.Map(model, log);
            log.ProposalApprovalId = log.Id;
            log.Id = 0;
            log.ParentId = 0;
            log.LogCreatedBy = ((ClaimsPrincipal)System.Threading.Thread.CurrentPrincipal).Identity.Name;
            log.LogCreatedDate = DateTime.UtcNow;
            return log;
        }
        public MPS_Log_DepositLine ToLogDepositLine(MPS_DepositLine model)
        {
            var log = new MPS_Log_DepositLine();
            AutoMapper.Mapper.Map(model, log);
            log.DepositLineId = log.Id;
            log.Id = 0;
            log.ParentId = 0;
            log.LogCreatedBy = ((ClaimsPrincipal)System.Threading.Thread.CurrentPrincipal).Identity.Name;
            log.LogCreatedDate = DateTime.UtcNow;
            return log;
        }
        public MPS_Log_IncomeDeposit ToLogIncomeDeposit(MPS_IncomeDeposit model)
        {
            var log = new MPS_Log_IncomeDeposit();
            AutoMapper.Mapper.Map(model, log);
            log.IncomeDepositId = log.Id;
            log.Id = 0;
            log.ParentId = 0;
            log.LogCreatedBy = ((ClaimsPrincipal)System.Threading.Thread.CurrentPrincipal).Identity.Name;
            log.LogCreatedDate = DateTime.UtcNow;
            return log;
        }
        public MPS_Log_IncomeOther ToLogIncomeOther(MPS_IncomeOther model)
        {
            var log = new MPS_Log_IncomeOther();
            AutoMapper.Mapper.Map(model, log);
            log.IncomeOtherId = log.Id;
            log.Id = 0;
            log.ParentId = 0;
            log.LogCreatedBy = ((ClaimsPrincipal)System.Threading.Thread.CurrentPrincipal).Identity.Name;
            log.LogCreatedDate = DateTime.UtcNow;
            return log;
        }
        public MPS_Log_AccountTrans ToLogAccountTrans(MPS_AccountTrans model)
        {
            var log = new MPS_Log_AccountTrans();
            AutoMapper.Mapper.Map(model, log);
            log.AccountTransId = log.Id;
            log.Id = 0;
            log.IncomeOtherId = 0;
            log.LogCreatedBy = ((ClaimsPrincipal)System.Threading.Thread.CurrentPrincipal).Identity.Name;
            log.LogCreatedDate = DateTime.UtcNow;
            return log;
        }
        public MPS_Log_IncomeTotalSale ToLogIncomeTotalSale(MPS_IncomeTotalSale model)
        {
            var log = new MPS_Log_IncomeTotalSale();
            AutoMapper.Mapper.Map(model, log);
            log.IncomeTotalSaleId = log.Id;
            log.Id = 0;
            log.ParentId = 0;
            log.LogCreatedBy = ((ClaimsPrincipal)System.Threading.Thread.CurrentPrincipal).Identity.Name;
            log.LogCreatedDate = DateTime.UtcNow;
            return log;
        }
        public MPS_Log_IncomeTotalSaleTemplate ToLogIncomeTotalSaleTemplate(MPS_IncomeTotalSaleTemplate model)
        {
            var log = new MPS_Log_IncomeTotalSaleTemplate();
            AutoMapper.Mapper.Map(model, log);
            log.IncomeTotalSaleTemplateId = log.Id;
            log.Id = 0;
            log.IncomeTotalSaleId = 0;
            log.LogCreatedBy = ((ClaimsPrincipal)System.Threading.Thread.CurrentPrincipal).Identity.Name;
            log.LogCreatedDate = DateTime.UtcNow;
            return log;
        }
        //public IEnumerable<MPS_Log_ProposalLine> ToLogProposalLines(IEnumerable<MPS_ProposalLine> models)
        //{
        //    return models != null ? models.Select(m => ToLogProposalLine(m)) : Enumerable.Empty<MPS_Log_ProposalLine>();
        //}

        public MPS_ProposalTable ToProposalTable(MPS_Log_ProposalTable log)
        {
            var model = new MPS_ProposalTable();
            AutoMapper.Mapper.Map(log, model);
            model.Id = log.ProposalId;
            return model;
        }
        public MPS_ProposalLine ToProposalLine(MPS_Log_ProposalLine log)
        {
            var model = new MPS_ProposalLine();
            AutoMapper.Mapper.Map(log, model);
            model.Id = log.ProposalLineId ?? 0;
            return model;
        }

        public MPS_ProposalLineTemplate ToProposalLineTemplate(MPS_Log_ProposalLineTemplate log)
        {
            var model = new MPS_ProposalLineTemplate();
            AutoMapper.Mapper.Map(log, model);
            model.Id = log.ProposalLineTemplateId ?? 0;
            return model;
        }

        public MPS_ProposalApproval ToProposalApproval(MPS_Log_ProposalApproval log)
        {
            var model = new MPS_ProposalApproval();
            AutoMapper.Mapper.Map(log, model);
            model.Id = log.ProposalApprovalId ?? 0;
            return model;
        }

    }
}