﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CrossingSoft.Framework.Models.Enums;
using CrossingSoft.Framework.Models.Interfaces;
using TheMall.MPS.API.Services;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Mappers.Abstracts;
using TheMall.MPS.Models.Interfaces;
using TheMall.MPS.Models.Master;
using TheMall.MPS.ViewModels;
using TheMall.MPS.ViewModels.Interfaces;
using TheMall.MPS.ViewModels.Master;


namespace TheMall.MPS.API.Mappers.Abstracts
{
    public interface IWorkflowTableMapper<out TWorkflowTable> : ILineMapper
    where TWorkflowTable : class, IMpsWorkflowTable, new()
    {
        T InitViewModel<T>(IMpsWorkflowTable workflowTable)
            where T : class, IMpsWorkflowViewModel, new();

        TWorkflowTable InitWorkflowTable(IMpsWorkflowViewModel viewModel);
    }
    public abstract class BaseWorkflowTableMapper<TWorkflowTable> : BaseLineMapper, IWorkflowTableMapper<TWorkflowTable>
         where TWorkflowTable : class, IMpsWorkflowTable ,new()
    {
        private readonly IMpsWorkflowTableService<TWorkflowTable> _workflowTableService;
        private readonly IEmployeeTableService _employeeTableService;
        private readonly IPlaceService _placeService;
        private readonly IAccountingService _accountingService;
        protected BaseWorkflowTableMapper(
           IMpsWorkflowTableService<TWorkflowTable> workflowTableService, 
            IEmployeeTableService employeeTableService, 
            IPlaceService placeService, IAccountingService accountingService)
        {
            _workflowTableService = workflowTableService;
            _employeeTableService = employeeTableService;
            _placeService = placeService;
            _accountingService = accountingService;
        }


        public T InitViewModel<T>(IMpsWorkflowTable workflowTable)
           where T : class, IMpsWorkflowViewModel, new()
        {
            var viewModel = new T();

            viewModel.Id = workflowTable.Id;
            viewModel.StatusFlag = workflowTable.StatusFlag;
            viewModel.Title = workflowTable.Title;
            viewModel.DocumentNumber = workflowTable.DocumentNumber;
            viewModel.Revision = workflowTable.Revision;
            viewModel.ProcInstId = workflowTable.ProcInstId;
            viewModel.Status = workflowTable.Status;
            viewModel.DocumentStatus = workflowTable.DocumentStatus;
            viewModel.CreatedDate = workflowTable.CreatedDate;
            viewModel.DocumentDate = workflowTable.DocumentDate;
            viewModel.SubmittedDate = workflowTable.SubmittedDate;
            viewModel.CompletedDate = workflowTable.CompletedDate;
            viewModel.CancelledDate = workflowTable.CancelledDate;
            viewModel.CreatedBy = workflowTable.CreatedBy;
            viewModel.CreatedByName = workflowTable.CreatedByName;
            viewModel.Requester = _employeeTableService.FindByUsername(workflowTable.RequesterUserName).ToViewModel();
            var unitcode = new MPS_M_UnitCode()
            {
                UnitCode = workflowTable.UnitCode,
                UnitName = workflowTable.UnitName,
            };
            viewModel.UnitCode = unitcode.ToViewModel();
           
            //var listAccounting = new List<EmployeeViewModel>();
            //if (!string.IsNullOrEmpty(workflowTable.AccountingUsername))
            //{
            //    string[] accounting = workflowTable.AccountingUsername.TrimEnd(';').Split(';');
            //    listAccounting.AddRange(accounting.Select(acc => _employeeTableService.FindByUsername(acc).ToViewModel()));
            //}
            //viewModel.Accounting = (listAccounting.Any()) ? listAccounting : null;

            if (workflowTable.AccountingBranch != null)
            {
                viewModel.Accounting = _placeService.FirstOrDefault(m => m.PlaceCode.Equals(workflowTable.AccountingBranch)).ToViewModel();

                // get accounting User
                var accountingUsers =
                    _accountingService.GetWhere(m => m.BranchCode.Equals(workflowTable.AccountingBranch));
                if (accountingUsers != null)
                {
                    var user = new List<EmployeeViewModel>();
                    foreach (var accountingLine in accountingUsers)
                    {
                        var lines = accountingLine;
                        var line = _employeeTableService.FirstOrDefault(m => m.EmpId.Equals(lines.EmpId) && m.EmpStatus == "Active").ToViewModel();
                        if (line != null) {
                            user.Add(line);
                        }                       
                       //user.Add(line);
                    }
                    viewModel.AccountingUsers = user;
                }
            }

            var listMailCC = new List<MailInformViewModel>();
            if (!string.IsNullOrEmpty(workflowTable.CCEmail))
            {
                string[] mailCC = workflowTable.CCEmail.TrimEnd(';').Split(';');
                string[] nameCC = workflowTable.CCUsername.TrimEnd(';').Split(';');
                for (int i = 0; i < mailCC.Length; i++)
                {
                    var newMail = new MailInformViewModel();
                    newMail.Name = nameCC[i];
                    newMail.Email = mailCC[i];
                    listMailCC.Add(newMail);
                }
            }
            viewModel.CCEmail = listMailCC ?? null;
            
            var listMailInform = new List<MailInformViewModel>();
            if (!string.IsNullOrEmpty(workflowTable.InformEmail))
            {
                string[] mailInform = workflowTable.InformEmail.TrimEnd(';').Split(';');
                string[] nameInform = workflowTable.InformUsername.TrimEnd(';').Split(';');
                for (int i = 0; i < mailInform.Length; i++)
                {
                    var newMail = new MailInformViewModel();
                    newMail.Name = nameInform[i];
                    newMail.Email = mailInform[i];
                    listMailInform.Add(newMail);
                }
            }
            viewModel.InformEmail = listMailInform ?? null;

            viewModel.DueDatePropose = (workflowTable.DueDatePropose.HasValue)?workflowTable.DueDatePropose.Value.ToUniversalTime():workflowTable.DueDatePropose;
            viewModel.DueDateAccept = (workflowTable.DueDateAccept.HasValue) ? workflowTable.DueDateAccept.Value.ToUniversalTime() : workflowTable.DueDateAccept;
            viewModel.DueDateApprove = (workflowTable.DueDateApprove.HasValue) ? workflowTable.DueDateApprove.Value.ToUniversalTime() : workflowTable.DueDateApprove;
            viewModel.DueDateFinalApprove = (workflowTable.DueDateFinalApprove.HasValue) ? workflowTable.DueDateFinalApprove.Value.ToUniversalTime() : workflowTable.DueDateFinalApprove;
            //viewModel.AccoutingBranch = workflowTable.AccountingBranch;<- ไม่ใช้อันนี้แล้ว
            viewModel.RePrintNo = workflowTable.RePrintNo;
            return viewModel;
        }
        
       

        public TWorkflowTable InitWorkflowTable(IMpsWorkflowViewModel viewModel)
        {
            TWorkflowTable model = null;
            if (viewModel.Id > 0)
            {
                model = _workflowTableService.GetWorkflowTable(viewModel.Id);

                if (model == null)
                {
                    throw new HttpException(404, viewModel.Id.ToString());
                    //throw HttpExceptionHelper.DataWithIDNotExists(viewModel.Id);
                }
                model.Title = viewModel.Title;
                model.DocumentDate = viewModel.DocumentDate;
                model.DocumentStatus =  (string.IsNullOrEmpty(viewModel.DocumentNumber))?DocumentStatus.Draft.ToString():viewModel.DocumentStatus;
                model.Status = (string.IsNullOrEmpty(viewModel.DocumentNumber)) ? DocumentStatus.Draft.ToString() : viewModel.Status;
                if(viewModel.UnitCode!=null)
                {
                    model.UnitCode = viewModel.UnitCode.UnitCode;
                    model.UnitName= viewModel.UnitCode.UnitName;
                }
              
                //if (viewModel.Accounting != null)
                //{
                //    var tempAccounting = string.Empty;
                //    var tempAccountingEmail = string.Empty;
                //    foreach (var acc in viewModel.Accounting)
                //    {
                //        tempAccounting += string.Concat(acc.Username, ";").Trim();
                //        tempAccountingEmail += string.Concat(acc.Email, ";").Trim();
                //    }
                //    model.AccountingUsername = (!string.IsNullOrEmpty(tempAccounting)) ? tempAccounting.TrimEnd(';') : null;
                //    model.AccountingEmail = (!string.IsNullOrEmpty(tempAccountingEmail)) ? tempAccountingEmail.TrimEnd(';') : null; 
                //}
                if (viewModel.Accounting != null)
                {
                    model.AccountingBranch = viewModel.Accounting.PlaceCode;
                }
                //////////

                if (viewModel.CCEmail != null)
                {
                    var tempCCusername = string.Empty;
                    var tempCCEmail = string.Empty;
                    foreach (var acc in viewModel.CCEmail)
                    {
                        if (!string.IsNullOrEmpty(acc.Email))
                        {
                            tempCCusername += string.Concat(acc.Name, ";").Trim();
                            tempCCEmail += string.Concat(acc.Email, ";").Trim();
                        }
                      
                    }
                    model.CCUsername = (!string.IsNullOrEmpty(tempCCusername)) ? tempCCusername.TrimEnd(';') : null;
                    model.CCEmail = (!string.IsNullOrEmpty(tempCCEmail)) ? tempCCEmail.TrimEnd(';') : null;
                }
                if (viewModel.InformEmail != null)
                {
                    var tempInusername = string.Empty;
                    var tempInEmail = string.Empty;
                    foreach (var acc in viewModel.InformEmail)
                    {
                        if (!string.IsNullOrEmpty(acc.Email))
                        {
                            tempInusername += string.Concat(acc.Name, ";").Trim();
                            tempInEmail += string.Concat(acc.Email, ";").Trim();
                        }
                    }
                    model.InformUsername = (!string.IsNullOrEmpty(tempInusername)) ? tempInusername.TrimEnd(';') : null;
                    model.InformEmail = (!string.IsNullOrEmpty(tempInEmail)) ? tempInEmail.TrimEnd(';') : null;
                }
                //Propose
                if (viewModel.DueDatePropose.HasValue)
                {
                    model.DueDatePropose = viewModel.DueDatePropose;
                }else
                {
                    model.DueDatePropose = DateTime.Now.AddDays(2).ToLocalTime().Date;
                }

                //Accept
                if (viewModel.DueDateAccept.HasValue)
                {
                    model.DueDateAccept = viewModel.DueDateAccept;
                }
                else
                {
                    model.DueDateAccept = DateTime.Now.AddDays(2).ToLocalTime().Date;
                }
                //Approver
                if (viewModel.DueDateApprove.HasValue)
                {
                    model.DueDateApprove = viewModel.DueDateApprove;
                }
                else
                {
                    model.DueDateApprove = DateTime.Now.AddDays(2).ToLocalTime().Date;
                }
                //Final Approver
                if (viewModel.DueDateFinalApprove.HasValue)
                {
                    model.DueDateFinalApprove = viewModel.DueDateFinalApprove;
                }
                else
                {
                    model.DueDateFinalApprove = DateTime.Now.AddDays(2).ToLocalTime().Date;
                }
              
                model.RePrintNo = viewModel.RePrintNo;
            }
            else
            {
                model = new TWorkflowTable();
                model.Id = viewModel.Id;
                model.Title = viewModel.Title;
                model.DocumentNumber = viewModel.DocumentNumber;
                model.Revision = viewModel.Revision;
                model.Status = (string.IsNullOrEmpty(viewModel.DocumentNumber)) ? DocumentStatus.Draft.ToString() : viewModel.Status;
                model.DocumentStatus = (string.IsNullOrEmpty(viewModel.DocumentNumber)) ? DocumentStatus.Draft.ToString() : viewModel.DocumentStatus;
                model.DocumentDate = viewModel.DocumentDate;
                model.StatusFlag = 0;
                if (viewModel.Requester!=null)
                {
                    model.RequesterName = string.Format("{0} {1}", viewModel.Requester.ThFName, viewModel.Requester.ThLName);
                    model.RequesterUserName = viewModel.Requester.Username;
                }
              
                //model.RequesterName = viewModel.Employee.RequesterThFName;
                //model.RequesterUserName = (viewModel.Employee!=null)?viewModel.Employee.Username:"";
                if (viewModel.UnitCode != null)
                {
                    model.UnitCode = viewModel.UnitCode.UnitCode;
                    model.UnitName = viewModel.UnitCode.UnitName;
                   
                }

                //if (viewModel.Accounting != null)
                //{
                //    var tempAccounting = string.Empty;
                //    var tempAccountingEmail = string.Empty;
                //    foreach (var acc in viewModel.Accounting)
                //    {
                //        tempAccounting += string.Concat(acc.Username, ";").Trim();
                //        tempAccountingEmail += string.Concat(acc.Email, ";").Trim();
                //    }
                //    model.AccountingUsername = (!string.IsNullOrEmpty(tempAccounting)) ? tempAccounting.TrimEnd(';') : null;
                //    model.AccountingEmail = (!string.IsNullOrEmpty(tempAccountingEmail)) ? tempAccountingEmail.TrimEnd(';') : null; 
                //}
                if (viewModel.Accounting != null)
                {
                    model.AccountingBranch = viewModel.Accounting.PlaceCode;
                }
                ///////////////////////////////////

                if (viewModel.CCEmail != null)
                {
                    var tempCCusername = string.Empty;
                    var tempCCEmail = string.Empty;
                    foreach (var acc in viewModel.CCEmail)
                    {
                        tempCCusername += string.Concat(acc.Name, ";").Trim();
                        tempCCEmail += string.Concat(acc.Email, ";").Trim();
                    }
                    model.CCUsername = (!string.IsNullOrEmpty(tempCCusername)) ? tempCCusername.TrimEnd(';') : null;
                    model.CCEmail = (!string.IsNullOrEmpty(tempCCEmail)) ? tempCCEmail.TrimEnd(';') : null;
                }
                if (viewModel.InformEmail != null)
                {
                    var tempInusername = string.Empty;
                    var tempInEmail = string.Empty;
                    foreach (var acc in viewModel.InformEmail)
                    {
                        tempInusername += string.Concat(acc.Name, ";").Trim();
                        tempInEmail += string.Concat(acc.Email, ";").Trim();
                    }
                    model.InformUsername = (!string.IsNullOrEmpty(tempInusername)) ? tempInusername.TrimEnd(';') : null;
                    model.InformEmail = (!string.IsNullOrEmpty(tempInEmail)) ? tempInEmail.TrimEnd(';') : null;
                }

                //Propose
                if (viewModel.DueDatePropose.HasValue)
                {
                    model.DueDatePropose = viewModel.DueDatePropose;
                }
                else
                {
                    model.DueDatePropose = DateTime.Now.AddDays(2).ToLocalTime().Date;
                }

                //Accept
                if (viewModel.DueDateAccept.HasValue)
                {
                    model.DueDateAccept = viewModel.DueDateAccept;
                }
                else
                {
                    model.DueDateAccept = DateTime.Now.AddDays(2).ToLocalTime().Date;
                }
                //Approver
                if (viewModel.DueDateApprove.HasValue)
                {
                    model.DueDateApprove = viewModel.DueDateApprove;
                }
                else
                {
                    model.DueDateApprove = DateTime.Now.AddDays(2).ToLocalTime().Date;
                }
                //Final Approver
                if (viewModel.DueDateFinalApprove.HasValue)
                {
                    model.DueDateFinalApprove = viewModel.DueDateFinalApprove;
                }
                else
                {
                    model.DueDateFinalApprove = DateTime.Now.AddDays(2).ToLocalTime().Date;
                }
                //model.DueDatePropose = (viewModel.DueDatePropose.HasValue) ? viewModel.DueDatePropose.Value.ToLocalTime().Date : viewModel.DueDatePropose;
                //model.DueDateAccept = (viewModel.DueDateAccept.HasValue) ? viewModel.DueDateAccept.Value.ToLocalTime().Date : viewModel.DueDateAccept;
                //model.DueDateApprove = (viewModel.DueDateApprove.HasValue)
                //    ? viewModel.DueDateApprove.Value.ToLocalTime().Date
                //    : viewModel.DueDateApprove;
                //model.DueDateFinalApprove = (viewModel.DueDateFinalApprove.HasValue) ? viewModel.DueDateFinalApprove.Value.ToLocalTime().Date : viewModel.DueDateFinalApprove;
                model.RePrintNo = viewModel.RePrintNo;
            }

            return model;
        }
    }
}