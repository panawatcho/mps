﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TheMall.MPS.Models.Interfaces;


namespace TheMall.MPS.API.Mappers.Abstracts
{
    public abstract class BaseWorkflowTableMapperProfile : AutoMapper.Profile
    {
        protected AutoMapper.IMappingExpression<TWorkflowTable, TWorkflowTable2> DefaultCopy<TWorkflowTable, TWorkflowTable2>()
            where TWorkflowTable : class, IMpsWorkflowTable, new()
            where TWorkflowTable2 : class, IMpsWorkflowTable, new()
        {
            return CreateMap<TWorkflowTable, TWorkflowTable2>()
                .ForMember(m => m.Id, opt => opt.Ignore())
                .ForMember(m => m.ProcInstId, opt => opt.Ignore())
                .ForMember(m => m.Revision, opt => opt.Ignore())
                .ForMember(m => m.StatusFlag, opt => opt.Ignore())
                .ForMember(m => m.Status, opt => opt.Ignore())
                .ForMember(m => m.DocumentStatus, opt => opt.Ignore())
                .ForMember(m => m.ExpectedDuration, opt => opt.Ignore())
                //.ForMember(m => m.DocumentDate, opt => opt.Ignore())
                .ForMember(m => m.CompletedDate, opt => opt.Ignore())
                .ForMember(m => m.CancelledDate, opt => opt.Ignore())
                .ForMember(m => m.Deleted, opt => opt.Ignore())
                .ForMember(m => m.DeletedBy, opt => opt.Ignore())
                .ForMember(m => m.DeletedByName, opt => opt.Ignore())
                .ForMember(m => m.DeletedDate, opt => opt.Ignore())
                .ForMember(m => m.CreatedBy, opt => opt.Ignore())
                .ForMember(m => m.CreatedByName, opt => opt.Ignore())
                .ForMember(m => m.CreatedDate, opt => opt.Ignore())
                .ForMember(m => m.ModifiedBy, opt => opt.Ignore())
                .ForMember(m => m.ModifiedByName, opt => opt.Ignore())
                .ForMember(m => m.ModifiedDate, opt => opt.Ignore());
        }
    }
    public abstract class BaseWorkflowTableMapperProfile<TSource, TDestination> : BaseWorkflowTableMapperProfile
        where TSource : IMpsWorkflowTable //todo: this is too strict
        where TDestination : IMpsWorkflowTable
    {
        private AutoMapper.IMappingExpression<TSource, TDestination> _mapping;


        [Obsolete("Use the constructor instead. Will be removed in 6.0")]
        protected sealed override void Configure()
        {
            _mapping = DoCreateMap();
            Configure2();
        }

        protected virtual void Configure2()
        {
            // Do nothing. User code should go in here.
        }

        private AutoMapper.IMappingExpression<TSource, TDestination> DoCreateMap()
        {

            var mapping = _CreateMap().IgnoreAllVirtual();
                //.ForMember(m => m.Requester, opt => opt.Ignore());
            mapping.ReverseMap().IgnoreAllVirtual();
                //.ForMember(m => m.RequesterEmplId, opt =>
                //{
                //    opt.Condition(m => m.Requester != null);
                //    opt.MapFrom(m => m.Requester.EmpId);
                //});
            return mapping;
           
        }

        public AutoMapper.IMappingExpression<TSource, TDestination> CreateMap()
        {
            return _mapping;
        }


        private AutoMapper.IMappingExpression<TSource, TDestination> _CreateMap()
        {
            return base.CreateMap<TSource, TDestination>();
        }

        public new AutoMapper.IMappingExpression<TSource2, TDestination2> CreateMap<TSource2, TDestination2>()
        {
            if (typeof(TSource) == typeof(TSource2)
                && typeof(TDestination) == typeof(TDestination2))
            {
                throw new Exception("Replace base mapping is not allowed. Please use CreateMap() instead.");
                //return _mapping;
            }
            else
            {
                return base.CreateMap<TSource2, TDestination2>();
            }
        }
        //public new AutoMapper.IMappingExpression<TSource, TDestination> CreateMap<TSource, TDestination>()
        //{
        //    return base.CreateMap<TSource, TDestination>().IgnoreAllVirtual()
        //        .ReverseMap().IgnoreAllVirtual()
        //        .ReverseMap();
        //}
    }
    
}