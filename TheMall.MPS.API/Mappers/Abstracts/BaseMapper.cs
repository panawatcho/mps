﻿using System;
using System.Linq.Expressions;
using System.Reflection;
using CrossingSoft.Framework.Infrastructure;
using CrossingSoft.Framework.Services;
using TheMall.MPS.API.Exceptions;

namespace TheMall.MPS.API.Mappers.Abstracts
{
    public abstract class BaseMapper<TEntity>
        where TEntity : class , IObjectState, new()
    {
        private readonly IService<TEntity> _workflowTableService;

        protected BaseMapper(IService<TEntity> workflowTableService)
        {
            _workflowTableService = workflowTableService;
        }

        //public T InitViewModel<T>(TEntity entity)
        //    where T : class, new()
        //{
        //    var viewModel = new T();

        //    return viewModel;
        //}

        public TEntity InitModel<TKey>(Expression<Func<TEntity, TKey>> keySelector, int id)
        {
            TEntity model = null;
            if (id > 0)
            {
                model = _workflowTableService.Find(id);

                if (model == null)
                {
                    throw Helper.DataWithIDNotExists(id);
                }
            }
            else
            {
                model = new TEntity();
                var keyInfo = GetPropertyInfo(model, keySelector);
                keyInfo.SetValue(model, id);
            }

            return model;
        }
        public TEntity InitModel<TKey>(Expression<Func<TEntity, TKey>> keySelector, string id)
        {
            TEntity model = null;
            if (!string.IsNullOrEmpty(id))
            {
                model = _workflowTableService.Find(id);

                if (model == null)
                {
                    throw Helper.DataWithIDNotExists(id);
                }
            }
            else
            {
                model = new TEntity();
                var keyInfo = GetPropertyInfo(model, keySelector);
                keyInfo.SetValue(model, id);
            }

            return model;
        }

        protected PropertyInfo GetPropertyInfo<TSource, TProperty>(
            TSource source,
            Expression<Func<TSource, TProperty>> propertyLambda)
        {
            Type type = typeof(TSource);

            MemberExpression member = propertyLambda.Body as MemberExpression;
            if (member == null)
                throw new ArgumentException(string.Format(
                    "Expression '{0}' refers to a method, not a property.",
                    propertyLambda.ToString()));

            PropertyInfo propInfo = member.Member as PropertyInfo;
            if (propInfo == null)
                throw new ArgumentException(string.Format(
                    "Expression '{0}' refers to a field, not a property.",
                    propertyLambda.ToString()));

            if (type != propInfo.ReflectedType &&
                !type.IsSubclassOf(propInfo.ReflectedType))
                throw new ArgumentException(string.Format(
                    "Expresion '{0}' refers to a property that is not from type {1}.",
                    propertyLambda.ToString(),
                    type));

            return propInfo;
        }
    }
}