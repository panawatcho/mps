﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CrossingSoft.Framework.Models.Interfaces;
using TheMall.MPS.Models.Enums;
using TheMall.MPS.ViewModels.Interfaces;


namespace TheMall.MPS.Mappers.Abstracts
{
    public interface ILineMapper
    {
        T InitLineViewModel<T>(int lineNo)
            where T : class, ILineViewModel, new();

        T InitLineViewModel<T>(ILineTable lineTable)
            where T : class, ILineViewModel, new();

        T InitLineTable<T>(ILineViewModel viewModel, IEnumerable<T> lines)
            where T : class, ILineTable, new();
    }
    public abstract class BaseLineMapper : ILineMapper
    {
        public T InitLineViewModel<T>(int lineNo)
           where T : class, ILineViewModel, new()
        {
            var viewModel = new T();

            viewModel.LineNo = lineNo;
            viewModel.Deleted = false;
            viewModel.StatusFlag = (int)DocumentStatusFlag.Draft;

            return viewModel;
        }

        public T InitLineViewModel<T>(ILineTable lineTable)
            where T : class, ILineViewModel, new()
        {
            var viewModel = new T();

            viewModel.Id = lineTable.Id;
            viewModel.ParentId = lineTable.ParentId;
            viewModel.LineNo = lineTable.LineNo;
            viewModel.Deleted = lineTable.Deleted;
            viewModel.StatusFlag = lineTable.StatusFlag;

            return viewModel;
        }

        public T InitLineTable<T>(ILineViewModel viewModel, IEnumerable<T> lines)
            where T : class, ILineTable, new()
        {
            T model = null;
            if (viewModel.Id > 0 && viewModel.ParentId > 0)
            {
                if (lines != null)
                {
                    model = lines.FirstOrDefault(m => m.Id == viewModel.Id);
                }
                if (model == null)
                {
                    return null;
                    throw new HttpException(404, viewModel.Id.ToString());
                    //throw HttpExceptionHelper.DataWithIDNotExists(viewModel.Id);
                }
            }
            //else if (viewModel.LineNo > 0 && lines != null && lines.Any(m => Math.Abs(m.LineNo - viewModel.LineNo) < 0.01))
            //{
            //    model = lines.FirstOrDefault(m => Math.Abs(m.LineNo - viewModel.LineNo) < 0.01);
            //    if (model == null)
            //    {
            //        return null;
            //    }
            //}
            else
            {
                model = new T();

                model.Id = viewModel.Id;
                model.ParentId = viewModel.ParentId;
                model.LineNo = viewModel.LineNo;
                model.StatusFlag = viewModel.StatusFlag;
            }

            model.Deleted = viewModel.Deleted;

            return model;
        }
    }
}