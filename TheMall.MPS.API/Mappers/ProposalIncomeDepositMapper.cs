﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TheMall.MPS.API.Mappers.Abstracts;
using TheMall.MPS.API.Services;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Mappers.Abstracts;
using TheMall.MPS.Models.Enums;
using TheMall.MPS.Models.Proposal;
using TheMall.MPS.ViewModels;
using TheMall.MPS.ViewModels.FileManager;
using TheMall.MPS.ViewModels.Proposal;

namespace TheMall.MPS.API.Mappers
{
    public interface IProposalIncomeDepositMapper : ILineMapper
    {
        MPS_IncomeDeposit ToModel(IncomeDepositViewModel viewModel, IEnumerable<MPS_IncomeDeposit> lines);
        IncomeDepositViewModel ToViewModel(MPS_IncomeDeposit model);
        MPS_IncomeDeposit ToModel(IncomeDepositViewModel viewModel);
        IEnumerable<IncomeDepositViewModel> ToViewModels(IEnumerable<MPS_IncomeDeposit> models);
        IEnumerable<MPS_IncomeDeposit> ToModels(IEnumerable<IncomeDepositViewModel> viewModels, IEnumerable<MPS_IncomeDeposit> lines);
   
    }

    public class ProposalIncomeDepositMapper : BaseLineMapper, IProposalIncomeDepositMapper
    {
        private readonly IEmployeeTableService _employeeService;
        private readonly IProposalApprovalMapper _approvalMapper;
        private readonly IProposalApprovalService _approvalService;
        private readonly IProposalIncomeDepositService _incomeDepositService;
        private readonly IBudgetDepositTransService _budgetDepositTransService;
        private readonly IProposalService _proposalService;

        public ProposalIncomeDepositMapper(IEmployeeTableService employeeService,IProposalApprovalMapper approvalMapper, IProposalApprovalService approvalService, IProposalIncomeDepositService incomeDepositService, IBudgetDepositTransService budgetDepositTransService, IProposalService proposalService)
        {
            _employeeService = employeeService;
            _approvalMapper = approvalMapper;
            _approvalService = approvalService;
            _incomeDepositService = incomeDepositService;
            _budgetDepositTransService = budgetDepositTransService;
            _proposalService = proposalService;
        }

        public MPS_IncomeDeposit ToModel(IncomeDepositViewModel viewModel, IEnumerable<MPS_IncomeDeposit> lines)
        {
            if (viewModel == null)
            {
                return null;
            }
            if (viewModel.DocumentNumber == null )
            {
                return null;
            }
            var model = InitLineTable(viewModel, lines);
            if (model == null)
            {
                return null;
            }
            model.Budget = viewModel.Budget;
            model.Remark = viewModel.Remark;
            if (viewModel.DocumentNumber!=null)
            {
                model.ProposalDepositRefID = viewModel.DocumentNumber.Id;
                model.ProposalDepositRefDoc = viewModel.DocumentNumber.DocumentNumber;
                model.ProposalDepositRefTitle = viewModel.DocumentNumber.Title; 
            }
           
            var tempApprove = string.Empty;
            if (viewModel.Approver != null)
            {
                foreach (var app in viewModel.Approver.OrderBy(m => m.ApproverKey).ThenBy(m => m.ApproverSequence))
                {
                    tempApprove += string.Concat(app.ApproverUserName, ";").Trim();
                }
            }
            model.DepositApprover = (!string.IsNullOrEmpty(tempApprove)) ? tempApprove.TrimEnd(';') : null;

            model.Status = viewModel.Status;
            model.ProcInstId = viewModel.ProcInstId;
            model.ActualCharge = viewModel.ActualCharge;
            model.RefMemoId = viewModel.RefMemoId;
            return model;
        }
        public IncomeDepositViewModel ToViewModel(MPS_IncomeDeposit model)
        {
            if (model == null)
            {
                return null;
            }
            var viewModel = this.InitLineViewModel<IncomeDepositViewModel>(model);

            viewModel.Budget = model.Budget;
            viewModel.Remark = model.Remark;

            var approve = _approvalService.GetWhere(m => !m.Deleted && m.ParentId == model.ProposalDepositRefID);
            approve = approve.GroupBy(customer => customer.ApproverUserName).Select(group => group.Last());
             
            var proposalDeposit = _proposalService.Find(m => m.Id == model.ProposalDepositRefID);
            var budgetTrans = _budgetDepositTransService.GetBudgetDepositTransTotal(model.ProposalDepositRefID);
             var balance = (budgetTrans != 0) ? proposalDeposit.TotalBudget - budgetTrans + model.Budget : 0;
            viewModel.DocumentNumber = new DepositNumberViewModel()
            {
                Id = model.ProposalDepositRefID,
                Title = model.ProposalDepositRefTitle,
                DocumentNumber = model.ProposalDepositRefDoc,
                Approver = _approvalMapper.ToViewModels(approve).ToList(),
                Balance = balance
                
               
            };
            if (viewModel.DocumentNumber != null &&
                    viewModel.DocumentNumber.DocumentNumber != null)
            {
                viewModel.DocNumber = viewModel.DocumentNumber.DocumentNumber;
                viewModel.Title = viewModel.DocumentNumber.Title;
               
            }

            var listApprover = new List<ProposalApprovalViewModel>();
            if (!string.IsNullOrEmpty(model.DepositApprover))
            {
               // var lastIndex = model.DepositApprover.LastIndexOf(';');
                string[] approver = model.DepositApprover.TrimEnd(';').Split(';');
                listApprover.AddRange(approver.Select(app => _approvalService.LastOrDefault(m => m.ParentId == model.ProposalDepositRefID && m.ApproverUserName.Equals(app))).Select(approval => _approvalMapper.ToViewModel(approval)));
            }
            viewModel.Approver = (listApprover.Any()) ? listApprover : null; 

            viewModel.Status = model.Status;
            viewModel.ProcInstId = model.ProcInstId;
            viewModel.ActualCharge = model.ActualCharge;
            viewModel.RefMemoId = model.RefMemoId;
            return viewModel;
        }
        public MPS_IncomeDeposit ToModel(IncomeDepositViewModel viewModel)
        {
            if (viewModel == null)
            {
                return null;
            }
            if (viewModel.DocumentNumber == null)
            {
                return null;
            }
            var model = _incomeDepositService.Find(m => m.Id == viewModel.Id);
            if (model == null)
            {
                return null;
            }
            model.Budget = viewModel.Budget;
            model.Remark = viewModel.Remark;
            if (viewModel.DocumentNumber != null)
            {
                model.ProposalDepositRefID = viewModel.DocumentNumber.Id;
                model.ProposalDepositRefDoc = viewModel.DocumentNumber.DocumentNumber;
                model.ProposalDepositRefTitle = viewModel.DocumentNumber.Title;
            }
            var tempApprove = string.Empty;
            if (viewModel.Approver != null)
            {
                foreach (var app in viewModel.Approver.OrderBy(m => m.ApproverKey).ThenBy(m => m.ApproverSequence))
                {
                    tempApprove += string.Concat(app.ApproverUserName, ";").Trim();
                }
            }
            model.DepositApprover = (!string.IsNullOrEmpty(tempApprove)) ? tempApprove.TrimEnd(';') : null;

            model.Status = viewModel.Status;
            model.ProcInstId = viewModel.ProcInstId;
            model.ActualCharge = viewModel.ActualCharge;
            model.RefMemoId = viewModel.RefMemoId;
            return model;
        }
        public IEnumerable<IncomeDepositViewModel> ToViewModels(IEnumerable<MPS_IncomeDeposit> models)
        {
            return models != null ? models.Select(ToViewModel) : Enumerable.Empty<IncomeDepositViewModel>();
        }
        public IEnumerable<MPS_IncomeDeposit> ToModels(IEnumerable<IncomeDepositViewModel> viewModels, IEnumerable<MPS_IncomeDeposit> lines)
        {
            return viewModels != null ? viewModels.Select(m => ToModel(m, lines)).Where(m => m != null) : Enumerable.Empty<MPS_IncomeDeposit>();
        }
    }
}