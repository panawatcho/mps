﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using TheMall.MPS.Models;
using TheMall.MPS.Models.Master;
using TheMall.MPS.ViewModels.Master;
using WebApi.OutputCache.Core.Time;

namespace TheMall.MPS.API.Mappers
{
    
    public static class SharedBranchMapper
    {
        public static MPS_M_SharedBranch ToModel(this SharedBranchViewModel viewModel)
        {
            if (viewModel == null)
                return null;
            var model = new MPS_M_SharedBranch()
            {
                BranchCode = viewModel.BranchCode,
                BranchName = viewModel.BranchName,
                BranchAddress = viewModel.BranchAddress,
                Order = viewModel.Order,
                Inactive = viewModel.InActive
            };
            return model;

          
        }

        public static SharedBranchViewModel ToViewModel(this MPS_M_SharedBranch model)
        {
            if (model == null)
                return null;
            var viewModel = new SharedBranchViewModel()
            {
                BranchCode = model.BranchCode,
                BranchName = model.BranchName,
                BranchAddress = model.BranchAddress,
                Order = model.Order,
                InActive = model.Inactive
            };
            return viewModel;

        }

        public static SharedBranchViewModel ToViewModelPercent(this MPS_M_SharedBranch model, MPS_M_TotalSaleLastYear totalSale)
        {
            if (model == null)
                return null;
            var viewModel = new SharedBranchViewModel()
            {
                BranchCode = model.BranchCode,
                BranchName = model.BranchName,
                PercentRE = totalSale.PercentRE??0,
                PercentTR = totalSale.PercentTR??0,
                TotalSale = totalSale.TotalSale ?? 0,
                AreaRE = totalSale.AreaRE??0,
                AreaTR = totalSale.AreaTR??0,
                Order = model.Order,
                InActive = model.Inactive
            };
            return viewModel;
        }

        public static IEnumerable<SharedBranchViewModel> ToViewModels(this IEnumerable<MPS_M_SharedBranch> models)
        {
            return models != null ? models.Select(ToViewModel) : Enumerable.Empty<SharedBranchViewModel>();
        }
        public static IEnumerable<MPS_M_SharedBranch> ToModels(IEnumerable<SharedBranchViewModel> viewModels, IEnumerable<MPS_M_SharedBranch> lines)
        {
            return viewModels != null ? viewModels.Select(ToModel) : Enumerable.Empty<MPS_M_SharedBranch>();
        }

    }
}