﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using TheMall.MPS.API.Mappers.Abstracts;
using TheMall.MPS.API.Services;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Mappers.Abstracts;
using TheMall.MPS.Models.Memo;
using TheMall.MPS.Models.Proposal;
using TheMall.MPS.ViewModels.InterfaceToMprocurement;
using TheMall.MPS.ViewModels.Memo;
using TheMall.MPS.ViewModels.Proposal;

namespace TheMall.MPS.API.Mappers
{
    public interface IProposalLineMapper : ILineMapper
    {
        MPS_ProposalLine ToModel(ProposalLineViewModel viewModel, IEnumerable<MPS_ProposalLine> lines);
        ProposalLineViewModel ToViewModel(MPS_ProposalLine model,bool propInfo = false);
        IEnumerable<ProposalLineViewModel> ToViewModels(IEnumerable<MPS_ProposalLine> models, bool propInfo = false);
        IEnumerable<MPS_ProposalLine> ToModels(IEnumerable<ProposalLineViewModel> viewModels, IEnumerable<MPS_ProposalLine> lines);
        ProposalLineViewModel ToViewModelOnlyExpenseAndAp(MPS_ProposalLine model);
        IEnumerable<ProposalLineViewModel> ToViewModelsOnlyExpenseAndAp(IEnumerable<MPS_ProposalLine> models);
        IEnumerable<ProposalLineViewModel> ToViewModelOnlyOne(IEnumerable<MPS_ProposalLine> models, int id);
        IEnumerable<M_ProposalLineViewModel> M_ToProposalLineViewModels(IEnumerable<MPS_ProposalLine> model);
        List<MPS_ProposalLine> GroupProposalLine(IEnumerable<ProposalLineViewModel> viewModel);
    }

    public class ProposalLineMapper : BaseLineMapper, IProposalLineMapper
    {
        private readonly IExpenseTopicService _expensTopicService;
        private readonly IAPcodeService _apCodeService;
        private readonly IUnitCodeService _unitCodeService;
        private readonly IProposalLineTemplateMapper _proposalLineTemplateMapper;
        private readonly IBudgetProposalTransService _budgetProposalTransService;
        private readonly IAllocationBasisService _allocationBasisService;
        private readonly IProposalBudgetPlanService _budgetPlanService;
        public ProposalLineMapper(
            IExpenseTopicService expensTopicService,
            IAPcodeService apCodeService,
            IUnitCodeService unitCodeService, 
            IProposalLineTemplateMapper proposalLineTemplateMapper, 
            IBudgetProposalTransService budgetProposalTransService, 
            IAllocationBasisService allocationBasisService, IProposalBudgetPlanService budgetPlanService)
        {
            _expensTopicService = expensTopicService;
            _apCodeService = apCodeService;
            _unitCodeService = unitCodeService;
            _proposalLineTemplateMapper = proposalLineTemplateMapper;
            _budgetProposalTransService = budgetProposalTransService;
            _allocationBasisService = allocationBasisService;
            _budgetPlanService = budgetPlanService;
        }

        public MPS_ProposalLine ToModel(ProposalLineViewModel viewModel, IEnumerable<MPS_ProposalLine> lines)
        {
            if (viewModel == null)
            {
                return null;
            }
            var model = InitLineTable(viewModel, lines);
            if (model == null)
            {
                return new MPS_ProposalLine();
            }
             model.ExpenseTopicCode = viewModel.ExpenseTopicCode;
             model.APCode = (viewModel.APCode!=null)?viewModel.APCode.APCode:string.Empty;
             model.Description = viewModel.Description;
             model.BudgetPlan = viewModel.BudgetPlan;
             model.Actual = viewModel.Actual ?? 0;
             model.Remark = viewModel.Remark;
            if (model.StatusFlag == 0)
            {
                model.CheckBudget = (viewModel.APCode != null) && viewModel.APCode.CheckBudget;
            }
            else
            {
                model.CheckBudget = viewModel.CheckBudget;
            }
         
             if (viewModel.UnitCode!=null)
             {
                 model.UnitCode = viewModel.UnitCode.UnitCode;
                 model.UnitName = viewModel.UnitCode.UnitName;
             }
           
            return model;
        }


        public ProposalLineViewModel ToViewModel(MPS_ProposalLine model , bool propInfo = false)
        {
            if (model == null)
            {
                return new ProposalLineViewModel();
            }
            var viewModel = this.InitLineViewModel<ProposalLineViewModel>(model);
            var expenseTopic =_expensTopicService.FirstOrDefault(m=>m.ExpenseTopicCode == model.ExpenseTopicCode).ToViewModel();

            if (expenseTopic != null)
            {
                viewModel.ExpenseTopicCode = expenseTopic.ExpenseTopicCode;
                viewModel.ExpenseTopicName = expenseTopic.ExpenseTopicName;
                viewModel.TypeAP = expenseTopic.TypeAP; 
                viewModel.ExpenseTopic = expenseTopic;//
              
               
            }
             viewModel.APCode = _apCodeService.FirstOrDefault(m=>m.APCode == model.APCode).ToViewModel();
             viewModel.UnitCode =  _unitCodeService.FirstOrDefault(m=>m.UnitCode == model.UnitCode).ToViewModel();
             viewModel.Description = model.Description;
            if (propInfo)
            {
                var transTotal = _budgetProposalTransService.GetBudgetTransTotal(model.Id);
                viewModel.BudgetPlan = model.BudgetPlan - transTotal; //// Budget Line
            }
            else
            {
                viewModel.BudgetPlan = model.BudgetPlan;
            }     
             viewModel.Actual = model.Actual;
             viewModel.Remark = model.Remark;
             viewModel.Amount = model.BudgetPlan - model.Actual;
             if (model.Actual == 0 || model.Actual == null )
             {
                 var result = model.BudgetPlan - model.Actual;
                 viewModel.Percent = (result != 0) ? (result / model.BudgetPlan) * 100 : 0;
             }
             else
             {
                 viewModel.Percent = ((model.BudgetPlan - model.Actual) / model.Actual) * 100;
             }
             viewModel.CheckBudget = model.CheckBudget;
             viewModel.ProposalLineTemplate = _proposalLineTemplateMapper.ToViewModels(model.ProposalLineTemplates);
             var balance = _budgetProposalTransService.GetBudgetTransTotal(model.Id);
            viewModel.Used = balance;// ใช้ไป
            viewModel.Remainning = model.BudgetPlan - balance;  // คงเหลือ
            var totalBudget = _budgetProposalTransService.GetBudgetTransTotal(model.ParentId, model.APCode, model.UnitCode);
            var propBudgetPlan = _budgetPlanService.GetProposalBudgetPlan(model.ParentId, model.APCode, model.UnitCode);
           
            viewModel.TotalBudgetPlan = (propBudgetPlan != null) ? propBudgetPlan.BudgetPlan - totalBudget : 0; // Budget รวม
            return viewModel;
        }
        
        public IEnumerable<ProposalLineViewModel> ToViewModels(IEnumerable<MPS_ProposalLine> models, bool propInfo = false)
        {
            return models != null ? models.Select(m=> ToViewModel(m, propInfo)) : Enumerable.Empty<ProposalLineViewModel>();
        }
        public IEnumerable<MPS_ProposalLine> ToModels(IEnumerable<ProposalLineViewModel> viewModels, IEnumerable<MPS_ProposalLine> lines)
        {
            return viewModels != null ? viewModels.Select(m => ToModel(m, lines)).Where(m => m != null) : Enumerable.Empty<MPS_ProposalLine>();
        }

        public ProposalLineViewModel ToViewModelOnlyExpenseAndAp(MPS_ProposalLine model)
        {
            if (model == null)
            {
                return null;
            }
            var viewModel = this.InitLineViewModel<ProposalLineViewModel>(model);

            var expenseTopic = _expensTopicService.FirstOrDefault(m => m.ExpenseTopicCode == model.ExpenseTopicCode).ToViewModel();
            if (expenseTopic != null)
            {
                viewModel.ExpenseTopicCode = expenseTopic.ExpenseTopicCode;
                viewModel.ExpenseTopicName = expenseTopic.ExpenseTopicName;
                viewModel.ExpenseTopic = expenseTopic;//
            }

            viewModel.APCode = _apCodeService.FirstOrDefault(m => m.APCode == model.APCode).ToViewModel();
            viewModel.UnitCode = _unitCodeService.FirstOrDefault(m => m.UnitCode == model.UnitCode).ToViewModel();
            return viewModel;
        }

        public IEnumerable<ProposalLineViewModel> ToViewModelsOnlyExpenseAndAp(IEnumerable<MPS_ProposalLine> models)
        {
            return models != null ? models.Select(ToViewModelOnlyExpenseAndAp) : Enumerable.Empty<ProposalLineViewModel>();
        }

        public IEnumerable<ProposalLineViewModel> ToViewModelOnlyOne(IEnumerable<MPS_ProposalLine> models, int id)
        {
            var mpsProposalLines = models as IList<MPS_ProposalLine> ?? models.ToList();
            foreach (var aLine in mpsProposalLines)
            {
                if (aLine.Id == id)
                {
                    return models != null ? mpsProposalLines.Select(m=>ToViewModel(m)) : Enumerable.Empty<ProposalLineViewModel>();
                }
            }

            return null;
        }

        public M_ProposalLineViewModel M_ToProposalLineViewModel(MPS_ProposalLine model)
        {
            var viewModel = new M_ProposalLineViewModel();
            viewModel.TableName = "BU_PROB_AP";
            viewModel.ProposalNumber = model.ProposalTable.DocumentNumber;
            viewModel.PropUnitCode = model.ProposalTable.UnitCode;
            viewModel.UnitCode = model.UnitCode;
            viewModel.APCode = model.APCode;
            if (model.ProposalTable != null)
            {
               var allocationBasis = _allocationBasisService.FindByProposalSharedTemplate(model.ProposalTable.TemplateLines);
                if (allocationBasis != null)
                {
                    viewModel.AllocationBasis = allocationBasis.AllocCode;
                }
            }
            viewModel.APDescription = model.Description;
            
            return viewModel;
        }

        public IEnumerable<M_ProposalLineViewModel> M_ToProposalLineViewModels(IEnumerable<MPS_ProposalLine> model)
        {
            return model != null ? model.Select(M_ToProposalLineViewModel) : null;
        }

        public List<MPS_ProposalLine> GroupProposalLine(IEnumerable<ProposalLineViewModel> viewModel)
        {
            if (viewModel == null)
            {
                return null;
            }
            var listModel = new List<MPS_ProposalLine>();
           // var groupViewModel = new List<ProposalLineViewModel>();
            var groupViewModel =(from t in viewModel.Where(m=>m.UnitCode!= null && !string.IsNullOrEmpty(m.UnitCode.UnitCode))
                                 group t by new {t.UnitCode.UnitCode, t.APCode.APCode}
                                 into grp   
                                 select new
                                 {
                                    grp.Key.UnitCode,
                                    grp.Key.APCode,
                                    BudgetPlan = grp.Where(r=> !r.Deleted).Sum(t => t.BudgetPlan),
                                    ParentId = grp.First().ParentId,
                                    Deleted = grp.All(t => t.Deleted)
                                }).ToList();
          

            foreach (var group in groupViewModel)
            {
                var model = new MPS_ProposalLine();
                model.APCode = group.APCode;
                model.UnitCode = group.UnitCode;
                model.ParentId = group.ParentId;
                model.BudgetPlan = group.BudgetPlan;
                model.Deleted = group.Deleted;
                listModel.Add(model);
            }
            return listModel;
        }
    }
}