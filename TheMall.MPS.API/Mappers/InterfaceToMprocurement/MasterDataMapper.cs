﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TheMall.MPS.Models.Master;
using TheMall.MPS.Models.Proposal;
using TheMall.MPS.ViewModels.InterfaceToMprocurement;
using TheMall.MPS.ViewModels.Proposal;

namespace TheMall.MPS.API.Mappers
{

    public static class MasterDataMapper 
    {

        public static M_ProposalIDViewModel M_ToProposalNumber(this MPS_ProposalTable model)
        {
            var viewModel = new M_ProposalIDViewModel();
            viewModel.ProposalNumber = model.DocumentNumber;
            viewModel.Description = model.Title;
            return viewModel;
        }

        public static IEnumerable<M_ProposalIDViewModel> M_ToProposalNumbers(this IEnumerable<MPS_ProposalTable> model)
        {
            return model != null ? model.Select(M_ToProposalNumber) : null;
        }

        public static M_ExpenseTopicViewModel M_ToExpenseTopicViewModel(this MPS_M_ExpenseTopic model)
        {
            var viewModel = new M_ExpenseTopicViewModel();
            viewModel.TableName = "EXPENSETOPIC";
            viewModel.ExpenseTopicCode = model.ExpenseTopicCode;
            viewModel.ExpenseTopicName = model.ExpenseTopicName;
            //viewModel.Description = model.ExpenseTopicName;
            //viewModel.APCodeList = model.APCode.ToAPCodeViewModels().ToList();
            return viewModel;
        }
        public static IEnumerable<M_ExpenseTopicViewModel> M_ToExpenseTopicViewModels(this IEnumerable<MPS_M_ExpenseTopic> model)
        {
            return model != null ? model.Select(M_ToExpenseTopicViewModel) : Enumerable.Empty<M_ExpenseTopicViewModel>();
        }

        public static M_APCodeViewModel M_ToAPCodeViewModel(this MPS_M_APCode model)
        {
            var viewModel = new M_APCodeViewModel();
            viewModel.TableName = "APCODE";
            viewModel.APCode= model.APCode;
            //viewModel.APName = model.APName;
            viewModel.Description = model.Description;
            //viewModel.ExpenseTopicCode = model.ExpenseTopicCode;
            return viewModel;
        }
        public static IEnumerable<M_APCodeViewModel> M_ToAPCodeViewModels(this IEnumerable<MPS_M_APCode> model)
        {
            return model != null ? model.Select(M_ToAPCodeViewModel) : Enumerable.Empty<M_APCodeViewModel>();
        }

        public static M_AllocationBasisViewModel M_ToAllocationBasisViewModel(this MPS_M_AllocationBasis model)
        {
            var viewModel = new M_AllocationBasisViewModel();
            viewModel.TableName = "ALLOCATIONCODE";
            viewModel.AllocCode = model.AllocCode;
            viewModel.Description = model.Description;
            return viewModel;
        }
        public static IEnumerable<M_AllocationBasisViewModel> M_ToAllocationBasisViewModels(this IEnumerable<MPS_M_AllocationBasis> model)
        {
            return model != null ? model.Select(M_ToAllocationBasisViewModel) : Enumerable.Empty<M_AllocationBasisViewModel>();
        }
    }
}