﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TheMall.MPS.Models.Enums;
using TheMall.MPS.Models.Procurement.PR;
using TheMall.MPS.ViewModels.InterfaceToMprocurement.PR;

namespace TheMall.MPS.API.Mappers
{
    public interface IPurchReqMapper
    {
        MPS_PurchReqTable ToPurchReqTable(PRHeaderViewModel viewModel);
        MPS_PurchReqLine ToPurchReqLine(PRItemViewModel viewModel, IEnumerable<PRItemViewModel> prItems);
        IEnumerable<MPS_PurchReqLine> ToPurchReqLines(PRItemViewModel[] viewModels);
    }
    public class PurchReqMapper : IPurchReqMapper
    {
        public PurchReqMapper()
        {
            
        }
        public MPS_PurchReqTable ToPurchReqTable(PRHeaderViewModel viewModel)
        {
            var model = new MPS_PurchReqTable();
            model.IP_ID = viewModel.IP_ID;
            model.REQ_ID = int.Parse(viewModel.REQ_ID);
            model.PR_NUM = viewModel.PR_NUM;
            model.SUBCOMMAND = viewModel.SUBCOMMAND;
            model.REQUISITIONER = viewModel.REQUISITIONER;
            model.BS_REQID = viewModel.BS_REQID;
            model.REQ_NAME = viewModel.REQ_NAME;
            model.COMPANY_CODE = viewModel.COMPANY_CODE;
            DateTime postDate;
            if (DateTime.TryParse(viewModel.POST_DATE, out postDate))
                model.POST_DATE = postDate;

            return model;
        }

        public MPS_PurchReqLine ToPurchReqLine(PRItemViewModel viewModel, IEnumerable<PRItemViewModel> prItems)
        {
            var model = new MPS_PurchReqLine();

            //Map PRItem
            model.ITEM_NUM = int.Parse(viewModel.ITEM_NUM);

            decimal quantity;
            if (decimal.TryParse(viewModel.QUANTITY, out quantity))
                model.QUANTITY = quantity;

            model.ITEM_DESC = viewModel.ITEM_DESC;
            model.UOM = viewModel.UOM;
            model.OLD_PRICE = viewModel.OLD_PRICE;

            decimal price;
            if (decimal.TryParse(viewModel.PRICE, out price))
                model.PRICE = price;

            model.OLD_TAX_AMOUNT = viewModel.OLD_TAX_AMOUNT;

            decimal taxAmount;
            if (decimal.TryParse(viewModel.TAX_AMOUNT, out taxAmount))
                model.TAX_AMOUNT = taxAmount;

            model.OLD_CURRENCY_CODE = viewModel.OLD_CURRENCY_CODE;
            model.CURRENCY_CODE = viewModel.CURRENCY_CODE;

            DateTime deliveryDate;
            if (DateTime.TryParse(viewModel.DELIV_DATE, out deliveryDate))
                model.DELIV_DATE = deliveryDate;

            model.LONGDESC = viewModel.LONGDESC;
            model.SHIP_INST = viewModel.SHIP_INST;
            model.PAYMENTDESC = viewModel.PAYMENTDESC;

            decimal actualPrice;
            if (decimal.TryParse(viewModel.ACTUALPRICE, out actualPrice))
                model.ACTUALPRICE = actualPrice;

            //Map PR Account Assignment
            var prAccountAssigment = prItems.FirstOrDefault(m => m.Name == MProcurementConstant.TableOrStructureName.PRAcctAssgn && m.ITEM_NUM == viewModel.ITEM_NUM);
            if (prAccountAssigment != null)
            {
                model.COSTCENTER = prAccountAssigment.COSTCENTER;
                model.PODOCTYPE = prAccountAssigment.PODOCTYPE;
                model.PURTYPE = prAccountAssigment.PURTYPE;
                model.GLACCOUNT = prAccountAssigment.GLACCOUNT;
                model.DEPARTMENT = prAccountAssigment.DEPARTMENT;
                model.PURGROUP = prAccountAssigment.PURGROUP;
                model.PROPOSALID = prAccountAssigment.PROPOSALID;
                model.APCODE = prAccountAssigment.APCODE;
                model.APDESC = prAccountAssigment.APDESC;
                model.REQUESTFORID = prAccountAssigment.REQUESTFORID;
                model.OLD_BUDGET_CODE = prAccountAssigment.OLD_BUDGET_CODE;
                model.OLD_ORG_ID = prAccountAssigment.OLD_ORG_ID;
            }


            return model;
        }

        public IEnumerable<MPS_PurchReqLine> ToPurchReqLines(PRItemViewModel[] viewModels)
        {
            var prItemList = viewModels.ToList();
            var prItems = prItemList.Where(m => m.Name == MProcurementConstant.TableOrStructureName.PRItem);

            return prItems != null && prItems.Any() ? prItems.Select(m => ToPurchReqLine(m, prItemList)) : Enumerable.Empty<MPS_PurchReqLine>(); 
        } 
    }
}