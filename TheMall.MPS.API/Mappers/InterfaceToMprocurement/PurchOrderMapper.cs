﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TheMall.MPS.Models.Enums;
using TheMall.MPS.Models.Procurement.PO;
using TheMall.MPS.ViewModels.InterfaceToMprocurement.PO;

namespace TheMall.MPS.API.Mappers
{
    public interface IPurchOrderMapper
    {
        MPS_PurchOrderTable ToPurchOrderTable(POHeaderViewModel viewModel);
        MPS_PurchOrderLine ToPurchOrderLine(POItemViewModel viewModel, IEnumerable<POItemViewModel> POItems);
        IEnumerable<MPS_PurchOrderLine> ToPurchOrderLines(POItemViewModel[] viewModels);
    }
    public class PurchOrderMapper : IPurchOrderMapper
    {
        public PurchOrderMapper()
        {
            
        }
        public MPS_PurchOrderTable ToPurchOrderTable(POHeaderViewModel viewModel)
        {
            var model = new MPS_PurchOrderTable();
            model.PO_ID = int.Parse(viewModel.PO_ID);
            model.PO_NUM = viewModel.PO_NUM;
            model.IP_ID = viewModel.IP_ID;
            int reqId;
            if (int.TryParse(viewModel.REQ_ID, out reqId))
                model.REQ_ID = reqId;

            model.PR_NUM = viewModel.PR_NUM;

            DateTime poDate;
            if (DateTime.TryParse(viewModel.PO_DATE, out poDate))
                model.PO_DATE = poDate;

            Boolean closePO;
            if (Boolean.TryParse(viewModel.CLOSEPO, out closePO))
                model.CLOSE_PO = closePO;

            return model;
        }

        public MPS_PurchOrderLine ToPurchOrderLine(POItemViewModel viewModel, IEnumerable<POItemViewModel> poItems)
        {
            var model = new MPS_PurchOrderLine();

            //Map POItem
            model.ITEM_NUM = int.Parse(viewModel.ITEM_NUM);
            model.PR_LINE_NUM = int.Parse(viewModel.PR_LINE_NUM);

            decimal quantity;
            if (decimal.TryParse(viewModel.QUANTITY, out quantity))
                model.QUANTITY = quantity;

            model.ITEM_DESC = viewModel.ITEM_DESC;
            model.UOM = viewModel.UOM;
            model.OLD_PRICE = viewModel.OLD_PRICE;

            decimal price;
            if (decimal.TryParse(viewModel.PRICE, out price))
                model.PRICE = price;

            model.OLD_TAX_AMOUNT = viewModel.OLD_TAX_AMOUNT;

            decimal taxAmount;
            if (decimal.TryParse(viewModel.TAX_AMOUNT, out taxAmount))
                model.TAX_AMOUNT = taxAmount;

            model.OLD_CURRENCY_CODE = viewModel.OLD_CURRENCY_CODE;
            model.CURRENCY_CODE = viewModel.CURRENCY_CODE;

            DateTime deliveryDate;
            if (DateTime.TryParse(viewModel.DELIV_DATE, out deliveryDate))
                model.DELIV_DATE = deliveryDate;

            decimal actualPrice;
            if (decimal.TryParse(viewModel.ACTUALPRICE, out actualPrice))
                model.ACTUALPRICE = actualPrice;

            //Map PO Account Assignment
            var poAccountAssigment = poItems.FirstOrDefault(m => m.Name == MProcurementConstant.TableOrStructureName.POAcctAssgn && m.ITEM_NUM == viewModel.ITEM_NUM);
            if (poAccountAssigment != null)
            {
                model.COSTCENTER = poAccountAssigment.COSTCENTER;
                model.PODOCTYPE = poAccountAssigment.PODOCTYPE;
                model.PURTYPE = poAccountAssigment.PURTYPE;
                model.GLACCOUNT = poAccountAssigment.GLACCOUNT;
                model.DEPARTMENT = poAccountAssigment.DEPARTMENT;
                model.PURGROUP = poAccountAssigment.PURGROUP;
                model.PROPOSALID = poAccountAssigment.PROPOSALID;
                model.APCODE = poAccountAssigment.APCODE;
                model.APDESC = poAccountAssigment.APDESC;
                model.REQUESTFORID = poAccountAssigment.REQUESTFORID;
                model.OLD_BUDGET_CODE = poAccountAssigment.OLD_BUDGET_CODE;
                model.OLD_ORG_ID = poAccountAssigment.OLD_ORG_ID;
            }


            return model;
        }

        public IEnumerable<MPS_PurchOrderLine> ToPurchOrderLines(POItemViewModel[] viewModels)
        {
            var poItemList = viewModels.ToList();
            var poItems = poItemList.Where(m => m.Name == MProcurementConstant.TableOrStructureName.POItem);

            return poItems != null && poItems.Any() ? poItems.Select(m => ToPurchOrderLine(m, poItemList)) : Enumerable.Empty<MPS_PurchOrderLine>(); 
        } 
    }
}