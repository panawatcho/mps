﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TheMall.MPS.Models.Enums;
using TheMall.MPS.Models.Procurement.GR;
using TheMall.MPS.ViewModels.InterfaceToMprocurement.GR;

namespace TheMall.MPS.API.Mappers
{
    public interface IGoodsReceiptMapper
    {
        MPS_GoodsReceiptTable ToGoodsReceiptTable(GRHeaderViewModel viewModel);
        MPS_GoodsReceiptLine ToGoodsReceiptLine(GRItemViewModel viewModel);
        IEnumerable<MPS_GoodsReceiptLine> ToGoodsReceiptLines(GRItemViewModel[] viewModels);
    }
    public class GoodsReceiptMapper : IGoodsReceiptMapper
    {
        public GoodsReceiptMapper()
        {

        }
        public MPS_GoodsReceiptTable ToGoodsReceiptTable(GRHeaderViewModel viewModel)
        {
            var model = new MPS_GoodsReceiptTable();
            model.RECEIPT_ID = int.Parse(viewModel.RECEIPT_ID);
            model.PO_ID = int.Parse(viewModel.PO_ID);
            model.PO_NUM = viewModel.PO_NUM;

            DateTime docDate;
            if (DateTime.TryParse(viewModel.DOC_DATE, out docDate))
                model.DOC_DATE = docDate;

            DateTime postDate;
            if (DateTime.TryParse(viewModel.POST_DATE, out postDate))
                model.POST_DATE = postDate;
            model.DELIV_NOTE = viewModel.DELIV_NOTE;
            model.HEADER_TXT = viewModel.HEADER_TXT;
            model.BILL_LAD = viewModel.BILL_LAD;
            model.SLIP_NUM = viewModel.SLIP_NUM;
            return model;
        }

        public MPS_GoodsReceiptLine ToGoodsReceiptLine(GRItemViewModel viewModel)
        {
            var model = new MPS_GoodsReceiptLine();

            //Map GRItem
            model.ITEM_NUM = int.Parse(viewModel.ITEM_NUM);
            model.LOCATION = viewModel.LOCATION;

            decimal quantity;
            if (decimal.TryParse(viewModel.QUANTITY, out quantity))
                model.QUANTITY = quantity;

            model.DELIV_CMPL = viewModel.DELIV_CMPL;
            model.REASON = viewModel.REASON;
            model.ITEM_TEXT = viewModel.ITEM_TEXT;
            model.MOVEMENTTYPE = viewModel.MOVEMENTTYPE;

            decimal actualPrice;
            if (decimal.TryParse(viewModel.ACTUALPRICE, out actualPrice))
                model.ACTUALPRICE = actualPrice;

            if (!string.IsNullOrEmpty(viewModel.REFERNUM))
                model.REFERNUM = int.Parse(viewModel.REFERNUM);
            return model;
        }

        public IEnumerable<MPS_GoodsReceiptLine> ToGoodsReceiptLines(GRItemViewModel[] viewModels)
        {
            var grItemList = viewModels.ToList();
            var grItems = grItemList.Where(m => m.Name == MProcurementConstant.TableOrStructureName.GRItem);

            return grItems != null && grItems.Any() ? grItems.Select(m => ToGoodsReceiptLine(m)) : Enumerable.Empty<MPS_GoodsReceiptLine>();
        }
    }
}