﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TheMall.MPS.Models;
using TheMall.MPS.ViewModels;


namespace TheMall.MPS.API.Mappers
{

    public  class RequesterMapper
    {
        public RequesterMapper()
        {
        }

        public  MPS_M_EmployeeTable ToModel( EmployeeViewModel viewModel)
        {
            if (viewModel == null)
                return null;

            var model = new MPS_M_EmployeeTable()
            {
                EmpId = viewModel.EmpId,
                EmpStatus = viewModel.EmpStatus,
                ThFName = viewModel.ThFName,
                ThLName = viewModel.ThLName,
                EnFName = viewModel.EnFName,
                EnLName = viewModel.EnLName,
                DepartmentCode = viewModel.DepartmentCode,
                DepartmentName = viewModel.DepartmentName,
                PositionCode = viewModel.PositionCode,
                PositionName = viewModel.PositionName,
                Email = viewModel.Email,
                TelephoneNo = viewModel.TelephoneNo,
                CompanyCode = viewModel.CompanyCode,
                CompanyName = viewModel.CompanyName,
                Username = viewModel.Username
            };
            return model;
        }

      
        public  EmployeeViewModel ToViewModel( MPS_M_EmployeeTable model)
        {

            if (model == null)
                return null;

            var viewModel = new EmployeeViewModel()
            {
                EmpId = model.EmpId,
                EmpStatus = model.EmpStatus,
                ThFName = model.ThFName,
                ThLName = model.ThLName,
                EnFName = model.EnFName,
                EnLName = model.EnLName,
                DepartmentCode = model.DepartmentCode,
                DepartmentName = model.DepartmentName,
                PositionCode = model.PositionCode,
                PositionName = model.PositionName,
                Email = model.Email,
                TelephoneNo = model.TelephoneNo,
                CompanyCode = model.CompanyCode,
                CompanyName = model.CompanyName,
                Username = model.Username
            };
            // var unitcode = _unitCode.GetAll();
            //var unitforemp = _unitCodeforemp.GetWhere(m => m.Username == model.RequesterUserName);
            //var unitCode = from unitemp in unitforemp
            //               join unit in unitcode on unitemp.UnitCode equals unit.UnitCode
            //               select new
            //               {
            //                   code
            //               };
            return viewModel;
        }

        //public static IEnumerable<MPS_EmployeeTable> ToModel(IEnumerable<EmployeeViewModel> viewModels, IEnumerable<MPS_EmployeeTable> lines)
        //{
        //    return viewModels != null ? viewModels.Select(m => ToModel(m, lines)).Where(m => m != null) : Enumerable.Empty<MPS_EmployeeTable>();
        //}

        public  IEnumerable<EmployeeViewModel> ToViewModel( IEnumerable<MPS_M_EmployeeTable> models)
        {
            return models != null ? models.Select(ToViewModel) : Enumerable.Empty<EmployeeViewModel>();
        }
        //public static EmployeeSearchViewModel ToSearchViewModel(this Employee model)
        //{
        //    if (model == null)
        //        return null;

        //    return AutoMapper.Mapper.Map<EmployeeSearchViewModel>(model);
        //}

        //public static IEnumerable<EmployeeSearchViewModel> ToSearchViewModel(this IEnumerable<Employee> models)
        //{
        //    return models != null ? models.Select(ToSearchViewModel) : Enumerable.Empty<EmployeeSearchViewModel>();
        //}
    }
}