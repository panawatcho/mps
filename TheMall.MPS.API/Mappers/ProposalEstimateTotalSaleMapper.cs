﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TheMall.MPS.API.Mappers.Abstracts;
using TheMall.MPS.API.Services;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Mappers.Abstracts;
using TheMall.MPS.Models.Proposal;
using TheMall.MPS.ViewModels.Master;
using TheMall.MPS.ViewModels.Proposal;

namespace TheMall.MPS.API.Mappers
{
    public interface IProposalEstimateTotalSaleMapper : ILineMapper
    {
        MPS_EstimateTotalSale ToModel(EstimateTotalSaleViewModel viewModel, IEnumerable<MPS_EstimateTotalSale> lines);
        EstimateTotalSaleViewModel ToViewModel(MPS_EstimateTotalSale model);
        IEnumerable<EstimateTotalSaleViewModel> ToViewModels(IEnumerable<MPS_EstimateTotalSale> models);
        IEnumerable<MPS_EstimateTotalSale> ToModels(IEnumerable<EstimateTotalSaleViewModel> viewModels, IEnumerable<MPS_EstimateTotalSale> lines);
    }

    public class ProposalEstimateTotalSaleMapper : BaseLineMapper, IProposalEstimateTotalSaleMapper
    {
        private readonly IProposalEstimateTotalSaleTemplateMapper _estimateTotalSaleTemplateMapper;

        public ProposalEstimateTotalSaleMapper(IProposalEstimateTotalSaleTemplateMapper estimateTotalSaleTemplateMapper)
        {
            _estimateTotalSaleTemplateMapper = estimateTotalSaleTemplateMapper;
        }

        public MPS_EstimateTotalSale ToModel(EstimateTotalSaleViewModel viewModel, IEnumerable<MPS_EstimateTotalSale> lines)
        {
            if (viewModel == null)
            {
                return null;
            }
            var model = InitLineTable(viewModel, lines);
            if (model == null)
            {
                return new MPS_EstimateTotalSale();
            }
            if (viewModel.ExpenseTopic != null)
            {
                model.ExpenseTopicCode = viewModel.ExpenseTopic.ExpenseTopicCode;
                model.ExpenseTopicName = viewModel.ExpenseTopic.ExpenseTopicName;
            }
         
            model.BudgetPlan = viewModel.BudgetPlan;
            model.BudgetPlan = viewModel.BudgetPlan;
            model.Actual = viewModel.Actual;
            model.PreviousActual = viewModel.PreviousActual;
            model.Remark = viewModel.Remark;


            return model;
        }
        public EstimateTotalSaleViewModel ToViewModel(MPS_EstimateTotalSale model)
        {
            if (model == null)
            {
                return null;
            }
            var viewModel = this.InitLineViewModel<EstimateTotalSaleViewModel>(model);

            if (viewModel.ExpenseTopic != null)
            {
                model.ExpenseTopicCode = viewModel.ExpenseTopic.ExpenseTopicCode;
                model.ExpenseTopicName = viewModel.ExpenseTopic.ExpenseTopicName;
            }
            viewModel.ExpenseTopic = new ExpenseTopicViewModel()
            {
                ExpenseTopicCode = model.ExpenseTopicCode,
                ExpenseTopicName = model.ExpenseTopicName
            };
            viewModel.BudgetPlan = viewModel.BudgetPlan;
            viewModel.BudgetPlan = viewModel.BudgetPlan;
            viewModel.Actual = viewModel.Actual;
            viewModel.PreviousActual = viewModel.PreviousActual;
            viewModel.Remark = viewModel.Remark;
            viewModel.EstimateTotalSaleTemplate = (model.EstimateTotalSaleTemplate != null) ? _estimateTotalSaleTemplateMapper.ToViewModels(model.EstimateTotalSaleTemplate) : new List<EstimateTotalSaleTemplateViewModel>();


            return viewModel;
        }

        public IEnumerable<EstimateTotalSaleViewModel> ToViewModels(IEnumerable<MPS_EstimateTotalSale> models)
        {
            return models != null ? models.Select(ToViewModel) : Enumerable.Empty<EstimateTotalSaleViewModel>();
        }
        public IEnumerable<MPS_EstimateTotalSale> ToModels(IEnumerable<EstimateTotalSaleViewModel> viewModels, IEnumerable<MPS_EstimateTotalSale> lines)
        {
            return viewModels != null ? viewModels.Select(m => ToModel(m, lines)).Where(m => m != null) : Enumerable.Empty<MPS_EstimateTotalSale>();
        }

       
    }
}