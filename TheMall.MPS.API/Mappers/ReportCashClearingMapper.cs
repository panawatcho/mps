﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TheMall.MPS.API.Services;
using TheMall.MPS.Infrastructure.Extension;
using TheMall.MPS.Models.CashClearing;
using TheMall.MPS.Models.Enums;
using TheMall.MPS.Reports.CashClearing;
using TheMall.MPS.Reports.Shared;
using TheMall.MPS.ViewModels.CashClearing;

namespace TheMall.MPS.API.Mappers
{
    public interface IReportCashClearingMapper
    {
        IEnumerable<CashClearingDataSource> ReportDataMapper(CashClearingViewModel cashAdvanceViewModel);
        IEnumerable<ApproverDataSource> ApproversDataMapper(IEnumerable<MPS_CashClearingApproval> approversData);
    }
    public class ReportCashClearingMapper : IReportCashClearingMapper
    {
        private readonly ICompanyService _companyService;

        public ReportCashClearingMapper(ICompanyService companyService)
        {
            _companyService = companyService;
        }

        public IEnumerable<CashClearingDataSource> ReportDataMapper(CashClearingViewModel cashClearingViewModel)
        {
            var list = new List<CashClearingDataSource>();
            decimal sumNetActual = 0;
            if (cashClearingViewModel != null)
            {
                foreach (var line in cashClearingViewModel.CashClearingLines)
                {
                        if (line.NetActual != null)
                        {
                            decimal netActual = (decimal) line.NetActual;
                            sumNetActual = sumNetActual + netActual;
                        }
                }
                foreach (var line in cashClearingViewModel.CashClearingLines)
                {
                    var reportDataSource = new CashClearingDataSource();
                    {

                    }

                    reportDataSource.Id = line.Id;

                    if (cashClearingViewModel.Branch != null)
                    {
                        reportDataSource.Branch = cashClearingViewModel.Branch.Description;
                        reportDataSource.CashClearingBranch = cashClearingViewModel.Branch.PlaceName;
                        reportDataSource.RequesterBranch = cashClearingViewModel.Requester.CompanyName;
                        //cashClearingViewModel.Branch.PlaceCode;
                    }
                    else
                    {
                        reportDataSource.Branch = "";
                    }

                    if (cashClearingViewModel.Requester != null)
                    {
                        reportDataSource.RequesterName = cashClearingViewModel.Requester.FullName;
                        //reportDataSource.RequesterBranch = cashClearingViewModel.Requester.CompanyName;
                        reportDataSource.RequesterDepositName = cashClearingViewModel.Requester.PositionName;
                    }
                    else
                    {
                        reportDataSource.RequesterName = "";
                        //reportDataSource.RequesterBranch = "";
                        reportDataSource.RequesterDepositName = "";
                    }

                    if (cashClearingViewModel.DocumentNumber != null)
                    {
                        reportDataSource.DocumentNumber = cashClearingViewModel.DocumentNumber;
                    }
                    else
                    {
                        reportDataSource.DocumentNumber = "";
                    }

                    if (cashClearingViewModel.ProposalRef != null)
                    {
                        if (cashClearingViewModel.ProposalRef.ProposalUnitCode != null)
                        {
                            reportDataSource.ProposalUnitCode = cashClearingViewModel.ProposalRef.ProposalUnitCode.UnitCode;
                            reportDataSource.ProposalUnitName = cashClearingViewModel.ProposalRef.ProposalUnitCode.UnitName;
                        }
                        reportDataSource.StartDate = cashClearingViewModel.ProposalRef.StartDate.ToString("dd/MM/yyyy");
                        reportDataSource.EndDate = cashClearingViewModel.ProposalRef.EndDate.ToString("dd/MM/yyyy");
                        reportDataSource.ProposalNumber = cashClearingViewModel.ProposalRef.DocumentNumber;
                        reportDataSource.ProposalName = cashClearingViewModel.ProposalRef.Title;
                    }
                    if (cashClearingViewModel.SubmittedDate != null)
                    {
                        DateTime submitDate = (DateTime)cashClearingViewModel.SubmittedDate;
                        reportDataSource.SubmitDate = submitDate.ToString("dd/MM/yyyy");
                    }

                    if (line.NetAmount != null) reportDataSource.LineNetAmount = (decimal) line.NetAmount;
                    reportDataSource.LineDescription = line.Description;
                    //reportDataSource.LineUnit = ;
                    reportDataSource.LineActual = (decimal)line.NetActualNoVatTax;
                    reportDataSource.LineTax = (decimal) line.TaxActual;
                    reportDataSource.LineVat = (decimal)line.VatActual;
                    reportDataSource.LineNetActual = (decimal) line.NetActual;
                    //if (line.Actual != null)
                    //{
                    //    reportDataSource.LineActual = (decimal) line.Actual;
                    //}

                    if (cashClearingViewModel.CashAdvanceViewModel != null)
                    {
                        reportDataSource.CashAdvanceNo = cashClearingViewModel.CashAdvanceViewModel.DocumentNumber;
                        //reportDataSource.CashAdvanceBudget = cashClearingViewModel.CashAdvanceViewModel.Budget;
                        reportDataSource.CashAdvanceBudget = 0;
                        foreach (var cashAdvanceLines in cashClearingViewModel.CashAdvanceViewModel.CashAdvanceLines)
                        {
                            reportDataSource.CashAdvanceBudget = (decimal) (reportDataSource.CashAdvanceBudget +
                                                                            cashAdvanceLines.NetAmount);
                        }
                        reportDataSource.Rebate = reportDataSource.CashAdvanceBudget - sumNetActual;
                        reportDataSource.RebateInCharacter = reportDataSource.Rebate.ToWords();
                    }

                    reportDataSource.LineRemark = line.Remark;
                    reportDataSource.BudgetSurplus = cashClearingViewModel.Budget;
                    reportDataSource.BudgetSurplusInCharacter = cashClearingViewModel.Budget.ToWords();
                    reportDataSource.Title = cashClearingViewModel.Title;
                    reportDataSource.Tax = line.Tax;
                    reportDataSource.Vat = line.Vat;
                    reportDataSource.Date = DateTime.Now.ToString("dd/MM/yyyy");
                    if (cashClearingViewModel.UnitCode != null)
                    {
                        reportDataSource.UnitCode = cashClearingViewModel.UnitCode.UnitCode;
                        reportDataSource.UnitName = cashClearingViewModel.UnitCode.UnitName;
                    }

                    if (cashClearingViewModel.Description != null)
                    {
                        reportDataSource.Objective = cashClearingViewModel.Description.Replace("font-family", string.Empty);
                        var imgs = reportDataSource.Objective.Contains("img");
                        reportDataSource.ObjectiveImg = imgs ? "*ดูรูปเพิ่มเติมได้ในระบบ" : "";
                    }

                    if (cashClearingViewModel.Company != null)
                    {
                        var company = _companyService.Find(m => m.VendorCode.Equals(cashClearingViewModel.Company.VendorCode));
                        reportDataSource.Company = company.CompanyName;
                    }
                    else
                    {
                        var dataCompany = _companyService.Find(m => m.VendorCode.Equals(cashClearingViewModel.Branch.PlaceCode));
                        reportDataSource.Company = dataCompany.CompanyName;
                    }

                    if (cashClearingViewModel.ExpenseTopic != null)
                    {
                        reportDataSource.ExpenseTopic = cashClearingViewModel.ExpenseTopic.ExpenseTopicCode + " - " + cashClearingViewModel.ExpenseTopic.ExpenseTopicName;
                    }

                    if (cashClearingViewModel.APCode != null)
                    {
                        reportDataSource.APCode = cashClearingViewModel.APCode.APCode + " - " + cashClearingViewModel.APCode.APName;
                    }

                    list.Add(reportDataSource);
                }
                return list;
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<ApproverDataSource> ApproversDataMapper(IEnumerable<MPS_CashClearingApproval> approversData)
        {
            if (approversData != null)
            {
                var list = new List<ApproverDataSource>();
                foreach (var approverData in approversData)
                {
                    var reportApproverDataSource = new ApproverDataSource() { };
                    reportApproverDataSource.ApproverId = approverData.Id;
                    reportApproverDataSource.ApproverSequence = approverData.ApproverSequence;
                    reportApproverDataSource.ApproverUsername = approverData.ApproverUserName;
                    reportApproverDataSource.ApproverFullName = approverData.ApproverFullName;
                    reportApproverDataSource.ApproverLevel = approverData.ApproverLevel;
                    reportApproverDataSource.ApproverPosition = approverData.Position;
                    var comment = approverData.CashClearingTable.Comments;
                    var approverCheck = approverData.ApproverLevel + " - Approval";
                    if (comment != null)
                    {
                        comment = comment.Where(m => m.Activity.ToLower() != "start").ToList();
                        var approve = comment.LastOrDefault(m => m.CreatedBy.Equals(approverData.ApproverUserName, StringComparison.InvariantCultureIgnoreCase) &&
                      m.Action.Equals(DocumentStatusFlag.Approve.ToString(), StringComparison.InvariantCultureIgnoreCase) &&
                      m.Activity.Equals(approverCheck, StringComparison.InvariantCultureIgnoreCase));
                        if (approve != null)
                        {
                            reportApproverDataSource.ApproveDateTime = approve.CreatedDate != null ? approve.CreatedDate.Value.ToString("dd/MM/yyyy") : "";
                        }
                        else
                        {
                            reportApproverDataSource.ApproveDateTime = "";
                        }
                    }

                    if (approverData.ApproverLevel.Equals(Approval.ApproveLevelList.Propose.GetDescription()))
                        reportApproverDataSource.ApproverLevelKey = Approval.ApproveKey.Propose;
                    else if (approverData.ApproverLevel.Equals(Approval.ApproveLevelList.Accept.GetDescription()))
                        reportApproverDataSource.ApproverLevelKey = Approval.ApproveKey.Accept;
                    else if (approverData.ApproverLevel.Equals(Approval.ApproveLevelList.Approval.GetDescription()))
                        reportApproverDataSource.ApproverLevelKey = Approval.ApproveKey.Approval;
                    else if (approverData.ApproverLevel.Equals(Approval.ApproveLevelList.FinalApproval.GetDescription()))
                        reportApproverDataSource.ApproverLevelKey = Approval.ApproveKey.FinalApproval;
                    else
                    {
                        reportApproverDataSource.ApproverLevelKey = 0;
                    }

                    list.Add(reportApproverDataSource);
                }
                return list;
            }
            else
            {
                return null;
            }
        }
    }
}