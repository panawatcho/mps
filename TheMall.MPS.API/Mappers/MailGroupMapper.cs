﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TheMall.MPS.Models.Master;
using TheMall.MPS.ViewModels.Master;

namespace TheMall.MPS.API.Mappers
{
    public class MailGroupMapperProfile : AutoMapper.Profile
    {
        public MailGroupMapperProfile()
        {
            CreateMap<MPS_M_MailGroup, MailGroupViewModel>()
              .ReverseMap();
        }
    }
    public static class MailGroupMapper
    {
        public static MPS_M_MailGroup ToModel(this MailGroupViewModel viewModel)
        {
            if (viewModel == null)
                return null;
            var model = new MPS_M_MailGroup()
            {
                GroupCode = viewModel.GroupCode,
                Email = viewModel.Email,
                InActive = viewModel.InActive,
            };
            return model;
        }


        public static MailGroupViewModel ToViewModel(this MPS_M_MailGroup model)
        {
            if (model == null)
                return null;
            var viewModel = new MailGroupViewModel()
            {
                GroupCode = model.GroupCode,
                Email = model.Email,
                InActive = model.InActive,
            };
            return viewModel;
        }

        public static IEnumerable<MailGroupViewModel> ToViewModels(this IEnumerable<MPS_M_MailGroup> models)
        {
            return models != null ? models.Select(ToViewModel) : Enumerable.Empty<MailGroupViewModel>();
        }
        public static IEnumerable<MPS_M_MailGroup> ToModels(IEnumerable<MailGroupViewModel> viewModels, IEnumerable<MPS_M_MailGroup> lines)
        {
            return viewModels != null ? viewModels.Select(ToModel) : Enumerable.Empty<MPS_M_MailGroup>();
        }
    }
}