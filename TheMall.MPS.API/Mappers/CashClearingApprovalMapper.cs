﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TheMall.MPS.API.Services;
using TheMall.MPS.Mappers.Abstracts;
using TheMall.MPS.Models.CashAdvance;
using TheMall.MPS.Models.CashClearing;
using TheMall.MPS.Models.PettyCash;
using TheMall.MPS.ViewModels.CashClearing;
using TheMall.MPS.ViewModels.Proposal;

namespace TheMall.MPS.API.Mappers
{
    public interface ICashClearingApprovalMapper : ILineMapper
    {
        MPS_CashClearingApproval ToModel(CashClearingApprovalViewModel viewModel,
            IEnumerable<MPS_CashClearingApproval> lines);

        CashClearingApprovalViewModel ToViewModel(MPS_CashClearingApproval model);
        IEnumerable<CashClearingApprovalViewModel> ToViewModels(IEnumerable<MPS_CashClearingApproval> models);

        IEnumerable<MPS_CashClearingApproval> ToModels(IEnumerable<CashClearingApprovalViewModel> viewModels,
            IEnumerable<MPS_CashClearingApproval> lines);

        CashClearingApprovalViewModel CashAdvanceToViewModel(MPS_CashAdvanceApproval model);
        IEnumerable<CashClearingApprovalViewModel> CashAdvanceToViewModels(IEnumerable<MPS_CashAdvanceApproval> models);

    }

    public class CashClearingApprovalMapper : BaseLineMapper, ICashClearingApprovalMapper
    { private readonly IEmployeeTableService _employeeTableService;
    public CashClearingApprovalMapper(IEmployeeTableService employeeTableService)
        {
            _employeeTableService = employeeTableService;
        }
        public MPS_CashClearingApproval ToModel(CashClearingApprovalViewModel viewModel,
            IEnumerable<MPS_CashClearingApproval> lines)
        {
            if (viewModel == null || string.IsNullOrEmpty(viewModel.ApproverUserName))
            {
                return null;
            }

            if (string.IsNullOrEmpty(viewModel.ApproverEmail))
                throw new Exception("Please select approver in Approver Grid every levels!!!");


            var model = InitLineTable(viewModel, lines);
            if (model == null)
            {
                return null;
            }
            model.ApproverSequence = viewModel.ApproverSequence;
            model.ApproverLevel = viewModel.ApproverLevel;
            if (viewModel.Employee != null)
            {
                model.ApproverUserName = viewModel.Employee.Username;
                model.ApproverFullName = viewModel.Employee.FullName;
            }
            model.ApproverEmail = viewModel.ApproverEmail;
            model.Position = viewModel.Position;
            model.Department = viewModel.Department;
            model.LockEditable = viewModel.LockEditable;
            if (viewModel.DueDate != null)
                model.DueDate = viewModel.DueDate.Value.ToLocalTime().Date.AddDays(1).AddSeconds(-1).ToUniversalTime(); ;
            ;
            return model;
        }

        public CashClearingApprovalViewModel ToViewModel(MPS_CashClearingApproval model)
        {
            if (model == null)
            {
                return null;
            }
            var viewModel = this.InitLineViewModel<CashClearingApprovalViewModel>(model);
            if (model.ApproverUserName != null)
                viewModel.Employee = _employeeTableService.FindByUsername(model.ApproverUserName).ToViewModel();
            viewModel.ApproverSequence = model.ApproverSequence;
            viewModel.ApproverLevel = model.ApproverLevel;
            viewModel.ApproverUserName = model.ApproverUserName;
            viewModel.ApproverFullName = model.ApproverFullName;
            viewModel.ApproverEmail = model.ApproverEmail;
            viewModel.Position = model.Position;
            viewModel.Department = model.Department;
            viewModel.DueDate = model.DueDate;
            viewModel.LockEditable = model.LockEditable;
            viewModel.ApproveStatus = model.ApproveStatus;
            return viewModel;
        }

        public IEnumerable<CashClearingApprovalViewModel> ToViewModels(IEnumerable<MPS_CashClearingApproval> models)
        {
            return models != null ? models.Select(ToViewModel) : Enumerable.Empty<CashClearingApprovalViewModel>();
        }

        public IEnumerable<MPS_CashClearingApproval> ToModels(IEnumerable<CashClearingApprovalViewModel> viewModels,
            IEnumerable<MPS_CashClearingApproval> lines)
        {
            return viewModels != null
                ? viewModels.Select(m => ToModel(m, lines)).Where(m => m != null)
                : Enumerable.Empty<MPS_CashClearingApproval>();
        }

        public CashClearingApprovalViewModel CashAdvanceToViewModel(MPS_CashAdvanceApproval model)
        {
            if (model == null)
            {
                return null;
            }
            var viewModel = this.InitLineViewModel<CashClearingApprovalViewModel>(model);
            if (model.ApproverUserName != null)
                viewModel.Employee = _employeeTableService.FindByUsername(model.ApproverUserName).ToViewModel();
            viewModel.ApproverSequence = model.ApproverSequence;
            viewModel.ApproverLevel = model.ApproverLevel;
            viewModel.ApproverUserName = model.ApproverUserName;
            viewModel.ApproverFullName = model.ApproverFullName;
            viewModel.ApproverEmail = model.ApproverEmail;
            viewModel.Position = model.Position;
            viewModel.Department = model.Department;
            viewModel.DueDate = model.DueDate;
            viewModel.LockEditable = model.LockEditable;
            viewModel.Id = 0;
            viewModel.ParentId = 0;
            return viewModel;
        }


        public IEnumerable<CashClearingApprovalViewModel> CashAdvanceToViewModels(IEnumerable<MPS_CashAdvanceApproval> models)
        {
            return models != null ? models.Select(CashAdvanceToViewModel) : Enumerable.Empty<CashClearingApprovalViewModel>();
        }
       
    }
}