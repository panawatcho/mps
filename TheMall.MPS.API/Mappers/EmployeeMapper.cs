﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TheMall.MPS.Models;
using TheMall.MPS.ViewModels;


namespace TheMall.MPS.API.Mappers
{
    public class EmployeeMapperProfile : AutoMapper.Profile
    {
        public EmployeeMapperProfile()
        {
            CreateMap<MPS_M_EmployeeTable, EmployeeViewModel>()
                .ReverseMap();
        }
    }
    public static class EmployeeMapper
    {

        public static MPS_M_EmployeeTable ToModel(this EmployeeViewModel viewModel)
        {
            if (viewModel == null)
                return null;

            var model = new MPS_M_EmployeeTable()
            {
                EmpId = viewModel.EmpId,
                EmpStatus = viewModel.EmpStatus,
                ThFName = viewModel.ThFName,
                ThLName = viewModel.ThLName,
                EnFName = viewModel.EnFName,
                EnLName = viewModel.EnLName,
                DepartmentCode = viewModel.DepartmentCode,
                DepartmentName = viewModel.DepartmentName,
                PositionCode = viewModel.PositionCode,
                PositionName = viewModel.PositionName,
                Email = viewModel.Email,
                TelephoneNo = viewModel.TelephoneNo,
                CompanyCode = viewModel.CompanyCode,
                CompanyName = viewModel.CompanyName,
                Username = viewModel.Username
            };
            return model;
        }

      
        public static EmployeeViewModel ToViewModel(this MPS_M_EmployeeTable model)
        {

            if (model == null)
                return null;

            var viewModel = new EmployeeViewModel();
            viewModel.EmpId = model.EmpId;
            viewModel.EmpStatus = model.EmpStatus;
       
            viewModel.ThFName = model.ThFName ?? "";
            viewModel.ThLName = model.ThLName ?? "";
            viewModel.EnFName = model.EnFName ?? "";
            viewModel.EnLName = model.EnLName ?? "";
            viewModel.DepartmentCode = model.DepartmentCode ?? "";
            viewModel.DepartmentName = model.DepartmentName ?? "";
            viewModel.PositionCode = model.PositionCode ?? "";
            viewModel.PositionName = model.PositionName ?? "";
            viewModel.PositionName = model.PositionName ?? "";
            viewModel.Email = model.Email ?? "";
            viewModel.TelephoneNo = model.TelephoneNo ?? "";
            viewModel.CompanyCode = model.CompanyCode ?? "";
            viewModel.CompanyName = model.CompanyName ?? "";
            viewModel.Username = model.Username ?? "";
       

            // var unitcode = _unitCode.GetAll();
            //var unitforemp = _unitCodeforemp.GetWhere(m => m.Username == model.RequesterUserName);
            //var unitCode = from unitemp in unitforemp
            //               join unit in unitcode on unitemp.UnitCode equals unit.UnitCode
            //               select new
            //               {
            //                   code
            //               };
            return viewModel;
        }

        //public static IEnumerable<MPS_EmployeeTable> ToModel(IEnumerable<EmployeeViewModel> viewModels, IEnumerable<MPS_EmployeeTable> lines)
        //{
        //    return viewModels != null ? viewModels.Select(m => ToModel(m, lines)).Where(m => m != null) : Enumerable.Empty<MPS_EmployeeTable>();
        //}

        public static IEnumerable<EmployeeViewModel> ToViewModel(this IEnumerable<MPS_M_EmployeeTable> models)
        {
            return models != null ? models.Select(ToViewModel) : Enumerable.Empty<EmployeeViewModel>();
        }
        //public static EmployeeSearchViewModel ToSearchViewModel(this Employee model)
        //{
        //    if (model == null)
        //        return null;

        //    return AutoMapper.Mapper.Map<EmployeeSearchViewModel>(model);
        //}

        //public static IEnumerable<EmployeeSearchViewModel> ToSearchViewModel(this IEnumerable<Employee> models)
        //{
        //    return models != null ? models.Select(ToSearchViewModel) : Enumerable.Empty<EmployeeSearchViewModel>();
        //}

        public static RequesterViewModel ToRequesterViewModel(this EmployeeViewModel viewModel)
        {
            if (viewModel == null)
                return null;

            var requesterViewModel = new RequesterViewModel()
            {
                EmpId = viewModel.EmpId,
                EmpStatus = viewModel.EmpStatus,
                ThFName = viewModel.ThFName,
                ThLName = viewModel.ThLName,
                EnFName = viewModel.EnFName,
                EnLName = viewModel.EnLName,
                Username = viewModel.Username,
                UnitCode = viewModel.UnitCode
            };

            return requesterViewModel;
        }

    }
}