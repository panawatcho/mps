﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using TheMall.MPS.Infrastructure.Security;
using TheMall.MPS.ViewModels.Menu;

namespace TheMall.MPS.API.Mappers
{
    public static class MenuMapper
    {
        public static IEnumerable<MenuItemViewModel> ToViewModel(this IReadOnlyList<MenuItem> models)
        {
            if (models == null || !models.Any())
                return Enumerable.Empty<MenuItemViewModel>();

            var viewModels = new List<MenuItemViewModel>();
            // DO NOT USE m.Children

            foreach (var model in models.Where(m => m.ParentId == null))
            {
                var viewModelslv1 = new MenuItemViewModel();
                var localLv1 = model;
                viewModelslv1 = localLv1.ToViewModel();

                //if (model.Children == null || !model.Children.Any())
                //{
                //}
                //else
                //{
                //    viewModelslv1 = localLv1.ToViewModel(models.Where(m => m.ParentId == localLv1.Id && m.Id != localLv1.Id && m.ParentId != m.Id));

                //    var viewModelslv2 = new List<MenuItemViewModel>();
                //    foreach (var modellv2 in model.Children)
                //    {
                //        var localLv2 = modellv2;
                //        var lv2 =
                //            modellv2.ToViewModel(model.Children.Where(m => m.ParentId == localLv2.Id && m.Id != localLv2.Id && m.ParentId != m.Id));

                //        var viewModelslv3 = new List<MenuItemViewModel>();
                //        foreach (var modellv3 in modellv2.Children)
                //        {
                //            var localLv3 = localLv1;
                //            var lv3 =
                //                modellv3.ToViewModel(
                //                    modellv2.Children.Where(
                //                        m => m.ParentId == localLv3.Id && m.Id != localLv3.Id && m.ParentId != m.Id));
                //            if (models.Any(m => m.Id == modellv3.Id))
                //                viewModelslv3.Add(lv3);
                //        }
                //        lv2.Children = viewModelslv3;
                //        if (models.Any(m => m.Id == modellv2.Id))
                //            viewModelslv2.Add(lv2);
                //    }
                //    viewModelslv1.Children = viewModelslv2;
                //}
                viewModels.Add(viewModelslv1);
            }
            return viewModels.Where(m => /*!string.IsNullOrEmpty(m.Link)*/ m.Link != null || (m.Children != null && m.Children.Any()));
        }

        private static Func<ICollection<MenuItem>, string, IEnumerable<MenuItem>> _childSelectorExpression =
            (ICollection<MenuItem> m, string parentId) => m.Where(n => n.ParentId == parentId && n.Id != parentId);

        //public static MenuItemViewModel ToViewModel(this MenuItem model, IEnumerable<MenuItem> children)
        //{
        //    var viewModel = model.ToViewModel();

        //    var childrenViewModel = new List<MenuItemViewModel>();
        //    if (children != null)
        //    {
        //        childrenViewModel.AddRange(children.Select(child => child.ToViewModel()));
        //    }
        //    viewModel.Children = childrenViewModel;

        //    return viewModel;
        //}

        public static MenuItemViewModel ToViewModel(this MenuItem model)
        {
            var viewModel = new MenuItemViewModel();

            viewModel.Id = model.Id;
            viewModel.Html = model.Html;
            viewModel.Link = model.RefererId;// ?? model.Link;
            viewModel.IconClass = model.IconClass;
            viewModel.Text = model.Text;
            viewModel.NewTab = model.NewTab;

            if (model.Children != null)
            {
                viewModel.Children = new List<MenuItemViewModel>(_childSelectorExpression(model.Children, model.Id).Select(child => child.ToViewModel()));
            }

            return viewModel;
        }
    }
}