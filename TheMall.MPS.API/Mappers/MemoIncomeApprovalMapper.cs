﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TheMall.MPS.API.Services;
using TheMall.MPS.Mappers.Abstracts;
using TheMall.MPS.Models.MemoIncome;
using TheMall.MPS.ViewModels.Proposal;

namespace TheMall.MPS.API.Mappers
{
    public interface IMemoIncomeApprovalMapper : ILineMapper
    {
        MPS_MemoIncomeApproval ToModel(MemoIncomeApprovalViewModel viewModel, IEnumerable<MPS_MemoIncomeApproval> lines);
        MemoIncomeApprovalViewModel ToViewModel(MPS_MemoIncomeApproval model);

        IEnumerable<MemoIncomeApprovalViewModel> ToViewModels(IEnumerable<MPS_MemoIncomeApproval> models);
        IEnumerable<MPS_MemoIncomeApproval> ToModels(IEnumerable<MemoIncomeApprovalViewModel> viewModels, IEnumerable<MPS_MemoIncomeApproval> lines);
    }

    public class MemoIncomeApprovalMapper : BaseLineMapper, IMemoIncomeApprovalMapper
    {
         private readonly IEmployeeTableService _employeeTableService;

         public MemoIncomeApprovalMapper(IEmployeeTableService employeeTableService)
        {
            _employeeTableService = employeeTableService;
        }
        public MPS_MemoIncomeApproval ToModel(MemoIncomeApprovalViewModel viewModel, IEnumerable<MPS_MemoIncomeApproval> lines)
        {
            if (viewModel == null || string.IsNullOrEmpty(viewModel.ApproverUserName))
            {
                return null;
            }

            if (string.IsNullOrEmpty(viewModel.ApproverEmail))
                throw new Exception("Please select approver in Approver Grid every levels!!!");

            var model = InitLineTable(viewModel, lines);

            if (model == null)
            {
                return null;
            }
             model.ApproverSequence = viewModel.ApproverSequence;
             model.ApproverLevel = viewModel.ApproverLevel;
             if (viewModel.Employee != null)
             {
                 model.ApproverUserName = viewModel.Employee.Username;
                 model.ApproverFullName = viewModel.Employee.FullName;
             }

            model.ApproverEmail = viewModel.ApproverEmail;
            model.Position = viewModel.Position;
            model.Department = viewModel.Department;
            model.LockEditable = viewModel.LockEditable;

             if (viewModel.DueDate != null)
                 model.DueDate = viewModel.DueDate.Value.ToLocalTime().Date.AddDays(1).AddSeconds(-1).ToUniversalTime(); ;
             ;
            return model;
        }

        public MemoIncomeApprovalViewModel ToViewModel(MPS_MemoIncomeApproval model)
        {
            if (model == null)
            {
                return null;
            }
            var viewModel = this.InitLineViewModel<MemoIncomeApprovalViewModel>(model);

            if (model.ApproverUserName != null)
                viewModel.Employee = _employeeTableService.FindByUsername(model.ApproverUserName).ToViewModel();
            viewModel.ApproverSequence = model.ApproverSequence;
            viewModel.ApproverLevel = model.ApproverLevel;
            viewModel.ApproverUserName = model.ApproverUserName;
            viewModel.ApproverFullName = model.ApproverFullName;
            viewModel.ApproverEmail = model.ApproverEmail;
            viewModel.Position = model.Position;
            viewModel.Department = model.Department;
            viewModel.DueDate = model.DueDate;
            viewModel.LockEditable = model.LockEditable;
            viewModel.ApproveStatus = model.ApproveStatus;
            return viewModel;
        }

        public IEnumerable<MemoIncomeApprovalViewModel> ToViewModels(IEnumerable<MPS_MemoIncomeApproval> models)
        {
            return models != null ? models.Select(ToViewModel) : Enumerable.Empty<MemoIncomeApprovalViewModel>();
        }
        public IEnumerable<MPS_MemoIncomeApproval> ToModels(IEnumerable<MemoIncomeApprovalViewModel> viewModels, IEnumerable<MPS_MemoIncomeApproval> lines)
        {
            return viewModels != null ? viewModels.Select(m => ToModel(m, lines)).Where(m => m != null) : Enumerable.Empty<MPS_MemoIncomeApproval>();
        }

       
    }
}