﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper.Execution;
using TheMall.MPS.Models.Master;
using TheMall.MPS.ViewModels.Master;

namespace TheMall.MPS.API.Mappers
{
    public static class AllocationBasisMapper
    {
        public static AllocationBasisViewModel ToViewModel(this MPS_M_AllocationBasis model)
        {
            if (model == null)
                return null;
            var viewModel = new AllocationBasisViewModel()
            {
                AllocCode = model.AllocCode,
                Description = model.Description,
                InActive = model.InActive,
                MRT = model.MRT,
                B5 = model.B5,
                B7 = model.B7,
                M02 = model.M02,
                M03 = model.M03,
                M05= model.M05,
                M06= model.M06,
                M07= model.M07,
                M08= model.M08,
                M09= model.M09,
                M10= model.M10,
                M11= model.M11,
                M12= model.M12,
                M13= model.M13,
                M14= model.M14,
                M15= model.M15,
                M16= model.M16,
                M17= model.M17,
                V1= model.V1,
                V2= model.V2,
                V3 = model.V3,
                V5 = model.V5,
                V9 = model.V9,
                TR = model.TR,
                RE = model.RE,
            };
            return viewModel;

        }
        public static IEnumerable<AllocationBasisViewModel> ToViewModels(this IEnumerable<MPS_M_AllocationBasis> models)
        {
            return models != null ? models.Select(ToViewModel) : Enumerable.Empty<AllocationBasisViewModel>();
        }
        public static MPS_M_AllocationBasis ToModel(this AllocationBasisViewModel viewModel)
        {
            if (viewModel == null)
                return null;
            var model = new MPS_M_AllocationBasis()
            {   Id = viewModel.Id,
                AllocCode = viewModel.AllocCode,
                Description = viewModel.Description,
                InActive = viewModel.InActive,
                MRT = viewModel.MRT,
                B5 = viewModel.B5,
                B7 = viewModel.B7,
                M02 = viewModel.M02,
                M03 = viewModel.M03,
                M05 = viewModel.M05,
                M06 = viewModel.M06,
                M07 = viewModel.M07,
                M08 = viewModel.M08,
                M09 = viewModel.M09,
                M10 = viewModel.M10,
                M11 = viewModel.M11,
                M12 = viewModel.M12,
                M13 = viewModel.M13,
                M14 = viewModel.M14,
                M15 = viewModel.M15,
                M16 = viewModel.M16,
                M17 = viewModel.M17,
                V1 = viewModel.V1,
                V2 = viewModel.V2,
                V3 = viewModel.V3,
                V5 = viewModel.V5,
                V9 = viewModel.V9,
                TR = viewModel.TR,
                RE = viewModel.RE,
            };
            return model;
        }
        public static IEnumerable<MPS_M_AllocationBasis> ToModels(IEnumerable<AllocationBasisViewModel> viewModels, IEnumerable<MPS_M_AllocationBasis> lines)
        {
            return viewModels != null ? viewModels.Select(ToModel) : Enumerable.Empty<MPS_M_AllocationBasis>();
        }
   
    }
}