﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using TheMall.MPS.API.Mappers.Abstracts;
using TheMall.MPS.API.Services;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Models.Enums;
using TheMall.MPS.Models.Master;
using TheMall.MPS.Models.Memo;
using TheMall.MPS.Models.PettyCash;
using TheMall.MPS.Models.Proposal;
using TheMall.MPS.ViewModels;
using TheMall.MPS.ViewModels.Master;
using TheMall.MPS.ViewModels.Memo;
using TheMall.MPS.ViewModels.PettyCash;

namespace TheMall.MPS.API.Mappers
{
    public interface IMemoMapper : IWorkflowTableMapper<MPS_MemoTable>
    {
        MPS_MemoTable ToModel(MemoViewModel viewModel);
        MemoViewModel ToViewModel(MPS_MemoTable model);
        MPS_MemoTable DeleteBy(MPS_MemoTable model, EmployeeViewModel employeeViewModel);
    }
    public class MemoMapper : BaseWorkflowTableMapper<MPS_MemoTable>, IMemoMapper
    {
            private readonly IMemoService _mpsWorkflowTableService;
            private readonly IMemoLineMapper _lineMapper;
            private readonly IExpenseTopicService _expenseTopicService;
            private readonly IAPcodeService _apCodeService;
            //private readonly IProposalLineMapper _proposalLineMapper;
            private readonly IProposalMapper _proposalMapper;
            private readonly IMemoApprovalMapper _approvalMapper;
            private readonly IProposalLineService _proposalLineService;
            private readonly IEmployeeTableService _employeeTableService;
            private readonly ITypeMemoService _typeMemo;
            private readonly IProposalService _proposalService;
            private readonly IUnitCodeService _unitCodeService;
            private readonly IPlaceService _placeService;
            private readonly IBudgetProposalTransService _budgetProposalTransService;
            private readonly IProposalIncomeDepositMapper _incomeDepositMapper;
          
            private readonly IProposalIncomeDepositService _incomeDepositService;
          public MemoMapper(IMemoService mpsWorkflowTableService,IMemoLineMapper lineMapper,
              IExpenseTopicService expenseTopicService,
              IAPcodeService apCodeService,
              IProposalLineService proposalLineService, 
              IProposalMapper proposalMapper, 
              IMemoApprovalMapper approvalMapper, 
              IEmployeeTableService employeeTableService, 
              ITypeMemoService typeMemo, 
              IProposalService proposalService, 
              IUnitCodeService unitCodeService, 
              IPlaceService placeService,
              IAccountingService accountingService,
              IBudgetProposalTransService budgetProposalTransService, 
              IProposalIncomeDepositMapper incomeDepositMapper, 
              IProposalIncomeDepositService incomeDepositService)
                : base(mpsWorkflowTableService, employeeTableService, placeService, accountingService)
          {
              _mpsWorkflowTableService = mpsWorkflowTableService;
              _lineMapper = lineMapper;
              _expenseTopicService = expenseTopicService;
              _apCodeService = apCodeService;
              _proposalMapper = proposalMapper;
              _approvalMapper = approvalMapper;
              _employeeTableService = employeeTableService;
              _typeMemo = typeMemo;
              _proposalService = proposalService;
              _unitCodeService = unitCodeService;
              _placeService = placeService;
              _budgetProposalTransService = budgetProposalTransService;
              _incomeDepositMapper = incomeDepositMapper;
              _incomeDepositService = incomeDepositService;
              _proposalLineService = proposalLineService;
          }
        public MPS_MemoTable ToModel(MemoViewModel viewModel)
        {
            if (viewModel == null)
            {
                return null;
            }
            var model = InitWorkflowTable(viewModel);
            if (viewModel.ExpenseTopic != null)
            {
                model.ExpenseTopicCode = viewModel.ExpenseTopic.ExpenseTopicCode;

            }
            if (viewModel.APCode != null)
            {
                model.APCode = viewModel.APCode.APCode;
            }
            if (viewModel.ProposalRef != null)
            {
                var proposalLine = _proposalLineService.Find(m => m.Id == viewModel.ProposalLineId);
                model.ProposalLine = proposalLine;
                model.ProposalLineID = viewModel.ProposalLineId;
                model.ProposalRefID = viewModel.ProposalRef.Id;
                model.ProposalRefDocumentNumber = viewModel.ProposalRef.DocumentNumber;

            }
            if (viewModel.UseProposal != null)
            {
                model.UseProposalID = viewModel.UseProposal.Id;
                model.UseProposalDocumentNumber = viewModel.UseProposal.DocumentNumber;
            }
            if (viewModel.Branch != null)
            {
                model.Branch = viewModel.Branch.PlaceCode;
            }
            model.Description = viewModel.Description;
            model.BudgetAPCode = viewModel.BudgetAPCode;
            model.BudgetDetail = viewModel.BudgetDetail;
            model.PaymentName = viewModel.PaymentName;
            model.PaymentDepartment = viewModel.PaymentDepartment;
            if (viewModel.PayToUnitCode != null)
            {
                model.PayToUnitCode = viewModel.PayToUnitCode.UnitCode;
            }
            else
            {
                model.PayToUnitCode = null;
            }

            if (viewModel.MemoType != null)
            {
                model.MemoType = viewModel.MemoType.TypeMemoCode;
                model.ActualCharge = viewModel.MemoType.ActualCharge;
            }
            if (viewModel.IncomeDepositViewModel != null &&
                (viewModel.MemoType != null && viewModel.MemoType.ActualCharge))
            {
                model.IncomeDepositId = viewModel.IncomeDepositViewModel.Id;
            }
            else
            {
                model.IncomeDepositId = null;
            }

            return model;
        }
        public MemoViewModel ToViewModel(MPS_MemoTable model)
        {
            if (model == null)
            {
                return null;
            }
            var viewModel = InitViewModel<MemoViewModel>(model);
          //  viewModel.Requester = _employeeTableService.FindByUsername(model.RequesterUserName).ToViewModel();
            if (model.ProposalLine != null)
            {
                viewModel.ProposalRef = _proposalMapper.ToProposalInfoViewModel(model.ProposalLine.ProposalTable);
            }
            if (model.UseProposalID != null)
            {
               var useProposal = _proposalService.FirstOrDefault(m => m.Id == model.UseProposalID);
               viewModel.UseProposal = _proposalMapper.ToProposalInfoViewModel(useProposal);
            }
            if (model.ExpenseTopicCode != null)
            {
                viewModel.ExpenseTopic = _expenseTopicService.Find(m=>m.ExpenseTopicCode==model.ExpenseTopicCode).ToViewModel();
            }
            if (model.APCode != null && viewModel.ProposalRef!=null)
            {
                viewModel.APCode =
                    viewModel.ProposalRef.APCode.Find( m => m.APCode == model.APCode && m.ProposalLineId == model.ProposalLineID);
            }
            if (model.MemoLine != null)
            {
                viewModel.MemoLines = _lineMapper.ToViewModels(model.MemoLine);
            }
            viewModel.Comments = model.Comments.ToViewModel();

            viewModel.Description = model.Description;
            viewModel.BudgetAPCode = model.BudgetAPCode;
            viewModel.BudgetDetail = model.BudgetDetail;
            viewModel.PaymentName = model.PaymentName;
            viewModel.PaymentDepartment = model.PaymentDepartment;
            if (model.Branch != null)
            {
                var branch = _placeService.Find(m => m.PlaceCode.Equals(model.Branch));
                viewModel.Branch = branch.ToViewModel();
            }

            if (model.PayToUnitCode != null)
            {
                viewModel.PayToUnitCode =
                    _unitCodeService.FirstOrDefault(m => m.UnitCode == model.PayToUnitCode).ToViewModel();
                if (viewModel.PayToUnitCode.UnitCode == null)
                {
                    viewModel.PayToUnitCode.UnitCode = "";
                    viewModel.PayToUnitCode.UnitName = "";
                    viewModel.PayToUnitCode.TypeYearPlan = "";
                }
            }
            else
            {
                viewModel.PayToUnitCode = null;
            }

            viewModel.MemoApproval = _approvalMapper.ToViewModels(model.MemoApproval);
            if (model.MemoType != null)
            {
               var typeMemo = _typeMemo.FirstOrDefault(m=>m.TypeMemoCode.Equals(model.MemoType,StringComparison.InvariantCultureIgnoreCase)).ToViewModel();
               viewModel.MemoType = typeMemo;
            }
            viewModel.ProposalLineId = model.ProposalLineID;
             var budgetTrans = _budgetProposalTransService.GetBudgetReserve(model.ProposalRefID , model.ProposalLineID , model.APCode , model.UnitCode , Budget.DocumentType.Memo);
            viewModel.BudgetTrans = budgetTrans;
            viewModel.IncomeDepositViewModel = (model.IncomeDeposit != null)
                ? _incomeDepositMapper.ToViewModel(model.IncomeDeposit)
                : null;
            //viewModel.IncomeDepositViewModel = (model.IncomeDepositId != null)
            //     ? _incomeDepositMapper.ToViewModel(_incomeDepositService.LastOrDefault(m => m.Id == model.IncomeDepositId))
            //     : null;
            return viewModel;

        }

        public MPS_MemoTable DeleteBy(MPS_MemoTable model, EmployeeViewModel employeeViewModel)
        {
            if (model != null)
            {
                if (model.Id > 0)
                {
                    model.Deleted = true;
                    model.DeletedDate = DateTime.Now;
                    model.DeletedBy = employeeViewModel.Username;
                    model.DeletedByName = employeeViewModel.FullName;
                    return model;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }
    }
}