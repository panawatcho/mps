﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TheMall.MPS.API.Mappers.Abstracts;
using TheMall.MPS.API.Services;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Mappers.Abstracts;
using TheMall.MPS.Models.Memo;
using TheMall.MPS.Models.Proposal;
using TheMall.MPS.ViewModels.Memo;
using TheMall.MPS.ViewModels.Proposal;

namespace TheMall.MPS.API.Mappers
{
    public interface IProposalBudgetPlanMapper
    {
        MPS_ProposalBudgetPlan ToModel(MPS_ProposalLine lineModel);
    }

    public class ProposalBudgetPlanMapper : IProposalBudgetPlanMapper
    {
        private readonly IProposalBudgetPlanService _budgetPlanService;

       public ProposalBudgetPlanMapper(IProposalBudgetPlanService budgetPlanService)
       {
           _budgetPlanService = budgetPlanService;
       }

        public MPS_ProposalBudgetPlan ToModel(MPS_ProposalLine lineModel)
        {
            if (lineModel == null)
            {
                return null;
            }
               //var  model = new MPS_ProposalBudgetPlan();
            var model = _budgetPlanService.Find(m => m.ProposalId == lineModel.ParentId
                                                 && m.UnitCode == lineModel.UnitCode &&
                                                 m.APCode == lineModel.APCode);
            if (model == null)
            {
                    model = new MPS_ProposalBudgetPlan();
                    model.ProposalId = lineModel.ParentId;
                    model.BudgetPlan = lineModel.BudgetPlan;
                    model.APCode = lineModel.APCode;
                    model.UnitCode = lineModel.UnitCode;
                
            }
            else
            {
                     model.BudgetPlan = lineModel.BudgetPlan;
            }
            return model;
        }


        public IEnumerable<MPS_ProposalBudgetPlan> ToModels(IEnumerable<MPS_ProposalLine> viewModels)
        {
            return viewModels != null ? viewModels.Select(ToModel).Where(m => m != null) : Enumerable.Empty<MPS_ProposalBudgetPlan>();
        }

       
    }
}