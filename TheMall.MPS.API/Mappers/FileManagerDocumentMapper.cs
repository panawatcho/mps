﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TheMall.MPS.API.Mappers
{
    public static class FileManagerDocumentMapper
    {
        public static ViewModels.FileManager.DocumentViewModel ToViewModel(this Models.FileManager.FileManagerDocument model)
        {
            if (model == null)
            {
                return null;
            }

            var viewModel = new ViewModels.FileManager.DocumentViewModel();

            viewModel.Id = model.Id;
            viewModel.Name = model.Name;
            viewModel.DirectoryId = model.DirectoryId;

            return viewModel;
        }

        public static IEnumerable<ViewModels.FileManager.DocumentViewModel> ToViewModel(
            this IEnumerable<Models.FileManager.FileManagerDocument> model)
        {
            if (model == null)
            {
                return Enumerable.Empty<ViewModels.FileManager.DocumentViewModel>();
            }

            return model.Select(ToViewModel).ToListSafe();
        }
    }
}