﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using TheMall.MPS.API.Services;
using TheMall.MPS.Models.Master;
using TheMall.MPS.ViewModels.Master;

namespace TheMall.MPS.API.Mappers
{
    public class CompanyMapperProfile : AutoMapper.Profile
    {
        public CompanyMapperProfile()
        {
            CreateMap<MPS_M_Company, CompanyViewModel>().ReverseMap();
        }
    }

    public static class CompanyMapper
    {
        public static MPS_M_Company ToModel(this CompanyViewModel viewModel)
        {
            if (viewModel == null)
                return null;
            var model = new MPS_M_Company()
            {
                VendorCode = viewModel.VendorCode,
                CompanyCode = viewModel.CompanyCode,
                CompanyName = viewModel.CompanyName,
                BranchCode = viewModel.BranchCode,
                Address = viewModel.Address,
                Order = viewModel.Order,
                TelephoneNumber = viewModel.TelephoneNumber,
                Description = viewModel.Description,
                InActive = viewModel.InActive
            };
            return model;
            return Mapper.Map<MPS_M_Company>(viewModel);
        }

        public static CompanyViewModel ToViewModel(this MPS_M_Company model)
        {
            if (model == null)
                return null;
            var viewModel = new CompanyViewModel()
            {
                VendorCode = model.VendorCode,
                BranchCode = model.BranchCode,
                CompanyCode = model.CompanyCode,
                CompanyName = model.CompanyName,
                Address = model.Address,
                Order = model.Order,
                TelephoneNumber = model.TelephoneNumber,
                Description = model.Description,
                InActive = model.InActive
            };
            return viewModel;
            return Mapper.Map<CompanyViewModel>(model);
        }

        public static IEnumerable<CompanyViewModel> ToViewModels(this IEnumerable<MPS_M_Company> models)
        {
            return models != null ? models.Select(ToViewModel) : Enumerable.Empty<CompanyViewModel>();
        }

        public static IEnumerable<MPS_M_Company> ToModels(IEnumerable<CompanyViewModel> viewModels,
            IEnumerable<MPS_M_Company> lines)
        {
            return viewModels != null ? viewModels.Select(ToModel) : Enumerable.Empty<MPS_M_Company>();
        }

    }
}

//public interface ICompanyMapper
//{
//    MPS_M_Company ToModel(CompanyViewModel viewModel);
//    CompanyViewModel ToViewModel(MPS_M_Company model);
//}

//public class CompanyMapper : ICompanyMapper
//{
//    private readonly ISharedBranchService _sharedBranchService;

//    public CompanyMapper(ISharedBranchService sharedBranchService)
//    {
//        _sharedBranchService = sharedBranchService;
//    }

//    public MPS_M_Company ToModel(CompanyViewModel viewModel)
//    {
//        var model = new MPS_M_Company();

//        viewModel.VendorCode = model.VendorCode;
//        viewModel.BranchCode = model.BranchCode;
//        viewModel.CompanyCode = model.CompanyCode;
//        viewModel.CompanyName = model.CompanyName;
//        viewModel.Address = model.Address;
//        viewModel.Order = model.Order;
//        viewModel.TelephoneNumber = model.TelephoneNumber;
//        viewModel.Description = model.Description;
//        viewModel.InActive = model.InActive;

//        return model;
//    }

//    public CompanyViewModel ToViewModel(MPS_M_Company model)
//    {
//        var viewModel = new CompanyViewModel();

//        model.VendorCode = viewModel.VendorCode;
//        model.CompanyCode = viewModel.CompanyCode;
//        model.CompanyName = viewModel.CompanyName;
//        model.BranchCode = viewModel.BranchCode;
//        model.Address = viewModel.Address;
//        model.Order = viewModel.Order;
//        model.TelephoneNumber = viewModel.TelephoneNumber;
//        model.Description = viewModel.Description;
//        model.InActive = viewModel.InActive;

//        return viewModel;
//    }
//}