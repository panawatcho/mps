﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using TheMall.MPS.Models.Master;
using TheMall.MPS.Models.Proposal;
using TheMall.MPS.ViewModels.Master;
using TheMall.MPS.ViewModels.Proposal;

namespace TheMall.MPS.API.Mappers
{
    public class APcodeMapperProfile : AutoMapper.Profile
    {
        public APcodeMapperProfile()
        {
            CreateMap<MPS_M_APCode, APCodeViewModel>()
                .ReverseMap();
        }

    }
    public static class APCodeMapper
    {
        public static MPS_M_APCode ToModel(this APCodeViewModel viewModel)
        {
            if (viewModel == null)
                return null;
            var model = new MPS_M_APCode();

            model.APCode = viewModel.APCode;
            model.APName = viewModel.APName;
            model.ExpenseTopicCode = viewModel.ExpenseTopicCode;
            model.Description = viewModel.Description;
            model.CheckBudget = viewModel.CheckBudget;
            model.SendMail = viewModel.SendMail;
            model.Order = viewModel.Order;

            return model;
        }

        public static APCodeViewModel ToViewModel(this MPS_M_APCode model)
        {
            if (model == null)
                return null;
            var viewModel = new APCodeViewModel()
            {
                APCode = model.APCode,
                APName = model.APName,
                ExpenseTopicCode = model.ExpenseTopicCode,
                Description =model.Description,
                CheckBudget =model.CheckBudget,
                SendMail  =model.SendMail,
                Order = model.Order
                
            };
           
           
            return viewModel;

          
        }
        public static APCodeByProposalLineViewModel ToProposalViewModel(this ProposalLineViewModel model)
        {
            if (model == null)
                return null;
            var viewModel = new APCodeByProposalLineViewModel()
            {
                APCode = model.APCode.APCode,
                APName = model.APCode.APName,
                ExpenseTopicCode = model.ExpenseTopicCode,
                Description = model.APCode.Description,
                CheckBudget = model.CheckBudget,
                SendMail = model.APCode.SendMail,
                Order = model.APCode.Order,
                BudgetPlan = model.BudgetPlan,
                TotalBudgetPlan =  model.TotalBudgetPlan,
                DescriptionLine = model.Description,
                ProposalLineId = model.Id,
                UnitCode = model.UnitCode.UnitCode,
            };

            return viewModel;
        }
      
       
        public static IEnumerable<APCodeViewModel> ToViewModels(this IEnumerable<MPS_M_APCode> models)
        {
            return models != null ? models.Select(ToViewModel) : Enumerable.Empty<APCodeViewModel>();
        }
        public static IEnumerable<MPS_M_APCode> ToModel(this IEnumerable<APCodeViewModel> models)
        {
            return models != null ? models.Select(ToModel) : Enumerable.Empty<MPS_M_APCode>();
        }
        public static IEnumerable<MPS_M_APCode> ToModels(IEnumerable<APCodeViewModel> viewModels, IEnumerable<MPS_M_APCode> lines)
        {
            return viewModels != null ? viewModels.Select(ToModel) : Enumerable.Empty<MPS_M_APCode>();
        }

        public static IEnumerable<APCodeByProposalLineViewModel> ToProposalViewModel(this IEnumerable<ProposalLineViewModel> model)
        {
            return model != null ? model.Select(ToProposalViewModel) : Enumerable.Empty<APCodeByProposalLineViewModel>();
        }

    }
}