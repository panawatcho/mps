﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TheMall.MPS.API.Mappers.Abstracts;
using TheMall.MPS.API.Services;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Mappers.Abstracts;
using TheMall.MPS.Models.Enums;
using TheMall.MPS.Models.Proposal;
using TheMall.MPS.ViewModels.Proposal;

namespace TheMall.MPS.API.Mappers
{
    public interface IProposalIncomeOtherMapper : ILineMapper
    {
        MPS_IncomeOther ToModel(IncomeOtherViewModel viewModel, IEnumerable<MPS_IncomeOther> lines);
        IncomeOtherViewModel ToViewModel(MPS_IncomeOther model);
        IEnumerable<IncomeOtherViewModel> ToViewModels(IEnumerable<MPS_IncomeOther> models);
        IEnumerable<MPS_IncomeOther> ToModels(IEnumerable<IncomeOtherViewModel> viewModels, IEnumerable<MPS_IncomeOther> lines);
        MPS_AccountTrans ToAccountTransModel(AccountTransViewModel viewModel);
        AccountTransViewModel ToAccountTransViewModel(MPS_AccountTrans model);
        IEnumerable<AccountTransViewModel> ToAccountTransViewModels(IEnumerable<MPS_AccountTrans> models);
        IEnumerable<MPS_AccountTrans> ToAccountTransModels(IEnumerable<AccountTransViewModel> viewModels);

    }

    public class ProposalIncomeOtherMapper : BaseLineMapper, IProposalIncomeOtherMapper
    {
        private readonly IAccountTransService _accountTransService;
        private readonly IProposalLineService _lineService;
        public ProposalIncomeOtherMapper(IAccountTransService accountTransService, IProposalLineService lineService)
        {
            _accountTransService = accountTransService;
            _lineService = lineService;
        }

        public MPS_IncomeOther ToModel(IncomeOtherViewModel viewModel, IEnumerable<MPS_IncomeOther> lines)
        {
            if (viewModel == null)
            {
                return null;
            }
            var model = InitLineTable(viewModel, lines);
            if (model == null)
            {
                return null;
            }
            model.Budget = viewModel.Budget;
            model.ContactName = viewModel.ContactName;
            model.ContactDepartment = viewModel.ContactDepartment;
            model.ContactEmail = viewModel.ContactEmail;
            model.ContactTel = viewModel.ContactTel;
            model.Description = viewModel.Description;
            model.DueDate = viewModel.DueDate.ToLocalTime().Date;
            model.Remark = viewModel.Remark;
            model.SponcorName = viewModel.SponcorName;
            //model.Actual = viewModel.Actual; //viewModel.AccountTrans.Sum(m=>m.Actual);
            if (viewModel.AccountTrans != null)
            {
                model.Actual = viewModel.AccountTrans.Where(m => !m.Deleted).Sum(m => m.Actual);
            }
            //model.Actual = viewModel.AccountTrans.Where(m => !m.Deleted).Sum(m => m.Actual);
            model.PostActual = viewModel.PostActual;
            if (viewModel.SponcorName == Budget.APCode.DO3)
            {
                model.ProposalLineId = (viewModel.ProposalLineId);
            }
            
          //  model.AccountTrans = ToAccountTransModels(viewModel.AccountTrans).ToList();

            return model;
        }
        public IncomeOtherViewModel ToViewModel(MPS_IncomeOther model)
        {
            if (model == null)
            {
                return null;
            }
            var viewModel = this.InitLineViewModel<IncomeOtherViewModel>(model);

            viewModel.Budget = model.Budget;
            viewModel.ContactName = model.ContactName;
            viewModel.ContactDepartment = model.ContactDepartment;
            viewModel.ContactEmail = model.ContactEmail;
            viewModel.ContactTel = model.ContactTel;
            viewModel.Description = model.Description;
            viewModel.DueDate = model.DueDate.ToUniversalTime();
            viewModel.Remark = model.Remark;
            viewModel.SponcorName = model.SponcorName;
            viewModel.Actual = model.Actual;
            viewModel.PostActual = model.PostActual;
            if (model.SponcorName == Budget.APCode.DO3)
            {
                viewModel.RefLineNo = _lineService.LastOrDefault(m => m.Id == model.ProposalLineId).LineNo;
                viewModel.ProposalLineId = model.ProposalLineId;
            }
           
            viewModel.AccountTrans = ToAccountTransViewModels(model.AccountTrans).ToList();
            

            return viewModel;
        }
        public IEnumerable<IncomeOtherViewModel> ToViewModels(IEnumerable<MPS_IncomeOther> models)
        {
            return models != null ? models.Select(ToViewModel) : Enumerable.Empty<IncomeOtherViewModel>();
        }
        public IEnumerable<MPS_IncomeOther> ToModels(IEnumerable<IncomeOtherViewModel> viewModels, IEnumerable<MPS_IncomeOther> lines)
        {
            return viewModels != null ? viewModels.Select(m => ToModel(m, lines)).Where(m => m != null) : Enumerable.Empty<MPS_IncomeOther>();
        }

        public MPS_AccountTrans ToAccountTransModel(AccountTransViewModel viewModel)
        {
            if (viewModel == null)
            {
                return null;
            }
            var model = _accountTransService.Find(m => m.Id == viewModel.Id);
            if (model != null)
            {
                model.Actual = viewModel.Actual;
                if (viewModel.ActualDate != null) model.ActualDate = viewModel.ActualDate.Value.ToLocalTime().Date;
                model.Remark = viewModel.Remark;
                model.IncomeOtherId = viewModel.IncomeOtherId;
                model.Source = viewModel.Source;
                model.FlagMemoInvoice = viewModel.FlagMemoInvoice;
                return model;
            }
            else
            {    var newmodel =  new MPS_AccountTrans();
                newmodel.Actual = viewModel.Actual;
                if (viewModel.ActualDate != null) newmodel.ActualDate = viewModel.ActualDate.Value.ToUniversalTime();
                newmodel.Remark = viewModel.Remark;
                newmodel.IncomeOtherId = viewModel.IncomeOtherId;
                newmodel.Source = viewModel.Source;
                newmodel.Id = viewModel.Id;
                newmodel.FlagMemoInvoice = viewModel.FlagMemoInvoice;
                return newmodel;
            }
           
         
           
        }
        public AccountTransViewModel ToAccountTransViewModel(MPS_AccountTrans model)
        {
            if (model == null)
            {
                return null;
            }
            var viewModel = new AccountTransViewModel();
            viewModel.Actual = model.Actual;
            if (model.ActualDate != null) viewModel.ActualDate = model.ActualDate.Value.ToUniversalTime();
            viewModel.Remark = model.Remark;
            viewModel.IncomeOtherId = model.IncomeOtherId;
            viewModel.Id = model.Id;
            viewModel.Source = model.Source;
            viewModel.LineNo = model.Id;
            viewModel.FlagMemoInvoice = model.FlagMemoInvoice;
            return viewModel;
        }
        public IEnumerable<AccountTransViewModel> ToAccountTransViewModels(IEnumerable<MPS_AccountTrans> models)
        {
            return models != null ? models.Select(ToAccountTransViewModel) : Enumerable.Empty<AccountTransViewModel>();
        }
        public IEnumerable<MPS_AccountTrans> ToAccountTransModels(IEnumerable<AccountTransViewModel> viewModels)
        {
            return viewModels != null ? viewModels.Select(ToAccountTransModel).Where(m => m != null) : Enumerable.Empty<MPS_AccountTrans>();
        }
    }
}