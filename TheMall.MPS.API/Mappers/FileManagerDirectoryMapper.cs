﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TheMall.MPS.API.Mappers
{
    public static class FileManagerDirectoryMapper
    {
        public static ViewModels.FileManager.DirectoryViewModel ToViewModel(this Models.FileManager.FileManagerDirectory model)
        {
            if (model == null)
            {
                return null;
            }

            var viewModel = new ViewModels.FileManager.DirectoryViewModel();

            viewModel.Id = model.Id;
            viewModel.Name = model.Name;
            viewModel.Subfolders = model.Subfolders.ToViewModel();

            return viewModel;
        }

        public static IEnumerable<ViewModels.FileManager.DirectoryViewModel> ToViewModel(
            this IEnumerable<Models.FileManager.FileManagerDirectory> model)
        {
            if (model == null)
            {
                return Enumerable.Empty<ViewModels.FileManager.DirectoryViewModel>();
            }

            return model.Select(ToViewModel).ToListSafe();
        }
    }
}