﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TheMall.MPS.Infrastructure.Identity;
using TheMall.MPS.ViewModels.AspNet;

namespace TheMall.MPS.API.Mappers
{
    public class AspNetRolesMapperMapperProfile : AutoMapper.Profile
    {
        public AspNetRolesMapperMapperProfile()
        {
            CreateMap<ApplicationRole, AspNetRolesViewModel>()
                .ReverseMap();
        }
    }
    public static class AspNetRolesMapper
    {
        public static ApplicationRole ToModel(this AspNetRolesViewModel viewModel)
        {
            if (viewModel == null)
                return null;
            var model = new ApplicationRole();
            model.Inactive = viewModel.Inactive;
            model.Name = viewModel.Name;
            model.Id = viewModel.Id;
            return model;

        }

        public static AspNetRolesViewModel ToViewModel(this ApplicationRole model)
        {
            if (model == null)
                return null;
            var viewModel = new AspNetRolesViewModel()
            {
                Inactive = model.Inactive,
                Name = model.Name,
                Id = model.Id,
            };
            return viewModel;
        }
        public static IEnumerable<AspNetRolesViewModel> ToViewModels(this IEnumerable<ApplicationRole> models)
        {
            return models != null ? models.Select(ToViewModel) : Enumerable.Empty<AspNetRolesViewModel>();
        }
        public static IEnumerable<ApplicationRole> ToModels(IEnumerable<AspNetRolesViewModel> viewModels, IEnumerable<ApplicationRole> lines)
        {
            return viewModels != null ? viewModels.Select(ToModel) : Enumerable.Empty<ApplicationRole>();
        }

    }
}