﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using TheMall.MPS.Models.Master;
using TheMall.MPS.ViewModels.Master;

namespace TheMall.MPS.API.Mappers
{
    public class UnitCodeMapperProfile : AutoMapper.Profile
    {
        public UnitCodeMapperProfile()
        {
            CreateMap<MPS_M_UnitCode, UnitCodeViewModel>()
                .ReverseMap();
        }
    }
    public static class UnitCodeMapper
    {
        public static MPS_M_UnitCode ToModel(this UnitCodeViewModel viewModel)
        {
            if (viewModel == null)
                return null;
            var model = new MPS_M_UnitCode()
            {
                UnitCode = viewModel.UnitCode,
                UnitName = viewModel.UnitName,
                Type = viewModel.Type,
                Order = viewModel.Order,
                InActive = viewModel.InActive
            };
            return model;
            return Mapper.Map<MPS_M_UnitCode>(viewModel);
        }

        public static UnitCodeViewModel ToViewModel(this MPS_M_UnitCode model)
        {
            if (model == null)
                return null;
            var viewModel = new UnitCodeViewModel()
            {
                UnitCode = model.UnitCode,
                UnitName = model.UnitName,
                Order = model.Order,
                Type = model.Type,
                InActive = model.InActive
            };
            return viewModel;
            return Mapper.Map<UnitCodeViewModel>(model);
        }
        public static IEnumerable<UnitCodeViewModel> ToViewModels(this IEnumerable<MPS_M_UnitCode> models)
        {
            return models != null ? models.Select(ToViewModel) : Enumerable.Empty<UnitCodeViewModel>();
        }
        public static IEnumerable<MPS_M_UnitCode> ToModels(IEnumerable<UnitCodeViewModel> viewModels, IEnumerable<MPS_M_UnitCode> lines)
        {
            return viewModels != null ? viewModels.Select(ToModel) : Enumerable.Empty<MPS_M_UnitCode>();
        }


        public static UnitCodeViewModel ToViewModelsFromEmployee(this UnitCodeForEmployeeViewModel viewModelsFromEmployee)
        {
            if (viewModelsFromEmployee == null)
                return null;
         
            var viewModel = new UnitCodeViewModel
            {            
                UnitCode = viewModelsFromEmployee.UnitCode.UnitCode,
                UnitName = viewModelsFromEmployee.UnitCode.UnitName
            };
            return viewModel;
        }

        public static IEnumerable<UnitCodeViewModel> ToViewModelsFromEmployee(this IEnumerable<UnitCodeForEmployeeViewModel> viewModels)
        {
            return viewModels != null ? viewModels.Select(ToViewModelsFromEmployee) : Enumerable.Empty<UnitCodeViewModel>();
        }

    }
}