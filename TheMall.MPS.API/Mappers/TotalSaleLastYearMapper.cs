﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TheMall.MPS.API.Services;
using TheMall.MPS.Models;
using TheMall.MPS.ViewModels.Master;

namespace TheMall.MPS.API.Mappers
{
    public class TotalSaleLastYearMapperProfile : AutoMapper.Profile
    {
        public TotalSaleLastYearMapperProfile()
        {
            CreateMap<MPS_M_TotalSaleLastYear, TotalSaleLastYearViewModel>()
                .ReverseMap();
        }
    }
    public static class TotalSaleLastYearMapper
    {
        public static MPS_M_TotalSaleLastYear ToModel(this TotalSaleLastYearViewModel viewModel)
        {
            var model = new MPS_M_TotalSaleLastYear();
            if (viewModel.SharedBranch != null)
            {
                model.BranchCode = viewModel.SharedBranch.BranchCode;
            }
            model.AreaRE = viewModel.AreaRE;
            model.AreaTR = viewModel.AreaTR;
            model.PercentRE = viewModel.PercentRE;
            model.PercentTR = viewModel.PercentTR;
            model.Year = viewModel.YearTime.ToString("yyyy");
            model.TotalSale = viewModel.TotalSale;
            return model;
        }

        public static TotalSaleLastYearViewModel ToViewModel(this MPS_M_TotalSaleLastYear model)
        {
            var viewModel = new TotalSaleLastYearViewModel();
            if (model.SharedBranch != null)
            {
                viewModel.SharedBranch = model.SharedBranch.ToViewModel();
            }
            viewModel.AreaRE = model.AreaRE;
            viewModel.AreaTR = model.AreaTR;
            viewModel.PercentRE = model.PercentRE;
            viewModel.PercentTR = model.PercentTR;
            viewModel.Year = model.Year;
            viewModel.TotalSale = model.TotalSale;
            viewModel.BranchCode = model.BranchCode;
            return viewModel;
        }

        public static IEnumerable<MPS_M_TotalSaleLastYear> ToModels(IEnumerable<TotalSaleLastYearViewModel> viewModels, IEnumerable<MPS_M_TotalSaleLastYear> lines)
        {
            return viewModels != null ? viewModels.Select(ToModel) : Enumerable.Empty<MPS_M_TotalSaleLastYear>();
        }

        public static IEnumerable<TotalSaleLastYearViewModel> ToViewModels(this IEnumerable<MPS_M_TotalSaleLastYear> models)
        {
            return models != null ? models.Select(ToViewModel) : Enumerable.Empty<TotalSaleLastYearViewModel>();
        }
    }
}