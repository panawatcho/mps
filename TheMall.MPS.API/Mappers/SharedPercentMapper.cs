﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using TheMall.MPS.Models.Master;
using TheMall.MPS.ViewModels.Master;

namespace TheMall.MPS.API.Mappers
{
    public class SharedPercentMapperProfile : AutoMapper.Profile
    {
        public SharedPercentMapperProfile()
        {
            CreateMap<MPS_M_SharedPercent, SharedPercentViewModel>()
              .ReverseMap();
        }
    }
    public static class SharedPercentMapper
    {
        public static MPS_M_SharedPercent ToModel(this SharedPercentViewModel viewModel)
        {
            if (viewModel == null)
                return null;
            var model = new MPS_M_SharedPercent()
            {
                Id = viewModel.Id,
                PlaceCode = viewModel.Place.PlaceCode,
                PlaceName = viewModel.Place.PlaceName,
                Percent = viewModel.Percent,
                TR = viewModel.TR,
                RE = viewModel.RE,
                AllocateType = viewModel.AllocateType
            };
            return model;

            
        }

        public static SharedPercentViewModel ToViewModel(this MPS_M_SharedPercent model)
        {
            if (model == null)
                return null;
            var viewModel = new SharedPercentViewModel()
            {
                Id = model.Id,
                Place = new PlaceViewModel() { PlaceCode = model.PlaceCode, PlaceName = model.PlaceName },
                Percent = model.Percent,
                TR = model.TR,
                RE = model.RE,
                AllocateType = model.AllocateType
            };
            return viewModel;

           
        }
        public static IEnumerable<SharedPercentViewModel> ToViewModels(this IEnumerable<MPS_M_SharedPercent> models)
        {
            return models != null ? models.Select(ToViewModel) : Enumerable.Empty<SharedPercentViewModel>();
        }
        public static IEnumerable<MPS_M_SharedPercent> ToModels(this IEnumerable<SharedPercentViewModel> viewModels)
        {
            return viewModels != null ? viewModels.Select(ToModel) : Enumerable.Empty<MPS_M_SharedPercent>();
        }

    }
}