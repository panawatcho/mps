﻿using System;
using System.Linq;
using TheMall.MPS.API.Mappers.Abstracts;
using TheMall.MPS.API.Services;
using TheMall.MPS.Models.Enums;
using TheMall.MPS.Models.PettyCash;
using TheMall.MPS.ViewModels;
using TheMall.MPS.ViewModels.PettyCash;

namespace TheMall.MPS.API.Mappers
{
    public interface IPettyCashMapper : IWorkflowTableMapper<MPS_PettyCashTable>
    {
        MPS_PettyCashTable ToModel(PettyCashViewModel viewModel);
        PettyCashViewModel ToViewModel(MPS_PettyCashTable model);
        MPS_PettyCashTable DeleteBy(MPS_PettyCashTable model, EmployeeViewModel employeeViewModel);
    }

    public class PettyCashMapper : BaseWorkflowTableMapper<MPS_PettyCashTable>, IPettyCashMapper
    {
        private readonly IPettyCashService _mpsWorkflowTableService;
        private readonly IPettyCashLineMapper _lineMapper;
        private readonly IExpenseTopicService _expenseTopicService;
        private readonly IAPcodeService _apCodeService;
        private readonly IProposalMapper _proposalMapper;
        private readonly IPettyCashApprovalMapper _approvalMapper;
        private readonly IPlaceService _placeService;
        private readonly IProposalLineService _proposalLineService;
        private readonly IEmployeeTableService _employeeTableService;
        private readonly IBudgetProposalTransService _budgetProposalTransService;
        private readonly ICompanyService _companyService;

        public PettyCashMapper(IPettyCashService mpsWorkflowTableService, 
            IPettyCashLineMapper lineMapper,
            IExpenseTopicService expenseTopicService,
            IAPcodeService apCodeService,
            IProposalLineMapper proposalLineMapper,
            IProposalLineService proposalLineService,
            IProposalMapper proposalMapper, 
            IPlaceService placeService,
            IPettyCashApprovalMapper approvalMapper,
            IProposalLineService proposalLineService1,
            IEmployeeTableService employeeTableService,
            IAccountingService accountingService, 
            IBudgetProposalTransService budgetProposalTransService,
            ICompanyService companyService)
            : base(mpsWorkflowTableService, employeeTableService, placeService, accountingService)
        {
            _mpsWorkflowTableService = mpsWorkflowTableService;
            _lineMapper = lineMapper;
            _expenseTopicService = expenseTopicService;
            _apCodeService = apCodeService;
            //proposalLineService = proposalLineService;
            _proposalMapper = proposalMapper;
            _placeService = placeService;
            _approvalMapper = approvalMapper;
            _proposalLineService = proposalLineService1;
            _employeeTableService = employeeTableService;
            _budgetProposalTransService = budgetProposalTransService;
            _companyService = companyService;
        }

        public MPS_PettyCashTable ToModel(PettyCashViewModel viewModel)
        {
            if (viewModel == null)
            {
                return null;
            }

            var model = InitWorkflowTable(viewModel);
            if (viewModel.ExpenseTopic != null)
            {
                model.ExpenseTopicCode = viewModel.ExpenseTopic.ExpenseTopicCode;
            }

            if (viewModel.APCode != null)
            {
                model.APCode = viewModel.APCode.APCode;
            }

            if (viewModel.ProposalRef != null)
            {
                var proposalLine = _proposalLineService.Find(m => m.Id == viewModel.ProposalLineId);
                model.ProposalLine = proposalLine;
                model.ProposalLineID = viewModel.ProposalLineId;
                model.ProposalRefID = viewModel.ProposalRef.Id;
                model.ProposalRefDocumentNumber = viewModel.ProposalRef.DocumentNumber;
            }

            model.Title = viewModel.GoodsIssue ? "Goods Issue" : "Petty Cash";
            model.PettyCashDate = DateTime.Now;
            model.Description = viewModel.Description;
            model.BudgetDetail = viewModel.BudgetDetail;
            model.BudgetAPCode = viewModel.BudgetAPCode;
            model.GoodsIssue = viewModel.GoodsIssue;

            if (viewModel.Company != null)
            {
                model.VendorCode = viewModel.Company.VendorCode;
            }
            
            if (viewModel.Branch != null)
            {
                model.Branch = viewModel.Branch.PlaceCode;
            }

            return model;
        }

        public PettyCashViewModel ToViewModel(MPS_PettyCashTable model)
        {
            if (model == null)
            {
                return null;
            }

            var viewModel = InitViewModel<PettyCashViewModel>(model);
            //  viewModel.Requester = _employeeTableService.FindByUsername(model.RequesterUserName).ToViewModel();
            if (model.ProposalLine != null)
            {
                viewModel.ProposalRef = _proposalMapper.ToProposalInfoViewModel(model.ProposalLine.ProposalTable);
            }

            if (model.ExpenseTopicCode != null)
            {
                viewModel.ExpenseTopic = _expenseTopicService.Find(m => m.ExpenseTopicCode == model.ExpenseTopicCode).ToViewModel();
            }

            if (model.APCode != null && viewModel.ProposalRef != null)
            {
                viewModel.APCode =
                    viewModel.ProposalRef.APCode.Find(
                        m => m.APCode == model.APCode && m.ProposalLineId == model.ProposalLineID);
            }

            if (model.PettyCashLine != null)
            {
                viewModel.PettyCashLines = _lineMapper.ToViewModels(model.PettyCashLine);
            }

            viewModel.PettyCashDate = DateTime.Now;
            viewModel.Description = model.Description ?? "";
            viewModel.BudgetDetail = model.BudgetDetail;
            viewModel.BudgetAPCode = model.BudgetAPCode;

            if (model.Branch != null)
            {
                var palce = _placeService.Find(m => m.PlaceCode.Equals(model.Branch));
                viewModel.Branch = palce.ToViewModel();
            }

            if (model.VendorCode != null)
            {
                var companyData = _companyService.Find(m => m.VendorCode.Equals(model.VendorCode));
                viewModel.Company = companyData.ToViewModel();
            }
            else
            {
                var vendorCode = _companyService.Find(m => m.VendorCode.Equals(viewModel.Branch.PlaceCode));
                if (vendorCode != null)
                {
                    viewModel.Company = vendorCode.ToViewModel();
                }
                else
                {
                    viewModel.Company = null;
                }
            }

            viewModel.Comments = model.Comments.ToViewModel();
            viewModel.PettyCashLines = _lineMapper.ToViewModels(model.PettyCashLine);
            viewModel.PettyCashApproval = _approvalMapper.ToViewModels(model.PettyCashApproval);
            viewModel.ProposalLineId = model.ProposalLineID;
            var budgetTrans = _budgetProposalTransService.GetBudgetReserve(model.ProposalRefID, model.ProposalLineID,
                model.APCode, model.UnitCode, Budget.DocumentType.PettyCash);
            viewModel.BudgetTrans = budgetTrans;
            viewModel.GoodsIssue = model.GoodsIssue;

            return viewModel;
        }

        public MPS_PettyCashTable DeleteBy(MPS_PettyCashTable model, EmployeeViewModel employeeViewModel)
        {
            if (model != null)
            {
                if (model.Id > 0)
                {
                    model.Deleted = true;
                    model.DeletedDate = DateTime.Now;
                    model.DeletedBy = employeeViewModel.Username;
                    model.DeletedByName = employeeViewModel.FullName;
                    return model;
                }

                return null;
            }

            return null;
        }
    }
}