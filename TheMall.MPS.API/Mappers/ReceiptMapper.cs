﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper.Execution;
using TheMall.MPS.API.Services;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Models;
using TheMall.MPS.Models.Master;
using TheMall.MPS.Models.Receipt;
using TheMall.MPS.ViewModels;
using TheMall.MPS.ViewModels.Receipt;
using TheMall.MPS.ViewModels.Master;
using TheMall.MPS.ViewModels.Interfaces;

namespace TheMall.MPS.API.Mappers
{
    // call
    public interface IReceiptMapper
    {
        MPS_ReceiptTable ToModel(ReceiptViewModel viewModel);
        ReceiptViewModel ToViewModel( MPS_ReceiptTable model); IEnumerable<ReceiptViewModel> ToViewModels( IEnumerable<MPS_ReceiptTable> models);
        IEnumerable<MPS_ReceiptTable> ToModels(IEnumerable<ReceiptViewModel> viewModels, IEnumerable<MPS_ReceiptTable> lines);
    }

    public class ReceiptMapper : IReceiptMapper
    {
        // import
        private readonly IReceiptLineMapper _receiptlinemapper;
        private readonly IEmployeeTableService _employeeTableService;
        private readonly IUnitCodeForEmployeeService _unitCodeForEmployee;
        private readonly IReceiptService _receiptService;
      
        public ReceiptMapper(
            IReceiptLineMapper receiptlinemapper, 
            IEmployeeTableService employeeTableService, IUnitCodeForEmployeeService unitCodeForEmployee, IReceiptService receiptService)
        {
            _receiptlinemapper = receiptlinemapper;
            _employeeTableService = employeeTableService;
            _unitCodeForEmployee = unitCodeForEmployee;
            _receiptService = receiptService;
          
        }

        // model to view model
        // display
        public ReceiptViewModel ToViewModel(MPS_ReceiptTable model)
        {
            if (model == null)
            {
                return null;
            }
               
            var viewModel = new ReceiptViewModel();
           
            if (!string.IsNullOrEmpty(model.RequesterUserName))
            {
                // emp(model)
                var emp = _employeeTableService.FindByUsername(model.RequesterUserName);
                // return emp(model) to view model
                viewModel.Requester = emp.ToViewModel();
            }

            if (!string.IsNullOrEmpty(model.RequestForUserName))
            {
                var emp = _employeeTableService.FindByUsername(model.RequestForUserName).ToViewModel();
                emp.UnitCode = _unitCodeForEmployee.GetUnitCodeForEmployees(model.RequestForUserName).ToViewModels().ToViewModelsFromEmployee();
                viewModel.Requesterfor = emp;
                
            }

            if (model.ReceiptLine != null)
            {
                viewModel.ReceiptLines = _receiptlinemapper.ToViewModels(model.ReceiptLine);
            } else {
                viewModel.ReceiptLines = new List<ReceiptLineViewModel>();
            }

            viewModel.Id = model.Id;
            viewModel.UnitCode = (model.Unitcode!=null && model.Unitcode.UnitCode!="") ?model.Unitcode.ToViewModel():new UnitCodeViewModel();
            //// type 2
            //viewModel.UnitCodes = model.Unitcode.ToViewModel().UnitCode;
            if (!string.IsNullOrEmpty(model.UnitCodeFor))
            {
                viewModel.UnitCodeFor = new MPS_M_UnitCode()
                {
                    UnitCode = model.UnitCodeFor,
                    UnitName = model.UnitNameFor

                }.ToViewModel();

            } else {
                viewModel.UnitCodeFor = new UnitCodeViewModel();
            }

            viewModel.BudgetNoVatTax = model.BudgetNoVatTax;
            viewModel.BudgetDetail = model.BudgetDetail;
            viewModel.DocumentNumber = model.DocumentNumber;
            viewModel.Status = model.Status;
            viewModel.StatusFlag = model.StatusFlag;
            viewModel.CreatedDate = model.CreatedDate;

            return viewModel;
        }

        public IEnumerable<ReceiptViewModel> ToViewModels( IEnumerable<MPS_ReceiptTable> models)
        {
            return models != null ? models.Select(ToViewModel) : Enumerable.Empty<ReceiptViewModel>();
        }

        // view model to model
        // insert
        public MPS_ReceiptTable ToModel(ReceiptViewModel viewModel)
        {
            if (viewModel == null)
            {
                return null;
            }

            var model = new MPS_ReceiptTable();

            // get
            if (viewModel.Id > 0)
            {
                model = _receiptService.Find(m => m.Id == viewModel.Id);
            }
            //
            if (viewModel.Requester != null)
            {
                model.RequesterUserName = viewModel.Requester.Username;
                model.RequesterName = viewModel.Requester.FullName;
            }
            //
            if (viewModel.UnitCode != null && viewModel.UnitCode.UnitCode!="")
            {
                model.UnitCode = viewModel.UnitCode.UnitCode;
                model.UnitName = viewModel.UnitCode.UnitName;
            } else {
                model.UnitCode = null;
                model.UnitName = null;
            }
            //
            if (viewModel.Requesterfor != null)
            {
                model.RequestForUserName = viewModel.Requesterfor.Username;
                model.RequesterForName = viewModel.Requesterfor.FullName;
            } else {
                model.RequestForUserName = null;
                model.RequesterForName = null;
            }
            //
            if (viewModel.UnitCodeFor != null)
            {
                model.UnitCodeFor = viewModel.UnitCodeFor.UnitCode;
                model.UnitNameFor = viewModel.UnitCodeFor.UnitName;
            } else {
                model.UnitCodeFor = null;
                model.UnitNameFor = null;
            }
            //
            if (viewModel.ReceiptLines != null)
            {
                model.BudgetDetail = (decimal) viewModel.ReceiptLines.Where(m => !m.Deleted).Sum(m => m.NetAmount);
            }
            //
            if (viewModel.StatusFlag == 9 && viewModel.Requester!=null)
            {
                model.CompletedDate = viewModel.CompletedDate;
                model.CompletedByUserName = viewModel.Requester.Username;
                model.CompletedByName = viewModel.Requester.FullName;
            }

            model.Id = viewModel.Id;
            model.BudgetNoVatTax = viewModel.BudgetNoVatTax;
            model.DocumentNumber = viewModel.DocumentNumber;
            model.Status = viewModel.Status;
            model.StatusFlag = viewModel.StatusFlag;

            return model;
        }

        public IEnumerable<MPS_ReceiptTable> ToModels(IEnumerable<ReceiptViewModel> viewModels, IEnumerable<MPS_ReceiptTable> lines)
        {
            return viewModels != null ? viewModels.Select(ToModel) : Enumerable.Empty<MPS_ReceiptTable>();
        }

    }
}