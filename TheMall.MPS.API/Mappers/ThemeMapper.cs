﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using TheMall.MPS.Models.Master;
using TheMall.MPS.ViewModels.Master;

namespace TheMall.MPS.API.Mappers
{
    public class ThemeMapperProfile : AutoMapper.Profile
    {
        public ThemeMapperProfile()
        {
            CreateMap<MPS_M_Themes, ThemeViewModel>().ReverseMap();
        }
    }
    public static class ThemeMapper
    {
        public static MPS_M_Themes ToModel(this ThemeViewModel viewModel)
        {
            if (viewModel == null)
                return null;
            var model = new MPS_M_Themes();

            model.ThemesCode = viewModel.ThemesCode;
            model.ThemesName = viewModel.ThemesName;
            model.InActive = viewModel.InActive;
            model.Order = viewModel.Order;

            return model;
            return Mapper.Map<MPS_M_Themes>(viewModel);
        }

        public static ThemeViewModel ToViewModel(this MPS_M_Themes model)
        {
            if (model == null)
                return null;
            var viewModel = new ThemeViewModel()
            {
                ThemesCode = model.ThemesCode,
                ThemesName = model.ThemesName,
                InActive = model.InActive,
                Order = model.Order
            };

            return viewModel;
            return Mapper.Map<ThemeViewModel>(model);
        }
        public static IEnumerable<ThemeViewModel> ToViewModels(this IEnumerable<MPS_M_Themes> models)
        {
            return models != null ? models.Select(ToViewModel) : Enumerable.Empty<ThemeViewModel>();
        }
        public static IEnumerable<MPS_M_Themes> ToModels(IEnumerable<ThemeViewModel> viewModels, IEnumerable<MPS_M_Themes> lines)
        {
            return viewModels != null ? viewModels.Select(ToModel) : Enumerable.Empty<MPS_M_Themes>();
        }
    }
}