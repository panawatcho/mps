﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using AutoMapper;
using TheMall.MPS.Models.Master;
using TheMall.MPS.ViewModels.Master;

namespace TheMall.MPS.API.Mappers
{
    public class BudgetYearPlanMapperProfile : AutoMapper.Profile
    {
        public BudgetYearPlanMapperProfile()
        {
            CreateMap<MPS_M_BudgetYearPlan, BudgetYearPlanViewModel>()
                .ReverseMap();
        }
    }
    public static class BudgetYearPlanMapper
    {
        public static MPS_M_BudgetYearPlan ToModel(this BudgetYearPlanViewModel viewModel)
        {
            if (viewModel == null)
                return null;
            var model = new MPS_M_BudgetYearPlan();
            model.TeamName = viewModel.TeamName;
            model.Type = viewModel.Type.ToUpper();
            model.Budget = viewModel.Budget;
            if (viewModel.UnitCodeViewModel != null)
            {
                model.UnitCode = viewModel.UnitCodeViewModel.UnitCode;
                model.UnitName = viewModel.UnitCodeViewModel.UnitName;
            }
            model.TypeYearPlan = viewModel.TypeYearPlan;
            model.Year = viewModel.YearTime.ToString("yyyy");
            model.Order = viewModel.Order;

            //{       
            //    TeamName = viewModel.TeamName,
            //    Type = viewModel.Type,
            //    Budget = viewModel.Budget,
            //    Year = viewModel.Year,
            //    UnitCode = viewModel.UnitCode,
            //    UnitName = viewModel.UnitName,
            //    TypeYearPlan = viewModel.TypeYearPlan
            //};
            return model;

        }

        public static BudgetYearPlanViewModel ToViewModel(this MPS_M_BudgetYearPlan model)
        {
            if (model == null)
                return null;
            var viewModel = new BudgetYearPlanViewModel()
            {
                //BudgetYearID = model.BudgetYearID,
                TeamName = model.TeamName,
                Type = model.Type,
                Budget = model.Budget,
                Year = model.Year,
                UnitCode = model.UnitCode,
                UnitName = model.UnitName,
                TypeYearPlan = model.TypeYearPlan,
                Order = model.Order
            };
            return viewModel;

           
        }
        public static IEnumerable<BudgetYearPlanViewModel> ToViewModels(this IEnumerable<MPS_M_BudgetYearPlan> models)
        {
            return models != null ? models.Select(ToViewModel) : Enumerable.Empty<BudgetYearPlanViewModel>();
        }
        public static IEnumerable<MPS_M_BudgetYearPlan> ToModels(IEnumerable<BudgetYearPlanViewModel> viewModels, IEnumerable<MPS_M_BudgetYearPlan> lines)
        {
            return viewModels != null ? viewModels.Select(ToModel) : Enumerable.Empty<MPS_M_BudgetYearPlan>();
        }

    }
}