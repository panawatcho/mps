﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Web;
using System.Linq;

namespace TheMall.MPS.API
{
    public sealed class AppSettings : CrossingSoft.Framework.Abstracts.BaseDynamicAppSettings
    {
        private static readonly Lazy<AppSettings> Lazy
                        = new Lazy<AppSettings>(() => new AppSettings());
        public static AppSettings Instance { get { return Lazy.Value; } }

        public static string CurrentEnvironment
        {
            get
            {
                var env = System.Configuration.ConfigurationManager.AppSettings["CurrentEnvironment"];
                if (!string.IsNullOrEmpty(env))
                {
                    return env;
                }
                else
                {
                    return "DEV";
                }
            }
        }

        public override string Environment
        {
            get { return AppSettings.CurrentEnvironment; }
        }

        public override dynamic Settings
        {
            get
            {
                if (_appSettings == null)
                {
                    _appSettings = new DynamicAppSettingsDictionary(() => InnerDictionary);
                }
                return _appSettings;
            }
        }

        protected override object GetValue(string key)
        {
            if (Settings == null)
            {
                throw new Exception("There was an error while application tried to bootstrap. Please contact system administrator.");
            }

            var binder = Microsoft.CSharp.RuntimeBinder.Binder.GetMember(Microsoft.CSharp.RuntimeBinder.CSharpBinderFlags.None, key, Settings.GetType(),
                new[] { Microsoft.CSharp.RuntimeBinder.CSharpArgumentInfo.Create(Microsoft.CSharp.RuntimeBinder.CSharpArgumentInfoFlags.None, null) });
            var callsite = System.Runtime.CompilerServices.CallSite<Func<System.Runtime.CompilerServices.CallSite, object, object>>.Create(binder);
            return callsite.Target(callsite, Settings);
        }

        protected override string InputKey
        {
            get { return "234BC2D7-2CCE-4E77-B293-41B11AF2AAB0"; }
        }

        protected override string CacheLocation
        {
            get
            {
                return HttpContext.Current.Server.MapPath("~") + "\\appSettingsCache.xml";
            }
        }

        public override void Refresh()
        {
            var appSettingService = Resolver.GetInstance<Services.IApplicationSettingService>();
            foreach (var row in appSettingService.Initialize(AppSettings.CurrentEnvironment))
            {
                InnerDictionary.Add(row.Key, row.Value);
            }

            RebuildCache();
        }

        public string AttachmentRootPath
        {
            get
            {
                return System.Configuration.ConfigurationManager.AppSettings["AttachmentRootPath"];
            }
        }

        public string AttachmentAllowFileTypes
        {
            get
            {
                return "^.*\\.(doc|DOC|docx|DOCX|pdf|PDF|gif|GIF|jpe?g|JPE?G|png|PNG|tiff?|TIFF?|zip|ZIP|rar|RAR|xlsx?|XLSX?|pptx?|PPTX?|txt|TXT|msg|MSG)$";
            }
        }

        public IEnumerable<string> CorsSites()
        {
            var corSites = System.Configuration.ConfigurationManager.AppSettings["CorsSites"];
            corSites = string.IsNullOrEmpty(corSites) ? "" : corSites;
            return corSites.Split(',').Where(m => !string.IsNullOrEmpty(m)).Select(m => m.Trim());


        }
        public Models.Enums.AttachmentMode AttachmentMode
        {
            get { return Models.Enums.AttachmentMode.FileSystem; }
        }
        public bool MailLogEnable()
        {
            return true;
        }

        public bool MailTraceEnable()
        {
            return false;
        }

        public string MailLogLocation()
        {
            return "MailLog\\Log";
        }

        public string MailTraceLocation()
        {
            return string.Empty;
        }
        public string SecurityLabelName()
        {
            return "K2";
        }
        public string BaseUrlViewFlow
        {
            get
            {
                return System.Configuration.ConfigurationManager.AppSettings["BaseUrlViewFlow"];
            }
        }
    }

    public class DynamicAppSettingsDictionary : CrossingSoft.Framework.Abstracts.BaseDynamicAppSettingsDictionary
    {
        public DynamicAppSettingsDictionary(Func<Dictionary<string, object>> trunk)
            : base(trunk)
        {
        }

        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            object value;
            InnerDictionary.TryGetValue(binder.Name, out value);

            if (value == null && !InnerDictionary.ContainsKey(binder.Name))
            {
                var appSettingService = Resolver.GetInstance<Services.IApplicationSettingService>();
                var setting = appSettingService.GetApplicationSetting(binder.Name, AppSettings.CurrentEnvironment);

                if (setting != null)
                {
                    value = setting.Value;
                }
                InnerDictionary.Add(binder.Name, value);
            }
            result = value;
            return true;
        }
    }
}