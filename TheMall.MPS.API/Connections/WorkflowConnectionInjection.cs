﻿using System;
using System.Configuration;
using System.Security.Claims;
using CrossingSoft.Framework.Infrastructure.K2;

namespace TheMall.MPS.API.Connections
{
    public class WorkflowConnectionInjection
    {
        public static IWorkflowConnection Inject()
        {
            var identity = System.Threading.Thread.CurrentPrincipal.Identity as ClaimsIdentity;
            if (identity != null && !string.IsNullOrEmpty(identity.Name))
            {
                return new WorkflowConnection(
                    ConfigurationManager.AppSettings["K2Server"],
                    Int32.Parse(ConfigurationManager.AppSettings["K2ServerPort"]),
                    ConfigurationManager.AppSettings["SecurityLabelName"],
                    ConfigurationManager.AppSettings["K2AdminDomain"],
                    ConfigurationManager.AppSettings["K2AdminUserName"],
                    ConfigurationManager.AppSettings["K2AdminPassword"],
                    true,
                    identity.Name);
            }
            else
            {
                return new WorkflowConnection(
                    ConfigurationManager.AppSettings["K2Server"],
                    Int32.Parse(ConfigurationManager.AppSettings["K2ServerPort"]),
                    ConfigurationManager.AppSettings["SecurityLabelName"],
                    ConfigurationManager.AppSettings["K2AdminDomain"],
                    ConfigurationManager.AppSettings["K2AdminUserName"],
                    ConfigurationManager.AppSettings["K2AdminPassword"],
                    true);
                    //,"k2.tester01");
            }
        }
    }
}