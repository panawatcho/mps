﻿using System;
using System.Configuration;
using CrossingSoft.Framework.Infrastructure.K2;

namespace TheMall.MPS.API.Connections
{
    public class HostConnectionInjection
    {
        public static IHostConnection Inject()
        {
            return new HostConnection(
                    ConfigurationManager.AppSettings["K2Server"],
                    Int32.Parse(ConfigurationManager.AppSettings["K2HostServerPort"]),
                    ConfigurationManager.AppSettings["SecurityLabelName"],
                    ConfigurationManager.AppSettings["K2AdminDomain"],
                    ConfigurationManager.AppSettings["K2AdminUserName"],
                    ConfigurationManager.AppSettings["K2AdminPassword"]);
        }
    }
}