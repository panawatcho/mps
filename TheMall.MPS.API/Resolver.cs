﻿using System;
using System.Web.Http;
using System.Web.Http.Dependencies;

namespace TheMall.MPS.API
{
    public static class Resolver
    {
        public static IDependencyScope BeginScope()
        {
            return GlobalConfiguration.Configuration.DependencyResolver.BeginScope();
        }

        public static TService GetInstance<TService>()
        {
            return (TService) GlobalConfiguration.Configuration.DependencyResolver.GetService(typeof (TService));
        }

        public static object GetInstance(Type serviceType)
        {
            return GlobalConfiguration.Configuration.DependencyResolver.GetService(serviceType);
            //return Microsoft.Practices.ServiceLocation.ServiceLocator.Current.GetInstance(serviceType);
        }
    }
}