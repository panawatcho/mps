﻿using System;
using System.Runtime.Serialization;

namespace TheMall.MPS.API.Exceptions
{
    [Serializable]
    public class BAPIExecutionException : Exception
    {
        //
        // For guidelines regarding the creation of new exception types, see
        //    http://msdn.microsoft.com/library/default.asp?url=/library/en-us/cpgenref/html/cpconerrorraisinghandlingguidelines.asp
        // and
        //    http://msdn.microsoft.com/library/default.asp?url=/library/en-us/dncscol/html/csharp07192001.asp
        //
        public string Level { get; private set; }

        public BAPIExecutionException()
        {
        }

        public BAPIExecutionException(string level, string message) : base(message)
        {
            Level = level;
        }

        public BAPIExecutionException(string level, string message, Exception inner)
            : base(message, inner)
        {
            Level = level;
        }

        protected BAPIExecutionException(
            SerializationInfo info,
            StreamingContext context) : base(info, context)
        {
        }
    }
}