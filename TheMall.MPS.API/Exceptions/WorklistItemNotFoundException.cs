﻿using System;
using System.Runtime.Serialization;

namespace TheMall.MPS.API.Exceptions
{
    [Serializable]
    public class WorklistItemNotFoundException : Exception
    {
        //
        // For guidelines regarding the creation of new exception types, see
        //    http://msdn.microsoft.com/library/default.asp?url=/library/en-us/cpgenref/html/cpconerrorraisinghandlingguidelines.asp
        // and
        //    http://msdn.microsoft.com/library/default.asp?url=/library/en-us/dncscol/html/csharp07192001.asp
        //
        public string SN { get; private set; }
        public int? WorkflowTableId { get; set; }

        public WorklistItemNotFoundException()
        {
        }

        public WorklistItemNotFoundException(string sn, string message) : base(message)
        {
            SN = sn;
        }

        public WorklistItemNotFoundException(string sn, string message, Exception inner)
            : base(message, inner)
        {
            SN = sn;
        }

        protected WorklistItemNotFoundException(
            SerializationInfo info,
            StreamingContext context) : base(info, context)
        {
        }
    }
}