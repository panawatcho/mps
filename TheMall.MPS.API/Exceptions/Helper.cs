﻿using System.Web;

namespace TheMall.MPS.API.Exceptions
{
    public static class Helper
    {
        public static HttpException BadRequest(string msg)
        {
            return new HttpException((int)System.Net.HttpStatusCode.BadRequest, msg);
        }

        public static HttpException DataWithIDNotExists(string id)
        {
            return new HttpException((int)System.Net.HttpStatusCode.BadRequest, string.Format("DataWithIDNotExists {0}", id));
            //return new HttpException((int)System.Net.HttpStatusCode.BadRequest, string.Format(Resources.Error.DataWithIDNotExists, id));
        }
        public static HttpException DataWithIDNotExists(int id)
        {
            return DataWithIDNotExists(id.ToString());
        }
    }
}