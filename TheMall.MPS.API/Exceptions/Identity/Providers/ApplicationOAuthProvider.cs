﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;
using CrossingSoft.Framework.Infrastructure;
using TheMall.MPS.Infrastructure;
using TheMall.MPS.Infrastructure.EntityFramework;
using TheMall.MPS.Infrastructure.Identity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using TheMall.MPS.API.Facades;
using TheMall.MPS.API.Services;
using TheMall.MPS.Models;

namespace TheMall.MPS.API.Identity.Providers
{
    public class ApplicationOAuthProvider : OAuthAuthorizationServerProvider
    {
        private readonly string _publicClientId;
        private readonly Func<UserManager<ApplicationUser>> _userManagerFactory;

        //public ApplicationOAuthProvider(string publicClientId, Func<UserManager<ApplicationUser>> userManagerFactory)
        public ApplicationOAuthProvider(string publicClientId)
        {
            if (publicClientId == null)
            {
                throw new ArgumentNullException("publicClientId");
            }

            //if (userManagerFactory == null)
            //{
            //    throw new ArgumentNullException("userManagerFactory");
            //}

            _publicClientId = publicClientId;
            //_userManagerFactory = userManagerFactory;
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            using (var userManager = context.OwinContext.GetUserManager<ApplicationUserManager>())
            {
                var passwordOk =
                    await
                        userManager.CheckPasswordAsync(new ApplicationUser()
                        {
                            UserName = context.UserName
                        },
                            context.Password);
                var employee = new MPS_M_EmployeeTable();
                using (Resolver.BeginScope())
                {
                    var employeeService = Resolver.GetInstance<IEmployeeTableService>();
                     employee = employeeService.FindByUsername(context.UserName);
                }
                if (!passwordOk || (passwordOk && employee==null))
                {
                    context.SetError("invalid_grant", "The user name or password is incorrect.");
                    return;
                }
               
               
                ApplicationUser user = await userManager.FindByNameAsync(context.UserName);
                if (user == null)
                {
                    ////TODO Get User's Information for create new user in ASPNetUsers table
                    //var employeeService =
                    //    (IEmployeeService)GlobalConfiguration.Configuration.DependencyResolver.GetService(typeof(IEmployeeService));
                    //var employee = employeeService.FindByUsername(context.UserName);
                    //if (employee == null)
                    user = new ApplicationUser() { Id = context.UserName, UserName = context.UserName, ObjectState = ObjectState.Added };
                    //else
                    //    user = new ApplicationUser() { UserName = context.UserName, Email = employee.Email, FirstName = employee.ThFName, LastName = employee.ThLName, ObjectState = ObjectState.Added };
                    IdentityResult result = await userManager.CreateAsync(user);

                    if (!result.Succeeded)
                    {
                        return;
                    }
                }

                ClaimsIdentity oAuthIdentity = await userManager.CreateIdentityAsync(user,
                    context.Options.AuthenticationType);
                var roleList = UserHelper.GetRolesId(oAuthIdentity).Select(m => m.Value);
                var userId = oAuthIdentity.GetUserName();

                foreach (var refererId in GenerateRefererClaim(roleList.ToListSafe(), userId, context.OwinContext.Get<MPSDbContext>()))
                {
                    oAuthIdentity.AddClaim(new Claim(Infrastructure.Security.Claim.CRS_REFERER_CLAIMTYPE, refererId));
                }

                //ClaimsIdentity cookiesIdentity = await userManager.CreateIdentityAsync(user,
                //    CookieAuthenticationDefaults.AuthenticationType);
                AuthenticationProperties properties = CreateProperties(user.UserName);
                AuthenticationTicket ticket = new AuthenticationTicket(oAuthIdentity, properties);
                context.Validated(ticket);
                //context.Request.Context.Authentication.SignIn(cookiesIdentity);
                context.Request.Context.Authentication.SignIn(oAuthIdentity);
            }
        }

        public IEnumerable<string> GenerateRefererClaim(IList<string> roleList, string userId, IMPSDbContext dbContext)
        {
            //var anonymous = from item in dbContext.MenuItems
            //                where (!item.Inactive && !item.Children.Any())
            //                      || (!item.Inactive && item.Children.Any(m => !m.Inactive))
            //                      && (string.IsNullOrEmpty(item.RefererId)
            //                        || item.Referer != null && !item.Referer.RolePermissions.Any() && !item.Referer.UserPermissions.Any())
            //                select item.RefererId;
            //var anonymous = new List<string>()
            //{
            //    "","Home/Home/","Home/Index/"
            //};
            var byRole = from rolePermission in dbContext.RefererRolePermissions
                         join role in roleList on rolePermission.RoleId equals role
                         where rolePermission.Enable
                         select rolePermission.RefererId;

            var byUser = from userPermission in dbContext.RefererUserPermissions
                         where userPermission.UserId == userId
                               && userPermission.Enable
                         select userPermission.RefererId;

            var exceptionByUser = from userPermission in dbContext.RefererUserPermissions
                                  where userPermission.UserId == userId
                                        && !userPermission.Enable
                                  select userPermission.RefererId;

            //var final = anonymous.Union(byRole).Union(byUser).Distinct().Except(exceptionByUser);
            var final = byUser.Union(byRole).Distinct().Except(exceptionByUser);

            return final.ToListSafe();
        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            return Task.FromResult<object>(null);
        }

        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            // Resource owner password credentials does not provide a client ID.
            if (context.ClientId == null)
            {
                context.Validated();
            }

            return Task.FromResult<object>(null);
        }

        public override Task ValidateClientRedirectUri(OAuthValidateClientRedirectUriContext context)
        {
            if (context.ClientId == _publicClientId)
            {
                Uri expectedRootUri = new Uri(context.Request.Uri, "/");

                if (expectedRootUri.AbsoluteUri == context.RedirectUri)
                {
                    context.Validated();
                }
            }

            return Task.FromResult<object>(null);
        }

        public static AuthenticationProperties CreateProperties(string userName)
        {
            IDictionary<string, string> data = new Dictionary<string, string>();
            data.Add("userName", userName);
            using (Resolver.BeginScope())
            {
                //var empService = Resolver.GetInstance<IEmployeeTableService>();
                //var me = empService.FindByUsername(userName);
                var empService = Resolver.GetInstance<IEmployeeFacade>();
                var me = empService.GetUserViewModel(userName);
                if (me != null)
                {
                    var serialized = Newtonsoft.Json.JsonConvert.SerializeObject(me);
                    data.Add("me", serialized);
                }
            }

        //    using (Resolver.BeginScope())
        //    {
        //        
        //        data.Add("userName", userName);
        //        //var service = Resolver.GetInstance<IEmployeeService>();
        //        //var employee = service.FindByUsername(userName);
        //        //if (employee != null)
        //        //{

        //        //    data.Add("sbuCode", employee.SBUCode ?? "");
        //        //}
        //        var roleMemberService = Resolver.GetInstance<IService<RoleMember>>();
        //        var sbuUser =
        //            roleMemberService.GetWhere(m => m.RoleID == "SBU_USER" && m.Username == userName).FirstOrDefault();
        //        string sbuCode = "";
        //        if (sbuUser != null)
        //            sbuCode = sbuUser.SBUCode;

        //        data.Add("sbuCode", sbuCode ?? "");

        //        bool hideBroker =
        //            roleMemberService.GetWhere(m => (m.RoleID == "SBU_USER" || m.RoleID == "PHYSICAL_USER" || m.RoleID == "LA_CENTER_USER") 
        //                && m.Username == userName).Any();

        //        data.Add("hideBroker", hideBroker.ToString());

        //    }

            return new AuthenticationProperties(data);

        }
    }
}