﻿using System.DirectoryServices.AccountManagement;
using System.Threading.Tasks;
using TheMall.MPS.Infrastructure.EntityFramework;
using TheMall.MPS.Infrastructure.Identity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;

namespace TheMall.MPS.API.Identity
{        
    public class ApplicationUserManager : UserManager<ApplicationUser>
    {
        private readonly PrincipalContext _context;
        public ApplicationUserManager(IUserStore<ApplicationUser> store, PrincipalContext context)
            : base(store)
        {
            _context = context;
        }      

        public override async Task<bool> CheckPasswordAsync(ApplicationUser user, string password)
        {
            return await Task.FromResult(_context.ValidateCredentials(user.UserName, password, ContextOptions.Negotiate));
        }

        public static ApplicationUserManager Create(IdentityFactoryOptions<ApplicationUserManager> options, IOwinContext context)
        {
            
            //var manager = new ApplicationUserManager(new UserStore<ApplicationUser>(context.Get<LAQDbContext>()));
            //var manager = new ApplicationUserManager(new UserStore<ApplicationUser>(context.Get<LAQDbContext>()), context.Get<PrincipalContext>());
            var manager = new ApplicationUserManager(new ApplicationUserStore(context.Get<MPSDbContext>()), context.Get<PrincipalContext>());
            // Configure validation logic for usernames
            //manager.UserValidator = new UserValidator<ApplicationUser>(manager)
            //{
            //    AllowOnlyAlphanumericUserNames = false,
            //    RequireUniqueEmail = true

            //};
            // Configure validation logic for passwords
            //manager.PasswordValidator = new PasswordValidator
            //{
            //    RequiredLength = 6,
            //    //RequireNonLetterOrDigit = true,
            //    //RequireDigit = true,
            //    //RequireLowercase = true,
            //    //RequireUppercase = true,
            //};
            var dataProtectionProvider = options.DataProtectionProvider;
            if (dataProtectionProvider != null)
            {
                manager.UserTokenProvider = new DataProtectorTokenProvider<ApplicationUser>(dataProtectionProvider.Create("ASP.NET Identity"));
            }
            return manager;
        }
    }
}