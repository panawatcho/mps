using System.Web.Http;
using TheMall.MPS.API;
using SimpleInjector;
using SimpleInjector.Integration.WebApi;

[assembly: WebActivatorEx.PostApplicationStartMethod(typeof(SimpleInjectorWebApiInitializer), "Initialize")]

namespace TheMall.MPS.API
{
    public static class SimpleInjectorWebApiInitializer
    {

        public static void Initialize()
        {
            var container = new Container();
            
            InitializeContainer(container);           

            // This is an extension method from the integration package.
            container.RegisterWebApiControllers(GlobalConfiguration.Configuration);

            container.Verify();

            GlobalConfiguration.Configuration.DependencyResolver =
                new SimpleInjectorWebApiDependencyResolver(container);

            //using (Resolver.BeginScope())
            //{
            //    AppSettings.Instance.Initialize();
            //}
        }
     
        private static void InitializeContainer(Container container)
        {
            new SimpleInjectorMainPackage().RegisterServices(container);
        }
    }
}