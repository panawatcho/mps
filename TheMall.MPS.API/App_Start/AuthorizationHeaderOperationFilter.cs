﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Filters;
using Swashbuckle.Swagger;

namespace TheMall.MPS.API
{
    public class AuthorizationHeaderOperationFilter : IOperationFilter
    {
        public string token = "Bearer i2bQBBGA5uqnqHU76m2Wo6bwrkPKJGqIC4M5PmqjRA95Y1qx4-fSd65Sf0p4x5dKLM4qinrnec-qGPFz8QB6G6qK3U6VkXKLA7Dl8dLlZihEyLxVkECGJW3iv7XvXwQMOfwudUD_YFzB05rZWnxCtloUhxYa-kEbmTCtzJZ_HfactRA4tT9C8ErlO6us2ltxJSp3CdR1ZPwT4YfF7vHAO8bzciqqmpdzFMLoy5pZfydTR3MKtCOkfOn3ScUsP4IJURkMorwIK0AQUQ2mRnjQqvX3qPkKIG8sAw7YT6PpxaOje5hVPALt2L44p_RXslI0iGH9CufkgmbLh4aQEhNUgEA8LlUHcEI70geFFtW87y8snRsN3Luev0txKk0_A0uhXDly6bUpkAByJNYScFGA6gaN4aORBqELTpG-gorED-03hmFmb7PHVQy08AuTtRy5UITjjsgoyk56PFx5Q44PfVj7Gr6DuCt7U4EgdOttIYyfw6wwbHLgv8p7SYR606fx4WyICMgcEUODImi--Scdg3_pY21F8gYNZPXfMgZhbBuvcQXCZ50PymaiK41c7dpOatfApcSJKs_tKyYrfKegLUcgcDoPDisDbydSAw9zuMyN0vmq3krgGv275qW80Lfeu7OWDEPzA5CvJ82MQeMnwQ";
        public void Apply(Operation operation, SchemaRegistry schemaRegistry, ApiDescription apiDescription)
        {
            var filterPipeline = apiDescription.ActionDescriptor.GetFilterPipeline();
            var isAuthorized = filterPipeline.Select(filterInfo => filterInfo.Instance)
                                             .Any(filter => filter is IAuthorizationFilter);

            var allowAnonymous = apiDescription.ActionDescriptor.GetCustomAttributes<AllowAnonymousAttribute>().Any();

            if (isAuthorized && !allowAnonymous)
            {
                var param = operation.parameters;
                if (param == null)
                    param = new List<Parameter>();

                param.Add(new Parameter
                {
                    name = "Authorization",
                    @in = "header",
                    description = "access token",
                    required = true,
                    type = "string",
                    @default = token
                });
                param.Add(new Parameter
                {
                    name = "X-Referer",
                    @in = "header",
                    description = "access token",
                    required = true,
                    type = "string",
                    @default = "/workspace/index/"
                });

                operation.parameters = param;
            }
        }
    }
}