﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Routing;
using Newtonsoft.Json;
using NLog;

namespace TheMall.MPS.API
{
    public class AccessLogHandler : DelegatingHandler
    {
        private static Logger _logger = LogManager.GetCurrentClassLogger();

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request,
            CancellationToken cancellationToken)
        {
            if (request.Content == null ||
                (request.Content.Headers != null && request.Content.Headers.ContentType != null &&
                request.Content.Headers.ContentType.Equals(MediaTypeHeaderValue.Parse("multipart/form-data"))))
            {
                return await base.SendAsync(request, cancellationToken);
            }
            try
            {
                var entry = CreateAccessLogEntryWithRequestData(request);
                if (request.Content != null)
                {
                    await request.Content.ReadAsStringAsync()
                        .ContinueWith(task =>
                        {
                            entry.RequestContentBody = task.Result;
                        }, cancellationToken);
                }
                return await base.SendAsync(request, cancellationToken)
                    .ContinueWith(task =>
                    {
                        var response = task.Result;

                        // Update the API log entry with response info
                        entry.ResponseStatusCode = (int)response.StatusCode;
                        entry.ResponseTimestamp = DateTime.Now;
                        if (response.RequestMessage != null
                            && response.RequestMessage.GetOwinContext() != null
                            && response.RequestMessage.GetOwinContext().Authentication != null)
                        {
                            var user = response.RequestMessage.GetOwinContext().Authentication.User;
                            if (user != null && user.Identity != null)
                            {
                                entry.User = response.RequestMessage.GetOwinContext().Authentication.User.Identity.Name;
                            }
                        }
                        if (response.Content != null)
                        {
                            //entry.ResponseContentBody = response.Content.ReadAsStringAsync().Result;
                            entry.ResponseContentType = response.Content.Headers.ContentType.MediaType;
                            entry.ResponseHeaders = SerializeHeaders(response.Content.Headers);
                        }

                        // TODO: Save the API log entry to the database
                        _logger.Info("{0},{1},{2},{3},{4}", entry.RequestUri, entry.Machine, entry.RequestIpAddress,
                            entry.User, entry.XReferer);

                        //LogEventInfo e = new LogEventInfo(LogLevel.Trace, "", "Pass my custom value");

                        //e.Properties["User"] = entry.User;
                        //e.Properties["Machine"] = entry.Machine;
                        //e.Properties["RequestIpAddress"] = entry.RequestIpAddress;
                        //e.Properties["RequestContentType"] = entry.RequestContentType;
                        //e.Properties["RequestContentBody"] = entry.RequestContentBody;
                        //e.Properties["RequestUri"] = entry.RequestUri;
                        //e.Properties["RequestMethod"] = entry.RequestMethod;
                        //e.Properties["RequestRouteTemplate"] = entry.RequestRouteTemplate;
                        //e.Properties["RequestRouteData"] = entry.RequestRouteData;
                        //e.Properties["RequestHeaders"] = entry.RequestHeaders;
                        //e.Properties["RequestTimestamp"] = entry.RequestTimestamp;
                        //e.Properties["ResponseContentType"] = entry.RequestContentType;
                        ////e.Properties["ResponseContentBody"] = entry.
                        //e.Properties["ResponseStatusCode"] = entry.ResponseStatusCode;
                        //e.Properties["ResponseHeaders"] = entry.ResponseHeaders;
                        //e.Properties["ResponseTimestamp"] = entry.ResponseTimestamp;
                        //e.Properties["XReferer"] = entry.XReferer;
                        //_logger.Trace("{0} {1} {2} {3}", entry.RequestUri, entry.Machine, entry.RequestIpAddress, entry.User);

                        return response;
                    }, cancellationToken);
            }
            catch (Exception)
            {
            }
            return await base.SendAsync(request, cancellationToken);
        }

        private Infrastructure.Security.AccessLogging CreateAccessLogEntryWithRequestData(HttpRequestMessage request)
        {
            var context = ((HttpContextBase)request.Properties["MS_HttpContext"]);
            var routeData = request.GetRouteData();
            IEnumerable<string> xReferers = null;
            request.Headers.TryGetValues("X-Referer", out xReferers);
            return new Infrastructure.Security.AccessLogging
            {
                //Application = "[insert-calling-app-here]",
                //User = context.User.Identity.Name,
                Machine = Environment.MachineName,
                RequestContentType = context.Request.ContentType,
                RequestRouteTemplate = routeData.Route.RouteTemplate,
                RequestRouteData = SerializeRouteData(routeData),
                RequestIpAddress = context.Request.UserHostAddress,
                RequestMethod = request.Method.Method,
                RequestHeaders = SerializeHeaders(request.Headers),
                RequestTimestamp = DateTime.Now,
                RequestUri = request.RequestUri.ToString(),
                XReferer = xReferers == null || !xReferers.Any() ? null : String.Join("|", xReferers)
            };
        }

        private string SerializeRouteData(IHttpRouteData routeData)
        {
            return JsonConvert.SerializeObject(routeData.Values, Formatting.Indented, new JsonSerializerSettings()
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                DateTimeZoneHandling = DateTimeZoneHandling.Utc
            });
        }

        private string SerializeHeaders(HttpHeaders headers)
        {
            var dict = new Dictionary<string, string>();

            foreach (var item in headers.ToList())
            {
                if (item.Value != null)
                {
                    var header = String.Empty;
                    foreach (var value in item.Value)
                    {
                        header += value + " ";
                    }

                    // Trim the trailing space and add item to the dictionary
                    header = header.TrimEnd(" ".ToCharArray());
                    dict.Add(item.Key, header);
                }
            }

            return JsonConvert.SerializeObject(dict, Formatting.Indented);
        }
    }
}
