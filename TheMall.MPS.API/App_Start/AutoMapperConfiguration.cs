﻿using System;
using System.Linq;
using System.Linq.Expressions;
using AutoMapper;
using AutoMapper.Configuration.Conventions;
using CrossingSoft.Framework.Models.Interfaces;
using TheMall.MPS.Infrastructure.Extension;
using TheMall.MPS.ViewModels;


namespace TheMall.MPS.API
{
    public class AutoMapperConfiguration
    {
        public static void Configure()
        {
            AutoMapper.Mapper.Initialize(cfg =>
            {
                //cfg.ShouldMapProperty = pi => !pi.PropertyType.IsClass;
                {
                    var mappers =
                        from type in System.Reflection.Assembly.GetExecutingAssembly().GetExportedTypes()
                        where type.Namespace == "TheMall.MPS.API.Mappers"
                              && type.Name.EndsWith("Profile")
                              && type.IsSubclassOf(typeof(AutoMapper.Profile))
                              && type.IsClass
                        select type;
                    //AutoMapper.Mapper.Initialize(configAction);
                    foreach (var mapper in mappers)
                    {
                        var mapper1 = (AutoMapper.Profile)System.Activator.CreateInstance(mapper);
                        cfg.AddProfile(mapper1);
                    }
                }
                cfg.CreateMap<Models.Enums.MailType, ViewModels.StaticDatasource>()
                    .ConvertUsing((source, destination, resolutionContext) => EnumToStaticDatasource(source, destination, resolutionContext));
                cfg.CreateMap<ViewModels.StaticDatasource, Models.Enums.MailType>()
                    .ConvertUsing((source, destination, resolutionContext) => (Models.Enums.MailType)StaticDatasourceToEnum(source, destination, resolutionContext));

               
               
            });
        }

        public static StaticDatasource EnumToStaticDatasource<TSource>(TSource source, StaticDatasource destination, ResolutionContext resolutionContext)
        {
            if (source == null)
            {

                return null;
                //var defaultValue = (int)Enum.GetValues(resolutionContext.DestinationType).GetValue(0);

                //return EnumExtension.ToStaticDatasource(resolutionContext.SourceType, defaultValue);
            }
            try
            {
                return EnumExtension.ToStaticDatasource(source.GetType(), System.Convert.ToInt32(source));
            }
            catch (Exception)
            {
                return null;
            }
        }
        public static int StaticDatasourceToEnum<TDestination>(StaticDatasource source, TDestination destination, ResolutionContext resolutionContext)
        {
            //var dataSource = resolutionContext.SourceValue as ViewModels.StaticDatasource;
            var dataSource = source;
            if (dataSource != null)
            {
                return System.Convert.ToInt32(dataSource.value);
            }
            return (int)EnumExtension.GetValues(destination.GetType()).GetValue(0);
            //return 0;
        }

      
    }
    public static class AutoMapperExtensions
    {
        public static IMappingExpression<TSource, TDestination> Ignore<TSource, TDestination>(
            this IMappingExpression<TSource, TDestination> map,
            Expression<Func<TDestination, object>> selector)
        {
            map.ForMember(selector, opt => opt.Ignore());
            return map;
        }

        public static IMappingExpression<TSource, TDestination>
               IgnoreAllVirtual<TSource, TDestination>(
                   this IMappingExpression<TSource, TDestination> expression)
        {
            var desType = typeof(TDestination);
            foreach (var property in desType.GetProperties().Where(m => m.GetMethod.IsVirtual && !m.PropertyType.IsPrimitive && m.PropertyType.IsPublic && !m.PropertyType.IsSealed))
            {
                expression.ForMember(property.Name, opt => opt.Ignore());
            }

            return expression;
        }
    }
}