﻿using System;
using System.Linq;
using CrossingSoft.Framework.Infrastructure.DataContext;
using CrossingSoft.Framework.Infrastructure.EF6.Factories;
using CrossingSoft.Framework.Infrastructure.OData.Repositories;
using CrossingSoft.Framework.Infrastructure.Repositories;
using CrossingSoft.Framework.Infrastructure.Session;
using CrossingSoft.Framework.Services;
using CrossingSoft.Framework.Services.OData;
using TheMall.MPS.API;
using TheMall.MPS.API.Services;
using TheMall.MPS.Infrastructure.EntityFramework;
using TheMall.MPS.Infrastructure.Repositories;
using TheMall.MPS.Infrastructure.Sessions;
using TheMall.MPS.Models.FileManager;
using SimpleInjector;
using SimpleInjector.Integration.WebApi;
using CrossingSoft.Framework.Infrastructure.K2;
using CrossingSoft.Framework.Models.Interfaces;
using TheMall.MPS.API.Factories;
using TheMall.MPS.API.Services.Abstracts;
using TheMall.MPS.API.Services.Interfaces;
using TheMall.MPS.Models.Workflow;
using TheMall.MPS.Models.Workflow.Abstracts;

namespace TheMall.MPS.API
{
    public class SimpleInjectorMainPackage : SimpleInjector.Packaging.IPackage
    {
        public void RegisterServices(Container container)
        {
            var webApiRequestLifeStyle = new WebApiRequestLifestyle();
            var hybridLifestyle = Lifestyle.CreateHybrid(
                lifestyleSelector: () => System.Web.HttpContext.Current != null,
                trueLifestyle: new WebApiRequestLifestyle(),
                falseLifestyle: Lifestyle.Transient);
            container.Register(typeof(IApplicationSettingService), typeof(ApplicationSettingService), hybridLifestyle);

            container.RegisterWebApiRequest<IMPSConnectionString, MPSConnectionString>();
            container.RegisterWebApiRequest<IMPSDbContext, MPSDbContext>();

            container.RegisterWebApiRequest<IK2DbConnectionString, K2DbConnectionString>();
            container.RegisterWebApiRequest<IK2DbContext, K2DbContext>();

            container.Register<IRepositoryProvider>(() => new RepositoryProvider(new RepositoryFactories()), webApiRequestLifeStyle);
            container.RegisterWebApiRequest<IMPSSession, MPSSession>();

            container.RegisterConditional(typeof(IMpsODataRepositoryAsync<>), typeof(TheMall.MPS.Infrastructure.Repositories.TrackableRepository<>), webApiRequestLifeStyle,
                c => !c.Handled && c.ServiceType.GetGenericArguments().Last().GetInterfaces().Any(m => m.Name == typeof(ITrackableModel).Name));
            container.RegisterConditional(typeof(IMpsRepositoryAsync<>), typeof(TheMall.MPS.Infrastructure.Repositories.TrackableRepository<>), webApiRequestLifeStyle,
                c => !c.Handled && c.ServiceType.GetGenericArguments().Last().GetInterfaces().Any(m => m.Name == typeof(ITrackableModel).Name));
            container.RegisterConditional(typeof(IODataRepositoryAsync<,,>), typeof(TheMall.MPS.Infrastructure.Repositories.TrackableRepository<,,>), webApiRequestLifeStyle,
                c => !c.Handled && c.ServiceType.GetGenericArguments().Last().GetInterfaces().Any(m => m.Name == typeof(ITrackableModel).Name));
            container.RegisterConditional(typeof(IODataRepositoryAsync<>), typeof(TheMall.MPS.Infrastructure.Repositories.TrackableRepository<>), webApiRequestLifeStyle,
                c => !c.Handled && c.ServiceType.GetGenericArguments().Last().GetInterfaces().Any(m => m.Name == typeof(ITrackableModel).Name));
            container.RegisterConditional(typeof(IODataRepository<>), typeof(TheMall.MPS.Infrastructure.Repositories.TrackableRepository<>), webApiRequestLifeStyle,
                c => !c.Handled && c.ServiceType.GetGenericArguments().Last().GetInterfaces().Any(m => m.Name == typeof(ITrackableModel).Name));
            container.RegisterConditional(typeof(IRepositoryAsync<>), typeof(TheMall.MPS.Infrastructure.Repositories.TrackableRepository<>), webApiRequestLifeStyle,
                c => !c.Handled && c.ServiceType.GetGenericArguments().Last().GetInterfaces().Any(m => m.Name == typeof(ITrackableModel).Name));
            container.RegisterConditional(typeof(IRepository<>), typeof(TheMall.MPS.Infrastructure.Repositories.TrackableRepository<>), webApiRequestLifeStyle,
                c => !c.Handled && c.ServiceType.GetGenericArguments().Last().GetInterfaces().Any(m => m.Name == typeof(ITrackableModel).Name));


            container.RegisterConditional(typeof(IMpsODataRepositoryAsync<>), typeof(TheMall.MPS.Infrastructure.Repositories.MpsODataRepositoryAsync<>), webApiRequestLifeStyle, c => !c.Handled);
            container.RegisterConditional(typeof(IMpsRepositoryAsync<>), typeof(TheMall.MPS.Infrastructure.Repositories.MpsRepositoryAsync<>), webApiRequestLifeStyle, c => !c.Handled);
            container.RegisterConditional(typeof(IODataRepositoryAsync<,,>), typeof(CrossingSoft.Framework.Infrastructure.EF6.Repository<,,>), webApiRequestLifeStyle, c => !c.Handled);
            container.RegisterConditional(typeof(IODataRepositoryAsync<>), typeof(CrossingSoft.Framework.Infrastructure.EF6.Repository<>), webApiRequestLifeStyle, c => !c.Handled);
            container.RegisterConditional(typeof(IODataRepository<>), typeof(CrossingSoft.Framework.Infrastructure.EF6.Repository<>), webApiRequestLifeStyle, c => !c.Handled);
            container.RegisterConditional(typeof(IRepositoryAsync<>), typeof(CrossingSoft.Framework.Infrastructure.EF6.Repository<>), webApiRequestLifeStyle, c => !c.Handled);
            container.RegisterConditional(typeof(IRepository<>), typeof(CrossingSoft.Framework.Infrastructure.EF6.Repository<>), webApiRequestLifeStyle, c => !c.Handled);

            //container.Register(typeof(IWorkflowService<>), new[] { typeof(WorkflowService<>).Assembly }, webApiRequestLifeStyle);
            //container.RegisterWebApiRequest<IWorkflowService<AdvanceClearingTable>, WorkflowService<AdvanceClearingTable>>();
            //container.RegisterWebApiRequest<IWorkflowService<AdvancePaymentTable>, WorkflowService<AdvancePaymentTable>>();
            // K2 Connections
            container.RegisterWebApiRequest<IHostConnection>(Connections.HostConnectionInjection.Inject);
            container.RegisterWebApiRequest<IWorkflowConnection>(Connections.WorkflowConnectionInjection.Inject);
            container.RegisterWebApiRequest<IWorkflowProcessAdminConnection>(Connections.WorkflowAdminConnectionInjection.Inject);
            container.Register<IWorkflowProcessAdminRepository, WorkflowProcessAdminRepository>(webApiRequestLifeStyle);
            container.Register(typeof(IK2Repository<>), typeof(CrossingSoft.Framework.Infrastructure.K2.Repository<>), webApiRequestLifeStyle);
            container.RegisterWebApiRequest<IWorkflowRepository, WorkflowRepository>();


            //container.Register(typeof(IWorkflowService<>), new[] { typeof(WorkflowService<>).Assembly }, webApiRequestLifeStyle);
            //container.Register(typeof(IMpsWorkflowTableService<>), new[] { typeof(MpsWorkflowTableService<>).Assembly }, webApiRequestLifeStyle);
           
            container.RegisterWebApiRequest<IProposalProcess, ProposalProcess>();
            container.RegisterWebApiRequest<IPettyCashProcess, PettyCashProcess>();
            container.RegisterWebApiRequest<IMemoProcess, MemoProcess>();

            container.RegisterWebApiRequest<IMemoIncomeProcess, MemoIncomeProcess>(); //k2

            container.RegisterWebApiRequest<ICashAdvanceProcess, CashAdvanceProcess>();
            container.RegisterWebApiRequest<ICashClearingProcess, CashClearingProcess>();
            container.RegisterSingleton<IMPSWorkflowServiceFactory>(new MPSWorkflowServiceFactory
            {
                { ProcessModelHelper.ProcessCode(typeof(ProposalProcess)), container.GetInstance<IProposalService> },
                { ProcessModelHelper.ProcessCode(typeof(PettyCashProcess)), container.GetInstance<IPettyCashService> },
                { ProcessModelHelper.ProcessCode(typeof(MemoProcess)), container.GetInstance<IMemoService> },
                { ProcessModelHelper.ProcessCode(typeof(CashAdvanceProcess)), container.GetInstance<ICashAdvanceService> },
                { ProcessModelHelper.ProcessCode(typeof(CashClearingProcess)), container.GetInstance<ICashClearingService> },
            });

            var assembly = System.Reflection.Assembly.GetExecutingAssembly(); // Automatic registration of service namespace
            var services =
                from type in assembly.GetExportedTypes()
                where type.Namespace == "TheMall.MPS.API.Services"
                where type.GetInterfaces().Any()
                where !type.IsGenericType
                where !type.IsInterface
                where type.Name != "ApplicationSettingService"
                select new { Service = type.GetInterfaces().FirstOrDefault(m => m.Namespace == "TheMall.MPS.API.Services"), Implementation = type };
            foreach (var service in services.Where(m => m.Service != null))
            {
                if (service.Service != null)
                {
                    container.RegisterConditional(service.Service, service.Implementation, webApiRequestLifeStyle, c => !c.Handled);
                    //container.Register(service.Service, service.Implementation, webApiRequestLifeStyle);
                }
            }
            container.Register(typeof(IWorkflowService<>), new[] { typeof(WorkflowService<>).Assembly }, webApiRequestLifeStyle);
            container.Register(typeof(IAttachmentService<>), typeof(AttachmentService<>), webApiRequestLifeStyle);
            container.RegisterConditional(typeof(IAttachmentService<,>), typeof(AttachmentService<,>), webApiRequestLifeStyle, c => !c.Handled);
            container.Register(typeof(ICommentService<>), typeof(CommentService<>), webApiRequestLifeStyle);
           // container.Register(typeof(IMpsWorkflowTableService<>), new[] { typeof(MpsWorkflowTableService<>).Assembly }, webApiRequestLifeStyle);

            // K2 Connections
            //container.RegisterWebApiRequest<IHostConnection>(Connections.HostConnectionInjection.Inject);
            //container.RegisterWebApiRequest<IWorkflowConnection>(Connections.WorkflowConnectionInjection.Inject);
            //container.RegisterWebApiRequest<IWorkflowProcessAdminConnection>(Connections.WorkflowAdminConnectionInjection.Inject);
            //container.Register<IWorkflowProcessAdminRepository, WorkflowProcessAdminRepository>(webApiRequestLifeStyle);
            //container.Register(typeof(IK2Repository<>), typeof(CrossingSoft.Framework.Infrastructure.K2.Repository<>), webApiRequestLifeStyle);

            // Facades
            // Automatic registration of facade namespace
            var facades =
                from type in assembly.GetExportedTypes()
                where type.Namespace == "TheMall.MPS.API.Facades"
                where type.GetInterfaces().Any()
                where !type.IsGenericType
                where !type.IsInterface
                select new { Service = type.GetInterfaces().FirstOrDefault(m => m.Namespace == "TheMall.MPS.API.Facades"), Implementation = type };

            foreach (var facade in facades.Where(m => m.Service != null))
            {
                container.Register(facade.Service, facade.Implementation, webApiRequestLifeStyle);
            }
            //container.Register(typeof(IAttachmentService<>), typeof(AttachmentService<>), webApiRequestLifeStyle);
            //container.Register(typeof(IHaveAttachmentFacade<>), typeof(ExampleFacade<>), webApiRequestLifeStyle);

            //var iHaveAttachments =
            //    from type in assembly.GetExportedTypes()
            //    where (type.GetInterfaces().Any(m => m.Name == typeof (Facades.Interfaces.IHaveAttachmentFacade<>).Name)
            //    || type.BaseType != null && type.BaseType.GetInterfaces().Any(m => m.Name == typeof (Facades.Interfaces.IHaveAttachmentFacade<>).Name))
            //    where !type.IsAbstract
            //    where !type.IsInterface
            //    select new
            //    {
            //        Service = (type.BaseType != null && type.BaseType.GetInterfaces().Any(m => m.Name == typeof (Facades.Interfaces.IHaveAttachmentFacade<>).Name)) ?
            //                type.BaseType.GetInterfaces().FirstOrDefault(m => m.Name == typeof(Facades.Interfaces.IHaveAttachmentFacade<>).Name)
            //                : type.GetInterfaces().FirstOrDefault(m => m.Name == typeof (Facades.Interfaces.IHaveAttachmentFacade<>).Name),
            //        Implementation = type,
            //        OriginalService = type.GetInterfaces().FirstOrDefault(m => m.Namespace == "TheMall.MPS.API.Facades"
            //            && m.Name != typeof (Facades.Interfaces.IHaveAttachmentFacade<>).Name),
            //    };

            //foreach (var type in iHaveAttachments)
            //{
            //    var type1 = type;
            //    container.Register(type.Service, () => container.GetInstance(type1.OriginalService), webApiRequestLifeStyle);
            //}

            container.RegisterConditional(typeof(IODataServiceAsync<>), ctx => IServiceTypeFactory(ctx, assembly, container), webApiRequestLifeStyle,
                c => !c.Handled);
            container.RegisterConditional(typeof(CrossingSoft.Framework.Services.OData.IODataService<>), ctx => IServiceTypeFactory(ctx, assembly, container), webApiRequestLifeStyle,
                c => !c.Handled);
            container.RegisterConditional(typeof(IServiceAsync<>), ctx => IServiceTypeFactory(ctx, assembly, container), webApiRequestLifeStyle,
                c => !c.Handled);
            container.RegisterConditional(typeof(CrossingSoft.Framework.Services.IService<>), ctx => IServiceTypeFactory(ctx, assembly, container), webApiRequestLifeStyle,
                c => !c.Handled);

            var mappers =
                from type in assembly.GetExportedTypes()
                where type.Namespace == "TheMall.MPS.API.Mappers"
                where type.GetInterfaces().Any()
                where !type.IsGenericType
                where !type.IsInterface
                select new { Service = type.GetInterfaces().FirstOrDefault(m => m.Namespace == "TheMall.MPS.API.Mappers"), Implementation = type };

            foreach (var mapper in mappers.Where(m => m.Service != null))
            {
                container.Register(mapper.Service, mapper.Implementation, webApiRequestLifeStyle);
            }

        }

        private static bool IsBothTrackableAndPsudoDelete(System.Type[] interfaces)
        {
            if (interfaces.Any(m => m.Name == typeof(CrossingSoft.Framework.Models.Interfaces.ITrackableModel).Name)
                &&
                interfaces.Any(m => m.Name == typeof(CrossingSoft.Framework.Models.Interfaces.IPseudoDelete).Name))
            {
                return true;
            }
            return false;
        }

        private Type IServiceTypeFactory(TypeFactoryContext typeFactoryContext, System.Reflection.Assembly assembly, Container container)
        {
            var types = from type in assembly.GetExportedTypes()
                        where type.Namespace == "TheMall.MPS.API.Services"

                        where (type.GetInterfaces().FirstOrDefault(m => m.FullName == typeof(CrossingSoft.Framework.Services.IService<>).FullName) != null && type.GetInterfaces().First(m => m.FullName == typeof(CrossingSoft.Framework.Services.IService<>).FullName).GetGenericArguments().Any(m => m == typeFactoryContext.ServiceType.GetGenericArguments().First()))
                        || (type.GetInterfaces().FirstOrDefault(m => m.FullName == typeof(IServiceAsync<>).FullName) != null && type.GetInterfaces().First(m => m.FullName == typeof(IServiceAsync<>).FullName).GetGenericArguments().Any(m => m == typeFactoryContext.ServiceType.GetGenericArguments().First()))
                        || (type.GetInterfaces().FirstOrDefault(m => m.FullName == typeof(CrossingSoft.Framework.Services.OData.IODataService<>).FullName) != null && type.GetInterfaces().First(m => m.FullName == typeof(CrossingSoft.Framework.Services.OData.IODataService<>).FullName).GetGenericArguments().Any(m => m == typeFactoryContext.ServiceType.GetGenericArguments().First()))
                        || (type.GetInterfaces().FirstOrDefault(m => m.FullName == typeof(IODataServiceAsync<>).FullName) != null && type.GetInterfaces().First(m => m.FullName == typeof(IODataServiceAsync<>).FullName).GetGenericArguments().Any(m => m == typeFactoryContext.ServiceType.GetGenericArguments().First()))
                        //where (type.GetInterfaces().FirstOrDefault(m => m.FullName == typeof(IService<>).Name) != null && type.GetInterfaces().First(m => m.FullName == typeof(IService<>).Name).GetGenericArguments().Any(m => m == typeFactoryContext.ServiceType.GetGenericArguments().First()))
                        //|| (type.GetInterface(typeof(IServiceAsync<>).Name) != null && type.GetInterface(typeof(IService<>).Name).GetGenericArguments().Any(m => m == typeFactoryContext.ServiceType.GetGenericArguments().First()))
                        //|| (type.GetInterface(typeof(IODataService<>).Name) != null && type.GetInterface(typeof(IService<>).Name).GetGenericArguments().Any(m => m == typeFactoryContext.ServiceType.GetGenericArguments().First()))
                        //|| (type.GetInterface(typeof(IODataServiceAsync<>).Name) != null && type.GetInterface(typeof(IService<>).Name).GetGenericArguments().Any(m => m == typeFactoryContext.ServiceType.GetGenericArguments().First()))
                        where !type.IsGenericType
                        where type.IsInterface
                        select type;

            var service = types.FirstOrDefault();
            if (service != null)
            {
                var registration = container.GetRegistration(service);
                return registration.Registration.ImplementationType;
            }
            else
            {
                return typeof(BasicService<>);
            }

        }
    }
}