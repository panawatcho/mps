﻿using System;
using System.Configuration;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Cors;
using TheMall.MPS.API.Identity;
using TheMall.MPS.API.Identity.Providers;
using TheMall.MPS.Infrastructure.EntityFramework;
using TheMall.MPS.Infrastructure.Identity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using Owin;

namespace TheMall.MPS.API
{
    public partial class Startup
    {
        public static OAuthAuthorizationServerOptions OAuthOptions { get; private set; }
        //public static Func<UserManager<IdentityUser>> UserManagerFactory { get; set; }
        //public static Func<ApplicationUserManager> UserManagerFactory { get; set; }
        public static string PublicClientId { get; private set; }

        // Enable the application to use OAuthAuthorization. You can then secure your Web APIs
        static Startup()
        {
            PublicClientId = "self";
            //UserManagerFactory = () => new ApplicationUserManager(new ApplicationUserStore(), );
            OAuthOptions = new OAuthAuthorizationServerOptions
            {
                TokenEndpointPath = new PathString("/Token"),
                AuthorizeEndpointPath = new PathString("/Account/Authorize"),
                //Provider = new ApplicationOAuthProvider(PublicClientId, UserManagerFactory),
                Provider = new ApplicationOAuthProvider(PublicClientId),
#if PROD || DEV
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(1),
#else
                  AccessTokenExpireTimeSpan = TimeSpan.FromDays(5),
#endif
                AllowInsecureHttp = true
            };
        }
        public void ConfigureAuth(IAppBuilder app)
        {
            var corSites = AppSettings.Instance.CorsSites();
            var policy = new CorsPolicy
            {
                AllowAnyHeader = true,
                AllowAnyMethod = true,
                AllowAnyOrigin = true,
                SupportsCredentials = true
            };
            //foreach (var site in corSites)
            //{
            //    policy.Origins.Add(site);
            //}
            //policy.Headers.Add("Origin");
            //policy.Headers.Add("X-Requested-With");
            //policy.Headers.Add("Content-Type");
            //policy.Headers.Add("Accept");
            //policy.Headers.Add("Authorization");
            //policy.Headers.Add("X-Referer");

            //policy.Methods.Add("GET");
            //policy.Methods.Add("POST");
            //policy.Methods.Add("PUT");
            //policy.Methods.Add("DELETE");
            //policy.Methods.Add("OPTIONS");
            policy.ExposedHeaders.Add("X-File-Name");
            app.UseCors(new CorsOptions
            {
                PolicyProvider = new CorsPolicyProvider
                {
                    PolicyResolver = context => Task.FromResult(policy)
                }
            });
            //app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);

            app.CreatePerOwinContext(MPSDbContext.Create);
            app.CreatePerOwinContext(() => new PrincipalContext(ContextType.Domain));
            app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);
            app.CreatePerOwinContext<ApplicationRoleManager>(ApplicationRoleManager.Create);
            // Enable the application to use a cookie to store information for the signed in user
            //app.UseCookieAuthentication(new CookieAuthenticationOptions
            //{
            //    AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
            //    LoginPath = new PathString("/Account/Login"),
            //    Provider = new CookieAuthenticationProvider
            //    {
            //        // Enables the application to validate the security stamp when the user logs in.
            //        // This is a security feature which is used when you change a password or add an external login to your account.  
            //        OnValidateIdentity = Microsoft.AspNet.Identity.Owin.SecurityStampValidator.OnValidateIdentity<ApplicationUserManager, Infrastructure.Identity.ApplicationUser>(
            //            validateInterval: TimeSpan.FromMinutes(0),
            //            regenerateIdentity: (manager, user) => user.GenerateUserIdentityAsync(manager, DefaultAuthenticationTypes.ApplicationCookie))
            //    }
            //});
            // Use a cookie to temporarily store information about a user logging in with a third party login provider
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            // Enables the application to temporarily store user information when they are verifying the second factor in the two-factor authentication process.
            app.UseTwoFactorSignInCookie(DefaultAuthenticationTypes.TwoFactorCookie, TimeSpan.FromMinutes(5));

            // Enables the application to remember the second login verification factor such as phone or email.
            // Once you check this option, your second step of verification during the login process will be remembered on the device where you logged in from.
            // This is similar to the RememberMe option when you log in.
            app.UseTwoFactorRememberBrowserCookie(DefaultAuthenticationTypes.TwoFactorRememberBrowserCookie);

            // Enable the application to use bearer tokens to authenticate users
            app.UseOAuthBearerTokens(OAuthOptions);

            // Uncomment the following lines to enable logging in with third party login providers
            //app.UseMicrosoftAccountAuthentication(
            //    clientId: "",
            //    clientSecret: "");

            //app.UseTwitterAuthentication(
            //    consumerKey: "",
            //    consumerSecret: "");

            //app.UseFacebookAuthentication(
            //    appId: "",
            //    appSecret: "");

            //app.UseGoogleAuthentication(new GoogleOAuth2AuthenticationOptions()
            //{
            //    ClientId = "",
            //    ClientSecret = ""
            //});
        }
    }

}
