﻿using System.Web.Http;
using System.Web.Http.OData.Builder;
using System.Web.Http.OData.Extensions;

namespace TheMall.MPS.API
{
    public static class ODataConfig
    {
        public static void Register(HttpConfiguration config)
        {
            ODataModelBuilder builder = new ODataConventionModelBuilder();

            builder.EntitySetRegisterLowerCase<Models.Example.ExampleTable>();
            builder.EntitySetRegisterLowerCase<Models.Example.ExampleAttachment>();
            builder.EntitySetRegisterLowerCase<Models.Example.ExampleLine>();
            builder.EntitySetRegisterLowerCase<Models.Example.ExampleLineAttachment>();
            //main Table
            builder.EntitySetRegisterLowerCase<Models.Proposal.MPS_ProposalTable>("proposaltable").EntityType.Ignore(m => m.Objective);
            builder.EntitySetRegisterLowerCase<Models.Proposal.MPS_ProposalLine>();
            builder.EntitySetRegisterLowerCase<Models.Proposal.MPS_ProposalLineTemplate>();
            builder.EntitySetRegisterLowerCase<Models.Proposal.MPS_ProposalComment>();
            builder.EntitySetRegisterLowerCase<Models.Proposal.MPS_ProposalAttachment>();
            builder.EntitySetRegisterLowerCase<Models.Proposal.MPS_IncomeDeposit>();
            builder.EntitySetRegisterLowerCase<Models.Proposal.MPS_IncomeOther>();
            builder.EntitySetRegisterLowerCase<Models.Proposal.MPS_IncomeTotalSale>();
            builder.EntitySetRegisterLowerCase<Models.Proposal.MPS_IncomeTotalSaleTemplate>();
            builder.EntitySetRegisterLowerCase<Models.Proposal.MPS_EstimateTotalSale>();
            builder.EntitySetRegisterLowerCase<Models.Proposal.MPS_EstimateTotalSaleTemplate>();
            builder.EntitySetRegisterLowerCase<Models.Proposal.MPS_DepositLine>();
            builder.EntitySetRegisterLowerCase<Models.Proposal.MPS_ProposalApproval>();
            builder.EntitySetRegisterLowerCase<Models.Proposal.MPS_ProposalLineSharedTemplate>();

            builder.EntitySetRegisterLowerCase<Models.PettyCash.MPS_PettyCashTable>("pettycashtable");
            builder.EntitySetRegisterLowerCase<Models.PettyCash.MPS_PettyCashApproval>();
            builder.EntitySetRegisterLowerCase<Models.PettyCash.MPS_PettyCashLine>();
            builder.EntitySetRegisterLowerCase<Models.PettyCash.MPS_PettyCashComment>();
            builder.EntitySetRegisterLowerCase<Models.PettyCash.MPS_PettyCashAttachment>();

            builder.EntitySetRegisterLowerCase<Models.CashAdvance.MPS_CashAdvanceTable>("cashadvancetable");
            builder.EntitySetRegisterLowerCase<Models.CashAdvance.MPS_CashAdvanceLine>();
            builder.EntitySetRegisterLowerCase<Models.CashAdvance.MPS_CashAdvanceComment>();
            builder.EntitySetRegisterLowerCase<Models.CashAdvance.MPS_CashAdvanceAttachment>();
            builder.EntitySetRegisterLowerCase<Models.CashAdvance.MPS_CashAdvanceApproval>();

            builder.EntitySetRegisterLowerCase<Models.CashClearing.MPS_CashClearingTable>("cashclearingtable");
            builder.EntitySetRegisterLowerCase<Models.CashClearing.MPS_CashClearingLine>();
            builder.EntitySetRegisterLowerCase<Models.CashClearing.MPS_CashClearingComment>();
            builder.EntitySetRegisterLowerCase<Models.CashClearing.MPS_CashClearingAttachment>();
            builder.EntitySetRegisterLowerCase<Models.CashClearing.MPS_CashClearingApproval>();


            //Master Table
            builder.EntitySetRegisterLowerCase<Models.Master.MPS_M_UnitCode>("unitcodetable");
            builder.EntitySetRegisterLowerCase<Models.Master.MPS_M_Place>("placetable");
            //Final Approver Master Table
            builder.EntitySetRegisterLowerCase<Models.Master.MPS_M_FinalApprover>("finalapprovertable");
            builder.EntitySetRegisterLowerCase<Models.MPS_M_EmployeeTable>("employeetable");
            //Shared branch Table
            builder.EntitySetRegisterLowerCase<Models.Master.MPS_M_SharedBranch>("sharedbranchtable");
            //Unit code for employee Table
            builder.EntitySetRegisterLowerCase<Models.Master.MPS_M_UnitCodeForEmployee>("unitcodeforemployeetable");
            //Mail group
            builder.EntitySetRegisterLowerCase<Models.Master.MPS_M_MailGroup>("mailgrouptable");
            //Process approve
            builder.EntitySetRegisterLowerCase<Models.Master.MPS_M_ProcessApprove>("processapprovetable");

            builder.EntitySetRegisterLowerCase<Models.Master.MPS_M_BudgetYearPlan>("budgetyearplantable");
            builder.EntitySetRegisterLowerCase<Models.Master.MPS_M_TypeMemo>("typememotable");
            builder.EntitySetRegisterLowerCase<Models.Master.MPS_M_TypeProposal>("typeproposaltable");
            builder.EntitySetRegisterLowerCase<Models.Master.MPS_M_ExpenseTopic>("expensetopictable");
            builder.EntitySetRegisterLowerCase<Models.Master.MPS_M_APCode>("apcodetable");
            //builder.EntitySetRegisterLowerCase<Models.Master.MPS_M_AllocateBasis>("allocatebasistable");

            //builder.EntitySetRegisterLowerCase<Models.Master.MPS_M_SharedPercent>("sharedpercenttable");

            builder.EntitySetRegisterLowerCase<Models.Memo.MPS_MemoTable>("memotable");
            builder.EntitySetRegisterLowerCase<Models.Memo.MPS_MemoLine>();
            builder.EntitySetRegisterLowerCase<Models.Memo.MPS_MemoComment>();
            builder.EntitySetRegisterLowerCase<Models.Memo.MPS_MemoAttachment>();
            builder.EntitySetRegisterLowerCase<Models.Memo.MPS_MemoApproval>();

            builder.EntitySetRegisterLowerCase<Models.View.MPS_VW_BudgetTracking>("budgettrackingview");
            builder.EntitySetRegisterLowerCase<Models.View.MPS_VW_ProposalTracking>("proposaltrackingview");
            builder.EntitySetRegisterLowerCase<Models.View.MPS_VW_ProposalUnitCode>("proposalunitcodeview");
            builder.EntitySetRegisterLowerCase<Models.View.MPS_VW_BudgetYearTracking>("budgetyeartrackingview");
            builder.EntitySetRegisterLowerCase<Models.View.MPS_VW_DocumentRelated>("documentrelatedview");
            //builder.EntitySetRegisterLowerCase<Models.View.MPS_VW_DepositTracking>("deposittrackingview");
            //builder.EntitySetRegisterLowerCase<Models.View.MPS_VW_Email>("emailview");

            builder.EntitySetRegisterLowerCase<Models.Receipt.MPS_ReceiptTable>("receipttable");
            builder.EntitySetRegisterLowerCase<Models.Receipt.MPS_ReceiptLine>();

            //Memo income
            builder.EntitySetRegisterLowerCase<Models.MemoIncome.MPS_MemoIncomeTable>("memoincometable");
            builder.EntitySetRegisterLowerCase<Models.MemoIncome.MPS_MemoIncomeLine>();
            builder.EntitySetRegisterLowerCase<Models.MemoIncome.MPS_MemoIncomeComment>();
            builder.EntitySetRegisterLowerCase<Models.MemoIncome.MPS_MemoIncomeAttachment>();
            builder.EntitySetRegisterLowerCase<Models.MemoIncome.MPS_MemoIncomeApproval>();
            builder.EntitySetRegisterLowerCase<Models.MemoIncome.MPS_MemoIncomeInvoice>();

            var model = builder.GetEdmModel();
            config.Routes.MapODataServiceRoute("odata", "odata", model);
            config.AddODataQueryFilter();
        }
    }

    public static class ODataModelBuilderExtension
    {
        public static EntitySetConfiguration<T> EntitySetRegisterLowerCase<T>(this ODataModelBuilder builder)
            where T : class
        {
            return builder.EntitySet<T>(typeof(T).Name.ToLower());
        }
        public static EntitySetConfiguration<T> EntitySetRegisterLowerCase<T>(this ODataModelBuilder builder, string name)
           where T : class
        {
            return builder.EntitySet<T>(name.ToLower());
        }
    }
}