﻿using System.Web.Http;
using System.Web.Http.Cors;
using TheMall.MPS.API.Attributes;

namespace TheMall.MPS.API
{
    public static class WebApiConfig
    {
        public const string DefaultApiRoute = "DefaultApi";
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            //var cors = new EnableCorsAttribute("*", "*", "*");
            //var cors = new EnableCorsAttribute(string.Join(",", AppSettings.Instance.CorsSites()), "Origin, X-Requested-With, Content-Type, Accept, Authorization, X-Referer", "GET, PUT, POST, DELETE, HEAD, OPTIONS");
            //cors.SupportsCredentials = true;
            //cors.ExposedHeaders.Add("X-File-Name");
            //config.EnableCors(cors);
            config.SuppressDefaultHostAuthentication();
            config.Filters.Add(new HostAuthenticationFilter(Microsoft.Owin.Security.OAuth.OAuthDefaults.AuthenticationType));
            config.Filters.Add(new HandleExceptionAttribute());
            config.Filters.Add(new Attributes.RefererAuthorize());

            // Web API routes
            config.MapHttpAttributeRoutes(new CustomDirectRouteProvider());

            config.Routes.MapHttpRoute(
                name: DefaultApiRoute,
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
            config.Routes.MapHttpRoute(
                name: "AttachmentRoute",
                routeTemplate: "api/{controller}/{id}/attachment/{attachmentId}");
            config.Routes.MapHttpRoute(
                name: "FileManagerFile",
                routeTemplate: "api/filemanager/directory/{directoryId}/document/{documentId}/attachment/{attachmentId}");
        }
    }
}
