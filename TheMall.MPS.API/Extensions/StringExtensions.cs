﻿using System;
using TheMall.MPS.Models.Enums;
using TheMall.MPS.Models.Master;
using TheMall.MPS.ViewModels.Master;

public static class StringExtensions
{
    public static string RemoveDomain(this string source)
    {
        var i = source.LastIndexOf(@"\", System.StringComparison.InvariantCultureIgnoreCase);
        if (i > 0)
        {
            return source.Substring(i + 1);
        }
        return source;
    }

    public static int GetProcInstIdFromSn(this string sn)
    {
        if (string.IsNullOrEmpty(sn))
        {
            throw new System.ArgumentNullException("sn");
        }

        var parts = sn.Split('_');

        if (parts.Length != 2)
        {
            throw new System.Exception(string.Format("Incorrectly formatted SN '{0}", sn));
        }

        int procInstId = 0;
        if (int.TryParse(parts[0], out procInstId))
        {
            return procInstId;
        }
        throw new System.Exception(string.Format("Incorrectly formatted SN '{0}", sn));
    }

    public static string ToLowerInvariantSafe(this string s)
    {
        if (string.IsNullOrEmpty(s))
        {
            return null;
        }

        return s.ToLowerInvariant();
    }

    //StringToText
    public static string ToWords(this decimal netAmount)
    {
        string bahtTxt, n, bahtTH = "";


        bahtTxt = netAmount.ToString("####.00");
        string[] num = { "ศูนย์", "หนึ่ง", "สอง", "สาม", "สี่", "ห้า", "หก", "เจ็ด", "แปด", "เก้า", "สิบ" };
        string[] rank = { "", "สิบ", "ร้อย", "พัน", "หมื่น", "แสน", "ล้าน" };
        string[] temp = bahtTxt.Split('.');
        string intVal = temp[0];
        string decVal = temp[1];
        string moreThanM= "";
        string lessThanM = "";
        if (Convert.ToDecimal(bahtTxt) == 0)
            bahtTH = "ศูนย์บาทถ้วน";
        else
        {
            if (Convert.ToDecimal(bahtTxt) == 1)
            {
                bahtTH = "หนึ่งบาทถ้วน";
            }
            else
            {
                if (intVal.Length <= 7)
                {
                    for (int i = 0; i < intVal.Length; i++)
                    {
                        n = intVal.Substring(i, 1);
                        if (n != "0")
                        {
                            if ((i == (intVal.Length - 1)) && (n == "1"))
                                bahtTH += "เอ็ด";
                            else if ((i == (intVal.Length - 2)) && (n == "2"))
                                bahtTH += "ยี่";
                            else if ((i == (intVal.Length - 2)) && (n == "1"))
                                bahtTH += "";
                            else
                                bahtTH += num[Convert.ToInt32(n)];
                            bahtTH += rank[(intVal.Length - i) - 1];
                        }
                    }
                    bahtTH += "บาท";
                    if (decVal == "00")
                        bahtTH += "ถ้วน";
                    else
                    {
                        for (int i = 0; i < decVal.Length; i++)
                        {
                            n = decVal.Substring(i, 1);
                            if (n != "0")
                            {
                                if ((i == decVal.Length - 1) && (n == "1"))
                                    bahtTH += "เอ็ด";
                                else if ((i == (decVal.Length - 2)) && (n == "2"))
                                    bahtTH += "ยี่";
                                else if ((i == (decVal.Length - 2)) && (n == "1"))
                                    bahtTH += "";
                                else
                                    bahtTH += num[Convert.ToInt32(n)];
                                bahtTH += rank[(decVal.Length - i) - 1];
                            }
                        }
                        bahtTH += "สตางค์";
                    }
                }
                else
                {
                    if (intVal.Length > 7)
                    {
                        int subString = intVal.Length - 6;
                        moreThanM = intVal.Substring(0, subString);
                        lessThanM = intVal.Substring(subString);
                        for (int i = 0; i < moreThanM.Length; i++)
                        {
                            n = moreThanM.Substring(i, 1);
                            if (n != "0")
                            {
                                if ((i == (moreThanM.Length - 1)) && (n == "1"))
                                    bahtTH += "เอ็ด";
                                else if ((i == (moreThanM.Length - 2)) && (n == "2"))
                                    bahtTH += "ยี่";
                                else if ((i == (moreThanM.Length - 2)) && (n == "1"))
                                    bahtTH += "";
                                else
                                    bahtTH += num[Convert.ToInt32(n)];
                                bahtTH += rank[(moreThanM.Length - i) - 1];
                            }
                        }
                        bahtTH += "ล้าน";
                        for (int i = 0; i < lessThanM.Length; i++)
                        {
                            n = lessThanM.Substring(i, 1);
                            if (n != "0")
                            {
                                if ((i == (lessThanM.Length - 1)) && (n == "1"))
                                    bahtTH += "เอ็ด";
                                else if ((i == (lessThanM.Length - 2)) && (n == "2"))
                                    bahtTH += "ยี่";
                                else if ((i == (lessThanM.Length - 2)) && (n == "1"))
                                    bahtTH += "";
                                else
                                    bahtTH += num[Convert.ToInt32(n)];
                                bahtTH += rank[(lessThanM.Length - i) - 1];
                            }
                        }
                        bahtTH += "บาท";
                        if (decVal == "00")
                            bahtTH += "ถ้วน";
                        else
                        {
                            for (int i = 0; i < decVal.Length; i++)
                            {
                                n = decVal.Substring(i, 1);
                                if (n != "0")
                                {
                                    if ((i == decVal.Length - 1) && (n == "1"))
                                        bahtTH += "เอ็ด";
                                    else if ((i == (decVal.Length - 2)) && (n == "2"))
                                        bahtTH += "ยี่";
                                    else if ((i == (decVal.Length - 2)) && (n == "1"))
                                        bahtTH += "";
                                    else
                                        bahtTH += num[Convert.ToInt32(n)];
                                    bahtTH += rank[(decVal.Length - i) - 1];
                                }
                            }
                            bahtTH += "สตางค์";
                        }
                    }

                
                }




            } 
        }

        return bahtTH;
    }

    public static bool IsTypeDeposit(this TypeProposalViewModel typeProposal)
    {
        if (typeProposal != null && !string.IsNullOrEmpty(typeProposal.TypeProposalCode))
        {
            if (typeProposal.TypeProposalCode.ToUpper() == ProposalType.ProposalTypeCode.Deposit ||
                typeProposal.TypeProposalCode.ToUpper() == ProposalType.ProposalTypeCode.DepositInternal)
            {
                return true;
            }
        }

        return false;
    }
    public static bool IsTypeDeposit(this MPS_M_TypeProposal typeProposal)
    {
        if (typeProposal != null && !string.IsNullOrEmpty(typeProposal.TypeProposalCode))
        {
            if (typeProposal.TypeProposalCode.ToUpper() == ProposalType.ProposalTypeCode.Deposit ||
                typeProposal.TypeProposalCode.ToUpper() == ProposalType.ProposalTypeCode.DepositInternal)
            {
                return true;
            }
        }
        return false;
    }

}