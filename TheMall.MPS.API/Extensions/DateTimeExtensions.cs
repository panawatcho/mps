﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TheMall.MPS.API.Extensions
{
    public static class DateTimeExtensions
    {
        /// <summary>
        /// Gets the 12:00:00 instance of a DateTime
        /// </summary>
        public static DateTime AbsoluteStart(this DateTime dateTime)
        {
            return dateTime.Date;
        }

        /// <summary>
        /// Gets the 11:59:59 instance of a DateTime
        /// </summary>
        public static DateTime AbsoluteEnd(this DateTime dateTime)
        {
            return AbsoluteStart(dateTime).AddDays(1).AddTicks(-1);
        }

        public static DateTime DueDateEnd(this DateTime dateTime)
        {
            return AbsoluteStart(dateTime).AddDays(1).AddSeconds(-1).ToUniversalTime();
        }
    }
}