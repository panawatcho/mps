﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
//using System.IdentityModel;
using System.Linq;

public static class ValidationResultExtensions
{
    public static AggregateException ToAggregateException(this IEnumerable<ValidationResult> validationResults, string message = null)
    {
        if (validationResults == null)
        {
            return new AggregateException();
        }
        if (string.IsNullOrEmpty(message))
        {
            return new AggregateException(validationResults.Select(m => new ValidationException(m.ErrorMessage)));
        }
        return new AggregateException(message, validationResults.Select(m => new ValidationException(m.ErrorMessage)));
    }

    //public static BadRequestException ToBadRequestException(this IEnumerable<ValidationResult> validationResults,
    //    string message = null)
    //{
    //    return new BadRequestException(message, validationResults.ToAggregateException(message));
    //}

    public static IList<ValidationResult> Append(this Exception exception, IList<ValidationResult> validationResults)
    {
        if (validationResults == null)
        {
            validationResults = new List<ValidationResult>();
        }

        validationResults.Add(new ValidationResult(exception.Message));
        return validationResults;
    }

    public static bool IsValid(this ValidationResult validationResult)
    {
        if (validationResult == null || string.IsNullOrEmpty(validationResult.ErrorMessage))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}