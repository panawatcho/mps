﻿public static class IEnumerableExtensions
{
    public static System.Collections.Generic.List<T> ToListSafe<T>(this System.Collections.Generic.IEnumerable<T> source)
    {
        if (source == null)
        {
            return new System.Collections.Generic.List<T>();
        }

        return System.Linq.Enumerable.ToList(source);
    }
    public static System.Collections.Generic.IReadOnlyList<T> ToReadOnlyListSafe<T>(this System.Collections.Generic.IEnumerable<T> source)
    {
        if (source == null)
        {
            return new System.Collections.Generic.List<T>();
        }

        return System.Linq.Enumerable.ToList(source);
    }
}