﻿// ReSharper disable once CheckNamespace
namespace TheMall.MPS.API.Services
{
	using CrossingSoft.Framework.Infrastructure.OData.Repositories;
	using TheMall.MPS.Models;

	public partial interface IExampleMasterTable2Service
	{

	}

	public partial class ExampleMasterTable2Service : Abstracts.BaseService<ExampleMasterTable2>
	{
		private readonly IODataRepositoryAsync<ExampleMasterTable2> _repository;

		public ExampleMasterTable2Service(IODataRepositoryAsync<ExampleMasterTable2> repository) : base(repository)
	    {
			_repository = repository;
	    }
	}
}

// ReSharper disable once CheckNamespace
namespace TheMall.MPS.API.Mappers
{
	using System.Collections.Generic;
	using System.Linq;
	using TheMall.MPS.Models;
	using TheMall.MPS.ViewModels;

	public static class ExampleMasterTable2Mapper
	{
        //public static ExampleMasterTable2 ToModel(this ExampleMasterTable2ViewModel viewModel, ExampleMasterTable2 model = null)
        //{
        //    if (viewModel == null)
        //    {
        //        return null;
        //    }
        //    if (model == null)
        //    {
        //        model = new ExampleMasterTable2();
        //    }
        //    model.Id = viewModel.Id;
        //    model.Name = viewModel.Name;
        //    model.ExampleMasterTableId = viewModel.ExampleMasterTableId;
        //    model.ExampleMasterTable = viewModel.ExampleMasterTable.ToModel();

        //    return model;
            
        //}

        //public static IEnumerable<ExampleMasterTable2> ToModel(this IEnumerable<ExampleMasterTable2ViewModel> viewModels, IReadOnlyList<ExampleMasterTable2> models = null)
        //{
        //    if (viewModels == null)
        //    {
        //        return Enumerable.Empty<ExampleMasterTable2>();
        //    }

        //    var lines = new List<ExampleMasterTable2>();
        //    foreach (var viewModel in viewModels)
        //    {
        //        ExampleMasterTable2 model = null;
        //        if (viewModel.Id > 0)
        //        {
        //            model = lines.FirstOrDefault(m => m.Id == viewModel.Id);
        //            if (model == null)
        //            {
        //                throw Exceptions.Helper.DataWithIDNotExists(viewModel.Id);
        //            }
        //        }
        //        else
        //        {
        //            model = new ExampleMasterTable2();

        //            model.Id = viewModel.Id;
        //            //model.ParentId = viewModel.ParentId;
        //            //model.LineNo = viewModel.LineNo;
        //            //model.StatusFlag = viewModel.StatusFlag;
        //        }

        //        lines.Add(model);
        //    }
        //    return lines;
        //}
	}
}
// ReSharper disable once CheckNamespace
namespace TheMall.MPS.API.Services
{
	using CrossingSoft.Framework.Infrastructure.OData.Repositories;
	using TheMall.MPS.Models;

	public partial interface IExampleMasterTableService
	{

	}

	public partial class ExampleMasterTableService : Abstracts.BaseService<ExampleMasterTable>
	{
		private readonly IODataRepositoryAsync<ExampleMasterTable> _repository;

		public ExampleMasterTableService(IODataRepositoryAsync<ExampleMasterTable> repository) : base(repository)
	    {
			_repository = repository;
	    }
	}
}

// ReSharper disable once CheckNamespace
namespace TheMall.MPS.API.Mappers
{
	using System.Collections.Generic;
	using System.Linq;
	using TheMall.MPS.Models;
	using TheMall.MPS.ViewModels;

	public static class ExampleMasterTableMapper
	{
        //public static ExampleMasterTable ToModel(this ExampleMasterTableViewModel viewModel, ExampleMasterTable model = null)
        //{
        //    if (viewModel == null)
        //    {
        //        return null;
        //    }
        //    if (model == null)
        //    {
        //        model = new ExampleMasterTable();
        //    }
        //    model.Id = viewModel.Id;
        //    model.Name = viewModel.Name;

        //    return model;
            
        //}

        //public static IEnumerable<ExampleMasterTable> ToModel(this IEnumerable<ExampleMasterTableViewModel> viewModels, IReadOnlyList<ExampleMasterTable> models = null)
        //{
        //    if (viewModels == null)
        //    {
        //        return Enumerable.Empty<ExampleMasterTable>();
        //    }

        //    var lines = new List<ExampleMasterTable>();
        //    foreach (var viewModel in viewModels)
        //    {
        //        ExampleMasterTable model = null;
        //        if (string.IsNullOrEmpty(viewModel.Id))
        //        {
        //            model = lines.FirstOrDefault(m => m.Id == viewModel.Id);
        //            if (model == null)
        //            {
        //                throw Exceptions.Helper.DataWithIDNotExists(viewModel.Id);
        //            }
        //        }
        //        else
        //        {
        //            model = new ExampleMasterTable();

        //            model.Id = viewModel.Id;
        //            //model.ParentId = viewModel.ParentId;
        //            //model.LineNo = viewModel.LineNo;
        //            //model.StatusFlag = viewModel.StatusFlag;
        //        }

        //        lines.Add(model);
        //    }
        //    return lines;
        //}
	}
}
