﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheMall.MPS.Resources
{
    public static class Helper
    {
        public static string RequiredError(string fieldName)
        {
            var display = Resources.Share.ResourceManager.GetString(fieldName);
            return string.Format(Resources.Error.Required, string.IsNullOrEmpty(display) ? fieldName : display);
        }

        public static string MustBeNumeric(string fieldName)
        {
            return string.Format(Resources.Error.MustBeANumericEntry, fieldName);
        }

        public static string DataWithIDNotExists(int id)
        {
            return string.Format(Resources.Error.DataWithIDNotExists, id);
        }

        public static string TranslateAction(string action)
        {
            var translatedAction = Resources.Action.ResourceManager.GetString(action, System.Globalization.CultureInfo.InvariantCulture);
            return translatedAction ?? action;
        }
    }
}
