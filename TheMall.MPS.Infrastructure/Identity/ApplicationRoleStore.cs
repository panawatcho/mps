﻿using System.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace TheMall.MPS.Infrastructure.Identity
{
    public class ApplicationRoleStore : RoleStore<ApplicationRole, string, ApplicationUserRole>
    {
        public ApplicationRoleStore()
            : base(new IdentityDbContext())
        {
            base.DisposeContext = true;
        }

        public ApplicationRoleStore(DbContext context) : base(context)
        {
        }

    }
}
