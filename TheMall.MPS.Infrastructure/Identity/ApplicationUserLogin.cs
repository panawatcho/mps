﻿using System.ComponentModel.DataAnnotations.Schema;
using CrossingSoft.Framework.Infrastructure;
using Microsoft.AspNet.Identity.EntityFramework;

namespace TheMall.MPS.Infrastructure.Identity
{
    public class ApplicationUserLogin : IdentityUserLogin, IObjectState
    {
        [NotMapped]
        public ObjectState ObjectState { get; set; }
    }
}
