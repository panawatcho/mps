﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CrossingSoft.Framework.Infrastructure;
using CrossingSoft.Framework.Models.Interfaces;
using TheMall.MPS.Models;
using Microsoft.AspNet.Identity.EntityFramework;

namespace TheMall.MPS.Infrastructure.Identity
{
    public class ApplicationRole : IdentityRole<string, ApplicationUserRole>, ITrackableModel
    {
        //public Role()
        //{
        //    Users = new List<UserRole>();
        //}
        //[Key]
        //[StringLength(128)]
        //public string Id { get; set; }

        //[StringLength(256)]
        //public string Name { get; set; }

        //public virtual ICollection<UserRole> Users { get; set; }

        public bool Inactive { get; set; }

        [NotMapped]
        public ObjectState ObjectState { get; set; }

        [StringLength(Metadata.UsernameLength)]
        public string CreatedBy { get; set; }
        [StringLength(Metadata.UserDisplayNameLength)]
        public string CreatedByName { get; set; }
        public DateTime? CreatedDate { get; set; }
        [StringLength(Metadata.UsernameLength)]
        public string ModifiedBy { get; set; }
        [StringLength(Metadata.UserDisplayNameLength)]
        public string ModifiedByName { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
