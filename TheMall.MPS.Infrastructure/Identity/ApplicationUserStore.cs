﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using TheMall.MPS.Infrastructure.EntityFramework;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace TheMall.MPS.Infrastructure.Identity
{
    public class ApplicationUserStore : UserStore<ApplicationUser, ApplicationRole, string, ApplicationUserLogin, ApplicationUserRole, ApplicationUserClaim>, IUserStore<ApplicationUser>
    {
        private readonly MPSDbContext _context;

        public ApplicationUserStore(MPSDbContext context)
            : base(context)
        {
            _context = context;
        }

        public override async Task<IList<string>> GetRolesAsync(ApplicationUser user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            var id = user.Id;
            var tUserRoles =
                from userRole in _context.Set<ApplicationUserRole>()
                where userRole.UserId.Equals(id)
                && !userRole.Inactive
                select userRole;
            var roles =
                await (from role in _context.Set<ApplicationRole>()
                    where !role.Inactive
                    join userRole in tUserRoles on role.Id equals userRole.RoleId
                    select new { role.Id, role.Name}).ToListAsync();

            var ids = roles.Select(m => m.Id).Distinct().ToList();
            if (ids.All(m => m != "*"))
            {
                ids.Add("*");
            }
            var names = roles.Select(m => m.Name).Distinct().ToList();
            if (ids.All(m => m != "Everyone"))
            {
                names.Add("Everyone");
            }
            foreach (var role in ids)
            {
                user.Claims.Add(new ApplicationUserClaim
                {
                    ClaimType = TheMall.MPS.Infrastructure.Security.Claim.CRS_ROLE_CLAIMTYPE,
                    ClaimValue = role
                });
            }

            return names;
        }  
    }
}
