﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CrossingSoft.Framework.Infrastructure;
using CrossingSoft.Framework.Models.Interfaces;
using TheMall.MPS.Models;
using Microsoft.AspNet.Identity.EntityFramework;

namespace TheMall.MPS.Infrastructure.Identity
{
    public class ApplicationUserRole : IdentityUserRole<string>,  ITrackableModel
    {
        //[Key]
        //[StringLength(128)]
        //public string RoleId { get; set; }

        //[Key]
        //[StringLength(128)]
        //public string UserId { get; set; }

        [ForeignKey("RoleId")]
        public virtual ApplicationRole Role { get; set; }
        [ForeignKey("UserId")]
        public virtual ApplicationUser User { get; set; }
        
        public bool Inactive { get; set; }

        [NotMapped]
        public ObjectState ObjectState { get; set; }
        [StringLength(Metadata.UsernameLength)]
        public string CreatedBy { get; set; }
        [StringLength(Metadata.UserDisplayNameLength)]
        public string CreatedByName { get; set; }
        public DateTime? CreatedDate { get; set; }
        [StringLength(Metadata.UsernameLength)]
        public string ModifiedBy { get; set; }
        [StringLength(Metadata.UserDisplayNameLength)]
        public string ModifiedByName { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
