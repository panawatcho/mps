﻿using System.ComponentModel.DataAnnotations.Schema;
using CrossingSoft.Framework.Infrastructure;
using Microsoft.AspNet.Identity.EntityFramework;

namespace TheMall.MPS.Infrastructure.Identity
{
    public class ApplicationUserClaim : IdentityUserClaim<string>, IObjectState
    {
        [NotMapped]
        public ObjectState ObjectState { get; set; }
    }
}
