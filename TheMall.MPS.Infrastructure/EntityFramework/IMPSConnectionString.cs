﻿namespace TheMall.MPS.Infrastructure.EntityFramework
{
    public interface IMPSConnectionString : IDbConnectionString
    {
    }
    public class MPSConnectionString : IMPSConnectionString
    {
        public string Name { get; set; }
        public MPSConnectionString()
        {

            Name = typeof (TheMall.MPS.Infrastructure.EntityFramework.MPSDbContext).Name;

        }
    }
}
