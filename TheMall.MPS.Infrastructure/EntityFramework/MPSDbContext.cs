﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.ModelConfiguration.Conventions;
using CrossingSoft.Framework.Infrastructure.DataContext;
using TheMall.MPS.Infrastructure.Identity;
using TheMall.MPS.Models.Budget;
using TheMall.MPS.Models.FileManager;
using TheMall.MPS.Models.Procurement.GR;
using TheMall.MPS.Models.Procurement.PO;
using TheMall.MPS.Models.Procurement.PR;
using TheMall.MPS.Models.View;

//using TheMall.MPS.Infrastructure.Migrations;

namespace TheMall.MPS.Infrastructure.EntityFramework
{
    public interface IMPSDbContext : IDataContextAsync
    {
        IDbSet<ApplicationRole> Roles { get; set; }
        IDbSet<Models.ApplicationSetting> ApplicationSettings { get; set; }
        IDbSet<Infrastructure.Security.MenuItem> MenuItems { get; set; }
        IDbSet<Infrastructure.Security.Referer> Referers { get; set; }
        IDbSet<Infrastructure.Security.RefererRolePermission> RefererRolePermissions { get; set; }
        IDbSet<Infrastructure.Security.RefererUserPermission> RefererUserPermissions { get; set; }
        IDbSet<Models.NumberSeq.NumberSeqSetup> NumberSeqSetups { get; set; }
        IDbSet<Models.NumberSeq.NumberSeqTable> NumberSeqTables { get; set; }
        IDbSet<Models.Mail.MailContent> MailContents { get; set; }
        IDbSet<Models.Mail.MailRecipient> MailRecipients { get; set; }
        IDbSet<Models.Mail.MailSetup> MailSetups { get; set; }
        IDbSet<Models.Mail.MailTemplate> MailTemplates { get; set; }
        IDbSet<Models.Process> Processes { get; set; }
        IDbSet<Models.DocumentType> DocumentTypes { get; set; }
        DbSet<T> Set<T>() where T : class;

        //IDbSet<Models.FileManager.FileManagerDirectory> FileManagerDirectories { get; set; }
        //IDbSet<Models.FileManager.FileManagerDocument> FileManagerDocuments { get; set; }
        //IDbSet<Models.FileManager.FileManagerFile> FileManagerFiles { get; set; }

        //IDbSet<Models.Example.ExampleTable> ExampleTables { get; set; }
        //IDbSet<Models.Example.ExampleAttachment> ExampleAttachments { get; set; }
        //IDbSet<Models.Example.ExampleLine> ExampleLines { get; set; }
        //IDbSet<Models.Example.ExampleLineAttachment> ExampleLineAttachments { get; set; } 

        //master
        IDbSet<Models.Master.MPS_M_UnitCode> MPS_M_UnitCodes { get; set; }
        IDbSet<Models.Master.MPS_M_TypeProposal> MPS_M_TypeProposals { get; set; }
        IDbSet<Models.Master.MPS_M_Place> MPS_M_Places { get; set; }
        //IDbSet<Models.Master.MPS_M_AllocateBasis> MPS_M_AllocateBasiss { get; set; }
        IDbSet<Models.Master.MPS_M_ExpenseTopic> MPS_M_ExpenseTopics { get; set; }
        IDbSet<Models.Master.MPS_M_APCode> MPS_M_APcodes { get; set; }
        IDbSet<Models.Master.MPS_M_TypeMemo> MPS_M_TypeMemos { get; set; }
        IDbSet<Models.Master.MPS_M_BudgetYearPlan> MPS_M_BudgetYearPlans { get; set; }
        // IDbSet<Models.Master.MPS_M_SharedPercent> MPS_M_SharedPercents { get; set; }
        // IDbSet<Models.Master.MPS_M_TemplateAllocateBasis> MPS_M_TemplateAllocateBasiss { get; set; }
        IDbSet<Models.Master.MPS_M_SharedBranch> MPS_M_SharedBranchs { get; set; }
        IDbSet<Models.Master.MPS_M_Configulation> MPS_M_Configulations { get; set; }
        IDbSet<Models.Master.MPS_M_UnitCodeForEmployee> MPS_M_UnitCodeForEmployees { get; set; }
        IDbSet<Models.Master.MPS_M_FinalApprover> MPS_M_FinalApprovers { get; set; }
        IDbSet<Models.Master.MPS_M_ProcessApprove> MPS_M_ProcessApprove { get; set; }
        IDbSet<Models.Master.MPS_M_MailGroup> MPS_M_MailGroups { get; set; }
        IDbSet<Models.Master.MPS_M_Accounting> MPS_M_Accountings { get; set; }
        IDbSet<Models.Master.MPS_M_Themes> MPS_M_Themes { get; set; }
        IDbSet<Models.Master.MPS_M_Company> MPS_M_Company { get; set; }

        //Workflow
        IDbSet<Models.Proposal.MPS_ProposalTable> MPS_ProposalTables { get; set; }
        IDbSet<Models.Proposal.MPS_ProposalLine> MPS_ProposalLines { get; set; }
        IDbSet<Models.Proposal.MPS_ProposalLineTemplate> MPS_ProposalLineAPs { get; set; }
        IDbSet<Models.Proposal.MPS_ProposalComment> MPS_ProposalComments { get; set; }
        IDbSet<Models.Proposal.MPS_ProposalAttachment> MPS_ProposalAttachments { get; set; }
        IDbSet<Models.Proposal.MPS_IncomeDeposit> MPS_IncomeDeposits { get; set; }
        IDbSet<Models.Proposal.MPS_IncomeOther> MPS_IncomeOthers { get; set; }
        IDbSet<Models.Proposal.MPS_IncomeTotalSale> MPS_IncomeTotalSales { get; set; }
        IDbSet<Models.Proposal.MPS_EstimateTotalSale> MPS_EstimateTotalSales { get; set; }
        IDbSet<Models.Proposal.MPS_EstimateTotalSaleTemplate> MPS_EstimateTotalSaleTemplates { get; set; }
        IDbSet<Models.Proposal.MPS_IncomeTotalSaleTemplate> MPS_IncomeTotalSaleTemplates { get; set; }
        IDbSet<Models.Proposal.MPS_ProposalApproval> MPS_ProposalApprovals { get; set; }
        IDbSet<Models.Proposal.MPS_ProposalLineSharedTemplate> MPS_ProposalLineSharedTemplates { get; set; }
        IDbSet<Models.Proposal.MPS_AccountTrans> MPS_AccountTrans { get; set; }
        IDbSet<Models.Proposal.MPS_ProposalBudgetPlan> MPS_ProposalBudgetPlans { get; set; }
        IDbSet<Models.Proposal.MPS_DepositLine> MPS_DepositLines { get; set; }
        IDbSet<Models.Proposal.MPS_DepositAccountTrans> MPS_DepositAccountTrans { get; set; }

        //memo
        IDbSet<Models.Memo.MPS_MemoTable> MPS_MemoTables { get; set; }
        IDbSet<Models.Memo.MPS_MemoLine> MPS_MemoLines { get; set; }
        IDbSet<Models.Memo.MPS_MemoComment> MPS_MemoComments { get; set; }
        IDbSet<Models.Memo.MPS_MemoAttachment> MPS_MemoAttachments { get; set; }
        IDbSet<Models.Memo.MPS_MemoApproval> MPS_MemoApprovals { get; set; }
        IDbSet<Models.PettyCash.MPS_PettyCashTable> MPS_PettyCashTables { get; set; }
        IDbSet<Models.PettyCash.MPS_PettyCashLine> MPS_PettyCashLines { get; set; }
        IDbSet<Models.PettyCash.MPS_PettyCashComment> MPS_PettyCashComments { get; set; }
        IDbSet<Models.PettyCash.MPS_PettyCashAttachment> MPS_PettyCashAttachments { get; set; }
        IDbSet<Models.PettyCash.MPS_PettyCashApproval> MPS_PettyCashApprovals { get; set; }
        IDbSet<Models.CashAdvance.MPS_CashAdvanceTable> MPS_CashAdvanceTables { get; set; }
        IDbSet<Models.CashAdvance.MPS_CashAdvanceLine> MPS_CashAdvanceLines { get; set; }
        IDbSet<Models.CashAdvance.MPS_CashAdvanceComment> MPS_CashAdvanceComments { get; set; }
        IDbSet<Models.CashAdvance.MPS_CashAdvanceAttachment> MPS_CashAdvanceAttachments { get; set; }
        IDbSet<Models.CashAdvance.MPS_CashAdvanceApproval> MPS_CashAdvanceApprovals { get; set; }
        IDbSet<Models.CashClearing.MPS_CashClearingTable> MPS_CashClearingTables { get; set; }
        IDbSet<Models.CashClearing.MPS_CashClearingLine> MPS_CashClearingLines { get; set; }
        IDbSet<Models.CashClearing.MPS_CashClearingComment> MPS_CashClearingComments { get; set; }
        IDbSet<Models.CashClearing.MPS_CashClearingAttachment> MPS_CashClearingAttachments { get; set; }
        IDbSet<Models.CashClearing.MPS_CashClearingApproval> MPS_CashClearingApprovals { get; set; }

        //Receipt
        IDbSet<Models.Receipt.MPS_ReceiptTable> MPS_ReceiptTable { get; set; }
        IDbSet<Models.Receipt.MPS_ReceiptLine> MPS_ReceiptLine { get; set; }

        //Memo Income
        IDbSet<Models.MemoIncome.MPS_MemoIncomeTable> MPS_MemoIncomeTable { get; set; }
        IDbSet<Models.MemoIncome.MPS_MemoIncomeLine> MPS_MemoIncomeLine { get; set; }
        IDbSet<Models.MemoIncome.MPS_MemoIncomeComment> MPS_MemoIncomeComment { get; set; }
        IDbSet<Models.MemoIncome.MPS_MemoIncomeAttachment> MPS_MemoIncomeAttachment { get; set; }
        IDbSet<Models.MemoIncome.MPS_MemoIncomeApproval> MPS_MemoIncomeApproval { get; set; }
        IDbSet<Models.MemoIncome.MPS_MemoIncomeInvoice> MPS_MemoIncomeInvoice { get; set; }

        //other
        IDbSet<Models.MPS_M_EmployeeTable> MPS_M_EmployeeTables { get; set; }

        //Budget
        IDbSet<Models.Budget.MPS_BudgetYearTrans> MPS_BudgetYearTrans { get; set; }
        IDbSet<Models.Budget.MPS_BudgetProposalTrans> MPS_BudgetProposalTrans { get; set; }
        IDbSet<Models.Budget.MPS_BudgetDepositTrans> MPS_BudgetDepositTrans { get; set; }
        IDbSet<Models.MPS_M_TotalSaleLastYear> MPS_M_TotalSaleLastYears { get; set; }
       
        IDbSet<Models.Procurement.MPS_ProcurementTable> MPS_ProcurementTables { get; set; }
        IDbSet<Models.Procurement.PR.MPS_PurchReqTable> MPS_PurchReqTables { get; set; }
        IDbSet<Models.Procurement.PR.MPS_PurchReqLine> MPS_PurchReqLines { get; set; }
        IDbSet<Models.Procurement.PO.MPS_PurchOrderTable> MPS_PurchOrderTables { get; set; }
        IDbSet<Models.Procurement.PO.MPS_PurchOrderLine> MPS_PurchOrderLines { get; set; }
        IDbSet<Models.Procurement.GR.MPS_GoodsReceiptTable> MPS_GoodsReceiptTables { get; set; }
        IDbSet<Models.Procurement.GR.MPS_GoodsReceiptLine> MPS_GoodsReceiptLines { get; set; }

        IDbSet<Models.Master.MPS_M_AllocationBasis> MPS_M_AllocationBasis { get; set; }

        //View
        IDbSet<Models.View.MPS_VW_BudgetTracking> MPS_VW_BudgetTracking { get; set; }
        IDbSet<Models.View.MPS_VW_Email> MPS_VW_Email { get; set; }
        IDbSet<Models.View.MPS_VW_ProposalTracking> MPS_VW_ProposalTracking { get; set; }
        IDbSet<Models.View.MPS_VW_ProposalUnitCode> MPS_VW_ProposalUnitCode { get; set; }
        IDbSet<Models.View.MPS_VW_DepositTracking> MPS_VW_DepositTracking { get; set; }
        IDbSet<Models.View.MPS_VW_BudgetYearTracking> MPS_VW_BudgetYearTracking { get; set; }
        IDbSet<Models.View.MPS_VW_DocumentRelated> MPS_VW_DocumentRelated { get; set; }
        IDbSet<Models.View.MPS_VW_MProcurement> MPS_VW_MProcurement { get; set; }

        // Proposal Log
        IDbSet<Models.ProposalLog.MPS_Log_ProposalTable> MPS_Log_ProposalTables { get; set; }
        IDbSet<Models.ProposalLog.MPS_Log_ProposalLine> MPS_Log_ProposalLines { get; set; }
        IDbSet<Models.ProposalLog.MPS_Log_ProposalLineTemplate> MPS_Log_ProposalLineTemplates { get; set; }
        IDbSet<Models.ProposalLog.MPS_Log_IncomeDeposit> MPS_Log_IncomeDeposits { get; set; }
        IDbSet<Models.ProposalLog.MPS_Log_IncomeOther> MPS_Log_IncomeOthers { get; set; }
        IDbSet<Models.ProposalLog.MPS_Log_IncomeTotalSale> MPS_Log_IncomeTotalSales { get; set; }
        IDbSet<Models.ProposalLog.MPS_Log_IncomeTotalSaleTemplate> MPS_Log_IncomeTotalSaleTemplates { get; set; }
        IDbSet<Models.ProposalLog.MPS_Log_ProposalApproval> MPS_Log_ProposalApprovals { get; set; }
        IDbSet<Models.ProposalLog.MPS_Log_ProposalLineSharedTemplate> MPS_Log_ProposalLineSharedTemplates { get; set; }
        IDbSet<Models.ProposalLog.MPS_Log_AccountTrans> MPS_Log_AccountTrans { get; set; }
        IDbSet<Models.ProposalLog.MPS_Log_DepositLine> MPS_Log_DepositLines { get; set; }
        IDbSet<Models.ProposalLog.MPS_Log_ProposalExpenseTopic> MPS_Log_ProposalExpenseTopics { get; set; }
        IDbSet<Models.ProposalLog.MPS_Log_ProposalTrackChanges> MPS_Log_ProposalTrackChanges { get; set; } 
    }

    public class MPSDbContext : CrossingSoft.Framework.Infrastructure.EF6.Identity.DataWithIdentityContext<ApplicationUser, ApplicationRole, string, ApplicationUserLogin, ApplicationUserRole, ApplicationUserClaim>, IMPSDbContext
    {
        public MPSDbContext()
            : base(new MPSConnectionString().Name)
        {
           // Database.SetInitializer(new MPSDbGenerator());
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<MPSDbContext, Migrations.Configuration>());
            this.Configuration.LazyLoadingEnabled = true;
            this.Configuration.ProxyCreationEnabled = true;
            ((IObjectContextAdapter)this).ObjectContext.ObjectMaterialized +=
                (sender, e) => Models.Attributes.DateTimeKindAttribute.Apply(e.Entity);
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Entity<MPS_BudgetProposalTrans>().Property(m => m.Amount).HasPrecision(18, 4);
            modelBuilder.Entity<MPS_PurchReqLine>().Property(m => m.ACTUALPRICE).HasPrecision(18, 4);
            modelBuilder.Entity<MPS_PurchReqLine>().Property(m => m.PRICE).HasPrecision(18, 4);
            modelBuilder.Entity<MPS_PurchReqLine>().Property(m => m.QUANTITY).HasPrecision(18, 4);
            modelBuilder.Entity<MPS_PurchReqLine>().Property(m => m.TAX_AMOUNT).HasPrecision(18, 4);

            modelBuilder.Entity<MPS_PurchOrderLine>().Property(m => m.ACTUALPRICE).HasPrecision(18, 4);
            modelBuilder.Entity<MPS_PurchOrderLine>().Property(m => m.PRICE).HasPrecision(18, 4);
            modelBuilder.Entity<MPS_PurchOrderLine>().Property(m => m.QUANTITY).HasPrecision(18, 4);
            modelBuilder.Entity<MPS_PurchOrderLine>().Property(m => m.TAX_AMOUNT).HasPrecision(18, 4);

            modelBuilder.Entity<MPS_GoodsReceiptLine>().Property(m => m.ACTUALPRICE).HasPrecision(18, 4);
            modelBuilder.Entity<MPS_GoodsReceiptLine>().Property(m => m.QUANTITY).HasPrecision(18, 4);

            modelBuilder.Configurations.Add(new BudgetTrackingViewConfiguration());
            modelBuilder.Configurations.Add(new EmailViewConfiguration());
            modelBuilder.Configurations.Add(new ProposalTrackingViewConfiguration());
            modelBuilder.Configurations.Add(new ProposalUnitCodeViewConfiguration());

            base.OnModelCreating(modelBuilder);
        }

        public static MPSDbContext Create()
        {
            return new MPSDbContext();
        }

        public override IDbSet<ApplicationUser> Users { get; set; }

        public virtual IDbSet<Models.ApplicationSetting> ApplicationSettings { get; set; }
        public virtual IDbSet<Infrastructure.Security.MenuItem> MenuItems { get; set; }
        public virtual IDbSet<Infrastructure.Security.Referer> Referers { get; set; }
        public virtual IDbSet<Infrastructure.Security.RefererRolePermission> RefererRolePermissions { get; set; }
        public virtual IDbSet<Infrastructure.Security.RefererUserPermission> RefererUserPermissions { get; set; }
        public virtual IDbSet<Models.NumberSeq.NumberSeqSetup> NumberSeqSetups { get; set; }
        public virtual IDbSet<Models.NumberSeq.NumberSeqTable> NumberSeqTables { get; set; }
        public virtual IDbSet<Models.Mail.MailContent> MailContents { get; set; }
        public virtual IDbSet<Models.Mail.MailRecipient> MailRecipients { get; set; }
        public virtual IDbSet<Models.Mail.MailSetup> MailSetups { get; set; }
        public virtual IDbSet<Models.Mail.MailTemplate> MailTemplates { get; set; }
        public virtual IDbSet<Models.Process> Processes { get; set; }
        public virtual IDbSet<Models.DocumentType> DocumentTypes { get; set; }
        //public virtual IDbSet<FileManagerDirectory> FileManagerDirectories { get; set; }
        //public virtual IDbSet<FileManagerDocument> FileManagerDocuments { get; set; }
        //public virtual IDbSet<FileManagerFile> FileManagerFiles { get; set; }


        //TODO: DELETE ME
        //public virtual IDbSet<Models.Example.ExampleTable> ExampleTables { get; set; }
        //public virtual IDbSet<Models.Example.ExampleAttachment> ExampleAttachments { get; set; }
        //public virtual IDbSet<Models.Example.ExampleLine> ExampleLines { get; set; }
        //public virtual IDbSet<Models.Example.ExampleLineAttachment> ExampleLineAttachments { get; set; } 


        //master
        public virtual IDbSet<Models.Master.MPS_M_UnitCode> MPS_M_UnitCodes { get; set; }
        public virtual IDbSet<Models.Master.MPS_M_TypeProposal> MPS_M_TypeProposals { get; set; }
        public virtual IDbSet<Models.Master.MPS_M_Place> MPS_M_Places { get; set; }
        // public virtual IDbSet<Models.Master.MPS_M_AllocateBasis> MPS_M_AllocateBasiss { get; set; }
        // public virtual IDbSet<Models.Master.MPS_M_SharedPercent> MPS_M_SharedPercents { get; set; }
        public virtual IDbSet<Models.Master.MPS_M_ExpenseTopic> MPS_M_ExpenseTopics { get; set; }
        public virtual IDbSet<Models.Master.MPS_M_APCode> MPS_M_APcodes { get; set; }
        public virtual IDbSet<Models.Master.MPS_M_TypeMemo> MPS_M_TypeMemos { get; set; }
        public virtual IDbSet<Models.Master.MPS_M_BudgetYearPlan> MPS_M_BudgetYearPlans { get; set; }
        // public virtual IDbSet<Models.Master.MPS_M_TemplateAllocateBasis> MPS_M_TemplateAllocateBasiss { get; set; }
        public virtual IDbSet<Models.Master.MPS_M_SharedBranch> MPS_M_SharedBranchs { get; set; }
        public virtual IDbSet<Models.Master.MPS_M_Configulation> MPS_M_Configulations { get; set; }
        public virtual IDbSet<Models.Master.MPS_M_UnitCodeForEmployee> MPS_M_UnitCodeForEmployees { get; set; }
        public virtual IDbSet<Models.Master.MPS_M_FinalApprover> MPS_M_FinalApprovers { get; set; }
        public virtual IDbSet<Models.Master.MPS_M_ProcessApprove> MPS_M_ProcessApprove { get; set; }
        public virtual IDbSet<Models.Master.MPS_M_MailGroup> MPS_M_MailGroups { get; set; }
        public virtual IDbSet<Models.Master.MPS_M_Accounting> MPS_M_Accountings { get; set; }
        public virtual IDbSet<Models.Master.MPS_M_Themes> MPS_M_Themes { get; set; }
        public virtual IDbSet<Models.Master.MPS_M_Company> MPS_M_Company { get; set; }

        //Workflow
        public virtual IDbSet<Models.Proposal.MPS_ProposalTable> MPS_ProposalTables { get; set; }
        public virtual IDbSet<Models.Proposal.MPS_ProposalLine> MPS_ProposalLines { get; set; }
        public virtual IDbSet<Models.Proposal.MPS_ProposalLineTemplate> MPS_ProposalLineAPs { get; set; }
        public virtual IDbSet<Models.Proposal.MPS_DepositLine> MPS_DepositLines { get; set; }
        public virtual IDbSet<Models.Proposal.MPS_ProposalComment> MPS_ProposalComments { get; set; }
        public virtual IDbSet<Models.Proposal.MPS_ProposalAttachment> MPS_ProposalAttachments { get; set; }
        public virtual IDbSet<Models.Proposal.MPS_IncomeDeposit> MPS_IncomeDeposits { get; set; }
        public virtual IDbSet<Models.Proposal.MPS_IncomeOther> MPS_IncomeOthers { get; set; }
        public virtual IDbSet<Models.Proposal.MPS_IncomeTotalSale> MPS_IncomeTotalSales { get; set; }
        public virtual IDbSet<Models.Proposal.MPS_EstimateTotalSale> MPS_EstimateTotalSales { get; set; }
        public virtual IDbSet<Models.Proposal.MPS_EstimateTotalSaleTemplate> MPS_EstimateTotalSaleTemplates { get; set; }
        public virtual IDbSet<Models.Proposal.MPS_IncomeTotalSaleTemplate> MPS_IncomeTotalSaleTemplates { get; set; }
        public virtual IDbSet<Models.Proposal.MPS_ProposalApproval> MPS_ProposalApprovals { get; set; }
        public virtual IDbSet<Models.Proposal.MPS_ProposalLineSharedTemplate> MPS_ProposalLineSharedTemplates { get; set; }
        public virtual IDbSet<Models.Proposal.MPS_AccountTrans> MPS_AccountTrans { get; set; }
        public virtual IDbSet<Models.Proposal.MPS_ProposalBudgetPlan> MPS_ProposalBudgetPlans { get; set; }
        public virtual IDbSet<Models.Proposal.MPS_DepositAccountTrans> MPS_DepositAccountTrans { get; set; }

        //memo
        public virtual IDbSet<Models.Memo.MPS_MemoTable> MPS_MemoTables { get; set; }
        public virtual IDbSet<Models.Memo.MPS_MemoLine> MPS_MemoLines { get; set; }
        public virtual IDbSet<Models.Memo.MPS_MemoComment> MPS_MemoComments { get; set; }
        public virtual IDbSet<Models.Memo.MPS_MemoAttachment> MPS_MemoAttachments { get; set; }
        public virtual IDbSet<Models.Memo.MPS_MemoApproval> MPS_MemoApprovals { get; set; }

        public virtual IDbSet<Models.PettyCash.MPS_PettyCashTable> MPS_PettyCashTables { get; set; }
        public virtual IDbSet<Models.PettyCash.MPS_PettyCashLine> MPS_PettyCashLines { get; set; }
        public virtual IDbSet<Models.PettyCash.MPS_PettyCashComment> MPS_PettyCashComments { get; set; }
        public virtual IDbSet<Models.PettyCash.MPS_PettyCashAttachment> MPS_PettyCashAttachments { get; set; }
        public virtual IDbSet<Models.PettyCash.MPS_PettyCashApproval> MPS_PettyCashApprovals { get; set; }

        public virtual IDbSet<Models.CashAdvance.MPS_CashAdvanceTable> MPS_CashAdvanceTables { get; set; }
        public virtual IDbSet<Models.CashAdvance.MPS_CashAdvanceLine> MPS_CashAdvanceLines { get; set; }
        public virtual IDbSet<Models.CashAdvance.MPS_CashAdvanceComment> MPS_CashAdvanceComments { get; set; }
        public virtual IDbSet<Models.CashAdvance.MPS_CashAdvanceAttachment> MPS_CashAdvanceAttachments { get; set; }
        public virtual IDbSet<Models.CashAdvance.MPS_CashAdvanceApproval> MPS_CashAdvanceApprovals { get; set; }

        public virtual IDbSet<Models.CashClearing.MPS_CashClearingTable> MPS_CashClearingTables { get; set; }
        public virtual IDbSet<Models.CashClearing.MPS_CashClearingLine> MPS_CashClearingLines { get; set; }
        public virtual IDbSet<Models.CashClearing.MPS_CashClearingComment> MPS_CashClearingComments { get; set; }
        public virtual IDbSet<Models.CashClearing.MPS_CashClearingAttachment> MPS_CashClearingAttachments { get; set; }
        public virtual IDbSet<Models.CashClearing.MPS_CashClearingApproval> MPS_CashClearingApprovals { get; set; }

        public virtual IDbSet<Models.MPS_M_EmployeeTable> MPS_M_EmployeeTables { get; set; }

        public virtual IDbSet<Models.Budget.MPS_BudgetYearTrans> MPS_BudgetYearTrans { get; set; }
        public virtual IDbSet<Models.Budget.MPS_BudgetProposalTrans> MPS_BudgetProposalTrans { get; set; }
        public virtual IDbSet<Models.Budget.MPS_BudgetDepositTrans> MPS_BudgetDepositTrans { get; set; }

        public virtual IDbSet<Models.MPS_M_TotalSaleLastYear> MPS_M_TotalSaleLastYears { get; set; }
     
        public virtual IDbSet<Models.Procurement.MPS_ProcurementTable> MPS_ProcurementTables { get; set; }
        public virtual IDbSet<Models.Procurement.PR.MPS_PurchReqTable> MPS_PurchReqTables { get; set; }
        public virtual IDbSet<Models.Procurement.PR.MPS_PurchReqLine> MPS_PurchReqLines { get; set; }
        public virtual IDbSet<Models.Procurement.PO.MPS_PurchOrderTable> MPS_PurchOrderTables { get; set; }
        public virtual IDbSet<Models.Procurement.PO.MPS_PurchOrderLine> MPS_PurchOrderLines { get; set; }
        public virtual IDbSet<Models.Procurement.GR.MPS_GoodsReceiptTable> MPS_GoodsReceiptTables { get; set; }
        public virtual IDbSet<Models.Procurement.GR.MPS_GoodsReceiptLine> MPS_GoodsReceiptLines { get; set; }

        public virtual IDbSet<Models.Master.MPS_M_AllocationBasis> MPS_M_AllocationBasis { get; set; }

        //Receipt
        public virtual IDbSet<Models.Receipt.MPS_ReceiptTable> MPS_ReceiptTable { get; set; }
        public virtual IDbSet<Models.Receipt.MPS_ReceiptLine> MPS_ReceiptLine { get; set; }

        //Memo Income
        public virtual IDbSet<Models.MemoIncome.MPS_MemoIncomeTable> MPS_MemoIncomeTable { get; set; }
        public virtual IDbSet<Models.MemoIncome.MPS_MemoIncomeLine> MPS_MemoIncomeLine { get; set; }
        public virtual IDbSet<Models.MemoIncome.MPS_MemoIncomeComment> MPS_MemoIncomeComment { get; set; }
        public virtual IDbSet<Models.MemoIncome.MPS_MemoIncomeAttachment> MPS_MemoIncomeAttachment { get; set; }
        public virtual IDbSet<Models.MemoIncome.MPS_MemoIncomeApproval> MPS_MemoIncomeApproval { get; set; }
        public virtual IDbSet<Models.MemoIncome.MPS_MemoIncomeInvoice> MPS_MemoIncomeInvoice { get; set; }
        //View
        public virtual IDbSet<Models.View.MPS_VW_BudgetTracking> MPS_VW_BudgetTracking { get; set; }
        public virtual IDbSet<Models.View.MPS_VW_Email> MPS_VW_Email { get; set; }
        public virtual  IDbSet<Models.View.MPS_VW_ProposalTracking> MPS_VW_ProposalTracking { get; set; }
        public virtual IDbSet<Models.View.MPS_VW_ProposalUnitCode> MPS_VW_ProposalUnitCode { get; set; }
        public virtual IDbSet<Models.View.MPS_VW_DepositTracking> MPS_VW_DepositTracking { get; set; }
        public virtual IDbSet<Models.View.MPS_VW_BudgetYearTracking> MPS_VW_BudgetYearTracking { get; set; }
        public virtual IDbSet<Models.View.MPS_VW_DocumentRelated> MPS_VW_DocumentRelated { get; set; }
        public virtual IDbSet<Models.View.MPS_VW_MProcurement> MPS_VW_MProcurement { get; set; }

        //Proposal Log
        public virtual IDbSet<Models.ProposalLog.MPS_Log_ProposalTable> MPS_Log_ProposalTables { get; set; }
        public virtual IDbSet<Models.ProposalLog.MPS_Log_ProposalLine> MPS_Log_ProposalLines { get; set; }
        public virtual IDbSet<Models.ProposalLog.MPS_Log_ProposalLineTemplate> MPS_Log_ProposalLineTemplates { get; set; }
        public virtual IDbSet<Models.ProposalLog.MPS_Log_IncomeDeposit> MPS_Log_IncomeDeposits { get; set; }
        public virtual IDbSet<Models.ProposalLog.MPS_Log_IncomeOther> MPS_Log_IncomeOthers { get; set; }
        public virtual IDbSet<Models.ProposalLog.MPS_Log_IncomeTotalSale> MPS_Log_IncomeTotalSales { get; set; }
        public virtual IDbSet<Models.ProposalLog.MPS_Log_IncomeTotalSaleTemplate> MPS_Log_IncomeTotalSaleTemplates { get; set; }
        public virtual IDbSet<Models.ProposalLog.MPS_Log_ProposalApproval> MPS_Log_ProposalApprovals { get; set; }
        public virtual IDbSet<Models.ProposalLog.MPS_Log_ProposalLineSharedTemplate> MPS_Log_ProposalLineSharedTemplates { get; set; }
        public virtual IDbSet<Models.ProposalLog.MPS_Log_AccountTrans> MPS_Log_AccountTrans { get; set; }
        public virtual IDbSet<Models.ProposalLog.MPS_Log_DepositLine> MPS_Log_DepositLines { get; set; }
        public virtual IDbSet<Models.ProposalLog.MPS_Log_ProposalExpenseTopic> MPS_Log_ProposalExpenseTopics { get; set; }
        public virtual IDbSet<Models.ProposalLog.MPS_Log_ProposalTrackChanges> MPS_Log_ProposalTrackChanges { get; set; } 
   
    }

    public class BudgetTrackingViewConfiguration : EntityTypeConfiguration<MPS_VW_BudgetTracking>
    {
        public BudgetTrackingViewConfiguration()
        {
            this.HasKey(t => t.RowNo);
            this.ToTable("MPS_VW_BudgetTracking");
        }
    }

    public class EmailViewConfiguration : EntityTypeConfiguration<MPS_VW_Email>
    {
        public EmailViewConfiguration()
        {
            this.HasKey(t => t.RowNo);
            this.ToTable("MPS_VW_Email");
        }
    }
    public class ProposalTrackingViewConfiguration : EntityTypeConfiguration<MPS_VW_ProposalTracking>
    {
        public ProposalTrackingViewConfiguration()
        {
            this.HasKey(t => t.RowNo);
            this.ToTable("MPS_VW_ProposalTracking");
        }
    }

    public class ProposalUnitCodeViewConfiguration : EntityTypeConfiguration<MPS_VW_ProposalUnitCode>
    {
        public ProposalUnitCodeViewConfiguration()
        {
            this.HasKey(t => t.RowNo);
            this.ToTable("MPS_VW_ProposalUnitCode");
        }
    }

    public class DepositTrackingViewConfiguration : EntityTypeConfiguration<MPS_VW_DepositTracking>
    {
        public DepositTrackingViewConfiguration()
        {
            this.HasKey(t => t.RowNo);
            this.ToTable("MPS_VW_DepositTracking");
        }
    }
    public class BudgetYearTrackingViewConfiguration : EntityTypeConfiguration<MPS_VW_BudgetYearTracking>
    {
        public BudgetYearTrackingViewConfiguration()
        {
            this.HasKey(t => t.RowNo);
            this.ToTable("MPS_VW_BudgetYearTracking");
        }
    }
    public class DocumentRelatedViewConfiguration : EntityTypeConfiguration<MPS_VW_DocumentRelated>
    {
        public DocumentRelatedViewConfiguration()
        {
            this.HasKey(t => t.RowNo);
            this.ToTable("MPS_VW_DocumentRelated");
        }
    }
    public class MProcurementViewConfiguration : EntityTypeConfiguration<MPS_VW_MProcurement>
    {
        public MProcurementViewConfiguration()
        {
            this.HasKey(t => t.RowNo);
            this.ToTable("MPS_VW_MProcurement");
        }
    }
}
