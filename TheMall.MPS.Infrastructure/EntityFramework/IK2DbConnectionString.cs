﻿namespace TheMall.MPS.Infrastructure.EntityFramework
{
    public interface IK2DbConnectionString
    {
        string Name { get; set; }
    }

    public class K2DbConnectionString : IK2DbConnectionString
    {
        public string Name { get; set; }
        public K2DbConnectionString()
        {
//#if DEV && DEBUG
            //const string metaData =
            //    "res://*/EntityFramework.K2.csdl|res://*/EntityFramework.K2.ssdl|res://*/EntityFramework.K2.msl";
            //const string dataSource = "172.16.2.27";
            //const string initialCatalog = "K2";
            //const string appName = "EntityFramework";
            //const string providerName = "System.Data.SqlClient";

            //SqlConnectionStringBuilder sqlBuilder = new SqlConnectionStringBuilder();
            //sqlBuilder.DataSource = dataSource;
            //sqlBuilder.InitialCatalog = initialCatalog;
            //sqlBuilder.MultipleActiveResultSets = true;
            //sqlBuilder.IntegratedSecurity = false;
            //sqlBuilder.UserID = "sa";
            //sqlBuilder.Password = "xxx";
            //sqlBuilder.ApplicationName = appName;

            //EntityConnectionStringBuilder efBuilder = new EntityConnectionStringBuilder();
            //efBuilder.Metadata = metaData;
            //efBuilder.Provider = providerName;
            //efBuilder.ProviderConnectionString = sqlBuilder.ConnectionString;

            //Name = efBuilder.ConnectionString;
//#else
            Name = "name=K2DbContext";

//#endif
        }
    }
}
