﻿namespace TheMall.MPS.Infrastructure.EntityFramework
{
    public interface IDbConnectionString
    {
        string Name { get; set; }
    }
}
