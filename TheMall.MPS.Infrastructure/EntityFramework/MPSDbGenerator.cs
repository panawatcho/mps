﻿using System;
using System.Data.Entity;
using System.Linq;
using CrossingSoft.Framework.Infrastructure;
using TheMall.MPS.Infrastructure.EntityFramework.Seeding;

namespace TheMall.MPS.Infrastructure.EntityFramework
{
    public class MPSDbGenerator : DropCreateDatabaseIfModelChanges<MPSDbContext>
    {
        protected override void Seed(MPSDbContext context)
        {
            base.Seed(context);

            var assembly = System.Reflection.Assembly.GetExecutingAssembly();
            var seedings =
                from type in assembly.GetExportedTypes()
                where type.GetInterfaces().Any(m => m == typeof(ISeeding))
                && !type.IsInterface
                select type ;
            foreach (var seeding in seedings)
            {
                ((ISeeding)Activator.CreateInstance(seeding)).Seed(context);
            }
        }
    }

    internal static class DbSetExtension

    {
        public static void AddOrUpdate<TEntity>(this IDbSet<TEntity> set, params TEntity[] entities) where TEntity : class, IObjectState
        {
            foreach (var entity in entities)
            {
                entity.ObjectState = ObjectState.Added;
            }
            System.Data.Entity.Migrations.DbSetMigrationsExtensions.AddOrUpdate(set, entities);
        }
    }
}
