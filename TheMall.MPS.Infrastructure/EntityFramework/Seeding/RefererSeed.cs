﻿using TheMall.MPS.Infrastructure.Security;

namespace TheMall.MPS.Infrastructure.EntityFramework.Seeding
{
    public class RefererSeed : ISeeding
    {
        public void Seed(IMPSDbContext context)
        {
            context.Referers.AddOrUpdate(
                new Referer
                {
                    Id = "",
                    Description = "Empty",
                },
                
                new Referer
                {
                    Id = "home/dashboard/",
                    Description = "Dashboard",
                },
                   new Referer
                   {
                       Id = "workspace/",
                       Description = "Workspace",
                   },
                new Referer
                {
                    Id = "http://www.google.co.th/",
                    Description = "Google",
                },
                new Referer
                {
                    Id = "config/theme/",
                    Description = "Theme configuration"
                },
                new Referer
                {
                    Id = "config/notification/",
                    Description = "Notification testing"
                },
                new Referer
                {
                    Id = "config/i18n/",
                    Description = "Internationalization testing"
                },
                new Referer
                {
                    Id = "config/upload/",
                    Description = "Upload"
                },
                new Referer
                {
                    Id = "report/view/",
                    Description = "Report viewer testing"
                },
                new Referer
                {
                    Id = "filemanager/",
                    Description = "File manager"
                },
                 new Referer
                 {
                     Id = "comparedocument/",
                     Description = "Compare File"
                 },
                 //MPS
                  new Referer
                {
                    Id = "Proposal/",
                    Description = "Create Proposal"
                },
                 new Referer
                {
                    Id = "Memo/",
                    Description = "Create Memog"
                },
                 new Referer
                {
                    Id = "PettyCash/",
                    Description = "Create Petty Cash"
                },
                 new Referer
                {
                    Id = "CashAdvance/",
                    Description = "Create Cash Advance"
                },
                new Referer
                {
                    Id = "UnitCode/",
                    Description = "Create UnitCode"
                },
                 new Referer
                {
                    Id = "Place/",
                    Description = "Create Store / Branch"
                },
                 new Referer
                {
                    Id = "TypeProposal/",
                    Description = "Create Proposal Type"
                },
                 new Referer
                {
                    Id = "TypeMemo/",
                    Description = "Create Memo Type"
                }
            );
            foreach (var referer in context.Referers.Local)
            {
                context.RefererRolePermissions.AddOrUpdate(
                    new RefererRolePermission
                    {
                        RoleId = "*",
                        RefererId = referer.Id,
                        Enable = true
                    }
                );
            }
        }
    }
}
