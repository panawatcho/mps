﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.Models.Master;

namespace TheMall.MPS.Infrastructure.EntityFramework.Seeding
{
    public class ExpenseTopicSeed : ISeeding
    {
        public void Seed(IMPSDbContext context)
        {
            //context.MPS_M_ExpenseTopics.AddOrUpdate(

            //    new MPS_M_ExpenseTopic
            //    {
            //       ExpenseTopicCode ="M",
            //       ExpenseTopicName = "Media / Printing",
            //       Description = "ค่าซื้อสื่อ หรือเช่าโฆษณา ค่าสิ่งพิมพ์ ค่าสื่อไปรษณีย์",
            //       TypeAP="A",
                   
            //    },
            //    new MPS_M_ExpenseTopic
            //    {
            //        ExpenseTopicCode = "P",
            //        ExpenseTopicName = "Production",
            //        Description = "ค่าผลิตงานโฆษณา เพื่อใช้ในการโฆษณาตามสื่อต่างๆ",
            //        TypeAP = "A",
                   

            //    },
            //    new MPS_M_ExpenseTopic
            //    {
            //        ExpenseTopicCode = "CP",
            //        ExpenseTopicName = "Consumer Promotion",
            //        Description = "ค่าใช้จ่ายส่งเสริมการขายด้านการตลาด เพื่อกระตุ้น / เพิ่มยอดขายของห้าง",
            //        TypeAP = "P",
                   
            //    }

            //    );
            //context.MPS_M_APcodes.AddOrUpdate(

            //    new MPS_M_APcode
            //    {
            //       APCode ="M1",
            //       APName ="TV",
            //       Description ="ค่า TV AIR TIME เช่น สปอตโฆษณา",
            //       CheckBudget = true,
            //       ExpenseTopicCode = "M"

            //    },
            //    new MPS_M_APcode
            //    {
            //        APCode = "M2",
            //        APName = "PRESS",
            //        Description = "ค่าโฆษณาหนังสือพิมพ์",
            //        CheckBudget = true,
            //        ExpenseTopicCode = "M"

            //    },
            //    new MPS_M_APcode
            //    {
            //        APCode = "M3",
            //        APName = "RADIO",
            //        Description = "ค่าโฆษณา Radio Spot",
            //        CheckBudget = true,
            //        ExpenseTopicCode = "M"

            //    },
            //     new MPS_M_APcode
            //     {
            //         APCode = "P3",
            //         APName = "RADIO",
            //         Description = "ค่าผลิต Spot โฆษณาเพื่อออกอากาศทางวิทยุ",
            //         CheckBudget = true,
            //         ExpenseTopicCode = "P"

            //     },
            //    new MPS_M_APcode
            //     {
            //         APCode = "P5",
            //         APName = "P.O.P",
            //         Description = "ค่าผลิตโครงสร้าง และอุปกรณ์สื่อภายในห้าง",
            //         CheckBudget = true,
            //         ExpenseTopicCode = "P"

            //     },
            //     new MPS_M_APcode
            //     {
            //         APCode = "P7",
            //         APName = "MAGAZINE",
            //         Description = "ค่าผลิต Art Work",
            //         CheckBudget = true,
            //         ExpenseTopicCode = "P"

            //     },
            //           new MPS_M_APcode
            //     {
            //         APCode = "CP1",
            //         APName = "CORORATE DISCOUNT",
            //         Description = "ค่าใช้จ่ายส่วนลดที่เป็น % ส่วนลด",
            //         CheckBudget = true,
            //         ExpenseTopicCode = "CP"

            //     },
            //       new MPS_M_APcode
            //       {
            //           APCode = "CP2",
            //           APName = "COUPON PRINTING",
            //           Description = "ค่าใช้จ่ายในการพิมพ์คูปองต่างๆ",
            //           CheckBudget = true,
            //           ExpenseTopicCode = "CP"

            //       }

            //    );
            //Branch
            //context.MPS_M_SharedBranchs.AddOrUpdate(

            //   new MPS_M_SharedBranch
            //   {
            //       BranchCode = "M1",
            //       BranchName = "M1",

            //   },
            //   new MPS_M_SharedBranch
            //   {
            //       BranchCode = "M2",
            //       BranchName = "M2",

            //   }
            //   ,
            //   new MPS_M_SharedBranch
            //   {
            //       BranchCode = "M3",
            //       BranchName = "M3",

            //   }
            //   ,
            //   new MPS_M_SharedBranch
            //   {
            //       BranchCode = "M4",
            //       BranchName = "M4",

            //   }
            //   ,
            //   new MPS_M_SharedBranch
            //   {
            //       BranchCode = "M5",
            //       BranchName = "M5",

            //   }
            //   ,
            //   new MPS_M_SharedBranch
            //   {
            //       BranchCode = "M6",
            //       BranchName = "M6",

            //   },
            //   new MPS_M_SharedBranch
            //   {
            //       BranchCode = "BluPort",
            //       BranchName = "BluPort",

            //   }
            //   );
        }
    }
   
}
