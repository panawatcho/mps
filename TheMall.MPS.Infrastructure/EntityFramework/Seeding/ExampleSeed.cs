﻿using System;
using TheMall.MPS.Models;
using TheMall.MPS.Models.Example;

namespace TheMall.MPS.Infrastructure.EntityFramework.Seeding
{
    public class ExampleSeed : ISeeding
    {
        public void Seed(IMPSDbContext context)
        {
            //#region Example
            //context.ExampleTables.AddOrUpdate(
            //    new ExampleTable
            //    {
            //        Id = 1,
            //        Title = "Title",
            //        Requester = "Me",
            //        RequesterName = "It's Me",
            //        DocumentDate = DateTime.UtcNow
            //    });

            //context.Processes.AddOrUpdate(
            //    new Process
            //    {
            //        ProcessCode = "Example",
            //        Name = "Example Process 1"
            //    },
            //    new Process
            //    {
            //        ProcessCode = "Process2",
            //        Name = "Process 2"
            //    });

            //context.DocumentTypes.AddOrUpdate(
            //    new DocumentType
            //    {
            //        Id = 1,
            //        Name = "Document 1",
            //        Activity = null,
            //        ProcessCode = "Example",
            //        IsRequired = true
            //    },
            //    new DocumentType
            //    {
            //        Id = 2,
            //        Name = "Document 2",
            //        Activity = null,
            //        ProcessCode = "Example",
            //        IsRequired = false
            //    },
            //    new DocumentType
            //    {
            //        Id = 3,
            //        Name = "Document 3",
            //        Activity = "Manager Approval",
            //        ProcessCode = "Example",
            //        IsRequired = true
            //    },
            //    new DocumentType
            //    {
            //        Id = 4,
            //        Name = "Document 4",
            //        Activity = "Manager Approval",
            //        ProcessCode = "Example",
            //        IsRequired = false
            //    },
            //    new DocumentType
            //    {
            //        Id = 5,
            //        Name = "Document 5",
            //        Activity = null,
            //        ProcessCode = "Process2",
            //        IsRequired = true
            //    },
            //    new DocumentType
            //    {
            //        Id = 6,
            //        Name = "Document 6",
            //        Activity = null,
            //        ProcessCode = "Process2",
            //        IsRequired = false
            //    },
            //    new DocumentType
            //    {
            //        Id = 7,
            //        Name = "Document 7",
            //        Activity = "Manager Approval",
            //        ProcessCode = "Process2",
            //        IsRequired = true
            //    },
            //    new DocumentType
            //    {
            //        Id = 8,
            //        Name = "Document 8",
            //        Activity = "Manager Approval",
            //        ProcessCode = "Process2",
            //        IsRequired = false
            //    });
            //#endregion
        }
    }
}
