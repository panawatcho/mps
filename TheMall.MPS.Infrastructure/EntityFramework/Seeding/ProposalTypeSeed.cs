﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.Models.Master;

namespace TheMall.MPS.Infrastructure.EntityFramework.Seeding
{
    public class ProposalTypeSeed : ISeeding
    {
        public void Seed(IMPSDbContext context)
        {
            context.MPS_M_TypeProposals.AddOrUpdate(

                new MPS_M_TypeProposal
                {
                    TypeProposalCode = "AP",
                    TypeProposalName = "A & P"
                     
                },
                new MPS_M_TypeProposal
                {
                    TypeProposalCode = "DEPOSIT",
                    TypeProposalName = "Deposit"

                },
                new MPS_M_TypeProposal
                {
                    TypeProposalCode = "DEPOSITIN",
                    TypeProposalName = "Deposit Internal"

                },
                new MPS_M_TypeProposal
                {
                    TypeProposalCode = "PREOP",
                    TypeProposalName = "Pre-Operation"

                },
                 new MPS_M_TypeProposal
                 {
                     TypeProposalCode = "SPECIAL",
                     TypeProposalName = "Special Event"

                 },
                  new MPS_M_TypeProposal
                  {
                      TypeProposalCode = "UNBUDGET",
                      TypeProposalName = "Un Budget"

                  }

                );

        }
    }
   
}
