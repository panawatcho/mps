﻿using TheMall.MPS.Infrastructure.Identity;

namespace TheMall.MPS.Infrastructure.EntityFramework.Seeding
{
    public class RoleSeed : ISeeding
    {
        public void Seed(IMPSDbContext context)
        {
            context.Roles.AddOrUpdate(
                new ApplicationRole
                {
                    Id = "*",
                    Name = "Everyone"
                });
        }
    }
}
