﻿using TheMall.MPS.Infrastructure.Security;

namespace TheMall.MPS.Infrastructure.EntityFramework.Seeding
{
    public class MenuItemSeed : ISeeding
    {
        public void Seed(IMPSDbContext context)
        {
            context.MenuItems.AddOrUpdate(
                new MenuItem
                {
                    Order = 1.00,
                    Id = "Workspace",
                    //Html = "<i class='fa fa-home'></i> <span>Home</span>",
                    IconClass = "fa fa-home",
                    Text = "WORKSPACE",
                    RefererId = "workspace/",
                },
                //new MenuItem
                //{
                //    Order = 2.00,
                //    Id = "Dashboard",
                //    //Html = "<i class='fa fa-circle-o'></i> <span>Dashboard</span>",
                //    IconClass = "fa fa-dashboard",
                //    Text = "Dashboard",
                //    RefererId = "home/dashboard/",
                //    //ParentId = "Home",
                //},
                new MenuItem
                {
                    Order = 3.00,
                    Id = "Proposal",
                    IconClass = "fa fa-circle-o",
                    Text = "PROPOSAL",
                    RefererId = "Proposal/",
                    //ParentId = "Home",
                },
                new MenuItem
                {
                    Order = 4.0,
                    Id = "Memo",
                    IconClass = "fa fa-circle-o",
                    Text = "MEMO",
                    RefererId = "Memo/",
                   // ParentId = "Level1",
                },
                 new MenuItem
                 {
                     Order = 5.0,
                     Id = "PettyCash",
                     IconClass = "fa fa-circle-o",
                     Text = "PETTY CASH",
                     RefererId = "PettyCash/",
                     // ParentId = "Level1",
                 },
                  new MenuItem
                  {
                      Order = 6.0,
                      Id = "CashAdvance",
                      IconClass = "fa fa-circle-o",
                      Text = "CASH ADVANCE",
                      RefererId = "CashAdvance/",
                      // ParentId = "Level1",
                  },
                new MenuItem
                {
                    Order = 7.0,
                    Id = "Master",
                    IconClass = "fa fa-database",
                    Text = "MASTER DATA",
                   
                },
                new MenuItem
                {
                    Order = 7.1,
                    Id = "UnitCode",
                    IconClass = "fa fa-database",
                    Text = "UNIT CODE",
                    RefererId = "UnitCode/",
                    ParentId = "Master",
                },
                new MenuItem
                {
                    Order = 7.2,
                    Id = "Place",
                    IconClass = "fa fa-database",
                    Text = "BRANCH",
                    RefererId = "Place/",
                    ParentId = "Master",
                },
                 new MenuItem
                 {
                     Order = 7.3,
                     Id = "TypeProposal",
                     IconClass = "fa fa-database",
                     Text = "TYPE PROPOSAL",
                     RefererId = "TypeProposal/",
                     ParentId = "Master",
                 },
                  new MenuItem
                  {
                      Order = 7.4,
                      Id = "TypeMemo",
                      IconClass = "fa fa-database",
                      Text = "TYPE MEMO",
                      RefererId = "TypeMemo/",
                      ParentId = "Master",
                  },
               
                new MenuItem
                {
                    Order = 8.00,
                    Id = "Reports",
                    IconClass = "fa fa-folder",
                    Text = "REPORTS"
                },
                new MenuItem
                {
                    Order = 8.10,
                    Id = "Report1",
                    IconClass = "fa fa-file-o",
                    Text = "Report 1",
                    RefererId = "report/view/",
                    ParentId = "Reports"
                },
                    new MenuItem
                {
                    Order = 8.20,
                    Id = "Report2",
                    IconClass = "fa fa-file-o",
                    Text = "Report 2",
                    RefererId = "report/view/",
                    ParentId = "Reports"
                }
               
            );
            
        }
    }
}
