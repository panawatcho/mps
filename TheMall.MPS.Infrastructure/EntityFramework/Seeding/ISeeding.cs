﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheMall.MPS.Infrastructure.EntityFramework.Seeding
{
    public interface ISeeding
    {
        void Seed(IMPSDbContext context);
    }
}
