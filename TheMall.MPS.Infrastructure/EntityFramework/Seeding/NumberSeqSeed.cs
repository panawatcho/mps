﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.Models.NumberSeq;

namespace TheMall.MPS.Infrastructure.EntityFramework.Seeding
{
    public class NumberSeqSeed : ISeeding
    {
        public void Seed(IMPSDbContext context)
        {
            //Proposal
            context.NumberSeqTables.AddOrUpdate(
                  new NumberSeqTable
                  {
                      NumberSeqCode = "AP",
                      Name = "A&P",
                      NextNum = 1,
                      Format =  "AP.yy.####"
                  }
              );
            context.NumberSeqTables.AddOrUpdate(
                new NumberSeqTable
                {
                    NumberSeqCode = "DEPOSIT",
                    Name = "Deposit",
                    NextNum = 1,
                    Format = "DP.yy.####"
                }
            );
            context.NumberSeqTables.AddOrUpdate(
                new NumberSeqTable
                {
                    NumberSeqCode = "DEPOSITIN",
                    Name = "Deposit Internal",
                    NextNum = 1,
                    Format = "DI.yy.####"
                }
            );
            context.NumberSeqTables.AddOrUpdate(
                new NumberSeqTable
                {
                    NumberSeqCode = "PREOP",
                    Name = "Pre-Operation",
                    NextNum = 1,
                    Format =  "PO.yy.####"
                }
            );
            context.NumberSeqTables.AddOrUpdate(
                new NumberSeqTable
                {
                    NumberSeqCode = "SPECIAL",
                    Name = "Special Event",
                    NextNum = 1,
                    Format = "SE.yy.####"
                }
            );
            context.NumberSeqTables.AddOrUpdate(
               new NumberSeqTable
               {
                   NumberSeqCode = "UNBUDGET",
                   Name = "Un Budget",
                   NextNum = 1,
                   Format = "UN.yyyy.####"
               }
           );
            //Memo
            context.NumberSeqTables.AddOrUpdate(
               new NumberSeqTable
               {
                   NumberSeqCode = "MEMO",
                   Name = "Memo",
                   NextNum = 1,
                   Format = "M-yy-###"
               }
           );
            //PettyCash
            context.NumberSeqTables.AddOrUpdate(
              new NumberSeqTable
              {
                  NumberSeqCode = "PETTYCASH",
                  Name = "Petty Cash",
                  NextNum = 1,
                  Format = "P-yy-###"
              }
          );
            //Cash Advance
            context.NumberSeqTables.AddOrUpdate(
             new NumberSeqTable
             {
                 NumberSeqCode = "ADVANCE",
                 Name = "Cash Advance",
                 NextNum = 1,
                 Format = "A-yy-###"
             }
         );
            //Cash Clearing
            context.NumberSeqTables.AddOrUpdate(
             new NumberSeqTable
             {
                 NumberSeqCode = "CLEARING",
                 Name = "Cash Clearing",
                 NextNum = 1,
                 Format = "C-yy-###"
             }
         );
            
            //NumberSeq Setups
            //Process Proposal
            context.NumberSeqSetups.AddOrUpdate(
                new NumberSeqSetup
                {
                    NumberSeqCode = "AP",
                    Name = "A&P",
                    Process = "ProposalA&P",
                    Year = DateTime.Now.Year.ToString()
                }
            );
            context.NumberSeqSetups.AddOrUpdate(
               new NumberSeqSetup
               {
                   NumberSeqCode = "DEPOSIT",
                   Name = "Deposit",
                   Process = "ProposalDeposit",
                   Year = DateTime.Now.Year.ToString()
               }
           );
            context.NumberSeqSetups.AddOrUpdate(
               new NumberSeqSetup
               {
                   NumberSeqCode = "DEPOSITIN",
                   Name = "Deposit Internal",
                   Process = "ProposalDepositInternal",
                   Year = DateTime.Now.Year.ToString()
               }
           );
            context.NumberSeqSetups.AddOrUpdate(
               new NumberSeqSetup
               {
                   NumberSeqCode = "PREOP",
                   Name = "Pre-Operation",
                   Process = "ProposalPreOperation",
                   Year = DateTime.Now.Year.ToString()
               }
           );
            context.NumberSeqSetups.AddOrUpdate(
               new NumberSeqSetup
               {
                   NumberSeqCode = "SPECIAL",
                   Name = "Special Event",
                   Process = "ProposalSpecialEvent",
                   Year = DateTime.Now.Year.ToString()
               }
           );
            context.NumberSeqSetups.AddOrUpdate(
               new NumberSeqSetup
               {
                   NumberSeqCode = "UNBUDGET",
                   Name = "Un Budget",
                   Process = "ProposalUnBudget",
                   Year = DateTime.Now.Year.ToString()
               }
           );
            //ProcessMemo
            context.NumberSeqSetups.AddOrUpdate(
               new NumberSeqSetup
               {
                   NumberSeqCode = "MEMO",
                   Name = "Memo",
                   Process = "Memo",
                   Year = DateTime.Now.Year.ToString()
               }
           );
            context.NumberSeqSetups.AddOrUpdate(
              new NumberSeqSetup
              {
                  NumberSeqCode = "PETTYCASH",
                  Name = "Petty Cash",
                  Process = "PettyCash",
                  Year = DateTime.Now.Year.ToString()
              }
          );
            context.NumberSeqSetups.AddOrUpdate(
              new NumberSeqSetup
              {
                  NumberSeqCode = "ADVANCE",
                  Name = "Cash Advance",
                  Process = "CashAdvance",
                  Year = DateTime.Now.Year.ToString()
              }
          );
            context.NumberSeqSetups.AddOrUpdate(
              new NumberSeqSetup
              {
                  NumberSeqCode = "CLEARING",
                  Name = "Cash Clearing",
                  Process = "CashClearing",
                  Year = DateTime.Now.Year.ToString()
              }
          );
        }
    }
}
