﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TheMall.MPS.Infrastructure.Identity;
using TheMall.MPS.Models.Abstracts;

namespace TheMall.MPS.Infrastructure.Security
{
    public class RefererRolePermission : BaseTrackableModel
    {
        [Key, Column(Order = 1)]
        [StringLength(256)]
        public string RefererId { get; set; }

        [ForeignKey("RefererId")]
        public virtual Referer Referer { get; set; }

        [Key, Column(Order = 2)]
        [StringLength(256)]
        public string RoleId { get; set; }

        [ForeignKey("RoleId")]
        public virtual ApplicationRole Role { get; set; }

        public bool Enable { get; set; }
    }
}
