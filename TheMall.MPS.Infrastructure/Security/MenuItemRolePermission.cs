﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TheMall.MPS.Infrastructure.Identity;
using TheMall.MPS.Models.Abstracts;

namespace TheMall.MPS.Infrastructure.Security
{
    public class MenuItemRolePermission : BaseTrackableModel
    {
        [Key]
        [Column(Order = 1)]
        [StringLength(256)]
        public string MenuItemId { get; set; }

        [ForeignKey("MenuItemId")]
        public MenuItem MenuItem { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(256)]
        public string RoleId { get; set; }

        [ForeignKey("RoleId")]
        public virtual ApplicationRole Role { get; set; }

        public bool Inactive { get; set; }
    }
}
