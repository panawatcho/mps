﻿namespace TheMall.MPS.Infrastructure.Security
{
    public static class Claim
    {
        public const string CRS_ROLE_CLAIMTYPE = "crs/role";
        public const string CRS_REFERER_CLAIMTYPE = "crs/referer";
    }
}
