﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TheMall.MPS.Models;
using TheMall.MPS.Models.Abstracts;

namespace TheMall.MPS.Infrastructure.Security
{
    /// <summary>
    /// MenuItem class. Support both internal and external link.
    /// </summary>
    public class MenuItem : BaseTrackableModel
    {
        public MenuItem()
        {
            Children = new List<MenuItem>();
        }
        [Key]
        [StringLength(256)]
        public string Id { get; set; }

        /// <summary>
        /// The internal link. Foreign key of Referer.
        /// </summary>
        [StringLength(Metadata.RefererIdLength)]
        public string RefererId { get; set; }
        [ForeignKey("RefererId")]
        public virtual Referer Referer { get; set; }

        /// <summary>
        ///  The class you need for <i></i>
        /// </summary>
        [StringLength(100)]
        public string IconClass { get; set; }

        /// <summary>
        /// Translatable text. Use this if you want the menu to be translatable by client side.
        /// </summary>
        [StringLength(200)]
        public string Text { get; set; }

        /// <summary>
        /// Direct html. If this field is not null, IconClass and Text will not be used.
        /// </summary>
        [StringLength(500)]
        public string Html { get; set; }

        /// <summary>
        /// Link to external Uri. No Referer required.
        /// </summary>
        //[StringLength(500)]
        //public string Link { get; set; }

        public bool Inactive { get; set; }

        public double Order { get; set; }

        public string ParentId { get; set; }

        public bool NewTab { get; set; }

        [ForeignKey("ParentId")]
        public virtual MenuItem Parent { get; set; }

        public virtual ICollection<MenuItem> Children { get; set; } 
    }
}
