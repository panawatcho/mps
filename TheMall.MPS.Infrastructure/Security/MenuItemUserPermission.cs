﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TheMall.MPS.Infrastructure.Identity;
using TheMall.MPS.Models.Abstracts;

namespace TheMall.MPS.Infrastructure.Security
{
    public class MenuItemUserPermission : BaseTrackableModel
    {
        [Key]
        [Column(Order = 1)]
        [StringLength(256)]
        public string MenuItemId { get; set; }

        [ForeignKey("MenuItemId")]
        public MenuItem MenuItem { get; set; }

        [Key]
        [Column(Order = 2)]
        public string ApplicationUserId { get; set; }

        [ForeignKey("ApplicationUserId")]
        public virtual ApplicationUser ApplicationUser { get; set; }

        //[StringLength(256)]
        //public string UserName {
        //    get
        //    {
        //        if (this.ApplicationUser != null)
        //        {
        //            return ApplicationUser.UserName;
        //        }
        //        return null;
        //    }
        //}

        public bool Inactive { get; set; }
    }
}
