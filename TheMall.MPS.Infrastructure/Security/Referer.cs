﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using TheMall.MPS.Models;
using TheMall.MPS.Models.Abstracts;

namespace TheMall.MPS.Infrastructure.Security
{
    public class Referer : BaseTrackableModel
    {
        [StringLength(Metadata.RefererIdLength)]
        public string Id { get; set; }
        [StringLength(256)]
        public string Description { get; set; }

        public virtual ICollection<RefererRolePermission> RolePermissions { get; set; }
        public virtual ICollection<RefererUserPermission> UserPermissions { get; set; } 
    }
}
