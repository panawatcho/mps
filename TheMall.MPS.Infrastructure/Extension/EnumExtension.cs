﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.Models.Attributes;
using TheMall.MPS.ViewModels;

namespace TheMall.MPS.Infrastructure.Extension
{
    public static class EnumExtension
    {
        #region Dictionary
        public static Dictionary<int, string> ToDictionary(this Type enumType)
        {
            if (!enumType.IsEnum)
                throw new Exception("This extension method only work on Enum type.");

            var dictionary = new Dictionary<int, string>();

            foreach (int i in Enum.GetValues(enumType))
            {
                var result = GetEnumDisplayName(i, enumType);
                dictionary.Add(i, result);
            }
            return dictionary;
        }

        public static Dictionary<string, string> ToDictionaryTextBased(this Type enumType, int? selectedIndex = null, bool addEmpty = false)
        {
            if (!enumType.IsEnum)
                throw new Exception("This extension method only work on Enum type.");

            var dictionary = new Dictionary<string, string>();
            var names = Enum.GetNames(enumType);
            foreach (int i in Enum.GetValues(enumType))
            {
                var result = GetEnumDisplayName(i, enumType);
                dictionary.Add(names[i], result);
            }
            return dictionary;
        }

        public static Dictionary<int, string> ToDictionary(this Type enumType, int key)
        {
            if (!enumType.IsEnum)
                throw new Exception("This extension method only work on Enum type.");
            var result = GetEnumDisplayName(key, enumType);

            var dictionary = new Dictionary<int, string>();
            dictionary.Add(key, result);
            return dictionary;
        }
        #endregion

        #region KeyValuePair
        public static IEnumerable<KeyValuePair<int, string>> ToKeyValuePairs(this Type enumType)
        {
            if (!enumType.IsEnum)
                throw new Exception("This extension method only work on Enum type.");

            var list = new List<KeyValuePair<int, string>>();

            foreach (int i in Enum.GetValues(enumType))
            {
                var result = GetEnumDisplayName(i, enumType);
                var pair = new KeyValuePair<int, string>
                    (
                    i,
                    result
                    );
                list.Add(pair);
            }
            return list;
        }

        public static IEnumerable<KeyValuePair<string, string>> ToKeyValuePairsTextBased(this Type enumType, int? selectedIndex = null, bool addEmpty = false)
        {
            if (!enumType.IsEnum)
                throw new Exception("This extension method only work on Enum type.");

            var list = new List<KeyValuePair<string, string>>();
            var j = 0;
            var names = Enum.GetNames(enumType);
            foreach (int i in Enum.GetValues(enumType))
            {
                var result = GetEnumDisplayName(i, enumType);
                var pair = new KeyValuePair<string, string>
                    (
                    names[j],
                    result
                    );
                j++;
                list.Add(pair);
            }
            return list;
        }

        public static KeyValuePair<int, string> ToKeyValuePair(this Type enumType, int key)
        {
            if (!enumType.IsEnum)
                throw new Exception("This extension method only work on Enum type.");
            var result = GetEnumDisplayName(key, enumType);

            return new KeyValuePair<int, string>(key, result);
        }
        #endregion

        #region StaticDataSouce

        public static IEnumerable<StaticDatasource> ToStaticDatasources(this Type enumType)
        {
            if (!enumType.IsEnum)
                throw new Exception("This extension method only work on Enum type.");

            var list = new List<ViewModels.StaticDatasource>();

            foreach (int i in Enum.GetValues(enumType))
            {
                var result = GetEnumDisplayName(i, enumType);
                list.Add(new ViewModels.StaticDatasource()
                {
                    value = i.ToString(),
                    text = result
                });
            }
            return list.OrderBy(m => m.value);
        }

        public static ViewModels.StaticDatasource ToStaticDatasource(this Type enumType, int? value)
        {
            if (!value.HasValue)
            {
                return null;
            }
            return ToStaticDatasource(enumType, value.Value);
        }

        public static ViewModels.StaticDatasource ToStaticDatasource(this Type enumType, int value)
        {
            Type type = enumType;
            if (enumType.IsGenericType && enumType.GenericTypeArguments.Any())
            {
                type = enumType.GetGenericArguments().First();
            }
            if (!type.IsEnum)
                throw new Exception("This extension method only work on Enum type.");

            //var values = Enum.GetValues(enumType);
            var result = GetEnumDisplayName(value, type);
            return new ViewModels.StaticDatasource()
            {
                value = value.ToString(),
                text = result
            };
        }
        public static ViewModels.StaticDatasource ToStaticDatasource<TEnum>(this Type enumType, TEnum value)
        {
            if (!enumType.IsEnum)
                throw new Exception("This extension method only work on Enum type.");

            var i = Convert.ToInt32(value);
            var result = GetEnumDisplayName(i, enumType);
            return new ViewModels.StaticDatasource()
            {
                value = i.ToString(),
                text = result
            };
        }
        public static ViewModels.StaticDatasource ToStaticDatasource<TEnum>(TEnum value)
        {
            if (value == null)
            {
                return null;
            }
            Type type = typeof(TEnum);
            if (type.IsGenericType && type.GenericTypeArguments.Any())
            {
                type = type.GetGenericArguments().First();
            }
            if (!type.IsEnum)
                throw new Exception("This extension method only work on Enum type.");

            var i = Convert.ToInt32(value);
            var result = GetEnumDisplayName(i, type);
            return new ViewModels.StaticDatasource()
            {
                value = i.ToString(),
                text = result
            };
        }
        public static IEnumerable<ViewModels.StaticDatasource> ToStaticDatasourcesTextBased(this Type enumType)
        {
            if (!enumType.IsEnum)
                throw new Exception("This extension method only work on Enum type.");

            var list = new List<ViewModels.StaticDatasource>();

            foreach (var i in enumType.ToDictionaryTextBased())
            {
                list.Add(new ViewModels.StaticDatasource()
                {
                    value = i.Key,
                    text = i.Value
                });
            }
            return list;
        }
        public static Array GetValues(this Type enumType)
        {
            Type type = enumType;
            if (enumType.IsGenericType && enumType.GenericTypeArguments.Any())
            {
                type = enumType.GetGenericArguments().First();
            }
            return Enum.GetValues(type);
        }
        #endregion

        #region private
        public static string GetDescription(this Enum enumType)
        {
            FieldInfo fi = enumType.GetType().GetField(enumType.ToString());


            var descriptionAttribute = (DescriptionAttribute)fi.GetCustomAttributes(
                                                typeof(DescriptionAttribute), false).FirstOrDefault();
            if (descriptionAttribute != null)
            {
                return descriptionAttribute.Description;
            }
            var resource = (ResourceDescriptionAttribute)fi.GetCustomAttributes(
                                                typeof(ResourceDescriptionAttribute), false).FirstOrDefault();
            if (resource != null)
            {
                return resource.Description;
            }
            return string.Empty;
        }

        private static string GetEnumDisplayName(int o, Type enumType)
        {
            string enumValue = Enum.GetName(enumType, o);
            if (string.IsNullOrEmpty(enumValue))
            {
                throw new ArgumentOutOfRangeException("o");
            }
            var field = enumType.GetField(enumValue);
            if (field != null)
            {
                var descriptionAttribute = field
                    .GetCustomAttributes(typeof(DescriptionAttribute), false)
                    .Cast<DescriptionAttribute>()
                    .FirstOrDefault();
                if (descriptionAttribute != null)
                {
                    return descriptionAttribute.Description;
                }

                return field.Name;
            }
            return o.ToString();
        }
        private static string GetEnumDisplayName(string o, Type enumType)
        {
            if (Enum.IsDefined(enumType, o))
            {
                var i = Enum.Parse(enumType, o, true);
                string enumValue = Enum.GetName(enumType, i);
                var field = enumType.GetField(enumValue);
                if (field != null)
                {
                    var descriptionAttrivute = field
                    .GetCustomAttributes(typeof(DescriptionAttribute), false)
                    .Cast<DescriptionAttribute>()
                    .FirstOrDefault();
                    if (descriptionAttrivute != null)
                    {
                        return descriptionAttrivute.Description;
                    }

                    return field.Name;
                }
            }
            return o;
        }

        public static bool GetIsRequired(this Enum enumType)
        {
            FieldInfo fi = enumType.GetType().GetField(enumType.ToString());


            var isRequiredAttribute = (IsRequiredAttribute)fi.GetCustomAttributes(
                                                typeof(IsRequiredAttribute), false).FirstOrDefault();
            if (isRequiredAttribute == null)
                return false;

            return isRequiredAttribute.IsRequired;
        }
        #endregion
    }
}
