﻿namespace TheMall.MPS.Infrastructure
{
    public static class Constants
    {
        public const string WILDCARD = "*";
        public const string SecurityLabelName = "K2";
        public const string PROCESS_FOLDER = "MPS\\Processes";
        public const int DUE_DURATION_SECONDS = 86400; // 24 Hours
#if DEBUG
        public const int CacheTime = 0;
        public const int ServerCacheTime = 0;
#else
        public const int CacheTime = 28800;
        public const int ServerCacheTime = 28800;
#endif
    }
}
