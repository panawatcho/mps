﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;

namespace TheMall.MPS.Infrastructure
{
    public static class UserHelper
    {
        public static IEnumerable<Claim> GetRolesId(IIdentity identity)
        {
            var c = identity as ClaimsIdentity;
            if (c != null)
            {
                return c.Claims.Where(m => m.Type == TheMall.MPS.Infrastructure.Security.Claim.CRS_ROLE_CLAIMTYPE);
            }
            return Enumerable.Empty<Claim>();
        }

        public static IEnumerable<Claim> GetReferer(IIdentity identity)
        {
            var c = identity as ClaimsIdentity;
            if (c != null)
            {
                return c.Claims.Where(m => m.Type == TheMall.MPS.Infrastructure.Security.Claim.CRS_REFERER_CLAIMTYPE);
            }
            return Enumerable.Empty<Claim>();
        }

        public static string CurrentUserName()
        {
            return ((ClaimsPrincipal) System.Threading.Thread.CurrentPrincipal).Identity.Name;
        }
    }
}