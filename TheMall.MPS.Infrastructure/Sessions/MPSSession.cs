﻿using CrossingSoft.Framework.Infrastructure.EF6;
using CrossingSoft.Framework.Infrastructure.EF6.Factories;
using CrossingSoft.Framework.Infrastructure.Session;
using TheMall.MPS.Infrastructure.EntityFramework;

namespace TheMall.MPS.Infrastructure.Sessions
{
    public interface IMPSSession : ISessionAsync
    {
        
    }
    public class MPSSession : Session, IMPSSession
    {
        public MPSSession(IMPSDbContext dataContext, IRepositoryProvider repositoryProvider)
            : base(dataContext, repositoryProvider)
        {
        }
    }
}