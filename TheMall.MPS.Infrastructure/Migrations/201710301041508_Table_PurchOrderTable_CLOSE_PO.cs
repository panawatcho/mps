namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Table_PurchOrderTable_CLOSE_PO : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MPS_PurchOrderTable", "CLOSE_PO", c => c.Boolean());
        }
        
        public override void Down()
        {
            DropColumn("dbo.MPS_PurchOrderTable", "CLOSE_PO");
        }
    }
}
