namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addfiledTableReceiptLine : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MPS_ReceiptLine", "NetAmountNoVatTax", c => c.Decimal(precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MPS_ReceiptLine", "NetAmountNoVatTax");
        }
    }
}
