namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class fk : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.MPS_M_FinalApprover", "EmpId");
            AddForeignKey("dbo.MPS_M_FinalApprover", "EmpId", "dbo.MPS_EmployeeTable", "EmpId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MPS_M_FinalApprover", "EmpId", "dbo.MPS_EmployeeTable");
            DropIndex("dbo.MPS_M_FinalApprover", new[] { "EmpId" });
        }
    }
}
