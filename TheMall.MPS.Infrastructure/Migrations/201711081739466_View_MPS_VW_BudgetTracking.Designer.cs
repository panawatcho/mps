// <auto-generated />
namespace TheMall.MPS.Infrastructure.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class View_MPS_VW_BudgetTracking : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(View_MPS_VW_BudgetTracking));
        
        string IMigrationMetadata.Id
        {
            get { return "201711081739466_View_MPS_VW_BudgetTracking"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
