namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addVendorCode_PettyCash : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.MPS_M_Company");
            AddColumn("dbo.MPS_PettyCashTable", "VendorCode", c => c.String());
            AddColumn("dbo.MPS_M_Company", "VendorCode", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.MPS_M_Company", "CompanyCode", c => c.String());
            AddPrimaryKey("dbo.MPS_M_Company", "VendorCode");
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.MPS_M_Company");
            AlterColumn("dbo.MPS_M_Company", "CompanyCode", c => c.String(nullable: false, maxLength: 128));
            DropColumn("dbo.MPS_M_Company", "VendorCode");
            DropColumn("dbo.MPS_PettyCashTable", "VendorCode");
            AddPrimaryKey("dbo.MPS_M_Company", "CompanyCode");
        }
    }
}
