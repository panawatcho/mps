namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addTablePrPo : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MPS_ProcuirementTable",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProcuirementType = c.String(maxLength: 2),
                        Process = c.String(),
                        BudgetDetail = c.Decimal(nullable: false, precision: 18, scale: 2),
                        BudgetAPCode = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ProposalRefID = c.Int(nullable: false),
                        ProposalRefDocumentNumber = c.String(maxLength: 20),
                        BranchCode = c.String(maxLength: 100),
                        Description = c.String(),
                        ProposalLineID = c.Int(nullable: false),
                        UnitCode = c.String(maxLength: 20),
                        UnitName = c.String(maxLength: 1000),
                        AccountingUsername = c.String(maxLength: 100),
                        AccountingEmail = c.String(maxLength: 200),
                        Title = c.String(nullable: false, maxLength: 200),
                        DocumentNumber = c.String(maxLength: 20),
                        Revision = c.Int(),
                        RequesterUserName = c.String(nullable: false, maxLength: 100),
                        RequesterName = c.String(nullable: false, maxLength: 200),
                        SubmittedDate = c.DateTime(),
                        Status = c.String(maxLength: 100),
                        DocumentStatus = c.String(maxLength: 100),
                        StatusFlag = c.Int(nullable: false),
                        ProcInstId = c.Int(),
                        ExpectedDuration = c.Int(),
                        DocumentDate = c.DateTime(),
                        CompletedDate = c.DateTime(),
                        CancelledDate = c.DateTime(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        Deleted = c.Boolean(nullable: false),
                        DeletedDate = c.DateTime(),
                        DeletedBy = c.String(maxLength: 100),
                        DeletedByName = c.String(maxLength: 200),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MPS_ProposalLine", t => t.ProposalLineID, cascadeDelete: true)
                .ForeignKey("dbo.MPS_M_UnitCode", t => t.UnitCode)
                .Index(t => t.ProposalLineID)
                .Index(t => t.UnitCode)
                .Index(t => new { t.DocumentNumber, t.Revision }, name: "DocumentNumber_Revision");
            
            AddColumn("dbo.MPS_M_UnitCode", "InActive", c => c.Boolean(nullable: false));
            AddColumn("dbo.MPS_M_APCode", "InActive", c => c.Boolean(nullable: false));
            AddColumn("dbo.MPS_M_ExpenseTopic", "InActive", c => c.Boolean(nullable: false));
            AddColumn("dbo.MPS_M_Place", "InActive", c => c.Boolean(nullable: false));
            AddColumn("dbo.MPS_M_TypeMemo", "InActive", c => c.Boolean(nullable: false));
            AddColumn("dbo.MPS_M_TypeProposal", "InActive", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MPS_ProcuirementTable", "UnitCode", "dbo.MPS_M_UnitCode");
            DropForeignKey("dbo.MPS_ProcuirementTable", "ProposalLineID", "dbo.MPS_ProposalLine");
            DropIndex("dbo.MPS_ProcuirementTable", "DocumentNumber_Revision");
            DropIndex("dbo.MPS_ProcuirementTable", new[] { "UnitCode" });
            DropIndex("dbo.MPS_ProcuirementTable", new[] { "ProposalLineID" });
            DropColumn("dbo.MPS_M_TypeProposal", "InActive");
            DropColumn("dbo.MPS_M_TypeMemo", "InActive");
            DropColumn("dbo.MPS_M_Place", "InActive");
            DropColumn("dbo.MPS_M_ExpenseTopic", "InActive");
            DropColumn("dbo.MPS_M_APCode", "InActive");
            DropColumn("dbo.MPS_M_UnitCode", "InActive");
            DropTable("dbo.MPS_ProcuirementTable");
        }
    }
}
