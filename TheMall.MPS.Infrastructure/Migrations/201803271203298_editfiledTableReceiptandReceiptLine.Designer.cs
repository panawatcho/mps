// <auto-generated />
namespace TheMall.MPS.Infrastructure.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class editfiledTableReceiptandReceiptLine : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(editfiledTableReceiptandReceiptLine));
        
        string IMigrationMetadata.Id
        {
            get { return "201803271203298_editfiledTableReceiptandReceiptLine"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
