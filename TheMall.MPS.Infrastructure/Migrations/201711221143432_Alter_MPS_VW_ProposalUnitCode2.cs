namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class Alter_MPS_VW_ProposalUnitCode2 : DbMigration
    {
        public override void Up()
        {
            this.Sql(@"ALTER VIEW [dbo].[MPS_VW_ProposalUnitCode] AS
                    SELECT ROW_NUMBER() OVER(ORDER BY Id) AS RowNo,
                 STUFF((
                    SELECT ',' + UnitCode
                    FROM (SELECT DISTINCT pl.ParentId,pl.UnitCode
                                    FROM MPS_ProposalLine pl
                                    WHERE pl.UnitCode IS NOT NULL AND 
                                    pl.Deleted = 0 AND
                                    pl.ParentId = pt.Id) AS Temp
                    FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 1, '') AS UnitCodeAP
                   ,pt.Id
                  ,pt.ProposalTypeCode
                  ,pt.ProposalTypeName
                  ,pt.Place
                  ,pt.Source
                  ,pt.Other
                  ,pt.DepositId
                  ,pt.DepositNumber
                  ,pt.StartDate
                  ,pt.EndDate
                  ,pt.Budget
                  ,pt.CustomersTargetGroup
                  ,pt.CheckBudget
                  ,pt.TransferToNextYear
                  ,pt.TotalMarketingExpense
                  ,pt.TotalIncomeDeposit
                  ,pt.TotalIncomeOther
                  ,pt.TotalSaleTarget
                  ,pt.DepartmentException
                  ,pt.UnitCode
                  ,pt.UnitName
                  ,pt.Title
                  ,pt.DocumentNumber
                  ,pt.Revision
                  ,pt.RequesterUserName
                  ,pt.RequesterName
                  ,pt.SubmittedDate
                  ,pt.Status
                  ,pt.DocumentStatus
                  ,pt.StatusFlag
                  ,pt.ProcInstId
                  ,pt.ExpectedDuration
                  ,pt.DocumentDate
                  ,pt.CompletedDate
                  ,pt.CancelledDate
                  ,pt.RowVersion
                  ,pt.Deleted
                  ,pt.DeletedDate
                  ,pt.DeletedBy
                  ,pt.DeletedByName
                  ,pt.CreatedBy
                  ,pt.CreatedByName
                  ,pt.CreatedDate
                  ,pt.ModifiedBy
                  ,pt.ModifiedByName
                  ,pt.ModifiedDate
                  ,pt.ProjectStartDate
                  ,pt.ProjectEndDate
                  ,pt.TR
                  ,pt.RE
                  ,pt.AccountingUsername
                  ,pt.CloseFlag
                  ,pt.AccountingEmail
                  ,pt.TotalBudget
                  ,pt.InformUsername
                  ,pt.InformEmail
                  ,pt.CCUsername
                  ,pt.CCEmail
                  ,pt.TotalAdvertising
                  ,pt.TotalPromotion
                  ,pt.PecentAdvertising
                  ,pt.PecentPromotion
                  ,pt.PecentMarketingExpense
                  ,pt.DueDatePropose
                  ,pt.DueDateAccept
                  ,pt.DueDateApprove
                  ,pt.DueDateFinalApprove
                  ,pt.AccountingBranch
                    FROM MPS_ProposalTable pt
					WHERE
                    pt.StatusFlag = 9");
        }

        public override void Down()
        {
        }
    }
}
