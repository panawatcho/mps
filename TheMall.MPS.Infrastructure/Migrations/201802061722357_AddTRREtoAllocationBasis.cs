namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTRREtoAllocationBasis : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MPS_M_AllocationBasis", "TR", c => c.Boolean(nullable: false));
            AddColumn("dbo.MPS_M_AllocationBasis", "RE", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MPS_M_AllocationBasis", "RE");
            DropColumn("dbo.MPS_M_AllocationBasis", "TR");
        }
    }
}
