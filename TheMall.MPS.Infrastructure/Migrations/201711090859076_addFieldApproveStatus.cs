namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addFieldApproveStatus : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MPS_ProposalApproval", "ApproveStatus", c => c.Boolean(nullable: false));
            AddColumn("dbo.MPS_CashAdvanceApproval", "ApproveStatus", c => c.Boolean(nullable: false));
            AddColumn("dbo.MPS_MemoApproval", "ApproveStatus", c => c.Boolean(nullable: false));
            AddColumn("dbo.MPS_PettyCashApproval", "ApproveStatus", c => c.Boolean(nullable: false));
            AddColumn("dbo.MPS_CashClearingApproval", "ApproveStatus", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MPS_CashClearingApproval", "ApproveStatus");
            DropColumn("dbo.MPS_PettyCashApproval", "ApproveStatus");
            DropColumn("dbo.MPS_MemoApproval", "ApproveStatus");
            DropColumn("dbo.MPS_CashAdvanceApproval", "ApproveStatus");
            DropColumn("dbo.MPS_ProposalApproval", "ApproveStatus");
        }
    }
}
