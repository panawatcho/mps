namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addFieldDescriptionToPlace : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MPS_M_Place", "Description", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.MPS_M_Place", "Description");
        }
    }
}
