namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MPS_MemoIncomeInvoice : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MPS_MemoIncomeTable", "AccOnProcessUsername", c => c.String());
            AddColumn("dbo.MPS_MemoIncomeTable", "AccCompleteUsername", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.MPS_MemoIncomeTable", "AccCompleteUsername");
            DropColumn("dbo.MPS_MemoIncomeTable", "AccOnProcessUsername");
        }
    }
}
