namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addFieldToActualChargeDepositMemo : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MPS_MemoTable", "ActualCharge", c => c.Boolean(nullable: false));
            AddColumn("dbo.MPS_MemoTable", "IncomeDepositId", c => c.Int());
            AddColumn("dbo.MPS_IncomeDeposit", "ActualCharge", c => c.Boolean(nullable: false));
            AddColumn("dbo.MPS_IncomeDeposit", "RefMemoId", c => c.Int());
            AddColumn("dbo.MPS_Log_IncomeOther", "ProposalLineId", c => c.Int());
            AddColumn("dbo.MPS_Log_IncomeDeposit", "ActualCharge", c => c.Boolean(nullable: false));
            AddColumn("dbo.MPS_Log_IncomeDeposit", "RefMemoId", c => c.Int());
            AddColumn("dbo.MPS_M_TypeMemo", "ActualCharge", c => c.Boolean(nullable: false));
            AlterColumn("dbo.MPS_M_ProcessApprove", "ProcessType", c => c.String());
            CreateIndex("dbo.MPS_MemoTable", "IncomeDepositId");
            CreateIndex("dbo.MPS_Log_IncomeOther", "ProposalLineId");
            AddForeignKey("dbo.MPS_MemoTable", "IncomeDepositId", "dbo.MPS_IncomeDeposit", "Id");
            AddForeignKey("dbo.MPS_Log_IncomeOther", "ProposalLineId", "dbo.MPS_ProposalLine", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MPS_Log_IncomeOther", "ProposalLineId", "dbo.MPS_ProposalLine");
            DropForeignKey("dbo.MPS_MemoTable", "IncomeDepositId", "dbo.MPS_IncomeDeposit");
            DropIndex("dbo.MPS_Log_IncomeOther", new[] { "ProposalLineId" });
            DropIndex("dbo.MPS_MemoTable", new[] { "IncomeDepositId" });
            AlterColumn("dbo.MPS_M_ProcessApprove", "ProcessType", c => c.String(maxLength: 10));
            DropColumn("dbo.MPS_M_TypeMemo", "ActualCharge");
            DropColumn("dbo.MPS_Log_IncomeDeposit", "RefMemoId");
            DropColumn("dbo.MPS_Log_IncomeDeposit", "ActualCharge");
            DropColumn("dbo.MPS_Log_IncomeOther", "ProposalLineId");
            DropColumn("dbo.MPS_IncomeDeposit", "RefMemoId");
            DropColumn("dbo.MPS_IncomeDeposit", "ActualCharge");
            DropColumn("dbo.MPS_MemoTable", "IncomeDepositId");
            DropColumn("dbo.MPS_MemoTable", "ActualCharge");
        }
    }
}
