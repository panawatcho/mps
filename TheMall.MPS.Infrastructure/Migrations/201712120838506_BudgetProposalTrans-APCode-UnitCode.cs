namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BudgetProposalTransAPCodeUnitCode : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MPS_BudgetProposalTrans", "APCode", c => c.String(maxLength: 20));
            AddColumn("dbo.MPS_BudgetProposalTrans", "UnitCode", c => c.String(maxLength: 20));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MPS_BudgetProposalTrans", "UnitCode");
            DropColumn("dbo.MPS_BudgetProposalTrans", "APCode");
        }
    }
}
