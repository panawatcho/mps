namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Table_GoodsReceiptTable_GoodsReceiptLine : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MPS_GoodsReceiptLine",
                c => new
                    {
                        RECEIPT_ID = c.Int(nullable: false),
                        ITEM_NUM = c.Int(nullable: false),
                        LOCATION = c.String(maxLength: 200),
                        QUANTITY = c.Decimal(precision: 18, scale: 2),
                        DELIV_CMPL = c.String(maxLength: 200),
                        REASON = c.String(maxLength: 1000),
                        ITEM_TEXT = c.String(maxLength: 1000),
                        MOVEMENTTYPE = c.String(maxLength: 100),
                        ACTUALPRICE = c.Decimal(nullable: false, precision: 18, scale: 2),
                        REFERNUM = c.Int(),
                        StatusFlag = c.Int(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                        DeletedDate = c.DateTime(),
                        DeletedBy = c.String(maxLength: 100),
                        DeletedByName = c.String(maxLength: 200),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => new { t.RECEIPT_ID, t.ITEM_NUM })
                .ForeignKey("dbo.MPS_GoodsReceiptTable", t => t.RECEIPT_ID, cascadeDelete: true)
                .Index(t => t.RECEIPT_ID);
            
            CreateTable(
                "dbo.MPS_GoodsReceiptTable",
                c => new
                    {
                        RECEIPT_ID = c.Int(nullable: false),
                        PO_ID = c.Int(nullable: false),
                        PO_NUM = c.String(maxLength: 200),
                        DOC_DATE = c.DateTime(),
                        POST_DATE = c.DateTime(),
                        DELIV_NOTE = c.String(maxLength: 500),
                        HEADER_TXT = c.String(maxLength: 500),
                        BILL_LAD = c.String(maxLength: 200),
                        SLIP_NUM = c.String(maxLength: 200),
                        Status = c.String(maxLength: 200),
                        StatusFlag = c.Int(nullable: false),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.RECEIPT_ID)
                .ForeignKey("dbo.MPS_PurchOrderTable", t => t.PO_ID, cascadeDelete: true)
                .Index(t => t.PO_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MPS_GoodsReceiptTable", "PO_ID", "dbo.MPS_PurchOrderTable");
            DropForeignKey("dbo.MPS_GoodsReceiptLine", "RECEIPT_ID", "dbo.MPS_GoodsReceiptTable");
            DropIndex("dbo.MPS_GoodsReceiptTable", new[] { "PO_ID" });
            DropIndex("dbo.MPS_GoodsReceiptLine", new[] { "RECEIPT_ID" });
            DropTable("dbo.MPS_GoodsReceiptTable");
            DropTable("dbo.MPS_GoodsReceiptLine");
        }
    }
}
