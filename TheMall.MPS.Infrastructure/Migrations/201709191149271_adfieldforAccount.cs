namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class adfieldforAccount : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MPS_IncomeOther", "Actual", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.MPS_IncomeOther", "PostActual", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MPS_IncomeOther", "PostActual");
            DropColumn("dbo.MPS_IncomeOther", "Actual");
        }
    }
}
