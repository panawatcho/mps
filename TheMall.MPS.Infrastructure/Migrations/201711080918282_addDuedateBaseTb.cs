namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addDuedateBaseTb : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MPS_CashAdvanceTable", "DueDatePropose", c => c.DateTime());
            AddColumn("dbo.MPS_CashAdvanceTable", "DueDateAccept", c => c.DateTime());
            AddColumn("dbo.MPS_CashAdvanceTable", "DueDateApprove", c => c.DateTime());
            AddColumn("dbo.MPS_CashAdvanceTable", "DueDateFinalApprove", c => c.DateTime());
            AddColumn("dbo.MPS_MemoTable", "DueDatePropose", c => c.DateTime());
            AddColumn("dbo.MPS_MemoTable", "DueDateAccept", c => c.DateTime());
            AddColumn("dbo.MPS_MemoTable", "DueDateApprove", c => c.DateTime());
            AddColumn("dbo.MPS_MemoTable", "DueDateFinalApprove", c => c.DateTime());
            AddColumn("dbo.MPS_PettyCashTable", "DueDatePropose", c => c.DateTime());
            AddColumn("dbo.MPS_PettyCashTable", "DueDateAccept", c => c.DateTime());
            AddColumn("dbo.MPS_PettyCashTable", "DueDateApprove", c => c.DateTime());
            AddColumn("dbo.MPS_PettyCashTable", "DueDateFinalApprove", c => c.DateTime());
            AddColumn("dbo.MPS_BudgetDepositTrans", "DocumentType", c => c.String(maxLength: 100));
            AddColumn("dbo.MPS_BudgetDepositTrans", "DocumentId", c => c.Int());
            AddColumn("dbo.MPS_BudgetDepositTrans", "DocumentNumber", c => c.String(maxLength: 20));
            AddColumn("dbo.MPS_CashClearingTable", "DueDatePropose", c => c.DateTime());
            AddColumn("dbo.MPS_CashClearingTable", "DueDateAccept", c => c.DateTime());
            AddColumn("dbo.MPS_CashClearingTable", "DueDateApprove", c => c.DateTime());
            AddColumn("dbo.MPS_CashClearingTable", "DueDateFinalApprove", c => c.DateTime());
            AddColumn("dbo.MPS_ProcurementTable", "DueDatePropose", c => c.DateTime());
            AddColumn("dbo.MPS_ProcurementTable", "DueDateAccept", c => c.DateTime());
            AddColumn("dbo.MPS_ProcurementTable", "DueDateApprove", c => c.DateTime());
            AddColumn("dbo.MPS_ProcurementTable", "DueDateFinalApprove", c => c.DateTime());
            DropColumn("dbo.MPS_BudgetDepositTrans", "IncomeId");
            DropColumn("dbo.MPS_BudgetDepositTrans", "DepositNumber");
        }
        
        public override void Down()
        {
            AddColumn("dbo.MPS_BudgetDepositTrans", "DepositNumber", c => c.String(maxLength: 20));
            AddColumn("dbo.MPS_BudgetDepositTrans", "IncomeId", c => c.Int());
            DropColumn("dbo.MPS_ProcurementTable", "DueDateFinalApprove");
            DropColumn("dbo.MPS_ProcurementTable", "DueDateApprove");
            DropColumn("dbo.MPS_ProcurementTable", "DueDateAccept");
            DropColumn("dbo.MPS_ProcurementTable", "DueDatePropose");
            DropColumn("dbo.MPS_CashClearingTable", "DueDateFinalApprove");
            DropColumn("dbo.MPS_CashClearingTable", "DueDateApprove");
            DropColumn("dbo.MPS_CashClearingTable", "DueDateAccept");
            DropColumn("dbo.MPS_CashClearingTable", "DueDatePropose");
            DropColumn("dbo.MPS_BudgetDepositTrans", "DocumentNumber");
            DropColumn("dbo.MPS_BudgetDepositTrans", "DocumentId");
            DropColumn("dbo.MPS_BudgetDepositTrans", "DocumentType");
            DropColumn("dbo.MPS_PettyCashTable", "DueDateFinalApprove");
            DropColumn("dbo.MPS_PettyCashTable", "DueDateApprove");
            DropColumn("dbo.MPS_PettyCashTable", "DueDateAccept");
            DropColumn("dbo.MPS_PettyCashTable", "DueDatePropose");
            DropColumn("dbo.MPS_MemoTable", "DueDateFinalApprove");
            DropColumn("dbo.MPS_MemoTable", "DueDateApprove");
            DropColumn("dbo.MPS_MemoTable", "DueDateAccept");
            DropColumn("dbo.MPS_MemoTable", "DueDatePropose");
            DropColumn("dbo.MPS_CashAdvanceTable", "DueDateFinalApprove");
            DropColumn("dbo.MPS_CashAdvanceTable", "DueDateApprove");
            DropColumn("dbo.MPS_CashAdvanceTable", "DueDateAccept");
            DropColumn("dbo.MPS_CashAdvanceTable", "DueDatePropose");
        }
    }
}
