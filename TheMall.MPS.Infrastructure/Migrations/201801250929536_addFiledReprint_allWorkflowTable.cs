namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addFiledReprint_allWorkflowTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MPS_CashAdvanceTable", "RePrintNo", c => c.Int());
            AddColumn("dbo.MPS_MemoTable", "RePrintNo", c => c.Int());
            AddColumn("dbo.MPS_ProposalTable", "RePrintNo", c => c.Int());
            AddColumn("dbo.MPS_PettyCashTable", "RePrintNo", c => c.Int());
            AddColumn("dbo.MPS_CashClearingTable", "RePrintNo", c => c.Int());
            AddColumn("dbo.MPS_Log_ProposalTable", "RePrintNo", c => c.Int());
            AddColumn("dbo.MPS_ProcurementTable", "RePrintNo", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.MPS_ProcurementTable", "RePrintNo");
            DropColumn("dbo.MPS_Log_ProposalTable", "RePrintNo");
            DropColumn("dbo.MPS_CashClearingTable", "RePrintNo");
            DropColumn("dbo.MPS_PettyCashTable", "RePrintNo");
            DropColumn("dbo.MPS_ProposalTable", "RePrintNo");
            DropColumn("dbo.MPS_MemoTable", "RePrintNo");
            DropColumn("dbo.MPS_CashAdvanceTable", "RePrintNo");
        }
    }
}
