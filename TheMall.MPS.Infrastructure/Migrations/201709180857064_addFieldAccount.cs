namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addFieldAccount : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MPS_CashAdvanceTable", "AccountingEmail", c => c.String(maxLength: 200));
            AddColumn("dbo.MPS_MemoTable", "AccountingEmail", c => c.String(maxLength: 200));
            AddColumn("dbo.MPS_PettyCashTable", "AccountingEmail", c => c.String(maxLength: 200));
            AddColumn("dbo.MPS_ProposalTable", "AccountingEmail", c => c.String(maxLength: 200));
            AddColumn("dbo.MPS_CashClearingTable", "AccountingEmail", c => c.String(maxLength: 200));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MPS_CashClearingTable", "AccountingEmail");
            DropColumn("dbo.MPS_ProposalTable", "AccountingEmail");
            DropColumn("dbo.MPS_PettyCashTable", "AccountingEmail");
            DropColumn("dbo.MPS_MemoTable", "AccountingEmail");
            DropColumn("dbo.MPS_CashAdvanceTable", "AccountingEmail");
        }
    }
}
