namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addfieldActualLastYearLog : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MPS_Log_IncomeOther", "ActualLastYear", c => c.Decimal(precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MPS_Log_IncomeOther", "ActualLastYear");
        }
    }
}
