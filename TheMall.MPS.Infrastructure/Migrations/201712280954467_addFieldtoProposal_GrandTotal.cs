namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addFieldtoProposal_GrandTotal : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MPS_ProposalTable", "GrandTotal", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.MPS_ProposalTable", "PercentGrandTotal", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.MPS_Log_ProposalTable", "GrandTotal", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.MPS_Log_ProposalTable", "PercentGrandTotal", c => c.Decimal(precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MPS_Log_ProposalTable", "PercentGrandTotal");
            DropColumn("dbo.MPS_Log_ProposalTable", "GrandTotal");
            DropColumn("dbo.MPS_ProposalTable", "PercentGrandTotal");
            DropColumn("dbo.MPS_ProposalTable", "GrandTotal");
        }
    }
}
