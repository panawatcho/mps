namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatedecimalbudgetproposaltransprpogr : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.MPS_BudgetProposalTrans", "Amount", c => c.Decimal(nullable: false, precision: 18, scale: 4));
            AlterColumn("dbo.MPS_GoodsReceiptLine", "QUANTITY", c => c.Decimal(precision: 18, scale: 4));
            AlterColumn("dbo.MPS_GoodsReceiptLine", "ACTUALPRICE", c => c.Decimal(nullable: false, precision: 18, scale: 4));
            AlterColumn("dbo.MPS_PurchOrderLine", "QUANTITY", c => c.Decimal(precision: 18, scale: 4));
            AlterColumn("dbo.MPS_PurchOrderLine", "PRICE", c => c.Decimal(precision: 18, scale: 4));
            AlterColumn("dbo.MPS_PurchOrderLine", "TAX_AMOUNT", c => c.Decimal(precision: 18, scale: 4));
            AlterColumn("dbo.MPS_PurchOrderLine", "ACTUALPRICE", c => c.Decimal(nullable: false, precision: 18, scale: 4));
            AlterColumn("dbo.MPS_PurchReqLine", "QUANTITY", c => c.Decimal(precision: 18, scale: 4));
            AlterColumn("dbo.MPS_PurchReqLine", "PRICE", c => c.Decimal(precision: 18, scale: 4));
            AlterColumn("dbo.MPS_PurchReqLine", "TAX_AMOUNT", c => c.Decimal(precision: 18, scale: 4));
            AlterColumn("dbo.MPS_PurchReqLine", "ACTUALPRICE", c => c.Decimal(nullable: false, precision: 18, scale: 4));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.MPS_PurchReqLine", "ACTUALPRICE", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.MPS_PurchReqLine", "TAX_AMOUNT", c => c.Decimal(precision: 18, scale: 2));
            AlterColumn("dbo.MPS_PurchReqLine", "PRICE", c => c.Decimal(precision: 18, scale: 2));
            AlterColumn("dbo.MPS_PurchReqLine", "QUANTITY", c => c.Decimal(precision: 18, scale: 2));
            AlterColumn("dbo.MPS_PurchOrderLine", "ACTUALPRICE", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.MPS_PurchOrderLine", "TAX_AMOUNT", c => c.Decimal(precision: 18, scale: 2));
            AlterColumn("dbo.MPS_PurchOrderLine", "PRICE", c => c.Decimal(precision: 18, scale: 2));
            AlterColumn("dbo.MPS_PurchOrderLine", "QUANTITY", c => c.Decimal(precision: 18, scale: 2));
            AlterColumn("dbo.MPS_GoodsReceiptLine", "ACTUALPRICE", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.MPS_GoodsReceiptLine", "QUANTITY", c => c.Decimal(precision: 18, scale: 2));
            AlterColumn("dbo.MPS_BudgetProposalTrans", "Amount", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
    }
}
