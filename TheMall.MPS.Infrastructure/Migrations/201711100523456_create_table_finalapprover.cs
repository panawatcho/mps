namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class create_table_finalapprover : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MPS_M_FinalApprover",
                c => new
                    {
                        ProcessCode = c.String(nullable: false, maxLength: 50),
                        UnitCode = c.String(nullable: false, maxLength: 20),
                        Username = c.String(nullable: false, maxLength: 100),
                        Amount = c.Decimal(precision: 18, scale: 2),
                        InActive = c.Boolean(nullable: false),
                        EmpId = c.String(maxLength: 10),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => new { t.ProcessCode, t.UnitCode, t.Username })
                .ForeignKey("dbo.MPS_M_EmployeeTable", t => t.EmpId)
                .ForeignKey("dbo.MPS_M_ProcessApprove", t => t.ProcessCode, cascadeDelete: true)
                .Index(t => t.ProcessCode)
                .Index(t => t.EmpId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MPS_M_FinalApprover", "ProcessCode", "dbo.MPS_M_ProcessApprove");
            DropForeignKey("dbo.MPS_M_FinalApprover", "EmpId", "dbo.MPS_M_EmployeeTable");
            DropIndex("dbo.MPS_M_FinalApprover", new[] { "EmpId" });
            DropIndex("dbo.MPS_M_FinalApprover", new[] { "ProcessCode" });
            DropTable("dbo.MPS_M_FinalApprover");
        }
    }
}
