namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProposalLineDescriptionfieldlength : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.MPS_ProposalLine", "Description", c => c.String(maxLength: 255));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.MPS_ProposalLine", "Description", c => c.String());
        }
    }
}
