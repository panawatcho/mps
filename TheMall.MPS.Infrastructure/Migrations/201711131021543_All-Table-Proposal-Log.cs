namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AllTableProposalLog : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MPS_Log_AccountTrans",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AccountTransId = c.Int(),
                        Actual = c.Decimal(precision: 18, scale: 2),
                        ActualDate = c.DateTime(),
                        Remark = c.String(),
                        IncomeOtherId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MPS_AccountTrans", t => t.AccountTransId)
                .ForeignKey("dbo.MPS_Log_IncomeOther", t => t.IncomeOtherId, cascadeDelete: true)
                .Index(t => t.AccountTransId)
                .Index(t => t.IncomeOtherId);
            
            CreateTable(
                "dbo.MPS_Log_IncomeOther",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IncomeOtherId = c.Int(),
                        SponcorName = c.String(maxLength: 200),
                        Description = c.String(),
                        Budget = c.Decimal(nullable: false, precision: 18, scale: 2),
                        DueDate = c.DateTime(nullable: false),
                        ContactName = c.String(maxLength: 200),
                        ContactTel = c.String(maxLength: 50),
                        ContactEmail = c.String(maxLength: 50),
                        ContactDepartment = c.String(maxLength: 100),
                        Remark = c.String(),
                        Actual = c.Decimal(precision: 18, scale: 2),
                        PostActual = c.Boolean(nullable: false),
                        StatusFlag = c.Int(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                        DeletedDate = c.DateTime(),
                        DeletedBy = c.String(),
                        DeletedByName = c.String(),
                        LineNo = c.Single(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        ParentId = c.Int(nullable: false),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MPS_IncomeOther", t => t.IncomeOtherId)
                .ForeignKey("dbo.MPS_Log_ProposalTable", t => t.ParentId, cascadeDelete: true)
                .Index(t => t.IncomeOtherId)
                .Index(t => t.ParentId);
            
            CreateTable(
                "dbo.MPS_Log_ProposalTable",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProposalId = c.Int(nullable: false),
                        ProposalTypeCode = c.String(maxLength: 10),
                        ProposalTypeName = c.String(maxLength: 100),
                        Place = c.String(),
                        Source = c.String(),
                        Other = c.String(maxLength: 100),
                        DepositId = c.Int(nullable: false),
                        DepositNumber = c.String(),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        ProjectStartDate = c.DateTime(),
                        ProjectEndDate = c.DateTime(),
                        Budget = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CustomersTargetGroup = c.String(),
                        CheckBudget = c.Boolean(nullable: false),
                        TransferToNextYear = c.Boolean(nullable: false),
                        Objective = c.String(),
                        TR = c.Boolean(nullable: false),
                        RE = c.Boolean(nullable: false),
                        CloseFlag = c.Boolean(nullable: false),
                        DepartmentException = c.String(maxLength: 200),
                        TotalBudget = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TotalIncomeDeposit = c.Decimal(precision: 18, scale: 2),
                        TotalIncomeOther = c.Decimal(precision: 18, scale: 2),
                        TotalMarketingExpense = c.Decimal(precision: 18, scale: 2),
                        TotalSaleTarget = c.Decimal(precision: 18, scale: 2),
                        TotalAdvertising = c.Decimal(precision: 18, scale: 2),
                        TotalPromotion = c.Decimal(precision: 18, scale: 2),
                        PecentAdvertising = c.Decimal(precision: 18, scale: 2),
                        PecentPromotion = c.Decimal(precision: 18, scale: 2),
                        PecentMarketingExpense = c.Decimal(precision: 18, scale: 2),
                        UnitCode = c.String(maxLength: 20),
                        UnitName = c.String(maxLength: 1000),
                        AccountingUsername = c.String(),
                        AccountingEmail = c.String(),
                        InformUsername = c.String(),
                        InformEmail = c.String(),
                        CCUsername = c.String(),
                        CCEmail = c.String(),
                        DueDatePropose = c.DateTime(),
                        DueDateAccept = c.DateTime(),
                        DueDateApprove = c.DateTime(),
                        DueDateFinalApprove = c.DateTime(),
                        Title = c.String(nullable: false, maxLength: 200),
                        DocumentNumber = c.String(maxLength: 20),
                        Revision = c.Int(),
                        RequesterUserName = c.String(nullable: false, maxLength: 100),
                        RequesterName = c.String(nullable: false, maxLength: 200),
                        SubmittedDate = c.DateTime(),
                        Status = c.String(maxLength: 100),
                        DocumentStatus = c.String(maxLength: 100),
                        StatusFlag = c.Int(nullable: false),
                        ProcInstId = c.Int(),
                        ExpectedDuration = c.Int(),
                        DocumentDate = c.DateTime(),
                        CompletedDate = c.DateTime(),
                        CancelledDate = c.DateTime(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        Deleted = c.Boolean(nullable: false),
                        DeletedDate = c.DateTime(),
                        DeletedBy = c.String(maxLength: 100),
                        DeletedByName = c.String(maxLength: 200),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MPS_ProposalTable", t => t.ProposalId, cascadeDelete: true)
                .ForeignKey("dbo.MPS_M_UnitCode", t => t.UnitCode)
                .Index(t => t.ProposalId)
                .Index(t => t.UnitCode)
                .Index(t => new { t.DocumentNumber, t.Revision }, name: "DocumentNumber_Revision");
            
            CreateTable(
                "dbo.MPS_Log_DepositLine",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DepositLineId = c.Int(),
                        PaymentNo = c.String(maxLength: 50),
                        DatePeriod = c.String(maxLength: 50),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ProcessStatus = c.String(maxLength: 100),
                        StatusFlag = c.Int(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                        DeletedDate = c.DateTime(),
                        DeletedBy = c.String(),
                        DeletedByName = c.String(),
                        LineNo = c.Single(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        ParentId = c.Int(nullable: false),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MPS_DepositLine", t => t.DepositLineId)
                .ForeignKey("dbo.MPS_Log_ProposalTable", t => t.ParentId, cascadeDelete: true)
                .Index(t => t.DepositLineId)
                .Index(t => t.ParentId);
            
            CreateTable(
                "dbo.MPS_Log_IncomeDeposit",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IncomeDepositId = c.Int(),
                        ProposalDepositRefID = c.Int(nullable: false),
                        ProposalDepositRefDoc = c.String(maxLength: 50),
                        ProposalDepositRefTitle = c.String(maxLength: 200),
                        Budget = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Remark = c.String(),
                        DepositApprover = c.String(),
                        ProcInstId = c.Int(),
                        Status = c.String(maxLength: 100),
                        StatusFlag = c.Int(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                        DeletedDate = c.DateTime(),
                        DeletedBy = c.String(),
                        DeletedByName = c.String(),
                        LineNo = c.Single(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        ParentId = c.Int(nullable: false),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MPS_IncomeDeposit", t => t.IncomeDepositId)
                .ForeignKey("dbo.MPS_Log_ProposalTable", t => t.ParentId, cascadeDelete: true)
                .Index(t => t.IncomeDepositId)
                .Index(t => t.ParentId);
            
            CreateTable(
                "dbo.MPS_Log_IncomeTotalSale",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IncomeTotalSaleId = c.Int(),
                        Description = c.String(),
                        Budget = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Remark = c.String(),
                        StatusFlag = c.Int(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                        DeletedDate = c.DateTime(),
                        DeletedBy = c.String(),
                        DeletedByName = c.String(),
                        LineNo = c.Single(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        ParentId = c.Int(nullable: false),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MPS_IncomeTotalSale", t => t.IncomeTotalSaleId)
                .ForeignKey("dbo.MPS_Log_ProposalTable", t => t.ParentId, cascadeDelete: true)
                .Index(t => t.IncomeTotalSaleId)
                .Index(t => t.ParentId);
            
            CreateTable(
                "dbo.MPS_Log_IncomeTotalSaleTemplate",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IncomeTotalSaleTemplateId = c.Int(),
                        BranchCode = c.String(maxLength: 100),
                        IncomeTotalSaleId = c.Int(nullable: false),
                        PercentOfDepartment = c.Decimal(precision: 18, scale: 2),
                        Actual = c.Decimal(precision: 18, scale: 2),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MPS_Log_IncomeTotalSale", t => t.IncomeTotalSaleId, cascadeDelete: true)
                .ForeignKey("dbo.MPS_IncomeTotalSaleTemplate", t => t.IncomeTotalSaleTemplateId)
                .Index(t => t.IncomeTotalSaleTemplateId)
                .Index(t => t.IncomeTotalSaleId);
            
            CreateTable(
                "dbo.MPS_Log_ProposalApproval",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProposalApprovalId = c.Int(),
                        ApproverSequence = c.Int(nullable: false),
                        ApproverLevel = c.String(maxLength: 50),
                        ApproverUserName = c.String(maxLength: 50),
                        ApproverFullName = c.String(maxLength: 100),
                        ApproverEmail = c.String(),
                        Position = c.String(maxLength: 100),
                        Department = c.String(maxLength: 100),
                        DueDate = c.DateTime(),
                        LockEditable = c.Boolean(nullable: false),
                        ApproveStatus = c.Boolean(nullable: false),
                        StatusFlag = c.Int(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                        DeletedDate = c.DateTime(),
                        DeletedBy = c.String(),
                        DeletedByName = c.String(),
                        LineNo = c.Single(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        ParentId = c.Int(nullable: false),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MPS_ProposalApproval", t => t.ProposalApprovalId)
                .ForeignKey("dbo.MPS_Log_ProposalTable", t => t.ParentId, cascadeDelete: true)
                .Index(t => t.ProposalApprovalId)
                .Index(t => t.ParentId);
            
            CreateTable(
                "dbo.MPS_Log_ProposalLine",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProposalLineId = c.Int(),
                        ExpenseTopicCode = c.String(maxLength: 20),
                        APCode = c.String(maxLength: 20),
                        Description = c.String(),
                        BudgetPlan = c.Decimal(nullable: false, precision: 18, scale: 2),
                        PreviousActual = c.Decimal(precision: 18, scale: 2),
                        Actual = c.Decimal(precision: 18, scale: 2),
                        Remark = c.String(),
                        UnitCode = c.String(maxLength: 20),
                        UnitName = c.String(maxLength: 1000),
                        CheckBudget = c.Boolean(nullable: false),
                        StatusFlag = c.Int(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                        DeletedDate = c.DateTime(),
                        DeletedBy = c.String(),
                        DeletedByName = c.String(),
                        LineNo = c.Single(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        ParentId = c.Int(nullable: false),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MPS_ProposalLine", t => t.ProposalLineId)
                .ForeignKey("dbo.MPS_Log_ProposalTable", t => t.ParentId, cascadeDelete: true)
                .Index(t => t.ProposalLineId)
                .Index(t => t.ParentId);
            
            CreateTable(
                "dbo.MPS_Log_ProposalLineTemplate",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProposalLineTemplateId = c.Int(),
                        BranchCode = c.String(maxLength: 100),
                        ProposalLineId = c.Int(nullable: false),
                        PercentOfDepartment = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TR_Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TR_Percent = c.Decimal(nullable: false, precision: 18, scale: 2),
                        RE_Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        RE_Percent = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MPS_Log_ProposalLine", t => t.ProposalLineId, cascadeDelete: true)
                .ForeignKey("dbo.MPS_ProposalLineTemplate", t => t.ProposalLineTemplateId)
                .Index(t => t.ProposalLineTemplateId)
                .Index(t => t.ProposalLineId);
            
            CreateTable(
                "dbo.MPS_Log_ProposalLineSharedTemplate",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProposalLineSharedTemplateId = c.Int(),
                        ParentId = c.Int(nullable: false),
                        BranchCode = c.String(maxLength: 100),
                        Selected = c.Boolean(nullable: false),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MPS_ProposalLineSharedTemplate", t => t.ProposalLineSharedTemplateId)
                .ForeignKey("dbo.MPS_Log_ProposalTable", t => t.ParentId, cascadeDelete: true)
                .Index(t => t.ProposalLineSharedTemplateId)
                .Index(t => t.ParentId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MPS_Log_ProposalTable", "UnitCode", "dbo.MPS_M_UnitCode");
            DropForeignKey("dbo.MPS_Log_ProposalLineSharedTemplate", "ParentId", "dbo.MPS_Log_ProposalTable");
            DropForeignKey("dbo.MPS_Log_ProposalLineSharedTemplate", "ProposalLineSharedTemplateId", "dbo.MPS_ProposalLineSharedTemplate");
            DropForeignKey("dbo.MPS_Log_ProposalTable", "ProposalId", "dbo.MPS_ProposalTable");
            DropForeignKey("dbo.MPS_Log_ProposalLine", "ParentId", "dbo.MPS_Log_ProposalTable");
            DropForeignKey("dbo.MPS_Log_ProposalLineTemplate", "ProposalLineTemplateId", "dbo.MPS_ProposalLineTemplate");
            DropForeignKey("dbo.MPS_Log_ProposalLineTemplate", "ProposalLineId", "dbo.MPS_Log_ProposalLine");
            DropForeignKey("dbo.MPS_Log_ProposalLine", "ProposalLineId", "dbo.MPS_ProposalLine");
            DropForeignKey("dbo.MPS_Log_ProposalApproval", "ParentId", "dbo.MPS_Log_ProposalTable");
            DropForeignKey("dbo.MPS_Log_ProposalApproval", "ProposalApprovalId", "dbo.MPS_ProposalApproval");
            DropForeignKey("dbo.MPS_Log_IncomeTotalSale", "ParentId", "dbo.MPS_Log_ProposalTable");
            DropForeignKey("dbo.MPS_Log_IncomeTotalSaleTemplate", "IncomeTotalSaleTemplateId", "dbo.MPS_IncomeTotalSaleTemplate");
            DropForeignKey("dbo.MPS_Log_IncomeTotalSaleTemplate", "IncomeTotalSaleId", "dbo.MPS_Log_IncomeTotalSale");
            DropForeignKey("dbo.MPS_Log_IncomeTotalSale", "IncomeTotalSaleId", "dbo.MPS_IncomeTotalSale");
            DropForeignKey("dbo.MPS_Log_IncomeOther", "ParentId", "dbo.MPS_Log_ProposalTable");
            DropForeignKey("dbo.MPS_Log_IncomeDeposit", "ParentId", "dbo.MPS_Log_ProposalTable");
            DropForeignKey("dbo.MPS_Log_IncomeDeposit", "IncomeDepositId", "dbo.MPS_IncomeDeposit");
            DropForeignKey("dbo.MPS_Log_DepositLine", "ParentId", "dbo.MPS_Log_ProposalTable");
            DropForeignKey("dbo.MPS_Log_DepositLine", "DepositLineId", "dbo.MPS_DepositLine");
            DropForeignKey("dbo.MPS_Log_IncomeOther", "IncomeOtherId", "dbo.MPS_IncomeOther");
            DropForeignKey("dbo.MPS_Log_AccountTrans", "IncomeOtherId", "dbo.MPS_Log_IncomeOther");
            DropForeignKey("dbo.MPS_Log_AccountTrans", "AccountTransId", "dbo.MPS_AccountTrans");
            DropIndex("dbo.MPS_Log_ProposalLineSharedTemplate", new[] { "ParentId" });
            DropIndex("dbo.MPS_Log_ProposalLineSharedTemplate", new[] { "ProposalLineSharedTemplateId" });
            DropIndex("dbo.MPS_Log_ProposalLineTemplate", new[] { "ProposalLineId" });
            DropIndex("dbo.MPS_Log_ProposalLineTemplate", new[] { "ProposalLineTemplateId" });
            DropIndex("dbo.MPS_Log_ProposalLine", new[] { "ParentId" });
            DropIndex("dbo.MPS_Log_ProposalLine", new[] { "ProposalLineId" });
            DropIndex("dbo.MPS_Log_ProposalApproval", new[] { "ParentId" });
            DropIndex("dbo.MPS_Log_ProposalApproval", new[] { "ProposalApprovalId" });
            DropIndex("dbo.MPS_Log_IncomeTotalSaleTemplate", new[] { "IncomeTotalSaleId" });
            DropIndex("dbo.MPS_Log_IncomeTotalSaleTemplate", new[] { "IncomeTotalSaleTemplateId" });
            DropIndex("dbo.MPS_Log_IncomeTotalSale", new[] { "ParentId" });
            DropIndex("dbo.MPS_Log_IncomeTotalSale", new[] { "IncomeTotalSaleId" });
            DropIndex("dbo.MPS_Log_IncomeDeposit", new[] { "ParentId" });
            DropIndex("dbo.MPS_Log_IncomeDeposit", new[] { "IncomeDepositId" });
            DropIndex("dbo.MPS_Log_DepositLine", new[] { "ParentId" });
            DropIndex("dbo.MPS_Log_DepositLine", new[] { "DepositLineId" });
            DropIndex("dbo.MPS_Log_ProposalTable", "DocumentNumber_Revision");
            DropIndex("dbo.MPS_Log_ProposalTable", new[] { "UnitCode" });
            DropIndex("dbo.MPS_Log_ProposalTable", new[] { "ProposalId" });
            DropIndex("dbo.MPS_Log_IncomeOther", new[] { "ParentId" });
            DropIndex("dbo.MPS_Log_IncomeOther", new[] { "IncomeOtherId" });
            DropIndex("dbo.MPS_Log_AccountTrans", new[] { "IncomeOtherId" });
            DropIndex("dbo.MPS_Log_AccountTrans", new[] { "AccountTransId" });
            DropTable("dbo.MPS_Log_ProposalLineSharedTemplate");
            DropTable("dbo.MPS_Log_ProposalLineTemplate");
            DropTable("dbo.MPS_Log_ProposalLine");
            DropTable("dbo.MPS_Log_ProposalApproval");
            DropTable("dbo.MPS_Log_IncomeTotalSaleTemplate");
            DropTable("dbo.MPS_Log_IncomeTotalSale");
            DropTable("dbo.MPS_Log_IncomeDeposit");
            DropTable("dbo.MPS_Log_DepositLine");
            DropTable("dbo.MPS_Log_ProposalTable");
            DropTable("dbo.MPS_Log_IncomeOther");
            DropTable("dbo.MPS_Log_AccountTrans");
        }
    }
}
