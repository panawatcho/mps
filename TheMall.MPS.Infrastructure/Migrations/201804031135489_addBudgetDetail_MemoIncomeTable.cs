namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addBudgetDetail_MemoIncomeTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MPS_MemoIncomeTable", "BudgetDetail", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MPS_MemoIncomeTable", "BudgetDetail");
        }
    }
}
