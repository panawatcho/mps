namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addfieldActualLastYearTotalSales : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MPS_IncomeTotalSale", "ActualLastYear", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.MPS_Log_IncomeTotalSale", "ActualLastYear", c => c.Decimal(precision: 18, scale: 2));
            DropColumn("dbo.MPS_IncomeOther", "ActualLastYear");
            DropColumn("dbo.MPS_Log_IncomeOther", "ActualLastYear");
        }
        
        public override void Down()
        {
            AddColumn("dbo.MPS_Log_IncomeOther", "ActualLastYear", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.MPS_IncomeOther", "ActualLastYear", c => c.Decimal(precision: 18, scale: 2));
            DropColumn("dbo.MPS_Log_IncomeTotalSale", "ActualLastYear");
            DropColumn("dbo.MPS_IncomeTotalSale", "ActualLastYear");
        }
    }
}
