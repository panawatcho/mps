// <auto-generated />
namespace TheMall.MPS.Infrastructure.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class addFieldDescriptionToPlace : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(addFieldDescriptionToPlace));
        
        string IMigrationMetadata.Id
        {
            get { return "201712040849393_addFieldDescriptionToPlace"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
