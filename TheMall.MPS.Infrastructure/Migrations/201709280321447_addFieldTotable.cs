namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addFieldTotable : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.MPS_M_BudgetYearPlan");
            AddColumn("dbo.MPS_CashAdvanceLine", "NetAmount", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.MPS_PettyCashLine", "NetAmount", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.MPS_CashClearingLine", "NetAmount", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.MPS_ProcuirementTable", "PR_Number", c => c.String(maxLength: 50));
            AddColumn("dbo.MPS_ProcuirementTable", "PO_Number", c => c.String(maxLength: 50));
            AddColumn("dbo.MPS_ProcuirementTable", "PR_Amount", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.MPS_ProcuirementTable", "PO_Amount", c => c.Decimal(precision: 18, scale: 2));
            AlterColumn("dbo.MPS_M_BudgetYearPlan", "BudgetYearID", c => c.String(nullable: false, maxLength: 15));
            AddPrimaryKey("dbo.MPS_M_BudgetYearPlan", "BudgetYearID");
            DropColumn("dbo.MPS_ProcuirementTable", "BudgetDetail");
        }
        
        public override void Down()
        {
            AddColumn("dbo.MPS_ProcuirementTable", "BudgetDetail", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            DropPrimaryKey("dbo.MPS_M_BudgetYearPlan");
            AlterColumn("dbo.MPS_M_BudgetYearPlan", "BudgetYearID", c => c.String(nullable: false, maxLength: 6));
            DropColumn("dbo.MPS_ProcuirementTable", "PO_Amount");
            DropColumn("dbo.MPS_ProcuirementTable", "PR_Amount");
            DropColumn("dbo.MPS_ProcuirementTable", "PO_Number");
            DropColumn("dbo.MPS_ProcuirementTable", "PR_Number");
            DropColumn("dbo.MPS_CashClearingLine", "NetAmount");
            DropColumn("dbo.MPS_PettyCashLine", "NetAmount");
            DropColumn("dbo.MPS_CashAdvanceLine", "NetAmount");
            AddPrimaryKey("dbo.MPS_M_BudgetYearPlan", "BudgetYearID");
        }
    }
}
