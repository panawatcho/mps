namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addAccountingBranch : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MPS_ProposalTable", "AccoutingBranch", c => c.String(maxLength: 100));
            AddColumn("dbo.MPS_CashAdvanceTable", "AccoutingBranch", c => c.String(maxLength: 100));
            AddColumn("dbo.MPS_MemoTable", "AccoutingBranch", c => c.String(maxLength: 100));
            AddColumn("dbo.MPS_PettyCashTable", "AccoutingBranch", c => c.String(maxLength: 100));
            AddColumn("dbo.MPS_CashClearingTable", "AccoutingBranch", c => c.String(maxLength: 100));
            AddColumn("dbo.MPS_Log_ProposalTable", "AccoutingBranch", c => c.String(maxLength: 100));
            AddColumn("dbo.MPS_ProcurementTable", "AccoutingBranch", c => c.String(maxLength: 100));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MPS_ProcurementTable", "AccoutingBranch");
            DropColumn("dbo.MPS_Log_ProposalTable", "AccoutingBranch");
            DropColumn("dbo.MPS_CashClearingTable", "AccoutingBranch");
            DropColumn("dbo.MPS_PettyCashTable", "AccoutingBranch");
            DropColumn("dbo.MPS_MemoTable", "AccoutingBranch");
            DropColumn("dbo.MPS_CashAdvanceTable", "AccoutingBranch");
            DropColumn("dbo.MPS_ProposalTable", "AccoutingBranch");
        }
    }
}
