namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addFieldToGoodsIssue : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MPS_PettyCashTable", "GoodsIssue", c => c.Boolean(nullable: false));
            AddColumn("dbo.MPS_CashClearingLine", "PayeeName", c => c.String());
            AddColumn("dbo.MPS_M_EmployeeTable", "BranchCode", c => c.String());
            AddColumn("dbo.MPS_M_EmployeeTable", "BranchName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.MPS_M_EmployeeTable", "BranchName");
            DropColumn("dbo.MPS_M_EmployeeTable", "BranchCode");
            DropColumn("dbo.MPS_CashClearingLine", "PayeeName");
            DropColumn("dbo.MPS_PettyCashTable", "GoodsIssue");
        }
    }
}
