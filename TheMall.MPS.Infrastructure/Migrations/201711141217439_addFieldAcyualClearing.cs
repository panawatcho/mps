namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addFieldAcyualClearing : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MPS_AccountTrans", "Source", c => c.String());
            AddColumn("dbo.MPS_CashClearingTable", "Actual", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.MPS_Log_AccountTrans", "Source", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.MPS_Log_AccountTrans", "Source");
            DropColumn("dbo.MPS_CashClearingTable", "Actual");
            DropColumn("dbo.MPS_AccountTrans", "Source");
        }
    }
}
