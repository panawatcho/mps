namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addTableReceiptandReceiptLine2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MPS_ReceiptLine",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                        Unit = c.Int(),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Remark = c.String(),
                        Vat = c.Decimal(precision: 18, scale: 2),
                        Tax = c.Decimal(precision: 18, scale: 2),
                        NetAmount = c.Decimal(precision: 18, scale: 2),
                        ReceiptTableId = c.Int(nullable: false),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MPS_ReceiptTable", t => t.ReceiptTableId, cascadeDelete: true)
                .Index(t => t.ReceiptTableId);
            
            CreateTable(
                "dbo.MPS_ReceiptTable",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RequesterUserName = c.String(nullable: false, maxLength: 100),
                        RequesterName = c.String(nullable: false, maxLength: 200),
                        UnitCode = c.String(maxLength: 20),
                        UnitName = c.String(maxLength: 1000),
                        RequestForUserName = c.String(maxLength: 100),
                        RequesterForName = c.String(maxLength: 200),
                        RePrintNo = c.Int(),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MPS_M_UnitCode", t => t.UnitCode)
                .Index(t => t.UnitCode);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MPS_ReceiptTable", "UnitCode", "dbo.MPS_M_UnitCode");
            DropForeignKey("dbo.MPS_ReceiptLine", "ReceiptTableId", "dbo.MPS_ReceiptTable");
            DropIndex("dbo.MPS_ReceiptTable", new[] { "UnitCode" });
            DropIndex("dbo.MPS_ReceiptLine", new[] { "ReceiptTableId" });
            DropTable("dbo.MPS_ReceiptTable");
            DropTable("dbo.MPS_ReceiptLine");
        }
    }
}
