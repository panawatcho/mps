namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class adfieldRemarkToDepsitLine : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MPS_DepositLine", "Remark", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.MPS_DepositLine", "Remark");
        }
    }
}
