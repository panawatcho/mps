namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addTable_MPS_M_Company : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MPS_M_Company",
                c => new
                    {
                        CompanyCode = c.String(nullable: false, maxLength: 128),
                        CompanyName = c.String(),
                        InActive = c.Boolean(nullable: false),
                        Order = c.Decimal(precision: 18, scale: 2),
                        Address = c.String(),
                        Description = c.String(),
                        TelephoneNumber = c.String(),
                        BranchCode = c.String(maxLength: 100),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.CompanyCode)
                .ForeignKey("dbo.MPS_M_SharedBranch", t => t.BranchCode)
                .Index(t => t.BranchCode);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MPS_M_Company", "BranchCode", "dbo.MPS_M_SharedBranch");
            DropIndex("dbo.MPS_M_Company", new[] { "BranchCode" });
            DropTable("dbo.MPS_M_Company");
        }
    }
}
