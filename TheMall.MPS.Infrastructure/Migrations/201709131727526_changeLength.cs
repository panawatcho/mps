namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changeLength : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MPS_EstimateTotalSale", "ExpenseTopicCode", c => c.String(maxLength: 20));
            AddColumn("dbo.MPS_EstimateTotalSale", "ExpenseTopicName", c => c.String(maxLength: 200));
            DropColumn("dbo.MPS_EstimateTotalSale", "MarketingGroup");
        }
        
        public override void Down()
        {
            AddColumn("dbo.MPS_EstimateTotalSale", "MarketingGroup", c => c.String(maxLength: 50));
            DropColumn("dbo.MPS_EstimateTotalSale", "ExpenseTopicName");
            DropColumn("dbo.MPS_EstimateTotalSale", "ExpenseTopicCode");
        }
    }
}
