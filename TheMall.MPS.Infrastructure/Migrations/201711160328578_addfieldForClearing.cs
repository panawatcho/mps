namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addfieldForClearing : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MPS_CashClearingLine", "VatActual", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.MPS_CashClearingLine", "TaxActual", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.MPS_CashClearingLine", "NetActual", c => c.Decimal(precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MPS_CashClearingLine", "NetActual");
            DropColumn("dbo.MPS_CashClearingLine", "TaxActual");
            DropColumn("dbo.MPS_CashClearingLine", "VatActual");
        }
    }
}
