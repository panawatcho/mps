namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addFieldAtBudgetTran : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MPS_BudgetYearTrans", "TypeYearPlan", c => c.String(maxLength: 50));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MPS_BudgetYearTrans", "TypeYearPlan");
        }
    }
}
