namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initialDB : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ApplicationSetting",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Environment = c.String(nullable: false, maxLength: 5),
                        Key = c.String(nullable: false, maxLength: 100),
                        Value = c.String(maxLength: 500),
                        Category = c.String(maxLength: 100),
                        Preload = c.Boolean(nullable: false),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => new { t.Environment, t.Key }, unique: true, name: "IX_EnvironmentKey");
            
            CreateTable(
                "dbo.DocumentType",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Name = c.String(maxLength: 100),
                        ProcessCode = c.String(maxLength: 50),
                        Activity = c.String(maxLength: 100),
                        IsRequired = c.Boolean(nullable: false),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Process", t => t.ProcessCode)
                .Index(t => t.ProcessCode);
            
            CreateTable(
                "dbo.Process",
                c => new
                    {
                        ProcessCode = c.String(nullable: false, maxLength: 50),
                        Name = c.String(maxLength: 100),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.ProcessCode);
            
            CreateTable(
                "dbo.MailContent",
                c => new
                    {
                        MailContentId = c.String(nullable: false, maxLength: 50),
                        Name = c.String(nullable: false, maxLength: 100),
                        Subject = c.String(nullable: false),
                        Body = c.String(),
                        MailTemplateId = c.String(nullable: false, maxLength: 50),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.MailContentId)
                .ForeignKey("dbo.MailTemplate", t => t.MailTemplateId, cascadeDelete: true)
                .Index(t => t.MailTemplateId);
            
            CreateTable(
                "dbo.MailTemplate",
                c => new
                    {
                        MailTemplateId = c.String(nullable: false, maxLength: 50),
                        Name = c.String(nullable: false, maxLength: 100),
                        TemplatePath = c.String(),
                        Template = c.String(),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.MailTemplateId);
            
            CreateTable(
                "dbo.MailRecipient",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProcessName = c.String(nullable: false, maxLength: 100),
                        MailType = c.Int(nullable: false),
                        ActivityName = c.String(nullable: false, maxLength: 300),
                        Email = c.String(maxLength: 100),
                        RecipientType = c.Int(nullable: false),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => new { t.ProcessName, t.MailType, t.ActivityName }, name: "IX_ProcessMailType");
            
            CreateTable(
                "dbo.MailSetup",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProcessName = c.String(maxLength: 100),
                        MailType = c.Int(nullable: false),
                        Extra = c.String(maxLength: 100),
                        MailContentId = c.String(nullable: false, maxLength: 50),
                        ActivityName = c.String(maxLength: 300),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MailContent", t => t.MailContentId, cascadeDelete: true)
                .Index(t => new { t.ProcessName, t.MailType, t.ActivityName, t.Extra }, unique: true, name: "IX_ProcessMailType")
                .Index(t => t.MailContentId);
            
            CreateTable(
                "dbo.MenuItem",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 256),
                        RefererId = c.String(maxLength: 500),
                        IconClass = c.String(maxLength: 100),
                        Text = c.String(maxLength: 200),
                        Html = c.String(maxLength: 500),
                        Inactive = c.Boolean(nullable: false),
                        Order = c.Double(nullable: false),
                        ParentId = c.String(maxLength: 256),
                        NewTab = c.Boolean(nullable: false),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MenuItem", t => t.ParentId)
                .ForeignKey("dbo.Referer", t => t.RefererId)
                .Index(t => t.RefererId)
                .Index(t => t.ParentId);
            
            CreateTable(
                "dbo.Referer",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 500),
                        Description = c.String(maxLength: 256),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.RefererRolePermission",
                c => new
                    {
                        RefererId = c.String(nullable: false, maxLength: 500),
                        RoleId = c.String(nullable: false, maxLength: 128),
                        Enable = c.Boolean(nullable: false),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => new { t.RefererId, t.RoleId })
                .ForeignKey("dbo.Referer", t => t.RefererId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.RefererId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Inactive = c.Boolean(nullable: false),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                        Inactive = c.Boolean(nullable: false),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        DisplayName = c.String(maxLength: 255),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.RefererUserPermission",
                c => new
                    {
                        RefererId = c.String(nullable: false, maxLength: 500),
                        UserId = c.String(nullable: false, maxLength: 128),
                        Enable = c.Boolean(nullable: false),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => new { t.RefererId, t.UserId })
                .ForeignKey("dbo.Referer", t => t.RefererId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.RefererId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.MPS_BudgetProposalTrans",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProposalId = c.Int(nullable: false),
                        ProposalLineId = c.Int(),
                        Status = c.String(maxLength: 100),
                        DocumentStatus = c.String(maxLength: 100),
                        DocumentType = c.String(maxLength: 100),
                        DocumentId = c.Int(),
                        DocumentNo = c.String(maxLength: 20),
                        RefDocumentType = c.String(maxLength: 100),
                        RefDocumentId = c.Int(),
                        RefDocumentNumber = c.String(maxLength: 20),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TransDate = c.DateTime(nullable: false),
                        InActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MPS_ProposalLine", t => t.ProposalLineId)
                .ForeignKey("dbo.MPS_ProposalTable", t => t.ProposalId, cascadeDelete: true)
                .Index(t => t.ProposalId)
                .Index(t => t.ProposalLineId);
            
            CreateTable(
                "dbo.MPS_ProposalLine",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ExpenseTopicCode = c.String(maxLength: 20),
                        APCode = c.String(maxLength: 20),
                        Description = c.String(),
                        BudgetPlan = c.Decimal(nullable: false, precision: 18, scale: 2),
                        PreviousActual = c.Decimal(precision: 18, scale: 2),
                        Actual = c.Decimal(precision: 18, scale: 2),
                        Remark = c.String(),
                        UnitCode = c.String(maxLength: 20),
                        UnitName = c.String(maxLength: 1000),
                        StatusFlag = c.Int(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                        DeletedDate = c.DateTime(),
                        DeletedBy = c.String(),
                        DeletedByName = c.String(),
                        LineNo = c.Single(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        ParentId = c.Int(nullable: false),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MPS_ProposalTable", t => t.ParentId, cascadeDelete: true)
                .Index(t => t.ParentId);
            
            CreateTable(
                "dbo.MPS_CashAdvanceTable",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ExpenseTopicCode = c.String(maxLength: 20),
                        APCode = c.String(maxLength: 20),
                        AdvanceDate = c.DateTime(),
                        AdvanceDueDate = c.DateTime(),
                        Branch = c.String(maxLength: 100),
                        Budget = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Description = c.String(),
                        RequestForEmpId = c.String(),
                        ProposalRefID = c.Int(nullable: false),
                        ProposalRefDocumentNumber = c.String(maxLength: 20),
                        RequestForUserName = c.String(maxLength: 100),
                        ProposalLineID = c.Int(nullable: false),
                        UnitCode = c.String(maxLength: 20),
                        UnitName = c.String(maxLength: 1000),
                        Title = c.String(nullable: false, maxLength: 200),
                        DocumentNumber = c.String(maxLength: 20),
                        Revision = c.Int(),
                        RequesterUserName = c.String(nullable: false, maxLength: 100),
                        RequesterName = c.String(nullable: false, maxLength: 200),
                        SubmittedDate = c.DateTime(),
                        Status = c.String(maxLength: 100),
                        DocumentStatus = c.String(maxLength: 100),
                        StatusFlag = c.Int(nullable: false),
                        ProcInstId = c.Int(),
                        ExpectedDuration = c.Int(),
                        DocumentDate = c.DateTime(),
                        CompletedDate = c.DateTime(),
                        CancelledDate = c.DateTime(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        Deleted = c.Boolean(nullable: false),
                        DeletedDate = c.DateTime(),
                        DeletedBy = c.String(maxLength: 100),
                        DeletedByName = c.String(maxLength: 200),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MPS_ProposalLine", t => t.ProposalLineID, cascadeDelete: true)
                .ForeignKey("dbo.MPS_M_UnitCode", t => t.UnitCode)
                .Index(t => t.ProposalLineID)
                .Index(t => t.UnitCode)
                .Index(t => new { t.DocumentNumber, t.Revision }, name: "DocumentNumber_Revision");
            
            CreateTable(
                "dbo.MPS_CashAdvanceAttachment",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ParentId = c.Int(nullable: false),
                        DocumentNumber = c.String(maxLength: 20),
                        ContentType = c.String(maxLength: 100),
                        ProcInstId = c.Int(),
                        SerialNumber = c.String(maxLength: 20),
                        Activity = c.String(maxLength: 100),
                        FileDescription = c.String(maxLength: 300),
                        FileName = c.String(maxLength: 300),
                        FileSize = c.Long(nullable: false),
                        FileSizeText = c.String(maxLength: 300),
                        DocumentLibrary = c.String(maxLength: 300),
                        Location = c.String(maxLength: 600),
                        DownloadUrl = c.String(maxLength: 600),
                        DocumentTypeId = c.Int(),
                        DocumentTypeName = c.String(maxLength: 100),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MPS_CashAdvanceTable", t => t.ParentId, cascadeDelete: true)
                .Index(t => t.ParentId);
            
            CreateTable(
                "dbo.MPS_CashAdvanceApproval",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ApproverSequence = c.Int(nullable: false),
                        ApproverLevel = c.String(maxLength: 50),
                        ApproverUserName = c.String(maxLength: 50),
                        ApproverFullName = c.String(maxLength: 100),
                        ApproverEmail = c.String(),
                        Position = c.String(maxLength: 100),
                        Department = c.String(maxLength: 100),
                        DueDate = c.DateTime(),
                        StatusFlag = c.Int(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                        DeletedDate = c.DateTime(),
                        DeletedBy = c.String(),
                        DeletedByName = c.String(),
                        LineNo = c.Single(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        ParentId = c.Int(nullable: false),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MPS_CashAdvanceTable", t => t.ParentId, cascadeDelete: true)
                .Index(t => t.ParentId);
            
            CreateTable(
                "dbo.MPS_CashAdvanceLine",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(maxLength: 100),
                        Unit = c.Int(),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Remark = c.String(),
                        StatusFlag = c.Int(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                        DeletedDate = c.DateTime(),
                        DeletedBy = c.String(),
                        DeletedByName = c.String(),
                        LineNo = c.Single(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        ParentId = c.Int(nullable: false),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MPS_CashAdvanceTable", t => t.ParentId, cascadeDelete: true)
                .Index(t => t.ParentId);
            
            CreateTable(
                "dbo.MPS_CashAdvanceComment",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ParentId = c.Int(nullable: false),
                        ProcInstId = c.Int(),
                        SerialNumber = c.String(maxLength: 20),
                        ProcessName = c.String(maxLength: 100),
                        Activity = c.String(maxLength: 100),
                        Action = c.String(maxLength: 100),
                        Comment = c.String(),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MPS_CashAdvanceTable", t => t.ParentId, cascadeDelete: true)
                .Index(t => t.ParentId);
            
            CreateTable(
                "dbo.MPS_M_UnitCode",
                c => new
                    {
                        UnitCode = c.String(nullable: false, maxLength: 20),
                        UnitName = c.String(maxLength: 1000),
                        Type = c.String(maxLength: 2),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.UnitCode);
            
            CreateTable(
                "dbo.MPS_MemoTable",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ExpenseTopicCode = c.String(maxLength: 20),
                        APCode = c.String(maxLength: 20),
                        BudgetAPCode = c.Decimal(precision: 18, scale: 2),
                        BudgetDetail = c.Decimal(precision: 18, scale: 2),
                        MemoType = c.String(maxLength: 50),
                        ProposalRefID = c.Int(nullable: false),
                        ProposalRefDocumentNumber = c.String(maxLength: 20),
                        UseProposalID = c.Int(),
                        UseProposalDocumentNumber = c.String(maxLength: 20),
                        PaymentName = c.String(maxLength: 100),
                        PaymentDepartment = c.String(),
                        Description = c.String(),
                        ProposalLineID = c.Int(nullable: false),
                        UnitCode = c.String(maxLength: 20),
                        UnitName = c.String(maxLength: 1000),
                        Title = c.String(nullable: false, maxLength: 200),
                        DocumentNumber = c.String(maxLength: 20),
                        Revision = c.Int(),
                        RequesterUserName = c.String(nullable: false, maxLength: 100),
                        RequesterName = c.String(nullable: false, maxLength: 200),
                        SubmittedDate = c.DateTime(),
                        Status = c.String(maxLength: 100),
                        DocumentStatus = c.String(maxLength: 100),
                        StatusFlag = c.Int(nullable: false),
                        ProcInstId = c.Int(),
                        ExpectedDuration = c.Int(),
                        DocumentDate = c.DateTime(),
                        CompletedDate = c.DateTime(),
                        CancelledDate = c.DateTime(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        Deleted = c.Boolean(nullable: false),
                        DeletedDate = c.DateTime(),
                        DeletedBy = c.String(maxLength: 100),
                        DeletedByName = c.String(maxLength: 200),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MPS_ProposalLine", t => t.ProposalLineID, cascadeDelete: true)
                .ForeignKey("dbo.MPS_M_UnitCode", t => t.UnitCode)
                .Index(t => t.ProposalLineID)
                .Index(t => t.UnitCode)
                .Index(t => new { t.DocumentNumber, t.Revision }, name: "DocumentNumber_Revision");
            
            CreateTable(
                "dbo.MPS_MemoAttachment",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ParentId = c.Int(nullable: false),
                        DocumentNumber = c.String(maxLength: 20),
                        ContentType = c.String(maxLength: 100),
                        ProcInstId = c.Int(),
                        SerialNumber = c.String(maxLength: 20),
                        Activity = c.String(maxLength: 100),
                        FileDescription = c.String(maxLength: 300),
                        FileName = c.String(maxLength: 300),
                        FileSize = c.Long(nullable: false),
                        FileSizeText = c.String(maxLength: 300),
                        DocumentLibrary = c.String(maxLength: 300),
                        Location = c.String(maxLength: 600),
                        DownloadUrl = c.String(maxLength: 600),
                        DocumentTypeId = c.Int(),
                        DocumentTypeName = c.String(maxLength: 100),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MPS_MemoTable", t => t.ParentId, cascadeDelete: true)
                .Index(t => t.ParentId);
            
            CreateTable(
                "dbo.MPS_MemoComment",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ParentId = c.Int(nullable: false),
                        ProcInstId = c.Int(),
                        SerialNumber = c.String(maxLength: 20),
                        ProcessName = c.String(maxLength: 100),
                        Activity = c.String(maxLength: 100),
                        Action = c.String(maxLength: 100),
                        Comment = c.String(),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MPS_MemoTable", t => t.ParentId, cascadeDelete: true)
                .Index(t => t.ParentId);
            
            CreateTable(
                "dbo.MPS_MemoApproval",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ApproverSequence = c.Int(nullable: false),
                        ApproverLevel = c.String(maxLength: 50),
                        ApproverUserName = c.String(maxLength: 50),
                        ApproverFullName = c.String(maxLength: 100),
                        ApproverEmail = c.String(),
                        Position = c.String(maxLength: 100),
                        Department = c.String(maxLength: 100),
                        DueDate = c.DateTime(),
                        StatusFlag = c.Int(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                        DeletedDate = c.DateTime(),
                        DeletedBy = c.String(),
                        DeletedByName = c.String(),
                        LineNo = c.Single(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        ParentId = c.Int(nullable: false),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MPS_MemoTable", t => t.ParentId, cascadeDelete: true)
                .Index(t => t.ParentId);
            
            CreateTable(
                "dbo.MPS_MemoLine",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(maxLength: 100),
                        Unit = c.Int(),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Vat = c.Decimal(precision: 18, scale: 2),
                        Tax = c.Decimal(precision: 18, scale: 2),
                        NetAmount = c.Decimal(precision: 18, scale: 2),
                        Remark = c.String(),
                        StatusFlag = c.Int(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                        DeletedDate = c.DateTime(),
                        DeletedBy = c.String(),
                        DeletedByName = c.String(),
                        LineNo = c.Single(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        ParentId = c.Int(nullable: false),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MPS_MemoTable", t => t.ParentId, cascadeDelete: true)
                .Index(t => t.ParentId);
            
            CreateTable(
                "dbo.MPS_PettyCashTable",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ExpenseTopicCode = c.String(maxLength: 20),
                        APCode = c.String(maxLength: 20),
                        PettyCashDate = c.DateTime(),
                        BudgetDetail = c.Decimal(nullable: false, precision: 18, scale: 2),
                        BudgetAPCode = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ProposalRefID = c.Int(nullable: false),
                        ProposalRefDocumentNumber = c.String(maxLength: 20),
                        Branch = c.String(maxLength: 100),
                        Description = c.String(maxLength: 100),
                        ProposalLineID = c.Int(nullable: false),
                        UnitCode = c.String(maxLength: 20),
                        UnitName = c.String(maxLength: 1000),
                        Title = c.String(nullable: false, maxLength: 200),
                        DocumentNumber = c.String(maxLength: 20),
                        Revision = c.Int(),
                        RequesterUserName = c.String(nullable: false, maxLength: 100),
                        RequesterName = c.String(nullable: false, maxLength: 200),
                        SubmittedDate = c.DateTime(),
                        Status = c.String(maxLength: 100),
                        DocumentStatus = c.String(maxLength: 100),
                        StatusFlag = c.Int(nullable: false),
                        ProcInstId = c.Int(),
                        ExpectedDuration = c.Int(),
                        DocumentDate = c.DateTime(),
                        CompletedDate = c.DateTime(),
                        CancelledDate = c.DateTime(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        Deleted = c.Boolean(nullable: false),
                        DeletedDate = c.DateTime(),
                        DeletedBy = c.String(maxLength: 100),
                        DeletedByName = c.String(maxLength: 200),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MPS_ProposalLine", t => t.ProposalLineID, cascadeDelete: true)
                .ForeignKey("dbo.MPS_M_UnitCode", t => t.UnitCode)
                .Index(t => t.ProposalLineID)
                .Index(t => t.UnitCode)
                .Index(t => new { t.DocumentNumber, t.Revision }, name: "DocumentNumber_Revision");
            
            CreateTable(
                "dbo.MPS_PettyCashAttachment",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ParentId = c.Int(nullable: false),
                        DocumentNumber = c.String(maxLength: 20),
                        ContentType = c.String(maxLength: 100),
                        ProcInstId = c.Int(),
                        SerialNumber = c.String(maxLength: 20),
                        Activity = c.String(maxLength: 100),
                        FileDescription = c.String(maxLength: 300),
                        FileName = c.String(maxLength: 300),
                        FileSize = c.Long(nullable: false),
                        FileSizeText = c.String(maxLength: 300),
                        DocumentLibrary = c.String(maxLength: 300),
                        Location = c.String(maxLength: 600),
                        DownloadUrl = c.String(maxLength: 600),
                        DocumentTypeId = c.Int(),
                        DocumentTypeName = c.String(maxLength: 100),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MPS_PettyCashTable", t => t.ParentId, cascadeDelete: true)
                .Index(t => t.ParentId);
            
            CreateTable(
                "dbo.MPS_PettyCashComment",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ParentId = c.Int(nullable: false),
                        ProcInstId = c.Int(),
                        SerialNumber = c.String(maxLength: 20),
                        ProcessName = c.String(maxLength: 100),
                        Activity = c.String(maxLength: 100),
                        Action = c.String(maxLength: 100),
                        Comment = c.String(),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MPS_PettyCashTable", t => t.ParentId, cascadeDelete: true)
                .Index(t => t.ParentId);
            
            CreateTable(
                "dbo.MPS_PettyCashApproval",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ApproverSequence = c.Int(nullable: false),
                        ApproverLevel = c.String(maxLength: 50),
                        ApproverUserName = c.String(maxLength: 50),
                        ApproverFullName = c.String(maxLength: 100),
                        ApproverEmail = c.String(),
                        Position = c.String(maxLength: 100),
                        Department = c.String(maxLength: 100),
                        DueDate = c.DateTime(),
                        StatusFlag = c.Int(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                        DeletedDate = c.DateTime(),
                        DeletedBy = c.String(),
                        DeletedByName = c.String(),
                        LineNo = c.Single(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        ParentId = c.Int(nullable: false),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MPS_PettyCashTable", t => t.ParentId, cascadeDelete: true)
                .Index(t => t.ParentId);
            
            CreateTable(
                "dbo.MPS_PettyCashLine",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(maxLength: 100),
                        Unit = c.Int(),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Remark = c.String(),
                        StatusFlag = c.Int(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                        DeletedDate = c.DateTime(),
                        DeletedBy = c.String(),
                        DeletedByName = c.String(),
                        LineNo = c.Single(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        ParentId = c.Int(nullable: false),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MPS_PettyCashTable", t => t.ParentId, cascadeDelete: true)
                .Index(t => t.ParentId);
            
            CreateTable(
                "dbo.MPS_ProposalLineTemplate",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        BranchCode = c.String(maxLength: 100),
                        ProposalLineId = c.Int(nullable: false),
                        PercentOfDepartment = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TR_Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TR_Percent = c.Decimal(nullable: false, precision: 18, scale: 2),
                        RE_Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        RE_Percent = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MPS_ProposalLine", t => t.ProposalLineId, cascadeDelete: true)
                .ForeignKey("dbo.MPS_M_SharedBranch", t => t.BranchCode)
                .Index(t => t.BranchCode)
                .Index(t => t.ProposalLineId);
            
            CreateTable(
                "dbo.MPS_M_SharedBranch",
                c => new
                    {
                        BranchCode = c.String(nullable: false, maxLength: 100),
                        BranchName = c.String(),
                        Inactive = c.Boolean(nullable: false),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.BranchCode);
            
            CreateTable(
                "dbo.MPS_ProposalTable",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProposalTypeCode = c.String(maxLength: 5),
                        ProposalTypeName = c.String(maxLength: 50),
                        Place = c.String(),
                        Source = c.String(),
                        Other = c.String(maxLength: 100),
                        DepositId = c.Int(nullable: false),
                        DepositNumber = c.String(),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        Budget = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CustomersTargetGroup = c.String(),
                        CheckBudget = c.Boolean(nullable: false),
                        TransferToNextYear = c.Boolean(nullable: false),
                        Objective = c.String(),
                        TR_RE = c.String(maxLength: 50),
                        TotalMarketingExpense = c.Decimal(precision: 18, scale: 2),
                        TotalIncomeDeposit = c.Decimal(precision: 18, scale: 2),
                        TotalIncomeOther = c.Decimal(precision: 18, scale: 2),
                        TotalSaleTarget = c.Decimal(precision: 18, scale: 2),
                        DepartmentException = c.String(maxLength: 200),
                        UnitCode = c.String(maxLength: 20),
                        UnitName = c.String(maxLength: 1000),
                        Title = c.String(nullable: false, maxLength: 200),
                        DocumentNumber = c.String(maxLength: 20),
                        Revision = c.Int(),
                        RequesterUserName = c.String(nullable: false, maxLength: 100),
                        RequesterName = c.String(nullable: false, maxLength: 200),
                        SubmittedDate = c.DateTime(),
                        Status = c.String(maxLength: 100),
                        DocumentStatus = c.String(maxLength: 100),
                        StatusFlag = c.Int(nullable: false),
                        ProcInstId = c.Int(),
                        ExpectedDuration = c.Int(),
                        DocumentDate = c.DateTime(),
                        CompletedDate = c.DateTime(),
                        CancelledDate = c.DateTime(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        Deleted = c.Boolean(nullable: false),
                        DeletedDate = c.DateTime(),
                        DeletedBy = c.String(maxLength: 100),
                        DeletedByName = c.String(maxLength: 200),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MPS_M_UnitCode", t => t.UnitCode)
                .Index(t => t.UnitCode)
                .Index(t => new { t.DocumentNumber, t.Revision }, name: "DocumentNumber_Revision");
            
            CreateTable(
                "dbo.MPS_ProposalAttachment",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ParentId = c.Int(nullable: false),
                        DocumentNumber = c.String(maxLength: 20),
                        ContentType = c.String(maxLength: 100),
                        ProcInstId = c.Int(),
                        SerialNumber = c.String(maxLength: 20),
                        Activity = c.String(maxLength: 100),
                        FileDescription = c.String(maxLength: 300),
                        FileName = c.String(maxLength: 300),
                        FileSize = c.Long(nullable: false),
                        FileSizeText = c.String(maxLength: 300),
                        DocumentLibrary = c.String(maxLength: 300),
                        Location = c.String(maxLength: 600),
                        DownloadUrl = c.String(maxLength: 600),
                        DocumentTypeId = c.Int(),
                        DocumentTypeName = c.String(maxLength: 100),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MPS_ProposalTable", t => t.ParentId, cascadeDelete: true)
                .Index(t => t.ParentId);
            
            CreateTable(
                "dbo.MPS_ProposalComment",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ParentId = c.Int(nullable: false),
                        ProcInstId = c.Int(),
                        SerialNumber = c.String(maxLength: 20),
                        ProcessName = c.String(maxLength: 100),
                        Activity = c.String(maxLength: 100),
                        Action = c.String(maxLength: 100),
                        Comment = c.String(),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MPS_ProposalTable", t => t.ParentId, cascadeDelete: true)
                .Index(t => t.ParentId);
            
            CreateTable(
                "dbo.MPS_DepositLine",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PaymentNo = c.String(maxLength: 50),
                        DatePeriod = c.String(maxLength: 50),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ProcessStatus = c.String(maxLength: 100),
                        StatusFlag = c.Int(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                        DeletedDate = c.DateTime(),
                        DeletedBy = c.String(),
                        DeletedByName = c.String(),
                        LineNo = c.Single(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        ParentId = c.Int(nullable: false),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MPS_ProposalTable", t => t.ParentId, cascadeDelete: true)
                .Index(t => t.ParentId);
            
            CreateTable(
                "dbo.MPS_EstimateTotalSale",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MarketingGroup = c.String(maxLength: 50),
                        BudgetPlan = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Actual = c.Decimal(nullable: false, precision: 18, scale: 2),
                        PreviousActual = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Remark = c.String(),
                        StatusFlag = c.Int(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                        DeletedDate = c.DateTime(),
                        DeletedBy = c.String(),
                        DeletedByName = c.String(),
                        LineNo = c.Single(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        ParentId = c.Int(nullable: false),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MPS_ProposalTable", t => t.ParentId, cascadeDelete: true)
                .Index(t => t.ParentId);
            
            CreateTable(
                "dbo.MPS_IncomeDeposit",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProposalDepositRefID = c.Int(nullable: false),
                        ProposalDepositRefDoc = c.String(maxLength: 50),
                        ProposalDepositRefTitle = c.String(maxLength: 200),
                        Budget = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Remark = c.String(),
                        StatusFlag = c.Int(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                        DeletedDate = c.DateTime(),
                        DeletedBy = c.String(),
                        DeletedByName = c.String(),
                        LineNo = c.Single(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        ParentId = c.Int(nullable: false),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MPS_ProposalTable", t => t.ParentId, cascadeDelete: true)
                .Index(t => t.ParentId);
            
            CreateTable(
                "dbo.MPS_IncomeOther",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SponcorName = c.String(maxLength: 100),
                        Description = c.String(maxLength: 100),
                        Budget = c.Decimal(nullable: false, precision: 18, scale: 2),
                        DueDate = c.DateTime(nullable: false),
                        ContactName = c.String(maxLength: 100),
                        ContactTel = c.String(maxLength: 50),
                        ContactEmail = c.String(maxLength: 50),
                        ContactDepartment = c.String(maxLength: 100),
                        Remark = c.String(),
                        StatusFlag = c.Int(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                        DeletedDate = c.DateTime(),
                        DeletedBy = c.String(),
                        DeletedByName = c.String(),
                        LineNo = c.Single(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        ParentId = c.Int(nullable: false),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MPS_ProposalTable", t => t.ParentId, cascadeDelete: true)
                .Index(t => t.ParentId);
            
            CreateTable(
                "dbo.MPS_IncomeTotalSale",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(maxLength: 100),
                        Budget = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Remark = c.String(),
                        StatusFlag = c.Int(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                        DeletedDate = c.DateTime(),
                        DeletedBy = c.String(),
                        DeletedByName = c.String(),
                        LineNo = c.Single(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        ParentId = c.Int(nullable: false),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MPS_ProposalTable", t => t.ParentId, cascadeDelete: true)
                .Index(t => t.ParentId);
            
            CreateTable(
                "dbo.MPS_IncomeTotalSaleTemplate",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        BranchCode = c.String(maxLength: 100),
                        IncomeTotalSaleId = c.Int(nullable: false),
                        PercentOfDepartment = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TR_Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TR_Percent = c.Decimal(nullable: false, precision: 18, scale: 2),
                        RE_Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        RE_Percent = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MPS_IncomeTotalSale", t => t.IncomeTotalSaleId, cascadeDelete: true)
                .ForeignKey("dbo.MPS_M_SharedBranch", t => t.BranchCode)
                .Index(t => t.BranchCode)
                .Index(t => t.IncomeTotalSaleId);
            
            CreateTable(
                "dbo.MPS_ProposalApproval",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ApproverSequence = c.Int(nullable: false),
                        ApproverLevel = c.String(maxLength: 50),
                        ApproverUserName = c.String(maxLength: 50),
                        ApproverFullName = c.String(maxLength: 100),
                        ApproverEmail = c.String(),
                        Position = c.String(maxLength: 100),
                        Department = c.String(maxLength: 100),
                        DueDate = c.DateTime(),
                        StatusFlag = c.Int(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                        DeletedDate = c.DateTime(),
                        DeletedBy = c.String(),
                        DeletedByName = c.String(),
                        LineNo = c.Single(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        ParentId = c.Int(nullable: false),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MPS_ProposalTable", t => t.ParentId, cascadeDelete: true)
                .Index(t => t.ParentId);
            
            CreateTable(
                "dbo.MPS_ProposalLineSharedTemplate",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ParentId = c.Int(nullable: false),
                        BranchCode = c.String(maxLength: 100),
                        Selected = c.Boolean(nullable: false),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MPS_ProposalTable", t => t.ParentId, cascadeDelete: true)
                .ForeignKey("dbo.MPS_M_SharedBranch", t => t.BranchCode)
                .Index(t => t.ParentId)
                .Index(t => t.BranchCode);
            
            CreateTable(
                "dbo.MPS_BudgetYearTrans",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Year = c.String(maxLength: 4),
                        UnitCode = c.String(maxLength: 20),
                        Status = c.String(maxLength: 100),
                        DocumentStatus = c.String(maxLength: 100),
                        DocumentType = c.String(maxLength: 100),
                        DocumentId = c.Int(),
                        DocumentNo = c.String(maxLength: 20),
                        RefDocumentType = c.String(maxLength: 100),
                        RefDocumentId = c.Int(),
                        RefDocumentNumber = c.String(maxLength: 20),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TransDate = c.DateTime(nullable: false),
                        InActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.MPS_CashClearingApproval",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ApproverSequence = c.Int(nullable: false),
                        ApproverLevel = c.String(maxLength: 50),
                        ApproverUserName = c.String(maxLength: 50),
                        ApproverFullName = c.String(maxLength: 100),
                        ApproverEmail = c.String(),
                        Position = c.String(maxLength: 100),
                        Department = c.String(maxLength: 100),
                        DueDate = c.DateTime(),
                        StatusFlag = c.Int(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                        DeletedDate = c.DateTime(),
                        DeletedBy = c.String(),
                        DeletedByName = c.String(),
                        LineNo = c.Single(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        ParentId = c.Int(nullable: false),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MPS_CashClearingTable", t => t.ParentId, cascadeDelete: true)
                .Index(t => t.ParentId);
            
            CreateTable(
                "dbo.MPS_CashClearingTable",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RefAdvanceNumber = c.String(maxLength: 20),
                        ClearingDate = c.DateTime(nullable: false),
                        Branch = c.String(maxLength: 100),
                        Budget = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Description = c.String(),
                        CashAdvanceID = c.Int(nullable: false),
                        UnitCode = c.String(maxLength: 20),
                        UnitName = c.String(maxLength: 1000),
                        Title = c.String(nullable: false, maxLength: 200),
                        DocumentNumber = c.String(maxLength: 20),
                        Revision = c.Int(),
                        RequesterUserName = c.String(nullable: false, maxLength: 100),
                        RequesterName = c.String(nullable: false, maxLength: 200),
                        SubmittedDate = c.DateTime(),
                        Status = c.String(maxLength: 100),
                        DocumentStatus = c.String(maxLength: 100),
                        StatusFlag = c.Int(nullable: false),
                        ProcInstId = c.Int(),
                        ExpectedDuration = c.Int(),
                        DocumentDate = c.DateTime(),
                        CompletedDate = c.DateTime(),
                        CancelledDate = c.DateTime(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        Deleted = c.Boolean(nullable: false),
                        DeletedDate = c.DateTime(),
                        DeletedBy = c.String(maxLength: 100),
                        DeletedByName = c.String(maxLength: 200),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MPS_CashAdvanceTable", t => t.CashAdvanceID, cascadeDelete: true)
                .ForeignKey("dbo.MPS_M_UnitCode", t => t.UnitCode)
                .Index(t => t.CashAdvanceID)
                .Index(t => t.UnitCode)
                .Index(t => new { t.DocumentNumber, t.Revision }, name: "DocumentNumber_Revision");
            
            CreateTable(
                "dbo.MPS_CashClearingAttachment",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ParentId = c.Int(nullable: false),
                        DocumentNumber = c.String(maxLength: 20),
                        ContentType = c.String(maxLength: 100),
                        ProcInstId = c.Int(),
                        SerialNumber = c.String(maxLength: 20),
                        Activity = c.String(maxLength: 100),
                        FileDescription = c.String(maxLength: 300),
                        FileName = c.String(maxLength: 300),
                        FileSize = c.Long(nullable: false),
                        FileSizeText = c.String(maxLength: 300),
                        DocumentLibrary = c.String(maxLength: 300),
                        Location = c.String(maxLength: 600),
                        DownloadUrl = c.String(maxLength: 600),
                        DocumentTypeId = c.Int(),
                        DocumentTypeName = c.String(maxLength: 100),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MPS_CashClearingTable", t => t.ParentId, cascadeDelete: true)
                .Index(t => t.ParentId);
            
            CreateTable(
                "dbo.MPS_CashClearingLine",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(maxLength: 100),
                        Unit = c.Int(),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Actual = c.Decimal(precision: 18, scale: 2),
                        Remark = c.String(),
                        StatusFlag = c.Int(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                        DeletedDate = c.DateTime(),
                        DeletedBy = c.String(),
                        DeletedByName = c.String(),
                        LineNo = c.Single(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        ParentId = c.Int(nullable: false),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MPS_CashClearingTable", t => t.ParentId, cascadeDelete: true)
                .Index(t => t.ParentId);
            
            CreateTable(
                "dbo.MPS_CashClearingComment",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ParentId = c.Int(nullable: false),
                        ProcInstId = c.Int(),
                        SerialNumber = c.String(maxLength: 20),
                        ProcessName = c.String(maxLength: 100),
                        Activity = c.String(maxLength: 100),
                        Action = c.String(maxLength: 100),
                        Comment = c.String(),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MPS_CashClearingTable", t => t.ParentId, cascadeDelete: true)
                .Index(t => t.ParentId);
            
            CreateTable(
                "dbo.MPS_EmployeeTable",
                c => new
                    {
                        EmpId = c.String(nullable: false, maxLength: 10),
                        EmpStatus = c.String(maxLength: 50),
                        ThFName = c.String(maxLength: 200),
                        ThLName = c.String(maxLength: 200),
                        EnFName = c.String(maxLength: 200),
                        EnLName = c.String(maxLength: 200),
                        DepartmentCode = c.String(maxLength: 20),
                        DepartmentName = c.String(maxLength: 200),
                        PositionCode = c.String(maxLength: 20),
                        PositionName = c.String(maxLength: 200),
                        Email = c.String(maxLength: 200),
                        TelephoneNo = c.String(maxLength: 10),
                        CompanyCode = c.String(maxLength: 5),
                        CompanyName = c.String(maxLength: 200),
                        Username = c.String(maxLength: 100),
                    })
                .PrimaryKey(t => t.EmpId);
            
            CreateTable(
                "dbo.MPS_EstimateTotalSaleTemplate",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        BranchCode = c.String(maxLength: 100),
                        EstimateTotalSaleId = c.Int(nullable: false),
                        PercentOfDepartment = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TR_Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TR_Percent = c.Decimal(nullable: false, precision: 18, scale: 2),
                        RE_Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        RE_Percent = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MPS_EstimateTotalSale", t => t.EstimateTotalSaleId, cascadeDelete: true)
                .ForeignKey("dbo.MPS_M_SharedBranch", t => t.BranchCode)
                .Index(t => t.BranchCode)
                .Index(t => t.EstimateTotalSaleId);
            
            CreateTable(
                "dbo.MPS_M_APcode",
                c => new
                    {
                        APCode = c.String(nullable: false, maxLength: 20),
                        APName = c.String(maxLength: 200),
                        Description = c.String(),
                        CheckBudget = c.Boolean(nullable: false),
                        ExpenseTopicCode = c.String(maxLength: 20),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.APCode)
                .ForeignKey("dbo.MPS_M_ExpenseTopic", t => t.ExpenseTopicCode)
                .Index(t => t.ExpenseTopicCode);
            
            CreateTable(
                "dbo.MPS_M_ExpenseTopic",
                c => new
                    {
                        ExpenseTopicCode = c.String(nullable: false, maxLength: 20),
                        ExpenseTopicName = c.String(maxLength: 200),
                        Description = c.String(),
                        TypeAP = c.String(maxLength: 1),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.ExpenseTopicCode);
            
            CreateTable(
                "dbo.MPS_M_BudgetYearPlan",
                c => new
                    {
                        BudgetYearID = c.String(nullable: false, maxLength: 6),
                        TeamName = c.String(),
                        Type = c.String(maxLength: 2),
                        Budget = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Year = c.String(maxLength: 4),
                        UnitCode = c.String(maxLength: 20),
                        UnitName = c.String(maxLength: 1000),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.BudgetYearID);
            
            CreateTable(
                "dbo.MPS_M_Configulation",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Value = c.String(nullable: false, maxLength: 128),
                        Text = c.String(),
                        Group = c.String(),
                        Description = c.String(),
                        ActiveFlag = c.Boolean(nullable: false),
                        Sequence = c.Single(),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => new { t.Id, t.Value });
            
            CreateTable(
                "dbo.MPS_M_Place",
                c => new
                    {
                        PlaceCode = c.String(nullable: false, maxLength: 100),
                        PlaceName = c.String(),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.PlaceCode);
            
            CreateTable(
                "dbo.MPS_M_TypeMemo",
                c => new
                    {
                        TypeMemoCode = c.String(nullable: false, maxLength: 50),
                        TypeMemoName = c.String(maxLength: 50),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.TypeMemoCode);
            
            CreateTable(
                "dbo.MPS_M_TypeProposal",
                c => new
                    {
                        TypeProposalCode = c.String(nullable: false, maxLength: 10),
                        TypeProposalName = c.String(maxLength: 100),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.TypeProposalCode);
            
            CreateTable(
                "dbo.NumberSeqSetup",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        NumberSeqCode = c.String(maxLength: 50),
                        Name = c.String(maxLength: 100),
                        Year = c.String(maxLength: 4),
                        Process = c.String(maxLength: 100),
                        Dimension1 = c.String(maxLength: 100),
                        Dimension2 = c.String(maxLength: 100),
                        Dimension3 = c.String(maxLength: 100),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.NumberSeqTable", t => t.NumberSeqCode)
                .Index(t => t.NumberSeqCode);
            
            CreateTable(
                "dbo.NumberSeqTable",
                c => new
                    {
                        NumberSeqCode = c.String(nullable: false, maxLength: 50),
                        Name = c.String(maxLength: 100),
                        NextNum = c.Int(nullable: false),
                        Format = c.String(maxLength: 20),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.NumberSeqCode);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.NumberSeqSetup", "NumberSeqCode", "dbo.NumberSeqTable");
            DropForeignKey("dbo.MPS_M_APcode", "ExpenseTopicCode", "dbo.MPS_M_ExpenseTopic");
            DropForeignKey("dbo.MPS_EstimateTotalSaleTemplate", "BranchCode", "dbo.MPS_M_SharedBranch");
            DropForeignKey("dbo.MPS_EstimateTotalSaleTemplate", "EstimateTotalSaleId", "dbo.MPS_EstimateTotalSale");
            DropForeignKey("dbo.MPS_CashClearingTable", "UnitCode", "dbo.MPS_M_UnitCode");
            DropForeignKey("dbo.MPS_CashClearingComment", "ParentId", "dbo.MPS_CashClearingTable");
            DropForeignKey("dbo.MPS_CashClearingLine", "ParentId", "dbo.MPS_CashClearingTable");
            DropForeignKey("dbo.MPS_CashClearingApproval", "ParentId", "dbo.MPS_CashClearingTable");
            DropForeignKey("dbo.MPS_CashClearingTable", "CashAdvanceID", "dbo.MPS_CashAdvanceTable");
            DropForeignKey("dbo.MPS_CashClearingAttachment", "ParentId", "dbo.MPS_CashClearingTable");
            DropForeignKey("dbo.MPS_BudgetProposalTrans", "ProposalId", "dbo.MPS_ProposalTable");
            DropForeignKey("dbo.MPS_BudgetProposalTrans", "ProposalLineId", "dbo.MPS_ProposalLine");
            DropForeignKey("dbo.MPS_ProposalTable", "UnitCode", "dbo.MPS_M_UnitCode");
            DropForeignKey("dbo.MPS_ProposalLineSharedTemplate", "BranchCode", "dbo.MPS_M_SharedBranch");
            DropForeignKey("dbo.MPS_ProposalLineSharedTemplate", "ParentId", "dbo.MPS_ProposalTable");
            DropForeignKey("dbo.MPS_ProposalLine", "ParentId", "dbo.MPS_ProposalTable");
            DropForeignKey("dbo.MPS_ProposalApproval", "ParentId", "dbo.MPS_ProposalTable");
            DropForeignKey("dbo.MPS_IncomeTotalSale", "ParentId", "dbo.MPS_ProposalTable");
            DropForeignKey("dbo.MPS_IncomeTotalSaleTemplate", "BranchCode", "dbo.MPS_M_SharedBranch");
            DropForeignKey("dbo.MPS_IncomeTotalSaleTemplate", "IncomeTotalSaleId", "dbo.MPS_IncomeTotalSale");
            DropForeignKey("dbo.MPS_IncomeOther", "ParentId", "dbo.MPS_ProposalTable");
            DropForeignKey("dbo.MPS_IncomeDeposit", "ParentId", "dbo.MPS_ProposalTable");
            DropForeignKey("dbo.MPS_EstimateTotalSale", "ParentId", "dbo.MPS_ProposalTable");
            DropForeignKey("dbo.MPS_DepositLine", "ParentId", "dbo.MPS_ProposalTable");
            DropForeignKey("dbo.MPS_ProposalComment", "ParentId", "dbo.MPS_ProposalTable");
            DropForeignKey("dbo.MPS_ProposalAttachment", "ParentId", "dbo.MPS_ProposalTable");
            DropForeignKey("dbo.MPS_ProposalLineTemplate", "BranchCode", "dbo.MPS_M_SharedBranch");
            DropForeignKey("dbo.MPS_ProposalLineTemplate", "ProposalLineId", "dbo.MPS_ProposalLine");
            DropForeignKey("dbo.MPS_PettyCashTable", "UnitCode", "dbo.MPS_M_UnitCode");
            DropForeignKey("dbo.MPS_PettyCashTable", "ProposalLineID", "dbo.MPS_ProposalLine");
            DropForeignKey("dbo.MPS_PettyCashLine", "ParentId", "dbo.MPS_PettyCashTable");
            DropForeignKey("dbo.MPS_PettyCashApproval", "ParentId", "dbo.MPS_PettyCashTable");
            DropForeignKey("dbo.MPS_PettyCashComment", "ParentId", "dbo.MPS_PettyCashTable");
            DropForeignKey("dbo.MPS_PettyCashAttachment", "ParentId", "dbo.MPS_PettyCashTable");
            DropForeignKey("dbo.MPS_MemoTable", "UnitCode", "dbo.MPS_M_UnitCode");
            DropForeignKey("dbo.MPS_MemoTable", "ProposalLineID", "dbo.MPS_ProposalLine");
            DropForeignKey("dbo.MPS_MemoLine", "ParentId", "dbo.MPS_MemoTable");
            DropForeignKey("dbo.MPS_MemoApproval", "ParentId", "dbo.MPS_MemoTable");
            DropForeignKey("dbo.MPS_MemoComment", "ParentId", "dbo.MPS_MemoTable");
            DropForeignKey("dbo.MPS_MemoAttachment", "ParentId", "dbo.MPS_MemoTable");
            DropForeignKey("dbo.MPS_CashAdvanceTable", "UnitCode", "dbo.MPS_M_UnitCode");
            DropForeignKey("dbo.MPS_CashAdvanceTable", "ProposalLineID", "dbo.MPS_ProposalLine");
            DropForeignKey("dbo.MPS_CashAdvanceComment", "ParentId", "dbo.MPS_CashAdvanceTable");
            DropForeignKey("dbo.MPS_CashAdvanceLine", "ParentId", "dbo.MPS_CashAdvanceTable");
            DropForeignKey("dbo.MPS_CashAdvanceApproval", "ParentId", "dbo.MPS_CashAdvanceTable");
            DropForeignKey("dbo.MPS_CashAdvanceAttachment", "ParentId", "dbo.MPS_CashAdvanceTable");
            DropForeignKey("dbo.MenuItem", "RefererId", "dbo.Referer");
            DropForeignKey("dbo.RefererUserPermission", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.RefererUserPermission", "RefererId", "dbo.Referer");
            DropForeignKey("dbo.RefererRolePermission", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.RefererRolePermission", "RefererId", "dbo.Referer");
            DropForeignKey("dbo.MenuItem", "ParentId", "dbo.MenuItem");
            DropForeignKey("dbo.MailSetup", "MailContentId", "dbo.MailContent");
            DropForeignKey("dbo.MailContent", "MailTemplateId", "dbo.MailTemplate");
            DropForeignKey("dbo.DocumentType", "ProcessCode", "dbo.Process");
            DropIndex("dbo.NumberSeqSetup", new[] { "NumberSeqCode" });
            DropIndex("dbo.MPS_M_APcode", new[] { "ExpenseTopicCode" });
            DropIndex("dbo.MPS_EstimateTotalSaleTemplate", new[] { "EstimateTotalSaleId" });
            DropIndex("dbo.MPS_EstimateTotalSaleTemplate", new[] { "BranchCode" });
            DropIndex("dbo.MPS_CashClearingComment", new[] { "ParentId" });
            DropIndex("dbo.MPS_CashClearingLine", new[] { "ParentId" });
            DropIndex("dbo.MPS_CashClearingAttachment", new[] { "ParentId" });
            DropIndex("dbo.MPS_CashClearingTable", "DocumentNumber_Revision");
            DropIndex("dbo.MPS_CashClearingTable", new[] { "UnitCode" });
            DropIndex("dbo.MPS_CashClearingTable", new[] { "CashAdvanceID" });
            DropIndex("dbo.MPS_CashClearingApproval", new[] { "ParentId" });
            DropIndex("dbo.MPS_ProposalLineSharedTemplate", new[] { "BranchCode" });
            DropIndex("dbo.MPS_ProposalLineSharedTemplate", new[] { "ParentId" });
            DropIndex("dbo.MPS_ProposalApproval", new[] { "ParentId" });
            DropIndex("dbo.MPS_IncomeTotalSaleTemplate", new[] { "IncomeTotalSaleId" });
            DropIndex("dbo.MPS_IncomeTotalSaleTemplate", new[] { "BranchCode" });
            DropIndex("dbo.MPS_IncomeTotalSale", new[] { "ParentId" });
            DropIndex("dbo.MPS_IncomeOther", new[] { "ParentId" });
            DropIndex("dbo.MPS_IncomeDeposit", new[] { "ParentId" });
            DropIndex("dbo.MPS_EstimateTotalSale", new[] { "ParentId" });
            DropIndex("dbo.MPS_DepositLine", new[] { "ParentId" });
            DropIndex("dbo.MPS_ProposalComment", new[] { "ParentId" });
            DropIndex("dbo.MPS_ProposalAttachment", new[] { "ParentId" });
            DropIndex("dbo.MPS_ProposalTable", "DocumentNumber_Revision");
            DropIndex("dbo.MPS_ProposalTable", new[] { "UnitCode" });
            DropIndex("dbo.MPS_ProposalLineTemplate", new[] { "ProposalLineId" });
            DropIndex("dbo.MPS_ProposalLineTemplate", new[] { "BranchCode" });
            DropIndex("dbo.MPS_PettyCashLine", new[] { "ParentId" });
            DropIndex("dbo.MPS_PettyCashApproval", new[] { "ParentId" });
            DropIndex("dbo.MPS_PettyCashComment", new[] { "ParentId" });
            DropIndex("dbo.MPS_PettyCashAttachment", new[] { "ParentId" });
            DropIndex("dbo.MPS_PettyCashTable", "DocumentNumber_Revision");
            DropIndex("dbo.MPS_PettyCashTable", new[] { "UnitCode" });
            DropIndex("dbo.MPS_PettyCashTable", new[] { "ProposalLineID" });
            DropIndex("dbo.MPS_MemoLine", new[] { "ParentId" });
            DropIndex("dbo.MPS_MemoApproval", new[] { "ParentId" });
            DropIndex("dbo.MPS_MemoComment", new[] { "ParentId" });
            DropIndex("dbo.MPS_MemoAttachment", new[] { "ParentId" });
            DropIndex("dbo.MPS_MemoTable", "DocumentNumber_Revision");
            DropIndex("dbo.MPS_MemoTable", new[] { "UnitCode" });
            DropIndex("dbo.MPS_MemoTable", new[] { "ProposalLineID" });
            DropIndex("dbo.MPS_CashAdvanceComment", new[] { "ParentId" });
            DropIndex("dbo.MPS_CashAdvanceLine", new[] { "ParentId" });
            DropIndex("dbo.MPS_CashAdvanceApproval", new[] { "ParentId" });
            DropIndex("dbo.MPS_CashAdvanceAttachment", new[] { "ParentId" });
            DropIndex("dbo.MPS_CashAdvanceTable", "DocumentNumber_Revision");
            DropIndex("dbo.MPS_CashAdvanceTable", new[] { "UnitCode" });
            DropIndex("dbo.MPS_CashAdvanceTable", new[] { "ProposalLineID" });
            DropIndex("dbo.MPS_ProposalLine", new[] { "ParentId" });
            DropIndex("dbo.MPS_BudgetProposalTrans", new[] { "ProposalLineId" });
            DropIndex("dbo.MPS_BudgetProposalTrans", new[] { "ProposalId" });
            DropIndex("dbo.RefererUserPermission", new[] { "UserId" });
            DropIndex("dbo.RefererUserPermission", new[] { "RefererId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.RefererRolePermission", new[] { "RoleId" });
            DropIndex("dbo.RefererRolePermission", new[] { "RefererId" });
            DropIndex("dbo.MenuItem", new[] { "ParentId" });
            DropIndex("dbo.MenuItem", new[] { "RefererId" });
            DropIndex("dbo.MailSetup", new[] { "MailContentId" });
            DropIndex("dbo.MailSetup", "IX_ProcessMailType");
            DropIndex("dbo.MailRecipient", "IX_ProcessMailType");
            DropIndex("dbo.MailContent", new[] { "MailTemplateId" });
            DropIndex("dbo.DocumentType", new[] { "ProcessCode" });
            DropIndex("dbo.ApplicationSetting", "IX_EnvironmentKey");
            DropTable("dbo.NumberSeqTable");
            DropTable("dbo.NumberSeqSetup");
            DropTable("dbo.MPS_M_TypeProposal");
            DropTable("dbo.MPS_M_TypeMemo");
            DropTable("dbo.MPS_M_Place");
            DropTable("dbo.MPS_M_Configulation");
            DropTable("dbo.MPS_M_BudgetYearPlan");
            DropTable("dbo.MPS_M_ExpenseTopic");
            DropTable("dbo.MPS_M_APcode");
            DropTable("dbo.MPS_EstimateTotalSaleTemplate");
            DropTable("dbo.MPS_EmployeeTable");
            DropTable("dbo.MPS_CashClearingComment");
            DropTable("dbo.MPS_CashClearingLine");
            DropTable("dbo.MPS_CashClearingAttachment");
            DropTable("dbo.MPS_CashClearingTable");
            DropTable("dbo.MPS_CashClearingApproval");
            DropTable("dbo.MPS_BudgetYearTrans");
            DropTable("dbo.MPS_ProposalLineSharedTemplate");
            DropTable("dbo.MPS_ProposalApproval");
            DropTable("dbo.MPS_IncomeTotalSaleTemplate");
            DropTable("dbo.MPS_IncomeTotalSale");
            DropTable("dbo.MPS_IncomeOther");
            DropTable("dbo.MPS_IncomeDeposit");
            DropTable("dbo.MPS_EstimateTotalSale");
            DropTable("dbo.MPS_DepositLine");
            DropTable("dbo.MPS_ProposalComment");
            DropTable("dbo.MPS_ProposalAttachment");
            DropTable("dbo.MPS_ProposalTable");
            DropTable("dbo.MPS_M_SharedBranch");
            DropTable("dbo.MPS_ProposalLineTemplate");
            DropTable("dbo.MPS_PettyCashLine");
            DropTable("dbo.MPS_PettyCashApproval");
            DropTable("dbo.MPS_PettyCashComment");
            DropTable("dbo.MPS_PettyCashAttachment");
            DropTable("dbo.MPS_PettyCashTable");
            DropTable("dbo.MPS_MemoLine");
            DropTable("dbo.MPS_MemoApproval");
            DropTable("dbo.MPS_MemoComment");
            DropTable("dbo.MPS_MemoAttachment");
            DropTable("dbo.MPS_MemoTable");
            DropTable("dbo.MPS_M_UnitCode");
            DropTable("dbo.MPS_CashAdvanceComment");
            DropTable("dbo.MPS_CashAdvanceLine");
            DropTable("dbo.MPS_CashAdvanceApproval");
            DropTable("dbo.MPS_CashAdvanceAttachment");
            DropTable("dbo.MPS_CashAdvanceTable");
            DropTable("dbo.MPS_ProposalLine");
            DropTable("dbo.MPS_BudgetProposalTrans");
            DropTable("dbo.RefererUserPermission");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.RefererRolePermission");
            DropTable("dbo.Referer");
            DropTable("dbo.MenuItem");
            DropTable("dbo.MailSetup");
            DropTable("dbo.MailRecipient");
            DropTable("dbo.MailTemplate");
            DropTable("dbo.MailContent");
            DropTable("dbo.Process");
            DropTable("dbo.DocumentType");
            DropTable("dbo.ApplicationSetting");
        }
    }
}
