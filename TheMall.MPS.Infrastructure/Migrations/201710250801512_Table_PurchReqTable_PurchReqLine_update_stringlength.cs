namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Table_PurchReqTable_PurchReqLine_update_stringlength : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.MPS_PurchReqLine", "ITEM_DESC", c => c.String(maxLength: 1000));
            AlterColumn("dbo.MPS_PurchReqLine", "UOM", c => c.String(maxLength: 200));
            AlterColumn("dbo.MPS_PurchReqLine", "OLD_PRICE", c => c.String(maxLength: 200));
            AlterColumn("dbo.MPS_PurchReqLine", "OLD_TAX_AMOUNT", c => c.String(maxLength: 200));
            AlterColumn("dbo.MPS_PurchReqLine", "OLD_CURRENCY_CODE", c => c.String(maxLength: 200));
            AlterColumn("dbo.MPS_PurchReqLine", "CURRENCY_CODE", c => c.String(maxLength: 200));
            AlterColumn("dbo.MPS_PurchReqLine", "LONGDESC", c => c.String(maxLength: 1000));
            AlterColumn("dbo.MPS_PurchReqLine", "SHIP_INST", c => c.String(maxLength: 200));
            AlterColumn("dbo.MPS_PurchReqLine", "PAYMENTDESC", c => c.String(maxLength: 1000));
            AlterColumn("dbo.MPS_PurchReqLine", "COSTCENTER", c => c.String(maxLength: 200));
            AlterColumn("dbo.MPS_PurchReqLine", "PODOCTYPE", c => c.String(maxLength: 200));
            AlterColumn("dbo.MPS_PurchReqLine", "PURTYPE", c => c.String(maxLength: 200));
            AlterColumn("dbo.MPS_PurchReqLine", "GLACCOUNT", c => c.String(maxLength: 200));
            AlterColumn("dbo.MPS_PurchReqLine", "DEPARTMENT", c => c.String(maxLength: 200));
            AlterColumn("dbo.MPS_PurchReqLine", "PURGROUP", c => c.String(maxLength: 200));
            AlterColumn("dbo.MPS_PurchReqLine", "PROPOSALID", c => c.String(maxLength: 200));
            AlterColumn("dbo.MPS_PurchReqLine", "APCODE", c => c.String(maxLength: 200));
            AlterColumn("dbo.MPS_PurchReqLine", "REQUESTFORID", c => c.String(maxLength: 200));
            AlterColumn("dbo.MPS_PurchReqLine", "OLD_BUDGET_CODE", c => c.String(maxLength: 200));
            AlterColumn("dbo.MPS_PurchReqLine", "OLD_ORG_ID", c => c.String(maxLength: 200));
            AlterColumn("dbo.MPS_PurchReqLine", "DeletedBy", c => c.String(maxLength: 100));
            AlterColumn("dbo.MPS_PurchReqLine", "DeletedByName", c => c.String(maxLength: 200));
            AlterColumn("dbo.MPS_PurchReqTable", "IP_ID", c => c.String(maxLength: 200));
            AlterColumn("dbo.MPS_PurchReqTable", "PR_NUM", c => c.String(maxLength: 200));
            AlterColumn("dbo.MPS_PurchReqTable", "SUBCOMMAND", c => c.String(maxLength: 100));
            AlterColumn("dbo.MPS_PurchReqTable", "REQUISITIONER", c => c.String(maxLength: 200));
            AlterColumn("dbo.MPS_PurchReqTable", "BS_REQID", c => c.String(maxLength: 200));
            AlterColumn("dbo.MPS_PurchReqTable", "REQ_NAME", c => c.String(maxLength: 200));
            AlterColumn("dbo.MPS_PurchReqTable", "COMPANY_CODE", c => c.String(maxLength: 200));
            AlterColumn("dbo.MPS_PurchReqTable", "Status", c => c.String(maxLength: 200));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.MPS_PurchReqTable", "Status", c => c.String());
            AlterColumn("dbo.MPS_PurchReqTable", "COMPANY_CODE", c => c.String());
            AlterColumn("dbo.MPS_PurchReqTable", "REQ_NAME", c => c.String());
            AlterColumn("dbo.MPS_PurchReqTable", "BS_REQID", c => c.String());
            AlterColumn("dbo.MPS_PurchReqTable", "REQUISITIONER", c => c.String());
            AlterColumn("dbo.MPS_PurchReqTable", "SUBCOMMAND", c => c.String());
            AlterColumn("dbo.MPS_PurchReqTable", "PR_NUM", c => c.String());
            AlterColumn("dbo.MPS_PurchReqTable", "IP_ID", c => c.String());
            AlterColumn("dbo.MPS_PurchReqLine", "DeletedByName", c => c.String());
            AlterColumn("dbo.MPS_PurchReqLine", "DeletedBy", c => c.String());
            AlterColumn("dbo.MPS_PurchReqLine", "OLD_ORG_ID", c => c.String());
            AlterColumn("dbo.MPS_PurchReqLine", "OLD_BUDGET_CODE", c => c.String());
            AlterColumn("dbo.MPS_PurchReqLine", "REQUESTFORID", c => c.String());
            AlterColumn("dbo.MPS_PurchReqLine", "APCODE", c => c.String());
            AlterColumn("dbo.MPS_PurchReqLine", "PROPOSALID", c => c.String());
            AlterColumn("dbo.MPS_PurchReqLine", "PURGROUP", c => c.String());
            AlterColumn("dbo.MPS_PurchReqLine", "DEPARTMENT", c => c.String());
            AlterColumn("dbo.MPS_PurchReqLine", "GLACCOUNT", c => c.String());
            AlterColumn("dbo.MPS_PurchReqLine", "PURTYPE", c => c.String());
            AlterColumn("dbo.MPS_PurchReqLine", "PODOCTYPE", c => c.String());
            AlterColumn("dbo.MPS_PurchReqLine", "COSTCENTER", c => c.String());
            AlterColumn("dbo.MPS_PurchReqLine", "PAYMENTDESC", c => c.String());
            AlterColumn("dbo.MPS_PurchReqLine", "SHIP_INST", c => c.String());
            AlterColumn("dbo.MPS_PurchReqLine", "LONGDESC", c => c.String());
            AlterColumn("dbo.MPS_PurchReqLine", "CURRENCY_CODE", c => c.String());
            AlterColumn("dbo.MPS_PurchReqLine", "OLD_CURRENCY_CODE", c => c.String());
            AlterColumn("dbo.MPS_PurchReqLine", "OLD_TAX_AMOUNT", c => c.String());
            AlterColumn("dbo.MPS_PurchReqLine", "OLD_PRICE", c => c.String());
            AlterColumn("dbo.MPS_PurchReqLine", "UOM", c => c.String());
            AlterColumn("dbo.MPS_PurchReqLine", "ITEM_DESC", c => c.String());
        }
    }
}
