namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addFlagClose : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MPS_ProposalTable", "CloseFlag", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MPS_ProposalTable", "CloseFlag");
        }
    }
}
