namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MPS_MemoIncomeInvoice2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MPS_MemoIncomeInvoice",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        InvoiceNo = c.String(),
                        Actual = c.Decimal(nullable: false, precision: 18, scale: 2),
                        InvoiceDate = c.DateTime(),
                        Remark = c.String(),
                        StatusFlag = c.Int(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                        DeletedDate = c.DateTime(),
                        DeletedBy = c.String(),
                        DeletedByName = c.String(),
                        LineNo = c.Single(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        ParentId = c.Int(nullable: false),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MPS_MemoIncomeTable", t => t.ParentId, cascadeDelete: true)
                .Index(t => t.ParentId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MPS_MemoIncomeInvoice", "ParentId", "dbo.MPS_MemoIncomeTable");
            DropIndex("dbo.MPS_MemoIncomeInvoice", new[] { "ParentId" });
            DropTable("dbo.MPS_MemoIncomeInvoice");
        }
    }
}
