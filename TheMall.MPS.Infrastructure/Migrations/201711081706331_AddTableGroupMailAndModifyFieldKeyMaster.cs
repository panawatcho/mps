namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTableGroupMailAndModifyFieldKeyMaster : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.MPS_M_FinalApprover", "EmpId", "dbo.MPS_M_EmployeeTable");
            DropIndex("dbo.MPS_M_FinalApprover", new[] { "EmpId" });
            DropColumn("dbo.MPS_M_FinalApprover", "Username");
            RenameColumn(table: "dbo.MPS_M_FinalApprover", name: "EmpId", newName: "Username");
            DropPrimaryKey("dbo.MPS_M_BudgetYearPlan");
            DropPrimaryKey("dbo.MPS_M_FinalApprover");
            DropPrimaryKey("dbo.MPS_M_TotalSaleLastYear");
            CreateTable(
                "dbo.MPS_M_ProcessApprove",
                c => new
                    {
                        ProcessCode = c.String(nullable: false, maxLength: 50),
                        ProcessName = c.String(),
                        ProcessType = c.String(maxLength: 10),
                        InActive = c.Boolean(nullable: false),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.ProcessCode);
            
            CreateTable(
                "dbo.MPS_M_MailGroup",
                c => new
                    {
                        GroupCode = c.String(nullable: false, maxLength: 100),
                        Email = c.String(),
                        InActive = c.Boolean(nullable: false),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.GroupCode);
            
            AddColumn("dbo.MPS_MemoTable", "PayToUnitCode", c => c.String(maxLength: 20));
            AddColumn("dbo.MPS_MemoTable", "SpecialType", c => c.Boolean(nullable: false));
            AddColumn("dbo.MPS_M_FinalApprover", "UnitCode", c => c.String(nullable: false, maxLength: 20));
            AddColumn("dbo.MPS_M_FinalApprover", "Amount", c => c.Decimal(precision: 18, scale: 2));
            AlterColumn("dbo.MPS_M_UnitCode", "Type", c => c.String(maxLength: 5));
            AlterColumn("dbo.MPS_M_BudgetYearPlan", "Type", c => c.String(maxLength: 5));
            AlterColumn("dbo.MPS_M_BudgetYearPlan", "Year", c => c.String(nullable: false, maxLength: 4));
            AlterColumn("dbo.MPS_M_BudgetYearPlan", "UnitCode", c => c.String(nullable: false, maxLength: 20));
            AlterColumn("dbo.MPS_M_FinalApprover", "Username", c => c.String(nullable: false, maxLength: 10));
            AlterColumn("dbo.MPS_M_FinalApprover", "Username", c => c.String(nullable: false, maxLength: 10));
            AlterColumn("dbo.MPS_M_TotalSaleLastYear", "BranchCode", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.MPS_M_TotalSaleLastYear", "Year", c => c.String(nullable: false, maxLength: 4));
            AddPrimaryKey("dbo.MPS_M_BudgetYearPlan", new[] { "UnitCode", "Year" });
            AddPrimaryKey("dbo.MPS_M_FinalApprover", new[] { "ProcessCode", "UnitCode", "Username" });
            AddPrimaryKey("dbo.MPS_M_TotalSaleLastYear", new[] { "BranchCode", "Year" });
            CreateIndex("dbo.MPS_M_FinalApprover", "ProcessCode");
            CreateIndex("dbo.MPS_M_FinalApprover", "Username");
            CreateIndex("dbo.MPS_M_TotalSaleLastYear", "BranchCode");
            AddForeignKey("dbo.MPS_M_FinalApprover", "ProcessCode", "dbo.MPS_M_ProcessApprove", "ProcessCode", cascadeDelete: true);
            AddForeignKey("dbo.MPS_M_TotalSaleLastYear", "BranchCode", "dbo.MPS_M_SharedBranch", "BranchCode", cascadeDelete: true);
            AddForeignKey("dbo.MPS_M_FinalApprover", "Username", "dbo.MPS_M_EmployeeTable", "EmpId", cascadeDelete: true);
            DropColumn("dbo.MPS_M_BudgetYearPlan", "BudgetYearID");
            DropColumn("dbo.MPS_M_FinalApprover", "Process");
            DropColumn("dbo.MPS_M_FinalApprover", "FromBudget");
            DropColumn("dbo.MPS_M_FinalApprover", "ToBudget");
            DropColumn("dbo.MPS_M_FinalApprover", "Operations");
            DropColumn("dbo.MPS_M_FinalApprover", "Order");
            DropColumn("dbo.MPS_M_TotalSaleLastYear", "Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.MPS_M_TotalSaleLastYear", "Id", c => c.Int(nullable: false, identity: true));
            AddColumn("dbo.MPS_M_FinalApprover", "Order", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.MPS_M_FinalApprover", "Operations", c => c.String());
            AddColumn("dbo.MPS_M_FinalApprover", "ToBudget", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.MPS_M_FinalApprover", "FromBudget", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.MPS_M_FinalApprover", "Process", c => c.String(maxLength: 100));
            AddColumn("dbo.MPS_M_BudgetYearPlan", "BudgetYearID", c => c.String(nullable: false, maxLength: 15));
            DropForeignKey("dbo.MPS_M_FinalApprover", "Username", "dbo.MPS_M_EmployeeTable");
            DropForeignKey("dbo.MPS_M_TotalSaleLastYear", "BranchCode", "dbo.MPS_M_SharedBranch");
            DropForeignKey("dbo.MPS_M_FinalApprover", "ProcessCode", "dbo.MPS_M_ProcessApprove");
            DropIndex("dbo.MPS_M_TotalSaleLastYear", new[] { "BranchCode" });
            DropIndex("dbo.MPS_M_FinalApprover", new[] { "Username" });
            DropIndex("dbo.MPS_M_FinalApprover", new[] { "ProcessCode" });
            DropPrimaryKey("dbo.MPS_M_TotalSaleLastYear");
            DropPrimaryKey("dbo.MPS_M_FinalApprover");
            DropPrimaryKey("dbo.MPS_M_BudgetYearPlan");
            AlterColumn("dbo.MPS_M_TotalSaleLastYear", "Year", c => c.String(maxLength: 4));
            AlterColumn("dbo.MPS_M_TotalSaleLastYear", "BranchCode", c => c.String(maxLength: 100));
            AlterColumn("dbo.MPS_M_FinalApprover", "Username", c => c.String(maxLength: 100));
            AlterColumn("dbo.MPS_M_FinalApprover", "Username", c => c.String(maxLength: 10));
            AlterColumn("dbo.MPS_M_BudgetYearPlan", "UnitCode", c => c.String(maxLength: 20));
            AlterColumn("dbo.MPS_M_BudgetYearPlan", "Year", c => c.String(maxLength: 4));
            AlterColumn("dbo.MPS_M_BudgetYearPlan", "Type", c => c.String(maxLength: 2));
            AlterColumn("dbo.MPS_M_UnitCode", "Type", c => c.String(maxLength: 2));
            DropColumn("dbo.MPS_M_FinalApprover", "Amount");
            DropColumn("dbo.MPS_M_FinalApprover", "UnitCode");
            DropColumn("dbo.MPS_MemoTable", "SpecialType");
            DropColumn("dbo.MPS_MemoTable", "PayToUnitCode");
            DropTable("dbo.MPS_M_MailGroup");
            DropTable("dbo.MPS_M_ProcessApprove");
            AddPrimaryKey("dbo.MPS_M_TotalSaleLastYear", "Id");
            AddPrimaryKey("dbo.MPS_M_FinalApprover", "ProcessCode");
            AddPrimaryKey("dbo.MPS_M_BudgetYearPlan", "BudgetYearID");
            RenameColumn(table: "dbo.MPS_M_FinalApprover", name: "Username", newName: "EmpId");
            AddColumn("dbo.MPS_M_FinalApprover", "Username", c => c.String(maxLength: 100));
            CreateIndex("dbo.MPS_M_FinalApprover", "EmpId");
            AddForeignKey("dbo.MPS_M_FinalApprover", "EmpId", "dbo.MPS_M_EmployeeTable", "EmpId");
        }
    }
}
