namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Table_AllocationBasis : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MPS_M_AllocationBasis",
                c => new
                    {
                        AllocCode = c.String(nullable: false, maxLength: 10),
                        Description = c.String(maxLength: 100),
                        InActive = c.Boolean(nullable: false),
                        MRT = c.Boolean(nullable: false),
                        B5 = c.Boolean(nullable: false),
                        B7 = c.Boolean(nullable: false),
                        M02 = c.Boolean(nullable: false),
                        M03 = c.Boolean(nullable: false),
                        M05 = c.Boolean(nullable: false),
                        M06 = c.Boolean(nullable: false),
                        M07 = c.Boolean(nullable: false),
                        M08 = c.Boolean(nullable: false),
                        M09 = c.Boolean(nullable: false),
                        M10 = c.Boolean(nullable: false),
                        M11 = c.Boolean(nullable: false),
                        M12 = c.Boolean(nullable: false),
                        M13 = c.Boolean(nullable: false),
                        M14 = c.Boolean(nullable: false),
                        M15 = c.Boolean(nullable: false),
                        M16 = c.Boolean(nullable: false),
                        M17 = c.Boolean(nullable: false),
                        V1 = c.Boolean(nullable: false),
                        V2 = c.Boolean(nullable: false),
                        V3 = c.Boolean(nullable: false),
                        V9 = c.Boolean(nullable: false),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.AllocCode);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.MPS_M_AllocationBasis");
        }
    }
}
