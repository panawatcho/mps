namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Table_PurchReqTable_PurchReqLine : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MPS_PurchReqLine",
                c => new
                    {
                        REQ_ID = c.Int(nullable: false),
                        ITEM_NUM = c.Int(nullable: false),
                        QUANTITY = c.Decimal(precision: 18, scale: 2),
                        ITEM_DESC = c.String(),
                        UOM = c.String(),
                        OLD_PRICE = c.String(),
                        PRICE = c.Decimal(precision: 18, scale: 2),
                        OLD_TAX_AMOUNT = c.String(),
                        TAX_AMOUNT = c.Decimal(precision: 18, scale: 2),
                        OLD_CURRENCY_CODE = c.String(),
                        CURRENCY_CODE = c.String(),
                        DELIV_DATE = c.DateTime(),
                        LONGDESC = c.String(),
                        SHIP_INST = c.String(),
                        PAYMENTDESC = c.String(),
                        ACTUALPRICE = c.Decimal(nullable: false, precision: 18, scale: 2),
                        COSTCENTER = c.String(),
                        PODOCTYPE = c.String(),
                        PURTYPE = c.String(),
                        GLACCOUNT = c.String(),
                        DEPARTMENT = c.String(),
                        PURGROUP = c.String(),
                        PROPOSALID = c.String(),
                        APCODE = c.String(),
                        REQUESTFORID = c.String(),
                        OLD_BUDGET_CODE = c.String(),
                        OLD_ORG_ID = c.String(),
                        StatusFlag = c.Int(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                        DeletedDate = c.DateTime(),
                        DeletedBy = c.String(),
                        DeletedByName = c.String(),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => new { t.REQ_ID, t.ITEM_NUM })
                .ForeignKey("dbo.MPS_PurchReqTable", t => t.REQ_ID, cascadeDelete: true)
                .Index(t => t.REQ_ID);
            
            CreateTable(
                "dbo.MPS_PurchReqTable",
                c => new
                    {
                        REQ_ID = c.Int(nullable: false),
                        IP_ID = c.String(),
                        PR_NUM = c.String(),
                        SUBCOMMAND = c.String(),
                        REQUISITIONER = c.String(),
                        BS_REQID = c.String(),
                        REQ_NAME = c.String(),
                        COMPANY_CODE = c.String(),
                        POST_DATE = c.DateTime(),
                        Status = c.String(),
                        StatusFlag = c.Int(nullable: false),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.REQ_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MPS_PurchReqLine", "REQ_ID", "dbo.MPS_PurchReqTable");
            DropIndex("dbo.MPS_PurchReqLine", new[] { "REQ_ID" });
            DropTable("dbo.MPS_PurchReqTable");
            DropTable("dbo.MPS_PurchReqLine");
        }
    }
}
