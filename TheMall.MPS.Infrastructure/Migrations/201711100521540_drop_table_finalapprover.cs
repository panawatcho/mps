namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class drop_table_finalapprover : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.MPS_M_FinalApprover", "Username", "dbo.MPS_M_EmployeeTable");
            DropForeignKey("dbo.MPS_M_FinalApprover", "ProcessCode", "dbo.MPS_M_ProcessApprove");
            DropIndex("dbo.MPS_M_FinalApprover", new[] { "ProcessCode" });
            DropIndex("dbo.MPS_M_FinalApprover", new[] { "Username" });
            DropTable("dbo.MPS_M_FinalApprover");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.MPS_M_FinalApprover",
                c => new
                    {
                        ProcessCode = c.String(nullable: false, maxLength: 50),
                        UnitCode = c.String(nullable: false, maxLength: 20),
                        Username = c.String(nullable: false, maxLength: 10),
                        Amount = c.Decimal(precision: 18, scale: 2),
                        InActive = c.Boolean(nullable: false),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => new { t.ProcessCode, t.UnitCode, t.Username });
            
            CreateIndex("dbo.MPS_M_FinalApprover", "Username");
            CreateIndex("dbo.MPS_M_FinalApprover", "ProcessCode");
            AddForeignKey("dbo.MPS_M_FinalApprover", "ProcessCode", "dbo.MPS_M_ProcessApprove", "ProcessCode", cascadeDelete: true);
            AddForeignKey("dbo.MPS_M_FinalApprover", "Username", "dbo.MPS_M_EmployeeTable", "EmpId", cascadeDelete: true);
        }
    }
}
