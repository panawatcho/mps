namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changeKeyAllocation : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.MPS_M_AllocationBasis");
            AddColumn("dbo.MPS_M_AllocationBasis", "Id", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.MPS_M_AllocationBasis", "AllocCode", c => c.String(maxLength: 10));
            AddPrimaryKey("dbo.MPS_M_AllocationBasis", "Id");
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.MPS_M_AllocationBasis");
            AlterColumn("dbo.MPS_M_AllocationBasis", "AllocCode", c => c.String(nullable: false, maxLength: 10));
            DropColumn("dbo.MPS_M_AllocationBasis", "Id");
            AddPrimaryKey("dbo.MPS_M_AllocationBasis", "AllocCode");
        }
    }
}
