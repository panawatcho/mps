namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addtableTotalsaleLastyear : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MPS_TotalSaleLastYear",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        BranchCode = c.String(maxLength: 100),
                        TotalSale = c.Decimal(precision: 18, scale: 2),
                        Year = c.String(maxLength: 4),
                        PercentTR = c.Decimal(precision: 18, scale: 2),
                        PercentRE = c.Decimal(precision: 18, scale: 2),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateIndex("dbo.MPS_M_UnitCodeForEmployee", "EmpId");
            AddForeignKey("dbo.MPS_M_UnitCodeForEmployee", "EmpId", "dbo.MPS_EmployeeTable", "EmpId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MPS_M_UnitCodeForEmployee", "EmpId", "dbo.MPS_EmployeeTable");
            DropIndex("dbo.MPS_M_UnitCodeForEmployee", new[] { "EmpId" });
            DropTable("dbo.MPS_TotalSaleLastYear");
        }
    }
}
