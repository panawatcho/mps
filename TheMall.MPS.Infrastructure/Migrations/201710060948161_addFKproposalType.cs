namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addFKproposalType : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.MPS_ProposalTable", "ProposalTypeCode");
            AddForeignKey("dbo.MPS_ProposalTable", "ProposalTypeCode", "dbo.MPS_M_TypeProposal", "TypeProposalCode");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MPS_ProposalTable", "ProposalTypeCode", "dbo.MPS_M_TypeProposal");
            DropIndex("dbo.MPS_ProposalTable", new[] { "ProposalTypeCode" });
        }
    }
}
