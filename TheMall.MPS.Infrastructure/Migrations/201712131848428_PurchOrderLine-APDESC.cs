namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PurchOrderLineAPDESC : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MPS_PurchOrderLine", "APDESC", c => c.String(maxLength: 255));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MPS_PurchOrderLine", "APDESC");
        }
    }
}
