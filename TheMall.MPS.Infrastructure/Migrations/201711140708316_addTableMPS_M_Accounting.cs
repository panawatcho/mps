namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addTableMPS_M_Accounting : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.MPS_M_APCode", "ExpenseTopicCode", "dbo.MPS_M_ExpenseTopic");
            DropIndex("dbo.MPS_M_APCode", new[] { "ExpenseTopicCode" });
            DropPrimaryKey("dbo.MPS_M_APCode");
            DropPrimaryKey("dbo.MPS_M_UnitCodeForEmployee");
            CreateTable(
                "dbo.MPS_M_Accounting",
                c => new
                    {
                        BranchCode = c.String(nullable: false, maxLength: 100),
                        Username = c.String(nullable: false, maxLength: 100),
                        Email = c.String(),
                        EmpId = c.String(maxLength: 10),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => new { t.BranchCode, t.Username })
                .ForeignKey("dbo.MPS_M_EmployeeTable", t => t.EmpId)
                .ForeignKey("dbo.MPS_M_SharedBranch", t => t.BranchCode, cascadeDelete: true)
                .Index(t => t.BranchCode)
                .Index(t => t.EmpId);
            
            AddColumn("dbo.MPS_MemoTable", "Branch", c => c.String(maxLength: 100));
            AlterColumn("dbo.MPS_M_APCode", "ExpenseTopicCode", c => c.String(nullable: false, maxLength: 20));
            AlterColumn("dbo.MPS_M_UnitCodeForEmployee", "Username", c => c.String(nullable: false, maxLength: 100));
            AddPrimaryKey("dbo.MPS_M_APCode", new[] { "APCode", "ExpenseTopicCode" });
            AddPrimaryKey("dbo.MPS_M_UnitCodeForEmployee", new[] { "EmpId", "UnitCode", "Username" });
            CreateIndex("dbo.MPS_M_APCode", "ExpenseTopicCode");
            AddForeignKey("dbo.MPS_M_APCode", "ExpenseTopicCode", "dbo.MPS_M_ExpenseTopic", "ExpenseTopicCode", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MPS_M_APCode", "ExpenseTopicCode", "dbo.MPS_M_ExpenseTopic");
            DropForeignKey("dbo.MPS_M_Accounting", "BranchCode", "dbo.MPS_M_SharedBranch");
            DropForeignKey("dbo.MPS_M_Accounting", "EmpId", "dbo.MPS_M_EmployeeTable");
            DropIndex("dbo.MPS_M_APCode", new[] { "ExpenseTopicCode" });
            DropIndex("dbo.MPS_M_Accounting", new[] { "EmpId" });
            DropIndex("dbo.MPS_M_Accounting", new[] { "BranchCode" });
            DropPrimaryKey("dbo.MPS_M_UnitCodeForEmployee");
            DropPrimaryKey("dbo.MPS_M_APCode");
            AlterColumn("dbo.MPS_M_UnitCodeForEmployee", "Username", c => c.String(maxLength: 100));
            AlterColumn("dbo.MPS_M_APCode", "ExpenseTopicCode", c => c.String(maxLength: 20));
            DropColumn("dbo.MPS_MemoTable", "Branch");
            DropTable("dbo.MPS_M_Accounting");
            AddPrimaryKey("dbo.MPS_M_UnitCodeForEmployee", new[] { "EmpId", "UnitCode" });
            AddPrimaryKey("dbo.MPS_M_APCode", "APCode");
            CreateIndex("dbo.MPS_M_APCode", "ExpenseTopicCode");
            AddForeignKey("dbo.MPS_M_APCode", "ExpenseTopicCode", "dbo.MPS_M_ExpenseTopic", "ExpenseTopicCode");
        }
    }
}
