namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addFieldtoProposal : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MPS_ProposalTable", "TotalOtherExpense", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.MPS_ProposalTable", "PercentOtherExpense", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.MPS_Log_ProposalTable", "PercentOtherExpense", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.MPS_Log_ProposalTable", "TotalOtherExpense", c => c.Decimal(precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MPS_Log_ProposalTable", "TotalOtherExpense");
            DropColumn("dbo.MPS_Log_ProposalTable", "PercentOtherExpense");
            DropColumn("dbo.MPS_ProposalTable", "PercentOtherExpense");
            DropColumn("dbo.MPS_ProposalTable", "TotalOtherExpense");
        }
    }
}
