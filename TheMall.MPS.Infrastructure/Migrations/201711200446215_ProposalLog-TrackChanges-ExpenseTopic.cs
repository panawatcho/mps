namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProposalLogTrackChangesExpenseTopic : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MPS_Log_ProposalExpenseTopic",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProposalLogId = c.Int(nullable: false),
                        ExpenseTopicCode = c.String(maxLength: 20),
                        Amount = c.Decimal(precision: 18, scale: 2),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MPS_Log_ProposalTable", t => t.ProposalLogId, cascadeDelete: true)
                .Index(t => t.ProposalLogId);
            
            CreateTable(
                "dbo.MPS_Log_ProposalTrackChanges",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ProposalId = c.Int(nullable: false),
                        ProposalLogId = c.Int(nullable: false),
                        RefTableName = c.String(maxLength: 200),
                        RefTableDesc = c.String(maxLength: 300),
                        RefTableId = c.Int(),
                        RefLogId = c.Int(),
                        FieldName = c.String(maxLength: 200),
                        FieldDescription = c.String(maxLength: 300),
                        State = c.String(maxLength: 200),
                        OldValue = c.String(),
                        NewValue = c.String(),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MPS_Log_ProposalExpenseTopic", "ProposalLogId", "dbo.MPS_Log_ProposalTable");
            DropIndex("dbo.MPS_Log_ProposalExpenseTopic", new[] { "ProposalLogId" });
            DropTable("dbo.MPS_Log_ProposalTrackChanges");
            DropTable("dbo.MPS_Log_ProposalExpenseTopic");
        }
    }
}
