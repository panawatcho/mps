namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeNameTable : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.MPS_ProcuirementTable", newName: "MPS_ProcurementTable");
            AddColumn("dbo.MPS_ProcurementTable", "ProcurementType", c => c.String(maxLength: 2));
            DropColumn("dbo.MPS_ProcurementTable", "ProcuirementType");
        }
        
        public override void Down()
        {
            AddColumn("dbo.MPS_ProcurementTable", "ProcuirementType", c => c.String(maxLength: 2));
            DropColumn("dbo.MPS_ProcurementTable", "ProcurementType");
            RenameTable(name: "dbo.MPS_ProcurementTable", newName: "MPS_ProcuirementTable");
        }
    }
}
