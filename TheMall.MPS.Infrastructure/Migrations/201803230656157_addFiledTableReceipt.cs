namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addFiledTableReceipt : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MPS_ReceiptLine", "StatusFlag", c => c.Int(nullable: false));
            AddColumn("dbo.MPS_ReceiptLine", "Deleted", c => c.Boolean(nullable: false));
            AddColumn("dbo.MPS_ReceiptLine", "DeletedDate", c => c.DateTime());
            AddColumn("dbo.MPS_ReceiptLine", "DeletedBy", c => c.String());
            AddColumn("dbo.MPS_ReceiptLine", "DeletedByName", c => c.String());
            AddColumn("dbo.MPS_ReceiptLine", "LineNo", c => c.Single(nullable: false));
            AddColumn("dbo.MPS_ReceiptTable", "BudgetDetail", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.MPS_ReceiptTable", "DocumentNumber", c => c.String(maxLength: 20));
            AddColumn("dbo.MPS_ReceiptTable", "Status", c => c.String(maxLength: 100));
            AddColumn("dbo.MPS_ReceiptTable", "StatusFlag", c => c.Int(nullable: false));
            AddColumn("dbo.MPS_ReceiptTable", "CompletedDate", c => c.DateTime());
            AddColumn("dbo.MPS_ReceiptTable", "CompletedByUserName", c => c.String(maxLength: 100));
            AddColumn("dbo.MPS_ReceiptTable", "CompletedByName", c => c.String(maxLength: 200));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MPS_ReceiptTable", "CompletedByName");
            DropColumn("dbo.MPS_ReceiptTable", "CompletedByUserName");
            DropColumn("dbo.MPS_ReceiptTable", "CompletedDate");
            DropColumn("dbo.MPS_ReceiptTable", "StatusFlag");
            DropColumn("dbo.MPS_ReceiptTable", "Status");
            DropColumn("dbo.MPS_ReceiptTable", "DocumentNumber");
            DropColumn("dbo.MPS_ReceiptTable", "BudgetDetail");
            DropColumn("dbo.MPS_ReceiptLine", "LineNo");
            DropColumn("dbo.MPS_ReceiptLine", "DeletedByName");
            DropColumn("dbo.MPS_ReceiptLine", "DeletedBy");
            DropColumn("dbo.MPS_ReceiptLine", "DeletedDate");
            DropColumn("dbo.MPS_ReceiptLine", "Deleted");
            DropColumn("dbo.MPS_ReceiptLine", "StatusFlag");
        }
    }
}
