namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IncomeOtherProposalLineId : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MPS_IncomeOther", "ProposalLineId", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.MPS_IncomeOther", "ProposalLineId");
        }
    }
}
