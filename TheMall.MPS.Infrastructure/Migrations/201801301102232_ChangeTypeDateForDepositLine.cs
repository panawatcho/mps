namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeTypeDateForDepositLine : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.MPS_DepositLine", "DatePeriod", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.MPS_DepositLine", "DatePeriod", c => c.String());
        }
    }
}
