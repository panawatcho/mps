namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addTableToAccounting : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.MPS_FinalApprover", newName: "MPS_M_FinalApprover");
            CreateTable(
                "dbo.MPS_AccountTrans",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Actual = c.Decimal(precision: 18, scale: 2),
                        ActualDate = c.DateTime(),
                        Remark = c.String(),
                        IncomeOtherId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MPS_IncomeOther", t => t.IncomeOtherId, cascadeDelete: true)
                .Index(t => t.IncomeOtherId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MPS_AccountTrans", "IncomeOtherId", "dbo.MPS_IncomeOther");
            DropIndex("dbo.MPS_AccountTrans", new[] { "IncomeOtherId" });
            DropTable("dbo.MPS_AccountTrans");
            RenameTable(name: "dbo.MPS_M_FinalApprover", newName: "MPS_FinalApprover");
        }
    }
}
