namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Fk_proposalLineId : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.MPS_IncomeOther", "ProposalLineId");
            AddForeignKey("dbo.MPS_IncomeOther", "ProposalLineId", "dbo.MPS_ProposalLine", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MPS_IncomeOther", "ProposalLineId", "dbo.MPS_ProposalLine");
            DropIndex("dbo.MPS_IncomeOther", new[] { "ProposalLineId" });
        }
    }
}
