namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PurchReqLineAPDESC : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MPS_PurchReqLine", "APDESC", c => c.String(maxLength: 255));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MPS_PurchReqLine", "APDESC");
        }
    }
}
