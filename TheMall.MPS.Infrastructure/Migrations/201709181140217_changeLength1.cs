namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changeLength1 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.MPS_ProposalTable", "ProposalTypeCode", c => c.String(maxLength: 10));
            AlterColumn("dbo.MPS_ProposalTable", "ProposalTypeName", c => c.String(maxLength: 100));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.MPS_ProposalTable", "ProposalTypeName", c => c.String(maxLength: 50));
            AlterColumn("dbo.MPS_ProposalTable", "ProposalTypeCode", c => c.String(maxLength: 5));
        }
    }
}
