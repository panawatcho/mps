namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addfieldlogcreateddate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MPS_Log_AccountTrans", "LogCreatedDate", c => c.DateTime());
            AddColumn("dbo.MPS_Log_AccountTrans", "LogCreatedBy", c => c.String(maxLength: 100));
            AddColumn("dbo.MPS_Log_IncomeOther", "LogCreatedDate", c => c.DateTime());
            AddColumn("dbo.MPS_Log_IncomeOther", "LogCreatedBy", c => c.String(maxLength: 100));
            AddColumn("dbo.MPS_Log_ProposalTable", "LogCreatedDate", c => c.DateTime());
            AddColumn("dbo.MPS_Log_ProposalTable", "LogCreatedBy", c => c.String(maxLength: 100));
            AddColumn("dbo.MPS_Log_DepositLine", "LogCreatedDate", c => c.DateTime());
            AddColumn("dbo.MPS_Log_DepositLine", "LogCreatedBy", c => c.String(maxLength: 100));
            AddColumn("dbo.MPS_Log_IncomeDeposit", "LogCreatedDate", c => c.DateTime());
            AddColumn("dbo.MPS_Log_IncomeDeposit", "LogCreatedBy", c => c.String(maxLength: 100));
            AddColumn("dbo.MPS_Log_IncomeTotalSale", "LogCreatedDate", c => c.DateTime());
            AddColumn("dbo.MPS_Log_IncomeTotalSale", "LogCreatedBy", c => c.String(maxLength: 100));
            AddColumn("dbo.MPS_Log_IncomeTotalSaleTemplate", "LogCreatedDate", c => c.DateTime());
            AddColumn("dbo.MPS_Log_IncomeTotalSaleTemplate", "LogCreatedBy", c => c.String(maxLength: 100));
            AddColumn("dbo.MPS_Log_ProposalApproval", "LogCreatedDate", c => c.DateTime());
            AddColumn("dbo.MPS_Log_ProposalApproval", "LogCreatedBy", c => c.String(maxLength: 100));
            AddColumn("dbo.MPS_Log_ProposalLine", "LogCreatedDate", c => c.DateTime());
            AddColumn("dbo.MPS_Log_ProposalLine", "LogCreatedBy", c => c.String(maxLength: 100));
            AddColumn("dbo.MPS_Log_ProposalLineTemplate", "LogCreatedDate", c => c.DateTime());
            AddColumn("dbo.MPS_Log_ProposalLineTemplate", "LogCreatedBy", c => c.String(maxLength: 100));
            AddColumn("dbo.MPS_Log_ProposalLineSharedTemplate", "LogCreatedDate", c => c.DateTime());
            AddColumn("dbo.MPS_Log_ProposalLineSharedTemplate", "LogCreatedBy", c => c.String(maxLength: 100));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MPS_Log_ProposalLineSharedTemplate", "LogCreatedBy");
            DropColumn("dbo.MPS_Log_ProposalLineSharedTemplate", "LogCreatedDate");
            DropColumn("dbo.MPS_Log_ProposalLineTemplate", "LogCreatedBy");
            DropColumn("dbo.MPS_Log_ProposalLineTemplate", "LogCreatedDate");
            DropColumn("dbo.MPS_Log_ProposalLine", "LogCreatedBy");
            DropColumn("dbo.MPS_Log_ProposalLine", "LogCreatedDate");
            DropColumn("dbo.MPS_Log_ProposalApproval", "LogCreatedBy");
            DropColumn("dbo.MPS_Log_ProposalApproval", "LogCreatedDate");
            DropColumn("dbo.MPS_Log_IncomeTotalSaleTemplate", "LogCreatedBy");
            DropColumn("dbo.MPS_Log_IncomeTotalSaleTemplate", "LogCreatedDate");
            DropColumn("dbo.MPS_Log_IncomeTotalSale", "LogCreatedBy");
            DropColumn("dbo.MPS_Log_IncomeTotalSale", "LogCreatedDate");
            DropColumn("dbo.MPS_Log_IncomeDeposit", "LogCreatedBy");
            DropColumn("dbo.MPS_Log_IncomeDeposit", "LogCreatedDate");
            DropColumn("dbo.MPS_Log_DepositLine", "LogCreatedBy");
            DropColumn("dbo.MPS_Log_DepositLine", "LogCreatedDate");
            DropColumn("dbo.MPS_Log_ProposalTable", "LogCreatedBy");
            DropColumn("dbo.MPS_Log_ProposalTable", "LogCreatedDate");
            DropColumn("dbo.MPS_Log_IncomeOther", "LogCreatedBy");
            DropColumn("dbo.MPS_Log_IncomeOther", "LogCreatedDate");
            DropColumn("dbo.MPS_Log_AccountTrans", "LogCreatedBy");
            DropColumn("dbo.MPS_Log_AccountTrans", "LogCreatedDate");
        }
    }
}
