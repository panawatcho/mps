namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changeLength2 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.MPS_DepositLine", "PaymentNo", c => c.String());
            AlterColumn("dbo.MPS_DepositLine", "DatePeriod", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.MPS_DepositLine", "DatePeriod", c => c.String(maxLength: 50));
            AlterColumn("dbo.MPS_DepositLine", "PaymentNo", c => c.String(maxLength: 50));
        }
    }
}
