namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Table_MPS_ProposalBudgetPlan : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MPS_ProposalBudgetPlan",
                c => new
                    {
                        ProposalId = c.Int(nullable: false),
                        APCode = c.String(nullable: false, maxLength: 20),
                        UnitCode = c.String(nullable: false, maxLength: 20),
                        BudgetPlan = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => new { t.ProposalId, t.APCode, t.UnitCode });
            
        }
        
        public override void Down()
        {
            DropTable("dbo.MPS_ProposalBudgetPlan");
        }
    }
}
