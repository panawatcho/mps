namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addTableApproverFinal : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MPS_FinalApprover",
                c => new
                    {
                        ProcessCode = c.String(nullable: false, maxLength: 50),
                        Process = c.String(maxLength: 100),
                        EmpId = c.String(maxLength: 10),
                        Username = c.String(maxLength: 100),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.ProcessCode);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.MPS_FinalApprover");
        }
    }
}
