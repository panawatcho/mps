namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MPS_VW_BudgetYearTracking : DbMigration
    {
        public override void Up()
        {
            this.Sql(@"CREATE VIEW [dbo].[MPS_VW_BudgetYearTracking] AS
                    SELECT ROW_NUMBER() OVER (ORDER BY Year,UnitCode) AS RowNo,*
                    ,ISNULL(ISNULL(Budget,0) - ISNULL(Reserve,0) + ISNULL([Return],0) + ISNULL(IncomeDeposit,0) + ISNULL(IncomeOther,0) + ISNULL(ReturnMemo,0),0) AS Remaining FROM 
                    (SELECT Year,UnitCode,UnitName,Budget
                    ,ISNULL((SELECT SUM(Amount) FROM [MPS].[dbo].[MPS_BudgetYearTrans] byt WHERE byt.UnitCode = mb.UnitCode AND byt.Year = mb.Year AND byt.DocumentStatus NOT IN ('Close','Return')),0) AS Reserve 
                    ,ISNULL((SELECT SUM(Amount) FROM [MPS].[dbo].[MPS_BudgetYearTrans] byt WHERE byt.UnitCode = mb.UnitCode AND byt.Year = mb.Year AND byt.DocumentStatus = 'Close' AND byt.Status = 'Return' AND byt.DocumentType = 'Proposal'),0) AS [Return]
                    ,ISNULL((SELECT SUM(Amount) FROM [MPS].[dbo].[MPS_BudgetYearTrans] byt WHERE byt.UnitCode = mb.UnitCode AND byt.Year = mb.Year AND byt.DocumentStatus = 'Close' AND byt.Status = 'IncomeDeposit' AND byt.DocumentType = 'Proposal'),0) AS IncomeDeposit
                    ,ISNULL((SELECT SUM(Amount) FROM [MPS].[dbo].[MPS_BudgetYearTrans] byt WHERE byt.UnitCode = mb.UnitCode AND byt.Year = mb.Year AND byt.DocumentStatus = 'Close' AND byt.Status = 'IncomeOther' AND byt.DocumentType = 'Proposal'),0) AS IncomeOther
                    ,ISNULL((SELECT SUM(Amount) FROM [MPS].[dbo].[MPS_BudgetYearTrans] byt WHERE byt.UnitCode = mb.UnitCode AND byt.Year = mb.Year AND byt.DocumentStatus = 'Return' AND byt.Status = 'Return' AND byt.DocumentType = 'Memo'),0) AS ReturnMemo 
                    FROM [MPS].[dbo].[MPS_M_BudgetYearPlan] mb ) AS BudgetYearTracking
                    ");
        }
        
        public override void Down()
        {
            this.Sql("DROP VIEW [dbo].[MPS_VW_BudgetYearTracking]");
        }
    }
}
