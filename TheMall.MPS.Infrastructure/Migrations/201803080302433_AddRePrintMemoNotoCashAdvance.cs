namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddRePrintMemoNotoCashAdvance : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MPS_CashAdvanceTable", "RePrintMemoNo", c => c.Int());
            AddColumn("dbo.MPS_MemoTable", "RePrintMemoNo", c => c.Int());
            AddColumn("dbo.MPS_ProposalTable", "RePrintMemoNo", c => c.Int());
            AddColumn("dbo.MPS_PettyCashTable", "RePrintMemoNo", c => c.Int());
            AddColumn("dbo.MPS_CashClearingTable", "RePrintMemoNo", c => c.Int());
            AddColumn("dbo.MPS_Log_ProposalTable", "RePrintMemoNo", c => c.Int());
            AddColumn("dbo.MPS_ProcurementTable", "RePrintMemoNo", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.MPS_ProcurementTable", "RePrintMemoNo");
            DropColumn("dbo.MPS_Log_ProposalTable", "RePrintMemoNo");
            DropColumn("dbo.MPS_CashClearingTable", "RePrintMemoNo");
            DropColumn("dbo.MPS_PettyCashTable", "RePrintMemoNo");
            DropColumn("dbo.MPS_ProposalTable", "RePrintMemoNo");
            DropColumn("dbo.MPS_MemoTable", "RePrintMemoNo");
            DropColumn("dbo.MPS_CashAdvanceTable", "RePrintMemoNo");
        }
    }
}
