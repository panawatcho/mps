namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addDepositLineIdToMemoIncomeTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MPS_MemoIncomeTable", "DepositLineId", c => c.Int());
            CreateIndex("dbo.MPS_MemoIncomeTable", "DepositLineId");
            AddForeignKey("dbo.MPS_MemoIncomeTable", "DepositLineId", "dbo.MPS_DepositLine", "Id");
            DropColumn("dbo.MPS_MemoIncomeTable", "DepositProposalRefID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.MPS_MemoIncomeTable", "DepositProposalRefID", c => c.Int());
            DropForeignKey("dbo.MPS_MemoIncomeTable", "DepositLineId", "dbo.MPS_DepositLine");
            DropIndex("dbo.MPS_MemoIncomeTable", new[] { "DepositLineId" });
            DropColumn("dbo.MPS_MemoIncomeTable", "DepositLineId");
        }
    }
}
