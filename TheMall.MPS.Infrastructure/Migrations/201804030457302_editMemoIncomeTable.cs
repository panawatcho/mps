namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editMemoIncomeTable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.MPS_MemoIncomeTable", "ProposalTable_Id", "dbo.MPS_ProposalTable");
            DropIndex("dbo.MPS_MemoIncomeTable", new[] { "ProposalTable_Id" });
            DropColumn("dbo.MPS_MemoIncomeTable", "ProposalRefID");
            RenameColumn(table: "dbo.MPS_MemoIncomeTable", name: "ProposalTable_Id", newName: "ProposalRefID");
            AddColumn("dbo.MPS_MemoIncomeTable", "Actual", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.MPS_MemoIncomeLine", "NetAmountNoVatTax", c => c.Decimal(precision: 18, scale: 2));
            AlterColumn("dbo.MPS_MemoIncomeTable", "ProposalRefID", c => c.Int(nullable: false));
            CreateIndex("dbo.MPS_MemoIncomeTable", "ProposalRefID");
            AddForeignKey("dbo.MPS_MemoIncomeTable", "ProposalRefID", "dbo.MPS_ProposalTable", "Id", cascadeDelete: true);
            DropColumn("dbo.MPS_MemoIncomeTable", "ActualCharge");
        }
        
        public override void Down()
        {
            AddColumn("dbo.MPS_MemoIncomeTable", "ActualCharge", c => c.Decimal(precision: 18, scale: 2));
            DropForeignKey("dbo.MPS_MemoIncomeTable", "ProposalRefID", "dbo.MPS_ProposalTable");
            DropIndex("dbo.MPS_MemoIncomeTable", new[] { "ProposalRefID" });
            AlterColumn("dbo.MPS_MemoIncomeTable", "ProposalRefID", c => c.Int());
            DropColumn("dbo.MPS_MemoIncomeLine", "NetAmountNoVatTax");
            DropColumn("dbo.MPS_MemoIncomeTable", "Actual");
            RenameColumn(table: "dbo.MPS_MemoIncomeTable", name: "ProposalRefID", newName: "ProposalTable_Id");
            AddColumn("dbo.MPS_MemoIncomeTable", "ProposalRefID", c => c.Int(nullable: false));
            CreateIndex("dbo.MPS_MemoIncomeTable", "ProposalTable_Id");
            AddForeignKey("dbo.MPS_MemoIncomeTable", "ProposalTable_Id", "dbo.MPS_ProposalTable", "Id");
        }
    }
}
