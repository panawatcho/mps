namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addfieldToFinalApprover : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MPS_FinalApprover", "FromBudget", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.MPS_FinalApprover", "ToBudget", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.MPS_FinalApprover", "Operations", c => c.String());
            AddColumn("dbo.MPS_FinalApprover", "InActive", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MPS_FinalApprover", "InActive");
            DropColumn("dbo.MPS_FinalApprover", "Operations");
            DropColumn("dbo.MPS_FinalApprover", "ToBudget");
            DropColumn("dbo.MPS_FinalApprover", "FromBudget");
        }
    }
}
