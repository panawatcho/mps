namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addTable_DepositAccountTrans : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MPS_DepositAccountTrans",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Actual = c.Decimal(precision: 18, scale: 2),
                        Remark = c.String(),
                        MemoIncomeId = c.Int(nullable: false),
                        DepositLineId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MPS_DepositLine", t => t.DepositLineId, cascadeDelete: true)
                .Index(t => t.DepositLineId);
            
            AddColumn("dbo.MPS_AccountTrans", "FlagMemoInvoice", c => c.Boolean(nullable: false));
            AddColumn("dbo.MPS_DepositLine", "InvoiceNo", c => c.String());
            AddColumn("dbo.MPS_DepositLine", "Actual", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.MPS_MemoIncomeTable", "DepositProposalRefID", c => c.Int());
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MPS_DepositAccountTrans", "DepositLineId", "dbo.MPS_DepositLine");
            DropIndex("dbo.MPS_DepositAccountTrans", new[] { "DepositLineId" });
            DropColumn("dbo.MPS_MemoIncomeTable", "DepositProposalRefID");
            DropColumn("dbo.MPS_DepositLine", "Actual");
            DropColumn("dbo.MPS_DepositLine", "InvoiceNo");
            DropColumn("dbo.MPS_AccountTrans", "FlagMemoInvoice");
            DropTable("dbo.MPS_DepositAccountTrans");
        }
    }
}
