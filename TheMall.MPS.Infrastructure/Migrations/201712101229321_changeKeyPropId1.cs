namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changeKeyPropId1 : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.MPS_ProposalBudgetPlan");
            AddPrimaryKey("dbo.MPS_ProposalBudgetPlan", new[] { "APCode", "UnitCode" });
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.MPS_ProposalBudgetPlan");
            AddPrimaryKey("dbo.MPS_ProposalBudgetPlan", new[] { "ProposalId", "APCode", "UnitCode" });
        }
    }
}
