namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFieldAndTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MPS_M_UnitCodeForEmployee",
                c => new
                    {
                        EmpId = c.String(nullable: false, maxLength: 10),
                        UnitCode = c.String(nullable: false, maxLength: 20),
                        Username = c.String(maxLength: 100),
                        UnitName = c.String(maxLength: 1000),
                        InActive = c.Boolean(nullable: false),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => new { t.EmpId, t.UnitCode });
            
            AddColumn("dbo.MPS_CashAdvanceTable", "AccountingUsername", c => c.String(maxLength: 100));
            AddColumn("dbo.MPS_MemoTable", "AccountingUsername", c => c.String(maxLength: 100));
            AddColumn("dbo.MPS_PettyCashTable", "AccountingUsername", c => c.String(maxLength: 100));
            AddColumn("dbo.MPS_ProposalTable", "ProjectStartDate", c => c.DateTime());
            AddColumn("dbo.MPS_ProposalTable", "ProjectEndDate", c => c.DateTime());
            AddColumn("dbo.MPS_ProposalTable", "TR", c => c.Boolean(nullable: false));
            AddColumn("dbo.MPS_ProposalTable", "RE", c => c.Boolean(nullable: false));
            AddColumn("dbo.MPS_ProposalTable", "AccountingUsername", c => c.String(maxLength: 100));
            AddColumn("dbo.MPS_CashClearingTable", "AccountingUsername", c => c.String(maxLength: 100));
            AddColumn("dbo.MPS_EmployeeTable", "DivisionCode", c => c.String(maxLength: 11));
            AddColumn("dbo.MPS_EmployeeTable", "DivisionName", c => c.String(maxLength: 200));
            AddColumn("dbo.MPS_EmployeeTable", "Authority", c => c.String(maxLength: 10));
            AddColumn("dbo.MPS_M_APcode", "SendMail", c => c.Boolean(nullable: false));
            AlterColumn("dbo.MPS_CashAdvanceLine", "Description", c => c.String());
            AlterColumn("dbo.MPS_MemoTable", "PaymentDepartment", c => c.String(maxLength: 200));
            AlterColumn("dbo.MPS_MemoLine", "Description", c => c.String());
            AlterColumn("dbo.MPS_PettyCashTable", "Description", c => c.String());
            AlterColumn("dbo.MPS_PettyCashLine", "Description", c => c.String());
            AlterColumn("dbo.MPS_IncomeOther", "SponcorName", c => c.String(maxLength: 200));
            AlterColumn("dbo.MPS_IncomeOther", "Description", c => c.String());
            AlterColumn("dbo.MPS_IncomeOther", "ContactName", c => c.String(maxLength: 200));
            AlterColumn("dbo.MPS_IncomeTotalSale", "Description", c => c.String());
            AlterColumn("dbo.MPS_CashClearingLine", "Description", c => c.String());
            AlterColumn("dbo.MPS_EmployeeTable", "DepartmentCode", c => c.String(maxLength: 10));
            AlterColumn("dbo.MPS_EmployeeTable", "PositionCode", c => c.String(maxLength: 10));
            DropColumn("dbo.MPS_ProposalTable", "TR_RE");
        }
        
        public override void Down()
        {
            AddColumn("dbo.MPS_ProposalTable", "TR_RE", c => c.String(maxLength: 50));
            AlterColumn("dbo.MPS_EmployeeTable", "PositionCode", c => c.String(maxLength: 20));
            AlterColumn("dbo.MPS_EmployeeTable", "DepartmentCode", c => c.String(maxLength: 20));
            AlterColumn("dbo.MPS_CashClearingLine", "Description", c => c.String(maxLength: 100));
            AlterColumn("dbo.MPS_IncomeTotalSale", "Description", c => c.String(maxLength: 100));
            AlterColumn("dbo.MPS_IncomeOther", "ContactName", c => c.String(maxLength: 100));
            AlterColumn("dbo.MPS_IncomeOther", "Description", c => c.String(maxLength: 100));
            AlterColumn("dbo.MPS_IncomeOther", "SponcorName", c => c.String(maxLength: 100));
            AlterColumn("dbo.MPS_PettyCashLine", "Description", c => c.String(maxLength: 100));
            AlterColumn("dbo.MPS_PettyCashTable", "Description", c => c.String(maxLength: 100));
            AlterColumn("dbo.MPS_MemoLine", "Description", c => c.String(maxLength: 100));
            AlterColumn("dbo.MPS_MemoTable", "PaymentDepartment", c => c.String());
            AlterColumn("dbo.MPS_CashAdvanceLine", "Description", c => c.String(maxLength: 100));
            DropColumn("dbo.MPS_M_APcode", "SendMail");
            DropColumn("dbo.MPS_EmployeeTable", "Authority");
            DropColumn("dbo.MPS_EmployeeTable", "DivisionName");
            DropColumn("dbo.MPS_EmployeeTable", "DivisionCode");
            DropColumn("dbo.MPS_CashClearingTable", "AccountingUsername");
            DropColumn("dbo.MPS_ProposalTable", "AccountingUsername");
            DropColumn("dbo.MPS_ProposalTable", "RE");
            DropColumn("dbo.MPS_ProposalTable", "TR");
            DropColumn("dbo.MPS_ProposalTable", "ProjectEndDate");
            DropColumn("dbo.MPS_ProposalTable", "ProjectStartDate");
            DropColumn("dbo.MPS_PettyCashTable", "AccountingUsername");
            DropColumn("dbo.MPS_MemoTable", "AccountingUsername");
            DropColumn("dbo.MPS_CashAdvanceTable", "AccountingUsername");
            DropTable("dbo.MPS_M_UnitCodeForEmployee");
        }
    }
}
