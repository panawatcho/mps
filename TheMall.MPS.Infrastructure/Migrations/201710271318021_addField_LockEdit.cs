namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addField_LockEdit : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MPS_ProposalApproval", "LockEditable", c => c.Boolean(nullable: false));
            AddColumn("dbo.MPS_CashAdvanceApproval", "LockEditable", c => c.Boolean(nullable: false));
            AddColumn("dbo.MPS_MemoApproval", "LockEditable", c => c.Boolean(nullable: false));
            AddColumn("dbo.MPS_PettyCashApproval", "LockEditable", c => c.Boolean(nullable: false));
            AddColumn("dbo.MPS_CashClearingApproval", "LockEditable", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MPS_CashClearingApproval", "LockEditable");
            DropColumn("dbo.MPS_PettyCashApproval", "LockEditable");
            DropColumn("dbo.MPS_MemoApproval", "LockEditable");
            DropColumn("dbo.MPS_CashAdvanceApproval", "LockEditable");
            DropColumn("dbo.MPS_ProposalApproval", "LockEditable");
        }
    }
}
