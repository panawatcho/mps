namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_MRTP_toAllocation : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MPS_M_AllocationBasis", "V5", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MPS_M_AllocationBasis", "V5");
        }
    }
}
