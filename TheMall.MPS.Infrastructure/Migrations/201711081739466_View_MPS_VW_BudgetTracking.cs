namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class View_MPS_VW_BudgetTracking : DbMigration
    {
        public override void Up()
        {
            this.Sql(@"CREATE VIEW [dbo].[MPS_VW_BudgetTracking] AS
SELECT       ROW_NUMBER() OVER (ORDER BY OpenProp.ProposalId) AS RowNo ,OpenProp.ProposalId, OpenProp.ProposalLineId, OpenProp.ProposalNo, OpenProp.ProposalUnitCode, OpenProp.UnitCode, OpenProp.APCode, OpenProp.BudgetPlan, ISNULL(BudgetTracking.BudgetUsed, 0) 
                         AS BudgetUsed, ISNULL(BudgetTracking.Memo,0) AS Memo,ISNULL(BudgetTracking.CashAdvance,0) AS CashAdvance, ISNULL(BudgetTracking.PettyCash,0) AS PettyCash, ISNULL(BudgetTracking.PR,0) AS PR, ISNULL(BudgetTracking.PO,0) AS PO, ISNULL(BudgetTracking.GR,0) AS GR, 
                         OpenProp.BudgetPlan - ISNULL(BudgetTracking.BudgetUsed, 0) AS BudgetRemaining,CloseFlag
FROM            (SELECT        pt.Id AS ProposalId, pl.Id AS ProposalLineId, pt.DocumentNumber AS ProposalNo, pt.UnitCode AS ProposalUnitCode, pl.UnitCode, pl.APCode, pl.BudgetPlan,pt.CloseFlag
                          FROM            dbo.MPS_ProposalTable AS pt INNER JOIN
                                                    dbo.MPS_ProposalLine AS pl ON pt.Id = pl.ParentId
                          WHERE        (pt.StatusFlag = 9) AND (pt.ProposalTypeCode NOT IN ('DEPOSIT', 'DEPOSITIN')) AND (pl.APCode IS NOT NULL) AND (pl.UnitCode IS NOT NULL)) AS OpenProp LEFT OUTER JOIN
                             (SELECT        pl.ParentId AS ProposalId, pl.Id AS ProposalLineId, pt.DocumentNumber AS ProposalNo, pl.UnitCode, pl.APCode, pl.BudgetPlan, TRANS.Amount AS BudgetUsed,
                                                             (SELECT        SUM(Amount) AS Expr1
                                                               FROM            dbo.MPS_BudgetProposalTrans
                                                               WHERE        (ProposalLineId = TRANS.ProposalLineId) AND (DocumentType = 'Memo')) AS Memo,
                                                             (SELECT        SUM(Amount) AS Expr1
                                                               FROM            dbo.MPS_BudgetProposalTrans AS MPS_BudgetProposalTrans_5
                                                               WHERE        (ProposalLineId = TRANS.ProposalLineId) AND (DocumentType = 'cashadvance')) AS CashAdvance,
                                                             (SELECT        SUM(Amount) AS Expr1
                                                               FROM            dbo.MPS_BudgetProposalTrans AS MPS_BudgetProposalTrans_4
                                                               WHERE        (ProposalLineId = TRANS.ProposalLineId) AND (DocumentType = 'pettycash')) AS PettyCash,
                                                             (SELECT        SUM(Amount) AS Expr1
                                                               FROM            dbo.MPS_BudgetProposalTrans AS MPS_BudgetProposalTrans_3
                                                               WHERE        (ProposalLineId = TRANS.ProposalLineId) AND (DocumentType = 'PR')) AS PR,
                                                             (SELECT        SUM(Amount) AS Expr1
                                                               FROM            dbo.MPS_BudgetProposalTrans AS MPS_BudgetProposalTrans_2
                                                               WHERE        (ProposalLineId = TRANS.ProposalLineId) AND (DocumentType = 'PO')) AS PO,
                                                             (SELECT        SUM(Amount) AS Expr1
                                                               FROM            dbo.MPS_BudgetProposalTrans AS MPS_BudgetProposalTrans_1
                                                               WHERE        (ProposalLineId = TRANS.ProposalLineId) AND (DocumentType = 'GR')) AS GR, pl.BudgetPlan - TRANS.Amount AS BudgetRemaining
                               FROM            (SELECT        ProposalLineId, SUM(Amount) AS Amount
                                                         FROM            dbo.MPS_BudgetProposalTrans AS bpt
                                                         GROUP BY ProposalLineId) AS TRANS INNER JOIN
                                                         dbo.MPS_ProposalLine AS pl ON TRANS.ProposalLineId = pl.Id INNER JOIN
                                                         dbo.MPS_ProposalTable AS pt ON pt.Id = pl.ParentId) AS BudgetTracking ON OpenProp.ProposalLineId = BudgetTracking.ProposalLineId");
        }

        public override void Down()
        {
            this.Sql("DROP VIEW [dbo].[MPS_VW_BudgetTracking]");
        }
    }
}
