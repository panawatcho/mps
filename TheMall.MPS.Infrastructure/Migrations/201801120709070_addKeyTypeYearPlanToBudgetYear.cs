namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addKeyTypeYearPlanToBudgetYear : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.MPS_M_BudgetYearPlan");
            AlterColumn("dbo.MPS_M_BudgetYearPlan", "TypeYearPlan", c => c.String(nullable: false, maxLength: 50));
            AddPrimaryKey("dbo.MPS_M_BudgetYearPlan", new[] { "UnitCode", "Year", "TypeYearPlan" });
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.MPS_M_BudgetYearPlan");
            AlterColumn("dbo.MPS_M_BudgetYearPlan", "TypeYearPlan", c => c.String(maxLength: 50));
            AddPrimaryKey("dbo.MPS_M_BudgetYearPlan", new[] { "UnitCode", "Year" });
        }
    }
}
