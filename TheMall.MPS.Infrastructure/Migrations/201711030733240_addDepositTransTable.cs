namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addDepositTransTable : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.MPS_EmployeeTable", newName: "MPS_M_EmployeeTable");
            RenameTable(name: "dbo.MPS_TotalSaleLastYear", newName: "MPS_M_TotalSaleLastYear");
            CreateTable(
                "dbo.MPS_BudgetDepositTrans",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProposalDepositId = c.Int(nullable: false),
                        Status = c.String(maxLength: 100),
                        DocumentStatus = c.String(maxLength: 100),
                        DocumentType = c.String(maxLength: 100),
                        DocumentId = c.Int(),
                        DocumentNo = c.String(maxLength: 20),
                        RefDocumentType = c.String(maxLength: 100),
                        RefDocumentId = c.Int(),
                        RefDocumentNumber = c.String(maxLength: 20),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TransDate = c.DateTime(nullable: false),
                        InActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MPS_ProposalTable", t => t.ProposalDepositId, cascadeDelete: true)
                .Index(t => t.ProposalDepositId);
            
            AddColumn("dbo.MPS_M_SharedBranch", "BranchAddress", c => c.String());
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MPS_BudgetDepositTrans", "ProposalDepositId", "dbo.MPS_ProposalTable");
            DropIndex("dbo.MPS_BudgetDepositTrans", new[] { "ProposalDepositId" });
            DropColumn("dbo.MPS_M_SharedBranch", "BranchAddress");
            DropTable("dbo.MPS_BudgetDepositTrans");
            RenameTable(name: "dbo.MPS_M_TotalSaleLastYear", newName: "MPS_TotalSaleLastYear");
            RenameTable(name: "dbo.MPS_M_EmployeeTable", newName: "MPS_EmployeeTable");
        }
    }
}
