namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class fieldCCmail : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MPS_ProposalTable", "InformUsername", c => c.String());
            AddColumn("dbo.MPS_ProposalTable", "InformEmail", c => c.String());
            AddColumn("dbo.MPS_ProposalTable", "CCUsername", c => c.String());
            AddColumn("dbo.MPS_ProposalTable", "CCEmail", c => c.String());
            AddColumn("dbo.MPS_CashAdvanceTable", "InformUsername", c => c.String());
            AddColumn("dbo.MPS_CashAdvanceTable", "InformEmail", c => c.String());
            AddColumn("dbo.MPS_CashAdvanceTable", "CCUsername", c => c.String());
            AddColumn("dbo.MPS_CashAdvanceTable", "CCEmail", c => c.String());
            AddColumn("dbo.MPS_MemoTable", "InformUsername", c => c.String());
            AddColumn("dbo.MPS_MemoTable", "InformEmail", c => c.String());
            AddColumn("dbo.MPS_MemoTable", "CCUsername", c => c.String());
            AddColumn("dbo.MPS_MemoTable", "CCEmail", c => c.String());
            AddColumn("dbo.MPS_PettyCashTable", "InformUsername", c => c.String());
            AddColumn("dbo.MPS_PettyCashTable", "InformEmail", c => c.String());
            AddColumn("dbo.MPS_PettyCashTable", "CCUsername", c => c.String());
            AddColumn("dbo.MPS_PettyCashTable", "CCEmail", c => c.String());
            AddColumn("dbo.MPS_CashClearingTable", "InformUsername", c => c.String());
            AddColumn("dbo.MPS_CashClearingTable", "InformEmail", c => c.String());
            AddColumn("dbo.MPS_CashClearingTable", "CCUsername", c => c.String());
            AddColumn("dbo.MPS_CashClearingTable", "CCEmail", c => c.String());
            AddColumn("dbo.MPS_ProcurementTable", "InformUsername", c => c.String());
            AddColumn("dbo.MPS_ProcurementTable", "InformEmail", c => c.String());
            AddColumn("dbo.MPS_ProcurementTable", "CCUsername", c => c.String());
            AddColumn("dbo.MPS_ProcurementTable", "CCEmail", c => c.String());
            AlterColumn("dbo.MPS_ProposalTable", "AccountingUsername", c => c.String());
            AlterColumn("dbo.MPS_ProposalTable", "AccountingEmail", c => c.String());
            AlterColumn("dbo.MPS_CashAdvanceTable", "AccountingUsername", c => c.String());
            AlterColumn("dbo.MPS_CashAdvanceTable", "AccountingEmail", c => c.String());
            AlterColumn("dbo.MPS_MemoTable", "AccountingUsername", c => c.String());
            AlterColumn("dbo.MPS_MemoTable", "AccountingEmail", c => c.String());
            AlterColumn("dbo.MPS_PettyCashTable", "AccountingUsername", c => c.String());
            AlterColumn("dbo.MPS_PettyCashTable", "AccountingEmail", c => c.String());
            AlterColumn("dbo.MPS_CashClearingTable", "AccountingUsername", c => c.String());
            AlterColumn("dbo.MPS_CashClearingTable", "AccountingEmail", c => c.String());
            AlterColumn("dbo.MPS_ProcurementTable", "AccountingUsername", c => c.String());
            AlterColumn("dbo.MPS_ProcurementTable", "AccountingEmail", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.MPS_ProcurementTable", "AccountingEmail", c => c.String(maxLength: 200));
            AlterColumn("dbo.MPS_ProcurementTable", "AccountingUsername", c => c.String(maxLength: 100));
            AlterColumn("dbo.MPS_CashClearingTable", "AccountingEmail", c => c.String(maxLength: 200));
            AlterColumn("dbo.MPS_CashClearingTable", "AccountingUsername", c => c.String(maxLength: 100));
            AlterColumn("dbo.MPS_PettyCashTable", "AccountingEmail", c => c.String(maxLength: 200));
            AlterColumn("dbo.MPS_PettyCashTable", "AccountingUsername", c => c.String(maxLength: 100));
            AlterColumn("dbo.MPS_MemoTable", "AccountingEmail", c => c.String(maxLength: 200));
            AlterColumn("dbo.MPS_MemoTable", "AccountingUsername", c => c.String(maxLength: 100));
            AlterColumn("dbo.MPS_CashAdvanceTable", "AccountingEmail", c => c.String(maxLength: 200));
            AlterColumn("dbo.MPS_CashAdvanceTable", "AccountingUsername", c => c.String(maxLength: 100));
            AlterColumn("dbo.MPS_ProposalTable", "AccountingEmail", c => c.String(maxLength: 200));
            AlterColumn("dbo.MPS_ProposalTable", "AccountingUsername", c => c.String(maxLength: 100));
            DropColumn("dbo.MPS_ProcurementTable", "CCEmail");
            DropColumn("dbo.MPS_ProcurementTable", "CCUsername");
            DropColumn("dbo.MPS_ProcurementTable", "InformEmail");
            DropColumn("dbo.MPS_ProcurementTable", "InformUsername");
            DropColumn("dbo.MPS_CashClearingTable", "CCEmail");
            DropColumn("dbo.MPS_CashClearingTable", "CCUsername");
            DropColumn("dbo.MPS_CashClearingTable", "InformEmail");
            DropColumn("dbo.MPS_CashClearingTable", "InformUsername");
            DropColumn("dbo.MPS_PettyCashTable", "CCEmail");
            DropColumn("dbo.MPS_PettyCashTable", "CCUsername");
            DropColumn("dbo.MPS_PettyCashTable", "InformEmail");
            DropColumn("dbo.MPS_PettyCashTable", "InformUsername");
            DropColumn("dbo.MPS_MemoTable", "CCEmail");
            DropColumn("dbo.MPS_MemoTable", "CCUsername");
            DropColumn("dbo.MPS_MemoTable", "InformEmail");
            DropColumn("dbo.MPS_MemoTable", "InformUsername");
            DropColumn("dbo.MPS_CashAdvanceTable", "CCEmail");
            DropColumn("dbo.MPS_CashAdvanceTable", "CCUsername");
            DropColumn("dbo.MPS_CashAdvanceTable", "InformEmail");
            DropColumn("dbo.MPS_CashAdvanceTable", "InformUsername");
            DropColumn("dbo.MPS_ProposalTable", "CCEmail");
            DropColumn("dbo.MPS_ProposalTable", "CCUsername");
            DropColumn("dbo.MPS_ProposalTable", "InformEmail");
            DropColumn("dbo.MPS_ProposalTable", "InformUsername");
        }
    }
}
