namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addCompanyCashadvance : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MPS_CashAdvanceTable", "VendorCode", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.MPS_CashAdvanceTable", "VendorCode");
        }
    }
}
