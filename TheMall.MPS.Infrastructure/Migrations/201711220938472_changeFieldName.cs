namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changeFieldName : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MPS_ProposalTable", "AccountingBranch", c => c.String(maxLength: 100));
            AddColumn("dbo.MPS_CashAdvanceTable", "AccountingBranch", c => c.String(maxLength: 100));
            AddColumn("dbo.MPS_MemoTable", "AccountingBranch", c => c.String(maxLength: 100));
            AddColumn("dbo.MPS_PettyCashTable", "AccountingBranch", c => c.String(maxLength: 100));
            AddColumn("dbo.MPS_CashClearingTable", "AccountingBranch", c => c.String(maxLength: 100));
            AddColumn("dbo.MPS_Log_ProposalTable", "AccountingBranch", c => c.String(maxLength: 100));
            AddColumn("dbo.MPS_ProcurementTable", "AccountingBranch", c => c.String(maxLength: 100));
            DropColumn("dbo.MPS_ProposalTable", "AccoutingBranch");
            DropColumn("dbo.MPS_CashAdvanceTable", "AccoutingBranch");
            DropColumn("dbo.MPS_MemoTable", "AccoutingBranch");
            DropColumn("dbo.MPS_PettyCashTable", "AccoutingBranch");
            DropColumn("dbo.MPS_CashClearingTable", "AccoutingBranch");
            DropColumn("dbo.MPS_Log_ProposalTable", "AccoutingBranch");
            DropColumn("dbo.MPS_ProcurementTable", "AccoutingBranch");
        }
        
        public override void Down()
        {
            AddColumn("dbo.MPS_ProcurementTable", "AccoutingBranch", c => c.String(maxLength: 100));
            AddColumn("dbo.MPS_Log_ProposalTable", "AccoutingBranch", c => c.String(maxLength: 100));
            AddColumn("dbo.MPS_CashClearingTable", "AccoutingBranch", c => c.String(maxLength: 100));
            AddColumn("dbo.MPS_PettyCashTable", "AccoutingBranch", c => c.String(maxLength: 100));
            AddColumn("dbo.MPS_MemoTable", "AccoutingBranch", c => c.String(maxLength: 100));
            AddColumn("dbo.MPS_CashAdvanceTable", "AccoutingBranch", c => c.String(maxLength: 100));
            AddColumn("dbo.MPS_ProposalTable", "AccoutingBranch", c => c.String(maxLength: 100));
            DropColumn("dbo.MPS_ProcurementTable", "AccountingBranch");
            DropColumn("dbo.MPS_Log_ProposalTable", "AccountingBranch");
            DropColumn("dbo.MPS_CashClearingTable", "AccountingBranch");
            DropColumn("dbo.MPS_PettyCashTable", "AccountingBranch");
            DropColumn("dbo.MPS_MemoTable", "AccountingBranch");
            DropColumn("dbo.MPS_CashAdvanceTable", "AccountingBranch");
            DropColumn("dbo.MPS_ProposalTable", "AccountingBranch");
        }
    }
}
