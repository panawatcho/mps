namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addFieldDepositApprover : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MPS_ProposalTable", "DepositApprover", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.MPS_ProposalTable", "DepositApprover");
        }
    }
}
