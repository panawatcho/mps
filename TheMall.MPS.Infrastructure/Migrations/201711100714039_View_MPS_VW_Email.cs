namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class View_MPS_VW_Email : DbMigration
    {
        public override void Up()
        {
            this.Sql(@"CREATE VIEW [dbo].[MPS_VW_Email] AS
                    SELECT ROW_NUMBER() OVER (ORDER BY Name) AS RowNo,* FROM (SELECT Username AS Name,Email, 'User' AS TypeMail
                    FROM [dbo].[MPS_M_EmployeeTable]
                    WHERE Email IS NOT NULL AND EmpStatus = 'Active'
                    UNION
                    SELECT GroupCode AS Name, Email,'Group' AS TypeMail
                    FROM [dbo].[MPS_M_MailGroup]
                    WHERE Inactive = 0) AS UserGroupEmail
                    ");
        }
        
        public override void Down()
        {
            this.Sql("DROP VIEW [dbo].[MPS_VW_Email]");
        }
    }
}
