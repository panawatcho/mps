namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editfiledTableReceiptandReceiptLine : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MPS_ReceiptTable", "UnitCodeFor", c => c.String(maxLength: 20));
            AddColumn("dbo.MPS_ReceiptTable", "UnitNameFor", c => c.String(maxLength: 1000));
            AddColumn("dbo.MPS_ReceiptTable", "BudgetNoVatTax", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.MPS_ReceiptTable", "Deleted", c => c.Boolean(nullable: false));
            AddColumn("dbo.MPS_ReceiptTable", "DeletedDate", c => c.DateTime());
            AddColumn("dbo.MPS_ReceiptTable", "DeletedBy", c => c.String());
            AddColumn("dbo.MPS_ReceiptTable", "DeletedByName", c => c.String());
            DropColumn("dbo.MPS_ReceiptLine", "StatusFlag");
        }
        
        public override void Down()
        {
            AddColumn("dbo.MPS_ReceiptLine", "StatusFlag", c => c.Int(nullable: false));
            DropColumn("dbo.MPS_ReceiptTable", "DeletedByName");
            DropColumn("dbo.MPS_ReceiptTable", "DeletedBy");
            DropColumn("dbo.MPS_ReceiptTable", "DeletedDate");
            DropColumn("dbo.MPS_ReceiptTable", "Deleted");
            DropColumn("dbo.MPS_ReceiptTable", "BudgetNoVatTax");
            DropColumn("dbo.MPS_ReceiptTable", "UnitNameFor");
            DropColumn("dbo.MPS_ReceiptTable", "UnitCodeFor");
        }
    }
}
