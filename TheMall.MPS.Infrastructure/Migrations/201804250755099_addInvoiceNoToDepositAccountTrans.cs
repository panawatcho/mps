namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addInvoiceNoToDepositAccountTrans : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MPS_DepositAccountTrans", "InvoiceNo", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.MPS_DepositAccountTrans", "InvoiceNo");
        }
    }
}
