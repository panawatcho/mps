namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changeKeyPropId2 : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.MPS_ProposalBudgetPlan", "ProposalId");
            AddForeignKey("dbo.MPS_ProposalBudgetPlan", "ProposalId", "dbo.MPS_ProposalTable", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MPS_ProposalBudgetPlan", "ProposalId", "dbo.MPS_ProposalTable");
            DropIndex("dbo.MPS_ProposalBudgetPlan", new[] { "ProposalId" });
        }
    }
}
