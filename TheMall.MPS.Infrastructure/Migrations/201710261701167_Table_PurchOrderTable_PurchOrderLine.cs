namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Table_PurchOrderTable_PurchOrderLine : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MPS_PurchOrderLine",
                c => new
                    {
                        PO_ID = c.Int(nullable: false),
                        ITEM_NUM = c.Int(nullable: false),
                        PR_LINE_NUM = c.Int(),
                        QUANTITY = c.Decimal(precision: 18, scale: 2),
                        ITEM_DESC = c.String(maxLength: 1000),
                        UOM = c.String(maxLength: 200),
                        OLD_PRICE = c.String(maxLength: 200),
                        PRICE = c.Decimal(precision: 18, scale: 2),
                        OLD_TAX_AMOUNT = c.String(maxLength: 200),
                        TAX_AMOUNT = c.Decimal(precision: 18, scale: 2),
                        OLD_CURRENCY_CODE = c.String(maxLength: 200),
                        CURRENCY_CODE = c.String(maxLength: 200),
                        DELIV_DATE = c.DateTime(),
                        ACTUALPRICE = c.Decimal(nullable: false, precision: 18, scale: 2),
                        COSTCENTER = c.String(maxLength: 200),
                        PODOCTYPE = c.String(maxLength: 200),
                        PURTYPE = c.String(maxLength: 200),
                        GLACCOUNT = c.String(maxLength: 200),
                        DEPARTMENT = c.String(maxLength: 200),
                        PURGROUP = c.String(maxLength: 200),
                        PROPOSALID = c.String(maxLength: 200),
                        APCODE = c.String(maxLength: 200),
                        REQUESTFORID = c.String(maxLength: 200),
                        OLD_BUDGET_CODE = c.String(maxLength: 200),
                        OLD_ORG_ID = c.String(maxLength: 200),
                        DeliveryComplete = c.Boolean(nullable: false),
                        StatusFlag = c.Int(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                        DeletedDate = c.DateTime(),
                        DeletedBy = c.String(maxLength: 100),
                        DeletedByName = c.String(maxLength: 200),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => new { t.PO_ID, t.ITEM_NUM })
                .ForeignKey("dbo.MPS_PurchOrderTable", t => t.PO_ID, cascadeDelete: true)
                .Index(t => t.PO_ID);
            
            CreateTable(
                "dbo.MPS_PurchOrderTable",
                c => new
                    {
                        PO_ID = c.Int(nullable: false),
                        PO_NUM = c.String(maxLength: 200),
                        IP_ID = c.String(maxLength: 200),
                        REQ_ID = c.Int(nullable: false),
                        PR_NUM = c.String(maxLength: 200),
                        PO_DATE = c.DateTime(),
                        ClosePO = c.Boolean(nullable: false),
                        Status = c.String(maxLength: 200),
                        StatusFlag = c.Int(nullable: false),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.PO_ID)
                .ForeignKey("dbo.MPS_PurchReqTable", t => t.REQ_ID, cascadeDelete: true)
                .Index(t => t.REQ_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MPS_PurchOrderTable", "REQ_ID", "dbo.MPS_PurchReqTable");
            DropForeignKey("dbo.MPS_PurchOrderLine", "PO_ID", "dbo.MPS_PurchOrderTable");
            DropIndex("dbo.MPS_PurchOrderTable", new[] { "REQ_ID" });
            DropIndex("dbo.MPS_PurchOrderLine", new[] { "PO_ID" });
            DropTable("dbo.MPS_PurchOrderTable");
            DropTable("dbo.MPS_PurchOrderLine");
        }
    }
}
