namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addCompanyCashClearing : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MPS_CashClearingTable", "VendorCode", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.MPS_CashClearingTable", "VendorCode");
        }
    }
}
