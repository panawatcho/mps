namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class View_MPS_VW_DepositTracking : DbMigration
    {
        public override void Up()
        {
            this.Sql(@"CREATE VIEW [dbo].[MPS_VW_DepositTracking] AS
                    SELECT ROW_NUMBER() OVER (ORDER BY ProposalId) AS RowNo,* FROM (SELECT Id AS ProposalId,DocumentNumber AS ProposalDocNo, 'Deposit Internal' AS [Type],DepositId,TotalBudget, [Status] FROM MPS_ProposalTable pt
                    WHERE DepositId IS NOT NULL AND DepositId > 0
                    UNION
                    SELECT pt.Id AS ProposalId,pt.DocumentNumber AS ProposalDocNo, 'Income Deposit'AS [Type],ProposalDepositRefId AS DepositId,IncomeDeposit.TotalBudget ,
                    (SELECT TOP 1 [Status] FROM MPS_IncomeDeposit icd WHERE icd.ParentId = IncomeDeposit.ParentId AND icd.ProposalDepositRefId = IncomeDeposit.ProposalDepositRefId AND Deleted = 0 ORDER BY ID DESC) AS [Status]
                    FROM 
                    (SELECT ParentId,ProposalDepositRefId, SUM(Budget) AS TotalBudget FROM MPS_IncomeDeposit WHERE Deleted = 0 GROUP BY ParentId,ProposalDepositRefId) AS IncomeDeposit
                    INNER JOIN MPS_ProposalTable pt
                    ON IncomeDeposit.ParentId = pt.Id) AS DepositTracking
                    ");
        }
        
        public override void Down()
        {
            this.Sql("DROP VIEW [dbo].[MPS_VW_DepositTracking]");
        }
    }
}
