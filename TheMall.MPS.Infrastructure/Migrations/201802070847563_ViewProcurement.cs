namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ViewProcurement : DbMigration
    {
        public override void Up()
        {
            this.Sql(@"CREATE VIEW [dbo].[MPS_VW_BudgetYearTracking] AS
 SELECT ROW_NUMBER() OVER (ORDER BY Year,UnitCode) AS RowNo,*
                    ,ISNULL(ISNULL(Budget,0) - ISNULL(Reserve,0) + ISNULL([Return],0) + ISNULL(IncomeDeposit,0) + ISNULL(IncomeOther,0) + ISNULL(ReturnMemo,0),0) AS Remaining FROM 
                    (SELECT Year,UnitCode,UnitName,Budget,TypeYearPlan
                    ,ISNULL((SELECT SUM(Amount) FROM [MPS].[dbo].[MPS_BudgetYearTrans] byt WHERE byt.TypeYearPlan = mb.TypeYearPlan AND byt.UnitCode = mb.UnitCode AND byt.Year = mb.Year AND byt.DocumentStatus NOT IN ('Close','Return')),0) AS Reserve 
                    ,ISNULL((SELECT SUM(Amount) FROM [MPS].[dbo].[MPS_BudgetYearTrans] byt WHERE byt.TypeYearPlan = mb.TypeYearPlan AND byt.UnitCode = mb.UnitCode AND byt.Year = mb.Year AND byt.DocumentStatus = 'Close' AND byt.Status = 'Return' AND byt.DocumentType = 'Proposal'),0) AS [Return]
                    ,ISNULL((SELECT SUM(Amount) FROM [MPS].[dbo].[MPS_BudgetYearTrans] byt WHERE byt.TypeYearPlan = mb.TypeYearPlan AND byt.UnitCode = mb.UnitCode AND byt.Year = mb.Year AND byt.DocumentStatus = 'Close' AND byt.Status = 'IncomeDeposit' AND byt.DocumentType = 'Proposal'),0) AS IncomeDeposit
                    ,ISNULL((SELECT SUM(Amount) FROM [MPS].[dbo].[MPS_BudgetYearTrans] byt WHERE byt.TypeYearPlan = mb.TypeYearPlan AND byt.UnitCode = mb.UnitCode AND byt.Year = mb.Year AND byt.DocumentStatus = 'Close' AND byt.Status = 'IncomeOther' AND byt.DocumentType = 'Proposal'),0) AS IncomeOther
                    ,ISNULL((SELECT SUM(Amount) FROM [MPS].[dbo].[MPS_BudgetYearTrans] byt WHERE byt.TypeYearPlan = mb.TypeYearPlan AND byt.UnitCode = mb.UnitCode AND byt.Year = mb.Year AND byt.DocumentStatus = 'Return' AND byt.Status = 'Return' AND byt.DocumentType = 'Memo'),0) AS ReturnMemo 
                    FROM [MPS].[dbo].[MPS_M_BudgetYearPlan] mb ) AS BudgetYearTracking
");
            this.Sql(@"CREATE VIEW [dbo].[MPS_VW_MProcurement] AS 
SELECT ROW_NUMBER() OVER (ORDER BY PROPOSALID) AS RowNo ,*
FROM (SELECT prl.REQ_ID,prt.PR_NUM,prl.PROPOSALID,prl.StatusFlag AS PRStatusFlag, pol.PO_ID, pot.PO_NUM,pot.StatusFlag as POStatusFlag
FROM MPS_PurchreqLine prl
INNER JOIN MPS_PurchReqTable prt
ON prt.REQ_ID = prl.REQ_ID
LEFT JOIN MPS_PurchOrderTable pot
ON prl.REQ_ID = pot.REQ_ID
LEFT JOIN MPS_PurchOrderLine pol
ON pol.PR_LINE_NUM = prl.ITEM_NUM
AND pol.PO_ID = pot.PO_ID) as MProcurement
");
        }
        
        public override void Down()
        {
            this.Sql("DROP VIEW [dbo].[MPS_VW_BudgetYearTracking]");
            this.Sql("DROP VIEW [dbo].[MPS_VW_MProcurement]");
        }
    }
}
