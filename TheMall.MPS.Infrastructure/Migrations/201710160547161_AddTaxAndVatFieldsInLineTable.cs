namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTaxAndVatFieldsInLineTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MPS_CashAdvanceLine", "Vat", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.MPS_CashAdvanceLine", "Tax", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.MPS_PettyCashLine", "Vat", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.MPS_PettyCashLine", "Tax", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.MPS_CashClearingLine", "Vat", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.MPS_CashClearingLine", "Tax", c => c.Decimal(precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MPS_CashClearingLine", "Tax");
            DropColumn("dbo.MPS_CashClearingLine", "Vat");
            DropColumn("dbo.MPS_PettyCashLine", "Tax");
            DropColumn("dbo.MPS_PettyCashLine", "Vat");
            DropColumn("dbo.MPS_CashAdvanceLine", "Tax");
            DropColumn("dbo.MPS_CashAdvanceLine", "Vat");
        }
    }
}
