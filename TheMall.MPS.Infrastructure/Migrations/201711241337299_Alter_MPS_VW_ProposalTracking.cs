namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Alter_MPS_VW_ProposalTracking : DbMigration
    {
        public override void Up()
        {
            this.Sql(@"ALTER VIEW [dbo].[MPS_VW_ProposalTracking] AS
SELECT ROW_NUMBER() OVER (ORDER BY ProposalId) AS RowNo,* FROM (SELECT pt.Id AS ProposalId,pt.DocumentNumber AS ProposalDocNo, pl.Id AS ProposalLineId, pl.ExpenseTopicCode, pl.APCode, pl.UnitCode, pl.UnitName, 'Memo' AS DocumentType, mt.Id AS DocumentId, mt.DocumentNumber AS DocumentNo, mt.Status, mt.DocumentStatus,mt.StatusFlag,mt.BudgetDetail AS Amount,null AS RefDocumentType,null AS RefDocumentId,null AS RefDocumentNo,null AS RefStatus,null AS RefDocumentStatus,null AS RefStatusFlag, null AS RefAmount
 FROM dbo.MPS_MemoTable mt
INNER JOIN dbo.MPS_ProposalLine pl
ON mt.ProposalLineID = pl.Id
INNER JOIN dbo.MPS_ProposalTable pt
ON pl.ParentId = pt.Id
WHERE mt.StatusFlag != 0 AND mt.Deleted = 0
UNION
SELECT pt.Id AS ProposalId,pt.DocumentNumber AS ProposalDocNo, pl.Id AS ProposalLineId, pl.ExpenseTopicCode, pl.APCode, pl.UnitCode, pl.UnitName, 'PettyCash' AS DocumentType, pct.Id AS DocumentId, pct.DocumentNumber AS DocumentNo, pct.Status, pct.DocumentStatus,pct.StatusFlag,pct.BudgetDetail AS Amount,null AS RefDocumentType,null AS RefDocumentId,null AS RefDocumentNo,null AS RefStatus,null AS RefDocumentStatus,null AS RefStatusFlag, null AS RefAmount
 FROM dbo.MPS_PettyCashTable pct
INNER JOIN dbo.MPS_ProposalLine pl
ON pct.ProposalLineID = pl.Id
INNER JOIN dbo.MPS_ProposalTable pt
ON pl.ParentId = pt.Id
WHERE pct.StatusFlag != 0 AND pct.Deleted = 0
UNION
SELECT pt.Id AS ProposalId,pt.DocumentNumber AS ProposalDocNo, pl.Id AS ProposalLineId, pl.ExpenseTopicCode, pl.APCode, pl.UnitCode, pl.UnitName, 'CashAdvance' AS DocumentType, cat.Id AS DocumentId, cat.DocumentNumber AS DocumentNo, cat.Status, cat.DocumentStatus,cat.StatusFlag,cat.Budget AS Amount, 
(CASE 
	WHEN cct.Id IS NOT NULL THEN 'CashClearing'
	else NULL
END) AS RefDocumentType, cct.Id AS RefDocumentId, cct.DocumentNumber AS RefDocumentNo, cct.Status AS RefStatus, cct.DocumentStatus AS RefDocumentStatus,cct.StatusFlag AS RefStatusFlag,cct.Actual AS RefAmount
FROM dbo.MPS_CashAdvanceTable cat
LEFT JOIN dbo.MPS_CashClearingTable cct
ON cat.Id = cct.CashAdvanceID
AND cct.StatusFlag != 0 AND cct.Deleted = 0
INNER JOIN dbo.MPS_ProposalLine pl
ON cat.ProposalLineID = pl.Id
INNER JOIN dbo.MPS_ProposalTable pt
ON pl.ParentId = pt.Id
WHERE cat.StatusFlag != 0 AND cat.Deleted = 0
--AND cct.StatusFlag != 0 AND cct.Deleted = 0
UNION
SELECT Prop.ProposalId,Prop.DocumentNumber AS ProposalDocNo,Prop.ProposalLineId,Prop.ExpenseTopicCode,Prop.APCode,Prop.UnitCode,Prop.UnitName,
 'PR'AS DocumentType, prt.REQ_ID AS DocumentId, prt.PR_NUM AS DocumentNo, prt.Status,  prt.Status AS DocumentStatus, prl.StatusFlag, prl.ACTUALPRICE AS Amount,
(CASE 
	WHEN po.PO_ID IS NOT NULL THEN 'PO'
	else NULL
END) AS RefDocumentType,po.PO_ID AS RefDocumentId,po.PO_NUM AS RefDocumentNo,po.Status AS RefStatus,po.Status AS RefDocumentStatus,po.StatusFlag AS RefStatusFlag, po.ACTUALPRICE AS RefAmount
FROM MPS_PurchReqLine prl
INNER JOIN MPS_PurchReqTable prt
ON prl.REQ_ID = prt.REQ_ID
INNER JOIN 
	(SELECT pt.Id AS ProposalId,pl.Id AS ProposalLineId,pt.DocumentNumber,pl.ExpenseTopicCode,pl.APCode,pl.UnitCode,pl.UnitName FROM MPS_ProposalLine pl
	INNER JOIN MPS_ProposalTable pt
	ON pl.ParentId = pt.Id) AS Prop
ON Prop.DocumentNumber = prl.PROPOSALID
AND Prop.APCode = prl.APCODE
AND Prop.UnitCode = prl.COSTCENTER
LEFT JOIN
	(SELECT pot.PO_ID,pot.PO_NUM,pot.REQ_ID,pol.PR_LINE_NUM,pol.ACTUALPRICE,pot.Status,pot.StatusFlag FROM MPS_PurchOrderLine pol
	INNER JOIN MPS_PurchOrderTable pot
	ON pol.PO_ID = pot.PO_ID) AS PO
ON PO.REQ_ID = prl.REQ_ID
AND PO.PR_LINE_NUM = prl.ITEM_NUM) AS ProposalTracking");
        }
        
        public override void Down()
        {
        }
    }
}
