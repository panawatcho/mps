namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addFieldAcyualAmountToEstimate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MPS_IncomeTotalSale", "ActualAmount", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.MPS_IncomeTotalSaleTemplate", "ActualAmount", c => c.Decimal(precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MPS_IncomeTotalSaleTemplate", "ActualAmount");
            DropColumn("dbo.MPS_IncomeTotalSale", "ActualAmount");
        }
    }
}
