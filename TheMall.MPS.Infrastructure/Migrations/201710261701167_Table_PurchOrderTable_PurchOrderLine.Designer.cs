// <auto-generated />
namespace TheMall.MPS.Infrastructure.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class Table_PurchOrderTable_PurchOrderLine : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Table_PurchOrderTable_PurchOrderLine));
        
        string IMigrationMetadata.Id
        {
            get { return "201710261701167_Table_PurchOrderTable_PurchOrderLine"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
