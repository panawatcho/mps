namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changeModelTotalSale : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MPS_IncomeTotalSaleTemplate", "Actual", c => c.Decimal(precision: 18, scale: 2));
            AlterColumn("dbo.MPS_IncomeTotalSaleTemplate", "PercentOfDepartment", c => c.Decimal(precision: 18, scale: 2));
            DropColumn("dbo.MPS_IncomeTotalSaleTemplate", "TR_Amount");
            DropColumn("dbo.MPS_IncomeTotalSaleTemplate", "TR_Percent");
            DropColumn("dbo.MPS_IncomeTotalSaleTemplate", "RE_Amount");
            DropColumn("dbo.MPS_IncomeTotalSaleTemplate", "RE_Percent");
        }
        
        public override void Down()
        {
            AddColumn("dbo.MPS_IncomeTotalSaleTemplate", "RE_Percent", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.MPS_IncomeTotalSaleTemplate", "RE_Amount", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.MPS_IncomeTotalSaleTemplate", "TR_Percent", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.MPS_IncomeTotalSaleTemplate", "TR_Amount", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.MPS_IncomeTotalSaleTemplate", "PercentOfDepartment", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            DropColumn("dbo.MPS_IncomeTotalSaleTemplate", "Actual");
        }
    }
}
