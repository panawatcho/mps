namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class VW_RelatedDocAddfield : DbMigration
    {
        public override void Up()
        {
            this.Sql(@"CREATE VIEW [dbo].[MPS_VW_DocumentRelated] AS 
SELECT ROW_NUMBER() OVER(ORDER BY [order],SubmittedDate) AS RowNo 
  ,  STUFF(( SELECT ';' + Username 
                     FROM (SELECT Username
                                    FROM MPS_M_Accounting acc
                                    WHERE acc.InActive = 0  and
                                    acc.BranchCode = DocumentAccounting.AccountingBranch
									
                                    ) AS Temp 
                    FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 1, '') 
				    AS UsernameAccounting
, * From 
(SELECT 
pt.Id
,'Proposal' As DocumentType
, 1 As [Order]
, Null As RefDocumentNumber
, Null As RefDocumentID
                  ,pt.UnitCode
                  ,pt.UnitName
                  ,pt.Title
                  ,pt.DocumentNumber
                  ,pt.RequesterUserName
                  ,pt.RequesterName
                  ,pt.Status
                  ,pt.DocumentStatus
                  ,pt.StatusFlag
                  ,pt.AccountingUsername
                  ,pt.AccountingEmail
                  ,pt.InformUsername
                  ,pt.InformEmail
                  ,pt.CCUsername
                  ,pt.CCEmail
                  ,pt.AccountingBranch,
				  pt.SubmittedDate,
				  pt.Revision
					,  STUFF(( SELECT ';' + ApproverUserName 
                     FROM (SELECT ApproverUserName
                                    FROM MPS_ProposalApproval app
                                    where 
                                    app.ParentId = pt.Id
									
                                    ) AS Temp 
                    FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 1, '') 
				    AS UsernameApprover
FROM dbo.MPS_ProposalTable pt
WHERE pt.StatusFlag !=0 AND pt.StatusFlag !=1 AND pt.Deleted = 0
UNION
SELECT 
mt.Id
,'Memo' As DocumentType
, 2 As [Order]
, mt.ProposalRefDocumentNumber As RefDocumentNumber
, mt.ProposalRefID As RefDocumentID
                  ,mt.UnitCode
                  ,mt.UnitName
                  ,mt.Title
                  ,mt.DocumentNumber
                  ,mt.RequesterUserName
                  ,mt.RequesterName
                  ,mt.Status
                  ,mt.DocumentStatus
                  ,mt.StatusFlag
                  ,mt.AccountingUsername
                  ,mt.AccountingEmail
                  ,mt.InformUsername
                  ,mt.InformEmail
                  ,mt.CCUsername
                  ,mt.CCEmail
                  ,mt.AccountingBranch,
				  mt.SubmittedDate
				   ,mt.Revision
					,  STUFF(( SELECT ';' + ApproverUserName 
                     FROM (SELECT ApproverUserName
                                    FROM MPS_MemoApproval app
                                    where 
                                    app.ParentId = mt.Id
									
                                    ) AS Temp 
                    FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 1, '') 
				    AS UsernameApprover
FROM dbo.MPS_MemoTable mt
WHERE mt.StatusFlag !=0 AND mt.StatusFlag !=1 AND mt.Deleted = 0
UNION
SELECT 
pc.Id
, (CASE 
	WHEN pc.GoodsIssue = 0 THEN 'PettyCash'
	else 'GoodsIssue'
END) As DocumentType
, (CASE 
	WHEN pc.GoodsIssue = 0 THEN 3
	else 4
END)  As [Order]
, pc.ProposalRefDocumentNumber As RefDocumentNumber
, pc.ProposalRefID As RefDocumentID
                  ,pc.UnitCode
                  ,pc.UnitName
                  ,pc.Title
                  ,pc.DocumentNumber
                  ,pc.RequesterUserName
                  ,pc.RequesterName
                  ,pc.Status
                  ,pc.DocumentStatus
                  ,pc.StatusFlag
                  ,pc.AccountingUsername
                  ,pc.AccountingEmail
                  ,pc.InformUsername
                  ,pc.InformEmail
                  ,pc.CCUsername
                  ,pc.CCEmail
                  ,pc.AccountingBranch,
				  pc.SubmittedDate,
				   pc.Revision,
                 STUFF(( SELECT ';' + ApproverUserName 
                     FROM (SELECT ApproverUserName
                                    FROM MPS_PettyCashApproval app
                                    where 
                                    app.ParentId = pc.Id
									
                                    ) AS Temp 
                    FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 1, '') 
				    AS UsernameApprover
				  
FROM dbo.MPS_PettyCashTable pc
WHERE pc.StatusFlag !=0 AND pc.StatusFlag !=1 AND pc.Deleted = 0
UNION
SELECT 
ca.Id
,'CashAdvance' As DocumentType
, 5 As [Order]
, ca.ProposalRefDocumentNumber As RefDocumentNumber
, ca.ProposalRefID As RefDocumentID
                  ,ca.UnitCode
                  ,ca.UnitName
                  ,ca.Title
                  ,ca.DocumentNumber
                  ,ca.RequesterUserName
                  ,ca.RequesterName
                  ,ca.Status
                  ,ca.DocumentStatus
                  ,ca.StatusFlag
                  ,ca.AccountingUsername
                  ,ca.AccountingEmail
                  ,ca.InformUsername
                  ,ca.InformEmail
                  ,ca.CCUsername
                  ,ca.CCEmail
                  ,ca.AccountingBranch,
				  ca.SubmittedDate,
				   ca.Revision
                 , STUFF(( SELECT ';' + ApproverUserName 
                     FROM (SELECT ApproverUserName
                                    FROM MPS_CashAdvanceApproval app
                                    where 
                                    app.ParentId = ca.Id
									
                                    ) AS Temp 
                    FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 1, '') 
				    AS UsernameApprover
FROM dbo.MPS_CashAdvanceTable ca
WHERE ca.StatusFlag !=0 AND ca.StatusFlag !=1 AND ca.Deleted = 0
UNION
SELECT 
cl.Id
,'CashClearing' As DocumentType
, 6 As [Order]
, cl.RefAdvanceNumber As RefDocumentNumber
, cl.CashAdvanceID As RefDocumentID
                  ,cl.UnitCode
                  ,cl.UnitName
                  ,cl.Title
                  ,cl.DocumentNumber
                  ,cl.RequesterUserName
                  ,cl.RequesterName
                  ,cl.Status
                  ,cl.DocumentStatus
                  ,cl.StatusFlag
                  ,cl.AccountingUsername
                  ,cl.AccountingEmail
                  ,cl.InformUsername
                  ,cl.InformEmail
                  ,cl.CCUsername
                  ,cl.CCEmail
                  ,cl.AccountingBranch
				  ,cl.SubmittedDate
				, cl.Revision
					,  STUFF(( SELECT ';' + ApproverUserName 
                     FROM (SELECT ApproverUserName
                                    FROM MPS_CashClearingApproval app
                                    where 
                                    app.ParentId = cl.Id
									
                                    ) AS Temp 
                    FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 1, '') 
				    AS UsernameApprover
FROM dbo.MPS_CashClearingTable cl
WHERE cl.StatusFlag !=0 AND cl.StatusFlag !=1 AND cl.Deleted = 0) As DocumentAccounting ");
        }
        
        public override void Down()
        {
             this.Sql("DROP VIEW [dbo].[MPS_VW_DocumentRelated]");
        }
    }
}
