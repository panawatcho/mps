namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFieldIncomeDep : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MPS_IncomeDeposit", "ProcInstId", c => c.Int());
            AddColumn("dbo.MPS_IncomeDeposit", "Status", c => c.String(maxLength: 100));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MPS_IncomeDeposit", "Status");
            DropColumn("dbo.MPS_IncomeDeposit", "ProcInstId");
        }
    }
}
