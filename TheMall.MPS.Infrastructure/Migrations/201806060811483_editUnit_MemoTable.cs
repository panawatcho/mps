namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editUnit_MemoTable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.MPS_MemoLine", "Unit", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.MPS_MemoLine", "Unit", c => c.Int());
        }
    }
}
