namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_ThemeTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MPS_M_Themes",
                c => new
                    {
                        ThemesCode = c.String(nullable: false, maxLength: 128),
                        ThemesName = c.String(),
                        InActive = c.Boolean(nullable: false),
                        Order = c.Decimal(precision: 18, scale: 2),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.ThemesCode);
            
            AddColumn("dbo.MPS_ProposalTable", "ThemesCode", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.MPS_ProposalTable", "ThemesCode");
            DropTable("dbo.MPS_M_Themes");
        }
    }
}
