namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addField_etc : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MPS_ProposalLine", "CheckBudget", c => c.Boolean(nullable: false));
            AddColumn("dbo.MPS_M_UnitCode", "Order", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.MPS_M_SharedBranch", "Order", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.MPS_ProposalTable", "TotalBudget", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.MPS_IncomeDeposit", "DepositApprover", c => c.String());
            AddColumn("dbo.MPS_M_UnitCodeForEmployee", "Order", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.MPS_FinalApprover", "Order", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.MPS_M_APCode", "Order", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.MPS_M_ExpenseTopic", "Order", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.MPS_M_BudgetYearPlan", "TypeYearPlan", c => c.String(maxLength: 50));
            AddColumn("dbo.MPS_M_BudgetYearPlan", "Order", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.MPS_M_Place", "Order", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.MPS_M_TypeMemo", "UnitCode", c => c.String(maxLength: 20));
            AddColumn("dbo.MPS_M_TypeMemo", "UnitName", c => c.String(maxLength: 1000));
            AddColumn("dbo.MPS_M_TypeMemo", "Order", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.MPS_M_TypeProposal", "TypeYearPlan", c => c.String(maxLength: 50));
            AddColumn("dbo.MPS_M_TypeProposal", "Order", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.MPS_TotalSaleLastYear", "AreaTR", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.MPS_TotalSaleLastYear", "AreaRE", c => c.Decimal(precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MPS_TotalSaleLastYear", "AreaRE");
            DropColumn("dbo.MPS_TotalSaleLastYear", "AreaTR");
            DropColumn("dbo.MPS_M_TypeProposal", "Order");
            DropColumn("dbo.MPS_M_TypeProposal", "TypeYearPlan");
            DropColumn("dbo.MPS_M_TypeMemo", "Order");
            DropColumn("dbo.MPS_M_TypeMemo", "UnitName");
            DropColumn("dbo.MPS_M_TypeMemo", "UnitCode");
            DropColumn("dbo.MPS_M_Place", "Order");
            DropColumn("dbo.MPS_M_BudgetYearPlan", "Order");
            DropColumn("dbo.MPS_M_BudgetYearPlan", "TypeYearPlan");
            DropColumn("dbo.MPS_M_ExpenseTopic", "Order");
            DropColumn("dbo.MPS_M_APCode", "Order");
            DropColumn("dbo.MPS_FinalApprover", "Order");
            DropColumn("dbo.MPS_M_UnitCodeForEmployee", "Order");
            DropColumn("dbo.MPS_IncomeDeposit", "DepositApprover");
            DropColumn("dbo.MPS_ProposalTable", "TotalBudget");
            DropColumn("dbo.MPS_M_SharedBranch", "Order");
            DropColumn("dbo.MPS_M_UnitCode", "Order");
            DropColumn("dbo.MPS_ProposalLine", "CheckBudget");
        }
    }
}
