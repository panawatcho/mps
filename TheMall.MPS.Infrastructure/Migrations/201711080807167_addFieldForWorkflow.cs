namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addFieldForWorkflow : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MPS_ProposalTable", "TotalAdvertising", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.MPS_ProposalTable", "TotalPromotion", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.MPS_ProposalTable", "PecentAdvertising", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.MPS_ProposalTable", "PecentPromotion", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.MPS_ProposalTable", "PecentMarketingExpense", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.MPS_ProposalTable", "DueDatePropose", c => c.DateTime());
            AddColumn("dbo.MPS_ProposalTable", "DueDateAccept", c => c.DateTime());
            AddColumn("dbo.MPS_ProposalTable", "DueDateApprove", c => c.DateTime());
            AddColumn("dbo.MPS_ProposalTable", "DueDateFinalApprove", c => c.DateTime());
            AddColumn("dbo.MPS_BudgetDepositTrans", "IncomeId", c => c.Int());
            AddColumn("dbo.MPS_BudgetDepositTrans", "DepositNumber", c => c.String(maxLength: 20));
            DropColumn("dbo.MPS_BudgetDepositTrans", "DocumentType");
            DropColumn("dbo.MPS_BudgetDepositTrans", "DocumentId");
            DropColumn("dbo.MPS_BudgetDepositTrans", "DocumentNo");
            DropColumn("dbo.MPS_BudgetDepositTrans", "RefDocumentType");
        }
        
        public override void Down()
        {
            AddColumn("dbo.MPS_BudgetDepositTrans", "RefDocumentType", c => c.String(maxLength: 100));
            AddColumn("dbo.MPS_BudgetDepositTrans", "DocumentNo", c => c.String(maxLength: 20));
            AddColumn("dbo.MPS_BudgetDepositTrans", "DocumentId", c => c.Int());
            AddColumn("dbo.MPS_BudgetDepositTrans", "DocumentType", c => c.String(maxLength: 100));
            DropColumn("dbo.MPS_BudgetDepositTrans", "DepositNumber");
            DropColumn("dbo.MPS_BudgetDepositTrans", "IncomeId");
            DropColumn("dbo.MPS_ProposalTable", "DueDateFinalApprove");
            DropColumn("dbo.MPS_ProposalTable", "DueDateApprove");
            DropColumn("dbo.MPS_ProposalTable", "DueDateAccept");
            DropColumn("dbo.MPS_ProposalTable", "DueDatePropose");
            DropColumn("dbo.MPS_ProposalTable", "PecentMarketingExpense");
            DropColumn("dbo.MPS_ProposalTable", "PecentPromotion");
            DropColumn("dbo.MPS_ProposalTable", "PecentAdvertising");
            DropColumn("dbo.MPS_ProposalTable", "TotalPromotion");
            DropColumn("dbo.MPS_ProposalTable", "TotalAdvertising");
        }
    }
}
