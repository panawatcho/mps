namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addMemoIncomeTable2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MPS_MemoIncomeApproval",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ApproverSequence = c.Int(nullable: false),
                        ApproverLevel = c.String(maxLength: 50),
                        ApproverUserName = c.String(maxLength: 50),
                        ApproverFullName = c.String(maxLength: 100),
                        ApproverEmail = c.String(),
                        Position = c.String(maxLength: 100),
                        Department = c.String(maxLength: 100),
                        DueDate = c.DateTime(),
                        LockEditable = c.Boolean(nullable: false),
                        ApproveStatus = c.Boolean(nullable: false),
                        StatusFlag = c.Int(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                        DeletedDate = c.DateTime(),
                        DeletedBy = c.String(),
                        DeletedByName = c.String(),
                        LineNo = c.Single(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        ParentId = c.Int(nullable: false),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MPS_MemoIncomeTable", t => t.ParentId, cascadeDelete: true)
                .Index(t => t.ParentId);
            
            CreateTable(
                "dbo.MPS_MemoIncomeTable",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TotalAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Category = c.String(),
                        ProposalRefID = c.Int(nullable: false),
                        ProposalRefDocumentNumber = c.String(maxLength: 20),
                        ProposalTypeCode = c.String(maxLength: 10),
                        Description = c.String(),
                        InvoiceNo = c.String(),
                        AccountingStatus = c.String(),
                        OtherIncomeRefId = c.Int(),
                        ActualCharge = c.Decimal(precision: 18, scale: 2),
                        UnitCode = c.String(maxLength: 20),
                        UnitName = c.String(maxLength: 1000),
                        AccountingBranch = c.String(maxLength: 100),
                        AccountingUsername = c.String(),
                        AccountingEmail = c.String(),
                        InformUsername = c.String(),
                        InformEmail = c.String(),
                        CCUsername = c.String(),
                        CCEmail = c.String(),
                        DueDatePropose = c.DateTime(),
                        DueDateAccept = c.DateTime(),
                        DueDateApprove = c.DateTime(),
                        DueDateFinalApprove = c.DateTime(),
                        RePrintNo = c.Int(),
                        Title = c.String(nullable: false, maxLength: 200),
                        DocumentNumber = c.String(maxLength: 20),
                        Revision = c.Int(),
                        RequesterUserName = c.String(nullable: false, maxLength: 100),
                        RequesterName = c.String(nullable: false, maxLength: 200),
                        SubmittedDate = c.DateTime(),
                        Status = c.String(maxLength: 100),
                        DocumentStatus = c.String(maxLength: 100),
                        StatusFlag = c.Int(nullable: false),
                        ProcInstId = c.Int(),
                        ExpectedDuration = c.Int(),
                        DocumentDate = c.DateTime(),
                        CompletedDate = c.DateTime(),
                        CancelledDate = c.DateTime(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        Deleted = c.Boolean(nullable: false),
                        DeletedDate = c.DateTime(),
                        DeletedBy = c.String(maxLength: 100),
                        DeletedByName = c.String(maxLength: 200),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                        ProposalTable_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MPS_IncomeOther", t => t.OtherIncomeRefId)
                .ForeignKey("dbo.MPS_ProposalTable", t => t.ProposalTable_Id)
                .ForeignKey("dbo.MPS_M_TypeProposal", t => t.ProposalTypeCode)
                .ForeignKey("dbo.MPS_M_UnitCode", t => t.UnitCode)
                .Index(t => t.ProposalTypeCode)
                .Index(t => t.OtherIncomeRefId)
                .Index(t => t.UnitCode)
                .Index(t => new { t.DocumentNumber, t.Revision }, name: "DocumentNumber_Revision")
                .Index(t => t.ProposalTable_Id);
            
            CreateTable(
                "dbo.MPS_MemoIncomeAttachment",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ParentId = c.Int(nullable: false),
                        DocumentNumber = c.String(maxLength: 20),
                        ContentType = c.String(maxLength: 100),
                        ProcInstId = c.Int(),
                        SerialNumber = c.String(maxLength: 20),
                        Activity = c.String(maxLength: 100),
                        FileDescription = c.String(maxLength: 300),
                        FileName = c.String(maxLength: 300),
                        FileSize = c.Long(nullable: false),
                        FileSizeText = c.String(maxLength: 300),
                        DocumentLibrary = c.String(maxLength: 300),
                        Location = c.String(maxLength: 600),
                        DownloadUrl = c.String(maxLength: 600),
                        DocumentTypeId = c.Int(),
                        DocumentTypeName = c.String(maxLength: 100),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MPS_MemoIncomeTable", t => t.ParentId, cascadeDelete: true)
                .Index(t => t.ParentId);
            
            CreateTable(
                "dbo.MPS_MemoIncomeComment",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ParentId = c.Int(nullable: false),
                        ProcInstId = c.Int(),
                        SerialNumber = c.String(maxLength: 20),
                        ProcessName = c.String(maxLength: 100),
                        Activity = c.String(maxLength: 100),
                        Action = c.String(maxLength: 100),
                        Comment = c.String(),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MPS_MemoIncomeTable", t => t.ParentId, cascadeDelete: true)
                .Index(t => t.ParentId);
            
            CreateTable(
                "dbo.MPS_MemoIncomeLine",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                        Unit = c.Int(),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Vat = c.Decimal(precision: 18, scale: 2),
                        Tax = c.Decimal(precision: 18, scale: 2),
                        NetAmount = c.Decimal(precision: 18, scale: 2),
                        Remark = c.String(),
                        StatusFlag = c.Int(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                        DeletedDate = c.DateTime(),
                        DeletedBy = c.String(),
                        DeletedByName = c.String(),
                        LineNo = c.Single(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        ParentId = c.Int(nullable: false),
                        CreatedBy = c.String(maxLength: 100),
                        CreatedByName = c.String(maxLength: 200),
                        CreatedDate = c.DateTime(),
                        ModifiedBy = c.String(maxLength: 100),
                        ModifiedByName = c.String(maxLength: 200),
                        ModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MPS_MemoIncomeTable", t => t.ParentId, cascadeDelete: true)
                .Index(t => t.ParentId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MPS_MemoIncomeTable", "UnitCode", "dbo.MPS_M_UnitCode");
            DropForeignKey("dbo.MPS_MemoIncomeTable", "ProposalTypeCode", "dbo.MPS_M_TypeProposal");
            DropForeignKey("dbo.MPS_MemoIncomeTable", "ProposalTable_Id", "dbo.MPS_ProposalTable");
            DropForeignKey("dbo.MPS_MemoIncomeLine", "ParentId", "dbo.MPS_MemoIncomeTable");
            DropForeignKey("dbo.MPS_MemoIncomeApproval", "ParentId", "dbo.MPS_MemoIncomeTable");
            DropForeignKey("dbo.MPS_MemoIncomeTable", "OtherIncomeRefId", "dbo.MPS_IncomeOther");
            DropForeignKey("dbo.MPS_MemoIncomeComment", "ParentId", "dbo.MPS_MemoIncomeTable");
            DropForeignKey("dbo.MPS_MemoIncomeAttachment", "ParentId", "dbo.MPS_MemoIncomeTable");
            DropIndex("dbo.MPS_MemoIncomeLine", new[] { "ParentId" });
            DropIndex("dbo.MPS_MemoIncomeComment", new[] { "ParentId" });
            DropIndex("dbo.MPS_MemoIncomeAttachment", new[] { "ParentId" });
            DropIndex("dbo.MPS_MemoIncomeTable", new[] { "ProposalTable_Id" });
            DropIndex("dbo.MPS_MemoIncomeTable", "DocumentNumber_Revision");
            DropIndex("dbo.MPS_MemoIncomeTable", new[] { "UnitCode" });
            DropIndex("dbo.MPS_MemoIncomeTable", new[] { "OtherIncomeRefId" });
            DropIndex("dbo.MPS_MemoIncomeTable", new[] { "ProposalTypeCode" });
            DropIndex("dbo.MPS_MemoIncomeApproval", new[] { "ParentId" });
            DropTable("dbo.MPS_MemoIncomeLine");
            DropTable("dbo.MPS_MemoIncomeComment");
            DropTable("dbo.MPS_MemoIncomeAttachment");
            DropTable("dbo.MPS_MemoIncomeTable");
            DropTable("dbo.MPS_MemoIncomeApproval");
        }
    }
}
