namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Alter_MPS_VW_ProposalUnitCode : DbMigration
    {
        public override void Up()
        {
            this.Sql(@"ALTER VIEW [dbo].[MPS_VW_ProposalUnitCode] AS
                    SELECT ROW_NUMBER() OVER(ORDER BY UnitCodeAP) AS RowNo,proposalUnitCode.UnitCodeAP 
                   ,pt.Id
                  ,pt.ProposalTypeCode
                  ,pt.ProposalTypeName
                  ,pt.Place
                  ,pt.Source
                  ,pt.Other
                  ,pt.DepositId
                  ,pt.DepositNumber
                  ,pt.StartDate
                  ,pt.EndDate
                  ,pt.Budget
                  ,pt.CustomersTargetGroup
                  ,pt.CheckBudget
                  ,pt.TransferToNextYear
                  ,pt.TotalMarketingExpense
                  ,pt.TotalIncomeDeposit
                  ,pt.TotalIncomeOther
                  ,pt.TotalSaleTarget
                  ,pt.DepartmentException
                  ,pt.UnitCode
                  ,pt.UnitName
                  ,pt.Title
                  ,pt.DocumentNumber
                  ,pt.Revision
                  ,pt.RequesterUserName
                  ,pt.RequesterName
                  ,pt.SubmittedDate
                  ,pt.Status
                  ,pt.DocumentStatus
                  ,pt.StatusFlag
                  ,pt.ProcInstId
                  ,pt.ExpectedDuration
                  ,pt.DocumentDate
                  ,pt.CompletedDate
                  ,pt.CancelledDate
                  ,pt.RowVersion
                  ,pt.Deleted
                  ,pt.DeletedDate
                  ,pt.DeletedBy
                  ,pt.DeletedByName
                  ,pt.CreatedBy
                  ,pt.CreatedByName
                  ,pt.CreatedDate
                  ,pt.ModifiedBy
                  ,pt.ModifiedByName
                  ,pt.ModifiedDate
                  ,pt.ProjectStartDate
                  ,pt.ProjectEndDate
                  ,pt.TR
                  ,pt.RE
                  ,pt.AccountingUsername
                  ,pt.CloseFlag
                  ,pt.AccountingEmail
                  ,pt.TotalBudget
                  ,pt.InformUsername
                  ,pt.InformEmail
                  ,pt.CCUsername
                  ,pt.CCEmail
                  ,pt.TotalAdvertising
                  ,pt.TotalPromotion
                  ,pt.PecentAdvertising
                  ,pt.PecentPromotion
                  ,pt.PecentMarketingExpense
                  ,pt.DueDatePropose
                  ,pt.DueDateAccept
                  ,pt.DueDateApprove
                  ,pt.DueDateFinalApprove
                  ,pt.AccountingBranch
                    FROM MPS_ProposalTable pt
                    INNER JOIN 
                    (SELECT DISTINCT pt.Id,pl.UnitCode AS UnitCodeAP
                    FROM MPS_ProposalLine pl
                    INNER JOIN MPS_ProposalTable pt
                    ON pl.ParentId = pt.Id
                    WHERE pl.UnitCode IS NOT NULL AND 
                    pl.Deleted = 0 AND
                    pt.StatusFlag = 9) AS proposalUnitCode
                    ON pt.Id = proposalUnitCode.Id");
        }
        
        public override void Down()
        {
        }
    }
}
