namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addFiled_Close_ProposalTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MPS_ProposalTable", "CloseDate", c => c.DateTime());
            AddColumn("dbo.MPS_ProposalTable", "ClosedBy", c => c.String(maxLength: 100));
            AddColumn("dbo.MPS_ProposalTable", "CloseByName", c => c.String(maxLength: 200));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MPS_ProposalTable", "CloseByName");
            DropColumn("dbo.MPS_ProposalTable", "ClosedBy");
            DropColumn("dbo.MPS_ProposalTable", "CloseDate");
        }
    }
}
