namespace TheMall.MPS.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editUnit_AllTable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.MPS_CashAdvanceLine", "Unit", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.MPS_PettyCashLine", "Unit", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.MPS_CashClearingLine", "Unit", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.MPS_MemoIncomeLine", "Unit", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.MPS_ReceiptLine", "Unit", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.MPS_ReceiptLine", "Unit", c => c.Int());
            AlterColumn("dbo.MPS_MemoIncomeLine", "Unit", c => c.Int());
            AlterColumn("dbo.MPS_CashClearingLine", "Unit", c => c.Int());
            AlterColumn("dbo.MPS_PettyCashLine", "Unit", c => c.Int());
            AlterColumn("dbo.MPS_CashAdvanceLine", "Unit", c => c.Int());
        }
    }
}
