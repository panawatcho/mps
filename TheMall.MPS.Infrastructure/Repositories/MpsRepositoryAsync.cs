﻿using CrossingSoft.Framework.Infrastructure;
using TheMall.MPS.Infrastructure.EntityFramework;
using TheMall.MPS.Infrastructure.Sessions;

namespace TheMall.MPS.Infrastructure.Repositories
{
    public interface IMpsRepositoryAsync<TEntity> :
        CrossingSoft.Framework.Infrastructure.Repositories.IRepositoryAsync<IMPSDbContext, IMPSSession, TEntity> 
        where TEntity : IObjectState
    {
        
    }
    public class MpsRepositoryAsync<TEntity> : CrossingSoft.Framework.Infrastructure.EF6.Repository<IMPSDbContext, IMPSSession, TEntity>, IMpsRepositoryAsync<TEntity> where TEntity : class, IObjectState
    {
        public MpsRepositoryAsync(IMPSDbContext context, IMPSSession session)
            : base(context, session)
        {
        }
    }
}
