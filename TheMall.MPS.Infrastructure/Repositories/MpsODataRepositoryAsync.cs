﻿using CrossingSoft.Framework.Infrastructure;
using TheMall.MPS.Infrastructure.EntityFramework;
using TheMall.MPS.Infrastructure.Sessions;

namespace TheMall.MPS.Infrastructure.Repositories
{
    public interface IMpsODataRepositoryAsync<TEntity> :
        CrossingSoft.Framework.Infrastructure.OData.Repositories.IODataRepositoryAsync<IMPSDbContext, IMPSSession, TEntity> 
        where TEntity : IObjectState
    {
        
    }
    public class MpsODataRepositoryAsync<TEntity> : CrossingSoft.Framework.Infrastructure.EF6.Repository<IMPSDbContext, IMPSSession, TEntity>, IMpsODataRepositoryAsync<TEntity> where TEntity : class, IObjectState
    {
        public MpsODataRepositoryAsync(IMPSDbContext context, IMPSSession session)
            : base(context, session)
        {
        }
    }
}
