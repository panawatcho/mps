﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using CrossingSoft.Framework.Infrastructure;
using CrossingSoft.Framework.Infrastructure.DataContext;
using CrossingSoft.Framework.Infrastructure.EF6;
using CrossingSoft.Framework.Infrastructure.OData.Repositories;
using CrossingSoft.Framework.Infrastructure.Session;
using CrossingSoft.Framework.Models.Interfaces;
using TheMall.MPS.Infrastructure.EntityFramework;
using TheMall.MPS.Infrastructure.Sessions;


namespace TheMall.MPS.Infrastructure.Repositories
{
    public class TrackableRepository<TEntity> :MpsODataRepositoryAsync<TEntity>,IMpsRepositoryAsync<TEntity>
        where TEntity : class,IObjectState, ITrackableModel
    {
        public TrackableRepository(IMPSDbContext context, IMPSSession sessionAsync)
            : base(context, sessionAsync)
        {
        }

        protected override void CreatePre(TEntity entity)
        {
            if (string.IsNullOrEmpty(entity.CreatedBy) && System.Threading.Thread.CurrentPrincipal != null)
            {
                entity.CreatedBy = ((ClaimsPrincipal)System.Threading.Thread.CurrentPrincipal).Identity.Name;
                //entity.CreatedByName = DTGO.Infrastructure.UserHelper.CurrentUserDisplayName();
            }
            if (entity.CreatedDate == null)
            {
                entity.CreatedDate = DateTime.UtcNow;
            }
            base.CreatePre(entity);
        }

        protected override void UpdatePre(TEntity entity)
        {
            if (string.IsNullOrEmpty(entity.CreatedBy) && System.Threading.Thread.CurrentPrincipal != null)
            {
                entity.CreatedBy = ((ClaimsPrincipal)System.Threading.Thread.CurrentPrincipal).Identity.Name;
                //entity.CreatedByName = DTGO.Infrastructure.UserHelper.CurrentUserDisplayName();
            }
            if (System.Threading.Thread.CurrentPrincipal != null)
            {
                entity.ModifiedBy = ((ClaimsPrincipal)System.Threading.Thread.CurrentPrincipal).Identity.Name;
                //entity.ModifiedByName = DTGO.Infrastructure.UserHelper.CurrentUserDisplayName();
            }

            entity.ModifiedDate = DateTime.UtcNow;
            base.UpdatePre(entity);
        }
    }

    public class TrackableRepository<TContext, TSession, TEntity> : Repository<TEntity>, IODataRepositoryAsync<TContext, TSession, TEntity>
        where TContext : IDataContextAsync
        where TSession : ISessionAsync
        where TEntity : class, IObjectState, ITrackableModel
    {
        public TrackableRepository(TContext context, TSession sessionAsync)
            : base(context, sessionAsync)
        {
        }


        protected override void CreatePre(TEntity entity)
        {
            if (string.IsNullOrEmpty(entity.CreatedBy) && System.Threading.Thread.CurrentPrincipal != null)
            {
                entity.CreatedBy = ((ClaimsPrincipal)System.Threading.Thread.CurrentPrincipal).Identity.Name;
                //entity.CreatedByName = DTGO.Infrastructure.UserHelper.CurrentUserDisplayName();
            }
            if (entity.CreatedDate == null)
            {
                entity.CreatedDate = DateTime.UtcNow;
            }
            base.CreatePre(entity);
        }

        protected override void UpdatePre(TEntity entity)
        {
            if (string.IsNullOrEmpty(entity.CreatedBy) && System.Threading.Thread.CurrentPrincipal != null)
            {
                entity.CreatedBy = ((ClaimsPrincipal)System.Threading.Thread.CurrentPrincipal).Identity.Name;
                //entity.CreatedByName = DTGO.Infrastructure.UserHelper.CurrentUserDisplayName();
            }
            if (System.Threading.Thread.CurrentPrincipal != null)
            {
                entity.ModifiedBy = ((ClaimsPrincipal)System.Threading.Thread.CurrentPrincipal).Identity.Name;
                //entity.ModifiedByName = DTGO.Infrastructure.UserHelper.CurrentUserDisplayName();
            }

            entity.ModifiedDate = DateTime.UtcNow;
            base.UpdatePre(entity);
        }
    }
}
