﻿app = angular.module("app.controllers");
app.factory('IdCashAdvanceService', function () {

    var Id = 0;

    function set(data) {
        Id = data;
    }

    function get() {
        return Id;
    }

    return {
        set: set,
        get: get
    }

});