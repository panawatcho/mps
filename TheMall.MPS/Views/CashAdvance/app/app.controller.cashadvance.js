﻿angular.module('app.controllers').controller('CashAdvanceController',
[
    '$scope', '$routeParams', 'inquiryGrid', '$location', '$http', 'IdCashAdvanceService', 'cashAdvanceDataSource',
    'IdProposalService', 'appUrl', 'lineGrid', '$window', 'messageBox', 'sweetAlert',
    function($scope, $routeParams, inquiryGrid, $location, $http, idCashAdvanceService, cashAdvanceDataSource,
        idProposalService, appUrl, lineGrid, $window, messageBox, sweetAlert) {

        $scope.printReport = function (advanceId) {
            if (advanceId) {
                var params = {
                    Id: advanceId,
                    inline: true,
                    //show: true,
                    fullscreen: true
                }
                var url = angular.crs.url.rootweb("report/CashAdvance/?params=" + JSON.stringify(params));
                return url;
            }
        }

        $scope.printReportMemoCashAdvance = function (advanceId) {
            if (advanceId) {
                var params = {
                    Id: advanceId,
                    inline: true,
                    //show: true,
                    fullscreen: true
                }
                var url = angular.crs.url.rootweb("report/MemoCashAdvance/?params=" + JSON.stringify(params));
                return url;
            }
        }

        $scope.printReportClear = function (clearingId) {
            if (clearingId) {
                var params = {
                    Id: clearingId,
                    inline: true,
                    //show: true,
                    fullscreen: true
                }
                var url = angular.crs.url.rootweb("report/CashClearing/?params=" + JSON.stringify(params));
                return url;
            }
        }
        //$scope.delete = function (id, statusFlag) {
        //    if (statusFlag === 9) {
        //        if ($window.confirm("Cancel this record ?")) {
        //            var result = cashAdvanceDataSource.deleteBy({ id: id, username: CURRENT_USERNAME },
        //                 function (data) {
        //                     messageBox.success("Cancel data succesful");
        //                     $location.path('/cashadvance');
        //                 });
        //        } else {
        //            return false;
        //        }
        //    } else {
        //        if ($window.confirm("Delete this record ?")) {
        //            var result = cashAdvanceDataSource.deleteBy({ id: id, username: CURRENT_USERNAME },
        //                 function (data) {
        //                     messageBox.success("Delete data succesful");
        //                     $location.path('/cashadvance');
        //                 });
        //        } else {
        //            return false;
        //        }
        //    }
        //}

        $scope.delete = function (id, statusFlag) {
            if (statusFlag === 9) {
                sweetAlert.swal({
                    title: 'Are you sure?',
                    text: "Cancel this record ?",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, Cancel it!'
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        var result = cashAdvanceDataSource.deleteBy({ id: id, username: CURRENT_USERNAME },
                        function (data) {
                            sweetAlert.success("Cancel data succesful");
                            $location.path('/cashadvance');
                        }, sweetAlert.error("You can't cancel data!"));
                    }
                });
            } else {
                sweetAlert.swal({
                    title: 'Are you sure?',
                    text: "Delete this record ?",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        var result = cashAdvanceDataSource.deleteBy({ id: id, username: CURRENT_USERNAME },
                        function (data) {
                            sweetAlert.success("Delete data succesful");
                            $location.path('/cashadvance');
                        }, sweetAlert.error("You can't delete data!"));
                    }
                });
            }
        }

        var commandTemplateAllstatus = function(data) {
            if (data.CreatedBy && data.CreatedBy.toUpperCase() === CURRENT_USERNAME.toUpperCase()) {
                if (data.StatusFlag === null || data.StatusFlag === 0) {
                    console.log("1");
                    return '<div class="btn-group" role="group">' +
                        '<a class="btn btn-default" style=" margin-right: 5px;" href="' + $routeParams.page + '/detail/' + data.Id + '" title="view"><i class="fa fa-file-text"></i></a>' +
                        '<a class="btn btn-default" style=" margin-right: 5px;" href="' + $routeParams.page + '/create/' + data.Id + '" title="edit"><i class="fa fa-edit"></i></a>' +
                        '<span class="btn btn-default" disabled="disabled" style="margin-right: 5px;" title="Cash clearing"><i class="fa fa-files-o"></i></span>' +
                        '<a class="btn btn-info" disabled="disabled" style=" margin-right: 5px; margin-bottom: 10px"><span class="fa fa-print"></span></a>' +
                       '<a class="btn btn-default"  style=" background-color:#dd4b39;" ng-click="delete(' + data.Id + ',' + data.StatusFlag + ')" title="Delete"><i class="fa fa-trash-o" style="color:white;"></i></a>' +
                        '</div>';
                } else {
                    console.log("2");
                    var htmlButtonV1 = '<div class="btn-group" role="group">' +
                        '<a class="btn btn-default" style="margin-right: 5px;" href="' + $routeParams.page + '/detail/' + data.Id + '" title="view"><i class="fa fa-file-text"></i></a>' +
                        '<span class="btn btn-default" disabled="disabled" style="margin-right: 5px;" title="edit"><i class="fa fa-edit"></i></span>';
                    if (data.StatusFlag === 9 && data.CashClearingStatus !== "Completed") {
                        htmlButtonV1 = htmlButtonV1 + '<a class="btn btn-default" style=" margin-right: 5px;" title="Cash clearing" ng-click="cashClearing(' + data.Id + ',' + data.CashClearingId + ',' + data.CheckComplete + ',' + data.CashClearingStatusFlag + ')"><i class="fa fa-files-o"></i></a>' +
                            '<a class="btn btn-info" disabled="disabled" style="margin-right: 5px; margin-bottom: 10px" ng-href="" target="_blank"><span class="fa fa-print"></span></a>' +
                            '<a class="btn btn-default"  style=" background-color:#dd4b39;" ng-click="delete(' + data.Id + ',' + data.StatusFlag + ')" title="Delete"><i class="fa fa-trash-o" style="color:white;"></i></a>' +
                            '</div>';
                    } else if (data.StatusFlag === 9 && data.CashClearingStatus === "Completed") {
                        htmlButtonV1 = htmlButtonV1 + '<a class="btn btn-default" style=" margin-right: 5px;" title="Cash clearing" ng-click="cashClearing(' + data.Id + ',' + data.CashClearingId + ',' + data.CheckComplete + ',' + data.CashClearingStatusFlag + ')"><i class="fa fa-files-o"></i></a>' +
                            '<a class="btn btn-info" style="margin-right: 5px; margin-bottom: 10px" ng-href="{{printReportClear(' + data.CashClearingId + ')}}" target="_blank"><span class="fa fa-print"></span></a>' +
                            '<span class="btn btn-default" disabled="disabled" style="margin-right: 5px; background-color:#dd4b39" title="Delete"><i class="fa fa-trash-o" style="color:white"></i></span>' +
                            '</div>';
                    } else if (data.StatusFlag === 1 && data.Status === "Accounting") {
                        htmlButtonV1 = htmlButtonV1 + '<span class="btn btn-default" disabled="disabled" style="margin-right: 5px;" title="Cash clearing"><i class="fa fa-files-o"></i></span>' +
                            '<a class="btn btn-info" style="margin-right: 5px; margin-bottom: 10px" ng-href="{{printReportMemoCashAdvance(' + data.Id + ')}}" target="_blank"><span class="fa fa-print"></span></a>' +
                            '<span class="btn btn-default" disabled="disabled" style="margin-right: 5px; background-color:#dd4b39" title="Delete"><i class="fa fa-trash-o" style="color:white"></i></span>' +
                            '</div>';
                    } else {
                        htmlButtonV1 = htmlButtonV1 + '<span class="btn btn-default" disabled="disabled" style="margin-right: 5px;" title="Cash clearing"><i class="fa fa-files-o"></i></span>' +
                            '<a class="btn btn-info" disabled="disabled" style=" margin-right: 5px; margin-bottom: 10px"><span class="fa fa-print"></span></a>' +
                            '<span class="btn btn-default" disabled="disabled" style="margin-right: 5px; background-color:#dd4b39" title="Delete"><i class="fa fa-trash-o" style="color:white"></i></span>' +
                            '</div>';
                    }
                    return htmlButtonV1;
                }
            } else {
                if (data.RequestForUserName && data.RequestForUserName.toUpperCase() === CURRENT_USERNAME.toUpperCase()) {
                    if (data.StatusFlag === null || data.StatusFlag === 0) {
                        console.log("3");
                        return '<div class="btn-group" role="group">' +
                            '<a class="btn btn-default" style=" margin-right: 5px;" href="' + $routeParams.page + '/detail/' + data.Id + '" title="view"><i class="fa fa-file-text"></i></a>' +
                            '<a class="btn btn-default" style=" margin-right: 5px;" href="' + $routeParams.page + '/create/' + data.Id + '" title="edit"><i class="fa fa-edit"></i></a>' +
                            '<span class="btn btn-default" disabled="disabled" style=" margin-right: 5px;" title="Cash clearing"><i class="fa fa-files-o"></i></span>' +
                            '<a class="btn btn-info" disabled="disabled" style="margin-bottom: 10px"><span class="fa fa-print"></span>' +
                             '<a class="btn btn-default"  style=" background-color:#dd4b39;" ng-click="delete(' + data.Id + ')" title="Delete"><i class="fa fa-trash-o" style="color:white;"></i></a>' +
                            '</div>';
                    } else {
                        var htmlButtonV2 = '<div class="btn-group" role="group">' +
                       '<a class="btn btn-default" style=" margin-right: 5px;" href="' + $routeParams.page + '/detail/' + data.Id + '" title="view"><i class="fa fa-file-text"></i></a>' +
                       '<span class="btn btn-default" disabled="disabled" style=" margin-right: 5px;" title="edit"><i class="fa fa-edit"></i></span>';
                        if (data.StatusFlag === 9 && data.CashClearingStatus !== "Completed") {
                            htmlButtonV2 = htmlButtonV2 + '<a class="btn btn-default" style=" margin-right: 5px;" title="Cash clearing" ng-click="cashClearing(' + data.Id + ',' + data.CashClearingId + ',' + data.CheckComplete + ',' + data.CashClearingStatusFlag + ')" ><i class="fa fa-files-o"></i></a>' +
                                '<a class="btn btn-info" disabled="disabled" style="margin-bottom: 10px" ng-href="" target="_blank"><span class="fa fa-print"></span>' +
                                '<a class="btn btn-default"  style=" background-color:#dd4b39;" ng-click="delete(' + data.Id + ')" title="Delete"><i class="fa fa-trash-o" style="color:white;"></i></a>' +
                                '</div>';
                        } else if (data.StatusFlag === 9 && data.CashClearingStatus === "Completed") {
                            htmlButtonV2 = htmlButtonV2 + '<a class="btn btn-default" style=" margin-right: 5px;" title="Cash clearing" ng-click="cashClearing(' + data.Id + ',' + data.CashClearingId + ',' + data.CheckComplete + ',' + data.CashClearingStatusFlag + ')" ><i class="fa fa-files-o"></i></a>' +
                                '<a class="btn btn-info" style="margin-bottom: 10px" ng-href="{{printReportClear(' + data.CashClearingId + ')}}" target="_blank"><span class="fa fa-print"></span></a>' + '<span class="btn btn-default" disabled="disabled" style="margin-left: 5px; background-color:#dd4b39" title="Delete"><i class="fa fa-trash-o" style="color:white"></i></span>' +
                                '</div>';
                        } else if (data.StatusFlag === 1 && data.Status === "Accounting") {
                            htmlButtonV1 = htmlButtonV1 + '<a class="btn btn-default" style=" margin-right: 5px;" title="Cash clearing" ng-click="cashClearing(' + data.Id + ',' + data.CashClearingId + ',' + data.CheckComplete + ',' + data.CashClearingStatusFlag + ')"><i class="fa fa-files-o"></i></a>' +
                                '<a class="btn btn-info" style="margin-right: 5px; margin-bottom: 10px" ng-href="{{printReportMemoCashAdvance(' + data.Id + ')}}" target="_blank"><span class="fa fa-print"></span></a>' +
                                '<span class="btn btn-default" disabled="disabled" style="margin-right: 5px; background-color:#dd4b39" title="Delete"><i class="fa fa-trash-o" style="color:white"></i></span>' +
                                '</div>';
                        } else {
                            htmlButtonV2 = htmlButtonV2 + '<span class="btn btn-default" disabled="disabled" style="margin-right: 5px;"  title="Cash clearing"><i class="fa fa-files-o"></i></span>' +
                                '<a class="btn btn-info" disabled="disabled" style="margin-bottom: 10px"><span class="fa fa-print"></span>' +
                                '<span class="btn btn-default" disabled="disabled" style="margin-right: 5px; background-color:#dd4b39" title="Delete"><i class="fa fa-trash-o" style="color:white"></i></span>' +
                         '</div>';
                        }
                        return htmlButtonV2;
                        //return '<div class="btn-group" role="group">' +
                        //    '<a class="btn btn-default" style="width:25px; margin-right: 5px;" href="' + $routeParams.page + '/detail/' + data.Id + '" title="view"><i class="fa fa-file-text"></i></a>' +
                        //    '<span class="btn btn-default" disabled="disabled" style="width:25px; margin-right: 5px;" title="edit"><i class="fa fa-edit"></i></span>' +
                        //    '<a class="btn btn-default" style="width:25px; margin-right: 5px;" ng-click="cashClearing(' + data.Id + ',' + data.CashClearingId + ',' + data.CheckComplete + ')" ><i class="fa fa-files-o"></i></a>' +
                        //    '</div>';
                    }
                } else {
                    return '<div class="btn-group" role="group">' +
                        '<span class="btn btn-default" disabled="disabled" style="margin-right: 5px;" title="edit"><i class="fa fa-file-text"></i></span>' +
                        '<span class="btn btn-default" disabled="disabled" style=" margin-right: 5px;" title="edit"><i class="fa fa-edit"></i></span>' +
                        '<span class="btn btn-default" disabled="disabled" style=" margin-right: 5px;" title="edit"><i class="fa fa-files-o"></i></span>' +
                        '<a class="btn btn-info" disabled="disabled" style="margin-bottom: 10px"><span class="fa fa-print"></span>' +
                          '<span class="btn btn-default" disabled="disabled" style="margin-right: 5px; background-color:#dd4b39" title="Delete"><i class="fa fa-trash-o" style="color:white"></i></span>' +
                        '</div>';
                }
            }

            // disabled="disabled"
            //console.log(data);
            //return '<div class="btn-group" role="group">' +
            //    '<a class="btn btn-default" style="width:50px;margin-right: 5px;" href="' + $routeParams.page + '/detail/' + data.Id + '" title="view"><i class="fa fa-file-text"></i></a>' +
            //    '<a class="btn btn-default" style="width:50px;margin-right: 5px;" href="' + $routeParams.page + '/create/' + data.Id + '" title="edit"><i class="fa fa-edit"></i></a>' +
            //    '<a class="k-button btn-default" style="width:50px;margin-right: 5px;" ng-disabled="' + !data.CheckComplete + '" ng-click="cashClearing(' + data.Id + ',' + data.CheckComplete + ')" ><i class="fa fa-file-text"></i></a>' +
            //    '</div>';

        }

        idProposalService.set(0);
        var gridKendoOptionFunction = function (kendoData, readonly) {
            var indexOption = {
                model: {
                    id: "LineNo",
                    fields: {
                        LineNo: { type: "number" },
                        CurrentId: { type: "number", defaultValue: 0 },
                        CreatedDate: { type: "date" }
                    }
                },
                columns: [
                    { template: commandTemplateAllstatus, width: '150px' },
                    { field: 'Status', title: 'CashAdvance Status', width: '150px' },
                    { field: 'CashClearingStatus', title: 'CashClearing status', width: '150px' },
                    {
                        field: 'ProposalRefDocumentNumber', title: 'Proposal Number', width: '160px',
                        template: function (data) {
                            return '<a href="proposal/detail/' + data.ProposalRefID + '" target="_blank">' + data.ProposalRefDocumentNumber + '<a/>';
                        },
                    },
                    { field: 'DocumentNumber', title: 'Advance Number', width: '160px' },
                    {
                        field: 'CashClearingDocumentNumber', title: 'Clearing Number', width: '160px',
                        template: function (data) {
                            if (data.CashClearingDocumentNumber) {

                                return '<a href="cashclearing/detail/' + data.CashClearingId + '" target="_blank">' + data.CashClearingDocumentNumber + '<a/>';
                            }
                            return "";
                        },
                    },
                    {
                        //field: 'Title', title: 'Cash advance title', width: '200px',
                        field: 'Title', title: 'Title', width: '200px',
                        template: function (data) {
                            if (data.Title !== undefined) {
                                return "<div style='width:100%; word-wrap: break-word;'>" + data.Title + "</div>";
                            }
                            return "";
                        }
                    },
                    {
                        field: 'AdvanceDueDate',
                        title: 'Due date',
                        width: '150px',
                        template: inquiryGrid.formatDate('AdvanceDueDate')
                    },
                    { field: 'ExpenseTopicName', title: 'Expense topic', width: '200px' },
                    { field: 'APCodeName', title: 'A&P code', width: '150px' },
                    //{
                    //    field: 'Description', title: 'Description', width: '250px',
                    //    template: function (data) {
                    //        if (data.Description !== undefined) {
                    //            return "<div style='width:100%; word-wrap: break-word;'>" + data.Description + "</div>";
                    //        }
                    //        return "";
                    //    }
                    //},
                    { field: 'Budget', title: 'Budget', width: '100px', template: inquiryGrid.formatCurrency('Budget') },
                    //{ field: 'DocumentStatus', title: 'Document status ', width: '150px' },
                   
                   
                    inquiryGrid.createdDateColumn
                ]
            }

            return {
                dataSource: new kendo.data.DataSource({
                    type: "json",
                    data: kendoData,
                    sort:{ field: "CreatedDate", dir: "desc" },
                    batch: false,
                    //serverPaging: true,
                    //serverSorting: true,
                    //serverFiltering: true,
                    pageSize: 10,
                    schema: {
                        total: function(data) { return kendoData.length; },
                        //data: function (data) { return data.value; },
                        model: kendo.data.Model.define({
                            id: "Id",
                            fields: {
                                Budget: { type: "number" },
                                AdvanceDueDate: { type: "date" },
                                CreatedDate: { type: "date" }
                            }
                        })
                    },
                    error: function(e) {
                    }
                }),
                toolbar: kendo.template($("#ToolbarTemplateAdvance").html()),
                columns: indexOption.columns,
                readonly: false,
                scrollable: true,
                edit: function(e) {
                    e.model.set("LineNo", lineGrid.nextLineNo(this.dataSource.data()));
                },
                //
                groupable: false,
                pageable: {
                    refresh: true,
                    pageSizes: [5, 10, 20, 50, 100]
                },
                reorderable: false,
                resizable: false,
                selectable: "row",
                sortable: {
                    allowUnsort: true,
                    mode: "single"
                },
                height: 500,
                mobile: false,
                filterable: {
                    extra: true, //do not show extra filters
                    operators: {
                        // redefine the string operators
                        string: {
                            contains: "Contains"
                        }
                    }
                }//,
              //  sort: { field: "CreatedDate", dir: "desc" }
            }
        }

        ///////////
        $scope.cashClearing = function(id, cashClearingId, checkData, cashClearingStatusFlag) {
          
            if (checkData) {
                if (cashClearingId === 0) {
                    idCashAdvanceService.set(id);
                    $location.path('/cashclearing/create');
                } else {
                    if (cashClearingStatusFlag !== 0) {
                        $location.path('/cashclearing/detail/' + cashClearingId);
                    } else {
                        $location.path('/cashclearing/create/' + cashClearingId);
                    }
                
                }
            }
        }

        cashAdvanceDataSource.getIndex({ username: CURRENT_USERNAME }, function (data) {
            $scope.gridKendoOption = gridKendoOptionFunction(data);
        });
    }]);