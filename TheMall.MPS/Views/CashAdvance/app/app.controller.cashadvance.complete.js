angular.module('app.controllers').controller('CashAdvanceController',
[
    '$scope', '$routeParams', 'inquiryGrid', '$location', '$http', 'IdCashAdvanceService', 'cashAdvanceDataSource',
    'IdProposalService','appUrl',
    function($scope, $routeParams, inquiryGrid, $location, $http, idCashAdvanceService, cashAdvanceDataSource,
        idProposalService, appUrl) {


        $scope.printReport = function (pettyCashId) {
    
            if (pettyCashId) {

                var params = {
                    Id: pettyCashId,
                    inline: true,
                    //show: true,
                    fullscreen: true
                }
                var url = angular.crs.url.rootweb("report/CashAdvance/?params=" + JSON.stringify(params));
                return url;
            }
        }

        var gridKendoOptionFunction = function (kendoData, readonly) {
            var indexOption = {
                model: {
                    id: "LineNo",
                    fields: {
                        LineNo: { type: "number" },
                        CurrentId: { type: "number", defaultValue: 0 }
                    }
                },
                columns: [
                    { template: commandTemplateAllstatus, width: '185px' },
                     { field: 'DocumentStatus', title: 'Status ', width: '150px' },
                    { field: 'ProposalRefDocumentNumber', title: 'Proposal number', width: '180px' },
                    { field: 'DocumentNumber', title: 'Advance number', width: '150px' },
                    { field: 'CashClearingDocumentNumber', title: 'Clearing number', width: '150px' },
                    {
                        //field: 'Title', title: 'Cash advance title', width: '200px',
                        field: 'Title', title: 'Title', width: '200px',
                        template: function (data) {
                            if (data.Title !== undefined) {
                                return "<div style='width:100%; word-wrap: break-word;'>" + data.Title + "</div>";
                            }
                            return "";
                        }
                    },
                    {
                        field: 'AdvanceDueDate',
                        title: 'Due date',
                        width: '150px',
                        template: inquiryGrid.formatDate('AdvanceDueDate')
                    },
                    { field: 'ExpenseTopicName', title: 'Expense topic', width: '200px' },
                    { field: 'APCodeName', title: 'A&P code', width: '150px' },
                    //{
                    //    field: 'Description', title: 'Description', width: '250px',
                    //    template: function (data) {
                    //        if (data.Description !== undefined) {
                    //            return "<div style='width:100%; word-wrap: break-word;'>" + data.Description + "</div>";
                    //        }
                    //        return "";
                    //    }
                    //},
                    { field: 'Budget', title: 'Budget', width: '100px' },
                    //{ field: 'DocumentStatus', title: 'Document status ', width: '150px' },
                   
                    { field: 'CashClearingStatus', title: 'Clearing status', width: '150px' },
                    inquiryGrid.createdDateColumn
                ]
            }

            return {
                dataSource: new kendo.data.DataSource({
                    type: "json",
                    data: kendoData,
                    sort: null,
                    batch: false,
                    //serverPaging: true,
                    //serverSorting: true,
                    //serverFiltering: true,
                    pageSize: 10,
                    schema: {
                        total: function(data) { return kendoData.length; },
                        //data: function (data) { return data.value; },
                        model: kendo.data.Model.define({
                            id: "Id",
                            fields: {
                                Budget: { type: "number" },
                                AdvanceDueDate: { type: "date" },
                                CreatedDate: { type: "date" }
                            }
                        })
                    },
                    error: function(e) {
                    }
                }),
                toolbar: kendo.template($("#ToolbarTemplate").html()),
                columns: indexOption.columns,
                readonly: false,
                scrollable: true,
                edit: function(e) {
                    e.model.set("LineNo", lineGrid.nextLineNo(this.dataSource.data()));
                },
                //
                groupable: false,
                pageable: {
                    refresh: true,
                    pageSizes: [5, 10, 20, 50, 100]
                },
                reorderable: false,
                resizable: false,
                selectable: "row",
                sortable: {
                    allowUnsort: true,
                    mode: "single"
                },
                height: 500,
                mobile: false,
                filterable: {
                    extra: true, //do not show extra filters
                    operators: {
                        // redefine the string operators
                        string: {
                            contains: "Contains"
                        }
                    }
                }
            }
        }

        ///////////
        $scope.cashClearing = function(id, cashClearingId, checkData, cashClearingStatusFlag) {
            console.log("555+");
            if (checkData) {
                if (cashClearingId === 0) {
                    idCashAdvanceService.set(id);
                    $location.path('/cashclearing/create');
                } else {
                    if (cashClearingStatusFlag !== 0) {
                        $location.path('/cashclearing/detail/' + cashClearingId);
                    } else {
                        $location.path('/cashclearing/create/' + cashClearingId);
                    }
                
                }
            }
        }

        cashAdvanceDataSource.getIndex({ username: CURRENT_USERNAME }, function (data) {
           console.log(data);
            $scope.gridKendoOption = gridKendoOptionFunction(data);
        });
    }]);