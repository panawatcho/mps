﻿app = angular.module('app.controllers');
app.controller('CashAdvanceFormController',
    ['$scope',
    '$routeParams', 'controlOptions',
    'lineGrid', '$attrs',
    'cashAdvanceDataSource', 'uploadHandler',
    'k2', 'CashAdvanceGridService', 'messageBox', 'IdProposalService', 'proposalDataSource',
    'authService', '$location',
    '$filter', 'approval', '$sce', 'approverGrids',
    function (
        $scope,
        $routeParams, controlOptions,
        lineGrid, $attrs,
        dataSource, uploadHandler,
        k2, cashAdvanceGridService, messageBox, idProposalService, proposalDataSource,
        authService,$location,
        $filter, approval, $sce, approverGrids) {

        var sn = k2.extractSN($routeParams), remoteResult;
        //$scope.uploadHandler = uploadHandler.init();
        //$scope.attchmentSaveUrl = dataSource.$url + '/attachment';

        $scope.form = {
            ListPlace: [],
            DocTypeFiles: [],
            OtherFiles: [],
            CashAdvanceLines: new kendo.data.ObservableArray([]),
            CashAdvanceApproval: new kendo.data.ObservableArray([]),
            Requester: [],
            InformEmail: [],
            CCEmail: []
        };

        $scope.vendor = new kendo.data.ObservableArray([]);
        var icheckHaveProposal = false,
            icheckChangeBranch = false;

        //Set Due Date
        //Current date
        $scope.currentDate = new Date();
        $scope.currentDatePlusTwo = new Date($scope.currentDate);
        $scope.currentDatePlusTwo.setDate($scope.currentDatePlusTwo.getDate() + 2);
        //Set date Propose
        if ($scope.currentDatePlusTwo.getDay() === 6) {
            $scope.currentDatePlusTwo.setDate($scope.currentDatePlusTwo.getDate() + 2);
        }
        if ($scope.currentDatePlusTwo.getDay() === 0) {
            $scope.currentDatePlusTwo.setDate($scope.currentDatePlusTwo.getDate() + 1);
        }
        $scope.form.DueDatePropose = new Date($scope.currentDatePlusTwo);
        //Set date Accept
        var dueDatePropose = new Date($scope.form.DueDatePropose);
        dueDatePropose.setDate(dueDatePropose.getDate() + 2);
        if (dueDatePropose.getDay() === 6) {
            dueDatePropose.setDate(dueDatePropose.getDate() + 2);
        }
        if (dueDatePropose.getDay() === 0) {
            dueDatePropose.setDate(dueDatePropose.getDate() + 1);
        }
        $scope.form.DueDateAccept = new Date(dueDatePropose);
        //Set date Approve
        var dueDateAccept = new Date($scope.form.DueDateAccept);
        dueDateAccept.setDate(dueDateAccept.getDate() + 2);
        if (dueDateAccept.getDay() === 6) {
            dueDateAccept.setDate(dueDateAccept.getDate() + 2);
        }
        if (dueDateAccept.getDay() === 0) {
            dueDateAccept.setDate(dueDateAccept.getDate() + 1);
        }
        $scope.form.DueDateApprove = new Date(dueDateAccept);
        //Set date Final Approve
        var dueDateFinalApprove = new Date($scope.form.DueDateApprove);
        dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 2);
        if (dueDateFinalApprove.getDay() === 6) {
            dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 2);
        }
        if (dueDateFinalApprove.getDay() === 0) {
            dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 1);
        }
        $scope.form.DueDateFinalApprove = new Date(dueDateFinalApprove);

        //SetDate function 
        //Propose
        var setDateFromPropose = function() {
            //Set date Propose
            var dueDatePropose = new Date($scope.form.DueDatePropose);
            dueDatePropose.setDate(dueDatePropose.getDate() + 2);
            if (dueDatePropose.getDay() === 6) {
                dueDatePropose.setDate(dueDatePropose.getDate() + 2);
            }
            if (dueDatePropose.getDay() === 0) {
                dueDatePropose.setDate(dueDatePropose.getDate() + 1);
            }
            $scope.form.DueDateAccept = new Date(dueDatePropose);
            //Set date Accept
            var dueDateAccept = new Date($scope.form.DueDateAccept);
            dueDateAccept.setDate(dueDateAccept.getDate() + 2);
            if (dueDateAccept.getDay() === 6) {
                dueDateAccept.setDate(dueDateAccept.getDate() + 2);
            }
            if (dueDateAccept.getDay() === 0) {
                dueDateAccept.setDate(dueDateAccept.getDate() + 1);
            }
            $scope.form.DueDateApprove = new Date(dueDateAccept);
            //Set date Final Approve
            var dueDateFinalApprove = new Date($scope.form.DueDateApprove);
            dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 2);
            if (dueDateFinalApprove.getDay() === 6) {
                dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 2);
            }
            if (dueDateFinalApprove.getDay() === 0) {
                dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 1);
            }
            $scope.form.DueDateFinalApprove = new Date(dueDateFinalApprove);
        }

        $scope.changeDueDatePropose = function () {
            //Set date Accept
            var datePropose = new Date($scope.form.DueDatePropose);
            var dateAccept =  new Date($scope.form.DueDateAccept);
            if (datePropose.getFullYear() > dateAccept.getFullYear()) {
                setDateFromPropose();
            } else if(datePropose.getFullYear() === dateAccept.getFullYear()
            && datePropose.getMonth() > dateAccept.getMonth()) {
                setDateFromPropose();
            } else if (datePropose.getFullYear() === dateAccept.getFullYear()
            && datePropose.getMonth() === dateAccept.getMonth()
            && datePropose.getDate() > dateAccept.getDate()) {
                setDateFromPropose();
            }
        }

        //Accept
        var setDateFromAccept = function() {
            //Set date Accept
            var dueDateAccept = new Date($scope.form.DueDateAccept);
            dueDateAccept.setDate(dueDateAccept.getDate() + 2);
            if (dueDateAccept.getDay() === 6) {
                dueDateAccept.setDate(dueDateAccept.getDate() + 2);
            }
            if (dueDateAccept.getDay() === 0) {
                dueDateAccept.setDate(dueDateAccept.getDate() + 1);
            }
            $scope.form.DueDateApprove = new Date(dueDateAccept);
            //Set date Final Approve
            var dueDateFinalApprove = new Date($scope.form.DueDateApprove);
            dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 2);
            if (dueDateFinalApprove.getDay() === 6) {
                dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 2);
            }
            if (dueDateFinalApprove.getDay() === 0) {
                dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 1);
            }
            $scope.form.DueDateFinalApprove = new Date(dueDateFinalApprove);
        }

        $scope.changeDueDateAccept = function() {
            var dateAccept = new Date($scope.form.DueDateAccept);
            var dateApprove = new Date($scope.form.DueDateApprove);
            if (dateAccept.getFullYear() > dateApprove.getFullYear()) {
                setDateFromAccept();
            }else if (dateAccept.getFullYear() === dateApprove.getFullYear()
                && dateAccept.getMonth() > dateApprove.getMonth()) {
                setDateFromAccept();
            }
            if (dateAccept.getFullYear() === dateApprove.getFullYear()
                && dateAccept.getMonth() === dateApprove.getMonth()
                && dateAccept.getDate() > dateApprove.getDate()) {
                setDateFromAccept();
            }
        }

        //Approve
        var setDateFromApprove = function() {
            //Set date Final Approve
            var dueDateFinalApprove = new Date($scope.form.DueDateApprove);
            dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 2);
            if (dueDateFinalApprove.getDay() === 6) {
                dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 2);
            }
            if (dueDateFinalApprove.getDay() === 0) {
                dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 1);
            }
            $scope.form.DueDateFinalApprove = new Date(dueDateFinalApprove);
        }

        $scope.changeDueDateApprove = function () {
            var dateApprove = new Date($scope.form.DueDateApprove);
            var dateFinalApprove = new Date($scope.form.DueDateFinalApprove);
            if (dateApprove.getFullYear() > dateFinalApprove.getFullYear()) {
                setDateFromApprove();
            } else if (dateApprove.getFullYear() === dateFinalApprove.getFullYear() &&
                dateApprove.getMonth() > dateFinalApprove.getMonth()) {
                setDateFromApprove();
            } else if (dateApprove.getFullYear() === dateFinalApprove.getFullYear() &&
                dateApprove.getMonth() === dateFinalApprove.getMonth() &&
                dateApprove.getDate() > dateFinalApprove.getDate()) {
                setDateFromApprove();
            }
        }

        $scope.uploadHandler = uploadHandler.init();
        $scope.attchmentSaveUrl = angular.crs.url.webApi("cashadvance/:id/attachment");
        //Show print button in detail page.
        $scope.checkCompleted = false;
        $scope.checkAccounting = false;
        //Check Submit
        $scope.checkSubmit = false;
        //Branch Default Data
        $scope.dataBranch = new kendo.data.ObservableArray([]);
        $scope.approverLevelFiveAndSix = true;
        $scope.checkedUnitCode = false;
        $scope.Disable = false;
        $scope.expenseCheck = false;
        $scope.checkAP = false;

        var _cashAdvanceLinesGridOption;
        $scope.forms = {};
        //////Approver
        //check read only
        //Check that user push "Get final approve" or not.
        var isSave = false;
        var checkFinalApproverButton = false;
        var readonly;
        $scope.disabled= false;
        if ($routeParams.mode.toUpperCase() !== "CREATE" && $routeParams.mode.toUpperCase() !== "REVISE") {
            readonly = true;
            $scope.approverLevelFiveAndSix = true;
            $scope.disabled = true;
        } else {
            readonly = false;
            $scope.approverLevelFiveAndSix = false;
            $scope.disabled = false;
        }

        //Check add data
        var approverGridOptionLevelFourDataCheck = false;
        //Get Final Approver
        $scope.getFinalApproverData = function () {
            if ($routeParams.mode.toUpperCase() !== "CREATE" && $routeParams.mode.toUpperCase() !== "REVISE") {
                return false;
            }
            //if (!_cashAdvanceLinesGridOption ||
            //    !_cashAdvanceLinesGridOption.dataSource ||
            //    !_cashAdvanceLinesGridOption.dataSource._aggregateResult ||
            //    !_cashAdvanceLinesGridOption.dataSource._aggregateResult.NetAmount ||
            //    !_cashAdvanceLinesGridOption.dataSource._aggregateResult.NetAmount.sum) {
            //    $scope.form.Budget = 0;
            //} else {
            //    var total = _cashAdvanceLinesGridOption.dataSource._aggregateResult.NetAmount.sum;
            //    $scope.form.Budget = total;
            //}
            //$scope.ApproverGridOptionLevelFourData = new kendo.data.ObservableArray([]);
            //$scope.ApproverGridOptionLevelFour.dataSource.data($scope.ApproverGridOptionLevelFourData);

            var id = 0;
            if ($routeParams.id !== undefined) {
                console.log($routeParams.id);
                id = $routeParams.id;
            } else if (sn) {
                id = $scope.form.Id;
            }
            checkFinalApproverButton = true;
            if ($scope.form.UnitCode && $scope.form.Budget > 0) {
                dataSource.finalApprove({ budget: $scope.form.Budget, id: id, unitcode: $scope.form.UnitCode.UnitCode }, function (data) {
                    if (data && data.Employee) {
                        approverGridOptionLevelFourDataCheck = true;

                        var approverFinal = {
                            Employee: data.Employee,
                            ApproverSequence: data.ApproverSequence,
                            ApproverLevel: data.ApproverLevel,
                            ApproverUserName: data.ApproverUserName,
                            ApproverEmail: data.ApproverEmail,
                            Position: data.Position,
                            Department: data.Department,
                            DueDate: data.DueDate,
                            LineNo: 1,
                            LockEditable: 1
                        };
                        // $scope.ApproverGridOptionLevelFourData.push(approverFinal);
                        $scope.ApproverGridOptionLevelFourData = [];
                        $scope.ApproverGridOptionLevelFourData.push(approverFinal);
                        //$scope.ApproverGridOptionLevelFour.dataSource.data($scope.ApproverGridOptionLevelFourData);
                        //$scope.ApproverGridOptionLevelFourData = $scope.ApproverGridOptionLevelFour.dataSource.data();
                    } else {
                        messageBox.error("ไม่พบผู้อนุมัติคนสุดท้าย กรุณาติดต่อผู้ดูและระบบ");
                        $scope.ApproverGridOptionLevelFourData = new kendo.data.ObservableArray([]);
                        //$scope.ApproverGridOptionLevelFour.dataSource.data($scope.ApproverGridOptionLevelFourData);
                        //$scope.ApproverGridOptionLevelFourData = $scope.ApproverGridOptionLevelFour.dataSource.data();
                    }
                    $scope.ApproverGridOptionLevelFour.dataSource.data($scope.ApproverGridOptionLevelFourData);
                    $scope.ApproverGridOptionLevelFourData = $scope.ApproverGridOptionLevelFour.dataSource.data();
                });
            } else {
                if (!$scope.form.UnitCode)
                    messageBox.error("Please select unit code!!!");
                if ($scope.form.Budget <= 0)
                    messageBox.error("Budget must not be zero!!!");
            }
        }

        //Merge All Approver to One Array before save
        var mergeArrayData = function (data, array) {
            angular.forEach(array, function (value, key) {
                data.push(value);
            });

            return data;
        }

        //Set approver grid data source on load data process
        var setApprovalGridDataSource = function (key, lv, data) {
            var approverData = data.filter(function (result) {
                return result.ApproverLevel === lv;
            });
            if (lv === 'เสนอ') {
                if (approverData.length === 0) {
                    $scope.ApproverGridOptionLevelOneData = new kendo.data.ObservableArray([]);
                } else {
                    $scope.ApproverGridOptionLevelOneData = [];
                    angular.forEach(approverData, function (value, key) {
                        //var approverFinal = {
                        //    Employee: value.Employee,
                        //    ApproverSequence: value.ApproverSequence,
                        //    ApproverLevel: value.ApproverLevel,
                        //    ApproverUserName: value.ApproverUserName,
                        //    ApproverEmail: value.ApproverEmail,
                        //    Position: value.Position,
                        //    Department: value.Department,
                        //    DueDate: value.DueDate,
                        //    LineNo: value.LineNo
                        //};
                        $scope.ApproverGridOptionLevelOneData.push(value);
                    });
                }
                if (!isSave) {
                    var gridLevelOne = approverGrids.ApproverGridOption(readonly, $scope.ApproverGridOptionLevelOneData, lv);
                    $scope.ApproverGridOptionLevelOne = gridLevelOne;
                }
                $scope.ApproverGridOptionLevelOne.dataSource.data($scope.ApproverGridOptionLevelOneData);
                $scope.ApproverGridOptionLevelOneData = $scope.ApproverGridOptionLevelOne.dataSource.data();
            } else if (lv === 'เห็นชอบ') {
                if (approverData.length === 0) {
                    $scope.ApproverGridOptionLevelTwoData = new kendo.data.ObservableArray([]);
                } else {
                    $scope.ApproverGridOptionLevelTwoData = [];
                    angular.forEach(approverData, function (value, key) {
                        $scope.ApproverGridOptionLevelTwoData.push(value);
                    });
                }
                if (!isSave) {
                    var gridLevelTwo = approverGrids.ApproverGridOption(readonly, $scope.ApproverGridOptionLevelTwoData, lv);
                    $scope.ApproverGridOptionLevelTwo = gridLevelTwo;
                }
                $scope.ApproverGridOptionLevelTwo.dataSource.data($scope.ApproverGridOptionLevelTwoData);
                $scope.ApproverGridOptionLevelTwoData = $scope.ApproverGridOptionLevelTwo.dataSource.data();
            } else if (lv === 'อนุมัติ') {
                if (approverData.length === 0) {
                    $scope.ApproverGridOptionLevelThreeData = new kendo.data.ObservableArray([]);
                } else {
                    $scope.ApproverGridOptionLevelThreeData = [];
                    angular.forEach(approverData, function (value, key) {
                        $scope.ApproverGridOptionLevelThreeData.push(value);
                    });
                }
                if (!isSave) {
                    var gridLevelThree = approverGrids.ApproverGridOption(readonly, $scope.ApproverGridOptionLevelThreeData, lv);
                    $scope.ApproverGridOptionLevelThree = gridLevelThree;
                }
                $scope.ApproverGridOptionLevelThree.dataSource.data($scope.ApproverGridOptionLevelThreeData);
                $scope.ApproverGridOptionLevelThreeData = $scope.ApproverGridOptionLevelThree.dataSource.data();
            } else if (lv === 'อนุมัติขั้นสุดท้าย') {
                approverGridOptionLevelFourDataCheck = true;
                if (approverData.length === 0) {
                    checkFinalApproverButton = false;
                    $scope.ApproverGridOptionLevelFourData = new kendo.data.ObservableArray([]);
                } else {
                    checkFinalApproverButton = true;
                    $scope.ApproverGridOptionLevelFourData = [];
                    angular.forEach(approverData, function (value, key) {
                        $scope.ApproverGridOptionLevelFourData.push(value);
                    });
                }
                if (!isSave) {
                    var gridLevelFour = approverGrids.ApproverGridOptionLevelFour(readonly, $scope.ApproverGridOptionLevelFourData);
                    $scope.ApproverGridOptionLevelFour = gridLevelFour;
                }
                $scope.ApproverGridOptionLevelFour.dataSource.data($scope.ApproverGridOptionLevelFourData);
                $scope.ApproverGridOptionLevelFourData = $scope.ApproverGridOptionLevelFour.dataSource.data();
            }
        }

        //Set Data Before Save
        $scope.setDataApproverBeforeSave = function () {
            $scope.form.CashAdvanceApproval = new kendo.data.ObservableArray([]);
            $scope.form.CashAdvanceApproval = mergeArrayData($scope.form.CashAdvanceApproval, $scope.ApproverGridOptionLevelOneData);
            $scope.form.CashAdvanceApproval = mergeArrayData($scope.form.CashAdvanceApproval, $scope.ApproverGridOptionLevelTwoData);
            $scope.form.CashAdvanceApproval = mergeArrayData($scope.form.CashAdvanceApproval, $scope.ApproverGridOptionLevelThreeData);
            $scope.form.CashAdvanceApproval = mergeArrayData($scope.form.CashAdvanceApproval, $scope.ApproverGridOptionLevelFourData);
            //setApproverLevelFiveAndSixData($scope.forms.ApproverGridOptionLevelFiveData, $scope.ApproverLevel[4]);
            //setApproverLevelFiveAndSixData($scope.forms.ApproverGridOptionLevelSixData, $scope.ApproverLevel[5]);
            //$scope.form.CashAdvanceApproval = mergeArrayData($scope.form.CashAdvanceApproval, $scope.ApproverGridOptionLevelFiveData);
            //$scope.form.CashAdvanceApproval = mergeArrayData($scope.form.CashAdvanceApproval, $scope.ApproverGridOptionLevelSixData);
        }

        //var _cashAdvanceLinesGridOption;
        var setCashAdvanceLinesGridOptionGrid = function (d) {
            //$scope.form.CashAdvanceLines = new kendo.data.ObservableArray(d);
            if (d) {
                $scope.form.CashAdvanceLines = d;
            } else {
                $scope.form.CashAdvanceLines =  new kendo.data.ObservableArray([]);
            }
            //console.log($scope.form.PettyCashLines);
            if (_cashAdvanceLinesGridOption) {
                _cashAdvanceLinesGridOption.dataSource.data($scope.form.CashAdvanceLines);
                $scope.form.CashAdvanceLines = _cashAdvanceLinesGridOption.dataSource.data();
            }
        }

        $scope.ApproverLevel = approverGrids.ApproverLevel;
        //var _approvalGridOption = [];
        //$scope.ApproverLevel = ["เสนอ", "เห็นชอบ", "อนุมัติ", "ทราบ", "อนุมัติขั้นสุดท้าย", "สำเนาเรียน"];

        $scope.temp = { listPlace: [] }
        var proposalLineId = idProposalService.get();

        //Show grid in Index page
        var showHeadRequestInIndex = function (showData) {
            $scope.FormGrid = cashAdvanceGridService.CashAdvanceGrid(showData, false);
        }

        showHeadRequestInIndex($scope.form.CashAdvanceLines);
        $scope.data = [];
     
        //ReadOnly
        $scope.isReadonlyDetails = function () {
            if ($routeParams.mode.toUpperCase() !== "CREATE" && $routeParams.mode.toUpperCase() !== "REVISE") {
                //$($("#texteditor").data().kendoEditor.body).attr('contenteditable', false);
                return true;
            }
            return false;
        }

        if (proposalLineId !== undefined && proposalLineId !== 0) {
            remoteResult = dataSource.getdatabindadvance({ propLineid: proposalLineId }, function (data) {
                $scope.form = remoteResult;
                $scope.form.Requester = angular.copy(authService.authentication.me);
                $scope.form.OtherFiles = [];
                setCashAdvanceLinesGridOptionGrid($scope.form.CashAdvanceLines);
                //Set Due Date
                //Current date
                $scope.currentDate = new Date();
                $scope.currentDatePlusTwo = new Date($scope.currentDate);
                $scope.currentDatePlusTwo.setDate($scope.currentDatePlusTwo.getDate() + 2);
                //Set date Propose
                if ($scope.currentDatePlusTwo.getDay() === 6) {
                    $scope.currentDatePlusTwo.setDate($scope.currentDatePlusTwo.getDate() + 2);
                }
                if ($scope.currentDatePlusTwo.getDay() === 0) {
                    $scope.currentDatePlusTwo.setDate($scope.currentDatePlusTwo.getDate() + 1);
                }
                $scope.form.DueDatePropose = new Date($scope.currentDatePlusTwo);
                //Set date Accept
                dueDatePropose = new Date($scope.form.DueDatePropose);
                dueDatePropose.setDate(dueDatePropose.getDate() + 2);
                if (dueDatePropose.getDay() === 6) {
                    dueDatePropose.setDate(dueDatePropose.getDate() + 2);
                }
                if (dueDatePropose.getDay() === 0) {
                    dueDatePropose.setDate(dueDatePropose.getDate() + 1);
                }
                $scope.form.DueDateAccept = new Date(dueDatePropose);
                //Set date Approve
                dueDateAccept = new Date($scope.form.DueDateAccept);
                dueDateAccept.setDate(dueDateAccept.getDate() + 2);
                if (dueDateAccept.getDay() === 6) {
                    dueDateAccept.setDate(dueDateAccept.getDate() + 2);
                }
                if (dueDateAccept.getDay() === 0) {
                    dueDateAccept.setDate(dueDateAccept.getDate() + 1);
                }
                $scope.form.DueDateApprove = new Date(dueDateAccept);
                //Set date Final Approve
                dueDateFinalApprove = new Date($scope.form.DueDateApprove);
                dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 2);
                if (dueDateFinalApprove.getDay() === 6) {
                    dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 2);
                }
                if (dueDateFinalApprove.getDay() === 0) {
                    dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 1);
                }
                $scope.form.DueDateFinalApprove = new Date(dueDateFinalApprove);
            });
        }
            if ($routeParams.id != undefined) {
                console.log("CashAdvance");
                remoteResult = dataSource.get({ id: $routeParams.id }, function (data) {
                    // checkFinalApproverButton = true;
                    $scope.form = remoteResult;
                    //$scope.form.CashAdvanceLines = new kendo.data.ObservableArray($scope.form.CashAdvanceLines);
                    //$scope.FormGrid.dataSource.data($scope.form.CashAdvanceLines);
                    setCashAdvanceLinesGridOptionGrid($scope.form.CashAdvanceLines);
                    $scope.expenseCheck = true;
                   
                    // New Approve
                    for (var j = 0; j < $scope.ApproverLevel.length; j++) {
                        setApprovalGridDataSource(j, $scope.ApproverLevel[j], $scope.form.CashAdvanceApproval);
                    }
                    if (!$scope.form.CCEmail) {
                        $scope.form.CCEmail = [];
                    }
                    if (!$scope.form.InformEmail) {
                        $scope.form.InformEmail = [];
                    }
                    //Print Button
                    //if ($scope.form) {
                    //    if ($scope.form.StatusFlag === 9 && $scope.form.Requester.Username.toUpperCase() === CURRENT_USERNAME.toUpperCase()) {
                    //        $scope.checkCompleted = true;
                    //    }
                    //}

                    // Print Button (Accounting)
                    if ($scope.form) {
                        if ($scope.form.StatusFlag === 1 && $scope.form.Requester.Username.toUpperCase() === CURRENT_USERNAME.toUpperCase() && $scope.form.Status === "Accounting") {
                            $scope.checkAccounting = true;
                        }
                    }

                    // New Approve
                    //Set date Propose
                    $scope.form.DueDatePropose = new Date($scope.form.DueDatePropose);
                    $scope.form.DueDatePropose.setHours(7);
                    //Set date Accept
                    $scope.form.DueDateAccept = new Date($scope.form.DueDateAccept);
                    $scope.form.DueDateAccept.setHours(7);
                    //Set date Approve
                    $scope.form.DueDateApprove = new Date($scope.form.DueDateApprove);
                    $scope.form.DueDateApprove.setHours(7);
                    //Set date Final Approve
                    $scope.form.DueDateFinalApprove = new Date($scope.form.DueDateFinalApprove);
                    $scope.form.DueDateFinalApprove.setHours(7);

                    //$scope.form.CashAdvanceApproval = $scope.data;
                    //var dataArray = $scope.ApproverGridOption;
                    //$scope.form.CashAdvanceApproval = dataArray.dataSource.data();
                    $scope.uploadHandler.setId(remoteResult.Id);
                });
            } else if (sn) {
                remoteResult = dataSource.worklist({ sn: sn }, function () {
                    $scope.form = remoteResult;
                    setCashAdvanceLinesGridOptionGrid(remoteResult.CashAdvanceLines);
                    $scope.expenseCheck = true;
                    // checkFinalApproverButton = true;
                    //Print Button
                    // $scope.checkCompleted = $scope.form.StatusFlag === 9;//
                    // Approve
                    //New Approve
                    for (var j = 0; j < $scope.ApproverLevel.length; j++) {
                        setApprovalGridDataSource(j, $scope.ApproverLevel[j], $scope.form.CashAdvanceApproval);
                    }
                    if (!$scope.form.CCEmail) {
                        $scope.form.CCEmail = [];
                    }
                    if (!$scope.form.InformEmail) {
                        $scope.form.InformEmail = [];
                    }

                    $scope.uploadHandler.setId(remoteResult.Id).setSN(sn);

                    //Set date Propose
                    $scope.form.DueDatePropose = new Date($scope.form.DueDatePropose);
                    $scope.form.DueDatePropose.setHours(7);
                    //Set date Accept
                    $scope.form.DueDateAccept = new Date($scope.form.DueDateAccept);
                    $scope.form.DueDateAccept.setHours(7);
                    //Set date Approve
                    $scope.form.DueDateApprove = new Date($scope.form.DueDateApprove);
                    $scope.form.DueDateApprove.setHours(7);
                    //Set date Final Approve
                    $scope.form.DueDateFinalApprove = new Date($scope.form.DueDateFinalApprove);
                    $scope.form.DueDateFinalApprove.setHours(7);
                }, k2.onSnError);
            } else {
                // $scope.form.CreatedDate = new Date();
                $scope.form.Requester = angular.copy(authService.authentication.me);
                //New Approve
                //Approver Level One(เสนอ) //ทำเป็น Grid ใน ไฟล์เดียวอยู่นะ
                $scope.ApproverGridOptionLevelOneData = new kendo.data.ObservableArray([]);;
                var gridLevelOne = approverGrids.ApproverGridOption(readonly, $scope.ApproverGridOptionLevelOneData, $scope.ApproverLevel[0]);
                $scope.ApproverGridOptionLevelOne = gridLevelOne;
                $scope.ApproverGridOptionLevelOneData = $scope.ApproverGridOptionLevelOne.dataSource.data();
                //Approver Level Two(เห็นชอบ)
                $scope.ApproverGridOptionLevelTwoData = new kendo.data.ObservableArray([]);
                var gridLevelTwo = approverGrids.ApproverGridOption(readonly, $scope.ApproverGridOptionLevelTwoData, $scope.ApproverLevel[1]);
                $scope.ApproverGridOptionLevelTwo = gridLevelTwo;
                $scope.ApproverGridOptionLevelTwoData = $scope.ApproverGridOptionLevelTwo.dataSource.data();
                //Approver Level Three(อนุมัติ)
                $scope.ApproverGridOptionLevelThreeData = new kendo.data.ObservableArray([]);
                var gridLevelThree = approverGrids.ApproverGridOption(readonly, $scope.ApproverGridOptionLevelThreeData, $scope.ApproverLevel[2]);
                $scope.ApproverGridOptionLevelThree = gridLevelThree;
                $scope.ApproverGridOptionLevelThreeData = $scope.ApproverGridOptionLevelThree.dataSource.data();
                //Approver Level Four(อนุมัติขั้นสุดท้าย)
                $scope.ApproverGridOptionLevelFourData = new kendo.data.ObservableArray([]);
                var gridLevelFour = approverGrids.ApproverGridOptionLevelFour(readonly, $scope.ApproverGridOptionLevelFourData);
                $scope.ApproverGridOptionLevelFour = gridLevelFour;
                //    $scope.ApproverGridOptionLevelFour.dataSource.data($scope.ApproverGridOptionLevelFourData);
                $scope.ApproverGridOptionLevelFourData = $scope.ApproverGridOptionLevelFour.dataSource.data();
                //Approver Level Five(ทราบ)
                $scope.ApproverGridOptionLevelFiveData = new kendo.data.ObservableArray([]);

                //Approver Level Six(สำเนาเรียน)
                $scope.ApproverGridOptionLevelSixData = new kendo.data.ObservableArray([]);

                //Set Due Date
                //Current date
                $scope.currentDate = new Date();
                $scope.currentDatePlusTwo = new Date($scope.currentDate);
                $scope.currentDatePlusTwo.setDate($scope.currentDatePlusTwo.getDate() + 2);
                //Set date Propose
                if ($scope.currentDatePlusTwo.getDay() === 6) {
                    $scope.currentDatePlusTwo.setDate($scope.currentDatePlusTwo.getDate() + 2);
                }
                if ($scope.currentDatePlusTwo.getDay() === 0) {
                    $scope.currentDatePlusTwo.setDate($scope.currentDatePlusTwo.getDate() + 1);
                }
                $scope.form.DueDatePropose = new Date($scope.currentDatePlusTwo);
                //Set date Accept
                dueDatePropose = new Date($scope.form.DueDatePropose);
                dueDatePropose.setDate(dueDatePropose.getDate() + 2);
                if (dueDatePropose.getDay() === 6) {
                    dueDatePropose.setDate(dueDatePropose.getDate() + 2);
                }
                if (dueDatePropose.getDay() === 0) {
                    dueDatePropose.setDate(dueDatePropose.getDate() + 1);
                }
                $scope.form.DueDateAccept = new Date(dueDatePropose);
                //Set date Approve
                dueDateAccept = new Date($scope.form.DueDateAccept);
                dueDateAccept.setDate(dueDateAccept.getDate() + 2);
                if (dueDateAccept.getDay() === 6) {
                    dueDateAccept.setDate(dueDateAccept.getDate() + 2);
                }
                if (dueDateAccept.getDay() === 0) {
                    dueDateAccept.setDate(dueDateAccept.getDate() + 1);
                }
                $scope.form.DueDateApprove = new Date(dueDateAccept);
                //Set date Final Approve
                dueDateFinalApprove = new Date($scope.form.DueDateApprove);
                dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 2);
                if (dueDateFinalApprove.getDay() === 6) {
                    dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 2);
                }
                if (dueDateFinalApprove.getDay() === 0) {
                    dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 1);
                }
                $scope.form.DueDateFinalApprove = new Date(dueDateFinalApprove);
            }
        
        //$("#pageForm").kendoValidator({
        //    errorTemplate: "<span>#=message#</span>"
        //});
        ////About Dropdown and popup search
        //var allAboutDropDownAndPopUpSearch = function () {
            //Unit code dropdown
        //$scope.unitcodeOption = controlOptions.dropdown('unitcode');

        $scope.unitcodeOptionNew = { //function () {
            dataSource: {
                type: "json",
                data: angular.copy(authService.authentication.me).UnitCode
            },
            optionLabel: "please select",
            dataTextField: 'UnitName',
            dataValueField: 'UnitCode',
            template: '#=data.UnitName#',
            valueTemplate: '#=data.UnitName#',
            filter: "contains",
            filtering: function (ev) {
                if (ev.filter) {
                    var filterValue = ev.filter.value;
                    ev.preventDefault();
                    this.dataSource.filter({
                        logic: "or",
                        filters: [
                          {
                              field: "UnitName",
                              operator: "contains",
                              value: filterValue
                          },
                          {
                              field: "UnitCode",
                              operator: "contains",
                              value: filterValue
                          }
                        ]
                    });
                } else {
                    ev.preventDefault();
                }
            }
        }
        if (angular.copy(authService.authentication.me).UnitCode.length === 1) {
            //console.log(angular.copy(authService.authentication.me).UnitCode[0]);
            $scope.form.UnitCode = angular.copy(authService.authentication.me).UnitCode[0];
            if (sn) {
                $scope.checkedUnitCode = false;
            } else {
                $scope.checkedUnitCode = true;
            }
        }

            $scope.employeePopupSearchOption = controlOptions.popupSearch('employee', { multiple: true });
            //Proposal number search
            $scope.proposalNumberOption = controlOptions.popupSearch('proposalnumberref');
            $scope.proposalNumberOption2 = $scope.proposalNumberOption();
            $scope.proposalNumberOption2.show = function() {
                this.fields("unitCode").set("value", $scope.form.UnitCode.UnitCode);
            }

            //Employee popup search
            $scope.requesterOption = controlOptions.popupSearch('employee');
            //$scope.employeeOption = controlOptions.combobox('api/employee/searchAll');
            $scope.employeeOption = approverGrids.employeeOption;
            //Depositnumber popuosearch
            $scope.depositNumberOption = controlOptions.popupSearch('depositnumber');
            //Proposaltype dropdown
            $scope.proposalTypeOption = controlOptions.dropdown('typeproposal');
           // $scope.accountingOption = controlOptions.popupSearch('accounting', { multiple: true });

            //Branch Accounting Dropdown
            $scope.branchAccountingOptions = {
                dataTextField: "PlaceName",
                dataValueField: "PlaceCode",
                autoBind: false,
                optionLabel: "please select",
                template: '#=data.PlaceCode# - #=data.PlaceName#',
                valueTemplate: '#=data.PlaceCode# - #=data.PlaceName#',
                filter : "contains",
                //enable: false,
                dataSource: {
                    type: 'json',
                    //serverFiltering: true,
                    transport: {
                        read: {
                            url: function () {
                                return angular.crs.url.webApi("accounting/getaccountingbranch");
                            }
                        }
                    }
                }
            }

            //AP code dropdown
            $scope.apCodeOption = {
                dataSource: new kendo.data.DataSource({
                    type: "json",
                    data: new kendo.data.ObservableArray([])
                }),
                dataTextField: 'APName',
                dataValueField: 'ProposalLineId',
                template: '#=data.APCode# - #=data.APName# #if(data.DescriptionLine){#- #=data.DescriptionLine # #}#',
                valueTemplate: '#=data.APCode# - #=data.APName# #if(data.DescriptionLine){#- #=data.DescriptionLine # #}#',
                optionLabel: "please select",
                dataBound: function () {
                    if (sn) {
                        this.enable(false);
                    }
                }
            }

            $scope.apCodeOption.select = function (data) {
                //   checkFinalApproverButton = false;
                //console.log($scope.form.ProposalRef);
                var dataItem = this.dataItem(data.item);
                //if (dataItem.APCode !== "") {
                //    var list = $scope.form.ProposalRef.ProposalLine.filter(function (data) {
                //        return data.ExpenseTopicCode === dataItem.ExpenseTopicCode
                //            && data.APCode.APCode === dataItem.APCode &&
                //            data.UnitCode.UnitCode === $scope.form.UnitCode.UnitCode;
                //    });
                $scope.checkAP = false;
                $scope.form.BudgetPlan = dataItem.BudgetPlan;
                $scope.form.ProposalLineId = dataItem.ProposalLineId;
                $scope.TotalBudgetPlan = dataItem.TotalBudgetPlan;

            }

            //Expense topic dropdown
            $scope.expenseTopicOption = {
                dataSource: new kendo.data.DataSource({
                    type: "json",
                    data: new kendo.data.ObservableArray([])
                }),
                dataTextField: 'ExpenseTopicName',
                dataValueField: 'ExpenseTopicCode',
                template: '#=data.ExpenseTopicCode# - #=data.ExpenseTopicName#',
                valueTemplate: '#=data.ExpenseTopicCode# - #=data.ExpenseTopicName#',
                optionLabel: "please select"
            }

            $scope.expenseTopicOption.select = function (data) {
                // $scope.checkAP = false;
                var dataItem = this.dataItem(data.item);
                var apcodes = $filter('filter')($scope.form.ProposalRef.APCode, {
                    UnitCode: $scope.form.UnitCode.UnitCode,
                    ExpenseTopicCode: dataItem.ExpenseTopicCode
                });
                $scope.apCodeOption.dataSource.data(apcodes);
                $scope.form.BudgetPlan = null;
                $scope.TotalBudgetPlan = null;
            }

            //Place dropdown
            $scope.placeOptions = {
                dataTextField: "PlaceName",
                dataValueField: "PlaceCode",
                autoBind: false,
                enable: false,
                dataSource: new kendo.data.DataSource({
                    type: "json",
                    data: new kendo.data.ObservableArray([])
                })
            }

            //Branch dropdown
            $scope.branchOptions = {
                dataSource: new kendo.data.DataSource({
                    type: "json",
                    data: $scope.dataBranch
                }),
                dataTextField: "PlaceCode",
                dataValueField: "PlaceCode",
                optionLabel: "please select",
                template: '#=data.PlaceCode# - #=data.PlaceName#',
                valueTemplate: '#=data.PlaceCode# - #=data.PlaceName#',
                open: function (e) {
                    var listContainer = e.sender.list.closest(".k-list-container");
                    var table = listContainer.children("template-dropdowlist-header");
                    if (table.length > 0) {
                        listContainer.width('auto');
                    }
                }
            }
            //$scope.branchOptions.select = function (data) {
            //    var dataItem = this.dataItem(data.item);
        //}

        ////Print Button
        //$scope.chkDeposit = false;
        //if ($scope.form) {
        //    if ($scope.form.StatusFlag === 9 && $scope.form.Requester.Username.toUpperCase() === CURRENT_USERNAME.toUpperCase()) {
        //        $scope.checkCompleted = true;
        //    }
        //}

        $scope.companyOptions = {
            dataSource: new kendo.data.DataSource({
                type: "json",
                data: new kendo.data.ObservableArray([])
            }),
            //optionLabel: "please select",
            dataTextField: "CompanyName",
            dataValueField: "VendorCode",
            template: "#=data.CompanyName#",
            valueTemplate: "#=data.CompanyName#",
            sort: {
                field: "Order",
                dir: "asc"
            }
        };

        // Print Button (Accounting)
        if ($scope.form) {
            if ($scope.form.StatusFlag === 1 && $scope.form.Requester.Username.toUpperCase() === CURRENT_USERNAME.toUpperCase() && $scope.form.Status === "Accounting") {
                $scope.checkAccounting = true;
            }
        }

        $scope.expenseTopic = new kendo.data.ObservableArray([]);
        $scope.apcode = new kendo.data.ObservableArray([]);
        $scope.ProposalRefTitle = "";

        $scope.$watch('form.ProposalRef', function () {
            if ($scope.form.ProposalRef && $scope.form.ProposalRef.ListPlace) {
                if ($scope.form.UnitCode != undefined) {
                    if (icheckHaveProposal) {
                        $scope.form.Branch = "";
                        $scope.form.Company = [];
                        $scope.branchOptions.dataSource.data([]);
                        $scope.companyOptions.dataSource.data([]);
                    }
                    icheckHaveProposal = true;

                    $scope.temp.listPlace = new kendo.data.ObservableArray($scope.form.ProposalRef.ListPlace);
                    if ($scope.temp.listPlace[0] !== null) {
                        var checkM02Data = $scope.form.ProposalRef.ListPlace.filter(function (data) {
                            return data.PlaceCode === "M02" && data.PlaceName === "Ramkhamhaeng";
                        });
                        $scope.placeOptions.dataSource.data(new kendo.data.ObservableArray($scope.form.ProposalRef.ListPlace));
                        $scope.temp.listPlace = new kendo.data.ObservableArray($scope.form.ProposalRef.ListPlace);
                        if (checkM02Data.length === 0) {
                            var m02Data = {
                                PlaceCode: "M02",
                                PlaceName: "Ramkhamhaeng",
                                InActive: false
                            }
                            var dataPlace = new kendo.data.ObservableArray($scope.form.ProposalRef.ListPlace);
                            var dataBrach = dataPlace;
                            dataBrach.push(m02Data);
                            $scope.branchOptions.dataSource.data(dataBrach);
                        } else {
                            $scope.branchOptions.dataSource.data(new kendo.data.ObservableArray($scope.temp.listPlace));
                        }
                        //var place = $scope.form.ProposalRef.ListPlace;
                        //var checkM02 = place.filter(function(data) {
                        //    return data.PlaceCode.toUpperCase() === "M02";
                        //});

                        //if (checkM02.length === 0) {
                        //    var m02 = {
                        //        PlaceCode: "M02",
                        //        PlaceName: "Ramkhamhaeng"
                        //    };
                        //    place.push(m02);
                        //}
                        //$scope.branchOptions.dataSource.data(new kendo.data.ObservableArray(place));
                    } else {
                        $scope.branchOptionsDropDownList.value('');
                        $scope.branchOptions.dataSource.data(new kendo.data.ObservableArray([]));
                        $scope.placeOptionsMultiSelectList.value('');
                    }

                    //company
                    $scope.dataCompany = $scope.form.ProposalRef.Company;

                    var proposalLine = $scope.form.ProposalRef.ProposalLine.filter(function (data) {
                        return data.UnitCode.UnitCode === $scope.form.UnitCode.UnitCode;
                    });
                    //var proposalLine = $filter('filter')($scope.form.ProposalRef.ProposalLine, { UnitCode: $scope.form.UnitCode });
                    if (proposalLine && proposalLine.length > 0) {
                        if (sn) {
                            $scope.checkedUnitCode = false;
                            $scope.expenseCheck = false;
                            $scope.checkSubmit = true;
                        } else {
                            $scope.checkSubmit = false;
                            $scope.expenseCheck = true;
                            $scope.checkedUnitCode = true;
                        }
                        $scope.expenseTopic = new kendo.data.ObservableArray([]);
                        $scope.apcode = new kendo.data.ObservableArray([]);
                        $scope.expenseTopicTemp = new kendo.data.ObservableArray([]);
                        angular.forEach(proposalLine, function(propline) {
                            $scope.expenseTopicTemp.push(propline.ExpenseTopic);
                            //$scope.apcode.push(propline.APCode);
                        });
                        angular.forEach($scope.expenseTopicTemp, function(temp) {
                            if ($scope.expenseTopic.length > 0) {
                                var check = $filter('filter')($scope.expenseTopic, { ExpenseTopicCode: temp.ExpenseTopicCode });
                                if (check.length === 0) {
                                    $scope.expenseTopic.push(temp);
                                }
                            } else {
                                $scope.expenseTopic.push(temp);
                            }
                        });
                        $scope.expenseTopicOption.dataSource.data($scope.expenseTopic);
                        //$scope.apCodeOption.dataSource.data($scope.form.ProposalRef.APCode);
                       // var apcodes = $filter('filter')($scope.form.ProposalRef.APCode, { UnitCode: $scope.form.UnitCode.UnitCode });
                        var apcodes = ($scope.form.ExpenseTopics) ? $filter('filter')($scope.form.ProposalRef.APCode, {
                            UnitCode: $scope.form.UnitCode.UnitCode,
                            ExpenseTopicCode: $scope.form.ExpenseTopics.ExpenseTopicCode
                        }) : $filter('filter')($scope.form.ProposalRef.APCode, {
                            UnitCode: $scope.form.UnitCode.UnitCode
                        });
                        $scope.apCodeOption.dataSource.data(apcodes);

                        var list;
                        if ($routeParams.id != undefined) {
                            if ($scope.ProposalRefTitle === "") {
                                $scope.ProposalRefTitle = $scope.form.ProposalRef.Title;
                                list = $scope.form.ProposalRef.ProposalLine.filter(function (data) {
                                    return data.Id === $scope.form.ProposalLineId;
                                });
                                $scope.form.BudgetPlan = list[0].BudgetPlan;
                                $scope.TotalBudgetPlan = list[0].TotalBudgetPlan;
                            } else {
                                if ($scope.ProposalRefTitle !== $scope.form.ProposalRef.Title) {
                                    $scope.ProposalRefTitle = $scope.form.ProposalRef.Title;
                                    $scope.form.BudgetPlan = null;
                                    $scope.TotalBudgetPlan = null;
                                }
                            }
                        } else if (sn) {
                            list = $scope.form.ProposalRef.ProposalLine.filter(function(data) {
                                return data.Id === $scope.form.ProposalLineId;
                            });

                            if (!angular.isUndefined(list) && list.length !== 0) {
                                $scope.form.BudgetPlan = list[0].BudgetPlan;
                                $scope.TotalBudgetPlan = list[0].TotalBudgetPlan;
                                if ($routeParams.mode.toUpperCase() === "REVISE") {
                                    $scope.form.BudgetPlan = $scope.form.BudgetPlan + $scope.form.BudgetTrans;
                                    $scope.TotalBudgetPlan = $scope.TotalBudgetPlan + +$scope.form.BudgetTrans;
                                }
                            }
                        } else if (proposalLineId !== undefined && proposalLineId !== 0) {
                            list = $scope.form.ProposalRef.ProposalLine.filter(function(data) {
                                return data.Id === $scope.form.ProposalLineId;
                            });
                            if (!angular.isUndefined(list) && list.length !== 0) {
                                $scope.form.BudgetPlan = list[0].Remainning;
                                $scope.TotalBudgetPlan = list[0].TotalBudgetPlan;
                            }
                        } else {
                            $scope.form.BudgetPlan = null;
                            $scope.TotalBudgetPlan = null;
                        }
                    }
                } else {
                    messageBox.error("Please, select Unit Code before select Proposal.");
                }
            } else if (icheckHaveProposal) {
                $scope.form.ProposalRef = "";
                $scope.form.ProposalRef.DocumentNumber = "";
                $scope.form.ProposalRef.Title = "";
                $scope.form.ExpenseTopics = [];
                $scope.form.APCodes = [];
                $scope.form.ExpenseTopic = [];
                $scope.form.APCode = [];
                $scope.form.BudgetPlan = null;
                $scope.TotalBudgetPlan = null;
                $scope.temp.listPlace = [];
                $scope.expenseCheck = false;
                $scope.expenseTopicDropDownList.value('');
                //$scope.expenseTopicOption.dataSource.data(new kendo.data.ObservableArray($scope.form.ExpenseTopics));
                //$scope.apCodeOption.dataSource.data(new kendo.data.ObservableArray($scope.form.APCodes));
                $scope.branchOptions.dataSource.data(new kendo.data.ObservableArray($scope.temp.listPlace));
                $scope.form.Branch = "";
                $scope.form.Company = [];
                $scope.companyOptions.dataSource.data([]);
            }
        });


        $scope.$watch("form.Branch",
            function () {
                var dataCompany = $scope.form.Company;
                $scope.companyOptions.dataSource.data([]);
                $scope.listCompany = new kendo.data.ObservableArray([]);
                if ($scope.form.Branch) {
                    if ($scope.form.Branch.PlaceCode) {
                        angular.forEach($scope.dataCompany,
                            function (value, key) {
                                if (value.BranchCode === $scope.form.Branch.PlaceCode) {
                                    $scope.listCompany.push(value);
                                }
                            });
                        $scope.companyOptions.dataSource.data(new kendo.data.ObservableArray($scope.listCompany));
                        if (!icheckChangeBranch) {
                            $scope.form.Company = dataCompany;
                        }
                        //if (!$scope.form.Company) {
                        //    if ($scope.listCompany.length === 1) {
                        //        $scope.form.Company = ($scope.form.Company)
                        //            ? $scope.form.Company
                        //            : $scope.listCompany[0];
                        //    }
                        //} else {
                        //    $scope.form.Company = $scope.form.Company;
                        //}
                    } else {
                        $scope.companyOptions.dataSource.data([]);
                    }
                }
            });

        $scope.branchOptions.select = function (data) {
            var vendorlist = $("#company").data("kendoDropDownList");
            vendorlist.bind("dataBound", function (e) {
                this.select(0);
                this.trigger("change");
            });
            icheckChangeBranch = true;
        };

        //Text Editor
        $scope.trustAsHtml = function (string) {
            return $sce.trustAsHtml(string);
        };

        var originwidth = 0;
        var originheight = 0;

        $scope.DescriptionEditor = {
            paste: function (e) {
                setTimeout(function () {
                    $("iframe").contents().find("div").attr("style", "max-width: 100%");
                    $("iframe").contents().find("img").attr("style", "max-width:100%;");
                }, 500);
            },
            tools: [
                    "bold",
                    "italic",
                    "underline",
                    "strikethrough",
                    "justifyLeft",
                    "justifyCenter",
                    "justifyRight",
                    "justifyFull",
                    "insertUnorderedList",
                    "insertOrderedList",
                    "fontName",
                    "fontSize",
                    "foreColor",
                    "backColor",
                    "createTable",
                    "print",
                        {
                            name: "maximize",
                            tooltip: "Maximize",
                            exec: function (e) {

                                if (originheight == 0 || originwidth == 0) {
                                    originheight = $("td.k-editable-area iframe.k-content").height();
                                    originwidth = $("td.k-editable-area iframe.k-content").width();
                                }
                                var editor = $(this).data("kendoEditor");
                                editor.wrapper.css({
                                    "z-index": 9999,
                                    width: $(window).width(),
                                    height: $(window).height(),
                                    position: "fixed",
                                    left: 0,
                                    top: 0
                                    //left: $("aside.main-sidebar .ng-scope").width(),
                                    //top: $("nav.navbar.navbar-static-top").height()
                                });
                                //editor.wrapper.css("height", "800px");
                            }
                        },
                    {
                        name: "restore",
                        tooltip: "Restore",
                        exec: function (e) {
                            var editor = $(this).data("kendoEditor");
                            editor.wrapper.css({
                                width: originwidth,
                                height: originheight,
                                position: "relative"
                            });
                        }
                    }
            ]
        }
       
        //function back-end
        //dataSource.approve(function (data) {

        $scope.validate = function(event, preventSubmit) {
            //Approver
            $scope.setDataApproverBeforeSave();

            //if ($scope.form.Budget <= $scope.form.BudgetPlan) {
                if (preventSubmit !== false) {
                    checkFinalApproverButton = true;
                }
                if (checkFinalApproverButton) {
                    k2.saveAndSubmit({
                        event: event,
                        preventSubmit: preventSubmit,
                        validator: $scope.validator,
                        save: dataSource.save,
                        data: $scope.form,
                        saveSuccess: function (response) {
                            if ($routeParams.mode.toUpperCase() === "REVISE") {
                                location.reload();
                            } else {
                                $scope.form = response;
                                proposalLineId = undefined;
                                isSave = true;
                                //location.reload();
                                 $location.path("cashadvance/create/" + response.Id);
                                setCashAdvanceLinesGridOptionGrid(response.CashAdvanceLines);
                                //$scope.expenseCheck = true;

                                for (var j = 0; j < $scope.ApproverLevel.length; j++) {
                                    setApprovalGridDataSource(j, $scope.ApproverLevel[j], response.CashAdvanceApproval);
                                }
                                if (!$scope.form.CCEmail) {
                                    $scope.form.CCEmail = [];
                                }
                                if (!$scope.form.InformEmail) {
                                    $scope.form.InformEmail = [];
                                }
                                $("#ccEmail").data("kendoMultiSelect").dataSource.data($scope.form.CCEmail);
                                $("#informEmail").data("kendoMultiSelect").dataSource.data($scope.form.InformEmail);
                                //Set date Propose
                                $scope.form.DueDatePropose = new Date($scope.form.DueDatePropose);
                                $scope.form.DueDatePropose.setHours(7);
                                //Set date Accept
                                $scope.form.DueDateAccept = new Date($scope.form.DueDateAccept);
                                $scope.form.DueDateAccept.setHours(7);
                                //Set date Approve
                                $scope.form.DueDateApprove = new Date($scope.form.DueDateApprove);
                                $scope.form.DueDateApprove.setHours(7);
                                //Set date Final Approve
                                $scope.form.DueDateFinalApprove = new Date($scope.form.DueDateFinalApprove);
                                $scope.form.DueDateFinalApprove.setHours(7);

                                icheckHaveProposal = false;
                                icheckChangeBranch = false;
                            }

                        },
                        submitSuccess: function (response) {
                            if (preventSubmit === false) {
                            }
                        },
                        uploadHandler: $scope.uploadHandler
                             //uploadSuccessIsCallback: true,
                             //uploadSuccess: function (response, callback) {
                             // },
                    });
                } else {
                    messageBox.error("Please push get final approver button!!!");
                }
            //} else {
            //    messageBox.error("The budget sum value must not be more than the butget plan!!!");
            //}
        }

        $scope.openK2ActionModal = function (action) {
            $scope.choosenAction = action;
            $scope.modalInstance = k2.createActionModal.call($scope);
        }

        $scope.chooseAction = function (action) {
            /*if ($routeParams.mode.toUpperCase() === "REVISE" && $scope.form.Budget <= 0) {
                messageBox.error("Butget must not be 0");
            } else if ($routeParams.mode.toUpperCase() === "REVISE" && $scope.form.Budget >= $scope.form.BudgetPlan) {
                messageBox.error("The budget sum value must not be more than the butget plan!!!");
            } else*/
            if (checkFinalApproverButton) {
                $scope.Action = action;
                //Approver
                $scope.setDataApproverBeforeSave();
                k2.chooseAction({
                    validator: $scope.validator,
                    actions: $scope.form.Actions,
                    action: action,
                    resource: $scope.form,
                    uploadHandler: $scope.uploadHandler
                });
            } else {
                messageBox.error("Please push get final approver button!!!");
            }
        }

        //Line Grid
        //$scope.isReadonlyDetails = function () {
        //    if ($routeParams.mode.toUpperCase() !== "CREATE" && $routeParams.mode.toUpperCase() !== "REVISE") {
        //        return true;
        //    }
        //    return false;
        //}

        var cashAdvanceLineSchema = {
            model: kendo.data.Model.define({
                id: "LineNo",
                fields: {
                    LineNo: { type: "number" },
                    Description: { type: "strings", validation: { required: true } },
                    Unit: { type: "number", validation: { min: 0, required: true } },
                    Amount: { type: "number", validation: { min: 0, required: true } },
                    Tax: { type: "number", validation: { min: 0, required: true } },
                    Vat: { type: "number", validation: { min: 0, required: true } },
                    NetAmount: { type: "number", editable: false },
                    NetAmountNoVatTax: { type: "number", validation: { min: 0, required: true } }
                }
            })
        };

        $scope.CashAdvanceLinesGridOption = function () {
            _cashAdvanceLinesGridOption = lineGrid.gridOption({
                schema: cashAdvanceLineSchema,
                data: $scope.form.CashAdvanceLines,
                aggregate: [
                 { field: "Amount", aggregate: "sum" },
                 { field: "NetAmount", aggregate: "sum" },
                 { field: "NetAmountNoVatTax", aggregate: "sum" }
                ],
                columns: [
                        {
                            field: 'Description', title: 'Description', width: 200,
                            template: function (data) {
                                if (data.Description !== undefined) {
                                    return "<div style='width:100%; word-wrap: break-word;'>" + data.Description + "</div>";
                                }
                                return "";
                            },
                            footerTemplate: "<div class='col-xs-6 col-sm-6 col-md-3 col-lg-3'>Total</div>" 
                        },
                    {
                        field: 'Unit', title: 'Unit', width: "70px"
                    },
                        {
                            field: 'Amount', title: 'Price/Unit',
                            width: "90px",
                            template: lineGrid.formatCurrency('Amount')
                        },
                    {
                        field: "Vat", title: "VAT %", width: "80px",
                        editor: function (container, options) {
                            $scope.vatValue = approverGrids.vatOption;                       
                            var editor = $('<select kendo-drop-down-list name="Vat" k-options="vatValue" style="width: 100%"></select>');
                            editor.attr("data-bind", "value:Vat,source:null");
                            editor.attr("style", "width:100%; word-wrap: break-word;");
                            editor.appendTo(container);
                            return editor;
                        }
                    },
                    {
                        field: "Tax", title: "TAX %", width: "80px",
                        editor: function (container, options) {
                            $scope.taxValue = approverGrids.taxOption;
                            var editor = $('<select kendo-drop-down-list name="Tax" k-options="taxValue" style="width: 100%"></select>');
                            editor.attr("data-bind", "value:Tax,source:null");
                            editor.attr("style", "width:100%; word-wrap: break-word;");
                            editor.appendTo(container);
                            return editor;
                        }
                    },
                        {
                            field: "NetAmount", title: "Net amount", width: "100px",
                            template: lineGrid.formatCurrency('NetAmount'),
                            footerTemplate: //"<div class='col-xs-6 col-sm-6 col-md-3 col-lg-3'>Total</div>" +
                                            "<div class='text-right'>{{sumAmount() | currency:' ':2}}</div>"
                            //"<div class='text-right'>#:(sum)? kendo.toString(sum,'n2'):0.00#</div>"
                        },
                        {
                            field: 'Remark', title: 'Remark', width: "200px",
                            template: function (data) {
                                if (data.Remark !== undefined) {
                                    return "<div style='width:100%; word-wrap: break-word;'>" + data.Remark + "</div>";
                                }
                                return "";
                            }
                        },
                        { field: 'Attachment', title: 'Attachment', width: "150px" },
                    {
                        field: "NetAmountNoVatTax",
                        title: "NetAmountNoVatTax",
                        width: "150px",
                        template: lineGrid.formatCurrency('NetAmountNoVatTax'),
                        footerTemplate: "<div class='text-right'>{{sumNetAmountNoVatTax() | currency:' ':2}}</div>",
                        hidden: true
                    }

                ],
                editable: {
                    //mode: "inline"
                   mode: "incell"
                },
                readonly: $scope.isReadonlyDetails(),
                noedit: true,
                //template: $('#_IncomeDeposit').html(),
                height: "400px",
                save: function (e) {
                    checkFinalApproverButton = false;
                    $scope.setNetAmount(e);
                    $scope.calNetAmountNoTaxVat(e);
                    this.saveChanges();
                },
                pageable: false,
                groupable: true,
              //  noedit: true,
       
            });
            return _cashAdvanceLinesGridOption;
        }

        //Check NetAMount
        $scope.setNetAmount = function (e) {
            //
            var unitValue = e.model.Unit;
            if (e.values.Unit || e.values.Unit === 0) {
                unitValue = e.values.Unit;
            }
            //
            var vatValue = e.model.Vat;
            if (e.values.Vat || e.values.Vat === 0) {
                vatValue = e.values.Vat;
            }
            //
            var taxValue = e.model.Tax;
            if (e.values.Tax || e.values.Tax === 0) {
                taxValue = e.values.Tax;
            }
            //
            var amountValue = e.model.Amount;
            if (e.values.Amount || e.values.Amount === 0) {
                amountValue = e.values.Amount;
                console.log("s");
            }

            var rawSumAmountValue = unitValue * amountValue;
            var vat;
            var amountWithVatAndTax;
            var tax;
            if (taxValue !== 0 && vatValue !== 0) {
                tax = ((taxValue * rawSumAmountValue) / 100).toFixed(2);
                vat = ((vatValue * rawSumAmountValue) / 100).toFixed(2);
                amountWithVatAndTax = vat - tax;
                e.model.NetAmount = rawSumAmountValue + amountWithVatAndTax;
            } else {
                if (taxValue === 0 && vatValue !== 0) {
                    vat = parseFloat(((vatValue * rawSumAmountValue) / 100).toFixed(2));
                    e.model.NetAmount = rawSumAmountValue + parseFloat(vat);
                } else {
                    if (taxValue !== 0 && vatValue === 0) {
                        tax = parseFloat(((taxValue * rawSumAmountValue) / 100).toFixed(2));
                        var sum = rawSumAmountValue - tax;
                        e.model.NetAmount = sum;
                    } else {
                        if (taxValue === 0 && vatValue === 0) {
                            e.model.NetAmount = rawSumAmountValue;
                        }
                    }
                }
            }
            e.model.NetAmount = parseFloat(Math.round(e.model.NetAmount * 100) / 100).toFixed(2); //
            return e.model;
        }

        //Show Budget from Grid sum Function
        $scope.sumAmount = function () {
            if (!_cashAdvanceLinesGridOption ||
                !_cashAdvanceLinesGridOption.dataSource ||
                !_cashAdvanceLinesGridOption.dataSource._aggregateResult ||
                !_cashAdvanceLinesGridOption.dataSource._aggregateResult.NetAmount ||
                !_cashAdvanceLinesGridOption.dataSource._aggregateResult.NetAmount.sum) {
               // $scope.form.Budget = 0;
                return $scope.form.Budget;
            }
            var total = _cashAdvanceLinesGridOption.dataSource._aggregateResult.NetAmount.sum;
            //$scope.form.Budget = total;
            return total;
        }

        //Set Budget
        $scope.calNetAmountNoTaxVat = function (e) {
            var unitValue = e.model.Unit;
            if (e.values.Unit || e.values.Unit === 0) {
                unitValue = e.values.Unit;
            }
            //
            var amountValue = e.model.Amount;
            if (e.values.Amount || e.values.Amount === 0) {
                amountValue = e.values.Amount;
            }
            //
            var rawSumAmountValue = unitValue * amountValue;
            e.model.NetAmountNoVatTax = rawSumAmountValue;
            return e.model;
        }

        //NetAmountNoVatTax
        $scope.sumNetAmountNoVatTax = function () {
            if (!_cashAdvanceLinesGridOption ||
                !_cashAdvanceLinesGridOption.dataSource ||
                !_cashAdvanceLinesGridOption.dataSource._aggregateResult ||
                !_cashAdvanceLinesGridOption.dataSource._aggregateResult.NetAmountNoVatTax ||
                !_cashAdvanceLinesGridOption.dataSource._aggregateResult.NetAmountNoVatTax.sum) {
                $scope.form.Budget= 0;
                return 0; //$scope.form.BudgetDetail;
            }
            var total = _cashAdvanceLinesGridOption.dataSource._aggregateResult.NetAmountNoVatTax.sum;
            $scope.form.Budget = total.toFixed(2);
            return total;
        }

        //changeAPCode function
        $scope.changeAPCode = function () {
            //console.log("5");
        }

        //Unitcode Dropdown OnChange Function
        $scope.unitCodeOptionChange = function () {
            //var dataItem = this.dataItem(data.item);
            checkFinalApproverButton = false;
            if ($scope.form.UnitCode !== "") {
                $scope.checkedUnitCode = true;
                if ($scope.form.ProposalRef) {
                    $scope.form.ProposalRef = "";                  
                    $scope.form.ExpenseTopics = [];
                    $scope.form.APCodes = [];
                    $scope.form.BudgetPlan = null;
                    $scope.TotalBudgetPlan = null;
                    $scope.temp.listPlace = [];
                    $scope.expenseCheck = false;
                    $scope.expenseTopicDropDownList.value('');
                    $scope.proposalNumberOption2.gridOptions.dataSource.data([]);
                    //$scope.expenseTopicOption.dataSource.data(new kendo.data.ObservableArray($scope.form.ExpenseTopics));
                    //$scope.apCodeOption.dataSource.data(new kendo.data.ObservableArray($scope.form.APCodes));
                    $scope.branchOptions.dataSource.data(new kendo.data.ObservableArray($scope.temp.listPlace));
                    proposalLineId = 0;
                }
            } else {
                $scope.checkedUnitCode = false;
                $scope.expenseCheck = false;
                $scope.form.BudgetPlan = null;
                $scope.TotalBudgetPlan = null;
            }
        }

        $scope.showComments = false;
        if ($routeParams.mode.toUpperCase() !== 'CREATE') {
            $scope.showComments = true;
        }

        //Test
        $scope.A = new kendo.data.ObservableArray([]);
        $scope.TestGrid = lineGrid.gridOption({
            schema : {
                    model: {
                        fields : {
                            id: "LineNo",
                            ApproverEmail: {
                                type: "number"
                            }
                        }
                    }
                },
            columns: [
                //{
                //    command: [
                //        {
                //            name: "edit",
                //            text: { edit: "Custom edit", cancel: "Custom cancel", update: "Custom update" }
                //        }
                //    ]
                //},
                { field: "ApproverEmail", title: "Email", width: "120px"}
            ],
                data: $scope.A,
     
            editable: {
                mode: "inline"
            },
            toolbar: [
                {
                    name: "final",
                    template: '<a class="k-button btn-default" ng-click="getData()"><i class="fa fa-plus"></i> ตรวจสอบผู้อนุมัติขั้นสุดท้าย</a>'
                },
                { name: "create" }
            ]
        });

        $scope.getData = function () {
            $scope.TestGrid.dataSource.data = [];
            $scope.A.empty();
            dataSource.finalApprove({ budget: $scope.form.Budget }, function(data) {
                $scope.A.push(data);
            });
        }

        $scope.printReport = function () {
            if ($routeParams.id != undefined) {
                var params = {
                    Id: $routeParams.id,
                    inline: true,
                    //show: true,
                    fullscreen: true
                }
                var url = angular.crs.url.rootweb("report/CashAdvance/?params=" + JSON.stringify(params));
                return url;
            }
        }

        $scope.printReportMemoCashAdvance = function () {
            if ($routeParams.id != undefined) {
                var params = {
                    Id: $routeParams.id,
                    inline: true,
                    //show: true,
                    fullscreen: true
                }
                var url = angular.crs.url.rootweb("report/MemoCashAdvance/?params=" + JSON.stringify(params));
                return url;
            }
        }

        //Search by Email 
        $scope.EmailOption = approverGrids.emailOption;

        // $scope.TestGrid.dataSource.data = $scope.A;
        //Test/////////////////////////////////

        //Branch to accounting
        $scope.BranchToAccounting = function(branch) {
            $scope.form.Accounting = branch;
        }
    }
]);

//Approval////////////////////////
//$scope.schemaApprove = function (level) {
//    return {
//        model: kendo.data.Model.define({
//            id: "LineNo",
//            fields: {
//                LineNo: { type: "number" },
//                ApproverSequence: { type: "number", validation: { min: 1 } },
//                ApproverLevel: { type: "string", defaultValue: level },
//                DueDate: { type: "date" },
//                Position: {
//                    type: "string",
//                },
//                ApproverEmail: {
//                    type: "string",
//                },
//                Department: {
//                    type: "string",
//                },
//            }
//        })
//    }
//}

//$scope.GenerateAutoSequenceNo = function (level) {
//    var approvalForLevel = $scope.form.CashAdvanceApproval.filter(function (data) {
//        return data.ApproverLevel == level && data.Deleted !== true;
//    });
//    var sequenceNo = Math.max.apply(Math, approvalForLevel.map(function (item) { return item.ApproverSequence; }));
//    return sequenceNo + 1;
//}

//$scope.ApproverGridOption = function (key, level) {
//    _approvalGridOption[key] = lineGrid.gridOption({
//        schema: $scope.schemaApprove(level),
//        data: $scope.form.CashAdvanceApproval,
//        sort: { field: "ApproverSequence", dir: "asc" },
//        filter: [
//         {
//             logic: "and",
//             filters: [
//                 {
//                     logic: "or",
//                     filters: [
//                         { field: "ApproverLevel", operator: "eq", value: level },
//                         { field: "ApproverLevel", operator: "eq", value: null },
//                     ]
//                 },
//                 {
//                     logic: "or",
//                     filters: [
//                         { field: "Deleted", operator: "eq", value: false },
//                         { field: "Deleted", operator: "eq", value: null }
//                     ]
//                 }
//             ],

//         }
//        ],
//        columns: [
//            { field: "ApproverSequence", title: "Sequence", width: "100px", },
//            {
//                field: "Employee", title: "Name", width: "200px",
//                template: "<div style='width:100%; word-wrap: break-word;'>" +
//                    $scope.requesterOption().inlineTemplate('Employee')+"</div>",
//                editor: function (container, options) {
//                    var editor = $(' <div kendo-popup-search k-ng-model="form.' + options.field + '" k-options="requesterOption()" name="Employee"></div>');
//                    editor.attr("data-bind", "value:Employee,source:null");
//                    editor.find('a.popup-search-placeholder-wrapper.form-control').attr('style', 'width:90%; height:20px;');
//                    editor.find('a.popup-search-target.form-control').attr('style', 'width:90%; height:20px;');
//                    editor.attr("style", "width:90%; height:100%; word-wrap: break-word;");
//                    editor.appendTo(container);
//                    return editor;
//                }
//            },
//            { field: "ApproverEmail", title: "Email", width: "150px", },
//            { field: "Position", title: "Position", width: "100px", },
//            { field: "Department", title: "Department", width: "100px", },
//            { field: "DueDate", title: "Due date", width: "100px", template: lineGrid.formatDateTime('DueDate') },
//        ],
//        readonly: $scope.isReadonlyDetails(),
//        editable: {
//            mode: "inline"
//        },
//        edit: function (e) {
//            if (e.model.isNew()) {
//                e.model.set("ApproverSequence", $scope.GenerateAutoSequenceNo(level));
//            }
//           // console.log(e);
//            //var line = e.sender.dataSource ? e.sender.dataSource.data() : false;
//            //if (line == undefined || line.length <= 0)
//            //    e.model.set("ApproverSequence", 1);
//            //else
//            //    e.model.set("ApproverSequence", line.length);
//        },
//        height: 300,
//        //noedit: true,
//        save: function (e) {
//            var check = $scope.form.CashAdvanceApproval.filter(function (data) {
//                return data.ApproverLevel == level && data.Deleted !== true && data.ApproverUserName == e.model.Employee.Username;
//            });
//            if (check.length === 0) {
//                e.model.set("ApproverUserName", e.model.Employee.Username);
//                e.model.set("ApproverEmail", e.model.Employee.Email);
//                e.model.set("Position", e.model.Employee.PositionName);
//                e.model.set("Department", e.model.Employee.DepartmentName);
//            } else {
//                e.model.set("Deleted", true);
//                messageBox.info("ชื่อซ้ำ");
//            }
//        },
//        nolock: true,
//        pageable: false,
//        dataBinding: function () {
//            console.log((this.dataSource.page() - 1) * this.dataSource.pageSize());
//        }
//    });
//    return _approvalGridOption[key];
//}

//Get final Approver อาจจะไม่ได้ใช้
//$scope.getFinalApprover = function () {
//    //debugger;
//    if (!_cashAdvanceLinesGridOption ||
//        !_cashAdvanceLinesGridOption.dataSource ||
//        !_cashAdvanceLinesGridOption.dataSource._aggregateResult ||
//        !_cashAdvanceLinesGridOption.dataSource._aggregateResult.NetAmount ||
//        !_cashAdvanceLinesGridOption.dataSource._aggregateResult.NetAmount.sum) {
//        $scope.form.Budget = 0;
//    } else {
//        var total = _cashAdvanceLinesGridOption.dataSource._aggregateResult.NetAmount.sum;
//        $scope.form.Budget = total;
//    }

//    dataSource.finalApprove({ budget: $scope.form.Budget }, function (data) {
//        $scope.ApproverGridOptionLevelFourData = new kendo.data.ObservableArray([data]);
//        $scope.ApproverGridOptionLevelFour.dataSource.data($scope.ApproverGridOptionLevelFourData);
//        //_approvalGridOption[3].dataSource.data([]);
//        //   _approvalGridOption[3].des
//        //                var approverFinal = {
//        //                    Employee: data.Employee,
//        //                    ApproverSequence: data.ApproverSequence,
//        //                    ApproverLevel: data.ApproverLevel,
//        //                    ApproverUserName: data.ApproverUserName,
//        //                    ApproverEmail: data.ApproverEmail,
//        //                    Position: data.Position,
//        //                    Department: data.Department,
//        //                    DueDate: data.DueDate,
//        //                    LineNo: data.LineNo
//        //                };
//        //               //$scope.form.CashAdvanceApproval.push(approverFinal);
//        //               //// console.log($scope.form.CashAdvanceApproval);
//        //               // _approvalGridOption[3].dataSource.data([approverFinal]);
//        //               // $scope.form.CashAdvanceApproval.push(approverFinal);
//        //               // console.log(_approvalGridOption[3].dataSource.options.data([]));

//        //               // /*var finalApprover = $scope.form.CashAdvanceApproval.filter(
//        //               //     function(data) {
//        //               //         return data.ApproverLevel === "อนุมัติขั้นสุดท้าย" && data.Deleted !== true;
//        //               //     });*/
//        //               // $scope.form.CashAdvanceApproval.push(data);
//        //               // _approvalGridOption[3].dataSource.data(new kendo.data.ObservableArray([$scope.form.CashAdvanceApproval]));
//        //                var count = 0;
//        //                var finalApprover = $scope.form.CashAdvanceApproval.filter(
//        //                    function (data2) 
//        //                    {
//        //                        return data2.ApproverLevel === "อนุมัติขั้นสุดท้าย" && data2.Deleted !== true;
//        //                    });
//        //                if (finalApprover && finalApprover.length !== 0) {
//        //                    angular.forEach(finalApprover, function (value, key) {
//        //                        console.log(data.ApproverUserName);
//        //                        console.log(value.ApproverUserName);
//        //                        if (data.ApproverUserName === value.ApproverUserName) {
//        //                            count++;
//        //                            console.log(count);
//        //                        }
//        //                    });
//        //                    if (count === 0) {
//        //                        var notFianlApprovers = $scope.form.CashAdvanceApproval.filter(
//        //                            function (data2) {
//        //                                return data2.ApproverLevel !== "อนุมัติขั้นสุดท้าย" && data2.Deleted !== true;
//        //                            });
//        //                        //_approvalGridOption[3].dataSource.data([]);
//        //                        //$scope.form.CashAdvanceApproval.push(approverFinal);
//        //                        //var finalApproverData = new kendo.data.ObservableArray(approverFinal);
//        //                        console.log(":)z");

//        //                        $scope.form.CashAdvanceApproval = notFianlApprovers;
//        //                        $scope.form.CashAdvanceApproval.push(approverFinal);

//        //                        $scope.ApproverGridOption = function (key, level) {
//        //                            //debugger;
//        //                            _approvalGridOption[key] = approval.ApproverGridOption(level, $scope.form.CashAdvanceApproval, $scope.isReadonlyDetails());
//        //                            return _approvalGridOption[key];
//        //                        }
//        //                        for (var j = 0; j < $scope.ApproverLevel.length; j++) {
//        //                            setApprovalGridDataSource(j, $scope.ApproverLevel[j], $scope.form.CashAdvanceApproval);
//        //                        }
//        //                        //console.log($scope.form.CashAdvanceApproval);

//        ////                        _approvalGridOption[3].dataSource.data(new kendo.data.ObservableArray([approverFinal]));
//        //                    }
//        //                } else {
//        //                    console.log(":P");            
//        //                    $scope.form.CashAdvanceApproval.push(approverFinal);
//        //                }
//    });


//    //var setCashAdvanceLinesGridOptionGrid = function (d) {
//    //    $scope.form.CashAdvanceLines = new kendo.data.ObservableArray(d);
//    //    //console.log($scope.form.PettyCashLines);
//    //    if (_cashAdvanceLinesGridOption) {
//    //        _cashAdvanceLinesGridOption.dataSource.data($scope.form.CashAdvanceLines);
//    //    }
//    //}
//    //return total;
//}