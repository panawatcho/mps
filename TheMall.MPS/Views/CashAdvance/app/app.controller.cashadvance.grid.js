﻿app = angular.module("app.controllers");
app.factory("CashAdvanceGridService", [
    "lineGrid", "messageBox", '$filter',
    function (lineGrid, messageBox, $filter) {
        var cashAdvanceGridOption = function (kendoData, readonly) {
            var cashAdvanceModel = {
                model: {
                    id: "LineNo",
                    fields: {
                        LineNo: { type: "number" }
                    }
                },
                columns: [
                    { field: 'Description', title: 'Description', width: 120 },
                    { field: 'Unit', title: 'Unit', width: 150 },
                    { field: 'Amount', title: 'Amount', width: 150 },
                    { field: 'Remark', title: 'Remark', width: 200 },
                    { field: 'Attachment', title: 'Attachment', width: 150 },
                    { command: readonly ? [] : ["edit", "destroy"], title: " ", width: 200 }

                ]
            }
            return {
                dataSource: new kendo.data.DataSource({
                    type: "json",
                    data: kendoData,
                    pageSize: 5,
                    serverPaging: true,
                    serverSorting: true,
                    schema: {
                        model: cashAdvanceModel.model
                    }
                }),
                toolbar: readonly ? [] : ["create"],
                defaultValues: function () {
                    return {
                        LineNo: lineGrid.nextLineNo($scope.form.CashAdvanceLines)
                    }
                },

                columns: cashAdvanceModel.columns,
                sortable: true,
                readonly: readonly,
                scrollable: true,
                template: function () {
                    return $("#AddCashAdvanceLine").html();
                },
                editable: {
                    mode: "popup",
                    template: $("#AddCashAdvanceLine").html(),
                    window: {
                        "animation": false,
                        "draggable": false
                    }
                },
                edit: function (e) {
                    e.model.set("LineNo", lineGrid.nextLineNo(this.dataSource.data()));
                }
            }
        };

        return {
            CashAdvanceGrid: cashAdvanceGridOption
        }
    }]);
