﻿angular.module('app.controllers').controller('SharedPercentController',
['$scope', 'inquiryGrid', '$routeParams',
function ($scope, inquiryGrid, $routeParams) {
    var _commandTemplateAllstatus = function (data) {

        return '<div class="btn-group" role="group">' +
            '<a class="btn btn-default" style="width:60px" href="' + $routeParams.page + '/Create/' + data.Id + '" title="edit"><i class="fa fa-edit"></i></a>' +
            '</div>';

    }
    $scope.gridKendoOption = inquiryGrid.gridKendoOption(angular.crs.url.odata('sharedpercenttable'));
    // $scope.gridKendoOption.dataSource.sort(inquiryGrid.sortOption);
    $scope.gridKendoOption.columns =
    [
        { field: 'PlaceCode', title: 'Place Code' },
        { field: 'PlaceName', title: 'Place Name' },
        { field: 'Percent', title: 'Percent' },
        { field: 'TR', title: 'TR' },
        { field: 'RE', title: 'RE' },
        {
            template: _commandTemplateAllstatus, width: '110px'
        }


    ];
    $scope.gridKendoOption.toolbar = kendo.template($("#toolbarTemplate").html());


}]);