﻿angular.module('app.controllers').controller('SharedPercentFormController',
[
    '$scope', '$routeParams', 'inquiryGrid', "controlOptions", "sharedPercentDataSource", "k2", "$location", "messageBox",
    function ($scope, $routeParams, inquiryGrid, controlOptions, dataSource, k2, $location, messageBox) {
    	$scope.fromEdit = false;
    	$scope.form = {
    		Allocate: new kendo.data.ObservableArray([])
    	}
    	var id = $routeParams.id;
    	if (id) {
    		dataSource.get({ id: id }).then(function (resp) {
    			$scope.form = resp;
    			$scope.fromEdit = true;

    			$scope.setKendoDataSource($scope.form.Allocate);
    		});
    	} else {

    		$scope.form = {
    			Allocate: new kendo.data.ObservableArray([])
    		};
    	}

    	$scope.validate = function (event) {
    		k2.saveAndSubmit({
    			event: event,
    			preventSubmit: true,
    			validator: $scope.validator,
    			save: dataSource.save,
    			data: $scope.form,
    			success: function (resp) {

    				$scope.setKendoDataSource(resp.Allocate);

    			}
    		});
    	}

    	$scope.placeNameDropdownKendoOption = controlOptions.dropdownKendoOption(angular.crs.url.webApi('place'));
    	$scope.placeNameDropdownKendoOption.dataTextField = "PlaceName";
    	$scope.placeNameDropdownKendoOption.dataValueField = "PlaceCode";
    	//$scope.dropdownKendoOption = controlOptions.dropdownKendoOption('employee');

    	$scope.setKendoDataSource = function (d) {
    		$scope.form.Allocate = new kendo.data.ObservableArray(d);
    		$scope.gridKendoOption.dataSource.data($scope.form.Allocate);

    	}

    	// $scope.gridKendoOption.dataSource.sort(inquiryGrid.sortOption);
    	$scope.gridKendoOption = {
    		dataSource: new kendo.data.DataSource({
    			data: $scope.form.Allocate,
    			schema: {
    				model: {
    					id: "Id",
    					fields: {
    						Id: { type: "number" },
    					}
    				}
    			},

    			batch: false,
    			filter: {
    				logic: "or",
    				filters: [
                        { field: "Deleted", operator: "eq", value: false },
                        { field: "Deleted", operator: "eq", value: null },
    				],

    			}

    		}),
    		editable: {
    			mode: 'inline',
    			createAt: 'bottom'
    		},

    		toolbar: [
                { name: "create", text: "Add Places" }
    		],

    		edit: function (e) {

    			if (!e.model.isNew()) {

    				return;
    			}

    			e.model.set("ID", $scope.form.Id);
    			e.model.set('Process', $scope.form.Allocate[0].Process);


    		},
    		sortable: {
    			mode: "single",
    			allowUnsort: false
    		},
    		columns: [
				{
					command: [
						{
							name: 'pseudo-delete',
							text: '',
							click: function (e) {
            					e.preventDefault();
            					var row = $(e.currentTarget).closest("tr");
            					var dataItem = this.dataItem(row);
            					dataItem.set('Deleted', true);
            					this.$angular_scope.$apply();

							},
							template: '<a class="k-button k-button-icontext k-grid-pseudo-delete" href="\\#"><span class="k-icon k-delete"></span></a>'
						},
						{ name: "edit", text: "" }
						],
					width: 200
				},
				{
        			field: 'place',
        			title: 'PlaceName',
        			template: function (e) {
        				return e.place ? e.place.PlaceName : "";
        			},
        			editor: function (container, options) {
        				var editor = $('<select id="' + options.field + '" kendo-dropdownlist k-options="placeNameDropdownKendoOption" k-ng-model="dataItem.' + options.field + '"></select>');
        				editor.appendTo(container);
        				return editor;
        			}
				},
				{
					field: 'Percent',
					title: 'Percent',
					template: function (e) {
						debugger;
						return e;
					},
					editor: function (container, options) {
						var editor = $('<input kendo-numeric-text-box k-min="0" k-max="100" ng-model="' + options.field + '" style="width: 100%;" />');
						editor.appendTo(container);
						return editor;
					}
				},
				{
					field: 'TR',
					title: 'TR',
					template: function (e) {
						return e;
					}
				},
				{
					field: 'RE',
					title: 'RE',
					template: function (e) {
						return e;
					}
				}
    		]

    	};

    	//$scope.gridKendoOption.toolbar = kendo.template($("#toolbarTemplate").html());

    }]);