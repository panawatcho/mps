﻿angular.module('app.controllers').controller('receiptController',
[
    '$scope', '$routeParams', 'inquiryGrid', 'authService', 'appUrl',
    'reportService', 'receiptDataSource', '$window', 'messageBox', '$location', 'sweetAlert',
    function ($scope, $routeParams, inquiryGrid, authService, appUrl,
        reportService, receiptDataSource, $window, messageBox, $location, sweetAlert) {

        var commandTemplateAllstatus = function (data) {
            if (data.CreatedBy && data.CreatedBy.toUpperCase() === CURRENT_USERNAME.toUpperCase()) {
                if (data.StatusFlag === null || data.StatusFlag === 1 && data.Status === "Active") {
                    return '<div class="btn-group" role="group">' +
                    '<a class="btn btn-default" style=" margin-right: 5px;" href="' + $routeParams.page + '/detail/' + data.Id + '" title="view"><i class="fa fa-file-text"></i></a>' +
                    '<a class="btn btn-default" style=" margin-right: 5px;" href="' + $routeParams.page + '/create/' + data.Id + '" title="edit"><i class="fa fa-edit"></i></a>' +
                    '<a class="btn btn-info" disabled="disabled" style=" margin-right: 5px; margin-bottom: 10px"><span class="fa fa-print"></span></a>' +
                    '<a class="btn btn-default"  style=" background-color:#dd4b39;" ng-click="delete(' + data.Id + ',' + data.StatusFlag + ')" title="Delete"><i class="fa fa-trash-o" style="color:white;"></i></a>' +
                    '</div>';
                } else {
                    var htmlButtonV1 = '<div class="btn-group" role="group">' +
                    '<a class="btn btn-default" style=" margin-right: 5px;" href="' + $routeParams.page + '/detail/' + data.Id + '" title="view"><i class="fa fa-file-text"></i></a>' +
                    '<span class="btn btn-default" disabled="disabled" style="margin-right: 5px;" title="edit"><i class="fa fa-edit"></i></span></a>';
                    if (data.StatusFlag === 9 && data.Status === "Completed") {
                        htmlButtonV1 = htmlButtonV1 + '<a class="btn btn-info" style="  margin-right: 5px; margin-bottom: 10px" ng-href="{{printReport(' + data.Id + ')}}" target="_blank"><span class="fa fa-print"></span></a>' +
                        '<span class="btn btn-default" disabled="disabled" style="margin-right: 5px; background-color:#dd4b39" title="Delete"><i class="fa fa-trash-o" style="color:white"></i></span>' +
                        '</div>';
                    } else {
                        htmlButtonV1 = htmlButtonV1 + '<a class="btn btn-info" disabled="disabled" style=" margin-right: 5px; margin-bottom: 10px"><span class="fa fa-print"></span></a>' +
                        '<span class="btn btn-default" disabled="disabled" style="margin-right: 5px; background-color:#dd4b39" title="Delete"><i class="fa fa-trash-o" style="color:white"></i></span>' +
                        '</div>';
                    }
                    return htmlButtonV1;
                }
            } else {
                return '<div class="btn-group" role="group">' +
                '<span class="btn btn-default" disabled="disabled" style="margin-right: 5px;" title="edit"><i class="fa fa-file-text"></i></span>' +
                '<span class="btn btn-default" disabled="disabled" style=" margin-right: 5px;" title="edit"><i class="fa fa-edit"></i></span>' +
                '<a class="btn btn-info" disabled="disabled" style=" margin-right: 5px; margin-bottom: 10px"><span class="fa fa-print"></span></a>' +
                '<span class="btn btn-default" disabled="disabled" style="margin-right: 5px; background-color:#dd4b39" title="Delete"><i class="fa fa-trash-o" style="color:white"></i></span>' +
                '</div>';
            }
        }

        var valueFalse = false;
        var params = {
            filters:
               [{
                    field: "CreatedBy",
                    operator: "eq",
                    value: CURRENT_USERNAME
               },
               {
                   field: "Deleted",
                   operator: "eq",
                   value: false
               }
            ],
            //height: 'auto',
            height: '450px',
            pageSize: 7,
            scrollable: false,
            sorts: inquiryGrid.sortOption,
            fields: {
                BudgetDetail: { type: "number" },
            }
        };

        // get data from DB
        $scope.gridKendoOption = inquiryGrid.gridKendoOption(angular.crs.url.odata('receipttable'), params);
    
        // index form
        $scope.gridKendoOption.columns =
            [
                //// Button View Edit Print Delete
                //inquiryGrid.commandColumn,
                { template: commandTemplateAllstatus, width: '110px' },
                { field: 'Status', title: 'Status', width: '120px' },
                { field: 'DocumentNumber', title: 'Receipt number', width: '150px' },
                { field: 'RequesterForName', title: 'RequesterFor', width: '180px' },
                { field: 'BudgetDetail', title: 'Budget', width: '130px', template: inquiryGrid.formatCurrency('BudgetDetail') },
                inquiryGrid.createdDateColumn
            ];
        // send toolbar template from view
        $scope.gridKendoOption.toolbar = kendo.template($("#receiptToolbarTemplate").html());

        $scope.printReport = function (receiptId) {
            if (receiptId) {
                var params = {
                    Id: receiptId,
                    inline: true,
                    //show: true,
                    fullscreen: true
                }
                var url = angular.crs.url.rootweb("report/Receipt/?params=" + JSON.stringify(params));
                return url;
            }
        }

        $scope.delete = function (id, statusFlag) {
                sweetAlert.swal({
                    title: 'Are you sure?',
                    text: "Delete this record ?",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        var result = receiptDataSource.deleteBy({ id: id, username: CURRENT_USERNAME },
                            function (data) {
                                sweetAlert.success("Delete data succesful");
                                $location.path('/receipt');
                            }, sweetAlert.error("You can't delete data!"));
                    }
                });
            }
    }]);
