﻿angular.module('app.controllers').controller('receiptFormController',
    ['$scope', '$routeParams', 'masterGrid', 'messageBox', 'lineGrid', '$location', '$http', 'controlOptions', 'receiptDataSource', 'employeeDataSource', 'authService', 'approverGrids',
    function($scope, $routeParams, masterGrid, messageBox, lineGrid, $location, $http, controlOptions, dataSource, employeeDataSource, authService, approverGrids) {

        var _receiptLinesGridOption,
            id = 0,
            remoteResult;

        $scope.TotalBudgetPlan = 0;

        $scope.form = {
            ReceiptLines: new kendo.data.ObservableArray([]),
            Requester: [],
        };

        var setreceiptLinesGridOptionGrid = function(d) {
            $scope.form.ReceiptLines = new kendo.data.ObservableArray(d);
            if (_receiptLinesGridOption) {
                _receiptLinesGridOption.dataSource.data($scope.form.ReceiptLines);
            }
        }

        // data requester
        $scope.form.Requester = angular.copy(authService.authentication.me);
        // data requesterfor
        $scope.requesterOption = controlOptions.popupSearch('employee');
        $scope.requesterForOption = controlOptions.popupSearch('employeefor');

        //// unitcode all
        // $scope.unitcodeOption = controlOptions.dropdown('unitcode');

        // unitcode requesterfor
        $scope.unitcodeOption = {
            dataSource: new kendo.data.DataSource({
                type: "json",
                data: new kendo.data.ObservableArray([])
            }),
            optionLabel: "please select",
            dataTextField: 'UnitName',
            dataValueField: 'UnitCode',
            template: '#=data.UnitCode# - #=data.UnitName#',
            valueTemplate: '#=data.UnitCode# - #=data.UnitName#',
            filter: "contains"
        }

        // unitcode requester
        $scope.unitcodeOptionNew = {
            dataSource: {
                type: "json",
                data: (angular.copy(authService.authentication.me))
                    ? angular.copy(authService.authentication.me).UnitCode
                    : []
            },
            optionLabel: "please select",
            dataTextField: 'UnitName',
            dataValueField: 'UnitCode',
            template: '#=data.UnitCode# - #=data.UnitName#',
            valueTemplate: '#=data.UnitCode# - #=data.UnitName#',
            filter: "contains"
        }
        if ((angular.copy(authService.authentication.me)) &&
            angular.copy(authService.authentication.me).UnitCode.length === 1) {
            $scope.form.UnitCode = angular.copy(authService.authentication.me).UnitCode[0];
        }

        //
        $scope.$watch('form.Requesterfor',
            function () {
                if ($scope.form.Requesterfor) {
                    if ($scope.form.Requesterfor.UnitCode) {
                        $scope.unitcodeOption.dataSource.data($scope.form.Requesterfor.UnitCode);
                        if (!$scope.form.UnitCodeFor) {
                            if ($scope.form.Requesterfor.UnitCode.length === 1) {
                                $scope.form.UnitCodeFor = ($scope.form.UnitCodeFor)
                                    ? $scope.form.UnitCodeFor
                                    : $scope.form.Requesterfor.UnitCode[0];
                            }
                        } else {
                            $scope.form.UnitCodeFor = $scope.form.UnitCodeFor;
                        }
                    }
                } else {
                    $scope.unitcodeOption.dataSource.data(new kendo.data.ObservableArray([]));
                    $scope.form.Requesterfor = "";
                    $scope.form.UnitCodeFor = "";
                }
            });


        // show receipt detail
        if ($routeParams.page.toUpperCase() === "RECEIPT" && $routeParams.mode.toUpperCase() === "CREATE") {
            $scope.form.Receipt = false;
        }

        //
        $scope.isReadonlyDetails = function() {
            if ($routeParams.mode.toUpperCase() !== "CREATE" && $routeParams.mode.toUpperCase() !== "REVISE") {
                //$scope.disabled = true;
                //$($("#texteditor").data().kendoEditor.body).attr('contenteditable', false);
                return true;
            }
            //$scope.disabled = false;
            return false;
        }

        // Line Detail
        var receiptLineSchema = {
            model: kendo.data.Model.define({
                id: "LineNo",
                fields: {
                    LineNo: { type: "number" },
                    Description: { type: "string", validation: { required: true } },
                    Unit: { type: "number", validation: { min: 0, required: true } },
                    Amount: { type: "number", validation: { min: 0, required: true } },
                    Tax: { type: "number", validation: { min: 0, required: true } },
                    Vat: { type: "number", validation: { min: 0, required: true } },
                    NetAmount: { type: "number", editable: false },
                    NetAmountNoVatTax: { type: "number", validation: { min: 0 } }
                }
            })
        };

        // LinesGridOption
        $scope.ReceiptLinesGridOption = function () {
            _receiptLinesGridOption = lineGrid.gridOption({
                schema: receiptLineSchema,
                data: $scope.form.ReceiptLines,
                aggregate: [
                    { field: "Amount", aggregate: "sum" },
                    { field: "NetAmount", aggregate: "sum" },
                    { field: "NetAmountNoVatTax", aggregate: "sum" }
                ],
                columns: [
                    {
                        field: "Description",
                        title: "Description",
                        width: "150px",
                        template: function(data) {
                            if (data.Description !== undefined) {
                                return "<div style='width:100%; word-wrap: break-word;'>" + data.Description + "</div>";
                            }
                            return "";
                        },
                        footerTemplate: "Total"
                    },
                    { field: "Unit", title: "Unit", width: "90px" },
                    {
                        field: "Amount",
                        title: "Price/Unit",
                        width: "90px",
                        template: lineGrid.formatCurrency('Amount')
                        ////footerTemplate: "<div class='text-right'>#:(sum)? kendo.toString(sum,'n2'):0.00#</div>"
                        //footerTemplate: "<div class='col-xs-6 col-sm-6 col-md-3 col-lg-3'>Total</div>" +
                        //                "<div class='text-right'>{{sumAmount() | currency:' ':2}}</div>"
                        ////footerTemplate: "<div class='text-right'>{{sumAmount() | currency:' ':2}}</div>"
                    },
                    {
                        field: "Vat",
                        title: "VAT %",
                        width: "90px",
                        editor: function(container, options) {
                            $scope.vatValue = approverGrids.vatOption;
                            var editor =
                                $(
                                    '<select kendo-drop-down-list name="Vat" k-options="vatValue" style="width: 100%"></select>');
                            editor.attr("data-bind", "value:Vat,source:null");
                            editor.attr("style", "width:100%; word-wrap: break-word;");
                            editor.appendTo(container);
                            return editor;
                        }
                    },
                    {
                        field: "Tax",
                        title: "TAX %",
                        width: "90px",
                        editor: function(container, options) {
                            $scope.taxValue = approverGrids.taxOption;
                            var editor =
                                $(
                                    '<select kendo-drop-down-list name="Tax" k-options="taxValue" style="width: 100%"></select>');
                            editor.attr("data-bind", "value:Tax,source:null");
                            editor.attr("style", "width:100%; word-wrap: break-word;");
                            editor.appendTo(container);
                            return editor;
                        }
                    },
                    {
                        field: "NetAmount",
                        title: "Net amount",
                        width: "90px",
                        template: lineGrid.formatCurrency('NetAmount'),
                        footerTemplate: //"<div class='col-xs-6 col-sm-6 col-md-3 col-lg-3'>Total</div>" +
                            "<div class='text-right'>{{sumAmount() | currency:' ':2}}</div>"
                        //"<div class='text-right'>#:(sum)? kendo.toString(sum,'n2'):0.00#</div>"
                    },
                    {
                        field: "Remark",
                        title: "Remark",
                        width: "150px",
                        template: function(data) {
                            if (data.Remark !== undefined) {
                                return "<div style='width:100%; word-wrap: break-word;'>" + data.Remark + "</div>";
                            }
                            return "";
                        }
                    },
                    {
                        field: "NetAmountNoVatTax",
                        title: "NetAmountNoVatTax",
                        width: "150px",
                        template: lineGrid.formatCurrency('NetAmountNoVatTax'),
                        //aggregates: ["sum"],
                        //footerTemplate: "<div class='text-right'>#:(sum)? kendo.toString(sum,'n2'):0.00#</div>",
                        footerTemplate: "<div class='text-right'>{{sumNetAmountNoVatTax() | currency:' ':2}}</div>",
                        hidden: true
                    }
                ],
                editable: {
                    mode: "incell"
                },
                readonly: $scope.isReadonlyDetails(),
                noedit: true,
                //template: $('#_IncomeDeposit').html(),
                height: "400px",
                //edit : function (e) {
                //       debugger;
                // },
                save: function(e) {
                    $scope.setNetAmount(e);
                    $scope.calNetAmountNoTaxVat(e);
                    this.saveChanges();
                },
                pageable: false,
                groupable: true
            });

            return _receiptLinesGridOption;
        }

        // NetAmountNoVatTax
        $scope.sumNetAmountNoVatTax = function() {
            if (!_receiptLinesGridOption ||
                !_receiptLinesGridOption.dataSource ||
                !_receiptLinesGridOption.dataSource._aggregateResult ||
                !_receiptLinesGridOption.dataSource._aggregateResult.NetAmountNoVatTax ||
                !_receiptLinesGridOption.dataSource._aggregateResult.NetAmountNoVatTax.sum) {
                $scope.form.BudgetNoVatTax = 0;
                return 0; //$scope.form.BudgetDetail;
            }
            var total = _receiptLinesGridOption.dataSource._aggregateResult.NetAmountNoVatTax.sum;
            $scope.form.BudgetNoVatTax = total;
            return total;
        }

        // Set Budget
        $scope.calNetAmountNoTaxVat = function(e) {
            var unitValue = e.model.Unit;
            if (e.values.Unit || e.values.Unit === 0) {
                unitValue = e.values.Unit;
            }
            //
            var amountValue = e.model.Amount;
            if (e.values.Amount || e.values.Amount === 0) {
                amountValue = e.values.Amount;
            }
            //
            var rawSumAmountValue = unitValue * amountValue;
            e.model.NetAmountNoVatTax = rawSumAmountValue;
            return e.model;
        }

        // Chek Budget function
        $scope.sumAmount = function() {
            if (!_receiptLinesGridOption ||
                !_receiptLinesGridOption.dataSource ||
                !_receiptLinesGridOption.dataSource._aggregateResult ||
                !_receiptLinesGridOption.dataSource._aggregateResult.NetAmount ||
                !_receiptLinesGridOption.dataSource._aggregateResult.NetAmount.sum) {
                $scope.form.BudgetDetail = 0;
                return $scope.form.BudgetDetail;
            }
            var total = _receiptLinesGridOption.dataSource._aggregateResult.NetAmount.sum;
            $scope.form.BudgetDetail = total;
            return total;
        }

        //Check NetAMount
        $scope.setNetAmount = function(e) {
            var unitValue = e.model.Unit;
            if (e.values.Unit || e.values.Unit === 0) {
                unitValue = e.values.Unit;
            }
            //
            var vatValue = e.model.Vat;
            if (e.values.Vat || e.values.Vat === 0) {
                vatValue = e.values.Vat;
            }
            //
            var taxValue = e.model.Tax;
            if (e.values.Tax || e.values.Tax === 0) {
                taxValue = e.values.Tax;
            }
            //
            var amountValue = e.model.Amount;
            if (e.values.Amount || e.values.Amount === 0) {
                amountValue = e.values.Amount;
            }
            var rawSumAmountValue = unitValue * amountValue;
            var vat;
            var amountWithVatAndTax;
            var tax;
            if (taxValue !== 0 && vatValue !== 0) {
                tax = ((taxValue * rawSumAmountValue) / 100).toFixed(2);
                vat = ((vatValue * rawSumAmountValue) / 100).toFixed(2);
                amountWithVatAndTax = vat - tax;
                e.model.NetAmount = rawSumAmountValue + amountWithVatAndTax;
            } else {
                if (taxValue === 0 && vatValue !== 0) {
                    vat = parseFloat(((vatValue * rawSumAmountValue) / 100).toFixed(2));
                    e.model.NetAmount = rawSumAmountValue + parseFloat(vat);
                } else {
                    if (taxValue !== 0 && vatValue === 0) {
                        tax = parseFloat(((taxValue * rawSumAmountValue) / 100).toFixed(2));
                        var sum = rawSumAmountValue - tax;
                        e.model.NetAmount = sum;
                    } else {
                        if (taxValue === 0 && vatValue === 0) {
                            e.model.NetAmount = rawSumAmountValue;
                        }
                    }
                }
            }
            e.model.NetAmount = parseFloat(Math.round(e.model.NetAmount * 100) / 100).toFixed(2); //
            return e.model;
        }


        if ($routeParams.id != undefined) {
            remoteResult = dataSource.get({ id: $routeParams.id },
                function(data) {
                    $scope.form = data;
                    setreceiptLinesGridOptionGrid(data.ReceiptLines);
                });
        }

        // save
        $scope.validate = function(event) {
            if ($routeParams.id !== undefined) {
                id = $routeParams.id;
                $scope.form.Id = id;
            }

            $scope.form.StatusFlag = 1;

            var checkUnitCode = false;
            if ($scope.form.UnitCode !== undefined &&
                $scope.form.UnitCode.UnitCode !== null &&
                $scope.form.UnitCode.UnitCode !== "") {
                checkUnitCode = true;
            }
            if (checkUnitCode) {
                dataSource.save($scope.form).$promise.then(
                    function(resp) {
                        if (resp) {
                            $scope.form = resp;
                            setreceiptLinesGridOptionGrid(resp.ReceiptLines);
                            messageBox.success('Saved successfully');
                            $location.path('receipt/create/' + $scope.form.Id);
                        }
                    },
                    function(error) {
                        messageBox.extractError(error);
                        //messageBox.error('Saved unsuccessfully');
                    }
                );
            } else {
                messageBox.error('Please select UnitCode!');
            }
        }

        // button complete
        $scope.validateComplete = function() {
            if ($routeParams.id !== undefined) {
                // url/mps/receipt/$routeParams
                id = $routeParams.id;
                $scope.form.Id = id;
            }
            $scope.form.StatusFlag = 9;
            var checkValidate = false;
            if ($scope.form.UnitCode !== undefined &&
                $scope.form.UnitCode.UnitCode) {
                if (($scope.form.Requesterfor && $scope.form.UnitCodeFor.UnitCode) ||
                    (!$scope.form.Requesterfor && !$scope.form.UnitCodeFor)) {
                    if ($scope.form.ReceiptLines.length > 0 &&
                        $scope.form.ReceiptLines.filter(function(data) {
                            return data.Deleted !== true;
                        }).length >
                        0 &&
                        $scope.form.BudgetDetail !== 0) {
                        checkValidate = true;
                    } else {
                        messageBox.error('Budget must not be 0');
                    }
                } else {
                    messageBox.error('Please select UnitCode for!');
                }
            } else {
                messageBox.error('Please select UnitCode!');
            }

            if (checkValidate) {
                dataSource.save($scope.form).$promise.then(
                    function(resp) {
                        if (resp) {
                            $scope.form = resp;
                            setreceiptLinesGridOptionGrid(resp.ReceiptLines);
                            messageBox.success('Saved successfully');
                            $location.path('receipt/detail/' + $scope.form.Id);
                        }
                    },
                    function(error) {
                        messageBox.extractError(error);
                        //messageBox.success('Saved unsuccessfully');
                    }
                );
            }
        }

        // report
        $scope.printReport = function() {
            if ($routeParams.id != undefined) {
                var params = {
                    Id: $routeParams.id,
                    inline: true,
                    //show: true,
                    fullscreen: true
                }
                var url = angular.crs.url.rootweb("report/receipt/?params=" + JSON.stringify(params));
                return url;
            }
        }
    }
]);