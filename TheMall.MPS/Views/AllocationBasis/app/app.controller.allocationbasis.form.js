﻿angular.module('app.controllers').controller('allocationBasisFormController',
[
    '$scope', '$routeParams', 'masterGrid', 'messageBox',
    'lineGrid', '$location',
    '$http', 'controlOptions', 'allocationBasisDataSource',
    function($scope, $routeParams, masterGrid, messageBox,
        lineGrid, $location,
        $http, controlOptions, dataSource) {
        $scope.form = {};

        $scope.checkNew = true;
        if (!$routeParams.Id) {
            $scope.canDeleted = false;
            $scope.edit = false;
            //   messageBox.error("Sequence number and name are invalid!!!");

            //$location.path("unitcodeforemployee/");
        } else {
            $scope.canDeleted = true;
            $scope.edit = true;
            console.log($routeParams.Id);
            dataSource.get({ id: $routeParams.Id}, function (data) {
                $scope.form = data;
                $scope.checkNew = false;
                console.log($scope.form);
            });
        }


        $scope.validate = function (event) {
            if (!$scope.validator.validate()) return false;
            if ($scope.checkNew) {
                dataSource.save($scope.form).$promise.then(
                    function (resp) {
                        if (resp) {
                            messageBox.success('Saved successfully');
                            $location.path('allocationbasis/');
                        }
                    },
                    function (error) {
                        messageBox.extractError(error);
                    }
                );
            } else {
                dataSource.edit($scope.form).$promise.then(
                    function (resp) {
                        if (resp) {
                            messageBox.success('Saved successfully');
                            $location.path('allocationbasis/');
                        }
                    },
                    function (error) {
                        messageBox.extractError(error);
                    }
                );
            }
        }

        $scope.delete = function () {
            var deleted = confirm("Delete this data ?");
            if (deleted) {
                dataSource.deleted($scope.form).$promise.then(
                     function (resp) {
                         if (resp) {
                             messageBox.success("Delete successfully.");
                             $location.path('allocationbasis/');
                         }
                     },
                    function (error) {
                        messageBox.extractError(error);
                    }
                    );
            }
        }
    }
]);