﻿angular.module('app.controllers').controller('allocationBasisController',
[
    '$scope', '$routeParams', 'masterGrid',
    function($scope, $routeParams, masterGrid) {
        var _gridKendoOption;
        var _commandTemplateAllstatus = function (data) {
            return '<div class="btn-group" role="group">' +
                '<a class="btn btn-default" style="width:60px" href="' + $routeParams.page + '/Form?Id=' + data.Id + '" title="edit"><i class="fa fa-edit"></i></a>' +
                '</div>';
        }
        $scope.apiUrl = angular.crs.url.webApi('allocationbasis/get');
        $scope.gridKendoOption = {
            dataSource: new kendo.data.DataSource({
                transport: {
                    read: {
                        async: true,
                        url: $scope.apiUrl,
                        dataType: "json"
                    },
                    parameterMap: function (options, operation) {
                        if (operation !== "read" && options.models) {
                            return {
                                Id: options.models[0].Id,
                                AllocCode: options.models[0].AllocCode,
                                Description: options.models[0].Description,
                                InActive: options.models[0].InActive,
                                MRT: options.models[0].MRT,
                                B5: options.models[0].B5,
                                B7: options.models[0].B7,
                                M02: options.models[0].M02,
                                M03: options.models[0].M03,
                                M05: options.models[0].M05,
                                M06: options.models[0].M06,
                                M07: options.models[0].M07,
                                M08: options.models[0].M08,
                                M09: options.models[0].M09,
                                M10: options.models[0].M10,
                                M11: options.models[0].M11,
                                M12: options.models[0].M12,
                                M13: options.models[0].M13,
                                M14: options.models[0].M14,
                                M15: options.models[0].M15,
                                M16: options.models[0].M16,
                                M17: options.models[0].M17,
                                V1: options.models[0].V1,
                                V2: options.models[0].V2,
                                V3: options.models[0].V3,
                                V5: options.models[0].V5,
                                V9: options.models[0].V9,
                                TR: options.models[0].TR,
                                RE: options.models[0].RE
                            };
                        }
                        return null;
                    }
                },
                pageSize: 10,
                batch: true,
                schema: {
                    model: {
                        id: "Id",
                        fields:
                        {
                            Id: { type: "number" },
                            AllocCode: { editable: false, nullable: false },
                            Description: { editable: false, nullable: false },
                            InActive: { editable: false, nullable: false },
                            MRT: { editable: false, nullable: false },
                            B5: { editable: false, nullable: false },
                            B7: { editable: false, nullable: false },
                            M02: { editable: false, nullable: false },
                            M03: { editable: false, nullable: false },
                            M05: { editable: false, nullable: false },
                            M06: { editable: false, nullable: false },
                            M07: { editable: false, nullable: false },
                            M08: { editable: false, nullable: false },
                            M09: { editable: false, nullable: false },
                            M10: { editable: false, nullable: false },
                            M11: { editable: false, nullable: false },
                            M12: { editable: false, nullable: false },
                            M13: { editable: false, nullable: false },
                            M14: { editable: false, nullable: false },
                            M15: { editable: false, nullable: false },
                            M16: { editable: false, nullable: false },
                            M17: { editable: false, nullable: false },
                            V1: { editable: false, nullable: false },
                            V2: { editable: false, nullable: false },
                            V3: { editable: false, nullable: false },
                            V5: { editable: false, nullable: false },
                            V9: { editable: false, nullable: false }
                        }
                    }
                }
            }),
            toolbar: ["create"],
            columns: [
                 { template: _commandTemplateAllstatus, width: '100px' },
                {
                    field: "AllocCode", title: "Allocation code", width: "100px",
                    template: function (data) {
                        if (data.AllocCode) {
                            return "<div style='width:100%; word-wrap: break-word;'>" + data.AllocCode + "</div>";
                        }
                        return "";
                    }
                },
                {
                    field: "Description", title: "Description", width: "150px",
                    template: function (data) {
                        if (data.Description) {
                            return "<div style='width:100%; word-wrap: break-word;'>" + data.Description + "</div>";
                        }
                        return "";
                    }
                },
                {
                    field: "InActive",
                    title: "Active",
                    width: "70px",
                    template: '<div class="text-center">#if(!data.InActive){# <i class="fa fa-check" style="color:green;"></i>#}else{#<i class="fa fa-times" style="color:red;"></i> #}#</div>'
                },
                {
                    field: "TR",
                    title: "TR",
                    width: "70px",
                    template: '<div class="text-center">#if(data.TR){# <i class="fa fa-check" style="color:green;"></i>#}else{#<i class="fa fa-times" style="color:red;"></i> #}#</div>'
                },
                {
                    field: "RE",
                    title: "RE",
                    width: "70px",
                    template: '<div class="text-center">#if(data.RE){# <i class="fa fa-check" style="color:green;"></i>#}else{#<i class="fa fa-times" style="color:red;"></i> #}#</div>'
                },
                {
                    field: "MRT",
                    title: "MRTL",
                    width: "70px",
                    template: '<div class="text-center">#if(data.MRT){# <i class="fa fa-check" style="color:green;"></i>#}else{#<i class="fa fa-times" style="color:red;"></i> #}#</div>'
                },
                {
                    field: "B5",
                    title: "B5",
                    width: "70px",
                    template: '<div class="text-center">#if(data.B5){# <i class="fa fa-check" style="color:green;"></i>#}else{#<i class="fa fa-times" style="color:red;"></i> #}#</div>'
                },
                {
                    field: "B7",
                    title: "B7",
                    width: "70px",
                    template: '<div class="text-center">#if(data.B7){# <i class="fa fa-check" style="color:green;"></i>#}else{#<i class="fa fa-times" style="color:red;"></i> #}#</div>'
                },
                {
                    field: "M02",
                    title: "M02",
                    width: "70px",
                    template: '<div class="text-center">#if(data.M02){# <i class="fa fa-check" style="color:green;"></i>#}else{#<i class="fa fa-times" style="color:red;"></i> #}#</div>'
                },
                {
                    field: "M03",
                    title: "M03",
                    width: "70px",
                    template: '<div class="text-center">#if(data.M03){# <i class="fa fa-check" style="color:green;"></i>#}else{#<i class="fa fa-times" style="color:red;"></i> #}#</div>'
                },
                {
                    field: "M05",
                    title: "M05",
                    width: "70px",
                    template: '<div class="text-center">#if(data.M05){# <i class="fa fa-check" style="color:green;"></i>#}else{#<i class="fa fa-times" style="color:red;"></i> #}#</div>'
                },
                {
                    field: "M06",
                    title: "M06",
                    width: "70px",
                    template: '<div class="text-center">#if(data.M06){# <i class="fa fa-check" style="color:green;"></i>#}else{#<i class="fa fa-times" style="color:red;"></i> #}#</div>'
                },
                {
                    field: "M07",
                    title: "M07",
                    width: "70px",
                    template: '<div class="text-center">#if(data.M07){# <i class="fa fa-check" style="color:green;"></i>#}else{#<i class="fa fa-times" style="color:red;"></i> #}#</div>'
                },
                {
                    field: "M08",
                    title: "M08",
                    width: "70px",
                    template: '<div class="text-center">#if(data.M08){# <i class="fa fa-check" style="color:green;"></i>#}else{#<i class="fa fa-times" style="color:red;"></i> #}#</div>'
                },
                {
                    field: "M09",
                    title: "M09",
                    width: "70px",
                    template: '<div class="text-center">#if(data.M09){# <i class="fa fa-check" style="color:green;"></i>#}else{#<i class="fa fa-times" style="color:red;"></i> #}#</div>'
                },
                {
                    field: "M10",
                    title: "M10",
                    width: "70px",
                    template: '<div class="text-center">#if(data.M10){# <i class="fa fa-check" style="color:green;"></i>#}else{#<i class="fa fa-times" style="color:red;"></i> #}#</div>'
                },
                {
                    field: "M11",
                    title: "M11",
                    width: "70px",
                    template: '<div class="text-center">#if(data.M11){# <i class="fa fa-check" style="color:green;"></i>#}else{#<i class="fa fa-times" style="color:red;"></i> #}#</div>'
                },
                {
                    field: "M12",
                    title: "M12",
                    width: "70px",
                    template: '<div class="text-center">#if(data.M12){# <i class="fa fa-check" style="color:green;"></i>#}else{#<i class="fa fa-times" style="color:red;"></i> #}#</div>'
                },
                {
                    field: "M13",
                    title: "M13",
                    width: "70px",
                    template: '<div class="text-center">#if(data.M13){# <i class="fa fa-check" style="color:green;"></i>#}else{#<i class="fa fa-times" style="color:red;"></i> #}#</div>'
                },
                {
                    field: "M14",
                    title: "M14",
                    width: "70px",
                    template: '<div class="text-center">#if(data.M14){# <i class="fa fa-check" style="color:green;"></i>#}else{#<i class="fa fa-times" style="color:red;"></i> #}#</div>'
                },
                {
                    field: "M15",
                    title: "M15",
                    width: "70px",
                    template: '<div class="text-center">#if(data.M15){# <i class="fa fa-check" style="color:green;"></i>#}else{#<i class="fa fa-times" style="color:red;"></i> #}#</div>'
                },
                {
                    field: "M16",
                    title: "M16",
                    width: "70px",
                    template: '<div class="text-center">#if(data.M16){# <i class="fa fa-check" style="color:green;"></i>#}else{#<i class="fa fa-times" style="color:red;"></i> #}#</div>'
                },
                {
                    field: "M17",
                    title: "M17",
                    width: "70px",
                    template: '<div class="text-center">#if(data.M17){# <i class="fa fa-check" style="color:green;"></i>#}else{#<i class="fa fa-times" style="color:red;"></i> #}#</div>'
                },
                {
                    field: "V1",
                    title: "V1",
                    width: "70px",
                    template: '<div class="text-center">#if(data.V1){# <i class="fa fa-check" style="color:green;"></i>#}else{#<i class="fa fa-times" style="color:red;"></i> #}#</div>'
                },
                {
                    field: "V2",
                    title: "V2",
                    width: "70px",
                    template: '<div class="text-center">#if(data.V2){# <i class="fa fa-check" style="color:green;"></i>#}else{#<i class="fa fa-times" style="color:red;"></i> #}#</div>'
                },
                {
                    field: "V3",
                    title: "V3",
                    width: "70px",
                    template: '<div class="text-center">#if(data.V3){# <i class="fa fa-check" style="color:green;"></i>#}else{#<i class="fa fa-times" style="color:red;"></i> #}#</div>'
                },
                {
                    field: "V5",
                    title: "MRTP",
                    width: "70px",
                    template: '<div class="text-center">#if(data.V5){# <i class="fa fa-check" style="color:green;"></i>#}else{#<i class="fa fa-times" style="color:red;"></i> #}#</div>'
                },
                {
                    field: "V9",
                    title: "V9",
                    width: "70px",
                    template: '<div class="text-center">#if(data.V9){# <i class="fa fa-check" style="color:green;"></i>#}else{#<i class="fa fa-times" style="color:red;"></i> #}#</div>'
                },
               
            ],
            filterable: true,
            pageable: {
                refresh: true,
                pageSizes: true,
                buttonCount: 5
            }
        };
        $scope.gridKendoOption.toolbar = kendo.template($("#allocationBasisTemplate").html());
    }
]);