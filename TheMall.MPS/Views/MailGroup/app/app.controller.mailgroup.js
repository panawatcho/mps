﻿angular.module('app.controllers').controller('mailGroupController',
[
    '$scope', '$routeParams', 'masterGrid','inquiryGrid',
    function ($scope, $routeParams, masterGrid, inquiryGrid) {
        var _gridKendoOption;
        var _commandTemplateAllstatus = function (data) {
            return '<div class="btn-group" role="group">' +
                '<a class="btn btn-default" style="width:60px" href="' + $routeParams.page + '/Form?GroupCode=' + data.GroupCode + '" title="edit"><i class="fa fa-edit"></i></a>' +
                '</div>';
        }
        $scope.apiUrl = angular.crs.url.webApi('mailgroup');
        $scope.gridKendoOption = {
            dataSource: new kendo.data.DataSource({
                transport: {
                    read: {
                        async: true,
                        url: $scope.apiUrl,
                        dataType: "json"
                    },
                    parameterMap: function(options, operation) {
                        if (operation !== "read" && options.models) {
                            return {
                                GroupCode: options.models[0].GroupCode,
                                Email: options.models[0].Email,
                                InActive: options.models[0].InActive,
                                Order: options.models[0].Order
                            };
                        }
                        return null;
                    }
                },
                pageSize: 10,
                batch: true,
                schema: {
                    model: {
                        fields:
                        {
                            GroupCode: { editable: true, nullable: false },
                            Email: { editable: true, nullable: false },
                            InActive: { editable: true, nullable: false },
                            Order: { editable: true, nullable: false },
                        }
                    }
                }
            }),
            toolbar: ["create"],
            columns:
            [
                {
                    field: "GroupCode",
                    title: "Group code",
                    width: "150px",
                    template: function(data) {
                        if (data.GroupCode) {
                            return '<div style="width:100%; word-wrap: break-word;">' + data.GroupCode + '</div>';
                        } else {
                            return "";
                        }
                    }
                },
                {
                    field: "Email",
                    title: "Email",
                    width: "200px",
                    template: function(data) {
                        if (data.Email) {
                            return '<div style="width:100%; word-wrap: break-word;">' + data.Email + '</div>';
                        } else {
                            return "";
                        }
                    }
                },
                {
                    field: "InActive",
                    title: "Active",
                    width: "100px",
                    template: '<div class="text-center">#if(!data.InActive){# <i class="fa fa-check" style="color:green;"></i>#}else{#<i class="fa fa-times" style="color:red;"></i> #}#</div>',
                    editor: '<input type="checkbox" name="InActive" data-bind="checked:InActive"></input>'
                },
                { template: _commandTemplateAllstatus, width: '100px' }
            ],
            filterable: true,
            pageable: {
                refresh: true,
                pageSizes: true,
                buttonCount: 5
            }
        };
        $scope.gridKendoOption.toolbar = kendo.template($("#mailGroupTemplate").html());
        //$scope.gridKendoOption = function() {
        //    if (!_gridKendoOption) {
        //        $scope.odataUrl = angular.crs.url.odata('mailgrouptable');
        //        $scope.apiUrl = angular.crs.url.webApi('mailgroup');
        //        _gridKendoOption = masterGrid.gridKendoOption($scope.odataUrl, $scope.apiUrl,
        //        {
        //            parameterMap: function (options, operation) {
        //                if (operation !== "read" && options.models) {

        //                    return {
        //                        GroupCode: options.models[0].GroupCode,
        //                        Email: options.models[0].Email,
        //                        InActive: options.models[0].InActive
        //                    };
        //                }
        //                return null;

        //            },
        //            schema: {
        //                model: {
        //                    id: "GroupCode",
        //                    fields: {
        //                        GroupCode: { editable: true, nullable: false },
        //                        Email: { editable: true, nullable: false },
        //                        InActive: { editable: true, nullable: false }
        //                    }
        //                }
        //            },
        //            toolbar: ["create"],
        //            columns: [
        //                {
        //                    field: "GroupCode", title: "Group code", width: "110px",
        //                    template: function (data) {
        //                        if (data.GroupCode) {
        //                            return '<div style="width:100%; word-wrap: break-word;">' + data.GroupCode + '</div>';
        //                        } else {
        //                            return "";
        //                        }
        //                    },
        //                    editor: function (container, options) {
        //                        var editor = $('<input type="text" name="GroupCode" style="width: 100%">');
        //                        if (options.model.GroupCode) {
        //                            console.log("s");
        //                            editor.attr("data-bind", "value:GroupCode");
        //                            editor.attr("style", "width:100%; word-wrap: break-word;");
        //                            editor.attr("readonly", "readonly");
        //                            editor.appendTo(container);
        //                            return editor;
        //                        }
        //                        else
        //                        {
        //                            console.log("f");
        //                            editor.attr("data-bind", "value:GroupCode");
        //                            editor.attr("style", "width:100%; word-wrap: break-word;");
        //                            editor.appendTo(container);
        //                            return editor;
        //                        }
        //                    }
        //                },
        //                {
        //                    field: "Email",
        //                    title: "Email",
        //                    width: "110px",
        //                    template: function(data) {
        //                        if (data.Email) {
        //                            return '<div style="width:100%; word-wrap: break-word;">' + data.Email + '</div>';
        //                        } else {
        //                            return "";
        //                        }
        //                    }
        //                },
        //                {
        //                    field: "InActive", title: "In active", width: "200px",
        //                    template:'<div class="text-center">#if(data.InActive){# <i class="fa fa-check" style="color:green;"></i>#}else{#<i class="fa fa-times" style="color:red;"></i> #}#</div>', //'<input type="checkbox" #=InActive? "checked=checked" : "" # disabled="disabled"  ></input>',
        //                    editor: '<div class="text-center"><input type="checkbox" name="InActive" data-bind="checked:InActive" style="text-align: center;  margin: auto;"></input></div>'
        //                },
        //                { command: ["edit", "destroy"], title: "&nbsp;", width: 200 }
        //            ],
        //            editable:
        //            {
        //                mode: "inline",
        //                createAt: "top"
        //            }
        //        });
        //    }
        //    return _gridKendoOption;
        //};
    }
]);