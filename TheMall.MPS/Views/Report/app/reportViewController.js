﻿'use strict';

angular.module('app.controllers')
    .controller("ReportViewController",
    [
        "appUrl", "$scope", "$http", "reportService",
        function (appUrl, $scope, $http, reportService) {
            var reportUrl = appUrl.api + "report/report1";
            debugger;
            $scope.download = reportService.download(reportUrl);
            $scope.view = reportService.view(reportUrl);
            $scope.params = {
                format : "XLS"
            };
            $scope.clear = reportService.clear;
            $scope.result = reportService.result;
            $scope.formatOption = reportService.fileTypesOption;
            $scope.fullscreen = reportService.goFullScreen;
        }
    ]
);