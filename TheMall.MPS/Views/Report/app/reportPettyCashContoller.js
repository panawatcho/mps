﻿'use strict';

angular.module('app.controllers')
    .controller("ReportPettyCashController",
    [
        "appUrl", "$scope", "$http", "reportService",
        function (appUrl, $scope, $http, reportService) {
            var reportUrl = appUrl.api + "reportpettycash/pettycash";
            $scope.download = reportService.download(reportUrl);
            $scope.view = reportService.view(reportUrl);
            $scope.params = reportService.initParams();
            $scope.clear = reportService.clear;
            $scope.result = reportService.result;
            $scope.formatOption = reportService.fileTypesOption;
            $scope.fullscreen = reportService.goFullScreen;
        }
    ]
);