﻿'use strict';

angular.module('app.controllers')
    .controller("ReportMemoIncomeController",
    [
        "appUrl", "$scope", "$http", "reportService",
        function (appUrl, $scope, $http, reportService) {
            var reportUrl = appUrl.api + "reportmemoincome/memoincome";
            $scope.download = reportService.download(reportUrl);
            $scope.view = reportService.view(reportUrl);
            $scope.params = reportService.initParams();
            $scope.clear = reportService.clear;
            $scope.result = reportService.result;
            $scope.formatOption = reportService.fileTypesOption;
            $scope.fullscreen = reportService.goFullScreen;
            //$scope.xxx = reportService.xxx(reportUrl
            //$scope.xxx = function () {
            //    debugger;
            //   //var mywindow = window.open('', '_blank');        
            //   //mywindow.document.write('<html><body style="height: 100%; width: 100%; overflow: hidden; margin: 0px; background-color: rgb(82, 86, 89);"><embed width="100%" height="100%" ////name="plugin" id="plugin" src=' + $scope.result.content + ' type="application/pdf"></body></html>');
            //   //mywindow.document.close(); 
            //   //mywindow.focus();
            //   //mywindow.print();
            //   //mywindow.close();

            //    //$scope.params = {
            //    //    format: 'PDF',
            //    //    fullscreen: true
            //    //}

            //    //$scope.view = reportService.view(reportUrl);
            //    //$scope.result = reportService.result;
               
            //    ////var innerContents = document.getElementById(Test).innerHTML;
            //    //var innerContents = $("#Test").html();
            //    //var mywindow = window.open('', '_blank', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
            //    //mywindow.document.open();
            //    //mywindow.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + innerContents + '</body></html>');
            //    //mywindow.document.close();

            //    var mywindow = window.open($scope.result.content, '_blank');
            //    mywindow.print();
            //}
        }]
);
