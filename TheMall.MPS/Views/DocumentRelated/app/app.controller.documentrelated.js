﻿angular.module('app.controllers').controller('documentrelatedController',
    ['$scope', '$routeParams', 'inquiryGrid', 'authService',
    function ($scope, $routeParams, inquiryGrid, authService) {

        var _detailTemplate = function (data) {
            if (data.DocumentType.toUpperCase() === "PROPOSAL" && data.StatusFlag===9)
            {
                return '<div class="btn-group" role="group">' +
                '<a class="btn btn-default" style="margin-right:5px;" href="' + data.DocumentType + '/detail/' + data.Id + '" title="view"><i class="fa fa-file-text"></i></a>' +
                '<a class="btn btn-info"  style="margin-right:5px; margin-bottom: 10px;" ng-href="{{printReport(' + data.Id + ')}}" target="_blank"><span class="fa fa-print"></span></a>' +
                '</div>';
            //} else if (data.DocumentType.toUpperCase() === "CASHADVANCE" && data.StatusFlag === 9 && data.UsernameAccounting.split(";").indexOf(CURRENT_USERNAME) !== -1) {
            //    return '<div class="btn-group" role="group">' +
            //    '<a class="btn btn-default" style="margin-right:5px;" href="' + data.DocumentType + '/detail/' + data.Id + '" title="view"><i class="fa fa-file-text"></i></a>' +
            //    '<a class="btn btn-info"  style="margin-right:5px; margin-bottom: 10px;" ng-href="{{printReportCashadvance(' + data.Id + ')}}" target="_blank"><span class="fa fa-print"></span></a>' +
            //     '</div>';
            }
            return '<div class="btn-group" role="group">' +
            '<a class="btn btn-default" style="margin-right:5px;" ng-href="' + data.DocumentType + '/detail/' + data.Id + '" title="view"><i class="fa fa-file-text"></i></a>' +
            // '<a class="btn btn-info"  style="margin-right:5px; margin-bottom: 10px;" ng-href="{{printReport(' + data.Id + ')}}" target="_blank"><span class="fa fa-print"></span></a>' +
            '</div>';
        }

        var params = {
            // height: '300px',
            //height: 'auto',
            pageSize: 7,
            scrollable: false,
            sorts: { field: 'RowNo', dir: 'asc' },
        }

        var filters = [{ field: "UsernameAccounting", operator: "contains", value: CURRENT_USERNAME },
                { field: "UsernameApprover", operator: "contains", value: CURRENT_USERNAME },
                { field: "InformUsername", operator: "contains", value: CURRENT_USERNAME },
                { field: "CCUsername", operator: "contains", value: CURRENT_USERNAME } ];

        params.filters = { filters: filters, logic: "or" };

        $scope.gridKendoOption = inquiryGrid.gridKendoOption(angular.crs.url.odata('documentrelatedview'), params);

        $scope.gridKendoOption.columns =
            [
               { template: _detailTemplate, width: '117px' },
               { field: 'DocumentType', title: 'Document Type', width: '150px', },
                {
                    field: 'DocumentNumber', title: 'Document Number', width: '150px',
                    template: function (data) {
                        if (data) {
                            if (data.DocumentNumber) {
                                if (data.Revision !== undefined && data.Revision !== 0 && data.Revision !== null) {
                                    return data.DocumentNumber + " (" + data.Revision + ")";
                                } else {
                                    return data.DocumentNumber;
                                }
                            } else {
                                return "";
                            }
                        }
                    }
                },
                {
                    field: 'Title', title: 'Title', width: '150px', 
                    template: function(data) {
                        if (data.Title !== undefined) {
                            return "<div style='width:100%; word-wrap: break-word;'>" + data.Title + "</div>";
                        }
                    }
                },
                { field: 'UnitCode', title: 'UnitCode', width: '150px', },
                { field: 'UnitName', title: 'UnitName', width: '150px', },
                { field: 'Status', title: 'Status', width: '150px', },
                {
                    field: 'RefDocumentNumber', title: 'Ref DocumentNumber', width: '150px',
                    template: function (data) {
                        if (data.RefDocumentID !==null) {

                            if (data.DocumentType === "CashClearing") {
                                return '<a href="cashadvance/detail/' + data.RefDocumentID + '" target="_blank">' + data.RefDocumentNumber + '<a/>';
                            } else {
                                return '<a href="proposal/detail/' + data.RefDocumentID + '" target="_blank">' + data.RefDocumentNumber + '<a/>';
                            }
                        }

                        return "";
                    },
                },    
            ];

        // $scope.gridKendoOption.toolbar = kendo.template($("#ToolbarTemplate").html());
        $scope.printReport = function (proosalId) {
            if (proosalId) {
                var params = {
                    Id: proosalId,
                    inline: true,
                    //show: true,
                    fullscreen: true
                }
                var url = angular.crs.url.rootweb("report/Proposal/?params=" + JSON.stringify(params));
                return url;
            }
        }

        //$scope.printReportCashadvance = function (proosalId) {
        //    if (proosalId) {
        //        var params = {
        //            Id: proosalId,
        //            inline: true,
        //            //show: true,
        //            fullscreen: true
        //        }
        //        var url = angular.crs.url.rootweb("report/CashAdvance/?params=" + JSON.stringify(params));
        //        return url;
        //    }
        //}
    }]);