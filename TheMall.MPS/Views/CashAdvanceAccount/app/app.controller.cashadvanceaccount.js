﻿angular.module('app.controllers').controller('cashadvanceaccountController',
[
    '$scope', '$routeParams', 'inquiryGrid', '$location', '$http', 'IdCashAdvanceService', 'accCashAdvanceDataSource',
    'IdProposalService', 'appUrl', 'lineGrid', '$window', 'messageBox', 'sweetAlert',
    function($scope, $routeParams, inquiryGrid, $location, $http, idCashAdvanceService, accCashAdvanceDataSource,
        idProposalService, appUrl, lineGrid, $window, messageBox, sweetAlert) {

        $scope.printReport = function (advanceId) {
            if (advanceId) {
                var params = {
                    Id: advanceId,
                    inline: true,
                    //show: true,
                    fullscreen: true
                }
                var url = angular.crs.url.rootweb("report/CashAdvance/?params=" + JSON.stringify(params));

                return url;
            }
        }

        $routeParams.page = 'cashadvance';

        var commandTemplateAllstatus = function (data) {
            if (data.StatusFlag === 9) {
                return '<div class="btn-group" role="group">' +
                    '<a class="btn btn-default" style="margin-right: 5px;" href="' + $routeParams.page + '/detail/' + data.Id + '" title="view"><i class="fa fa-file-text"></i></a>' +
                        '<a class="btn btn-info" style="margin-right: 5px; margin-bottom: 10px" ng-href="{{printReport(' + data.Id + ')}}" target="_blank"><span class="fa fa-print"></span></a>' +
                        '</div>';
            }
        }        
        
        idProposalService.set(0);

        var gridKendoOptionFunction = function (kendoData, readonly) {
            var indexOption = {
                model: {
                    id: "LineNo",
                    fields: {
                        LineNo: { type: "number" },
                        CurrentId: { type: "number", defaultValue: 0 },
                        CreatedDate: { type: "date" }
                    }
                },
                columns: [
                    { template: commandTemplateAllstatus, width: '80px' },
                    { field: 'Status', title: 'Status', width: '120px' },
                    {
                        field: 'ProposalRefDocumentNumber', title: 'Proposal Number', width: '150px',
                        template: function (data) {
                            return '<a href="proposal/detail/' + data.ProposalRefID + '" target="_blank">' + data.ProposalRefDocumentNumber + '<a/>';
                        },
                    },
                    { field: 'DocumentNumber', title: 'Advance Number', width: '150px' },              
                    //{
                    //    field: 'Title', title: 'Title', width: '200px',
                    //    template: function (data) {
                    //        if (data.Title !== undefined) {
                    //            return "<div style='width:100%; word-wrap: break-word;'>" + data.Title + "</div>";
                    //        }
                    //        return "";
                    //    }
                    //},
                    {
                        field: 'AdvanceDueDate',
                        title: 'Due date',
                        width: '150px',
                        template: inquiryGrid.formatDate('AdvanceDueDate')
                    },
                    { field: 'ExpenseTopicName', title: 'Expense topic', width: '200px' },
                    { field: 'APCodeName', title: 'A&P code', width: '150px' },
                    { field: 'Budget', title: 'Budget', width: '100px', template: inquiryGrid.formatCurrency('Budget') },

                    inquiryGrid.createdDateColumn
                ]
            }

            return {
                dataSource: new kendo.data.DataSource({
                    type: "json",
                    data: kendoData,
                    sort:{ field: "CreatedDate", dir: "desc" },
                    batch: false,
                    pageSize: 5,
                    schema: {
                        total: function(data) { return kendoData.length; },
                        //data: function (data) { return data.value; },
                        model: kendo.data.Model.define({
                            id: "Id",
                            fields: {
                                Budget: { type: "number" },
                                AdvanceDueDate: { type: "date" },
                                CreatedDate: { type: "date" }
                            }
                        })
                    },
                    error: function(e) {
                    }
                }),
                //toolbar: kendo.template($("#ToolbarTemplateAdvance").html()),
                columns: indexOption.columns,
                readonly: false,
                scrollable: true,
                //edit: function(e) {
                //    e.model.set("LineNo", lineGrid.nextLineNo(this.dataSource.data()));
                //},
                //
                groupable: false,
                pageable: {
                    refresh: true,
                    pageSizes: [5, 10, 20, 50, 100]
                },
                reorderable: false,
                resizable: false,
                selectable: "row",
                sortable: {
                    allowUnsort: true,
                    mode: "single"
                },
                height: 320,
                mobile: false,
                filterable: {
                    extra: true, //do not show extra filters
                    operators: {
                        // redefine the string operators
                        string: {
                            contains: "Contains"
                        }
                    }
                }//,
                //  sort: { field: "CreatedDate", dir: "desc" }
            }
        }
        
        accCashAdvanceDataSource.getIndex({ username: CURRENT_USERNAME }, function (data) {
            $scope.gridKendoOptionRelated = gridKendoOptionFunction(data);
        });
    }]);