﻿'use strict';
angular.module('app.controllers').controller('WorkspaceCashAdvanceController',
    ['$scope', '$http', 'inquiryGrid', 'workspaceCashAdvanceDataSource', 'appText', 'controlOptions',
    function ($scope, $http, inquiryGrid, dataSource, appText, controlOptions) {

        var _employeeGridKendoOption;

        $scope.gridKendoOption = inquiryGrid.gridKendoOptionApi(dataSource.worklist.url,
        {
            height: '350x',
            scrollable: false,
        });

        $scope.gridKendoOption.dataSource = dataSource.worklist.kendoDataSource;
        $scope.gridKendoOption.filterable = {
            extra: false,
            operators: {
                string: {
                    contains: appText.contains,
                    doesnotcontain: appText.doesnotcontain,
                    eq: appText.eq,
                    neq: appText.neq
                }
            }
        };

        $scope.gridKendoOption.columns =
        [
            { template: '<a class=\'k-button\' href=\'#= Data #\'><i class=\'fa fa-pencil\'></i> Open</a>', width: '90px' },
            { field: "Folio", title: "Document No", width: "200px" },
            //{ field: "ProcessName", title: "ProcessName", width: "200px" },
            //{ field: "SN", title: "SN", width: "200px" },
            {
                field: "Title", title: "Title", width: "180px", filterable: false, sortable: false,
                template: function (data) {
                    if (data.Title !== undefined) {
                        return "<div style='width:100%; word-wrap: break-word;'>" + data.Title + "</div>";
                    }
                 return "";
                }
            },
            { field: "ActivityDescription", title: "Activity description", width: "150px" },
            {
                field: "StartDate", title: "Received", width: "120px", attributes: { "class": "text-center" }, template: inquiryGrid.formatDate('StartDate'),
                filterable: false
            },
        ];
        
        $scope.openredirectUsers = function () {
            var dataItem = this.dataItem;
            $scope.selectWorklist = dataItem;
            $scope.employeeWindow.center();
            $scope.employeeWindow.open();
        }

        $scope.employeeWindowOption = {
            width: 600,
            height: 300,
            title: "Redirect",
            modal: true
        }

        $scope.employeePopupOptions = controlOptions.popupSearch('employee');
    }]);