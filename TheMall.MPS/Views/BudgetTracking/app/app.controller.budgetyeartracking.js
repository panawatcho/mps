﻿angular.module('app.controllers').controller('budgetyeartrackingController',
[
    '$scope', '$routeParams', 'inquiryGrid', 'authService',
    function ($scope, $routeParams, inquiryGrid, authService) {
        var params = {
            height: '450px',
            //height: 'auto',
            pageSize: 10,
            scrollable: true,
            fields: {
                Budget: { type: "number" },
                Reserve: { type: "number" },
                Return: { type: "number" },
                ReturnMemo: { type: "number" },
                IncomeOther: { type: "number" },
                IncomeDeposit: { type: "number" },
                Remaining: { type: "number" }

            }
        }
        var filters = [];
        if (authService.authentication && authService.authentication.me) {
            angular.forEach(authService.authentication.me.UnitCode, function (data) {
                if (data.UnitCode && CURRENT_USERNAME.toUpperCase() !== "K2ADMIN") {
                    var filter = { field: "UnitCode", operator: "contains", value: data.UnitCode };
                    filters.push(filter);
                }
            });
        }
        if (filters.length === 0 && CURRENT_USERNAME.toUpperCase() !== "K2ADMIN") {
            filters.push({ field: "UnitCode", operator: "contains", value: "-" });
        }
        params.filters = { filters: filters, logic: "or" };
        $scope.gridKendoOption = inquiryGrid.gridKendoOption(angular.crs.url.odata('budgetyeartrackingview'), params);
        $scope.gridKendoOption.columns = [

            {
                field: 'Year',
                title: 'Year',
                width: '90px', locked: true
            },
            {
                field: 'UnitCode',
                title: 'Unit Code (A&P)',
                width: '110px', locked: true
            },
            {
                field: 'UnitName',
                title: 'UnitName',
                width: '180px', locked: true
            },
             {
                 field: 'TypeYearPlan',
                 title: 'Type Budget',
                 width: '130px', locked: true,
                 template: function (data) {
                    
                     if (data.TypeYearPlan === 'AP') {
                                        return "<span>A&P Budget</span>";
                                   }
                           else {
                                        return "<span>INCOME Budget</span>";
                             }
                 }
                 
             },
            {
                field: 'Budget',
                title: 'Budget (Plan)',
                width: '120px', locked: true,
                template: inquiryGrid.formatCurrency4('Budget')
            },
            {
                field: 'Reserve',
                title: 'Budget (Reserve)',
                width: '120px',
                template: inquiryGrid.formatCurrency4('Reserve')
            },
            {
                field: 'Return',
                title: 'Budget (Return)',
                width: '120px',
                template: inquiryGrid.formatCurrency4('Return')
            },
            {
                field: 'IncomeDeposit',
                title: 'Income Deposit',
                width: '120px',
                template: inquiryGrid.formatCurrency4('IncomeDeposit')
            },
            {
                field: 'IncomeOther',
                title: 'Income Other',
                width: '120px',
                template: inquiryGrid.formatCurrency4('IncomeOther')
            },
            {
                field: 'ReturnMemo',
                title: 'Return (Memo)',
                width: '120px',
                template: inquiryGrid.formatCurrency4('ReturnMemo')
            },
            {

                title: 'Budget (Remaining)',
                width: '120px',
                template: function (data) {
                    var remaining = data.Budget - (data.Reserve + data.Return + data.IncomeDeposit + data.IncomeOther + data.ReturnMemo);

                    var strremaining = (remaining !== 0) ? kendo.toString(remaining, "n4") : 0.0000;
                    return "<div class='text-right'>" + strremaining + "</div>";

                }
            }

        ];
        
        
        
        $scope.gridKendoOption.toolbar = ["excel"];
        $scope.gridKendoOption.excel = {

            fileName: new Date().getFullYear().toString() + "_" + (new Date().getMonth() + 1).toString() + "_" + new Date().getDate().toString() + "_" + "Budget Tracking (Year).xlsx",
            proxyURL: "https://demos.telerik.com/kendo-ui/service/export",
            filterable: true,
            allPages: true,

        }
        
    }
]);