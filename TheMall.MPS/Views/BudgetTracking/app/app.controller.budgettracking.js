﻿angular.module('app.controllers').controller('budgettrackingController',
[
    '$scope', '$routeParams', 'inquiryGrid', 'authService','reportService','appUrl',
    function ($scope, $routeParams, inquiryGrid, authService, reportService, appUrl) {
        var params = {
            mobile: true,
            height: '350px',
            //height: 'auto',
            pageSize: 10,
            scrollable: true,
            fields: {
                BudgetPlan: { type: "number" },
                BudgetUsed: { type: "number" },
                BudgetRemaining: { type: "number" },
                Memo: { type: "number" },
                PettyCash: { type: "number" },
                CashAdvance: { type: "number" },
                PR: { type: "number" },
                PO: { type: "number" },
                GR: { type: "number" }
            }
        }
        var filters = [];
        if (authService.authentication && authService.authentication.me) {
            angular.forEach(authService.authentication.me.UnitCode,
                function (data) {
                    //console.log(data);
                    if (data.UnitCode && CURRENT_USERNAME.toUpperCase() !== "K2ADMIN") {
                        var filter = { field: "UnitCode", operator: "contains", value: data.UnitCode };
                        filters.push(filter);
                    }
                });
        }
        if (filters.length === 0 && CURRENT_USERNAME.toUpperCase() !== "K2ADMIN") {
            filters.push({ field: "UnitCode", operator: "contains", value: "-" });
        }
        params.filters = { filters: filters, logic: "or" };

        $scope.gridKendoOption = inquiryGrid.gridKendoOption(angular.crs.url.odata('budgettrackingview'), params);

        $scope.gridKendoOption.columns = [
            {
                field: 'ProposalNo',
                title: 'Proposal Number',
                width: '100px',
                template: function(data) {
                    return '<a href="proposal/detail/' +
                        data.ProposalId +
                        '" target="_blank">' +
                        data.ProposalNo +
                        '<a/>';
                },
                locked: true
            },
            {
                field: 'ProposalUnitCode',
                title: 'Unit Code (Proposal)',
                width: '110px',
                locked: true
            },
            {
                field: 'UnitCode',
                title: 'Unit Code (A&P)',
                width: '110px',
                locked: true
            },
            {
                field: 'APCode',
                title: 'A&P',
                width: '80px',
                locked: true
            },
            {
                field: 'Description',
                title: 'Description',
                width: '120px',
                locked: true
            },
            {
                field: 'BudgetPlan',
                title: 'Budget (Plan)',
                width: '120px',
                locked: true,
                template: inquiryGrid.formatCurrency4('BudgetPlan')
            },
            {
                field: 'BudgetUsed',
                title: 'Budget (Reserve)',
                width: '120px',
                locked: true,
                template: inquiryGrid.formatCurrency4('BudgetUsed')
            },
            {
                field: 'BudgetRemaining',
                title: 'Budget (Remaining)',
                width: '120px',
                locked: true,
                template: inquiryGrid.formatCurrency4('BudgetRemaining')
            },
            {
                title: 'Budget (Reserve)',
                columns: [
                    {
                        field: 'Memo',
                        title: 'Memo',
                        width: '120px',
                        template: inquiryGrid.formatCurrency4('Memo')
                    },
                    {
                        field: 'PettyCash',
                        title: 'Petty Cash',
                        width: '120px',
                        template: inquiryGrid.formatCurrency4('PettyCash')
                    },
                    {
                        field: 'CashAdvance',
                        title: 'Cash Advance',
                        width: '120px',
                        template: inquiryGrid.formatCurrency4('CashAdvance')
                    },
                    {
                        field: 'PR',
                        title: 'Purchase Requisition',
                        width: '120px',
                        template: inquiryGrid.formatCurrency4('PR')
                    },
                    {
                        field: 'PO',
                        title: 'Purchase Order',
                        width: '120px',
                        template: inquiryGrid.formatCurrency4('PO')
                    },
                    {
                        field: 'GR',
                        title: 'Goods Receipt',
                        width: '120px',
                        template: inquiryGrid.formatCurrency4('GR')
                    }
                ]
            }
        ];

       //$scope.gridKendoOption.toolbar = kendo.template($("#budgettrackingToolbarTemplate").html());

        //export excel
        $scope.gridKendoOption.excel = {
            fileName: new Date().getFullYear().toString() + "_" + (new Date().getMonth() + 1).toString() + "_" + new Date().getDate().toString() + "_" + "Budget Tracking (Proposal).xlsx",
            proxyURL: "https://demos.telerik.com/kendo-ui/service/export",
            filterable: true,
            allPages: true,
        }

        //$scope.gridKendoOption.toolbar = ["excel", "<a class='btn btn-primary' ng-click='download()' title=''><i class='fa fa-download'></i> Download</a>"];

        $scope.gridKendoOption.toolbar = ["excel",
         {
             template: $("#budgettrackingToolbarTemplate").html()
         }
        ]

        //$scope.printReport = function () {
        //    var params = {
        //        inline: true,
        //        //show: true,
        //        fullscreen: true,
        //        format: "XLS"
        //    }
        //    var url = angular.crs.url.rootweb("report/Budgettracking/?params=" + JSON.stringify(params));
        //    return url;
        //}

        var reportUrl = appUrl.api + "reportbudgettracking/budgettracking/" + CURRENT_USERNAME;
        $scope.download = reportService.download(reportUrl);
    }
]);