﻿angular.module('app.controllers').controller('budgetYearPlanFormController',
[
    '$scope', '$routeParams', 'masterGrid', 'messageBox',
    'lineGrid', '$location',
    '$http', 'controlOptions','budgetYearPlanDataSource',
    function($scope, $routeParams, masterGrid, messageBox,
        lineGrid, $location,
        $http, controlOptions, budgetYearPlanDataSource) {
        $scope.unitcodeOption = controlOptions.dropdown('unitcode');
        $scope.form = {};
        $scope.temp = {};
        $scope.checkNew = true;

        if (!$routeParams.UnitCode || !$routeParams.Year) {
            $scope.canDeleted = false;
            $scope.edit = false;
            //   messageBox.error("Sequence number and name are invalid!!!");
            //$location.path("unitcodeforemployee/");
        } else {
            $scope.canDeleted = true;
            $scope.edit = true;
            budgetYearPlanDataSource.searchByYearAndUnitCode({ year: $routeParams.Year, unitcode: $routeParams.UnitCode }, function (data) {
                $scope.form = data;
                $scope.checkNew = false;
                if (data.length > 0) {
                    $scope.form = data[0];
                }
                if ($scope.form.TypeYearPlan) {
                    $scope.temp.typeYearPlan = {
                        value: $scope.form.TypeYearPlan
                    }
                }
            });
        }

        $scope.typeYearPlanOption = {
            dataTextField: "value",
            dataValueField: "value",
            autoBind: false,
            optionLabel: "please select",
            template: '#=data.value#',
            valueTemplate: '#=data.value#',
            dataSource: new kendo.data.DataSource({
                type: "json",
                data: new kendo.data.ObservableArray([
                    {
                        value: "AP"
                    },
                    {
                        value: "DP"
                    }
                ])
            })
        }




        $scope.YearTime = {
            start: "decade",
            depth: "decade",
            format:"yyyy"
        }

        $scope.validate = function (event) {
            if (!$scope.validator.validate()) return false;
            if ($scope.temp.typeYearPlan) {
                $scope.form.TypeYearPlan = $scope.temp.typeYearPlan.value;
            }
            if ($scope.checkNew) {
                budgetYearPlanDataSource.save($scope.form).$promise.then(
                    function (resp) {
                        if (resp) {
                            messageBox.success('Saved successfully');
                            $location.path('budgetyearplan/');
                        }
                    },
                    function (error) {
                        messageBox.extractError(error);
                    }
                );
            } else {
                budgetYearPlanDataSource.edit($scope.form).$promise.then(
                    function (resp) {
                        if (resp) {
                            messageBox.success('Saved successfully');
                            $location.path('budgetyearplan/');
                        }
                    },
                    function (error) {
                        messageBox.extractError(error);
                    }
                );
            }
        }

        $scope.delete = function () {
            var deleted = confirm("Delete this data ?");
            if (deleted) {
                budgetYearPlanDataSource.deleted($scope.form).$promise.then(
                     function (resp) {
                         if (resp) {
                             messageBox.success("Delete successfully.");
                             $location.path('budgetyearplan/');
                         }
                     },
                    function (error) {
                        messageBox.extractError(error);
                    }
                    );
            }
        }
    }
]);