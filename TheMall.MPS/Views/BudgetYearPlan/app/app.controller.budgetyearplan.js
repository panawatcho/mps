﻿angular.module('app.controllers').controller('budgetyearplanController',
[
    '$scope', '$routeParams','masterGrid','inquiryGrid',
    function ($scope, $routeParams, masterGrid,inquiryGrid) {
    
        var _gridKendoOption;
        var _commandTemplateAllstatus = function (data) {
            return '<div class="btn-group" role="group">' +
                '<a class="btn btn-default" style="width:60px" href="' + $routeParams.page + '/Form?Year=' + data.Year + '&UnitCode=' + data.UnitCode + '" title="edit"><i class="fa fa-edit"></i></a>' +
                '</div>';
        }
        $scope.odataUrl = angular.crs.url.odata('budgetyearplantable');
        $scope.apiUrl = angular.crs.url.webApi('budgetyearplan');
        $scope.gridKendoOption = {
            dataSource: new kendo.data.DataSource({
                transport: {
                    read: {
                        async: true,
                        url: $scope.apiUrl,
                        dataType: "json"
                    },
                    parameterMap: function (options, operation) {
                        if (operation !== "read" && options.models) {
                        return {
                                Id: options.models[0].Id,
                                TeamName: options.models[0].TeamName,
                                Type: options.models[0].Type,
                                Budget: options.models[0].Budget,
                                Year: options.models[0].Year,
                                UnitCode: options.models[0].UnitCode,
                                UnitName: options.models[0].UnitName,
                                Order: options.models[0].Order,
                                TypeYearPlan: options.models[0].TypeYearPlan
                                };
                            }
                            return null;
                    }
                },
                pageSize: 10,
                batch: true,
                schema: {
                    model: {
                        id: "Id",
                        fields:
                        {
                            Id: { type: "number" },
                            TeamName: { editable: true, nullable: false },
                            Type: { editable: true, nullable: false },
                            Budget: { editable: true, nullable: false },
                            Year: { editable: true, nullable: false },
                            UnitCode: { editable: true, nullable: false },
                            UnitName: { editable: true, nullable: false },
                            Order: { editable: true, nullable: false },
                            TypeYearPlan: { editable: true, nullable: false }
                        }
                    }
                }
            }),
            toolbar: ["create"],
            columns: [
                {
                    field: "TeamName", title: "Team Name", width: "170px",
                    template: function (data) {
                        if (data.TeamName) {
                            return "<div style='width:100%; word-wrap: break-word;'>" + data.TeamName + "</div>";
                        }
                        return "";
                    }
                },
                {
                    field: "Type", title: "Type", width: "70px",
                    template: function (data) {
                        if (data.Type) {
                            return "<div style='width:100%; word-wrap: break-word;'>" + data.Type + "</div>";
                        }
                        return "";
                    }
                },
                {
                    field: "Budget", title: "Budget", width: "100px",
                    template: function (data) {
                        if (data.Budget) {
                            return "<div style='width:100%; word-wrap: break-word;'>" + data.Budget + "</div>";
                        }
                        return "";
                    }
                },
                {
                    field: "Year", title: "Year", width: "70px",
                    template: function (data) {
                        if (data.Year) {
                            return "<div style='width:100%; word-wrap: break-word;'>" + data.Year + "</div>";
                        }
                        return "";
                    }
                },
                {
                    field: "UnitCode", title: "Unit Code", width: "220px",
                    template: function (data) {
                        if (data.UnitCode) {
                            return "<div style='width:100%; word-wrap: break-word;'>" + data.UnitCode + ' - ' + data.UnitName + "</div>";
                        }
                        return "";
                    }
                },
                {
                    field: "TypeYearPlan", title: "Year plan type", width: "100px",
                    template: function (data) {
                        if (data.TypeYearPlan) {
                            return "<div style='width:100%; word-wrap: break-word;'>" + data.TypeYearPlan + "</div>";
                        }
                        return "";
                    }
                },
                {
                    field: "Order", title: "Order", width: "50px",
                    template: inquiryGrid.formatCurrency('Order')
                },
                { template: _commandTemplateAllstatus, width: '100px' }
            ],
            filterable: true,
            pageable: {
                refresh: true,
                pageSizes: true,
                buttonCount: 5
            }
        };
        $scope.gridKendoOption.toolbar = kendo.template($("#budgetYearPlanTemplate").html());
        //$scope.gridKendoOption = function () {
        //    if (!_gridKendoOption) {
        //        $scope.odataUrl = angular.crs.url.odata('budgetyearplantable');
        //        $scope.apiUrl = angular.crs.url.webApi('budgetyearplan');
              
        //        _gridKendoOption = masterGrid.gridKendoOption($scope.odataUrl, $scope.apiUrl,
        //        {
        //            parameterMap: function (options, operation) {
        //                if (operation !== "read" && options.models) {
        //                    return {
        //                        Id: options.models[0].Id,
        //                        TeamName: options.models[0].TeamName,
        //                        Type: options.models[0].Type,
        //                        Budget: options.models[0].Budget,
        //                        Year: options.models[0].Year,
        //                        UnitCode: options.models[0].UnitCode,
        //                        UnitName: options.models[0].UnitName};
        //                }
        //                return null;
        //            },
        //            schema: {
        //                model: {
        //                    id: "Id",
        //                    fields: {
        //                        Id: {type:"number" },
        //                        TeamName: { editable: true, nullable: false },
        //                        Type: { editable: true, nullable: false },
        //                        Budget: { editable: true, nullable: false },
        //                        Year: { editable: true, nullable: false },
        //                        UnitCode: { editable: true, nullable: false },
        //                        UnitName: { editable: true, nullable: false }
        //                    }
        //                }
        //            },
        //            toolbar: ["create"],
        //            columns: [
        //                { field: "TeamName", title: "Team Name", width: "150px" },
        //                { field: "Type", title: "Type", width: "150px" },
        //                { field: "Budget", title: "Budget", width: "150px" },
        //                { field: "Year", title: "Year", width: "150px" },
        //                { field: "UnitCode", title: "Unit Code", width: "150px" },
        //                { field: "UnitName", title: "Unit Name", width: "150px" },
        //                { command: ["edit", "destroy"], title: "&nbsp;", width: 200 }
        //            ],
        //            editable: {
        //                       mode: "inline",
        //                       createAt: "top"
        //                   }
        //        });
        //    }
        //    return _gridKendoOption;
        //}
     
        //var _gridKendoOption;
        //$scope.gridKendoOption = function () {
        //    if (!_gridKendoOption) {
        //        $scope.datasourceUrl = angular.crs.url.odata('unitcodetable');
        //        $scope.Url = angular.crs.url.webApi('unitcode');
              
        //        _gridKendoOption = {
        //            dataSource: new kendo.data.DataSource({
        //                transport: {
        //                    read: {
        //                        async: true,
        //                        url: $scope.datasourceUrl,
        //                        dataType: "json",
        //                        data: undefined
        //                    },
        //                    update: {
        //                        url: $scope.Url + "/Update",
        //                        dataType: "json",
        //                        method: "post"
        //                    },
        //                    destroy: {
        //                        url: $scope.Url + "/Delete",
        //                        dataType: "json",
        //                        method: "post"
        //                    },
        //                    create: {
        //                        url: $scope.Url + "/Create",
        //                        dataType: "json",
        //                        method: "post"
        //                    },
        //                    parameterMap: function (options, operation) {
        //                        if (operation !== "read" && options.models) {
        //                            return {
        //                                UnitCode: options.models[0].UnitCode,
        //                                UnitName: options.models[0].UnitName,
                                      
        //                            };
        //                        }
        //                        return null;
        //                    }
        //                },
        //                error: function (e) {
        //                    console.log(e);
        //                    var message = e.xhr.responseJSON.Message;
        //                    alert(message);
        //                },
                       
        //                batch: true,
        //                pageSize: 10,
        //                schema: {
        //                    data: function (data) { return data.value; },
        //                    total: function (data) { return data["odata.count"]; },
        //                    model: {
        //                        id: "UnitCode",
        //                        fields: {
        //                            UnitCode: { editable: true, nullable: false },
        //                            UnitName: { editable: true, nullable: false },
                                   
        //                        }
        //                    }
        //                }
        //            }),
        //            height: 550,
        //            toolbar: ["create"],
        //            columns: [
        //                { field: "UnitCode", title: "Unit Code", width: "200px" },
        //                { field: "UnitName", title: "Unit Namee", width: "200px" },
        //                { command: ["edit", "destroy"], title: "&nbsp;", width: 200 }
        //            ],
                   
        //            pageable: {
        //                refresh: true,
        //                pageSizes: [5, 10, 20, 50, 100]
        //            },
                  
        //            selectable: "row",
        //            editable: {
        //                mode: "inline",
        //                createAt: "top"
        //            }

        //        };
        //    }
        //    return _gridKendoOption;
        //}





    }
]);