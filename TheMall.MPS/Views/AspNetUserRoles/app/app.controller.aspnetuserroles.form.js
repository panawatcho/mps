﻿angular.module('app.controllers').controller('aspNetUserRolesFormController',
[
    '$scope', '$routeParams', 'masterGrid', 'messageBox',
    'lineGrid', '$location',
    '$http', 'controlOptions', 'aspNetUserRolesDataSource',
    function ($scope, $routeParams, masterGrid, messageBox, lineGrid, $location,
        $http, controlOptions,aspNetUserRolesDataSource) {
        $scope.form = {};
        $scope.checkNew = true;
        $scope.employeeOption = controlOptions.dropdown('employee');
        $scope.aspNetRolesOption = controlOptions.dropdown('aspnetroles');

        if (!$routeParams.RoleId && !$routeParams.Username) {
            $scope.canDeleted = false;
            $scope.edit = false;
            //   messageBox.error("Sequence number and name are invalid!!!");
            //$location.path("unitcodeforemployee/");
        } else {
            $scope.canDeleted = true;
            $scope.edit = true;
            aspNetUserRolesDataSource.getByUserIdAndRoleId({ roleId: $routeParams.RoleId, username: $routeParams.Username }, function (data) {
                $scope.form = data;
                $scope.checkNew = false;
                console.log($scope.form);
            });
        }

        $scope.validate = function (event) {
            if (!$scope.validator.validate()) return false;
            if ($scope.checkNew) {
                aspNetUserRolesDataSource.save($scope.form).$promise.then(
                    function (resp) {
                        if (resp) {
                            messageBox.success('Saved successfully');
                            $location.path('aspnetuserroles/');
                        }
                    },
                    function (error) {
                        messageBox.extractError(error);
                    }
                );
            } else {
                aspNetUserRolesDataSource.edit($scope.form).$promise.then(
                    function (resp) {
                        if (resp) {
                            messageBox.success('Saved successfully');
                            $location.path('aspnetuserroles/');
                        }
                    },
                    function (error) {
                        messageBox.extractError(error);
                    }
                );
            }
        }

        $scope.delete = function () {
            var deleted = confirm("Delete this data ?");
            if (deleted) {
                aspNetUserRolesDataSource.deleted($scope.form).$promise.then(
                     function (resp) {
                         if (resp) {
                             messageBox.success("Delete successfully.");
                             $location.path('aspnetuserroles/');
                         }
                     },
                    function (error) {
                        messageBox.extractError(error);
                    }
                );
            }
        }
    }
]);