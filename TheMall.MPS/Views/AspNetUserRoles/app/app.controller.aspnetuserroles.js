﻿angular.module('app.controllers').controller('aspnetuserrolesController',
[
    '$scope', '$routeParams', 'masterGrid', 'inquiryGrid',
    function ($scope, $routeParams, masterGrid, inquiryGrid) {
        var _gridKendoOption;
        var _commandTemplateAllstatus = function (data) {
            return '<div class="btn-group" role="group">' +
                '<a class="btn btn-default" style="width:60px" href="' + $routeParams.page + '/Form?RoleId=' + data.RoleId + '&Username=' + data.Username + '" title="edit"><i class="fa fa-edit"></i></a>' +
                '</div>';
        }

        $scope.apiUrl = angular.crs.url.webApi('aspnetuserroles/getall');

        $scope.gridKendoOption = {
            dataSource: new kendo.data.DataSource({
                transport: {
                    read: {
                        async: true,
                        url: $scope.apiUrl,
                        dataType: "json"
                    },
                    parameterMap: function (options, operation) {
                        if (operation !== "read" && options.models) {
                            return {
                                RoleId: options.models[0].RoleId,
                                Username: options.models[0].Username,
                                Inactive: options.models[0].Inactive
                            };
                        }
                        return null;
                    }
                },
                pageSize: 10,
                batch: true,
                sort: { field: "RoleId", dir: "desc" },
                schema: {
                    model: {
                        id: "RoleId",
                        fields:
                        {
                            RoleId: { editable: true, nullable: false },
                            Username: { editable: true, nullable: false },
                            Inactive: { editable: true, nullable: false }
                        }
                    }
                }
            }),
            toolbar: ["create"],
            columns: [
                {
                    field: "RoleId", title: "Role id", width: "200px",
                    template: function (data) {
                        if (data.RoleId) {
                            return "<div style='width:100%; word-wrap: break-word;'>" + data.RoleId + " - " + data.Role.Name + "</div>";
                        }
                        return "";
                    }
                },
                {
                    field: "Username", title: "Username", width: "200px",
                    template: function (data) {
                        if (data.Username) {
                            return "<div style='width:100%; word-wrap: break-word;'>" + data.Username + "</div>";
                        }
                        return "";
                    }
                },
                {
                    field: "Inactive",
                    title: "Active",
                    width: "80px",
                    template: '<div class="text-center">#if(!data.Inactive){# <i class="fa fa-check" style="color:green;"></i>#}else{#<i class="fa fa-times" style="color:red;"></i> #}#</div>'
                    //template: function(data) {

                    //    return '<center><input type="checkbox" disabled="disabled" ng-checked="' + !data.InActive + '"/></center>';
                    //}
                },
                { template: _commandTemplateAllstatus, width: '100px' }
            ],
            filterable: true,
            pageable: {
                refresh: true,
                pageSizes: true,
                buttonCount: 5
            }
        };
        $scope.gridKendoOption.toolbar = kendo.template($("#applicationUserRolesTemplate").html());

    }
]);

