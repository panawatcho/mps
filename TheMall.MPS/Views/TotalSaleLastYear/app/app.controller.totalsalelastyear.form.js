﻿angular.module('app.controllers').controller('totalsalelastyearformcontroller',
[
    '$scope', '$routeParams', 'masterGrid', 'messageBox',
    'lineGrid', '$location',
    '$http', 'controlOptions', 'totalSaleLastYearDataSource',
    function($scope, $routeParams, masterGrid, messageBox,
        lineGrid, $location,
        $http, controlOptions, dataSource) {
        $scope.form = {};

        $scope.checkNew = true;
        $scope.sharedBranchDropdownOption = controlOptions.dropdown('sharedbranch');
        //Date time year
        $scope.YearTime = {
            start: "decade",
            depth: "decade",
            format: "yyyy"
        }
        //

        if (!$routeParams.BranchCode || !$routeParams.Year) {
            $scope.canDeleted = false;
            $scope.edit = false;
            //   messageBox.error("Sequence number and name are invalid!!!");

            //$location.path("unitcodeforemployee/");
        } else {
            $scope.canDeleted = true;
            $scope.edit = true;
            dataSource.search({ branchCode: $routeParams.BranchCode, year: $routeParams.Year },
                function (data) {
                $scope.form = data;
                $scope.checkNew = false;
                console.log($scope.form);
                });
        }
        //save&update
        $scope.validate = function (event) {
            if (!$scope.validator.validate()) return false;
            if ($scope.checkNew) {
                dataSource.save($scope.form).$promise.then(
                    function (resp) {
                        if (resp) {
                            messageBox.success('Saved successfully');
                            $location.path('totalsalelastyear/');
                        }
                    },
                    function (error) {
                        messageBox.extractError(error);
                    }
                );
            } else {
                dataSource.edit($scope.form).$promise.then(
                    function (resp) {
                        if (resp) {
                            messageBox.success('Saved successfully');
                            $location.path('totalsalelastyear/');
                        }
                    },
                    function (error) {
                        messageBox.extractError(error);
                    }
                );
            }
        }

        $scope.delete = function () {
            var deleted = confirm("Delete this data ?");
            if (deleted) {
                dataSource.deleted($scope.form).$promise.then(
                     function (resp) {
                         if (resp) {
                             messageBox.success("Delete successfully.");
                             $location.path('totalsalelastyear/');
                         }
                     },
                    function (error) {
                        messageBox.extractError(error);
                    }
                    );
            }
        }
    }
]);