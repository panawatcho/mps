﻿angular.module('app.controllers').controller('totalsalelastyearcontroller',
[
    '$scope', '$routeParams', 'masterGrid','lineGrid',
    function ($scope, $routeParams, masterGrid, lineGrid) {
        var _commandTemplateAllstatus = function (data) {
            return '<div class="btn-group" role="group">' +
                '<a class="btn btn-default" style="width:60px" href="' + $routeParams.page + '/Form?BranchCode=' + data.BranchCode + '&Year=' + data.Year + '" title="edit"><i class="fa fa-edit"></i></a>' +
                '</div>';
        }
        $scope.apiUrl = angular.crs.url.webApi('totalsalelastyear/getalldata');
        $scope.gridKendoOption = {
            dataSource: new kendo.data.DataSource({
                transport: {
                    read: {
                        async: true,
                        url: $scope.apiUrl,
                        dataType: "json"
                    },
                    parameterMap: function (options, operation) {
                        if (operation !== "read" && options.models) {
                            return {
                                BranchCode: options.models[0].BranchCode,
                                TotalSale: options.models[0].TotalSale,
                                Year: options.models[0].Year,
                                PercentTR: options.models[0].PercentTR,
                                PercentRE: options.models[0].PercentRE,
                                AreaTR: options.models[0].AreaTR,
                                AreaRE: options.models[0].AreaRE
                            };
                        }
                        return null;
                    }
                },
                pageSize: 10,
                batch: true,
                schema: {
                    model: {
                        id: "BranchCode",
                        fields:
                        {
                            BranchCode: { type: "string" },
                            TotalSale: { editable: false, nullable: false },
                            Year: { editable: false, nullable: false },
                            PercentTR: { editable: false, nullable: false },
                            PercentRE: { editable: false, nullable: false },
                            AreaTR: { editable: false, nullable: false },
                            AreaRE: { editable: false, nullable: false }
                        }
                    }
                }
            }),
            toolbar: ["create"],
            columns: [
                  {
                      field: "BranchCode", title: "Branch code", width: "150px",
                      template: function (data) {
                          if (data.BranchCode) {
                              return "<div style='width:100%; word-wrap: break-word;'>" + data.BranchCode + "</div>";
                          }
                          return "";
                      }
                  },
                  {
                      field: "Year", title: "Year", width: "150px",
                      template: function (data) {
                          if (data.Year) {
                              return "<div style='width:100%; word-wrap: break-word;'>" + data.Year + "</div>";
                          }
                          return "";
                      }
                  },
                  {
                      field: "TotalSale", title: "Total sale", width: "150px",
                      template: lineGrid.formatCurrency('TotalSale')
                  },
                  {
                      field: "PercentTR", title: "PercentTR", width: "150px",
                      template: lineGrid.formatCurrency('PercentTR')
                  },
                  {
                      field: "PercentRE", title: "PercentRE", width: "150px",
                      template: lineGrid.formatCurrency('PercentRE')
                  },
                  {
                      field: "AreaTR", title: "AreaTR", width: "150px",
                      template: lineGrid.formatCurrency('AreaTR')
                  },
                  {
                      field: "AreaRE", title: "AreaRE", width: "150px",
                      template: lineGrid.formatCurrency('AreaRE')
                  },
                  {
                      template: _commandTemplateAllstatus, width: '110px'
                  }
            ],
            filterable: true,
            pageable: {
                refresh: true,
                pageSizes: true,
                buttonCount: 5
            }
        }
        $scope.gridKendoOption.toolbar = kendo.template($("#totalSaleLastYearTemplate").html());
    }
]); 