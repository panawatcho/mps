﻿angular.module('app.controllers').controller('accountingFormController',
[
    '$scope', '$routeParams', 'masterGrid', 'messageBox',
    'accountingDataSource', 'lineGrid', '$location',
    '$http', 'controlOptions',
    function ($scope, $routeParams, masterGrid, messageBox,
        accountingDataSource, lineGrid, $location,
        $http, controlOptions) {
        $scope.form = {};

        $scope.employeeOption = controlOptions.dropdown('employee');

        $scope.branchOption = controlOptions.dropdown('sharedbranch');
        $scope.checkNew = true;


        if (!$routeParams.BranchCode || !$routeParams.Username) {
            $scope.canDeleted = false;
            $scope.edit = false;
            //   messageBox.error("Sequence number and name are invalid!!!");

            //$location.path("unitcodeforemployee/");
        } else {
            $scope.canDeleted = true;
            $scope.edit = true;
            accountingDataSource.search({ branchCode: $routeParams.BranchCode, username: $routeParams.Username },
                function (data) {
                    if (data) {
                        $scope.checkNew = false;
                        $scope.form = data;
                        console.log($scope.form);
                    }
            });
        }


        $scope.validate = function (event) {
            if (!$scope.validator.validate()) return false;
            if ($scope.checkNew) {
                console.log(":)");
                accountingDataSource.save($scope.form).$promise.then(
                    function (resp) {
                        if (resp) {
                            messageBox.success('Saved successfully');
                            $location.path('accounting/');
                        }
                    },
                    function (error) {
                        messageBox.extractError(error);
                    }
                );
            } else {
                accountingDataSource.edit($scope.form).$promise.then(
                    function (resp) {
                        if (resp) {
                            messageBox.success('Saved successfully');
                            $location.path('accounting/');
                        }
                    },
                    function (error) {
                        messageBox.extractError(error);
                    }
                );
            }
        }




        $scope.delete = function () {
            var deleted = confirm("Delete this data ?");
            if (deleted) {
                accountingDataSource.deleted({ branchCode: $routeParams.BranchCode, username: $routeParams.Username }, function (resp) {
                    //messageBox.success("Delete successfully.");
                    $location.path('accounting/');
                });
            }
        }
    }
]);