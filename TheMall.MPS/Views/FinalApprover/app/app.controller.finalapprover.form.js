﻿angular.module('app.controllers').controller('finalApproverFormController',
[
    '$scope', '$routeParams', 'masterGrid', 'lineGrid', 'finalApproverDataSource', 'controlOptions',
    'messageBox','$location',
    function ($scope, $routeParams, masterGrid, lineGrid, finalApproverDataSource, controlOptions,
        messageBox, $location) {
        $scope.form = {};
        $scope.checkNew = true;


        $scope.employeeOption = controlOptions.dropdown('employee');

        $scope.unitcodeOption = controlOptions.dropdown('unitcode');

        $scope.processApproveOption = controlOptions.dropdown('processapprove');

        if (!$routeParams.processCode && !$routeParams.unitCode && !$routeParams.username) {
            $scope.canDeleted = false;
            $scope.edit = false;
            //   messageBox.error("Sequence number and name are invalid!!!");

            //$location.path("unitcodeforemployee/");
        } else {
            $scope.canDeleted = true;
            $scope.edit = true;
            finalApproverDataSource.searchByKeys({
                processCode: $routeParams.processCode,
                unitCode: $routeParams.unitCode,
                username: $routeParams.username
            }, function (data) {
                $scope.checkNew = false;
                $scope.form = data;
                console.log(data);
            });
        }

        $scope.validate = function (event) {
            if (!$scope.validator.validate()) return false;
            if ($scope.checkNew) {
                console.log(":)");
                finalApproverDataSource.save($scope.form).$promise.then(
                    function (resp) {
                        if (resp) {
                            messageBox.success('Saved successfully');
                            $location.path('finalapprover/');
                        }
                    },
                    function (error) {
                        messageBox.extractError(error);
                    }
                );
            } else {
                console.log(":P");
                finalApproverDataSource.edit($scope.form).$promise.then(
                    function (resp) {
                        if (resp) {
                            messageBox.success('Saved successfully');
                            $location.path('finalapprover/');
                        }
                    },
                    function (error) {
                        messageBox.extractError(error);
                    }
                );
            }
        }

        $scope.delete = function () {
            var deleted = confirm("Delete this data ?");
            if (deleted) {
                console.log("8)");
                finalApproverDataSource.deleted($scope.form).$promise.then(
                     function (resp) {
                         if (resp) {
                             messageBox.success("Delete successfully.");
                             $location.path('finalapprover/');
                         }
                     },
                    function (error) {
                        messageBox.extractError(error);
                    }
                    );
            }
        }
    }
]);