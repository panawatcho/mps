﻿angular.module('app.controllers').controller('finalApproverController',
[
    '$scope', '$routeParams', 'masterGrid','lineGrid',
    function ($scope, $routeParams, masterGrid, lineGrid) {

        var _gridKendoOption;
        var _commandTemplateAllstatus = function (data) {
            return '<div class="btn-group" role="group">' +
                '<a class="btn btn-default" style="width:60px" href="' + $routeParams.page + '/Form?processCode=' + data.ProcessCode + '&unitCode=' + data.UnitCode + '&username=' + data.Username + '" title="edit"><i class="fa fa-edit"></i></a>' +
                '</div>';
        }
        $scope.odataUrl = angular.crs.url.odata('finalapprovertable');
        $scope.apiUrl = angular.crs.url.webApi('finalapprover');
        $scope.gridKendoOption = {
            dataSource: new kendo.data.DataSource({
                transport: {
                    read: {
                        async: true,
                        url: $scope.apiUrl,
                        dataType: "json"
                    },
                    parameterMap: function(options, operation) {
                        if (operation !== "read" && options.models) {
                            return {
                                EmpId: options.models[0].EmpId,
                                Username: options.models[0].Username,
                                ProcessCode: options.models[0].ProcessCode,
                                Budget: options.models[0].Budget,
                                Amount: options.models[0].Amount,
                                UnitCode: options.models[0].UnitCode,
                                InActive: options.models[0].InActive
                            };
                        }
                        return null;
                    }
                },
                pageSize: 10,
                batch: true,
                schema: {
                    model: {
                        id: "EmpId",
                        fields:
                        {
                            EmpId: { type: "string" },
                            Username: { editable: true, nullable: false },
                            ProcessCode: { editable: true, nullable: false },
                            Amount: { editable: true, nullable: false },
                            UnitCode: { editable: true, nullable: false },
                            InActive: { editable: true, nullable: false }
                        }
                    }
                }
            }),
            toolbar: ["create"],
            columns: [
                      { field: "ProcessCode", title: "ProcessCode", width: "100px" },
                      { field: "EmpId", title: "Employee id", width: "100px" },
                      { field: "Username", title: "Username", width: "200px" },
                      { field: "UnitCode", title: "Unit code", width: "150px" },
                {
                    field: "Amount", title: "Amount", width: "100px" ,
                    template: lineGrid.formatCurrency('Amount')
                },
                {
                    field: "InActive", title: "Active", width: "100px",
                    template: '<div class="text-center">#if(!data.InActive){# <i class="fa fa-check" style="color:green;"></i>#}else{#<i class="fa fa-times" style="color:red;"></i> #}#</div>'
                },
                      { template: _commandTemplateAllstatus, width: '100px' }
            ],
            filterable: true,
            pageable: {
                refresh: true,
                pageSizes: true,
                buttonCount: 5
            }
        };
        $scope.gridKendoOption.toolbar = kendo.template($("#finalApproverTemplate").html());

    }
]);