﻿app = angular.module("app.controllers");
app.factory("linetrackingcontroller", [
    "lineGrid", "messageBox", '$filter',
    function (inquiryGrid, messageBox, $filter) {
       
           var params = {
            filters: [ { field: "RequesterUserName", operator: "eq", value: CURRENT_USERNAME }
            ],
            mobile: true,
            height: '350px',
            //height: 'auto',
            pageSize: 5,
            scrollable: false,
            sorts: inquiryGrid.sortOption
        }

        $scope.trackingGridOption = inquiryGrid.gridKendoOption(angular.crs.url.odata('proposaltrackingview'), params);

        $scope.trackingGridOption.columns =
            [
             
              { field: 'DocumentType', title: 'Proposal type', width: '130px' },
              { field: 'DocumentNo', title: 'Proposal type', width: '130px' },
              { field: 'Status', title: 'Status', width: '130px' },
              { field: 'Amount', title: 'Amount', width: '130px' },
              { field: 'RefDocumentType', title: 'RefDocumentType', width: '130px' },
              { field: 'RefDocumentNo', title: 'RefDocumentNo', width: '130px' },
              { field: 'RefStatus', title: 'RefStatus', width: '130px' },
              { field: 'RefAmount', title: 'RefAmount', width: '130px' },
            
            ];
    }]);
