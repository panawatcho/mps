﻿/// <reference path="app.controller.proposal.revise.js" />
angular.module('app.controllers').controller('proposalReviseController',
    ['$scope', '$routeParams', 'controlOptions', 'lineGrid',
    '$attrs', 'proposalDataSource', 'sharedBranchDataSource',
    'expensetopicDataSource', 'uploadHandler', 'k2', '$filter',
    'IdProposalService', '$location', 'authService', 'messageBox', 'approverGrids', '$sce', '$http', 'blockUI', 'inquiryGrid', 'proposalTrackingDataSource','$timeout',
    function ($scope, $routeParams, controlOptions, lineGrid,
        $attrs, dataSource, sharedBranchDataSource,
        expensetopicDataSource, uploadHandler, k2, $filter,
        idProposalService, $location, authService, messageBox, approverGrids, $sce, $http, blockUI, inquiryGrid, proposalTrackingDataSource, $timeout) {

        //$controller('BaseController', { $scope: $scope, dataSource: dataSource, $attrs: $attrs });
        $(".sidebar-toggle").click(function () {
            //$scope.allocateGrid.refresh();
            $timeout(function () {
                $scope.allocateGrid.refresh();
            }, 1000);
        });
        var id = 0,
            sn = k2.extractSN($routeParams),
            remoteResult,
            _allocateGridOption,
            _depositGridOption,
            _incomeDepositGridOption,
            _incomeDepositReviseGridOption,
            _incomeOtherGridOption,
            _totalSaleTargetGridOption,
            _accountGridObtion,
            _expenseGrid,
            _incomeOtherGridOption2,
            _logTrackChangeGridOption;

        //
        var _approvalGridOption = [];

        //Current date
        $scope.currentDate = new Date();
        //$scope.ApproverLevel = ["เสนอ", "เห็นชอบ", "อนุมัติ", "ทราบ", "อนุมัติขั้นสุดท้าย", "สำเนาเรียน"];
        $scope.ApproverLevel = approverGrids.ApproverLevel;
        $scope.uploadHandler = uploadHandler.init();
        $scope.attchmentSaveUrl = angular.crs.url.webApi("proposal/:id/attachment");
        $scope.forms = {};
        $scope.approverLevelFiveAndSix = true;
        $scope.newModel = {};
        $scope.checkOtherTheme = false;

        $scope.form = {
            ListPlace: [],
            DocTypeFiles: [],
            OtherFiles: [],
            SharedBranchTemplateLines: [],
            ProposalLine: [],
            IncomeDeposit: new kendo.data.ObservableArray([]),
            IncomeOther: new kendo.data.ObservableArray([]),
            IncomeTotalSale: new kendo.data.ObservableArray([]),
            ProposalApproval: new kendo.data.ObservableArray([]),
            DepositLines: new kendo.data.ObservableArray([]),
            Requester: [],
            DepartmentException: [],
            InformEmail: [],
            CCEmail: [],
            Approver : [],
            LogProposalTrackChanges: [],
            //AccountTrans: new kendo.data.ObservableArray([])
        };

        //Set Due Date
        //Current date
        $scope.currentDate = new Date();
        $scope.currentDatePlusTwo = new Date($scope.currentDate);
        $scope.currentDatePlusTwo.setDate($scope.currentDatePlusTwo.getDate() + 2);
        //Set date Propose
        if ($scope.currentDatePlusTwo.getDay() === 6) {
            $scope.currentDatePlusTwo.setDate($scope.currentDatePlusTwo.getDate() + 2);
        }
        if ($scope.currentDatePlusTwo.getDay() === 0) {
            $scope.currentDatePlusTwo.setDate($scope.currentDatePlusTwo.getDate() + 1);
        }
        $scope.form.DueDatePropose = new Date($scope.currentDatePlusTwo);
        //Set date Accept
        var dueDatePropose = new Date($scope.form.DueDatePropose);
        dueDatePropose.setDate(dueDatePropose.getDate() + 2);
        if (dueDatePropose.getDay() === 6) {
            dueDatePropose.setDate(dueDatePropose.getDate() + 2);
        }
        if (dueDatePropose.getDay() === 0) {
            dueDatePropose.setDate(dueDatePropose.getDate() + 1);
        }
        $scope.form.DueDateAccept = new Date(dueDatePropose);
        //Set date Approve
        var dueDateAccept = new Date($scope.form.DueDateAccept);
        dueDateAccept.setDate(dueDateAccept.getDate() + 2);
        if (dueDateAccept.getDay() === 6) {
            dueDateAccept.setDate(dueDateAccept.getDate() + 2);
        }
        if (dueDateAccept.getDay() === 0) {
            dueDateAccept.setDate(dueDateAccept.getDate() + 1);
        }
        $scope.form.DueDateApprove = new Date(dueDateAccept);
        //Set date Final Approve
        var dueDateFinalApprove = new Date($scope.form.DueDateApprove);
        dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 2);
        if (dueDateFinalApprove.getDay() === 6) {
            dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 2);
        }
        if (dueDateFinalApprove.getDay() === 0) {
            dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 1);
        }
        $scope.form.DueDateFinalApprove = new Date(dueDateFinalApprove);

        //SetDate function 
        //Propose
        var setDateFromPropose = function () {
            //Set date Propose
            var dueDatePropose = new Date($scope.form.DueDatePropose);
            dueDatePropose.setDate(dueDatePropose.getDate() + 2);
            if (dueDatePropose.getDay() === 6) {
                dueDatePropose.setDate(dueDatePropose.getDate() + 2);
            }
            if (dueDatePropose.getDay() === 0) {
                dueDatePropose.setDate(dueDatePropose.getDate() + 1);
            }
            $scope.form.DueDateAccept = new Date(dueDatePropose);
            //Set date Accept
            var dueDateAccept = new Date($scope.form.DueDateAccept);
            dueDateAccept.setDate(dueDateAccept.getDate() + 2);
            if (dueDateAccept.getDay() === 6) {
                dueDateAccept.setDate(dueDateAccept.getDate() + 2);
            }
            if (dueDateAccept.getDay() === 0) {
                dueDateAccept.setDate(dueDateAccept.getDate() + 1);
            }
            $scope.form.DueDateApprove = new Date(dueDateAccept);
            //Set date Final Approve
            var dueDateFinalApprove = new Date($scope.form.DueDateApprove);
            dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 2);
            if (dueDateFinalApprove.getDay() === 6) {
                dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 2);
            }
            if (dueDateFinalApprove.getDay() === 0) {
                dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 1);
            }
            $scope.form.DueDateFinalApprove = new Date(dueDateFinalApprove);
        }

        $scope.changeDueDatePropose = function () {
            //Set date Accept
            var datePropose = new Date($scope.form.DueDatePropose);
            var dateAccept = new Date($scope.form.DueDateAccept);
            if (datePropose.getFullYear() > dateAccept.getFullYear()) {
                setDateFromPropose();
            } else if (datePropose.getFullYear() === dateAccept.getFullYear()
            && datePropose.getMonth() > dateAccept.getMonth()) {
                setDateFromPropose();
            } else if (datePropose.getFullYear() === dateAccept.getFullYear()
            && datePropose.getMonth() === dateAccept.getMonth()
            && datePropose.getDate() > dateAccept.getDate()) {
                setDateFromPropose();
            }
        }

        //Accept
        var setDateFromAccept = function () {
            //Set date Accept
            var dueDateAccept = new Date($scope.form.DueDateAccept);
            dueDateAccept.setDate(dueDateAccept.getDate() + 2);
            if (dueDateAccept.getDay() === 6) {
                dueDateAccept.setDate(dueDateAccept.getDate() + 2);
            }
            if (dueDateAccept.getDay() === 0) {
                dueDateAccept.setDate(dueDateAccept.getDate() + 1);
            }
            $scope.form.DueDateApprove = new Date(dueDateAccept);
            //Set date Final Approve
            var dueDateFinalApprove = new Date($scope.form.DueDateApprove);
            dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 2);
            if (dueDateFinalApprove.getDay() === 6) {
                dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 2);
            }
            if (dueDateFinalApprove.getDay() === 0) {
                dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 1);
            }
            $scope.form.DueDateFinalApprove = new Date(dueDateFinalApprove);
        }

        $scope.changeDueDateAccept = function () {
            var dateAccept = new Date($scope.form.DueDateAccept);
            var dateApprove = new Date($scope.form.DueDateApprove);
            if (dateAccept.getFullYear() > dateApprove.getFullYear()) {
                setDateFromAccept();
            } else if (dateAccept.getFullYear() === dateApprove.getFullYear()
                && dateAccept.getMonth() > dateApprove.getMonth()) {
                setDateFromAccept();
            }
            if (dateAccept.getFullYear() === dateApprove.getFullYear()
                && dateAccept.getMonth() === dateApprove.getMonth()
                && dateAccept.getDate() > dateApprove.getDate()) {
                setDateFromAccept();
            }
        }

        //Approve
        var setDateFromApprove = function () {
            //Set date Final Approve
            var dueDateFinalApprove = new Date($scope.form.DueDateApprove);
            dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 2);
            if (dueDateFinalApprove.getDay() === 6) {
                dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 2);
            }
            if (dueDateFinalApprove.getDay() === 0) {
                dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 1);
            }
            $scope.form.DueDateFinalApprove = new Date(dueDateFinalApprove);
        }

        $scope.changeDueDateApprove = function () {
            var dateApprove = new Date($scope.form.DueDateApprove);
            var dateFinalApprove = new Date($scope.form.DueDateFinalApprove);
            if (dateApprove.getFullYear() > dateFinalApprove.getFullYear()) {
                setDateFromApprove();
            } else if (dateApprove.getFullYear() === dateFinalApprove.getFullYear() &&
                dateApprove.getMonth() > dateFinalApprove.getMonth()) {
                setDateFromApprove();
            } else if (dateApprove.getFullYear() === dateFinalApprove.getFullYear() &&
                dateApprove.getMonth() === dateFinalApprove.getMonth() &&
                dateApprove.getDate() > dateFinalApprove.getDate()) {
                setDateFromApprove();
            }
        }

        $scope.Approval = new kendo.data.ObservableArray([]),
            $scope.tempDepartment = { DepartmentException: [] }

        $scope.disabled = false;
        $scope.disabledInform = false;
        $scope.isReadonlyDetails = function () {
            if ($routeParams.mode.toUpperCase() !== "CREATE"
                && $routeParams.mode.toUpperCase() !== "REVISE"
                && $routeParams.mode.toUpperCase() !== "REVISEPROPOSAL") {
                // $($("#objectiveeditor").data().kendoEditor.body).attr('contenteditable', false);
                $scope.disabled = true;
                return true;
            }
            if ($routeParams.mode.toUpperCase() === "REVISEPROPOSAL") {
                $scope.disabledInform = true;
            }
            $scope.disabled = false;
            return false;
        }

        $scope.maxDate = new Date(3000, 0, 1, 0, 0, 0);
        $scope.minDate = new Date();
        $scope.startDateChanged = function (starDate) {
            $scope.minDate = new Date(starDate);
        };

        $scope.endDateChanged = function (endDate) {
            $scope.maxDate = new Date(endDate);
        };

        //Send Data To Memo, Cash Advance and Petty Cash.
        $scope.Memo = function (id) {
            idProposalService.set(id);
            $location.path('/memo/create');
        }

        $scope.cashAdvance = function (id) {
            idProposalService.set(id);
            $location.path('/cashadvance/create/');
        }

        $scope.PettyCash = function (id) {
            idProposalService.set(id);
            $location.path('/pettycash/create/');
        }

        var checkCreateDocPermission = function (data) {
            var auth = false;
            if ($scope.form.Remainning !== 0) {
                if ($scope.form && $scope.form.CheckCreateDocStatus) {
                    if (data.UnitCode && $scope.form.UnitCodeForEmployee) {
                        var unitCodeForEmployee = $scope.form.UnitCodeForEmployee.filter(function (r) {
                            return r.UnitCode.UnitCode === data.UnitCode.UnitCode;
                        });
                        if (unitCodeForEmployee && unitCodeForEmployee.length >= 1)
                            auth = true;
                    }
                }
            }
            return auth;
        }

        var _templateCreateDoc = function (data) {
            if (checkCreateDocPermission(data)) {
                return '<div class="btn-group" >' +
                    '<button type="button" class="btn btn-default dropdown-toggle"' +
                    'data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' +
                    'Create Document<span class="caret"></span></button>' +
                    '<ul class="dropdown-menu">' +
                    '<li><a href="#" ng-click="Memo(' + data.Id + ')">Memo</a></li>' +
                    '<li><a href="#" ng-click="PettyCash(' + data.Id + ')">Petty Cash</a></li>' +
                    '<li><a href="#" ng-click="cashAdvance(' + data.Id + ')">Cash Advance</a></li>' +
                    '<li><a href="#">M-Procurement</a></li></ul></div>';
            }

            return '';
        }

        var isSave = false;
        var setAllocateGridDataSource = function (d) {
            $scope.form.ProposalLine = new kendo.data.ObservableArray(d);
            if (_allocateGridOption) {
                _allocateGridOption.dataSource.data($scope.form.ProposalLine);
            }
        }
        var setDepositGridDataSource = function (d) {
            $scope.form.DepositLines = new kendo.data.ObservableArray(d);
            if (_depositGridOption) {
                _depositGridOption.dataSource.data($scope.form.DepositLines);
            }
        }
        var setIncomeDepositGridDataSource = function (d) {
            $scope.form.IncomeDeposit = new kendo.data.ObservableArray(d);
            if (_incomeDepositGridOption) {
                _incomeDepositGridOption.dataSource.data($scope.form.IncomeDeposit);
            }
            if (_incomeDepositReviseGridOption) {
                _incomeDepositReviseGridOption.dataSource.data($scope.form.IncomeDeposit);
            }
        }
        var setLogProposalTrackChangesGridDataSource = function (d) {
            $scope.form.LogProposalTrackChanges = new kendo.data.ObservableArray(d);
            if (_logTrackChangeGridOption) {
                _logTrackChangeGridOption.dataSource.data($scope.form.LogProposalTrackChanges);
            }
        }
        //Check that user push "Get final approve" or not.
        var checkFinalApproverButton = false;

        //////Approver
        var readonly;
        if ($routeParams.mode.toUpperCase() !== "CREATE"
            && $routeParams.mode.toUpperCase() !== "REVISE"
            && $routeParams.mode.toUpperCase() !== "REVISEPROPOSAL") {
            readonly = true;
            $scope.approverLevelFiveAndSix = true;
        } else {
            readonly = false;
            $scope.approverLevelFiveAndSix = false;
        }
        //Check add data
        var approverGridOptionLevelFourDataCheck = false;
        //Get Final Approver
        $scope.getFinalApproverData = function () {
            if ($routeParams.mode.toUpperCase() !== "CREATE" && $routeParams.mode.toUpperCase() !== "REVISE") {
                return false;
            }
            if (!$scope.form.Budget) {
                $scope.form.Budget = 0;
            }
            var id = 0;
            if ($routeParams.id !== undefined) {
                id = $routeParams.id;
            }
            if ($scope.form.UnitCode) {
                checkFinalApproverButton = true;
                dataSource.finalApprove({ budget: $scope.form.Budget, id: id, unitcode: $scope.form.UnitCode.UnitCode }, function (data) {
                    if (data && data.Employee) {
                        approverGridOptionLevelFourDataCheck = true;
                        var approverFinal = {
                            Employee: data.Employee,
                            ApproverSequence: data.ApproverSequence,
                            ApproverLevel: data.ApproverLevel,
                            ApproverUserName: data.ApproverUserName,
                            ApproverEmail: data.ApproverEmail,
                            Position: data.Position,
                            Department: data.Department,
                            DueDate: data.DueDate,
                            LineNo: 1,
                            LockEditable: 1
                        };
                        //console.log(approverFinal.Deleted);
                        // $scope.ApproverGridOptionLevelFourData.push(approverFinal);
                        $scope.ApproverGridOptionLevelFourData = [];
                        $scope.ApproverGridOptionLevelFourData.push(approverFinal);
                        //$scope.ApproverGridOptionLevelFour.dataSource.data($scope.ApproverGridOptionLevelFourData);
                        //$scope.ApproverGridOptionLevelFourData = $scope.ApproverGridOptionLevelFour.dataSource.data();
                    } else {
                        messageBox.error("ไม่พบผู้อนุมัติคนสุดท้าย กรุณาติดต่อผู้ดูและระบบ");
                        $scope.ApproverGridOptionLevelFourData = new kendo.data.ObservableArray([]);
                        //$scope.ApproverGridOptionLevelFour.dataSource.data($scope.ApproverGridOptionLevelFourData);
                        //$scope.ApproverGridOptionLevelFourData = $scope.ApproverGridOptionLevelFour.dataSource.data();
                    }
                    $scope.ApproverGridOptionLevelFour.dataSource.data($scope.ApproverGridOptionLevelFourData);
                    $scope.ApproverGridOptionLevelFourData = $scope.ApproverGridOptionLevelFour.dataSource.data();
                });
            } else {
                messageBox.error("Please select unit code!!!");
            }
        }
        //Merge All Approver to One Array before save
        var mergeArrayData = function (data, array) {
            angular.forEach(array, function (value, key) {
                data.push(value);
            });
            return data;
        }

        //Set approver grid data source on load data process
        var setApprovalGridDataSource = function (key, lv, data) {
            var approverData = data.filter(function (result) {
                return result.ApproverLevel === lv;
            });
            if (lv === 'เสนอ') {
                if (approverData.length === 0) {
                    $scope.ApproverGridOptionLevelOneData = new kendo.data.ObservableArray([]);
                } else {
                    $scope.ApproverGridOptionLevelOneData = [];
                    angular.forEach(approverData, function (value, key) {
                        $scope.ApproverGridOptionLevelOneData.push(value);
                    });
                }
                if (!isSave) {
                    var gridLevelOne = approverGrids.ApproverGridOption(readonly, $scope.ApproverGridOptionLevelOneData, lv);
                    $scope.ApproverGridOptionLevelOne = gridLevelOne;
                }
                $scope.ApproverGridOptionLevelOne.dataSource.data($scope.ApproverGridOptionLevelOneData);
                $scope.ApproverGridOptionLevelOneData = $scope.ApproverGridOptionLevelOne.dataSource.data();
            } else if (lv === 'เห็นชอบ') {
                if (approverData.length === 0) {
                    $scope.ApproverGridOptionLevelTwoData = new kendo.data.ObservableArray([]);
                } else {
                    $scope.ApproverGridOptionLevelTwoData = [];
                    angular.forEach(approverData, function (value, key) {
                        $scope.ApproverGridOptionLevelTwoData.push(value);
                    });
                }
                if (!isSave) {
                    var gridLevelTwo = approverGrids.ApproverGridOption(readonly, $scope.ApproverGridOptionLevelTwoData, lv);
                    $scope.ApproverGridOptionLevelTwo = gridLevelTwo;
                }
                $scope.ApproverGridOptionLevelTwo.dataSource.data($scope.ApproverGridOptionLevelTwoData);
                $scope.ApproverGridOptionLevelTwoData = $scope.ApproverGridOptionLevelTwo.dataSource.data();
            } else if (lv === 'อนุมัติ') {
                if (approverData.length === 0) {
                    $scope.ApproverGridOptionLevelThreeData = new kendo.data.ObservableArray([]);
                } else {
                    $scope.ApproverGridOptionLevelThreeData = [];
                    angular.forEach(approverData, function (value, key) {
                        $scope.ApproverGridOptionLevelThreeData.push(value);
                    });
                }
                if (!isSave) {
                    var gridLevelThree = approverGrids.ApproverGridOption(readonly, $scope.ApproverGridOptionLevelThreeData, lv);
                    $scope.ApproverGridOptionLevelThree = gridLevelThree;
                }
                $scope.ApproverGridOptionLevelThree.dataSource.data($scope.ApproverGridOptionLevelThreeData);
                $scope.ApproverGridOptionLevelThreeData = $scope.ApproverGridOptionLevelThree.dataSource.data();
            } else if (lv === 'อนุมัติขั้นสุดท้าย') {
                approverGridOptionLevelFourDataCheck = true;
                if (approverData.length === 0) {
                    checkFinalApproverButton = false;
                    $scope.ApproverGridOptionLevelFourData = new kendo.data.ObservableArray([]);
                } else {
                    checkFinalApproverButton = true;
                    $scope.ApproverGridOptionLevelFourData = [];
                    angular.forEach(approverData, function (value, key) {
                        $scope.ApproverGridOptionLevelFourData.push(value);
                    });
                }
                if (!isSave) {
                    var gridLevelFour = approverGrids.ApproverGridOptionLevelFour(readonly, $scope.ApproverGridOptionLevelFourData, true);
                    $scope.ApproverGridOptionLevelFour = gridLevelFour;
                }
                $scope.ApproverGridOptionLevelFour.dataSource.data($scope.ApproverGridOptionLevelFourData);
                $scope.ApproverGridOptionLevelFourData = $scope.ApproverGridOptionLevelFour.dataSource.data();
            } else if (lv === 'ทราบ') {
                if (approverData.length === 0) {
                    $scope.ApproverGridOptionLevelFiveData = new kendo.data.ObservableArray([]);
                } else {
                    $scope.ApproverGridOptionLevelFiveData = [];
                    $scope.forms.ApproverGridOptionLevelFiveData = [];
                    angular.forEach(approverData, function (value, key) {
                        $scope.forms.ApproverGridOptionLevelFiveData.push(value.Employee);
                        $scope.ApproverGridOptionLevelFiveData.push(value);
                    });
                }
            } else if (lv === 'สำเนาเรียน') {
                if (approverData.length === 0) {
                    $scope.ApproverGridOptionLevelSixData = new kendo.data.ObservableArray([]);
                } else {
                    $scope.ApproverGridOptionLevelSixData = [];
                    $scope.forms.ApproverGridOptionLevelSixData = [];
                    angular.forEach(approverData, function (value, key) {
                        $scope.ApproverGridOptionLevelSixData.push(value);
                        $scope.forms.ApproverGridOptionLevelSixData.push(value.Employee);
                    });
                }
            }
        }

        //Set approver level five and six data
        var setApproverLevelFiveAndSixData = function (data, lv) {
            var count = 0;
            if ($routeParams.id !== undefined || sn) {
                if (!data) {
                    if (lv === "ทราบ") {
                        angular.forEach($scope.ApproverGridOptionLevelFiveData, function (value) {
                            value.Deleted = true;
                        });
                        return false;
                    } else if (lv === "สำเนาเรียน") {
                        angular.forEach($scope.ApproverGridOptionLevelSixData, function (value) {
                            value.Deleted = true;
                        });
                        return false;
                    }
                }
                if (data.length > 0) {
                    if (lv === "ทราบ") {
                        angular.forEach($scope.ApproverGridOptionLevelFiveData, function (value) {
                            value.Deleted = true;
                        });
                        angular.forEach(data, function (value) {
                            count++;
                            var approverData = {
                                LineNo: count,
                                ApproverSequence: count,
                                ApproverLevel: lv,
                                DueDate: new Date(),
                                Position: value.PositionName,
                                ApproverEmail: value.Email,
                                Department: value.DepartmentName,
                                Employee: value,
                                ApproverUserName: value.Username
                            }
                            $scope.ApproverGridOptionLevelFiveData.push(approverData);
                        });
                    } else if (lv === "สำเนาเรียน") {
                        angular.forEach($scope.ApproverGridOptionLevelSixData, function (value) {
                            value.Deleted = true;
                        });
                        angular.forEach(data, function (value) {
                            count++;
                            var approverData = {
                                LineNo: count,
                                ApproverSequence: count,
                                ApproverLevel: lv,
                                DueDate: new Date(),
                                Position: value.PositionName,
                                ApproverEmail: value.Email,
                                Department: value.DepartmentName,
                                Employee: value,
                                ApproverUserName: value.Username
                            }
                            $scope.ApproverGridOptionLevelSixData.push(approverData);
                        });
                    }
                }
            } else {
                if (!data) {
                    return false;
                }
                if (data.length > 0) {
                    angular.forEach(data, function (value) {
                        count++;
                        var approverData = {
                            LineNo: count,
                            ApproverSequence: count,
                            ApproverLevel: lv,
                            DueDate: new Date(),
                            Position: value.PositionName,
                            ApproverEmail: value.Email,
                            Department: value.DepartmentName,
                            Employee: value,
                            ApproverUserName: value.Username
                        }
                        if (lv === "ทราบ") {
                            $scope.ApproverGridOptionLevelFiveData.push(approverData);
                        } else if (lv === "สำเนาเรียน") {
                            $scope.ApproverGridOptionLevelSixData.push(approverData);
                        }
                    });
                }
            }
        }

        var setIncomeOtherGridDataSource = function (d) {
            $scope.form.IncomeOther = new kendo.data.ObservableArray(d);
            if (_incomeOtherGridOption) {
                _incomeOtherGridOption.dataSource.data($scope.form.IncomeOther);
            }
            if (_incomeOtherGridOption2) {
                _incomeOtherGridOption2.dataSource.data($scope.form.IncomeOther);
            }
        }
        var setIncomeTotalSaleGridDataSource = function (d) {
            $scope.form.IncomeTotalSale = new kendo.data.ObservableArray(d);
            if (_totalSaleTargetGridOption) {
                _totalSaleTargetGridOption.dataSource.data($scope.form.IncomeTotalSale);
            }
        }
        var setExpenseTopicGridDataSource = function (d) {
            $scope.expenseTopicTemplate = new kendo.data.ObservableArray(d);
            if (_expenseGrid) {
                _expenseGrid.dataSource.data($scope.expenseTopicTemplate);
            }
        }
        var setSharedBranchTemplates = function (d, addTemplate) {
            if ($scope.SharedBranch && d) {
                for (var i = 0; i < $scope.SharedBranch.length; i++) {
                    var templateLine = d.filter(function (data) {
                        return data.BranchCode === $scope.SharedBranch[i].BranchCode;
                    });
                    if (templateLine && templateLine.length > 0) {
                        $scope.SharedBranch[i].Selected = templateLine[0].Selected;
                    }
                }
                if (addTemplate)
                    $scope.addFromTemplate();
            }
        }
        if ($routeParams.id != undefined) {
            $http.get(angular.crs.url.webApi("proposal/reviseproposal") + '?id=' + $routeParams.id).then(function (result) {
                if (result.data === true) {
                    remoteResult = dataSource.get({ id: $routeParams.id }, function (data) {
                        $scope.form = remoteResult;
                        $scope.checkDeposit();
                        setDepositGridDataSource(remoteResult.DepositLines);
                        setIncomeDepositGridDataSource(remoteResult.IncomeDeposit);
                        setIncomeOtherGridDataSource(remoteResult.IncomeOther);
                        setIncomeTotalSaleGridDataSource(remoteResult.IncomeTotalSale);
                        setAllocateGridDataSource(remoteResult.ProposalLine);
                        $scope.SharedBranch = remoteResult.SharedBranchTemplate;
                        setSharedBranchTemplates(remoteResult.SharedBranchTemplateLines, true);
                        setLogProposalTrackChangesGridDataSource(remoteResult.LogProposalTrackChanges);

                        if (!$scope.form.CCEmail) {
                            $scope.form.CCEmail = [];
                        }
                        if (!$scope.form.InformEmail) {
                            $scope.form.InformEmail = [];
                        }
                        //$scope.form.ProposalApproval = new kendo.data.ObservableArray(remoteResult.ProposalApproval);
                        //New Approve
                        for (var j = 0; j < $scope.ApproverLevel.length; j++) {
                            setApprovalGridDataSource(j, $scope.ApproverLevel[j], $scope.form.ProposalApproval);
                        }

                        $scope.uploadHandler.setId(remoteResult.Id);
                        $scope.dataTemplate = $scope.SharedBranch.filter(function (data) {
                            return data.Selected === true;
                        });
                        //Set date Propose
                        $scope.form.DueDatePropose = new Date($scope.form.DueDatePropose);
                        $scope.form.DueDatePropose.setHours(7);
                        //Set date Accept
                        $scope.form.DueDateAccept = new Date($scope.form.DueDateAccept);
                        $scope.form.DueDateAccept.setHours(7);
                        //Set date Approve
                        $scope.form.DueDateApprove = new Date($scope.form.DueDateApprove);
                        $scope.form.DueDateApprove.setHours(7);
                        //Set date Final Approve
                        $scope.form.DueDateFinalApprove = new Date($scope.form.DueDateFinalApprove);
                        $scope.form.DueDateFinalApprove.setHours(7);
                    }, k2.onSnError);
                } else {
                    messageBox.error("ไม่สามารถ Revise Proposal ได้");
                    $location.path('proposal/index');
                }
            }, function (response) {
                messageBox.extractError(response);
            });
        } else if (sn) {
            //$http.get(angular.crs.url.webApi("proposal/reviseproposal") + '?id=' + $routeParams.id).then(function (result) {
            //    if (result.data === true) {
            remoteResult = dataSource.worklist({ sn: sn }, function () {
                $scope.form = remoteResult;
                $scope.checkDeposit();
                setDepositGridDataSource(remoteResult.DepositLines);
                setIncomeDepositGridDataSource(remoteResult.IncomeDeposit);
                setIncomeOtherGridDataSource(remoteResult.IncomeOther);
                setIncomeTotalSaleGridDataSource(remoteResult.IncomeTotalSale);
                setAllocateGridDataSource(remoteResult.ProposalLine);
                $scope.SharedBranch = remoteResult.SharedBranchTemplate;
                setSharedBranchTemplates(remoteResult.SharedBranchTemplateLines, true);
                //New Approve
                //   $scope.form.ProposalApproval = new kendo.data.ObservableArray(remoteResult.ProposalApproval);
                for (var j = 0; j < $scope.ApproverLevel.length; j++) {
                    setApprovalGridDataSource(j, $scope.ApproverLevel[j], $scope.form.ProposalApproval);
                }
                if (!$scope.form.CCEmail) {
                    $scope.form.CCEmail = [];
                }
                if (!$scope.form.InformEmail) {
                    $scope.form.InformEmail = [];
                }
                setLogProposalTrackChangesGridDataSource(remoteResult.LogProposalTrackChanges);
                $scope.uploadHandler.setId(remoteResult.Id).setSN(sn);
                $scope.dataTemplate = $scope.SharedBranch.filter(function (data) {
                    return data.Selected === true;
                });
                //Set date Propose
                $scope.form.DueDatePropose = new Date($scope.form.DueDatePropose);
                $scope.form.DueDatePropose.setHours(7);
                //Set date Accept
                $scope.form.DueDateAccept = new Date($scope.form.DueDateAccept);
                $scope.form.DueDateAccept.setHours(7);
                //Set date Approve
                $scope.form.DueDateApprove = new Date($scope.form.DueDateApprove);
                $scope.form.DueDateApprove.setHours(7);
                //Set date Final Approve
                $scope.form.DueDateFinalApprove = new Date($scope.form.DueDateFinalApprove);
                $scope.form.DueDateFinalApprove.setHours(7);
            }, k2.onSnError);
            //    } 
            //else {
            //        messageBox.error("ไม่สามารถ Revise Proposal ได้");
            //        $location.path('proposal/index');
            //    }
            //}, function (response) {
            //    messageBox.extractError(response);
            //});

        } else {
            messageBox.error("ไม่สามารถ Revise Proposal ได้ เนื่องจากไม่พบข้อมูล Proposal");
            $location.path('proposal/index');
        }

        $scope.expenseTopicTemplate = expensetopicDataSource.init(function (response) {
            $scope.expenseTopicTemplate = response;
            setExpenseTopicGridDataSource(response);
        }, function (response) {
            $scope.messageBox.extractError(response);
        });

        $scope.employeeOption = {
            dataSource: {
                transport: {
                    serverFiltering: true,
                    read: {
                        dataType: "json", //instead of "type: 'jsonp',"
                        url: angular.crs.url.webApi('employee/search')
                    }
                }
            },
            optionLabel: "please select",
            dataTextField: 'FullName',
            dataValueField: 'FullName',
            template: '#=data.FullName#',
            valueTemplate: '#=data.FullName#',
            //options.template = '#=data.UnitCode# - #=data.UnitName#';
            //options.valueTemplate = '#=data.UnitCode# - #=data.UnitName#';
            filter: "contains"
        }

        $scope.expenseTopicOption = controlOptions.popupSearch('expensetopic');
        $scope.expenseTopicOptionDw = controlOptions.dropdown('expensetopic');
        $scope.apCodeOption = controlOptions.dropdown('apcode');
        $scope.requesterOption = controlOptions.popupSearch('employee');
        $scope.accountingOption = controlOptions.popupSearch('accounting', { multiple: true });
        $scope.employeePopupSearchOption = controlOptions.popupSearch('employee', { multiple: true });
        $scope.depositNumberOption = controlOptions.popupSearch('depositnumber');

        //$scope.proposalTypeOption = controlOptions.dropdown('typeproposal');
        $scope.proposalTypeOption = {
            dataSource: {
                type: "json",
                transport: {
                    serverFiltering: true,
                    read: {
                        url: angular.crs.url.webApi('typeproposal')
                    }
                }
            },
            dataTextField: "TypeProposalName",
            dataValueField: "TypeProposalCode",
            optionLabel: {
                TypeProposalCode: '',
                TypeProposalName: ''
            }
        }

        $scope.themeOption = {
            dataSource: {
                type: "json",
                transport: {
                    serverFiltering: true,
                    read: {
                        url: angular.crs.url.webApi('theme'),
                        cache: false
                    }
                },
                sort: {
                    field: 'Order',
                    dir: 'asc'
                }
            },
            dataTextField: "ThemesName",
            dataValueField: "ThemesCode",
            template: '#=data.ThemesName#',
            valueTemplate: '#=data.ThemesName#',
            //filter: "contains",
        }

        $scope.themeOptionChange = function () {
            if ($scope.form.Theme.ThemesCode === 'Other') {
                $scope.checkOtherTheme = true;
            } else {
                $scope.checkOtherTheme = false;
                $scope.form.ThemeNewName = '';
            }
        }

        $scope.unitcodeOption = controlOptions.dropdown('unitcode');
        $scope.unitcodeOptionToAP = controlOptions.dropdown('unitcode');
        $scope.unitcodeOptionNew = {
            dataSource: {
                type: "json",
                data: (angular.copy(authService.authentication.me)) ? angular.copy(authService.authentication.me).UnitCode : []
            },
            optionLabel: "please select",
            dataTextField: 'UnitName',
            dataValueField: 'UnitCode',
            template: '#=data.UnitName#',
            valueTemplate: '#=data.UnitName#',
            filter: "contains"
        }
        if ((angular.copy(authService.authentication.me)) && angular.copy(authService.authentication.me).UnitCode.length === 1) {

            $scope.form.UnitCode = angular.copy(authService.authentication.me).UnitCode[0];
        }

        $scope.processOption = controlOptions.dropdown('processdeposit');
        $scope.IncomeDepositNumberOption = controlOptions.popupSearch('incomedepositnumber');
        $scope.apCodeOptions = function (text) {
            var listAPcode = $filter('filter')($scope.expenseTopicTemplate, { ExpenseTopicCode: text });
            return {
                dataSource: { data: (listAPcode !== null) ? listAPcode[0].APCode : [] },
                dataTextField: "APCode",
                dataValueField: "APName",
            }
        }
        $scope.show = false;
        $scope.chkDeposit = false;
        $scope.checkDepositSource = false;
        $scope.checkDepositProposalRef = false;
        $scope.checkDeposit = function () {
            if ($scope.form.ProposalType !== undefined) {
                if ($scope.form.ProposalType.TypeProposalCode === "DEPOSIT" || $scope.form.ProposalType.TypeProposalCode === "DEPOSITIN") {
                    $scope.show = true;
                    $scope.form.ListPlace = [];
                    $scope.form.SharedBranchTemplateLines = new kendo.data.ObservableArray([]);
                    $scope.form.Other = null;
                    $scope.form.IncomeDeposit = new kendo.data.ObservableArray([]);
                    $scope.form.IncomeOther = new kendo.data.ObservableArray([]);
                    $scope.form.IncomeTotalSale = new kendo.data.ObservableArray([]);
                    $scope.form.ProposalLine = new kendo.data.ObservableArray([]);
                    $scope.form.SharedBranchTemplateLines = new kendo.data.ObservableArray([]);
                    setIncomeDepositGridDataSource($scope.form.IncomeDeposit);
                    setIncomeOtherGridDataSource($scope.form.IncomeOther);
                    setIncomeTotalSaleGridDataSource($scope.form.IncomeTotalSale);
                    setAllocateGridDataSource($scope.form.ProposalLine);
                    setSharedBranchTemplates($scope.form.SharedBranchTemplateLines, true);
                    if ($scope.form.ProposalType.TypeProposalName === "Deposit") {
                        //////Ken
                        $scope.form.DepositNumber = null;//new kendo.data.ObservableArray([]);
                        $scope.form.Approver = [];
                        $scope.chkDeposit = true;
                        $scope.checkDepositSource = true;
                        $scope.checkDepositProposalRef = false;
                    } else {
                        $scope.form.Source = null;
                        $scope.chkDeposit = false;
                        $scope.checkDepositSource = false;
                        $scope.checkDepositProposalRef = true;
                    }
                } else {
                    $scope.show = false;
                    $scope.form.Source = null;
                    $scope.form.DepositNumber = null;
                    $scope.form.Approver = [];
                    $scope.form.DepositLines = new kendo.data.ObservableArray([]);
                    setDepositGridDataSource($scope.form.DepositLines);
                    //ken
                    if ($scope.form.ProposalType.TypeProposalCode === "AP" || $scope.form.ProposalType.TypeProposalCode === "SPECIAL") {
                        $scope.form.CheckBudget = true;
                        $scope.checkDepositSource = false;
                        $scope.checkDepositProposalRef = false;
                    } else {
                        if ($scope.form.ProposalType.TypeProposalCode === "PREOP" || $scope.form.ProposalType.TypeProposalCode === "UNBUDGET") {
                            $scope.form.CheckBudget = false;
                            $scope.checkDepositSource = false;
                            $scope.checkDepositProposalRef = false;
                        }
                    }
                }
            }
        }

        //////Ken
        var checkInputs = function (elements) {
            elements.each(function () {
                var element = $(this);
                var input = element.children("input");
                input.prop("checked", element.hasClass("k-state-selected"));
            });
        };

        $scope.placeOptions = {
            dataTextField: "PlaceName",
            dataValueField: "PlaceCode",
            template: '#=data.PlaceCode# - #=data.PlaceName#',
            valueTemplate: '#=data.PlaceCode# - #=data.PlaceName#',
            autoBind: false,
            dataSource: {
                type: 'json',
                serverFiltering: true,
                transport: {
                    read: {
                        url: function () {
                            return angular.crs.url.webApi('place/getlist');
                        }
                    }
                }
            },
            itemTemplate: '<input type="checkbox" class="col-xs-2 col-sm-2 col-md-2 col-lg-2" style="padding:0px;"/>' +
                '<div class="col-xs-5 col-sm-5 col-md-7 col-lg-7">' +
                '#:data.PlaceCode# - #=data.PlaceName#' +
                '</div>',
            dataBound: function () {
                var items = this.ul.find("li");
                setTimeout(function () {
                    checkInputs(items);
                });
            },
            change: function () {
                var items = this.ul.find("li");
                checkInputs(items);
            },
            autoClose: false
        };

        $scope.departmentExceptionOption = {
            dataSource: new kendo.data.DataSource({
                type: "json",
                data: new kendo.data.ObservableArray([])
            }),
            dataTextField: "BranchCode",
            dataValueField: "BranchCode",
            autoBind: false,
        }
        //text editor
        $scope.trustAsHtml = function (string) {
            return $sce.trustAsHtml(string);
        };

        var originwidth = 0;
        var originheight = 0;
        $scope.ObjectiveEditor = {
            paste: function (e) {
                setTimeout(function () {
                    $("iframe").contents().find("div").attr("style", "max-width: 100%");
                    $("iframe").contents().find("img").attr("style", "max-width: 100%");
                }, 500);
            },
            tools: [
                "bold",
                "italic",
                "underline",
                "strikethrough",
                "justifyLeft",
                "justifyCenter",
                "justifyRight",
                "justifyFull",
                "insertUnorderedList",
                "insertOrderedList",
                "fontName",
                "fontSize",
                "foreColor",
                "backColor",
                "createTable",
                "print",
                {
                    name: "maximize",
                    tooltip: "Maximize",
                    exec: function (e) {

                        if (originheight == 0 || originwidth == 0) {
                            originheight = $("td.k-editable-area iframe.k-content").height();
                            originwidth = $("td.k-editable-area iframe.k-content").width();
                        }
                        var editor = $(this).data("kendoEditor");
                        editor.wrapper.css({
                            "z-index": 9999,
                            width: $(window).width(),
                            height: $(window).height(),
                            position: "fixed",
                            left: 0,
                            top: 0
                            //left: $("aside.main-sidebar .ng-scope").width(),
                            //top: $("nav.navbar.navbar-static-top").height()
                        });
                    }
                },
                {
                    name: "restore",
                    tooltip: "Restore",
                    exec: function (e) {
                        var editor = $(this).data("kendoEditor");
                        editor.wrapper.css({
                            width: originwidth,
                            height: originheight,
                            position: "relative"
                        });
                    }
                },
            ]
        }

        //Deposit Detail
        $scope.sumAmount2 = function () {
            if (!_depositGridOption ||
                !_depositGridOption.dataSource ||
                !_depositGridOption.dataSource._aggregateResult ||
                !_depositGridOption.dataSource._aggregateResult.Amount ||
                !_depositGridOption.dataSource._aggregateResult.Amount.sum) {
                $scope.TotalBudget = 0;
                return $scope.TotalBudget;
            }

            var total = _depositGridOption.dataSource._aggregateResult.Amount.sum;
            $scope.TotalBudget = total;
            if ($scope.TotalBudget !== 0) {
                $scope.form.TotalBudget = $scope.TotalBudget;
                $scope.form.Budget = $scope.TotalBudget;
            }

            return total;
        }

        $scope.DepositLinesGridOption = function () {
            _depositGridOption = lineGrid.gridOption({
                schema: {
                    model: kendo.data.Model.define({
                        id: "LineNo",
                        fields: {
                            LineNo: { type: "number" },
                            Amount: { type: "number", editable: $scope.AccInput, validation: { min: 1, max: 99999999999 } },
                            PaymentNo: {
                                validation: { required: true },
                                editable: $scope.AccInput
                            },
                            DatePeriod: {
                                validation: { required: true },
                                editable: $scope.AccInput
                            },
                            ProcessStatus: { editable: !$scope.AccInput }
                        }
                    })
                },
                data: $scope.form.DepositLines,
                aggregate: [
                    { field: "Amount", aggregate: "sum" },
                ],
                titel: "<center>Deposit detail</center>",
                columns: [
                    { field: "PaymentNo", title: "Payment no.", width: "200px", },
                    {
                        field: "DatePeriod",
                        title: "Date period",
                        width: "200px",
                        footerTemplate: "Total  Amount"
                    },
                    {
                        field: "Amount",
                        title: "Amount",
                        width: "200px",
                        template: lineGrid.formatCurrency('Amount'),
                        footerTemplate: "<div class='text-right'>{{sumAmount2() | currency:' ':2}}</div>",
                        // footerTemplate: "<div class='text-right'>#:(sum)? kendo.toString(sum,'n2'):0.00#</div>"

                    },
                    {
                        field: "ProcessStatus",
                        title: "Process status",
                        //template: $scope.processOption().inlineTemplate('ProcessStatus'),
                        editor: function (container, options) {
                            var editor = $('<select id="' + options.field + '" kendo-dropdownlist k-options="processOption()"  ng-model="dataItem.' + options.field + '"></select>');
                            editor.appendTo(container);
                            return editor;
                        },
                        width: "200px",
                    },
                ],
                editable: {
                    mode: "inline"
                },
                // readonly: $scope.isReadonlyDetails(),
                //noedit: true,
                //template: $('#_IncomeDeposit').html(),
                nocommand: $scope.showCommand,
                noadd: ($routeParams.mode.toUpperCase() === "CREATE" || $routeParams.mode.toUpperCase() === "REVISE") ? false : true,
                nodelete: ($routeParams.mode.toUpperCase() === "ACC-APPROVE") ? true : false,
                save: function (e) {
                },
                height: 400,
                pageable: false,
                groupable: true
            });
            return _depositGridOption;
        }

        //income Grid Income
        var procInst = (sn) ? sn.split('_') : null;
        $scope.reviseDeposit = true;
        if ($routeParams.mode.toUpperCase() === "REVISE-DEPOSIT" || $routeParams.mode.toUpperCase() === "REVISE" || $routeParams.mode.toUpperCase() === "CREATE" || $routeParams.mode.toUpperCase() === "REVISEPROPOSAL") {
            $scope.reviseDeposit = false;
        }
        $scope.approverReq = false;
        $scope.IncomeDepositGridOption = function () {
            _incomeDepositGridOption = lineGrid.gridOption({
                schema: {
                    model: kendo.data.Model.define({
                        id: "LineNo",
                        fields: {
                            LineNo: { type: "number" },
                            Budget: { type: "number", validation: { required: true, } },
                            DocumentNumber: {
                                validation: { required: true }
                            },
                            Approver: {
                                //validation: {
                                //    required: true
                                //},
                                defaultValue: []
                            }
                        }
                    })
                },
                data: $scope.form.IncomeDeposit,
                aggregate: [
                    { field: "Budget", aggregate: "sum" },
                ],
                columns: [
                    {
                        title: "<center>Income from  deposit proposal</center>",
                        columns: [
                            {
                                field: "DocumentNumber",
                                title: "Deposit proposal number ",
                                template: $scope.IncomeDepositNumberOption().inlineTemplate('DocumentNumber'),
                                editor: function (container, options) {

                                    var editor = $('<div kendo-popup-search k-ng-model="dataItem.' + options.field + '" k-options="IncomeDepositNumberOption()" ' +
                                        'k-on-change="getApprover(dataItem.' + options.field + ')" style="width:90%; height:20px;" required="required"></div>');
                                    editor.appendTo(container);
                                    return editor;
                                },
                                width: "250px",
                                footerTemplate: "Total income deposit"
                            },
                            //{ field: "Title", title: "Proposal Name", width: "200px", template: "#= (data && data.DocumentNumber) ? data.DocumentNumber.Title : '' #", },
                            {
                                field: "Budget",
                                title: "Budget request amount",
                                template: lineGrid.formatCurrency('Budget'),
                                width: "170px",
                                footerTemplate: "<div class='text-right'>#:(sum)? kendo.toString(sum,'n2'):0.00#</div>"
                            },
                            {
                                field: "Approver",
                                title: "Approver",
                                width: "250px",
                                template: '<div class="text-left" ng-repeat="approve in dataItem.Approver">{{approve.ApproverFullName}}</div>',
                                editor: function (container, options) {
                                    var model = options.model;

                                    $scope.approverReq = false;
                                    if (model.DocumentNumber) {
                                        $scope.depositApproverOption.dataSource.data(model.DocumentNumber.Approver);
                                        $scope.approverReq = true;
                                    }
                                    var editor = $('<select kendo-multi-select name="' + options.field + container.length + '" ' +
                                        'k-options="depositApproverOption" data-bind="value:' + options.field + '" k-ng-model="dataItem.' + options.field + '" style="width:90%;" ng-required="approverReq" ></select>');

                                    editor.appendTo(container);
                                    return editor;
                                }
                            },
                            {
                                field: "ActualCharge",
                                title: "Actual Charge",
                                width: "100px",
                                template: function (data) {
                                    if (data.ActualCharge) {
                                        return '<div class="text-center"><i class="fa fa-check" style="color:green;"></i></div>';
                                    } else
                                        return '<div class="text-center"><i class="fa fa-times" style="color:red;"></i></div>';
                                },
                                editor: '<center><input type="checkbox" class="checkbox" ng-model="dataItem.ActualCharge"/></center>'
                            },
                            {
                                field: "Remark", title: "Remark", width: "180px",
                                template: function (data) {
                                    if (data.Remark) {
                                        return "<div style='width:100%; word-wrap: break-word;'>" + data.Remark + "</div>";
                                    }

                                    return "";
                                },
                            },
                        ],
                    }
                ],
                // readonly: !$scope.reviseDeposit,
                editable: {
                    mode: "inline"
                },
                nocommand: $scope.reviseDeposit,
                noadd: $scope.reviseDeposit,
                height: ($scope.isReadonlyDetails()) ? 300 : 400,
                //template: $('#_IncomeDeposit').html(),
                edit: function (e) {
                },
                save: function (e) {
                    if (e.model.DocumentNumber && e.model.DocumentNumber.Balance) {
                        var list = $scope.form.IncomeDeposit.filter(function (datatemp) {
                            return datatemp.DocumentNumber.DocumentNumber === e.model.DocumentNumber.DocumentNumber &&
                                datatemp.LineNo !== e.model.LineNo;
                        });
                        if (list) {
                            var sumBudget = 0;
                            angular.forEach(list, function (tmp) {
                                if (tmp.Deleted === undefined || !tmp.Deleted) {
                                    sumBudget += tmp.Budget;
                                }
                                return sumBudget;
                            });
                            var newBalance = e.model.DocumentNumber.Balance - sumBudget;
                            if (e.model.Budget > newBalance) {
                                messageBox.error("Budget exceed: Statements remainning " + newBalance
                                    + " baht");
                                e.model.Budget = newBalance;
                                if (e.model.Budget === 0) {
                                    e.model.Deleted = true;
                                }
                            } else if (e.model.Budget === 0) {
                                messageBox.error("Statements  0 baht");
                                e.model.Deleted = true;
                            } else {
                                return e.model;
                            }
                        } else if (e.model.DocumentNumber.Balance === 0) {
                            messageBox.error("Budget exceed: Statements remainning 0 baht");
                            e.model.Deleted = true;
                        }
                    }
                },
                pageable: false,
                groupable: true
            });
            return _incomeDepositGridOption;
        }

        $scope.IncomeDepositReviseGridOption = function () {
            _incomeDepositGridOption = lineGrid.gridOption({
                schema: {
                    model: kendo.data.Model.define({
                        id: "LineNo",
                        fields: {
                            LineNo: { type: "number" },
                            Budget: { type: "number", },
                            Approver: { defaultValue: [] }
                        }
                    })
                },
                dataBound: function (e) {
                    var grid = this;
                    var gridData = grid.dataSource.view();
                    for (var i = 0; i < gridData.length; i++) {
                        var currentUid = gridData[i].uid;
                        if ($scope.reviseDeposit === false && gridData[i].StatusFlag === 1) {
                            var currentRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                            var editBtn = $(currentRow).find(".k-grid-edit");
                            var deleteBtn = $(currentRow).find(".k-grid-pseudo-delete");
                            editBtn.hide();
                            deleteBtn.hide();
                        }
                    }
                },
                data: $scope.form.IncomeDeposit,
                aggregate: [
                    { field: "Budget", aggregate: "sum" },
                ],
                columns: [
                    {
                        title: "<center>Income from  deposit proposal</center>",
                        columns: [
                            {
                                field: "DocumentNumber",
                                title: "Deposit proposal number ",
                                template: $scope.IncomeDepositNumberOption().inlineTemplate('DocumentNumber'),
                                editor: function (container, options) {
                                    var editor = $('<div kendo-popup-search k-ng-model="dataItem.' + options.field + '" k-options="IncomeDepositNumberOption()"  name="' + options.field + '" ' +
                                        'k-on-change="getApprover(dataItem.' + options.field + ')" style="width:90%; height:20px;"></div>');
                                    editor.appendTo(container);
                                    return editor;
                                },
                                width: "250px",
                                footerTemplate: "Total income deposit"
                            },
                            //{ field: "Title", title: "Proposal Name", width: "200px", template: "#= (data && data.DocumentNumber) ? data.DocumentNumber.Title : '' #", },
                            {
                                field: "Budget",
                                title: "Budget request amount",
                                template: lineGrid.formatCurrency('Budget'),
                                width: "170px",
                                footerTemplate: "<div class='text-right'>#:(sum)? kendo.toString(sum,'n2'):0.00#</div>"
                            },
                            {
                                field: "Approver",
                                title: "Approver",
                                width: "250px",
                                template: '<div class="text-left" ng-repeat="approve in dataItem.Approver">{{approve.ApproverFullName}}</div>',
                                editor: function (container, options) {
                                    var model = options.model;
                                    if (model.DocumentNumber) {
                                        $scope.depositApproverOption.dataSource.data(model.DocumentNumber.Approver);
                                    }
                                    var editor = $('<select kendo-multi-select id="' + options.field + '" ' +
                                        'k-options="depositApproverOption" data-bind="value:' + options.field + '" k-ng-model="dataItem.' + options.field + '" style="width:90%;" required="required"></select>');
                                    editor.appendTo(container);
                                    return editor;
                                }
                            },
                            {
                                field: "ActualCharge",
                                title: "Actual Charge",
                                width: "100px",
                                template: function (data) {
                                    if (data.ActualCharge) {
                                        return '<div class="text-center"><i class="fa fa-check" style="color:green;"></i></div>';
                                    } else
                                        return '<div class="text-center"><i class="fa fa-times" style="color:red;"></i></div>';
                                },
                                editor: '<center><input type="checkbox" class="checkbox" ng-model="dataItem.ActualCharge"/></center>'
                            },
                            {
                                field: "Remark", title: "Remark", width: "180px",
                                template: function (data) {
                                    if (data.Remark) {
                                        return "<div style='width:100%; word-wrap: break-word;'>" + data.Remark + "</div>";
                                    }
                                    return "";
                                },
                            },
                        ],
                    }
                ],
                // readonly: !$scope.reviseDeposit,
                editable: {
                    mode: "inline"
                },
                nocommand: $scope.reviseDeposit,
                noadd: $scope.reviseDeposit,
                height: ($scope.isReadonlyDetails()) ? 400 : 500,
                //template: $('#_IncomeDeposit').html(),
                edit: function (e) {
                    if (e.model.isNew()) {
                        if (procInst !== null) {
                            var newData = $filter('filter')($scope.form.IncomeDeposit, {
                                ProcInstId: procInst[0],
                                Status: "Cancelled"
                            });
                            if (newData.length > 0) {
                                e.model.DocumentNumber = newData[length - 1].DocumentNumber;
                                var newApprover = newData[length - 1].Approver;
                                if (e.model.DocumentNumber) {
                                    e.model.ProcInstId = newData[length - 1].ProcInstId;
                                    for (var i = 0; i < newApprover.Length; i++) {
                                        e.model.Approver[i] = newApprover[i];
                                        e.model.Approver[i].LineNo = i;
                                        e.model.Approver[i].Id = 0;
                                    }
                                    $scope.depositApproverOption.dataSource.data(new kendo.data.ObservableArray(e.model.DocumentNumber.Approver));
                                }
                            }
                        }
                    }
                },
                save: function (e) {
                    var oldData = $filter('filter')($scope.form.IncomeDeposit, {
                        DocumentNumber: e.model.DocumentNumber
                    });
                    if (!oldData) {
                        e.model.ProcInstId = null;
                    }
                },
                pageable: false,
                groupable: true
            });

            return _incomeDepositGridOption;
        }

        $scope.depositApproverOption = {
            dataTextField: "ApproverFullName",
            dataValueField: "ApproverUserName",
            template: '#=data.ApproverUserName#|#=data.ApproverFullName#',
            optionLabel: "please select",
            valueTemplate: '#=data.ApproverUserName#',
            autoBind: true,
            dataSource: new kendo.data.DataSource({
                type: "json",
                data: new kendo.data.ObservableArray([])
            }),
        };

        $scope.depositApproverOption.dataSource.data([]);
        $scope.getApprover = function (data) {
            if (data !== undefined && data.Approver !== null) {
                $scope.depositApproverOption.dataSource.data(data.Approver);
                $scope.approverReq = true;
            }
        }

        if ($routeParams.mode.toUpperCase() === "ACC-APPROVE") {
            $scope.AccInput = false;
            $scope.showCommand = false;
        } else if ($routeParams.mode.toUpperCase() === "CREATE" || $routeParams.mode.toUpperCase() === "REVISE") {
            $scope.AccInput = true;
            $scope.showCommand = false;
        } else {
            $scope.AccInput = true;
            $scope.showCommand = true;
        }

        var schemaIncomeOther = {
            model: kendo.data.Model.define({
                id: "LineNo",
                fields: {
                    LineNo: { type: "number" },
                    Budget: { type: "number", validation: { min: 1, max: 99999999999 } },
                    SponcorName: {
                        validation: { required: true }
                    },
                    ContactName: {
                        validation: { required: true }
                    },
                    ContactTel: {
                        validation: { required: true }
                    },
                    DueDate: {
                        type: "date"
                    },
                }
            })
        }

        $scope.IncomeOtherGridOption = function () {
            _incomeOtherGridOption = lineGrid.gridOption({
                schema: schemaIncomeOther,
                data: $scope.form.IncomeOther,
                aggregate: [
                    { field: "Budget", aggregate: "sum" },
                    { field: "Actual", aggregate: "sum" },
                ],
                columns: [
                    {
                        title: "<center>Income from other sources</center>",
                        columns: [
                            {
                                field: "SponcorName", title: "Source", width: "130px",
                                template: function (data) {
                                    if (data.SponcorName !== undefined || data.SponcorName !== null) {
                                        return "<div style='width:100%; word-wrap: break-word;'>" + data.SponcorName + "</div>";
                                    }
                                    return "";
                                },
                                editable: function (data) {
                                    if (data.SponcorName === "DO3")
                                        return true;
                                    return false;

                                }
                            },
                            {
                                field: "Description",
                                title: "Description",
                                width: "180px",
                                template: function (data) {
                                    if (data.Description) {
                                        return "<div style='width:100%; word-wrap: break-word;'>" + data.Description + "</div>";
                                    }
                                    return "";
                                },
                                footerTemplate: "Total Income other"
                            },
                            {
                                field: "Budget",
                                title: "Budget",
                                width: "100px",
                                template: lineGrid.formatCurrency('Budget'),
                                footerTemplate: "<div class='text-right'>#:(sum)? kendo.toString(sum,'n2'):0.00#</div>"
                            },
                            {
                                field: "DueDate", title: "Due date", width: "120px", template: lineGrid.formatDateTime('DueDate'),
                                editor: function (container, options) {
                                    var editor = $(' <input kendo-date-picker k-ng-model="dataItem.' + options.field + '" k-format="\'dd/MM/yyyy\'"  k-min="currentDate" />');
                                    editor.appendTo(container);
                                    return editor;
                                }
                            },
                            {
                                title: '<center>Contact (TMG)</center>',
                                columns:
                                [
                                    {
                                        field: "ContactName", title: "Name", width: "120px", editable: false,
                                        template: function (data) {
                                            if (data.ContactName) {
                                                return "<div style='width:100%; word-wrap: break-word;'>" + data.ContactName + "</div>";
                                            }
                                            return "";
                                        },
                                    },
                                    {
                                        field: "ContactTel", title: "Tel.", width: "120px", editable: false ,
                                        template: function (data) {
                                            if (data.ContactTel) {
                                                return "<div style='width:100%; word-wrap: break-word;'>" + data.ContactTel + "</div>";
                                            }
                                            return "";
                                        },
                                    },
                                    {
                                        field: "ContactEmail", title: "Email", width: "120px", editable: false,
                                        template: function (data) {
                                            if (data.ContactEmail) {
                                                return "<div style='width:100%; word-wrap: break-word;'>" + data.ContactEmail + "</div>";
                                            }
                                            return "";
                                        },
                                    },
                                    {
                                        field: "ContactDepartment", title: "Department", width: "150px", editable: false,
                                        template: function (data) {
                                            if (data.ContactDepartment) {
                                                return "<div style='width:100%; word-wrap: break-word;'>" + data.ContactDepartment + "</div>";
                                            }
                                            return "";
                                        },
                                    },
                                ]
                            },
                            {
                                field: "Remark", title: "Remark", width: "200px",
                                template: function (data) {
                                    if (data.Remark) {
                                        return "<div style='width:100%; word-wrap: break-word;'>" + data.Remark + "</div>";
                                    }
                                    return "";
                                },
                            },
                        ],
                    }
                ],
                editable: {
                    mode: "incell"
                },
                //comment no หมด
                 noedit:true,
                 nocommand: $scope.showCommand,
                 noadd: $scope.showCommand,
                // nodelete: ($routeParams.mode.toUpperCase() === "ACC-APPROVE") ? true : false,
                height: ($scope.isReadonlyDetails()) ? 400 : 500,
                //template: $('#_IncomeOther').html(),
                readonly: $scope.isReadonlyDetails(),
                save: function (e) {
                    //var keys = Object.keys(e.values);
                    //var key = keys[0];
                    //if (key) {
                    //    e.model[key] = e.values[key];
                    //}

                    //this.saveChanges();
                },
                pageable: false,
                groupable: true,
                dataBound: function (e) {
                    var grid = this;
                    var gridData = grid.dataSource.view();
                    for (var i = 0; i < gridData.length; i++) {
                        var currentUid = gridData[i].uid;
                        if (gridData[i].StatusFlag === 1) {
                            var currentRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                            var editBtn = $(currentRow).find(".k-grid-edit");
                            var deleteBtn = $(currentRow).find(".k-grid-pseudo-delete");
                            editBtn.hide();
                            deleteBtn.hide();
                        }
                    }
                },
                edit: function (e) {
                    if (e.model.StatusFlag === 1) {
                        e.sender.closeCell();
                    }                    
                }
            });

            return _incomeOtherGridOption;
        }

        var accountingInvoice = function (e) {
            var detailRow = e.detailRow;
            //if (!e.data.AccountTrans)
            //    e.data.AccountTrans = new kendo.data.ObservableArray([]);
            detailRow.find('.k-detail-cell').append("<div class='gridAcc' style='width:90%;'></div>");
            detailRow.find('.gridAcc').kendoGrid($scope.AccountGridObtion(e.data.AccountTrans));
        }

        var expandedRowUid;
        $scope.IncomeOtherGridOption2 = function () {
            _incomeOtherGridOption2 = lineGrid.gridOption({
                schema: {
                    model: kendo.data.Model.define({
                        id: "LineNo",
                        fields: {
                            LineNo: { type: "number" },
                            Budget: {
                                type: "number",
                                validation: { min: 1, max: 99999999999 },
                                editable: false
                            },
                            // Actual: { type: "number" },
                            SponcorName: { type: "string", editable: false },
                            Description: { editable: false },
                            DueDate: {
                                type: "date",
                                editable: false
                            },
                            Actual: { editable: false },
                            ContactTel: { editable: false },
                            ContactName: { editable: false },
                            ContactEmail: { editable: false },
                            ContactDepartment: { editable: false },
                            Remark: { editable: false },
                        }
                    })
                },
                dataBound: function (e) {
                    if (expandedRowUid) {
                        this.expandRow($('tr[data-uid=' + expandedRowUid + ']'));
                    }
                    var grid = this;
                    var gridData = grid.dataSource.view();
                    for (var i = 0; i < gridData.length; i++) {
                        var sumActual = 0;
                        $scope.AccountTrans = gridData[i].AccountTrans;
                        angular.forEach($scope.AccountTrans, function (tmp) {
                            return sumActual += tmp.Actual;
                        });
                        gridData[i].Actual = sumActual;
                    }
                },
                data: $scope.form.IncomeOther,
                aggregate: [
                    { field: "Budget", aggregate: "sum" },
                    { field: "Actual", aggregate: "sum" },
                ],
                columns: [
                    {
                        title: "<center>Income from other sources</center>",
                        columns: [
                             {
                                 field: "PostActual",
                                 title: "Post actual",
                                 width: "80px",
                                 template: function (data) {
                                     return '<center><input type="checkbox" class="checkbox" ng-model="dataItem.PostActual"/></center>';
                                 }
                             },
                            {
                                field: "SponcorName", title: "Source", width: "130px", editable: false,
                                template: function (data) {
                                    if (data.SponcorName) {
                                        return "<div style='width:100%; word-wrap: break-word;'>" + data.SponcorName + "</div>";
                                    }
                                    return "";
                                },
                            },
                            {
                                field: "Description",
                                title: "Description",
                                width: "180px",
                                editable: false,
                                template: function (data) {
                                    if (data.Description) {
                                        return "<div style='width:100%; word-wrap: break-word;'>" + data.Description + "</div>";
                                    }
                                    return "";
                                },
                                footerTemplate: "Total Income other"
                            },
                            {
                                field: "Budget",
                                title: "Budget",
                                width: "100px",
                                template: lineGrid.formatCurrency('Budget'), editable: false,
                                footerTemplate: "<div class='text-right'>#:(sum)? kendo.toString(sum,'n2'):0.00#</div>"
                            },
                            {
                                field: "Actual",
                                title: "Actual",
                                width: "100px",
                                template: lineGrid.formatCurrency('Actual'), editable: false,
                                footerTemplate: "<div class='text-right'>#:(sum)? kendo.toString(sum,'n2'):0.00#</div>"
                            },
                            {
                                field: "DueDate", title: "Due date", width: "120px", template: lineGrid.formatDateTime('DueDate'),
                                editable: false,
                                editor: function (container, options) {
                                    var editor = $(' <input crs-date-picker k-ng-model="dataItem.' + options.field + '"  />');
                                    editor.appendTo(container);
                                    return editor;
                                }
                            },
                            {
                                title: '<center>Contact (TMG)</center>',
                                columns:
                                [
                                    {
                                        field: "ContactName", title: "Name", width: "120px", editable: false,
                                        template: function (data) {
                                            if (data.ContactName) {
                                                return "<div style='width:100%; word-wrap: break-word;'>" + data.ContactName + "</div>";
                                            }
                                            return "";
                                        },
                                    },
                                    {
                                        field: "ContactTel", title: "Tel.", width: "120px", editable: false ,
                                        template: function (data) {
                                            if (data.ContactTel) {
                                                return "<div style='width:100%; word-wrap: break-word;'>" + data.ContactTel + "</div>";
                                            }
                                            return "";
                                        },
                                    },
                                    {
                                        field: "ContactEmail", title: "Email", width: "120px", editable: false,
                                        template: function (data) {
                                            if (data.ContactEmail) {
                                                return "<div style='width:100%; word-wrap: break-word;'>" + data.ContactEmail + "</div>";
                                            }
                                            return "";
                                        },
                                    },
                                    {
                                        field: "ContactDepartment", title: "Department", width: "150px", editable: false,
                                        template: function (data) {
                                            if (data.ContactDepartment) {
                                                return "<div style='width:100%; word-wrap: break-word;'>" + data.ContactDepartment + "</div>";
                                            }
                                            return "";
                                        },
                                    },
                                ]
                            },
                            { field: "Remark", title: "Remark", width: "200px", editable: false },
                        ],
                    }
                ],
                //comment no หมด
               noadd: true,
                //nodelete: true,
                nocommand: true,
                editable: {
                    mode: "incell"
                },
                edit: function (e) {
                },
                readonly: $scope.isReadonlyDetails(),
                save: function (e) {
                },
                pageable: false,
                groupable: true
            });

            _incomeOtherGridOption2.detailInit = accountingInvoice;
            _incomeOtherGridOption2.detailExpand = function (e) {
                expandedRowUid = e.masterRow.data('uid');
            };

            return _incomeOtherGridOption2;
        }

        var schemaAccountGridObtion = {
            model: kendo.data.Model.define({
                id: "LineNo",
                fields: {
                    LineNo: { type: "number" },
                    Actual: { type: "number", validation: { min: 1, max: 99999999999 }},
                    ActualDate: { type: "date" },
                    Remark: { type: "string" },
                    // IncomeOtherId: { defaultValue: data.Id }
                }
            })
        };

        $scope.AccountGridObtion = function (data) {
            if (!data)
                data = new kendo.data.ObservableArray([]);
            _accountGridObtion = lineGrid.gridOption({
                schema: schemaAccountGridObtion,
                data: data,
                columns: [
                    { field: "Source", title: "Source", width: "100px", },
                    { field: "Actual", title: "Actual", width: "100px", },
                    {
                        field: "ActualDate",
                        title: "Actual Date",
                        width: "80px",
                        template: lineGrid.formatDateTime('ActualDate'),
                        editor: function (container, options) {
                            var editor = $(' <input crs-date-picker k-ng-model="dataItem.' + options.field + '"  />');
                            editor.appendTo(container);
                            return editor;
                        }
                    },
                    { field: "Remark", title: "Remark", width: "200px", },
                ],
                // noedit: true,
                editable: {
                    mode: "popup"
                },
                //height: ($scope.isReadonlyDetails()) ? 300 : 300,
                template: $('#_AccountingEditTemplate').html(),
                save: function (e) {
                    // $scope.incomeOtherGrid.refresh();
                },
                pageable: false,
                groupable: true
            });

            return _accountGridObtion;
        }

        $scope.newRowTotalSale = function () {
            var result = {};
            angular.forEach($scope.SharedBranch, function (tmp) {
                var template = {};
                template.BranchCode = tmp.BranchCode;
                template.BranchName = tmp.BranchName;
                template.Actual = 0;
                result[tmp.BranchCode] = template;
            });
            return result;
        }

        $scope.TotalSaleTargetGridOption = function () {
            if (!$scope.form || !$scope.SharedBranch || $scope.SharedBranch.length === 0)
                return undefined;
            _totalSaleTargetGridOption = lineGrid.gridOption({
                data: $scope.form.IncomeTotalSale,
                schema: {
                    model: kendo.data.Model.define({
                        id: "LineNo",
                        fields: {
                            LineNo: { type: "number" },
                            Budget: { type: "number", editable: false },
                            ActualLastYear: { type: "number", },
                            // Description: { validation:{ required: true }
                            // },
                            PercentShared: { defaultValue: $scope.newRowTotalSale() }
                        }
                    })
                },
                aggregate: function () {
                    var agg = [
                        { field: "Budget", aggregate: "sum" },
                       { field: "ActualLastYear", aggregate: "sum" }
                    ];
                    for (var t = 0; t < $scope.SharedBranch.length; t++) {
                        if ($scope.SharedBranch[t]) {
                            agg.push({ field: 'PercentShared["' + $scope.SharedBranch[t].BranchCode + '"].Actual', aggregate: "sum" });
                        }
                    }
                    return agg;
                }(),
                //  toolbar: (!$scope.isReadonlyDetails()) ? '<a class="k-button btn-default" ng-click="AddTotalSaleNewRecord()">Add New Record</a>' : false,
                columns: [
                    {
                        title: "<center>Sales Target / Rental Fee</center>",
                        columns: [
                            {
                                field: "Description",
                                title: "Description",
                                width: "200px",
                                template: function (data) {
                                    if (data.Description) {
                                        return "<div style='width:100%; word-wrap: break-word;'>" + data.Description + "</div>";
                                    }
                                    return "";
                                },
                                footerTemplate: "Total"
                            },
                            {
                                field: "ActualLastYear",
                                title: "Actual (Last Year)",
                                width: "150px",
                                template: lineGrid.formatCurrency('ActualLastYear'),
                                footerTemplate: "<div class='text-right'>#:(sum)? kendo.toString(sum,'n2'):0.00#</div>"
                            },
                            {
                                field: "Budget",
                                title: "Total sales",
                                width: "150px",
                                template: lineGrid.formatCurrency('Budget'),
                                footerTemplate: "<div class='text-right'>#:(sum)? kendo.toString(sum,'n2'):0.00#</div>"
                            },
                            {
                                title: "<center>Shared</center>",
                                columns: function () {

                                    var arr = [];
                                    for (var t = 0; t < $scope.SharedBranch.length; t++) {
                                        if ($scope.SharedBranch[t]) {
                                            arr.push({
                                                field: 'PercentShared["' + $scope.SharedBranch[t].BranchCode + '"]',
                                                title: '<center>' + $scope.SharedBranch[t].BranchCode + '_' + $scope.SharedBranch[t].BranchName + '</center>',
                                                //width: 'auto',
                                                hidden: $scope.hiddenBranchColumn($scope.SharedBranch[t].BranchCode),
                                                columns: [
                                                    {
                                                        field: 'PercentShared["' + $scope.SharedBranch[t].BranchCode + '"].Actual',
                                                        title: "Estimate",
                                                        width: '150px',
                                                        template: lineGrid.formatCurrency("PercentShared['" + $scope.SharedBranch[t].BranchCode + "'].Actual"),
                                                        editor: function (container, options) {
                                                            var editor = $("<input price-text-box  type='number' style='width: 100%;'  k-ng-model='dataItem." + options.field + "' k-on-change='CalculateTotalSalesTarget(dataItem)' >");
                                                            editor.appendTo(container);
                                                            return editor;
                                                        },
                                                        footerTemplate:
                                                            "<div style='height:70px'>"
                                                                + "<div class='text-right'>Total : #= kendo.toString(sum,'n2') #</div>"
                                                                + "<div class='text-right'>A (%) : {{calcTotalAdvSalesByBranch(#= sum #,SharedBranch[" + t + "].BranchCode,'A') | currency:' ':2 }}</div>"
                                                                + "<div class='text-right'>P (%) : {{calcTotalAdvSalesByBranch(#= sum #,SharedBranch[" + t + "].BranchCode,'P') | currency:' ':2 }}</div>"
                                                                + "<div class='text-right'>A&P (%) : {{calcTotalAdvSalesByBranch(#= sum #,SharedBranch[" + t + "].BranchCode,'A&P') | currency:' ':2 }}</div>"
                                                                + "</div>"

                                                    }
                                                ]
                                            });
                                        }
                                    }
                                    return arr;
                                }()
                            },
                            {
                                field: "Remark", title: "Remark", width: "260px",
                                template: function (data) {
                                    if (data.Remark) {
                                        return "<div style='width:100%; word-wrap: break-word;'>" + data.Remark + "</div>";
                                    }
                                    return "";
                                },
                            },
                        ],
                    }
                ],
                readonly: true,
                editable: {
                    mode: "inline"
                },
               height: 500,
                //template: $('#_IncomeOther').html(),
               save: function (e) {
                   var keys = Object.keys(e.values);
                   var key = keys[0];
                   if (key) {
                       e.model[key] = e.values[key];
                   }
                    e.model.Budget = 0;
                    angular.forEach($scope.dataTemplate, function (tmps) {
                        e.model.Budget += e.model.PercentShared[tmps.BranchCode].Actual;
                    });
                    this.saveChanges();
                },
                pageable: false,
                groupable: true
            });
            return _totalSaleTargetGridOption;
        }

        $scope.calcTotalAdvSalesByBranch = function (sum, branch, type) {
            if (!sum)
                return 0;
            var trAmount = 0;
            var reAmount = 0;
            if (type === "A&P") {
                if (_allocateGridOption &&
                    _allocateGridOption.dataSource &&
                    _allocateGridOption.dataSource._aggregateResult &&
                    _allocateGridOption.dataSource._aggregateResult['PercentShared["' + branch + '"].RE_Amount'] &&
                    _allocateGridOption.dataSource._aggregateResult['PercentShared["' + branch + '"].RE_Amount'].sum) {
                    reAmount = _allocateGridOption.dataSource._aggregateResult['PercentShared["' + branch + '"].RE_Amount'].sum;
                }
                if (_allocateGridOption &&
                    _allocateGridOption.dataSource &&
                    _allocateGridOption.dataSource._aggregateResult &&
                    _allocateGridOption.dataSource._aggregateResult['PercentShared["' + branch + '"].TR_Amount'] &&
                    _allocateGridOption.dataSource._aggregateResult['PercentShared["' + branch + '"].TR_Amount'].sum) {
                    trAmount = _allocateGridOption.dataSource._aggregateResult['PercentShared["' + branch + '"].TR_Amount'].sum;
                }
            } else {
                var view = _allocateGridOption.dataSource.view();
                for (var i = 0; i <= view.length; i++) {
                    if (view[i] &&
                        view[i].value === type &&
                        view[i].aggregates['PercentShared["' + branch + '"].RE_Amount'] &&
                        view[i].aggregates['PercentShared["' + branch + '"].RE_Amount'].sum) {
                        reAmount = view[i].aggregates['PercentShared["' + branch + '"].RE_Amount'].sum;
                    }
                    if (view[i] &&
                        view[i].value === type &&
                        view[i].aggregates['PercentShared["' + branch + '"].TR_Amount'] &&
                        view[i].aggregates['PercentShared["' + branch + '"].TR_Amount'].sum) {
                        trAmount = view[i].aggregates['PercentShared["' + branch + '"].TR_Amount'].sum;
                    }
                }
            }

            return ((trAmount + reAmount) / sum) * 100;
        }

        $scope.CalculateTotalSalesTarget = function (dataItem) {
            dataItem.Budget = 0;
            angular.forEach($scope.dataTemplate, function (tmps) {
                dataItem.Budget += dataItem.PercentShared[tmps.BranchCode].Actual;
            });
            $scope.totalSaleTargetGridOption.saveChanges();
            $scope.totalSaleTargetGridOption.refresh();
        }

        //Allocate
        $scope.selecSharedTemplate = function () {
            $scope.SharedBranchWindow.open().center();
        }

        $scope.selecExpenseTemplate = function () {
            if ($scope.form.ProposalType && $scope.form.ProposalType.TypeProposalCode === "AP") {
                $scope.tempExpenseTopicTemplate = new kendo.data.ObservableArray([]);
                $scope.tempExpenseTopicTemplate = $scope.expenseTopicTemplate.filter(function (data) {
                    return data.TypeAP.toUpperCase() !== "L";
                });
                setExpenseTopicGridDataSource($scope.tempExpenseTopicTemplate);
            }
            $scope.expenseWindow.open().center();
        }

        $scope.AddNewRecord = function () {
            var newModel = new allocateschema.model();
            newModel.ExpenseTopic = { ExpenseTopicCode: "" };
            newModel.APCode = { APCode: "" };
            newModel.UnitCode = {
                UnitCode: "",
                UnitName: ""
            };
            $scope.newModel = newModel;
            $scope.detailWindow.open().center();
        }

        $scope.AddTotalSaleNewRecord = function () {
            $scope.totalsale = {};
            $scope.totalsaleWindow.open().center();
        }

        $scope.addFromTemplate = function () {
            if ($scope.SharedBranch && $scope.allocateGrid && $scope.totalSaleTargetGridOption) {
                for (var i = 0; i < $scope.SharedBranch.length ; i++) {
                    if ($scope.SharedBranch[i] && $scope.SharedBranch[i].Selected) {
                        $scope.allocateGrid.showColumn('PercentShared["' + $scope.SharedBranch[i].BranchCode + '"]');
                        $scope.totalSaleTargetGridOption.showColumn('PercentShared["' + $scope.SharedBranch[i].BranchCode + '"]');
                    } else {
                        $scope.allocateGrid.hideColumn('PercentShared["' + $scope.SharedBranch[i].BranchCode + '"]');
                        $scope.totalSaleTargetGridOption.hideColumn('PercentShared["' + $scope.SharedBranch[i].BranchCode + '"]');
                    }
                }

                //$scope.departmentExceptionOption.dataSource.data($filter('filter')($scope.SharedBranch, { Selected: true }));
                $scope.dataTemplate = $scope.SharedBranch.filter(function (data) {
                    return data.Selected === true;
                });
                $scope.hiddenColumn($scope.allocateGrid);
                if ($scope.form.ProposalLine) {
                    angular.forEach($scope.form.ProposalLine, function (tmp) {
                        $scope.calDifference(tmp);
                    });
                }
                $scope.allocateGrid.refresh();
                $scope.totalSaleTargetGridOption.refresh();
            }
            $scope.SharedBranchWindow.close();
        }

        $scope.setPercentSharedToGrid = function () {
            var PercentShared = {};
            angular.forEach($scope.SharedBranch, function (tmp) {
                var template = {};
                template.BranchCode = tmp.BranchCode;
                template.BranchName = tmp.BranchName;
                template.TR_Percent = 0;
                template.TR_Amount = 0;
                template.RE_Percent = 0;
                template.RE_Amount = 0;
                template.Selected = tmp.Selected;
                PercentShared[tmp.BranchCode] = template;

            });
            return PercentShared;
        }
        $scope.addRowToGrid = function () {
            blockUI.start({
                theme: true,
                baseZ: 20000
            });
            if ($scope.expenseTopic) {
                var tempExpense = $scope.expenseTopic;
                var n = 0;
                angular.forEach(tempExpense, function (val) {
                    angular.forEach(val.APCode, function (arr) {
                        var tempModel = new allocateschema.model();
                        tempModel.ExpenseTopicCode = val.ExpenseTopicCode;
                        tempModel.ExpenseTopicName = val.ExpenseTopicName;
                        tempModel.TypeAP = val.TypeAP;
                        tempModel.APCode = arr;
                        tempModel.Description = "";
                        tempModel.LineNo = lineGrid.nextLineNo($scope.form.ProposalLine);
                        tempModel.PercentShared = $scope.setPercentSharedToGrid();

                        $scope.form.ProposalLine.push(tempModel);
                    });
                    setAllocateGridDataSource($scope.form.ProposalLine);
                });
                blockUI.stop();
                $scope.expenseWindow.close();
                $scope.clearExpenseTopicTemplate();
            }
        }

        $scope.changeExpenseTopic = function (newModel) {
            newModel.APCode = { APCode: "" };
            newModel.UnitCode = {
                UnitCode: "",
                UnitName: ""
            };
            return newModel;
        }

        $scope.AddNewRecordToGrid = function (newModel) {        
            blockUI.start();
            if (newModel !== null && (newModel.ExpenseTopic.ExpenseTopicCode !== "" && newModel.APCode.APCode !== "")) {
                var tempModel = new allocateschema.model();
                tempModel.ExpenseTopicCode = newModel.ExpenseTopic.ExpenseTopicCode;
                tempModel.ExpenseTopicName = newModel.ExpenseTopic.ExpenseTopicName;
                tempModel.TypeAP = newModel.ExpenseTopic.TypeAP;
                tempModel.APCode = newModel.APCode;
                tempModel.UnitCode = newModel.UnitCode;
                tempModel.Description = newModel.Description;
                tempModel.Actual = newModel.Actual;
                tempModel.BudgetPlan = newModel.BudgetPlan;
                tempModel.LineNo = lineGrid.nextLineNo($scope.form.ProposalLine);
                tempModel.PercentShared = $scope.setPercentSharedToGrid();

                if (tempModel.APCode.APCode === "DO3") {
                    tempModel.BudgetPlan = newModel.BudgetPlan;
                    $scope.pushAllocateToIncomeOther(tempModel);
                    newModel.BudgetPlan = 0;
                }

                tempModel.BudgetPlan = newModel.BudgetPlan;
                tempModel = $scope.calDifference(tempModel);
                $scope.form.ProposalLine.push(tempModel);
                //$scope.allocateGrid.saveChanges();
                //$scope.allocateGrid.refresh();
                setAllocateGridDataSource($scope.form.ProposalLine);
                $scope.detailWindow.close();
            } else {
                messageBox.error("Please Input ExpenseTopic and AP Code !");
                // $scope.detailWindow.close();
            }
            blockUI.stop();
        }

        $scope.expenseTopic = new kendo.data.ObservableArray([]);
        $scope.selectRowToAllocateGrid = function (event, that) {
            var model = that.dataItem;
            var checked = angular.element(event.target).context.checked;
            if (checked) {
                //push
                $scope.expenseTopic.push(model);
            }
            else
            {
                $scope.expenseTopic.splice(model, 1);
            }
        }

        $scope.expenseGridOption = function () {
            _expenseGrid = lineGrid.gridOption({
                schema: {
                    model: kendo.data.Model.define({
                        id: "LineNo",
                        fields: {
                            LineNo: { type: "number" },
                        }
                    })
                },
                data: $scope.expenseTopicTemplate,
                columns: [
                    {
                        title: "Select",
                        template: '<center><input type="checkbox"  ng-click="selectRowToAllocateGrid($event,this)" name="Selected" /> </center>',
                        //template: '<center><input type="checkbox" id="{{data.ExpenseTopic.ExpenseTopicCode}}" class="k-checkbox"  ng-click="selectRowToAllocateGrid($event,this)" name="Selected" />' +
                        //     '<label class="k-checkbox-label" for="{{data..ExpenseTopic.ExpenseTopicCode}}"></label></center>',
                        width: "30px"
                    },
                    { field: "ExpenseTopicCode", title: "ExpenseTopic Code", width: "50px", },
                    { field: "ExpenseTopicName", title: "ExpenseTopic Name", width: "80px", },
                    { field: "Description", title: "Description", width: "180px", },
                    { field: 'TypeAP', title: 'TypeAP', width: '30px' },
                ],
                nocommand: true,
                noadd: true,
                selectable: "multiple, row",
                height: 550,
            });
            return _expenseGrid;
        }

        $scope.CheckDuplicate = function (dataItem) {
            var lineSame = $scope.form.ProposalLine.filter(function (data) {
                if (tempModel.APCode.APCode === "DO3") {
                    return data.APCode.APCode === tempModel.APCode.APCode && !data.Deleted;
                } else {
                    return data.ExpenseTopicCode === dataItem.ExpenseTopicCode
                        && data.APCode.APCode === dataItem.APCode.APCode
                        && data.UnitCode.UnitCode === dataItem.UnitCode.UnitCode
                        && !data.Deleted;
                }
            });
            if (lineSame.length > 1) {
                messageBox.error("รายการนี้มีอยู่แล้ว ไม่สามารถเพิ่มรายการได้");
                return dataItem.UnitCode.UnitCode = "";
            }
        }

        $scope.sharedBranchColor = function (data) {
            var result = "";
            if ($scope.dataTemplate) {
                for (var i = 0; i < $scope.dataTemplate.length; i++) {
                    if (data === $scope.dataTemplate[i].BranchCode) {
                        if ((i % 2) === 0) {
                            result = "light-color";
                            return result;
                        } else {
                            result = "dark-color";
                            return result;
                        }
                    }
                }
            }

            return result;
        }

        var allocateschema = {
            model: kendo.data.Model.define({
                id: "LineNo",
                fields: {
                    LineNo: { type: "number" },
                    ExpenseTopicCode: { type: "string", validation: { required: true } },
                    APCode: { validation: { required: true } },
                    BudgetPlan: { type: "number" },
                    Actual: { type: "number" },

                    Percent: { type: "number", editable: false },
                    Amount: {
                        type: "number",
                        editable: false
                    },
                    UnitCode: {
                    },
                }
            })
        }

        $scope.AllocateGridOption = function () {
            if (!$scope.form || !$scope.SharedBranch || $scope.SharedBranch.length === 0)
                return undefined;

            _allocateGridOption = lineGrid.gridOption({
                schema: allocateschema,
                data: $scope.form.ProposalLine,
                nolock: false,
                dataBound: function (e) {
                    $scope.hiddenColumn(this);
                    $scope.deleteAllocateToIncomeOther();
                    var grid = this;
                    var gridView = grid.dataSource.view();
                    for (var v = 0; v < gridView.length; v++) {
                        var gridGrouping = grid.dataSource.view()[v].items;
                        for (var g = 0; g < gridGrouping.length; g++) {
                            var gridData = gridGrouping[g].items;
                            for (var i = 0; i < gridData.length; i++) {
                                var currentUid = gridData[i].uid;
                                if (gridData[i].StatusFlag === 1) {
                                    var currentRow = grid.lockedTable.find("tr[data-uid='" + currentUid + "']");
                                    var deleteBtn = $(currentRow).find(".k-grid-pseudo-delete");
                                    //deleteBtn.hide();
                                    deleteBtn.remove();
                                }
                            }
                        }
                    }
                },
                toolbar: (!$scope.isReadonlyDetails()) ? '<a class="k-button btn-default" ng-click="AddNewRecord()"><i class="fa fa-plus"></i> Add New Record</a>' : false,
                columns: [
                    //{ field: "TypeAP", title: "Type", hidden: true },
                    //{ field: "ExpenseTopicCode", title: "ExpenseTopic", width: "150px", hidden: true, },
                    {
                        field: "TypeAP", title: "Type",
                        groupHeaderTemplate: function (data) {
                            if (data.value === 'A') {
                                return "<span>ADVERTISING EXPENSES (A)</span>";
                            }
                            else if (data.value === 'P') {
                                return "<span>PROMOTION EXPENSES (P)</span>";
                            }
                            else if (data.value === 'L') {
                                return "<span>OTHER EXPENSES (P&L)</span>";
                            }
                        },
                        hidden: true
                    },
                    {
                        field: "ExpenseTopicCode", title: "ExpenseTopic", width: "150px",
                        groupHeaderTemplate: function (data) {
                            if ($scope.expenseTopicTemplate.length === 0)
                                return data.value;
                            var result = $filter('filter')($scope.expenseTopicTemplate, {
                                ExpenseTopicCode: data.value
                            });
                            return "<span>" + data.value + " " + result[0].ExpenseTopicName + "</span>";
                        },
                        hidden: true,
                    },
                    {
                        hidden: !$scope.isReadonlyDetails(),
                        width: "120px",
                        locked: true,
                        template: '<a  ng-click="viewTracking(dataItem.Id)" class="k-button btn-info "><i class="fa fa-search"></i>Tracking</a>'
                    },
                    {
                        field: "APCode",
                        title: "AP",
                        width: "45px",
                        locked: true,
                        template: function (e) {
                            return e.APCode ? "<span title='" + e.APCode.Description + "'>" + e.APCode.APCode + "</span>" : "";
                        },
                        //footerTemplate: '<div style="float:right; ">' + 'Total</div>',
                        //groupFooterTemplate: function (data) {
                        //    return '<div style="float:right; "> Total (' + data.parent().value + ')</div>';
                        //},
                        editor: function (container, options) {
                            var editor = $('<select id="' + options.field + '" kendo-dropdownlist k-options="apCodeOptions(dataItem.ExpenseTopicCode)" k-ng-model="dataItem.' + options.field + '"></select>');
                            editor.appendTo(container);
                            return editor;
                        }
                    },
                    {
                        field: "UnitCode",
                        title: "Unit code",
                        width: "80px",
                        locked: true,
                        //template: function (e) {

                        //    return e.UnitCode ? e.UnitCode.UnitName : [];
                        //},
                        template: function (e) {
                            return e.UnitCode ? "<span title='" + e.UnitCode.UnitName + "'>" + e.UnitCode.UnitCode + "</span>" : "";
                        },
                        //  template: '<select id="UnitCode" kendo-drop-down-list k-options="unitcodeOption()" k-ng-model="dataItem.UnitCode" required></select>',
                        editor: function (container, options) {
                            var editor = $('<select id="' + options.field + '" kendo-drop-down-list k-options="unitcodeOptionToAP()"' +
                                //' k-on-change="CheckDuplicate(dataItem)" ' +
                                'k-ng-model="dataItem.' + options.field + '"></select>');
                            editor.appendTo(container);
                            return editor;
                        }
                        , footerTemplate: '<div style="float:right; ">' + 'Total</div>',
                        groupFooterTemplate: function (data) {
                            if (data.parent().value === 'L')
                                return '<div style="float:right; "> Total (P&L)</div>';
                            return '<div style="float:right; "> Total (' + data.parent().value + ')</div>';
                        }
                    },
                    {
                        field: "Description",
                        title: "Description",
                        locked: true,
                        template: function (data) {
                            if (data.Description) {
                                return "<div style='width:100%; word-wrap: break-word;'>" + data.Description + "</div>";
                            }
                            return "";
                        },
                        footerTemplate: 'Marketing Expense',
                        // groupFooterTemplate : "Total",
                        width: "160px",


                    },
                    {
                        field: "Actual",
                        title: "<center>Actual</br>(Last Year)</center>",
                        width: "110px",
                        locked: true,
                        template: lineGrid.formatCurrency('Actual'),
                        // template: ' <input type="number" name="input" ng-model="dataItem.Actual" min="0" >',
                        aggregates: ["sum"],
                        footerTemplate: "<div class='text-right'>#:(sum)? kendo.toString(sum,'n2'):0.00#</div>",
                        groupFooterTemplate: "<div class='text-right'>#:(sum)? kendo.toString(sum,'n2'):0.00#</div>"
                    },
                    {
                        field: "BudgetPlan",
                        title: "<center>Plan</center>",
                        width: "110px",
                        locked: true,
                        template: lineGrid.formatCurrency('BudgetPlan'),
                        aggregates: ["sum"],
                        //footerTemplate: "<div class='text-right'>#:(sum)? kendo.toString(sum,'n2'):0.00#</div>",
                        footerTemplate: "<div class='text-right'>{{sumAmount() | currency:' ':2}}</div>",
                        //"<div class='text-right'>#:(sum)? kendo.toString(sum,'n2'):0.00#</div>",
                        groupFooterTemplate: "<div class='text-right'>#:(sum)? kendo.toString(sum,'n2'):0.00#</div>"

                    },
                    {
                        field: "Used",
                        title: "<center>Used</center>",
                        width: "110px",
                        template: lineGrid.formatCurrency('Used'),
                        editable: false,
                        // hidden: $routeParams.mode.toUpperCase() !== "DETAIL"
                        footerTemplate: "<div class='text-right'>#:(sum)? kendo.toString(sum,'n2'):0.00#</div>",

                    },
                    {
                        field: "Remainning",
                        title: "<center>Remainning</center>",
                        width: "110px",
                        template: lineGrid.formatCurrency('Remainning'),
                        editable: false,
                        //hidden: $routeParams.mode.toUpperCase() !== "DETAIL"
                        footerTemplate: "<div class='text-right'>#:(sum)? kendo.toString(sum,'n2'):0.00#</div>",
                    },
                    {
                        title: "<center>Difference</center>",
                        columns: [
                            {
                                field: "Percent",
                                title: "%",
                                width: "80px",
                                template: lineGrid.formatCurrency('Percent'),
                                footerTemplate: function (data) {
                                    var difAmount = data.BudgetPlan.sum - data.Actual.sum;
                                    var percent = 0;
                                    if (data.Actual.sum === 0)
                                        percent = (difAmount / data.BudgetPlan.sum) * 100;
                                    else {
                                        percent = (difAmount / data.Actual.sum) * 100;
                                    }
                                    var strPercent = (percent !== 0) ? kendo.toString(percent, "n2") : 0.00;
                                    return "<div class='text-right'>" + strPercent + "</div>";
                                },
                                groupFooterTemplate: function (data) {
                                    var difAmount = data.BudgetPlan.sum - data.Actual.sum;
                                    var percent = 0;
                                    if (data.Actual.sum === 0)
                                        percent = (difAmount / data.BudgetPlan.sum) * 100;
                                    else {
                                        percent = (difAmount / data.Actual.sum) * 100;
                                    }
                                    var strPercent = (percent !== 0) ? kendo.toString(percent, "n2") : 0.00;
                                    return "<div class='text-right'>" + strPercent + "</div>";
                                }
                            },
                            {
                                field: "Amount",
                                title: "Amount",
                                width: "90px",
                                template: lineGrid.formatCurrency('Amount'),
                                footerTemplate: function (data) {
                                    var difAmount = data.BudgetPlan.sum - data.Actual.sum;
                                    var strAmount = (difAmount !== 0) ? kendo.toString(difAmount, "n2") : 0.00;
                                    return "<div class='text-right'>" + strAmount + "</div>";
                                },
                                groupFooterTemplate: function (data) {
                                    var difAmount = data.BudgetPlan.sum - data.Actual.sum;
                                    var strAmount = (difAmount !== 0) ? kendo.toString(difAmount, "n2") : 0.00;
                                    return "<div class='text-right'>" + strAmount + "</div>";
                                }
                            },
                        ],
                    },
                    {
                        title: "<center>Shared</center>",
                        columns: function () {
                            var arr = [];
                            for (var t = 0; t < $scope.SharedBranch.length; t++) {
                                if ($scope.SharedBranch[t]) {
                                    arr.push({
                                        field: 'PercentShared["' + $scope.SharedBranch[t].BranchCode + '"]',
                                        title: '<center>' + $scope.SharedBranch[t].BranchCode +'_' +$scope.SharedBranch[t].BranchName + '</center>',
                                        headerAttributes: {
                                            "class": $scope.sharedBranchColor($scope.SharedBranch[t].BranchCode)
                                        },
                                        hidden: $scope.hiddenBranchColumn($scope.SharedBranch[t].BranchCode),
                                        columns: [
                                            {
                                                title: "TR",
                                                headerAttributes: {
                                                    "class": $scope.sharedBranchColor($scope.SharedBranch[t].BranchCode)
                                                },
                                                columns: [
                                                    {
                                                        field: 'PercentShared["' + $scope.SharedBranch[t].BranchCode + '"].TR_Percent',
                                                        title: "%",
                                                        width: "70px",
                                                        // template: lineGrid.formatCurrency('PercentShared["' + $scope.SharedBranch[t].BranchCode + '"].TR_Percent')
                                                        template: lineGrid.formatPercent("PercentShared['" + $scope.SharedBranch[t].BranchCode + "'].TR_Percent"),
                                                        editor: function (container, options) {
                                                            var editor = $("<input price-text-box  type='number' readonly style='width: 100%;' ng-model='dataItem." + options.field + "'  >");
                                                            editor.appendTo(container);
                                                            return editor;
                                                        },
                                                        attributes: {
                                                            "class": $scope.sharedBranchColor($scope.SharedBranch[t].BranchCode)
                                                        },
                                                        headerAttributes: {
                                                            "class": $scope.sharedBranchColor($scope.SharedBranch[t].BranchCode)
                                                        },
                                                    },
                                                    {
                                                        field: 'PercentShared["' + $scope.SharedBranch[t].BranchCode + '"].TR_Amount',
                                                        title: "Amount",
                                                        width: "100px",
                                                        template: lineGrid.formatCurrencyZero("PercentShared['" + $scope.SharedBranch[t].BranchCode + "'].TR_Amount"),
                                                        editor: function (container, options) {
                                                            var editor = $("<input price-text-box  type='number' style='width: 100%;' k-ng-model='dataItem." + options.field + "' k-on-change='calpercentShared(dataItem)' >");
                                                            editor.appendTo(container);
                                                            return editor;
                                                        },
                                                        attributes: {
                                                            "class": $scope.sharedBranchColor($scope.SharedBranch[t].BranchCode)
                                                        },
                                                        headerAttributes: {
                                                            "class": $scope.sharedBranchColor($scope.SharedBranch[t].BranchCode)
                                                        },
                                                        footerTemplate: "<div class='text-right'>#:(sum)? kendo.toString(sum,'n2'):0.00#</div>",
                                                        groupFooterTemplate: "<div class='text-right'>#:(sum)? kendo.toString(sum,'n2'):0.00#</div>"
                                                    }
                                                ],
                                            },
                                            {
                                                title: "RE",
                                                headerAttributes: {
                                                    "class": $scope.sharedBranchColor($scope.SharedBranch[t].BranchCode)
                                                },
                                                columns: [
                                                    {
                                                        field: 'PercentShared["' + $scope.SharedBranch[t].BranchCode + '"].RE_Percent',
                                                        title: "%",
                                                        width: "70px",
                                                        //template: lineGrid.formatCurrency('PercentShared["' + $scope.SharedBranch[t].BranchCode + '"].RE_Percent')
                                                        template: lineGrid.formatPercent("PercentShared['" + $scope.SharedBranch[t].BranchCode + "'].RE_Percent"),
                                                        editor: function (container, options) {
                                                            var editor = $("<input price-text-box readonly type='number' style='width: 100%;' ng-model='dataItem." + options.field + "' >");
                                                            editor.appendTo(container);
                                                            return editor;
                                                        },
                                                        editable: false,
                                                        attributes: {
                                                            "class": $scope.sharedBranchColor($scope.SharedBranch[t].BranchCode)
                                                        },
                                                        headerAttributes: {
                                                            "class": $scope.sharedBranchColor($scope.SharedBranch[t].BranchCode)
                                                        },

                                                    },
                                                    {
                                                        field: 'PercentShared["' + $scope.SharedBranch[t].BranchCode + '"].RE_Amount',
                                                        title: "Amount",
                                                        width: "100px",
                                                        // template: lineGrid.formatCurrency('PercentShared["' + $scope.SharedBranch[t].BranchCode + '"].RE_Amount')
                                                        template: lineGrid.formatCurrencyZero("PercentShared['" + $scope.SharedBranch[t].BranchCode + "'].RE_Amount"),
                                                        editor: function (container, options) {
                                                            var editor = $("<input price-text-box  type='number' style='width: 100%;' k-ng-model='dataItem." + options.field + "' k-on-change='calpercentShared(dataItem)'>");
                                                            editor.appendTo(container);
                                                            return editor;
                                                        },
                                                        attributes: {
                                                            "class": $scope.sharedBranchColor($scope.SharedBranch[t].BranchCode)
                                                        },
                                                        headerAttributes: {
                                                            "class": $scope.sharedBranchColor($scope.SharedBranch[t].BranchCode)
                                                        },
                                                        footerTemplate: "<div class='text-right'>#:(sum)? kendo.toString(sum,'n2'):0.00#</div>",
                                                        groupFooterTemplate: "<div class='text-right'>#:(sum)? kendo.toString(sum,'n2'):0.00#</div>"
                                                    }
                                                ],

                                            }
                                        ]
                                    });
                                }
                            }
                            return arr;
                        }()
                    },
                    {
                        field: "Remark", title: "Remark", width: "120px",
                        template: function (data) {
                            if (data.Remark) {
                                return "<div style='width:100%; word-wrap: break-word;'>" + data.Remark + "</div>";
                            }
                            return "";
                        },
                    },
                    {
                        field: "Document", template: _templateCreateDoc, width: "200px",
                        hidden: !($routeParams.mode.toUpperCase() === "DETAIL")
                    },
                ],
                group: function () {
                    var arr = [
                        { field: "BudgetPlan", aggregate: "sum" },
                        { field: "Actual", aggregate: "sum" },
                        { field: "Percent", aggregate: "sum" },
                        { field: "Amount", aggregate: "sum" },
                        { field: "Used", aggregate: "sum" },
                                { field: "Remainning", aggregate: "sum" }
                    ];
                    for (var t = 0; t < $scope.SharedBranch.length; t++) {
                        if ($scope.SharedBranch[t]) {
                            arr.push({ field: 'PercentShared["' + $scope.SharedBranch[t].BranchCode + '"].TR_Amount', aggregate: "sum" });
                            arr.push({ field: 'PercentShared["' + $scope.SharedBranch[t].BranchCode + '"].RE_Amount', aggregate: "sum" });
                        }
                    }
                    return [
                        {
                            field: "TypeAP",
                            // template: '#if(data.TypeAP){# #=data.TypeAP # #}#',
                            aggregates: arr
                        },
                        {
                            field: "ExpenseTopicCode",
                            template: '#if(data.ExpenseTopicCode){# #=data.ExpenseTopicCode # #}#',
                            aggregates: arr
                        }
                    ];
                }(),
                aggregate: function () {
                    var arr = [
                        { field: "BudgetPlan", aggregate: "sum" },
                        { field: "Actual", aggregate: "sum" },
                        { field: "Percent", aggregate: "sum" },
                        { field: "Amount", aggregate: "sum" },
                        { field: "Used", aggregate: "sum" },
                        { field: "Remainning", aggregate: "sum" }
                    ];
                    for (var t = 0; t < $scope.SharedBranch.length; t++) {
                        if ($scope.SharedBranch[t]) {
                            arr.push({ field: 'PercentShared["' + $scope.SharedBranch[t].BranchCode + '"].TR_Amount', aggregate: "sum" });
                            arr.push({ field: 'PercentShared["' + $scope.SharedBranch[t].BranchCode + '"].RE_Amount', aggregate: "sum" });
                        }
                    }
                    return arr;
                }(),
                editable: {
                    mode: "incell"
                },
                sort: { field: 'APCode.Order', dir: 'asc' },
                readonly: $scope.isReadonlyDetails(),
                noadd: true,
                // nodelete :true,
                //template: $('#_detailsTemplate').html(),
                noedit: true,
                height: 600,
                edit: function (e) {
                    if (e.model.StatusFlag) {
                        var name = e.container.find("[name]").first().attr("name");
                        if (name && name !== "BudgetPlan"
                            && name.indexOf("TR_Amount") === -1
                            && name.indexOf("RE_Amount") === -1) {
                            e.sender.closeCell();
                        }
                        if (!name) {
                            var ngModel = e.container.find("[k-ng-model]").first().attr("k-ng-model");
                            if (ngModel && ngModel !== "BudgetPlan"
                            && ngModel.indexOf("TR_Amount") === -1
                            && ngModel.indexOf("RE_Amount") === -1) {
                                e.sender.closeCell();
                            }

                            if (!ngModel) {
                                e.sender.closeCell();
                            }
                        }
                    }
                },
                save: function (e) {
                    var keys = Object.keys(e.values);
                    var key = keys[0];
                    if (key) {
                        e.model[key] = e.values[key];
                    }
                    if (e.model.isNew()) {
                        if (e.model.APCode.APCode === "DO3") {
                            $scope.pushAllocateToIncomeOther(e.model);
                            e.model.BudgetPlan = 0;
                        }
                    }
                    else {
                        if (e.model.APCode.APCode === "DO3") {
                            e.model.BudgetPlan = 0;
                        }
                    }
                    var name = e.container.find("[name]").first().attr("name");
                    if (name && (name === "BudgetPlan" || name === "Actual")) {
                        if (name === "Actual") {
                            $scope.calDifference(e.model, true);
                        } else {
                            $scope.calDifference(e.model);
                        }
                    }
                    this.saveChanges();
                },
                pageable: false,
                groupable: true
            });

            _allocateGridOption.resizable = true;
            return _allocateGridOption;
        }

        $scope.calDifference = function (model, change) {
            if (change) {
                var budget = (model.BudgetPlan === 0 || model.BudgetPlan === null || model.APCode.APCode === "DO3") ? 0 : model.BudgetPlan;
                model.BudgetPlan = budget;
                var actual = (model.Actual === 0 || model.Actual === null) ? 0 : model.Actual;
                model.Amount = budget - actual;
                var cal = 0;
                if (actual === 0) {
                    cal = ((budget - actual) / budget) * 100;
                } else {
                    cal = ((budget - actual) / actual) * 100;
                }
                model.Percent = cal;
            } else {
                var budget = (model.BudgetPlan === 0 || model.BudgetPlan === null || model.APCode.APCode === "DO3") ? 0 : model.BudgetPlan;
                model.BudgetPlan = budget;
                var actual = (model.Actual === 0 || model.Actual === null) ? 0 : model.Actual;
                model.Amount = budget - actual;
                var cal = 0;
                if (actual === 0) {
                    cal = ((budget - actual) / budget) * 100;
                } else {
                    cal = ((budget - actual) / actual) * 100;
                }
                model.Percent = cal;

                var sumtotalSale = 0;
                var sumtotalArea = 0;
                angular.forEach($scope.dataTemplate, function (tmps) {
                    sumtotalSale += tmps.TotalSale;
                    sumtotalArea += tmps.AreaRE;
                });
                $scope.calAllocate(model, sumtotalSale, sumtotalArea);
            }
            return model;
        }

        $scope.calAllocate = function (model, sumtotalSale, sumtotalArea) {
            var budget = model.BudgetPlan;
            var caseCheck = "";
            if ($scope.form.TR && !$scope.form.RE) {
                caseCheck = "isTR";
            } else if (!$scope.form.TR && $scope.form.RE) {
                caseCheck = "isRE";
            } else if ($scope.form.TR && $scope.form.RE) {
                caseCheck = "TRRE";
            }
            switch (caseCheck) {
                case "isTR":
                    angular.forEach($scope.dataTemplate, function (tmp) {
                        var tempPercent = (tmp.TotalSale / sumtotalSale) * 100;
                        var TR_Percent = tempPercent;
                        var TR_Amount = (budget * tempPercent) / 100;
                        model.PercentShared[tmp.BranchCode].TR_Percent = TR_Percent;
                        model.PercentShared[tmp.BranchCode].TR_Amount = TR_Amount;
                        //  model.set('PercentShared["' + tmp.BranchCode + '"].TR_Percent', TR_Percent);
                        //  model.set('PercentShared["' + tmp.BranchCode + '"].TR_Amount', TR_Amount);
                    });
                    break;

                case "isRE":
                    angular.forEach($scope.dataTemplate, function (tmp) {

                        var tempPercent = (sumtotalArea === 0) ? 0 : (tmp.AreaRE / sumtotalArea) * 100;
                        var RE_Percent = tempPercent;
                        var RE_Amount = (budget * tempPercent) / 100;
                        model.PercentShared[tmp.BranchCode].RE_Percent = RE_Percent;
                        model.PercentShared[tmp.BranchCode].RE_Amount = RE_Amount;
                    });
                    break;

                case "TRRE":
                    angular.forEach($scope.dataTemplate, function (tmp) {
                        var tempPercent = (tmp.TotalSale / sumtotalSale) * 100;
                        var TR_Percent = (tempPercent * tmp.PercentTR) / 100;
                        var RE_Percent = (tempPercent * tmp.PercentRE) / 100;
                        var TR_Amount = (budget * TR_Percent) / 100;
                        var RE_Amount = (budget * RE_Percent) / 100;
                        model.PercentShared[tmp.BranchCode].RE_Percent = RE_Percent;
                        model.PercentShared[tmp.BranchCode].RE_Amount = RE_Amount;
                        model.PercentShared[tmp.BranchCode].TR_Percent = TR_Percent;
                        model.PercentShared[tmp.BranchCode].TR_Amount = TR_Amount;
                        //model.set('PercentShared["' + tmp.BranchCode + '"].TR_Percent', TR_Percent);
                        //model.set('PercentShared["' + tmp.BranchCode + '"].RE_Percent', RE_Percent);
                        //model.set('PercentShared["' + tmp.BranchCode + '"].TR_Amount', TR_Amount);
                        //model.set('PercentShared["' + tmp.BranchCode + '"].RE_Amount', RE_Amount);
                    });
                    break;
            }
            return model;
        }

        $scope.calpercentShared = function (model) {
            var budget = model.BudgetPlan;
            var caseCheck = "";
            if ($scope.form.TR && !$scope.form.RE) {
                caseCheck = "isTR";
            } else if (!$scope.form.TR && $scope.form.RE) {
                caseCheck = "isRE";
            } else if ($scope.form.TR && $scope.form.RE) {
                caseCheck = "TRRE";
            }
            switch (caseCheck) {
                case "isTR":
                    var sumAmount = 0;
                    angular.forEach($scope.dataTemplate, function (tmps) {
                        var trAmount = model.PercentShared[tmps.BranchCode].TR_Amount;
                        model.PercentShared[tmps.BranchCode].TR_Percent = (trAmount * 100) / budget;
                        model.PercentShared[tmps.BranchCode].TR_Amount = trAmount;
                        sumAmount += trAmount;
                    });
                    break;

                case "isRE":
                    var sumAmount = 0;
                    angular.forEach($scope.dataTemplate, function (tmps) {
                        var reAmount = model.PercentShared[tmps.BranchCode].RE_Amount;
                        model.PercentShared[tmps.BranchCode].RE_Percent = (reAmount * 100) / budget;
                        model.PercentShared[tmps.BranchCode].RE_Amount = reAmount;
                        sumAmount += reAmount;
                    });
                    break;

                case "TRRE":
                    var percent = 100 / ($scope.dataTemplate.length * 2);
                    angular.forEach($scope.dataTemplate, function (tmps) {
                        var trAmount = model.PercentShared[tmps.BranchCode].TR_Amount;
                        var reAmount = model.PercentShared[tmps.BranchCode].RE_Amount;
                        model.PercentShared[tmps.BranchCode].RE_Percent = (reAmount * 100) / budget;
                        model.PercentShared[tmps.BranchCode].RE_Amount = reAmount;
                        model.PercentShared[tmps.BranchCode].TR_Percent = (trAmount * 100) / budget;
                        model.PercentShared[tmps.BranchCode].TR_Amount = trAmount;

                    });
                    break;
            }

            $scope.allocateGrid.saveChanges();
            $scope.allocateGrid.refresh();
            return model;
        };

        $scope.hiddenBranchColumn = function (code) {
            if ($scope.form.SharedBranchTemplateLines) {
                var templateLine = $scope.form.SharedBranchTemplateLines.filter(function (data) {
                    return data.BranchCode === code;
                });
                if (templateLine && templateLine.length > 0) {
                    return !templateLine[0].Selected;
                }
            }
            return true;
        }

        $scope.hiddenColumn = function (thisGrid) {
            var grid = $scope.allocateGrid;
            if (thisGrid) {
                grid = thisGrid;
            }
            if (!$scope.form.TR && $scope.form.RE) {
                angular.forEach($scope.dataTemplate, function (tmp) {
                    grid.hideColumn('PercentShared["' + tmp.BranchCode + '"].TR_Percent');
                    grid.hideColumn('PercentShared["' + tmp.BranchCode + '"].TR_Amount');
                    grid.showColumn('PercentShared["' + tmp.BranchCode + '"].RE_Percent');
                    grid.showColumn('PercentShared["' + tmp.BranchCode + '"].RE_Amount');

                });
            }
            else if ($scope.form.TR && !$scope.form.RE) {
                angular.forEach($scope.dataTemplate, function (tmp) {

                    grid.hideColumn('PercentShared["' + tmp.BranchCode + '"].RE_Percent');
                    grid.hideColumn('PercentShared["' + tmp.BranchCode + '"].RE_Amount');
                    grid.showColumn('PercentShared["' + tmp.BranchCode + '"].TR_Percent');
                    grid.showColumn('PercentShared["' + tmp.BranchCode + '"].TR_Amount');
                    // grid.editableColumn('PercentShared["' + tmp.BranchCode + '"].TR_Percent');
                });
            }
            else if (!$scope.form.TR && !$scope.form.RE) {
                angular.forEach($scope.dataTemplate, function (tmp) {
                    grid.hideColumn('PercentShared["' + tmp.BranchCode + '"].RE_Percent');
                    grid.hideColumn('PercentShared["' + tmp.BranchCode + '"].RE_Amount');
                    grid.hideColumn('PercentShared["' + tmp.BranchCode + '"].TR_Percent');
                    grid.hideColumn('PercentShared["' + tmp.BranchCode + '"].TR_Amount');
                });
            } else {
                angular.forEach($scope.dataTemplate, function (tmp) {
                    grid.showColumn('PercentShared["' + tmp.BranchCode + '"].RE_Percent');
                    grid.showColumn('PercentShared["' + tmp.BranchCode + '"].RE_Amount');
                    grid.showColumn('PercentShared["' + tmp.BranchCode + '"].TR_Percent');
                    grid.showColumn('PercentShared["' + tmp.BranchCode + '"].TR_Amount');
                });
            }
        }

        $scope.pushAllocateToIncomeOther = function (data) {
            if (data) {
                var income = new schemaIncomeOther.model();
                income.SponcorName = data.APCode.APCode;
                income.Description = data.APCode.APName;
                income.Budget = data.BudgetPlan;
                income.ContactName = data.UnitCode.UnitName;
                income.LineNo = lineGrid.nextLineNo($scope.form.IncomeOther);
                $scope.form.IncomeOther.push(income);
                setIncomeOtherGridDataSource($scope.form.IncomeOther);
            }
        }

        $scope.deleteAllocateToIncomeOther = function () {
            if ($scope.form.ProposalLine)
                for (var i = 0; i < $scope.form.ProposalLine.length; i++) {
                    if ($scope.form.ProposalLine[i].Deleted === true && $scope.form.ProposalLine[i].APCode.APCode === "DO3") {
                        var income = $filter('filter')($scope.form.IncomeOther, {
                            SponcorName: "DO3"
                        });
                        angular.forEach(income, function (values) {
                            values.Deleted = true;
                        });
                    }
                }
        }
        //EndAllocate

        $scope.GenerateAutoSequenceNo = function (level) {
            var proposalApprovalForLevel = $scope.form.ProposalApproval.filter(function (data) {
                return data.ApproverLevel === level && data.Deleted !== true;
            });
            var sequenceNo = Math.max.apply(Math, proposalApprovalForLevel.map(function (item) { return item.ApproverSequence; }));
            return sequenceNo + 1;
        }
        if ($routeParams.mode.toUpperCase() !== 'REVISEPROPOSAL') {
            checkFinalApproverButton = true;
        }

        $scope.GenerateAutoSequenceNo = function (level) {
            var proposalApprovalForLevel = $scope.form.ProposalApproval.filter(function (data) {
                return data.ApproverLevel === level && data.Deleted !== true;
            });
            var sequenceNo = Math.max.apply(Math, proposalApprovalForLevel.map(function (item) { return item.ApproverSequence; }));
            return sequenceNo + 1;
        }

        $scope.validateTotalbudget = function () {
            if ($scope.form.TotalBudget != 0) {
                return true;
            } else {
                messageBox.error("Budget is not 0 baht!!!");
                return false;
            }
        }

        $scope.validateAllocatPlan = function () {
            var icheck = true;
            for (var i = 0; i < $scope.form.ProposalLine.length; i++) {
                if ($scope.form.ProposalLine && !$scope.form.ProposalLine[i].Deleted) {
                    var budget = $scope.form.ProposalLine[i].BudgetPlan,
                        caseCheck = "";

                    if ($scope.form.TR && !$scope.form.RE) {
                        caseCheck = "isTR";
                    } else if (!$scope.form.TR && $scope.form.RE) {
                        caseCheck = "isRE";
                    } else if ($scope.form.TR && $scope.form.RE) {
                        caseCheck = "TRRE";
                    }

                    switch (caseCheck) {
                        case "isTR":
                            var sumAmount = 0;
                            angular.forEach($scope.dataTemplate, function (tmps) {
                                var trAmount = $scope.form.ProposalLine[i].PercentShared[tmps.BranchCode].TR_Amount;
                                $scope.form.ProposalLine[i].PercentShared[tmps.BranchCode].TR_Percent = (trAmount * 100) / budget;
                                $scope.form.ProposalLine[i].PercentShared[tmps.BranchCode].TR_Amount = trAmount;
                                sumAmount += trAmount;
                            });
                            if (sumAmount !== budget) {
                                icheck = false;
                            } else {
                                icheck = true;
                            }
                            break;

                        case "isRE":
                            var sumAmount = 0;
                            angular.forEach($scope.dataTemplate, function (tmps) {
                                var reAmount = $scope.form.ProposalLine[i].PercentShared[tmps.BranchCode].RE_Amount;
                                $scope.form.ProposalLine[i].PercentShared[tmps.BranchCode].RE_Percent = (reAmount * 100) / budget;
                                $scope.form.ProposalLine[i].PercentShared[tmps.BranchCode].RE_Amount = reAmount;
                                sumAmount += reAmount;
                            });
                            if (sumAmount !== budget) {
                                icheck = false;
                            } else {
                                icheck = true;
                            }
                            break;

                        case "TRRE":
                            var sumAmount = 0;
                            var percent = 100 / ($scope.dataTemplate.length * 2);
                            angular.forEach($scope.dataTemplate, function (tmps) {
                                var trAmount = $scope.form.ProposalLine[i].PercentShared[tmps.BranchCode].TR_Amount;
                                var reAmount = $scope.form.ProposalLine[i].PercentShared[tmps.BranchCode].RE_Amount;
                                $scope.form.ProposalLine[i].PercentShared[tmps.BranchCode].RE_Percent = (reAmount * 100) / budget;
                                $scope.form.ProposalLine[i].PercentShared[tmps.BranchCode].RE_Amount = reAmount;
                                $scope.form.ProposalLine[i].PercentShared[tmps.BranchCode].TR_Percent = (trAmount * 100) / budget;
                                $scope.form.ProposalLine[i].PercentShared[tmps.BranchCode].TR_Amount = trAmount;
                                sumAmount += trAmount;
                                sumAmount += reAmount;
                            });
                            if (sumAmount !== budget) {
                                icheck = false;
                            } else {
                                icheck = true;
                            }
                            break;
                    }
                }
                if (icheck === false) {
                    messageBox.error("จำนวนเงินการแบ่ง % ไม่เท่ากับ budget plan !");
                    break;
                    return false;
                }
            }
            if (icheck) {
                return true;
            }
        }

        $scope.validate = function (event, preventSubmit) {
            var bcheck = true,
                checkOtherTheme = false,
                themeDropdown = $("#theme").data("kendoDropDownList");
            $scope.form.SharedBranchTemplate = $scope.SharedBranch;
            $scope.setDataApproverBeforeSave(); //Approver

            if (preventSubmit !== false) { // click save
                checkFinalApproverButton = true;
                bcheck = true;
            } else {
                if (!$scope.validateTotalbudget()) {
                    bcheck = false;
                }
                if (!$scope.validateAllocatPlan()) {
                    bcheck = false;
                }
            }

            if ($scope.form.Theme == undefined) {
                themeDropdown.select(0);
                themeDropdown.trigger("change");
            }

            if ($scope.form.Theme.ThemesCode === "Other") {
                checkOtherTheme = true;
                if (!$scope.form.ThemeNewName) {
                    messageBox.error("Please enter a theme!!!");
                    bcheck = false;
                }
            }

            if (bcheck) {
                if (checkFinalApproverButton) {
                    k2.saveAndSubmit({
                        event: event,
                        preventSubmit: preventSubmit,
                        validator: $scope.validator,
                        save: dataSource.save,
                        data: $scope.form,
                        saveSuccess: function (response) {
                            if ($routeParams.mode.toUpperCase() === "ACC-APPROVE" || $routeParams.mode.toUpperCase() === "REVISE") {
                                location.reload();
                            }
                                // location.reload();
                            else {
                                $scope.form = response;
                                setAllocateGridDataSource(response.ProposalLine);
                                setDepositGridDataSource(response.DepositLines);
                                setIncomeDepositGridDataSource(response.IncomeDeposit);
                                setIncomeOtherGridDataSource(response.IncomeOther);
                                setIncomeTotalSaleGridDataSource(response.IncomeTotalSale);
                                $scope.SharedBranch = response.SharedBranchTemplate;
                                setSharedBranchTemplates(response.SharedBranchTemplateLines, false);
                                //setLogProposalTrackChangesGridDataSource(response.LogProposalTrackChanges);
                                isSave = true;
                                //$scope.form.ProposalApproval = new kendo.data.ObservableArray(response.ProposalApproval);
                                for (var j = 0; j < $scope.ApproverLevel.length; j++) {
                                    setApprovalGridDataSource(j, $scope.ApproverLevel[j], $scope.form.ProposalApproval);
                                }

                                if (!$scope.form.CCEmail) {
                                    $scope.form.CCEmail = [];
                                }

                                if (!$scope.form.InformEmail) {
                                    $scope.form.InformEmail = [];
                                }

                                $("#ccEmail").data("kendoMultiSelect").dataSource.data($scope.form.CCEmail);
                                $("#informEmail").data("kendoMultiSelect").dataSource.data($scope.form.InformEmail);
                                //$location.path("proposal/detail/" + response.Id);

                                //SetDate 
                                //Start date
                                $scope.form.StartDate = new Date($scope.form.StartDate);
                                $scope.form.StartDate.setHours(7);
                                //End date
                                $scope.form.EndDate = new Date($scope.form.EndDate);
                                $scope.form.EndDate.setHours(7);
                                //Project end date
                                $scope.form.ProjectEndDate = new Date($scope.form.ProjectEndDate);
                                $scope.form.ProjectEndDate.setHours(7);
                                //Project start date
                                if ($scope.form.ProjectStartDate) {
                                    $scope.form.ProjectStartDate = new Date($scope.form.ProjectStartDate);
                                    $scope.form.ProjectStartDate.setHours(7);
                                }

                                //Approver 
                                $scope.form.DueDatePropose = new Date($scope.form.DueDatePropose);
                                $scope.form.DueDatePropose.setHours(7);
                                $scope.form.DueDateAccept = new Date($scope.form.DueDateAccept);
                                $scope.form.DueDateAccept.setHours(7);
                                $scope.form.DueDateApprove = new Date($scope.form.DueDateApprove);
                                $scope.form.DueDateApprove.setHours(7);
                                $scope.form.DueDateFinalApprove = new Date($scope.form.DueDateFinalApprove);
                                $scope.form.DueDateFinalApprove.setHours(7);

                                //new theme
                                if (checkOtherTheme) {
                                    if (response.Theme.ThemesCode) {
                                        themeDropdown.dataSource.add({
                                            "ThemesCode": $scope.form.Theme.ThemesCode,
                                            "ThemesName": $scope.form.Theme.ThemesCode
                                        });
                                        themeDropdown.select(0); // select (ต้องรู้ index)
                                        themeDropdown.trigger("change");
                                        $scope.form.ThemeNewName = "";
                                    }
                                }
                            }
                        },
                        submitSuccess: function (response) {
                            if (preventSubmit === false) {
                                //$location.path("proposal/");
                                // $location.path("proposal/detail/" + $routeParams.id);
                            }
                        },
                        uploadHandler: $scope.uploadHandler,
                        //uploadSuccessIsCallback: true,
                        //uploadSuccess: function (response, callback) {
                        //},
                    });
                } else {
                    messageBox.error("Please push get final approver button!!!");
                }
            }            
        }

        $scope.openK2ActionModal = function (action) {
            $scope.choosenAction = action;
            $scope.modalInstance = k2.createActionModal.call($scope);
        }

        $scope.chooseAction = function (action) {
            var bcheck = true,
                themeDropdown = $("#theme").data("kendoDropDownList");

            if ($scope.form.Theme == undefined) {
                themeDropdown.select(0);
                themeDropdown.trigger("change");
            }

            if ($scope.form.Theme.ThemesCode === "Other") {
                checkOtherTheme = true;
                if (!$scope.form.ThemeNewName) {
                    messageBox.error("Please enter a theme!!!");
                    bcheck = false;
                }
            }

            if (action.toUpperCase() === "RESUBMIT") {
                if (!$scope.validateTotalbudget()) {
                    bcheck = false;
                }
                if (!$scope.validateAllocatPlan()) {
                    bcheck = false;
                }
            }
            if (bcheck) {
                $scope.Action = action;
                $scope.setDataApproverBeforeSave();
                k2.chooseAction({
                    validator : $scope.validator,
                    actions : $scope.form.Actions,
                    action : action,
                    resource : $scope.form,
                    uploadHandler : $scope.uploadHandler
                });
            }
        }

        $scope.showComments = false;
        if ($routeParams.mode.toUpperCase() !== 'CREATE') {
            $scope.showComments = true;
        }

        $scope.totalSale = function () {;
            if (!_totalSaleTargetGridOption ||
                !_totalSaleTargetGridOption.dataSource ||
                !_totalSaleTargetGridOption.dataSource._aggregateResult ||
                !_totalSaleTargetGridOption.dataSource._aggregateResult.Budget ||
                !_totalSaleTargetGridOption.dataSource._aggregateResult.Budget.sum) {
                $scope.totalSaleNet = 0;
                return 0;
            }
            $scope.form.TotalSaleTarget = $scope.totalSaleNet = _totalSaleTargetGridOption.dataSource._aggregateResult.Budget.sum;
            return $scope.totalSaleNet;
        }

        $scope.totalSaleMarketing = function () {;
            //if (!_allocateGridOption ||
            //    !_allocateGridOption.dataSource ||
            //    !_allocateGridOption.dataSource._aggregateResult ||
            //    !_allocateGridOption.dataSource._aggregateResult.BudgetPlan ||
            //    !_allocateGridOption.dataSource._aggregateResult.BudgetPlan.sum) {
            //    $scope.totalSaleMktExpNet = 0;
            //    return 0;
            //}
            //$scope.form.TotalMarketingExpense = $scope.totalSaleMktExpNet = _allocateGridOption.dataSource._aggregateResult.BudgetPlan.sum;
            //return $scope.totalSaleMktExpNet;
            if (!_allocateGridOption ||
                       !_allocateGridOption.dataSource ||
                       !_allocateGridOption.dataSource._aggregateResult ||
                       !_allocateGridOption.dataSource._aggregateResult.BudgetPlan)
            {
                $scope.totalSaleMktExpNet = 0;
                return 0;
            }
            $scope.form.TotalMarketingExpense = $scope.totalSaleMktExpNet = $scope.totalSaleAdvBudgetPlan() + $scope.totalSalePromoBudgetPlan();
            return $scope.totalSaleMktExpNet;
        }

        $scope.grandTotal = function () {
            if (!_allocateGridOption ||
                !_allocateGridOption.dataSource ||
                !_allocateGridOption.dataSource._aggregateResult ||
                !_allocateGridOption.dataSource._aggregateResult.BudgetPlan ||
                !_allocateGridOption.dataSource._aggregateResult.BudgetPlan.sum) {
                $scope.totalSaleGrandTotal = 0;
                return 0;
            }

            $scope.form.GrandTotal = $scope.totalSaleGrandTotal = _allocateGridOption.dataSource._aggregateResult.BudgetPlan.sum;
            return $scope.totalSaleGrandTotal;
        }

        $scope.totalSaleAdvBudgetPlan = function () {
            var total = 0;
            var sumTotalSaleAdvBudgetPlan = $scope.form.ProposalLine.filter(function (data) {
                if (!data.Deleted && data.TypeAP && data.TypeAP === "A") {
                    if (data.BudgetPlan)
                        total += data.BudgetPlan;
                    return true;
                }
                return false;
            });
            $scope.form.TotalAdvertising = $scope.totalSaleAdvExpNet = total;
            return total;
        }

        $scope.totalSalePromoBudgetPlan = function () {
            var total = 0;
            var sumTotalSaleAdvBudgetPlan = $scope.form.ProposalLine.filter(function (data) {
                if (!data.Deleted && data.TypeAP && data.TypeAP === "P") {
                    if (data.BudgetPlan)
                        total += data.BudgetPlan;
                    return true;
                }
                return false;
            });
            $scope.form.TotalPromotion = $scope.totalSalePromoExpNet = total;
            return $scope.totalSalePromoExpNet;
        }

        $scope.totalSaleOtherExpenseBudgetPlan = function () {
            var total = 0;
            var sumTotalSaleAdvBudgetPlan = $scope.form.ProposalLine.filter(function (data) {
                if (!data.Deleted && data.TypeAP && data.TypeAP === "L") {
                    if (data.BudgetPlan)
                        total += data.BudgetPlan;
                    return true;
                }
                return false;
            });
            $scope.form.TotalOtherExpense = $scope.totalSaleOtherExpenseExpNet = total;
            return $scope.totalSaleOtherExpenseExpNet;
        }

        $scope.percentAdv = function () {
            if (!$scope.totalSaleNet)
                return $scope.form.PecentAdvertising = 0;
            $scope.form.PecentAdvertising = ($scope.totalSaleAdvExpNet / $scope.totalSaleNet) * 100;
            return $scope.form.PecentAdvertising;
        }

        $scope.percentPromo = function () {
            if (!$scope.totalSaleNet)
                return $scope.form.PecentPromotion = 0;
            $scope.form.PecentPromotion = ($scope.totalSalePromoExpNet / $scope.totalSaleNet) * 100;
            return $scope.form.PecentPromotion;
        }

        $scope.percentOtherExpense = function () {
            if (!$scope.totalSaleNet)
                return $scope.form.PercentOtherExpense = 0;
            $scope.form.PercentOtherExpense = ($scope.totalSaleOtherExpenseExpNet / $scope.totalSaleNet) * 100;
            return $scope.form.PercentOtherExpense;
        }

        $scope.percentMarketing = function () {
            if (!$scope.totalSaleNet)
                return $scope.form.PecentMarketingExpense = 0;
            $scope.form.PecentMarketingExpense = ($scope.totalSaleMktExpNet / $scope.totalSaleNet) * 100;
            return $scope.form.PecentMarketingExpense;
        }

        $scope.percentGrandTotal = function () {
            if (!$scope.totalSaleNet)
                return $scope.form.PercentGrandTotal = 0;
            $scope.form.PercentGrandTotal = ($scope.totalSaleGrandTotal / $scope.totalSaleNet) * 100;
            return $scope.form.PercentGrandTotal;
        }

        $scope.sumAmount = function () {
            if (!_allocateGridOption ||
                !_allocateGridOption.dataSource ||
                !_allocateGridOption.dataSource._aggregateResult ||
                !_allocateGridOption.dataSource._aggregateResult.BudgetPlan ||
                !_allocateGridOption.dataSource._aggregateResult.BudgetPlan.sum) {
                $scope.form.TotalBudget = 0;
                return $scope.form.TotalBudget;
            }
            var total = _allocateGridOption.dataSource._aggregateResult.BudgetPlan.sum;
            $scope.form.TotalBudget = total;
            return total;
        }

        $scope.haveIncome = function () {
            var incomeTotal = $scope.form.IncomeOther.filter(function (data) {
                return data.Deleted !== true;
            });
            return ((incomeTotal.length > 0) || (($scope.form.ProposalType) && $scope.form.ProposalType.TypeProposalCode === "DEPOSIT")) ? true : false;
            //return ((incomeTotal.length > 0) || $scope.form.ProposalType.TypeProposalCode ==="DEPOSIT") ? true : false;
        }

        $scope.hideCloseProposal = function () {
            if ($scope.form.StatusFlag === 9 && !$scope.form.CloseFlag && (($scope.form.Requester) && $scope.form.Requester.Username.toUpperCase() === CURRENT_USERNAME.toUpperCase()))
                return true;
            return false;
        }

        $scope.closeProposal = function () {
            $http.get(angular.crs.url.webApi("proposal/closeproposal") + '?id=' + $routeParams.id).then(function (result) {
                if (result.data === true) {
                    $scope.form.CloseFlag = result.data;
                    return messageBox.info("ปิด Proposal :" + $scope.form.DocumentNumber + " เรียบร้อยแล้ว");
                } else {
                    $scope.form.CloseFlag = result.data;
                    return messageBox.error("ยังไม่สามารถปิด Proposal :" + $scope.form.DocumentNumber + " ได้ ยังมีบางรายการกำลังดำเนินงานอยู่");
                }
            });
        }

        $scope.modeRevise = function () {
            if ($routeParams.mode.toUpperCase() === "REVISE") {
                return true;
            }
            return false;
        }

        //Set Data Before Save
        $scope.setDataApproverBeforeSave = function () {
            $scope.form.ProposalApproval = new kendo.data.ObservableArray([]);
            $scope.form.ProposalApproval = mergeArrayData($scope.form.ProposalApproval, $scope.ApproverGridOptionLevelOneData);
            $scope.form.ProposalApproval = mergeArrayData($scope.form.ProposalApproval, $scope.ApproverGridOptionLevelTwoData);
            $scope.form.ProposalApproval = mergeArrayData($scope.form.ProposalApproval, $scope.ApproverGridOptionLevelThreeData);
            $scope.form.ProposalApproval = mergeArrayData($scope.form.ProposalApproval, $scope.ApproverGridOptionLevelFourData);
            setApproverLevelFiveAndSixData($scope.forms.ApproverGridOptionLevelFiveData, $scope.ApproverLevel[4]);
            setApproverLevelFiveAndSixData($scope.forms.ApproverGridOptionLevelSixData, $scope.ApproverLevel[5]);
            $scope.form.ProposalApproval = mergeArrayData($scope.form.ProposalApproval, $scope.ApproverGridOptionLevelFiveData);
            $scope.form.ProposalApproval = mergeArrayData($scope.form.ProposalApproval, $scope.ApproverGridOptionLevelSixData);
        };
       
        //Search by Email 
        $scope.EmailOption = approverGrids.emailOption;       
        $scope.clearExpenseTopicTemplate = function () {
            $scope.expenseTopic = new kendo.data.ObservableArray([]);
            if ($scope.expenseTopicTemplate) {
                $scope.expenseGrid.refresh();
            }
        }

        $scope.branchAccountingOptions = {
            dataTextField: "PlaceName",
            dataValueField: "PlaceCode",
            autoBind: false,
            optionLabel: "please select",
            template: '#=data.PlaceCode# - #=data.PlaceName#',
            valueTemplate: '#=data.PlaceCode# - #=data.PlaceName#',
            filter: "contains",
            //enable: false,
            dataSource: {
                type: 'json',
                //serverFiltering: true,
                transport: {
                    read: {
                        url: function () {
                            return angular.crs.url.webApi("accounting/getaccountingbranch");
                        }
                    }
                }
            }
        }

        $scope.cancelRevise = function () {
            $http.get(angular.crs.url.webApi("proposal/cancelrevise") + '?id=' + $routeParams.id + '&log=false').then(function (result) {
                if (result.data === true) {
                    messageBox.info("ยกเลิกการ Revise Proposal เรียบร้อยแล้ว");
                    $location.path('proposal/index');
                } else {
                    messageBox.error("ไม่สามารถยกเลิก Revise Proposal ได้");
                    //$location.path('proposal/index');
                }
            }, function (response) {
                messageBox.extractError(response);
            });
        }

        $scope.depositInApproverOption = {
            dataTextField: "ApproverFullName",
            dataValueField: "ApproverUserName",
            template: '#=data.ApproverUserName#|#=data.ApproverFullName#',
            optionLabel: "please select",
            valueTemplate: '#=data.ApproverUserName#',
            autoBind: true,
            dataSource: new kendo.data.DataSource({
                type: "json",
                data: new kendo.data.ObservableArray([])
            }),
        };

        $scope.depositInApproverOption.dataSource.data([]);
        $scope.$watch('form.DepositNumber', function () {
            debugger;
            if ($scope.form.DepositNumber && $scope.form.DepositNumber.Approver) {
                $scope.depositInApproverOption.dataSource.data($scope.form.DepositNumber.Approver);
            }
        });

        $scope.logTrackChangeGridOption = function () {
            if (_logTrackChangeGridOption)
                return _logTrackChangeGridOption;
            _logTrackChangeGridOption = lineGrid.gridOption({
                schema: {
                    model: kendo.data.Model.define({
                        id: "Id",
                        fields: {
                            Id: { type: "number" },

                        }
                    })
                },
                data: $scope.form.LogProposalTrackChanges,
                columns: [
                    { field: 'RefTableDesc', title: 'RefTableDesc', width: '100px' },
                    { field: 'FieldDescription', title: 'FieldDescription', width: '100px' },
                    { field: 'State', title: 'State', width: '100px' },
                    { field: 'OldValue', title: 'OldValue', width: '100px' },
                    { field: 'NewValue', title: 'NewValue', width: '100px' },
                    {
                        field: 'CreatedDate', title: 'CreatedDate', width: '100px',
                        template: lineGrid.formatDateTime('CreatedDate'),
                    },
                ],
                nocommand: true,
                noadd: true,
            });
            return _logTrackChangeGridOption;
        }
    }]);