﻿angular.module('app.controllers').controller('proposalCompleteController',
[
    '$scope', '$routeParams', 'inquiryGrid', 'authService',
    function ($scope, $routeParams, inquiryGrid, authService) {
        var params = {
           // height: '300px',
            //height: 'auto',
            pageSize: 5,
            scrollable: false,
            sorts : { field: 'CreatedDate', dir: 'desc' },
        }
        var filters = [];
        if (authService.authentication && authService.authentication.me) {
            angular.forEach(authService.authentication.me.UnitCode, function (data) {
                if (data.UnitCode) {
                    var filter = { field: "UnitCodeAP", operator: "contains", value: data.UnitCode };
                    filters.push(filter);
                }
            });   
        }
        if (filters.length === 0) {
            filters.push({ field: "UnitCodeAP", operator: "contains", value: "-" });
        }
        params.filters = { filters: filters, logic: "or" };
        $scope.gridKendoOption = inquiryGrid.gridKendoOption(angular.crs.url.odata('proposalunitcodeview'), params);
        $scope.gridKendoOption.columns =
            [
               inquiryGrid.detailColumn,
                {
                    field: 'DocumentNumber', title: 'Proposal Number', width: '150px',
                    template: function (data) {
                        if (data) {
                            if (data.DocumentNumber) {
                                if (data.Revision !== undefined && data.Revision !== 0 && data.Revision !== null) {
                                    return data.DocumentNumber + " (" + data.Revision + ")";
                                } else {
                                    return data.DocumentNumber;
                                }
                            } else {
                                return "";
                            }

                        }
                    }
                        
                },
              { field: 'Title', title: 'Proposal title', width: '170px' },
              //{ field: 'Place', title: 'Store / Branch', width: '150px' },
              { field: 'UnitCode', title: 'Unit Code', width: '105px', },
              //{ field: 'UnitCode', title: 'Unit Code', width: '150px', template: "#=data.UnitCode# - #=data.UnitName#" },
              { field: 'ProposalTypeName', title: 'Proposal Type', width: '130px' },
              { field: 'StartDate', title: 'Start Date', width: '120px', template: inquiryGrid.formatDate('StartDate') },
              { field: 'EndDate', title: 'End Date', width: '115px', template: inquiryGrid.formatDate('EndDate') },

             inquiryGrid.createdDateColumn

            ];
       
      
        // $scope.gridKendoOption.toolbar = kendo.template($("#ToolbarTemplate").html());


    }]);