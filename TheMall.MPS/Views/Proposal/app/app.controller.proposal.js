﻿angular.module('app.controllers').controller('proposalController',
[
    '$scope', '$routeParams', 'inquiryGrid', 'IdProposalService', '$location',
     'proposalDataSource', '$window', 'messageBox', 'sweetAlert',
    function ($scope, $routeParams, inquiryGrid, idProposalService, $location,
         proposalDataSource, $window, messageBox, sweetAlert) {

        var params = {
            filters: [
                { field: "RequesterUserName", operator: "eq", value: CURRENT_USERNAME },
                {
                    field: "Deleted",
                    operator: "eq",
                    value: false
                }
            ],
           
            height: '350px',
            //height: 'auto',
            pageSize: 5,
            scrollable: false,
            sorts: inquiryGrid.sortOption
        };

        var commandTemplateAllstatus = function (data) {
            if ((data.StatusFlag == null || data.StatusFlag === 0) && data.RequesterUserName && data.RequesterUserName.toUpperCase() === CURRENT_USERNAME.toUpperCase()) {
                return '<div class="btn-group" role="group">' +
                    '<a class="btn btn-default" style="margin-right: 5px;" href="' + $routeParams.page + '/detail/' + data.Id + '" title="view"><i class="fa fa-file-text"></i></a>' +
                    '<a class="btn btn-default" style="margin-right: 5px;" href="' + $routeParams.page + '/create/' + data.Id + '" title="edit"><i class="fa fa-edit"></i></a>' +
                    '<a class="btn btn-info" disabled="disabled" style="margin-right:5px; margin-bottom: 10px;"><span class="fa fa-print"></span></a>' +
                    '<a class="btn btn-default"  style=" background-color:#dd4b39;" ng-click="delete(' + data.Id + ')" title="Delete"><i class="fa fa-trash-o" style="color:white;"></i></a>' +
                    '</div>';
            }
            else if ((data.StatusFlag === 9 && data.CloseFlag) && data.RequesterUserName && data.RequesterUserName.toUpperCase() === CURRENT_USERNAME.toUpperCase()) {
                return '<div class="btn-group" role="group">' +
                          '<a class="btn btn-default" style="margin-right: 5px;" href="' + $routeParams.page + '/detail/' + data.Id + '" title="view"><i class="fa fa-file-text"></i></a>' +
                          '<span class="btn btn-default" disabled="disabled" style="margin-right: 5px;" title="edit"><i class="fa fa-edit"></i></span>' +
                          '<a class="btn btn-info"  style="margin-right:5px; margin-bottom: 10px;" ng-href="{{printReport(' + data.Id + ')}}" target="_blank"><span class="fa fa-print"></span></a>' +
                          '<span class="btn btn-default" disabled="disabled" style="margin-right: 5px; background-color:#dd4b39" title="Delete"><i class="fa fa-trash-o" style="color:white"></i></span>' +
                          '</div>';
            }
            else if (((data.StatusFlag === 9 || data.StatusFlag === 8) && !data.CloseFlag) && data.RequesterUserName && data.RequesterUserName.toUpperCase() === CURRENT_USERNAME.toUpperCase()) {
                if (data.StatusFlag === 9) {
                    if (data.ProposalTypeCode === "DEPOSIT" || data.ProposalTypeCode === "DEPOSITIN") {
                        return '<div class="btn-group" role="group">' +
                            '<a class="btn btn-default" style="margin-right: 5px;" href="' + $routeParams.page + '/detail/' + data.Id + '" title="view"><i class="fa fa-file-text"></i></a>' +
                            '<span class="btn btn-default" disabled="disabled" style="margin-right: 5px;" title="edit"><i class="fa fa-edit"></i></span>' +
                            '<a class="btn btn-info"  style="margin-right:5px; margin-bottom: 10px;" ng-href="{{printReport(' + data.Id + ')}}" target="_blank"><span class="fa fa-print"></span></a>' +
                            '<span class="btn btn-default" disabled="disabled" style="margin-right: 5px; background-color:#dd4b39" title="Delete"><i class="fa fa-trash-o" style="color:white"></i></span>' +
                            '</div>';

                    } else {
                        return '<div class="btn-group" role="group">' +
                            '<a class="btn btn-default" style="margin-right: 5px;" href="' + $routeParams.page + '/detail/' + data.Id + '" title="view"><i class="fa fa-file-text"></i></a>' +
                            '<a class="btn btn-warning" style="margin-right: 5px;" ng-click="reviseProposal(' + data.Id + ')" title="revise"><i class="fa fa-reply"></i></a>' +
                            '<a class="btn btn-info"  style="margin-right:5px; margin-bottom: 10px;" ng-href="{{printReport(' + data.Id + ')}}" target="_blank"><span class="fa fa-print"></span></a>' +
                            '<span class="btn btn-default" disabled="disabled" style="margin-right: 5px; background-color:#dd4b39" title="Delete"><i class="fa fa-trash-o" style="color:white"></i></span>' +
                            '</div>';
                    }
                } else if (data.StatusFlag === 8) {
                    if (data.DocumentStatus === "Draft") {
                        return '<div class="btn-group" role="group">' +
                            '<a class="btn btn-default" style="margin-right: 5px;" href="' + $routeParams.page + '/detail/' + data.Id + '" title="view"><i class="fa fa-file-text"></i></a>' +
                            '<a class="btn btn-warning" style="margin-right: 5px;" href="' + $routeParams.page + '/reviseproposal/' + data.Id + '" title="revise"><i class="fa fa-reply"></i></a>' +
                            '<a class="btn btn-info" disabled="disabled" style="margin-right:5px; margin-bottom: 10px"><span class="fa fa-print"></span></a>' +
                            '<span class="btn btn-default" disabled="disabled" style="margin-right: 5px; background-color:#dd4b39" title="Delete"><i class="fa fa-trash-o" style="color:white"></i></span>' +
                            '</div>';
                    } else {
                        return '<div class="btn-group" role="group">' +
                            '<a class="btn btn-default" style="margin-right: 5px;" href="' + $routeParams.page + '/detail/' + data.Id + '" title="view"><i class="fa fa-file-text"></i></a>' +
                            '<span class="btn btn-default" disabled="disabled" style="margin-right: 5px;" title="edit"><i class="fa fa-edit"></i></span>' +
                            '<a class="btn btn-info"  style="margin-right:5px; margin-bottom: 10px;" ng-href="{{printReport(' + data.Id + ')}}" target="_blank"><span class="fa fa-print"></span></a>' +
                            '<span class="btn btn-default" disabled="disabled" style="margin-right: 5px; background-color:#dd4b39" title="Delete"><i class="fa fa-trash-o" style="color:white"></i></span>' +
                            '</div>';
                    }
                }
            }

            return '<div class="btn-group" role="group">' +
                    '<a class="btn btn-default" style="margin-right: 5px;" href="' + $routeParams.page + '/detail/' + data.Id + '" title="view"><i class="fa fa-file-text"></i></a>' +
                    '<span class="btn btn-default" disabled="disabled" style="margin-right: 5px;" title="edit"><i class="fa fa-edit"></i></span>' +
                    '<a class="btn btn-info" disabled="disabled" style="margin-right:5px; margin-bottom: 10px"><span class="fa fa-print"></span></a>' +
                    '<span class="btn btn-default" disabled="disabled" style="margin-right: 5px; background-color:#dd4b39" title="Delete"><i class="fa fa-trash-o" style="color:white"></i></span>' +
                '</div>';
        };


        $scope.mygridKendoOption = inquiryGrid.gridKendoOption(angular.crs.url.odata('proposaltable'), params);

        $scope.mygridKendoOption.columns =
            [
               //inquiryGrid.commandTemplateCurrentUserName,
                { template: commandTemplateAllstatus, width: '117px' },
              { field: 'Status', title: 'Status', width: '125px' },
                {
                    field: 'DocumentNumber', title: 'Proposal number', width: '130px' ,
                    template: function (data) {
                        if (data) {
                            if (data.DocumentNumber) {
                                if (data.Revision !== undefined && data.Revision !== 0 && data.Revision !== null) {
                                    return data.DocumentNumber + " (" + data.Revision + ")";
                                } else {
                                    return data.DocumentNumber;
                                }
                            } else {
                                return "";
                            }
                        }
                    }
                },
                {
                    field: 'Title', title: 'Proposal title', width: '150px',
                    template: function (data) {
                        if (data.Title !== undefined) {
                            return "<div style='width:100%; word-wrap: break-word;'>" + data.Title + "</div>";
                        }
                        return "";
                    }
                },
              //{ field: 'Place', title: 'Store / Branch', width: '150px' },
              //{ field: 'UnitCode', title: 'Unit Code', width: '105px', },
              //{ field: 'UnitCode', title: 'Unit Code', width: '150px', template: "#=data.UnitCode# - #=data.UnitName#" },
              { field: 'ProposalTypeName', title: 'Proposal type', width: '130px' },
              { field: 'StartDate', title: 'Start date', width: '105px', template: inquiryGrid.formatDate('StartDate') },
              { field: 'EndDate', title: 'End date', width: '100px', template: inquiryGrid.formatDate('EndDate') },
              inquiryGrid.createdDateColumn

            ];
        $scope.mygridKendoOption.toolbar = kendo.template($("#ToolbarTemplate").html());


        //PrintReport
        $scope.printReport = function (advanceId) {
            if (advanceId) {
                var params = {
                    Id: advanceId,
                    inline: true,
                    //show: true,
                    fullscreen: true
                };
                var url = angular.crs.url.rootweb("report/Proposal/?params=" + JSON.stringify(params));
                return url;
            }
        };


        //Delete
        $scope.delete = function(id) {
            sweetAlert.swal({
                title: 'Are you sure?',
                text: "Delete this record ?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then(function(isConfirm) {
                if (isConfirm) {
                    var result = proposalDataSource.deleteBy({ id: id, username: CURRENT_USERNAME },
                        function(data) {
                            sweetAlert.success("Delete data succesful");
                            $location.path('/proposal');
                        }, sweetAlert.error("You can't delete data!"));
                }
            });
        };

        //
        $scope.reviseProposal = function (id) {
            sweetAlert.swal({
                text: "Do you want to revise this proposal ?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then(function(isConfirm) {
                if (isConfirm) {
                    var url = angular.crs.url.rootweb("proposal/reviseproposal/" + id);
                    $window.location.href = url;
                    // $location.path('proposal/reviseproposal/' + id);
                }
            });
        };
        //if ($window.confirm("Delete this record ?")) {
            //    var result = proposalDataSource.deleteBy({ id: id, username: CURRENT_USERNAME },
            //         function (data) {
            //             messageBox.success("Delete data succesful");
            //             $location.path('/proposal');
            //         });
            //} else {
            //    return false;
            //}
        //}
    }]);