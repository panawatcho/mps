﻿app = angular.module("app.controllers");
app.factory('IdProposalService', function () {

    var proposalId = 0;

    function set(id) {
        proposalId = id;
    }

    function get() {
        return proposalId;
    }

    return {
        set: set,
        get: get
    }

    //var Id = 0;

    //function set(data) {
    //    Id = data;
    //}

    //function get() {
    //    return Id;
    //}

    //return {
    //    set: set,
    //    get: get
    //}
});