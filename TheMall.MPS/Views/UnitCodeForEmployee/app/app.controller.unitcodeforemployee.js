﻿angular.module('app.controllers').controller('unitcodeforemployeecontroller',
[
    '$scope', '$routeParams', 'masterGrid', 'messageBox', 'unitCodeForEmployeeDataSource', 'lineGrid',
    '$http','inquiryGrid',
    function ($scope, $routeParams, masterGrid, messageBox, unitCodeForEmployeeDataSource, lineGrid, $http, inquiryGrid) {
        var _gridKendoOption;

        $scope.employeeOption = {
            dataSource: {
                transport: {
                    serverFiltering: true,
                    read: {
                        dataType: "json",  //instead of "type: 'jsonp',"
                        url: angular.crs.url.webApi('employee/search')
                    }
                }
            },
            optionLabel: "please select",
            dataTextField: 'FullName',
            dataValueField: 'FullName',
            template: '#=data.FullName#',
            valueTemplate: '#=data.FullName#',
            filter: "contains"
        }

       $scope.unitcodeOption =  {
            dataTextField: "UnitName",
            dataValueField: "UnitCode",
            optionLabel: "please select",
            dataSource: {
                type: 'json',
                serverFiltering: true,
                transport: {
                    read: {
                            url: function () {
                                return angular.crs.url.webApi('unitcode/search');
                            }
                    }
                }
            }
       }

       $scope.apiUrl = angular.crs.url.webApi('unitcodeforemployee');


       //$http.get($scope.apiUrl).then(function (response) {
       //    $scope.data = response.data;
       //});
       var _commandTemplateAllstatus = function (data) {
           return '<div class="btn-group" role="group">' +
               '<a class="btn btn-default" style="width:60px" href="' + $routeParams.page + '/Form?EmpId=' + data.EmpId + '&UnitCode=' + data.UnitCode.UnitCode + '" title="edit"><i class="fa fa-edit"></i></a>' +
               '</div>';
       }
        console.log($routeParams.page);
        $scope.gridKendoOption = {
            dataSource: new kendo.data.DataSource({
                transport: {
                    read: {
                        async: true,
                        url: $scope.apiUrl,
                        dataType: "json"
                    },
                    parameterMap: function(options, operation) {
                        if (operation !== "read" && options.models) {
                            return {
                                EmpId: options.EmpId[0].EmpId,
                                Employee: options.models[0].Employee,
                                Order: options.models[0].Order,
                                UnitCode: options.EmpId[0].UnitCode,
                                InActive: options.models[0].InActive
                            };
                        }
                        return null;
                    }
                },
                pageSize: 10,
                batch:true,
                schema: {
                    model: {
                        id: "Id",
                        fields: {
                            EmpId: { editable: true, nullable: false },
                            Employee: { editable: true, nullable: false },
                            UnitCode: { editable: true, nullable: false },
                            InActive: { type: "boolean", editable: true, nullable: false },
                            Order: { editable: true, nullable: false },
                        }
                    }
                }
            }),
            toolbar: ["create"],
            columns: [
                //{ field: "EmpId", title: "Employee id", width: "200px" },
                {
                    field: "Employee.Username",
                    title: "Username",
                    width: "100px",
                    template: function(data) {
                        if (data.Employee) {
                            return data.Employee.Username;
                        }
                        return null;
                    }
                },
                  {
                      field: "Employee.FullName",
                      title: "Name",
                      width: "100px",
                      template: function (data) {
                          if (data.Employee) {
                              return data.Employee.FullName;
                          }
                          return null;
                      }
                  },
                {
                    field: "UnitCode.UnitCode",
                    title: "Unit code",
                    width: "300px",
                    template: function(data) {
                        if (data.UnitCode) {
                            return data.UnitCode.UnitCode +' - '+data.UnitCode.UnitName;
                        }
                        return null;
                    }
                },
                {
                    field: "InActive",
                    title: "Active",
                    width: "70px",
                    template: '<div class="text-center">#if(!data.InActive){# <i class="fa fa-check" style="color:green;"></i>#}else{#<i class="fa fa-times" style="color:red;"></i> #}#</div>'
                    //template: function(data) {
                       
                    //    return '<center><input type="checkbox" disabled="disabled" ng-checked="' + !data.InActive + '"/></center>';
                    //}
                },
                 {
                     field: "Order", title: "Order", width: "70px",
                     template: inquiryGrid.formatCurrency('Order')
                 },
                {
                    template: _commandTemplateAllstatus, width: '110px'
                }
            ],
            filterable: true,
            pageable: {
                refresh: true,
                pageSizes: true,
                buttonCount: 5
            }
        };
        $scope.gridKendoOption.toolbar = kendo.template($("#unitCodeForEmployeeTemplate").html());
    }
]);