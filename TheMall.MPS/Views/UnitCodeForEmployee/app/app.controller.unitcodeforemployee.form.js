﻿angular.module('app.controllers').controller('unitcodeForEmployeeFormController',
[
    '$scope', '$routeParams', 'masterGrid', 'messageBox',
    'unitCodeForEmployeeDataSource', 'lineGrid','$location',
    '$http','controlOptions',
    function($scope, $routeParams, masterGrid, messageBox,
        unitCodeForEmployeeDataSource, lineGrid, $location,
        $http, controlOptions) {
        $scope.form = {};

        $scope.employeeOption = controlOptions.dropdown('employee');

        $scope.unitcodeOption = controlOptions.dropdown('unitcode');
       


        if (!$routeParams.UnitCode || !$routeParams.EmpId) {
            $scope.canDeleted = false;
            $scope.edit = false;
            //   messageBox.error("Sequence number and name are invalid!!!");

            //$location.path("unitcodeforemployee/");
        } else {
            $scope.canDeleted = true;
            $scope.edit = true;
            unitCodeForEmployeeDataSource.Search({ empId: $routeParams.EmpId, unitCode: $routeParams.UnitCode }, function(data) {
                if (data.length > 0) {
                    $scope.form = data[0];
                }
            });
        }

        $scope.validate = function (event) {
            if (!$scope.validator.validate()) return false;
            unitCodeForEmployeeDataSource.Save($scope.form).$promise.then(
                function (resp) {
                    if (resp) {
                        messageBox.success('Saved successfully');
                        $location.path('unitcodeforemployee/');
                    }
                },
                function (error) {
                    messageBox.extractError(error);
                }
            );
        }

        $scope.delete = function () {
            var deleted = confirm("Delete this data ?");
            if (deleted) {
                unitCodeForEmployeeDataSource.Delete({ empId: $routeParams.EmpId, unitCode: $routeParams.UnitCode },function (resp) {
                    //messageBox.success("Delete successfully.");
                    $location.path('unitcodeforemployee/');
                });
            } 
        }
    }
]);