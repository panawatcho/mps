﻿angular.module('app.controllers').controller('sharedBranchFormController',
[
    '$scope', '$routeParams', 'masterGrid', 'controlOptions',
    'sharedBranchDataSource', 'messageBox', '$location',
    function($scope, $routeParams, masterGrid, controlOptions,
        dataSource, messageBox, $location) {
        $scope.form = {};
        $scope.checkNew = true;
        $scope.checkMemoType = false;
        $scope.temp = {};


        if (!$routeParams.branchCode) {
            $scope.canDeleted = false;
            $scope.edit = false;
            //   messageBox.error("Sequence number and name are invalid!!!");

            //$location.path("unitcodeforemployee/");
        } else {
            $scope.canDeleted = true;
            $scope.edit = true;
            dataSource.getByBranchCode({
                branchCode: $routeParams.branchCode
            }, function (data) {
                $scope.checkNew = false;
                $scope.form = data;
            });
        }

        $scope.validate = function(event) {
            if (!$scope.validator.validate()) return false;
            if ($scope.checkNew) {
                dataSource.save($scope.form).$promise.then(
                    function(resp) {
                        if (resp) {
                            messageBox.success('Saved successfully');
                            $location.path('sharedbranch/');
                        }
                    },
                    function(error) {
                        messageBox.extractError(error);
                    }
                );
            } else {
                dataSource.edit($scope.form).$promise.then(
                    function(resp) {
                        if (resp) {
                            messageBox.success('Saved successfully');
                            $location.path('sharedbranch/');
                        }
                    },
                    function(error) {
                        messageBox.extractError(error);
                    }
                );
            }
        };

        $scope.delete = function () {
            var deleted = confirm("Delete this data ?");
            if (deleted) {
                dataSource.deleted($scope.form).$promise.then(
                     function (resp) {
                         if (resp) {
                             messageBox.success("Delete successfully.");
                             $location.path('sharedbranch/');
                         }
                     },
                    function (error) {
                        messageBox.extractError(error);
                    }
                    );
            }
        }
    }
]);