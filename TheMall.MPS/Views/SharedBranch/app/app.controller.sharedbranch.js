﻿angular.module('app.controllers').controller('sharedbranchcontroller',
[
    '$scope', '$routeParams', 'masterGrid','inquiryGrid',
    function ($scope, $routeParams, masterGrid, inquiryGrid) {

        var _gridKendoOption;
        $scope.odataUrl = angular.crs.url.odata('sharedbranchtable');
        $scope.apiUrl = angular.crs.url.webApi('sharedbranch');
        var _commandTemplateAllstatus = function (data) {
            return '<div class="btn-group" role="group">' +
                '<a class="btn btn-default" style="width:60px" href="' + $routeParams.page + '/Form?branchCode=' + data.BranchCode + '" title="edit"><i class="fa fa-edit"></i></a>' +
                '</div>';
        }
        $scope.gridKendoOption = {
            dataSource: new kendo.data.DataSource({
                transport: {
                    read: {
                        async: true,
                        url: $scope.apiUrl,
                        dataType: "json"
                    },
                    parameterMap: function(options, operation) {
                        if (operation !== "read" && options.models) {
                            return {
                                BranchCode: options.models[0].BranchCode,
                                BranchName: options.models[0].BranchName,
                                InActive: options.models[0].InActive,
                                Order: options.models[0].Order
                            };
                        }
                        return null;
                    }
                },
                pageSize: 10,
                batch: true,
                schema: {
                    model: {
                        fields:
                        {
                            BranchCode: { editable: true, nullable: false },
                            BranchName: { editable: true, nullable: false },
                            InActive: { editable: true, nullable: false },
                            Order: { editable: true, nullable: false }
                        }
                    }
                }
            }),
            toolbar: ["create"],
            columns: [
                {
                    field: "BranchCode", title: "Branch code", width: "200px",
                    template: function (data) {
                        if (data.BranchCode) {
                            return "<div style='width:100%; word-wrap: break-word;'>" + data.BranchCode + "</div>";
                        }
                        return "";
                    }
                },
                {
                    field: "BranchName", title: "Branch name", width: "200px",
                    template: function (data) {
                        if (data.BranchName) {
                            return "<div style='width:100%; word-wrap: break-word;'>" + data.BranchName + "</div>";
                        }
                        return "";
                    }
                },
                {
                    field: "InActive",
                    title: "Active",
                    width: "200px",
                    template: '<div class="text-center">#if(!data.InActive){# <i class="fa fa-check" style="color:green;"></i>#}else{#<i class="fa fa-times" style="color:red;"></i> #}#</div>',
                    editor: '<input type="checkbox" name="InActive" data-bind="checked:InActive"></input>'
                },
                {
                    field: "Order", title: "Order", width: "200px",
                    template: inquiryGrid.formatCurrency('Order')
                },
                { template: _commandTemplateAllstatus, width: '100px' }
            ],
            filterable: true,
            pageable: {
                refresh: true,
                pageSizes: true,
                buttonCount: 5
            }
        };
        $scope.gridKendoOption.toolbar = kendo.template($("#sharedBranchTemplate").html());
        //$scope.gridKendoOption = function() {
        //    if (!_gridKendoOption) {
        //        $scope.odataUrl = angular.crs.url.odata('sharedbranchtable');
        //        $scope.apiUrl = angular.crs.url.webApi('sharedbranch');
        //        _gridKendoOption = masterGrid.gridKendoOption($scope.odataUrl, $scope.apiUrl,
        //        {
        //            parameterMap: function (options, operation) {
        //                if (operation !== "read" && options.models) {
        //                    return {
        //                        BranchCode: options.models[0].BranchCode,
        //                        BranchName: options.models[0].BranchName,
        //                        InActive: options.models[0].InActive
        //                    };
        //                }
        //                return null;
        //            },
        //            schema: {
        //                model: {
        //                    id: "BranchCode",
        //                    fields: {
        //                        BranchCode: { editable: true, nullable: false },
        //                        BranchName: { editable: true, nullable: false },
        //                        InActive: { editable: true, nullable: false }
        //                    }
        //                }
        //            },
        //            toolbar: ["create"],
        //            columns: [
        //                { field: "BranchCode", title: "Branch code", width: "200px" },
        //                { field: "BranchName", title: "Branch name", width: "200px" },
        //                {
        //                    field: "Active", title: "In active", width: "200px",
        //                    template:'<div class="text-center">#if(!data.InActive){# <iclass="fa fa-check" style="color:green;"></i>#}else{#<i class="fa fa-times" style="color:red;"></i> #}#</div>',
        //                    editor: '<input type="checkbox" name="InActive" data-bind="checked:InActive"></input>'
        //                },
        //                { command: ["edit", "destroy"], title: "&nbsp;", width: 200 }
        //            ],
        //            editable: {
        //                mode: "inline",
        //                createAt: "top"
        //            }
        //        });
        //        }
        //    return _gridKendoOption;
   
           // }
    }
]);