﻿angular.module('app.controllers').controller('employeeController',
[
    '$scope', '$routeParams', 'masterGrid',
    function($scope, $routeParams, masterGrid) {
        var _gridKendoOption;

        var _commandTemplateAllstatus = function (data) {
            return '<div class="btn-group" role="group">' +
                '<a class="btn btn-default" style="width:60px" href="' + $routeParams.page + '/Form?EmpId=' + data.EmpId +'" title="edit"><i class="fa fa-edit"></i></a>' +
                '</div>';
        }

        $scope.apiUrl = angular.crs.url.webApi('employee');
        $scope.gridKendoOption = {
            dataSource: new kendo.data.DataSource({
                transport: {
                    read: {
                        async: true,
                        url: $scope.apiUrl,
                        dataType: "json"
                    },
                    parameterMap: function (options, operation) {
                        if (operation !== "read" && options.models) {
                            return {
                                EmpId: options.models[0].EmpId,
                                EmpStatus: options.models[0].EmpStatus,
                                ThFName: options.models[0].ThFName,
                                ThLName: options.models[0].ThLName,
                                EnFName: options.models[0].EnFName,
                                EnLName: options.models[0].EnLName,
                                DepartmentCode: options.models[0].DepartmentCode,
                                DepartmentName: options.models[0].DepartmentName,
                                PositionCode: options.models[0].PositionCode,
                                PositionName: options.models[0].PositionName,
                                Email: options.models[0].Email,
                                TelephoneNo: options.models[0].TelephoneNo,
                                CompanyCode: options.models[0].CompanyCode,
                                CompanyName: options.models[0].CompanyName,
                                Username: options.models[0].Username,
                                DivisionCode: options.models[0].DivisionCode,
                                DivisionName: options.models[0].DivisionName,
                                Authority: options.models[0].Authority
                            };
                        }
                        return null;
                    }
                },
                pageSize: 10,
                batch: true,
                schema: {
                    model: {
                        id: "Id",
                        fields: {
                            EmpId: { editable: false, nullable: false },
                            EmpStatus:{editable: false, nullable: false },
                            Username: { editable: false, nullable: false },
                            ThFName: { editable: false, nullable: false },
                            ThLName: { editable: false, nullable: false },
                            EnFName: { editable: false, nullable: false },
                            EnLName: { editable: false, nullable: false },
                            DepartmentCode: { editable: false, nullable: false },
                            DepartmentName: { editable: false, nullable: false },
                            PositionCode: { editable: false, nullable: false },
                            PositionName: { editable: false, nullable: false },
                            Email: { editable: false, nullable: false },
                            TelephoneNo: { editable: false, nullable: false },
                            CompanyCode: { editable: false, nullable: false },
                            CompanyName: { editable: false, nullable: false },
                            DivisionCode: { editable: false, nullable: false },
                            DivisionName: { editable: false, nullable: false },
                            Authority: { editable: false, nullable: false }
                        }
                    }
                }
            }),
            toolbar: ["create"],
            columns: [
                {
                    template: _commandTemplateAllstatus, width: '110px'
                },
                       { field: "EmpId", title: "Employee id", width: "110px" },
                       {
                           field: "EmpStatus",
                           title: "Employee status",
                           width: "110px",
                           template: function(data) {
                               if (data.EmpStatus) {
                                   return '<div style="width:100%; word-wrap: break-word;">' + data.EmpStatus + '</div>';
                               } else {
                                   return "";
                               }
                           }
                       },
                       {
                           field: "Username",
                           title: "Username",
                           width: "120px",
                           template: function(data) {
                               if (data.Username) {
                                   return '<div style="width:100%; word-wrap: break-word;">' + data.Username + '</div>';
                               } else {
                                   return "";
                               }
                           }
                       },
                       {
                           field: "ThFName",
                           title: "First name in Thai",
                           width: "150px",
                           template: function(data) {
                               if (data.ThFName) {
                                   return '<div style="width:100%; word-wrap: break-word;">' + data.ThFName + '</div>';
                               } else {
                                   return "";
                               }
                           }
                       },
                       {
                           field: "ThLName",
                           title: "Last name in Thai",
                           width: "140px",
                           template: function(data) {
                               if (data.ThLName) {
                                   return '<div style="width:100%; word-wrap: break-word;">' + data.ThLName + '</div>';
                               } else {
                                   return "";
                               }
                           }
                       },
                       {
                           field: "EnFName",
                           title: "First name in English",
                           width: "150px",
                           template: function(data) {
                               if (data.EnFName) {
                                   return '<div style="width:100%; word-wrap: break-word;">' + data.EnFName + '</div>';
                               } else {
                                   return "";
                               }
                           }
                       },
                       {
                           field: "EnLName",
                           title: "Last name in Endlish",
                           width: "140px",
                           template: function(data) {
                               if (data.EnLName) {
                                   return '<div style="width:100%; word-wrap: break-word;">' + data.EnLName + '</div>';
                               } else {
                                   return "";
                               }
                           }
                       },
                       {
                           field: "DepartmentCode",
                           title: "Department code",
                           width: "120px",
                           template: function(data) {
                               if (data.DepartmentCode) {
                                   return '<div style="width:100%; word-wrap: break-word;">' + data.DepartmentCode + '</div>';
                               } else {
                                   return "";
                               }
                           }
                       },
                       {
                           field: "DepartmentName",
                           title: "Department name",
                           width: "120px",
                           template: function(data) {
                               if (data.DepartmentName) {
                                   return '<div style="width:100%; word-wrap: break-word;">' + data.DepartmentName + '</div>';
                               } else {
                                   return "";
                               }
                           }
                       },
                       {
                           field: "PositionCode",
                           title: "Position code",
                           width: "120px",
                           template: function(data) {
                               if (data.PositionCode) {
                                   return '<div style="width:100%; word-wrap: break-word;">' + data.PositionCode + '</div>';
                               } else {
                                   return "";
                               }
                           }
                       },
                       {
                           field: "PositionName",
                           title: "Position name",
                           width: "120px",
                           template: function(data) {
                               if (data.PositionName) {
                                   return '<div style="width:100%; word-wrap: break-word;">' + data.PositionName + '</div>';
                               } else {
                                   return "";
                               }
                           }
                       },
                       {
                           field: "Email",
                           title: "Email",
                           width: "150px",
                           template: function(data) {
                               if (data.Email) {
                                   return '<div style="width:100%; word-wrap: break-word;">' + data.Email + '</div>';
                               } else {
                                   return "";
                               }
                           }
                       },
                       {
                           field: "TelephoneNo",
                           title: "Telephone number",
                           width: "120px",
                           template: function(data) {
                               if (data.TelephoneNo) {
                                   return '<div style="width:100%; word-wrap: break-word;">' + data.TelephoneNo + '</div>';
                               } else {
                                   return "";
                               }
                           }
                       },
                       {
                           field: "CompanyCode",
                           title: "Company code",
                           width: "120px",
                           template: function(data) {
                               if (data.CompanyCode) {
                                   return '<div style="width:100%; word-wrap: break-word;">' + data.CompanyCode + '</div>';
                               } else {
                                   return "";
                               }
                           }
                       },
                       {
                           field: "CompanyName",
                           title: "Company name",
                           width: "120px",
                           template: function(data) {
                               if (data.CompanyName) {
                                   return '<div style="width:100%; word-wrap: break-word;">' + data.CompanyName + '</div>';
                               } else {
                                   return "";
                               }
                           }
                       },
                       {
                           field: "DivisionCode",
                           title: "Division code",
                           width: "120px",
                           template: function(data) {
                               if (data.DivisionCode) {
                                   return '<div style="width:100%; word-wrap: break-word;">' + data.DivisionCode + '</div>';
                               } else {
                                   return "";
                               }
                           }
                       },
                       {
                           field: "DivisionName",
                           title: "Division name",
                           width: "120px",
                           template: function(data) {
                               if (data.DivisionName) {
                                   return '<div style="width:100%; word-wrap: break-word;">' + data.DivisionName + '</div>';
                               } else {
                                   return "";
                               }
                           }
                       },
                       {
                           field: "Authority",
                           title: "Authority",
                           width: "120px",
                           template: function(data) {
                               if (data.Authority) {
                                   return '<div style="width:100%; word-wrap: break-word;">' + data.Authority + '</div>';
                               } else {
                                   return "";
                               }
                           }
                       },
                        
            ],
            filterable: true,
            pageable: {
                refresh: true,
                pageSizes: true,
                buttonCount: 5
            }
        };
        $scope.gridKendoOption.toolbar = kendo.template($("#employeeTemplate").html());

        //$scope.gridKendoOption = function () {

            //if (!_gridKendoOption) {
            //    $scope.odataUrl = angular.crs.url.odata('employeetable');
            //    $scope.apiUrl = angular.crs.url.webApi('employee');
            //    _gridKendoOption = masterGrid.gridKendoOption($scope.odataUrl, $scope.apiUrl,
            //    {
            //        parameterMap: function(options, operation) {
            //            if (operation !== "read" && options.models) {
            //                return {
            //                    EmpId: options.models[0].EmpId,
            //                    EmpStatus: options.models[0].EmpStatus,
            //                    ThFName: options.models[0].ThFName,
            //                    ThLName: options.models[0].ThLName,
            //                    EnFName: options.models[0].EnFName,
            //                    EnLName: options.models[0].EnLName,
            //                    DepartmentCode: options.models[0].DepartmentCode,
            //                    DepartmentName: options.models[0].DepartmentName,
            //                    PositionCode: options.models[0].PositionCode,
            //                    PositionName: options.models[0].PositionName,
            //                    Email: options.models[0].Email,
            //                    TelephoneNo: options.models[0].TelephoneNo,
            //                    CompanyCode: options.models[0].CompanyCode,
            //                    CompanyName: options.models[0].CompanyName,
            //                    Username: options.models[0].Username,
            //                    DivisionCode: options.models[0].DivisionCode,
            //                    DivisionName: options.models[0].DivisionName,
            //                    Authority: options.models[0].Authority
            //                };
            //            }
            //            return null;
            //        },
            //        schema: {
            //            model: {
            //                id: "EmpId",
            //                fields: {
            //                    EmpId: { editable: false, nullable: false },
            //                    EmpStatus:{editable: false, nullable: false },
            //                    Username: { editable: false, nullable: false },
            //                    ThFName: { editable: false, nullable: false },
            //                    ThLName: { editable: false, nullable: false },
            //                    EnFName: { editable: false, nullable: false },
            //                    EnLName: { editable: false, nullable: false },
            //                    DepartmentCode: { editable: false, nullable: false },
            //                    DepartmentName: { editable: false, nullable: false },
            //                    PositionCode: { editable: false, nullable: false },
            //                    PositionName: { editable: false, nullable: false },
            //                    Email: { editable: false, nullable: false },
            //                    TelephoneNo: { editable: false, nullable: false },
            //                    CompanyCode: { editable: false, nullable: false },
            //                    CompanyName: { editable: false, nullable: false },
            //                    DivisionCode: { editable: false, nullable: false },
            //                    DivisionName: { editable: false, nullable: false },
            //                    Authority: { editable: false, nullable: false }
            //                }
            //            }
            //        },
            //        toolbar: ["create"],
            //        columns: [
            //            { field: "EmpId", title: "Employee id", width: "110px" },
            //            {
            //                field: "EmpStatus",
            //                title: "Employee status",
            //                width: "110px",
            //                template: function(data) {
            //                    if (data.EmpStatus) {
            //                        return '<div style="width:100%; word-wrap: break-word;">' + data.EmpStatus + '</div>';
            //                    } else {
            //                        return "";
            //                    }
            //                }
            //            },
            //            {
            //                field: "Username",
            //                title: "Username",
            //                width: "120px",
            //                template: function(data) {
            //                    if (data.Username) {
            //                        return '<div style="width:100%; word-wrap: break-word;">' + data.Username + '</div>';
            //                    } else {
            //                        return "";
            //                    }
            //                }
            //            },
            //            {
            //                field: "ThFName",
            //                title: "First name in Thai",
            //                width: "150px",
            //                template: function(data) {
            //                    if (data.ThFName) {
            //                        return '<div style="width:100%; word-wrap: break-word;">' + data.ThFName + '</div>';
            //                    } else {
            //                        return "";
            //                    }
            //                }
            //            },
            //            {
            //                field: "ThLName",
            //                title: "Last name in Thai",
            //                width: "140px",
            //                template: function(data) {
            //                    if (data.ThLName) {
            //                        return '<div style="width:100%; word-wrap: break-word;">' + data.ThLName + '</div>';
            //                    } else {
            //                        return "";
            //                    }
            //                }
            //            },
            //            {
            //                field: "EnFName",
            //                title: "First name in English",
            //                width: "150px",
            //                template: function(data) {
            //                    if (data.EnFName) {
            //                        return '<div style="width:100%; word-wrap: break-word;">' + data.EnFName + '</div>';
            //                    } else {
            //                        return "";
            //                    }
            //                }
            //            },
            //            {
            //                field: "EnLName",
            //                title: "Last name in Endlish",
            //                width: "140px",
            //                template: function(data) {
            //                    if (data.EnLName) {
            //                        return '<div style="width:100%; word-wrap: break-word;">' + data.EnLName + '</div>';
            //                    } else {
            //                        return "";
            //                    }
            //                }
            //            },
            //            {
            //                field: "DepartmentCode",
            //                title: "Department code",
            //                width: "120px",
            //                template: function(data) {
            //                    if (data.DepartmentCode) {
            //                        return '<div style="width:100%; word-wrap: break-word;">' + data.DepartmentCode + '</div>';
            //                    } else {
            //                        return "";
            //                    }
            //                }
            //            },
            //            {
            //                field: "DepartmentName",
            //                title: "Department name",
            //                width: "120px",
            //                template: function(data) {
            //                    if (data.DepartmentName) {
            //                        return '<div style="width:100%; word-wrap: break-word;">' + data.DepartmentName + '</div>';
            //                    } else {
            //                        return "";
            //                    }
            //                }
            //            },
            //            {
            //                field: "PositionCode",
            //                title: "Position code",
            //                width: "120px",
            //                template: function(data) {
            //                    if (data.PositionCode) {
            //                        return '<div style="width:100%; word-wrap: break-word;">' + data.PositionCode + '</div>';
            //                    } else {
            //                        return "";
            //                    }
            //                }
            //            },
            //            {
            //                field: "PositionName",
            //                title: "Position name",
            //                width: "120px",
            //                template: function(data) {
            //                    if (data.PositionName) {
            //                        return '<div style="width:100%; word-wrap: break-word;">' + data.PositionName + '</div>';
            //                    } else {
            //                        return "";
            //                    }
            //                }
            //            },
            //            {
            //                field: "Email",
            //                title: "Email",
            //                width: "150px",
            //                template: function(data) {
            //                    if (data.Email) {
            //                        return '<div style="width:100%; word-wrap: break-word;">' + data.Email + '</div>';
            //                    } else {
            //                        return "";
            //                    }
            //                }
            //            },
            //            {
            //                field: "TelephoneNo",
            //                title: "Telephone number",
            //                width: "120px",
            //                template: function(data) {
            //                    if (data.TelephoneNo) {
            //                        return '<div style="width:100%; word-wrap: break-word;">' + data.TelephoneNo + '</div>';
            //                    } else {
            //                        return "";
            //                    }
            //                }
            //            },
            //            {
            //                field: "CompanyCode",
            //                title: "Company code",
            //                width: "120px",
            //                template: function(data) {
            //                    if (data.CompanyCode) {
            //                        return '<div style="width:100%; word-wrap: break-word;">' + data.CompanyCode + '</div>';
            //                    } else {
            //                        return "";
            //                    }
            //                }
            //            },
            //            {
            //                field: "CompanyName",
            //                title: "Company name",
            //                width: "120px",
            //                template: function(data) {
            //                    if (data.CompanyName) {
            //                        return '<div style="width:100%; word-wrap: break-word;">' + data.CompanyName + '</div>';
            //                    } else {
            //                        return "";
            //                    }
            //                }
            //            },
            //            {
            //                field: "DivisionCode",
            //                title: "Division code",
            //                width: "120px",
            //                template: function(data) {
            //                    if (data.DivisionCode) {
            //                        return '<div style="width:100%; word-wrap: break-word;">' + data.DivisionCode + '</div>';
            //                    } else {
            //                        return "";
            //                    }
            //                }
            //            },
            //            {
            //                field: "DivisionName",
            //                title: "Division name",
            //                width: "120px",
            //                template: function(data) {
            //                    if (data.DivisionName) {
            //                        return '<div style="width:100%; word-wrap: break-word;">' + data.DivisionName + '</div>';
            //                    } else {
            //                        return "";
            //                    }
            //                }
            //            },
            //            {
            //                field: "Authority",
            //                title: "Authority",
            //                width: "120px",
            //                template: function(data) {
            //                    if (data.Authority) {
            //                        return '<div style="width:100%; word-wrap: break-word;">' + data.Authority + '</div>';
            //                    } else {
            //                        return "";
            //                    }
            //                }
            //            },
            //            //{
            //            //    command: [
            //            //        {
            //            //            name: 'edit',
            //            //            text: '',
            //            //            className: 'grid-button-mini',
            //            //            title: 'edit'

            //            //        }, "destroy"
            //            //    ],
            //            //    title: "&nbsp;",
            //            //    width: 200
            //            //}
            //             {
            //                 template: _commandTemplateAllstatus, width: '110px'
            //             }
            //        ],
            //        editable: {
            //            mode: "inline",
            //            createAt: "top"
            //        },
            //        height: 600,
            //        filterable: true,
            //        pageable: {
            //            refresh: true,
            //            pageSizes: true,
            //            buttonCount: 5
            //        }
            //});
            //}
            //return _gridKendoOption;

        //}
    }
]);