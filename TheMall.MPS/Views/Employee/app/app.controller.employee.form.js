﻿angular.module('app.controllers').controller('employeeformcontroller',
[
    '$scope', '$routeParams', 'masterGrid', 'messageBox',
    'unitCodeForEmployeeDataSource', 'lineGrid',
    '$http','controlOptions','employeeDataSource','$location',
    function ($scope, $routeParams, masterGrid, messageBox,
        unitCodeForEmployeeDataSource, lineGrid,
        $http, controlOptions, employeeDataSource, $location) {
        $scope.form = {};

        //$scope.unitcodeOption = controlOptions.dropdown('unitcode');
        //$scope.statusOption = {
        //    dataTextField: "Status",
        //    dataValueField: "Status",
        //    optionLabel: "please select",
        //    dataSource: [
        //        {
        //            Status : "Active"
        //        },
        //        {
        //            Status: "InActive"
        //        }
        //    ]
        //}
        $scope.statusOption = controlOptions.dropdown('empstatus');
        $scope.checkNew = true;

        if (!$routeParams.EmpId) {
            $scope.canDeleted = false;
            $scope.edit = false;
            //   messageBox.error("Sequence number and name are invalid!!!");

            //$location.path("unitcodeforemployee/");
        } else {
            $scope.canDeleted = true;
            $scope.edit = true;
            employeeDataSource.searchById({ empId: $routeParams.EmpId }, function (data) {
                $scope.checkNew = false;
                $scope.form = data;
                console.log(data);
            });
        }


        $scope.validate = function (event) {
            if (!$scope.validator.validate()) return false;
            if ($scope.checkNew) {
                employeeDataSource.save($scope.form).$promise.then(
                    function(resp) {
                        if (resp) {
                            messageBox.success('Saved successfully');
                            $location.path('employee/');
                        }
                    },
                    function(error) {
                        messageBox.extractError(error);
                    }
                );
            } else {
                employeeDataSource.edit($scope.form).$promise.then(
                    function (resp) {
                        if (resp) {
                            messageBox.success('Saved successfully');
                            $location.path('employee/');
                        }
                    },
                    function (error) {
                        messageBox.extractError(error);
                    }
                );
            }
        }



        $scope.delete = function () {
            var deleted = confirm("Delete this data ?");
            if (deleted) {
                employeeDataSource.deleted({ empId: $routeParams.EmpId}, function (resp) {
                    messageBox.success("Delete successfully.");
                    $location.path('employee/');
                });
            }
        }
    }
]);