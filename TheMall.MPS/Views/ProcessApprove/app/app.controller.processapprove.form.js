﻿angular.module('app.controllers').controller('processApproveFormController',
[
    '$scope', '$routeParams', 'masterGrid', 'controlOptions',
    'processApproveDataSource', 'messageBox', '$location',
    function ($scope, $routeParams, masterGrid, controlOptions,
        processApproveDataSource, messageBox, $location) {
        $scope.form = {};
        $scope.checkNew = true;
        $scope.checkMemoType = false;
        $scope.temp = {};

        $scope.approveType = {
            dataTextField: "text",
            dataValueField: "value",
            autoBind: false,
            enable: false,
            dataSource: new kendo.data.DataSource({
                type: "json",
                data: new kendo.data.ObservableArray([
                    {
                        value: "CashAdvance",
                        text: "Cash advance"
                    },
                    {
                        value: "CashClearing",
                        text: "Cash clearing"
                    },
                    {
                        value: "PettyCash",
                        text: "Petty cash"
                    },
                    {
                        value: "Proposal",
                        text: "Proposal"
                    },
                    {
                        value: "Memo",
                        text: "Memo"
                    },
                    {
                        value: "MemoIncome",
                        text: "MemoIncome"
                    }
                    ])
            }),
            optionLabel: "please select"
        }
        $scope.approveType.select = function (data) {
            var dataItem = this.dataItem(data.item);
            console.log(dataItem);
            $scope.form.ProcessCode = dataItem.value;
            if (dataItem.value === "Memo") {
                $scope.checkMemoType = true;
            } else {
                $scope.checkMemoType = false;
            }
        }

        $scope.memoTypeOption = controlOptions.dropdown('typememo');
//            case "typememo":
//return function () {
//    var options = dropdownKendoOption(angular.crs.url.webApi('typememo'));
//    options.dataTextField = 'TypeMemolName';
//    options.dataValueField = 'TypeMemoCode';
//    options.template = '#=data.TypeMemoCode# - #=data.TypeMemoName#';
//    options.valueTemplate = '#=data.TypeMemoCode# - #=data.TypeMemoName#';
                           
//    return options;
//}
        $scope.setProcessCode = function () {
            if ($scope.temp.ProcessName &&
                $scope.temp.ProcessName.value &&
                $scope.temp.ProcessType &&
                $scope.temp.ProcessType.TypeMemoCode) {
                $scope.form.ProcessCode = $scope.temp.ProcessName.value + $scope.temp.ProcessType.TypeMemoCode;
            }    
        }


        $scope.SwitchFuction = function (caseStr) {
            switch (caseStr) {
                case 'CashAdvance':
                    return 'Cash advance';
                    //break;
                case 'CashClearing':
                    return 'Cash clearing';
                    //break;
                case 'Memo':
                    return 'Memo';
                    //break;
                case 'PettyCash':
                    return 'Petty cash';
                    //break;
                case 'Proposal':
                    return 'Proposal';
                    //break;
                default:
            }
        };

        if (!$routeParams.processCode) {
            $scope.canDeleted = false;
            $scope.edit = false;
            //   messageBox.error("Sequence number and name are invalid!!!");

            //$location.path("unitcodeforemployee/");
        } else {
            $scope.canDeleted = true;
            $scope.edit = true;
            processApproveDataSource.get({
                processCode: $routeParams.processCode
            }, function (data) {
                $scope.checkNew = false;
                $scope.form = data;
                console.log(":)");
                var text = $scope.SwitchFuction($scope.form.ProcessName);
                $scope.temp.ProcessName ={
                    value: $scope.form.ProcessName,
                    text: text
                }
                $scope.temp.ProcessType = {
                    TypeMemoName: $scope.form.ProcessType,
                    TypeMemoCode: $scope.form.ProcessType
            }
                console.log(data);
            });
        }



        $scope.validate = function (event) {
            if (!$scope.validator.validate()) return false;
            if ($scope.temp.ProcessName &&
             $scope.temp.ProcessName.value) {
                $scope.form.ProcessName = $scope.temp.ProcessName.value;
            }
            if ($scope.temp.ProcessType && $scope.temp.ProcessType.TypeMemoCode) {
                $scope.form.ProcessType = $scope.temp.ProcessType.TypeMemoCode;
            }


            if ($scope.checkNew) {
                console.log(":)");
                processApproveDataSource.save($scope.form).$promise.then(
                    function (resp) {
                        if (resp) {
                            messageBox.success('Saved successfully');
                            $location.path('processapprove/');
                        }
                    },
                    function (error) {
                        messageBox.extractError(error);
                    }
                );
            } else {
                console.log(":P");
                processApproveDataSource.edit($scope.form).$promise.then(
                    function (resp) {
                        if (resp) {
                            messageBox.success('Saved successfully');
                            $location.path('processapprove/');
                        }
                    },
                    function (error) {
                        messageBox.extractError(error);
                    }
                );
            }
        }

        $scope.delete = function () {
            var deleted = confirm("Delete this data ?");
            if (deleted) {
                console.log("8)");
                processApproveDataSource.deleted($scope.form).$promise.then(
                     function (resp) {
                         if (resp) {
                             messageBox.success("Delete successfully.");
                             $location.path('processapprove/');
                         }
                     },
                    function (error) {
                        messageBox.extractError(error);
                    }
                    );
            }
        }
    }

]);