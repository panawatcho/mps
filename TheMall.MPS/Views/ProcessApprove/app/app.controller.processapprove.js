﻿angular.module('app.controllers').controller('processApproveController',
[
    '$scope', '$routeParams', 'masterGrid',
    function($scope, $routeParams, masterGrid) {
        var _gridKendoOption;

        var _commandTemplateAllstatus = function (data) {
            return '<div class="btn-group" role="group">' +
                '<a class="btn btn-default" style="width:60px" href="' + $routeParams.page + '/Form?processCode=' + data.ProcessCode + '" title="edit"><i class="fa fa-edit"></i></a>' +
                '</div>';
        }
        $scope.odataUrl = angular.crs.url.odata('processapprovetable');
        $scope.apiUrl = angular.crs.url.webApi('processapprove');
        $scope.gridKendoOption = {
            dataSource: new kendo.data.DataSource({
                transport: {
                    read: {
                        async: true,
                        url: $scope.apiUrl,
                        dataType: "json"
                    },
                    parameterMap: function(options, operation) {
                        if (operation !== "read" && options.models) {
                            return {
                                ProcessCode: options.models[0].Id,
                                ProcessName: options.models[0].TeamName,
                                ProcessType: options.models[0].Type,
                                InActive: options.models[0].Budget
                            };
                        }
                        return null;
                    }
                },
                pageSize: 10,
                batch: true,
                schema: {
                    model: {
                        fields:
                        {
                            ProcessCode: { editable: true, nullable: false },
                            ProcessName: { editable: true, nullable: false },
                            ProcessType: { editable: true, nullable: false },
                            InActive: { editable: true, nullable: false }
                        }
                    }
                }
            }),
            toolbar: ["create"],
            columns: [
                {
                    field: "ProcessCode",
                    title: "Process code",
                    width: "150px",
                    template: function (data) {
                        console.log(data);
                        if (data.ProcessCode) {
                           
                            return "<div style='width:100%; word-wrap: break-word;'>" + data.ProcessCode + "</div>";
                        }
                        return "";
                    }
                },
                {
                    field: "ProcessName",
                    title: "Process name",
                    width: "150px",
                    template: function(data) {
                        if (data.ProcessName) {
                            return "<div style='width:100%; word-wrap: break-word;'>" + data.ProcessName + "</div>";
                        }
                        return "";
                    }
                },
                {
                    field: "ProcessType",
                    title: "Process type",
                    width: "150px",
                    template: function(data) {
                        if (data.ProcessType) {
                            return "<div style='width:100%; word-wrap: break-word;'>" + data.ProcessType + "</div>";
                        }
                        return "";
                    }
                },
                {
                    field: "InActive",
                    title: "Active",
                    width: "150px",
                    template: '<div class="text-center">#if(!data.InActive){# <i class="fa fa-check" style="color:green;"></i>#}else{#<i class="fa fa-times" style="color:red;"></i> #}#</div>'
                },
                { template: _commandTemplateAllstatus, width: '100px' }
            ],
            filterable: true,
            pageable: {
                refresh: true,
                pageSizes: true,
                buttonCount: 5
            }
        };
        $scope.gridKendoOption.toolbar = kendo.template($("#processApproveTemplate").html());
    }
]);