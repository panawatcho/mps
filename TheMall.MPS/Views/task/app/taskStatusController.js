﻿'use strict';
angular.module('app.controllers').controller('TaskStatusController', [
    '$scope', "taskDataSource", "$routeParams",
    function ($scope, taskDataSource, $routeParams) {
        $scope.form = {};
        $scope.filter = {
            Folio: $routeParams.folio
        };
        var tasks = [];
        var query = function() {
            tasks = [];
            taskDataSource.folio({ procInstId:$scope.filter.ProcInstId, folio: $scope.filter.Folio }).$promise.then(function (resp) {

                for (var i = 0; i < resp.length; i++) {
                    for (var j = 0; j < resp[i].ActInsts.length; j++) {
                        tasks.push({
                            Index: j,
                            ProcInstId: resp[i].ProcInstId,
                            Folio: resp[i].Folio,
                            ProcDescription: resp[i].ProcDescription,
                            ActivityDescription: resp[i].ActInsts[j].ActivityDescription,
                            EventStartDate: new Date(resp[i].ActInsts[j].EventStartDate),
                            EventDueDate: new Date(resp[i].ActInsts[j].EventDueDate),
                            ActInstDestFqn: resp[i].ActInsts[j].ActInstDestFqn,
                            RowSpan: resp[i].ActInsts.length
                        });
                    }
                }
                $scope.form.tasks = tasks;
                console.log(resp);
            });
        }

        $scope.validate = function(event) {
            event.preventDefault();

            query();
        }

        if ($scope.filter.Folio) {
            query();
        }
    }
]);