﻿angular.module('app.controllers').controller('accountingcontroller',
[
    '$scope', '$routeParams', 'masterGrid', 'messageBox', 'unitCodeForEmployeeDataSource', 'lineGrid',
    '$http',
    function ($scope, $routeParams, masterGrid, messageBox, unitCodeForEmployeeDataSource, lineGrid, $http) {
        var _gridKendoOption;

        $scope.employeeOption = {
            dataSource: {
                transport: {
                    serverFiltering: true,
                    read: {
                        dataType: "json",  //instead of "type: 'jsonp',"
                        url: angular.crs.url.webApi('employee/search')
                    }
                }
            },
            optionLabel: "please select",
            dataTextField: 'FullName',
            dataValueField: 'FullName',
            template: '#=data.FullName#',
            valueTemplate: '#=data.FullName#',
            filter: "contains"
        }

        $scope.unitcodeOption = {
            dataTextField: "UnitName",
            dataValueField: "UnitCode",
            optionLabel: "please select",
            dataSource: {
                type: 'json',
                serverFiltering: true,
                transport: {
                    read: {
                        url: function () {
                            return angular.crs.url.webApi('unitcode/search');
                        }
                    }
                }
            }
        }

        $scope.apiUrl = angular.crs.url.webApi('accounting/ShowAll');


        //$http.get($scope.apiUrl).then(function (response) {
        //    $scope.data = response.data;
        //});
        var _commandTemplateAllstatus = function (data) {
            return '<div class="btn-group" role="group">' +
                '<a class="btn btn-default" style="width:60px" href="' + $routeParams.page + '/Form?BranchCode=' + data.BranchCode + '&Username=' + data.Username + '" title="edit"><i class="fa fa-edit"></i></a>' +
                '</div>';
        }
        console.log($routeParams.page);
        $scope.gridKendoOption = {
            dataSource: new kendo.data.DataSource({
                transport: {
                    read: {
                        async: true,
                        url: $scope.apiUrl,
                        dataType: "json"
                    },
                    parameterMap: function (options, operation) {
                        if (operation !== "read" && options.models) {
                            return {
                                EmpId: options.EmpId[0].EmpId,
                                BranchCode: options.models[0].BranchCode,
                                Username: options.models[0].Username,
                                Email: options.models[0].Email,
                                InActive: options.models[0].InActive
                            };
                        }
                        return null;
                    }
                },
                pageSize: 10,
                batch: true,
                schema: {
                    model: {
                        id: "EmpId",
                        fields: {
                            BranchCode: { editable: true, nullable: false },
                            EmpId: { editable: true, nullable: false },
                            Username: { editable: true, nullable: false },
                            Email: { editable: true, nullable: false },
                            InActive: { type: "boolean", editable: true, nullable: false }
                        }
                    }
                }
            }),
            toolbar: ["create"],
            columns: [
                //{ field: "EmpId", title: "Employee id", width: "200px" },
                {
                    field: "BranchCode",
                    title: "Branch code",
                    width: "80px"//,
                    //template: function (data) {
                    //    if (data.Employee) {
                    //        return data.Employee.Username;
                    //    }
                    //    return null;
                    //}
                },
                  {
                      field: "EmpId",
                      title: "EmpId",
                      width: "80px"//,
                      //template: function (data) {
                      //    if (data.Employee) {
                      //        return data.Employee.FullName;
                      //    }
                      //    return null;
                      //}
                  },
                {
                    field: "Username",
                    title: "Username",
                    width: "200px"//,
                    //template: function (data) {
                    //    if (data.UnitCode) {
                    //        return data.UnitCode.UnitCode + ' - ' + data.UnitCode.UnitName;
                    //    }
                    //    return null;
                    //}
                },
                     {
                         field: "Email",
                         title: "Email",
                         width: "200px"//,
                         //template: function (data) {
                         //    if (data.UnitCode) {
                         //        return data.UnitCode.UnitCode + ' - ' + data.UnitCode.UnitName;
                         //    }
                         //    return null;
                         //}
                     },
                {
                    field: "InActive",
                    title: "Active",
                    width: "80px",
                    template: '<div class="text-center">#if(!data.InActive){# <i class="fa fa-check" style="color:green;"></i>#}else{#<i class="fa fa-times" style="color:red;"></i> #}#</div>'
                    //template: function(data) {

                    //    return '<center><input type="checkbox" disabled="disabled" ng-checked="' + !data.InActive + '"/></center>';
                    //}
                },
                {
                    template: _commandTemplateAllstatus, width: '110px'
                }
            ],
            filterable: true,
            pageable: {
                refresh: true,
                pageSizes: true,
                buttonCount: 5
            },
        };
        $scope.gridKendoOption.toolbar = kendo.template($("#accountingTemplate").html());
    }
]);