﻿angular.module('app.controllers').controller('apcodeController',
[
    '$scope', '$routeParams','masterGrid',
    function ($scope, $routeParams, masterGrid) {
    
        var _gridKendoOption;
        $scope.gridKendoOption = function () {
            if (!_gridKendoOption) {
                $scope.odataUrl = angular.crs.url.odata('apcodetable');
                $scope.apiUrl = angular.crs.url.webApi('apcode');
              
                _gridKendoOption = masterGrid.gridKendoOption($scope.odataUrl, $scope.apiUrl,
                {
                    parameterMap: function (options, operation) {
                        if (operation !== "read" && options.models) {
                            return {
                                APCode: options.models[0].APCode,
                                APName: options.models[0].APName
                            };
                        }
                        return null;
                    },
                    schema: {
                        model: {
                            id: "APCode",
                            fields: {
                                APCode: { editable: true, nullable: false },
                                APName: { editable: true, nullable: false }, 
                            }
                        }
                    },
                    toolbar: ["create"],
                    columns: [
                        { field: "APCode", title: "AP Code", width: "200px" },
                        { field: "APName", title: "AP Name", width: "200px" },
                        { command: ["edit", "destroy"], title: "&nbsp;", width: 200 }
                    ],
                    editable: {
                               mode: "inline",
                               createAt: "top"
                           }
                });
            }
            return _gridKendoOption;
        }
     
        //var _gridKendoOption;
        //$scope.gridKendoOption = function () {
        //    if (!_gridKendoOption) {
        //        $scope.datasourceUrl = angular.crs.url.odata('unitcodetable');
        //        $scope.Url = angular.crs.url.webApi('unitcode');
              
        //        _gridKendoOption = {
        //            dataSource: new kendo.data.DataSource({
        //                transport: {
        //                    read: {
        //                        async: true,
        //                        url: $scope.datasourceUrl,
        //                        dataType: "json",
        //                        data: undefined
        //                    },
        //                    update: {
        //                        url: $scope.Url + "/Update",
        //                        dataType: "json",
        //                        method: "post"
        //                    },
        //                    destroy: {
        //                        url: $scope.Url + "/Delete",
        //                        dataType: "json",
        //                        method: "post"
        //                    },
        //                    create: {
        //                        url: $scope.Url + "/Create",
        //                        dataType: "json",
        //                        method: "post"
        //                    },
        //                    parameterMap: function (options, operation) {
        //                        if (operation !== "read" && options.models) {
        //                            return {
        //                                UnitCode: options.models[0].UnitCode,
        //                                UnitName: options.models[0].UnitName,
                                      
        //                            };
        //                        }
        //                        return null;
        //                    }
        //                },
        //                error: function (e) {
        //                    console.log(e);
        //                    var message = e.xhr.responseJSON.Message;
        //                    alert(message);
        //                },
                       
        //                batch: true,
        //                pageSize: 10,
        //                schema: {
        //                    data: function (data) { return data.value; },
        //                    total: function (data) { return data["odata.count"]; },
        //                    model: {
        //                        id: "UnitCode",
        //                        fields: {
        //                            UnitCode: { editable: true, nullable: false },
        //                            UnitName: { editable: true, nullable: false },
                                   
        //                        }
        //                    }
        //                }
        //            }),
        //            height: 550,
        //            toolbar: ["create"],
        //            columns: [
        //                { field: "UnitCode", title: "Unit Code", width: "200px" },
        //                { field: "UnitName", title: "Unit Namee", width: "200px" },
        //                { command: ["edit", "destroy"], title: "&nbsp;", width: 200 }
        //            ],
                   
        //            pageable: {
        //                refresh: true,
        //                pageSizes: [5, 10, 20, 50, 100]
        //            },
                  
        //            selectable: "row",
        //            editable: {
        //                mode: "inline",
        //                createAt: "top"
        //            }

        //        };
        //    }
        //    return _gridKendoOption;
        //}





    }
]);