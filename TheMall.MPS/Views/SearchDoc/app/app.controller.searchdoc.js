﻿angular.module('app.controllers').controller('searchDocController',
[
    '$scope', '$routeParams', 'inquiryGrid', 'IdProposalService', 'authService',
    '$window', 'messageBox', '$location', 'searchDocDataSource', 
    function($scope, $routeParams, inquiryGrid, idProposalService, authService,
        $window, messageBox, $location, searchDocDataSource) {
        
        $scope.form = {}
        $scope.temp = {}
        $scope.processOption = {
            dataSource: {
                type: "json",
                data: [
                {
                    processTitle: "Proposal",
                    processValue: "Proposal"
                },
                {
                    processTitle: "Memo",
                    processValue: "Memo"
                },
                {
                    processTitle: "Memo Income",
                    processValue: "MemoIncome"
                },
                {
                    processTitle: "Petty cash",
                    processValue: "PettyCash"
                },
                {
                    processTitle: "Cash advance",
                    processValue: "CashAdvance"
                },
                {
                    processTitle: "Cash clearing",
                    processValue: "CashClearing"
                },
                ]
            },
            optionLabel: "please select",
            dataTextField: 'processTitle',
            dataValueField: 'processValue',
            template: '#=data.processTitle#',
            valueTemplate: '#=data.processTitle#'
        }


        $scope.statusOption = { //function () {
            dataSource: {
                type: "json",
                data:
                [
                    {
                        documentStatus: "Draft",
                        statusFlag: "0"
                    },
                    {
                        documentStatus: "Active",
                        statusFlag: "1"
                    },
                    {
                        documentStatus: "Revise",
                        statusFlag: "2"
                    },
                    {
                        documentStatus: "Completed",
                        statusFlag: "9"
                    },
                    {
                        documentStatus: "Cancelled",
                        statusFlag: "5"
                    },
                    {
                        documentStatus: "Deleted",
                        statusFlag: "5"
                    },
                    {
                        documentStatus: "Rejected",
                        statusFlag: "5"
                    }
                ]
            },
            optionLabel: "please select",
            dataTextField: 'documentStatus',
            dataValueField: 'statusFlag',
            template: '#=data.documentStatus#',
            valueTemplate: '#=data.documentStatus#'
        }





        $scope.Search = function() {
            if ($scope.temp.Process && $scope.temp.Process.processValue !== "") {
                $scope.form.Process = $scope.temp.Process.processValue;
                
                if ($scope.temp.Status && $scope.temp.Status.documentStatus !== ""
                    && $scope.temp.Status.documentStatus !== "please select") {
                    $scope.form.Status = $scope.temp.Status.documentStatus;
                } else {
                    $scope.form.Status = null;
                }

                if ($scope.form.CreateDate) {
                    $scope.date = new Date($scope.form.CreateDate);
                    $scope.date.setHours($scope.date.getHours() + 7);
                    $scope.form.CreateDate = $scope.date;
                } else {
                    $scope.date = null;
                }
                searchDocDataSource.search($scope.form, function(data) {
                    $scope.gridKendoOption.dataSource.data(data);
                    if ($scope.date) {
                        $scope.date = new Date($scope.form.CreateDate);
                        $scope.date.setHours($scope.date.getHours() - 7);
                        $scope.form.CreateDate = $scope.date;
                    }
                });
            } else {
                messageBox.error("Please select a process before search!!!");
            }
        }

        $scope.Clear = function() {
            $scope.form = {};
            $scope.temp = {};
            $scope.gridKendoOption.dataSource.data([]);
            $scope.temp.Process = { "processTitle": "", "processValue": "" };
            $scope.temp.Status = {
                documentStatus: "",
                statusFlag: ""
            };
            $scope.form.CreateDate = null;
            $scope.form.Process = null;
            $scope.form.DocumentNumber = null;
            $scope.form.Title = null;
        }


        var commandToDetail = function (data) {
            return '<div class="btn-group" role="group">' +
                '<a class="btn btn-default" style="width:60px margin-right: 5px;" href="' + $scope.form.Process + '/detail/' + data.Id + '" title="detail" target="_blank"><i class="fa fa-file-text"></i></a>' +
                '</div>';
        }

        $scope.gridKendoOption = {
            dataSource: new kendo.data.DataSource({
                type: "json",
                data: new kendo.data.ObservableArray([]),
                sort: { field: "CreateDate", dir: "desc" },
                batch: false,
                pageSize: 15,
                schema: {
                    model: kendo.data.Model.define({
                        id: "Id",
                        fields: {
                            DocumentNumber: { type: "string" },
                            Username: { type: "string" },
                            Status: { type: "string" },
                            Title: { type: "string" },
                            CreateDate: { type: "date" }
                        }
                    })
                },
                error: function (e) {
                }
            }),
            columns: [
                      { template: commandToDetail, width: '25px' },
                 {
                     field: 'DocumentNumber', title: 'Document number', width: '120px',
                     template: function (data) {
                         if (data.DocumentNumber !== undefined && data.DocumentNumber != null) {
                             return "<div style='width:100%; word-wrap: break-word;  text-align: center;'>" + data.DocumentNumber + "</div>";
                         }
                         return "";
                     }
                 },
           
                  {
                      field: 'UnitCode', title: 'Unit code', width: '200px',
                      template: function (data) {
                          if (data.UnitCode !== undefined && data.UnitCode != null) {
                              return "<div style='width:100%; word-wrap: break-word;'>" +
                                  data.UnitCode + " - " + data.UnitName + "</div>";
                          }
                          return "";
                      }
                  },
                {
                    field: 'Title', title: 'Title', width: '200px',
                    template: function (data) {
                        if (data.Title !== undefined && data.Title != null) {
                            return "<div style='width:100%; word-wrap: break-word;'>" + data.Title + "</div>";
                        }
                        return "";
                    }
                },
                 {
                     field: 'Username', title: 'Username', width: '150px',
                     template: function (data) {
                         if (data.Username !== undefined && data.Username != null) {
                             return "<div style='width:100%; word-wrap: break-word;'>" + data.Username + "</div>";
                         }
                         return "";
                     }
                 },
                     {
                         field: 'Status', title: 'Status', width: '80px',
                         template: function (data) {
                             if (data.Status !== undefined && data.Status != null) {
                                 return "<div style='width:100%; word-wrap: break-word;  text-align: center;'>" + data.Status + "</div>";
                             }
                             return "";
                         }
                     },
                   {
                       field: 'CreateDate',
                       title: 'Create date',
                       width: '100px',
                       template:function (data) {
                           if (data.CreateDate !== undefined && data.CreateDate != null) {
                               var date = new Date(data.CreateDate);
                               date.setHours(date.getHours() - 7);
                               return kendo.toString(new Date(date), "dd/MM/yyyy"); //inquiryGrid.formatDate('CreateDate');
                           }
                           return "";
                       }
                   }
            ],
            pageable: {
                refresh: true,
                pageSizes: [5, 10, 20, 50, 100]
            },
            filterable: {
                extra: true, //do not show extra filters
                operators: {
                    // redefine the string operators
                    string: {
                        contains: "Contains"
                    }
                }
            },
            reorderable: false,
            resizable: false,
            selectable: "row",
            sortable: {
                allowUnsort: true,
                mode: "single"
            },
            height: 520
        };

    }
]);