﻿angular.module('app.controllers').controller('typememoController',
[
    '$scope', '$routeParams','masterGrid','inquiryGrid',
    function ($scope, $routeParams, masterGrid, inquiryGrid) {
    
        var _gridKendoOption;
        var _commandTemplateAllstatus = function (data) {
            return '<div class="btn-group" role="group">' +
                '<a class="btn btn-default" style="width:60px" href="' + $routeParams.page + '/Form?typeMemoCode=' + data.TypeMemoCode + '" title="edit"><i class="fa fa-edit"></i></a>' +
                '</div>';
        }
        $scope.apiUrl = angular.crs.url.webApi('typememo');
        $scope.gridKendoOption = {
            dataSource: new kendo.data.DataSource({
                transport: {
                    read: {
                        async: true,
                        url: $scope.apiUrl,
                        dataType: "json"
                    },
                    parameterMap: function(options, operation) {
                        if (operation !== "read" && options.models) {
                            return {
                                TypeMemoCode: options.models[0].TypeMemoCode,
                                TypeMemoName: options.models[0].TypeMemoName,
                                UnitCode: options.models[0].UnitCode,
                                InActive: options.models[0].InActive,
                                Order: options.models[0].Order,
                                ActualCharge: options.models[0].ActualCharge
                            };
                        }
                        return null;
                    }
                },   
                pageSize: 10,
                batch: true,
                schema: {
                    model: {
                        fields:
                        {
                            TypeMemoCode: { editable: true, nullable: false },
                            UnitCode: { editable: true, nullable: false },
                            InActive: { editable: true, nullable: false },
                            Order: { editable: true, nullable: false },
                            ActualCharge: { editable: true, nullable: false },
                        }
                    }
                }
            }),
            toolbar: ["create"],
            columns: [
            {
                field: "TypeMemoCode", title: "Type memo", width: "200px",
                template: function (data) {
                    if (data.TypeMemoCode) {
                        return "<div style='width:100%; word-wrap: break-word;'>" + data.TypeMemoCode + ' - ' + data.TypeMemoName + "</div>";
                    }
                    return "";
                }
            },
            {
                field: "UnitCode", title: "Unit code", width: "200px",
                template: function (data) {
                    if (data.UnitCode.UnitCode) {
                        return "<div style='width:100%; word-wrap: break-word;'>" + data.UnitCode.UnitCode + ' - ' + data.UnitCode.UnitName + "</div>";
                    }
                    return "";
                }
            },
             {
                 field: "ActualCharge",
                 title: "Actual Charge",
                 width: "100px",
                 template: '<div class="text-center">#if(data.ActualCharge){# <i class="fa fa-check" style="color:green;"></i>#}else{#<i class="fa fa-times" style="color:red;"></i> #}#</div>',
                 editor: '<input type="checkbox" name="InActive" data-bind="checked:InActive"></input>'
             },
            {
                field: "InActive",
                title: "Active",
                width: "100px",
                template: '<div class="text-center">#if(!data.InActive){# <i class="fa fa-check" style="color:green;"></i>#}else{#<i class="fa fa-times" style="color:red;"></i> #}#</div>',
                editor: '<input type="checkbox" name="InActive" data-bind="checked:InActive"></input>'
            },
            {
                field: "Order", title: "Order", width: "70px",
                template: inquiryGrid.formatCurrency('Order')
            },
            { template: _commandTemplateAllstatus, width: '100px' }
            ],
            filterable: true,
            pageable: {
                refresh: true,
                pageSizes: true,
                buttonCount: 5
            }
        };
        $scope.gridKendoOption.toolbar = kendo.template($("#typeMemoTemplate").html());

//$scope.gridKendoOption = function () {
        //    if (!_gridKendoOption) {
        //        $scope.odataUrl = angular.crs.url.odata('typememotable');
        //        $scope.apiUrl = angular.crs.url.webApi('typememo');

        //        _gridKendoOption = masterGrid.gridKendoOption($scope.odataUrl, $scope.apiUrl,
        //        {
        //            parameterMap: function (options, operation) {
        //                if (operation !== "read" && options.models) {
        //                    return {
        //                        TypeMemoCode: options.models[0].TypeMemoCode,
        //                        TypeMemoName: options.models[0].TypeMemoName
        //                    };
        //                }
        //                return null;
        //            },
        //            schema: {
        //                model: {
        //                    id: "TypeMemoCode",
        //                    fields: {
        //                        TypeMemoCode: { editable: true, nullable: false },
        //                        TypeMemoName: { editable: true, nullable: false },
        //                    }
        //                }
        //            },
        //            toolbar: ["create"],
        //            columns: [
        //                { field: "TypeMemoCode", title: "Memo type code", width: "200px" },
        //                { field: "TypeMemoName", title: "Memo type name", width: "200px" },
        //                { command: ["edit", "destroy"], title: "&nbsp;", width: 200 }
        //            ],
        //            editable: {
        //                       mode: "inline",
        //                       createAt: "top"
        //                   }
        //        });
        //    }
        //    return _gridKendoOption;
        //}


    }
]);