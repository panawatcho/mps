﻿angular.module('app.controllers').controller('aspNetRolesFormController',
[
    '$scope', '$routeParams', 'masterGrid', 'messageBox',
    'lineGrid', '$location',
    '$http', 'controlOptions', 'aspNetRolesDataSource','aspNetUserRolesDataSource',
    function($scope, $routeParams, masterGrid, messageBox, lineGrid, $location,
        $http, controlOptions, aspNetRolesDataSource, aspNetUserRolesDataSource) {
        $scope.form = {};
        $scope.checkNew = true;

        if (!$routeParams.Id) {
            $scope.canDeleted = false;
            $scope.edit = false;
            //   messageBox.error("Sequence number and name are invalid!!!");
            //$location.path("unitcodeforemployee/");
        } else {
            $scope.canDeleted = true;
            $scope.edit = true;
            aspNetRolesDataSource.getById({ id: $routeParams.Id}, function (data) {
                $scope.form = data;
                $scope.checkNew = false;
                console.log($scope.form);
            });

            aspNetUserRolesDataSource.getByRoleId({ roleId: $routeParams.Id }, function (data) {
                $scope.gridKendoUserRolesOption.dataSource.data(data);
            });
        }

        $scope.validate = function(event) {
            if (!$scope.validator.validate()) return false;
            if ($scope.checkNew) {
                aspNetRolesDataSource.save($scope.form).$promise.then(
                    function(resp) {
                        if (resp) {
                            messageBox.success('Saved successfully');
                            $location.path('aspnetroles/');
                        }
                    },
                    function(error) {
                        messageBox.extractError(error);
                    }
                );
            } else {
                aspNetRolesDataSource.edit($scope.form).$promise.then(
                    function(resp) {
                        if (resp) {
                            messageBox.success('Saved successfully');
                            $location.path('aspnetroles/');
                        }
                    },
                    function(error) {
                        messageBox.extractError(error);
                    }
                );
            }
        }

        $scope.delete = function () {
            var deleted = confirm("Delete this data ?");
            if (deleted) {
                aspNetRolesDataSource.deleted($scope.form).$promise.then(
                     function (resp) {
                         if (resp) {
                             messageBox.success("Delete successfully.");
                             $location.path('aspnetroles/');
                         }
                     },
                    function (error) {
                        messageBox.extractError(error);
                    }
                );
            }
        }


        $scope.gridKendoUserRolesOption = {
            dataSource: new kendo.data.DataSource({
                type: "json",
                data: new kendo.data.ObservableArray([]),
                //sort: { field: "Username", dir: "desc" },
                batch: false,
                pageSize: 15,
                schema: {
                    model: kendo.data.Model.define({
                        id: "Username",
                        fields: {
                            Username: { type: "string" },
                            Inactive: { type: "bool" }
                        }
                    })
                },
                error: function (e) {
                }
            }),
            columns: [
                 {
                     field: 'Username', title: 'Username', width: '120px',
                     template: function (data) {
                         if (data.Username !== undefined && data.Username != null) {
                             return "<div style='width:100%; word-wrap: break-word;  text-align: center;'>" + data.Username + "</div>";
                         }
                         return "";
                     }
                 },
                   {
                       field: "Inactive",
                       title: "Active",
                       width: "80px",
                       template: '<div class="text-center">#if(!data.Inactive){# <i class="fa fa-check" style="color:green;"></i>#}else{#<i class="fa fa-times" style="color:red;"></i> #}#</div>'
                       //template: function(data) {

                       //    return '<center><input type="checkbox" disabled="disabled" ng-checked="' + !data.InActive + '"/></center>';
                       //}
                   }
            ],
            pageable: {
                refresh: true,
                pageSizes: [5, 10, 20, 50, 100]
            },
            filterable: {
                extra: true, //do not show extra filters
                operators: {
                    // redefine the string operators
                    string: {
                        contains: "Contains"
                    }
                }
            },
            reorderable: false,
            resizable: false,
            selectable: "row",
            sortable: {
                allowUnsort: true,
                mode: "single"
            },
            height: 520
        };
    }
]);