﻿angular.module('app.controllers').controller('aspnetrolesController',
[
    '$scope', '$routeParams', 'masterGrid', 'inquiryGrid',
    function($scope, $routeParams, masterGrid, inquiryGrid) {
        var _gridKendoOption;
        var _commandTemplateAllstatus = function (data) {
            return '<div class="btn-group" role="group">' +
                '<a class="btn btn-default" style="width:60px" href="' + $routeParams.page + '/Form?Id=' + data.id + '" title="edit"><i class="fa fa-edit"></i></a>' +
                '</div>';
        }

        $scope.apiUrl = angular.crs.url.webApi('aspnetroles/getall');

        $scope.gridKendoOption = {
            dataSource: new kendo.data.DataSource({
                transport: {
                    read: {
                        async: true,
                        url: $scope.apiUrl,
                        dataType: "json"
                    },
                    parameterMap: function (options, operation) {
                        if (operation !== "read" && options.models) {
                            return {
                                Id: options.models[0].Id,
                                Name: options.models[0].Name,
                                Inactive: options.models[0].Inactive
                            };
                        }
                        return null;
                    }
                },
                pageSize: 10,
                batch: true,
                sort: { field: "Id", dir: "desc" },
                schema: {
                    model: {
                        id: "Id",
                        fields:
                        {
                            Id: { editable: true, nullable: false },
                            Name: { editable: true, nullable: false },
                            Inactive: { editable: true, nullable: false }
                        }
                    }
                }
            }),
            toolbar: ["create"],
            columns: [
                {
                    field: "Id", title: "Id", width: "200px",
                    template: function (data) {
                        if (data.Id) {
                            return "<div style='width:100%; word-wrap: break-word;'>" + data.Id + "</div>";
                        }
                        return "";
                    }
                },
                {
                    field: "Name", title: "Name", width: "200px",
                    template: function (data) {
                        if (data.Name) {
                            return "<div style='width:100%; word-wrap: break-word;'>" + data.Name + "</div>";
                        }
                        return "";
                    }
                },
                {
                    field: "Inactive",
                    title: "Active",
                    width: "80px",
                    template: '<div class="text-center">#if(!data.Inactive){# <i class="fa fa-check" style="color:green;"></i>#}else{#<i class="fa fa-times" style="color:red;"></i> #}#</div>'
                    //template: function(data) {

                    //    return '<center><input type="checkbox" disabled="disabled" ng-checked="' + !data.InActive + '"/></center>';
                    //}
                },
                { template: _commandTemplateAllstatus, width: '100px' }
            ],
            filterable: true,
            pageable: {
                refresh: true,
                pageSizes: true,
                buttonCount: 5
            }
        };
        $scope.gridKendoOption.toolbar = kendo.template($("#aspNetRolesTemplate").html());

    }
]);
    
      