﻿angular.module('app.controllers').controller('unitCodeFormController',
[
    '$scope', '$routeParams', 'masterGrid', 'controlOptions',
    'unitcodeDataSource', 'messageBox', '$location',
    function($scope, $routeParams, masterGrid, controlOptions,
        dataSource, messageBox, $location) {

        $scope.form = {};
        $scope.checkNew = true;
        $scope.checkMemoType = false;
        $scope.temp = {};
        if (!$routeParams.UnitCode) {
            $scope.canDeleted = false;
            $scope.edit = false;
        } else {
            $scope.canDeleted = true;
            $scope.edit = true;
            dataSource.getByUnitCode({
                unitCode: $routeParams.UnitCode
            }, function (data) {
                $scope.checkNew = false;
                $scope.form = data;
            });
        }

        $scope.validate = function (event) {
            if (!$scope.validator.validate()) return false;
            if ($scope.checkNew) {
                dataSource.save($scope.form).$promise.then(
                    function (resp) {
                        if (resp) {
                            messageBox.success('Saved successfully');
                            $location.path('unitcode/');
                        }
                    },
                    function (error) {
                        messageBox.extractError(error);
                    }
                );
            } else {
                dataSource.edit($scope.form).$promise.then(
                    function (resp) {
                        if (resp) {
                            messageBox.success('Saved successfully');
                            $location.path('unitcode/');
                        }
                    },
                    function (error) {
                        messageBox.extractError(error);
                    }
                );
            }
        };

        $scope.delete = function () {
            var deleted = confirm("Delete this data ?");
            if (deleted) {
                dataSource.deleted($scope.form).$promise.then(
                     function (resp) {
                         if (resp) {
                             messageBox.success("Delete successfully.");
                             $location.path('unitcode/');
                         }
                     },
                    function (error) {
                        messageBox.extractError(error);
                    }
                    );
            }
        }
    }
]);