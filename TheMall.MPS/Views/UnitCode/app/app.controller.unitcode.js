﻿angular.module('app.controllers').controller('unitcodeController',
[
    '$scope', '$routeParams','inquiryGrid',
    function ($scope, $routeParams, inquiryGrid) {
    
        var _gridKendoOption;
        var _commandTemplateAllstatus = function (data) {
            return '<div class="btn-group" role="group">' +
                '<a class="btn btn-default" style="width:60px" href="' + $routeParams.page + '/Form?UnitCode=' + data.UnitCode + '" title="edit"><i class="fa fa-edit"></i></a>' +
                '</div>';
        }
        $scope.apiUrl = angular.crs.url.webApi('unitcode');
        $scope.gridKendoOption = {
            dataSource: new kendo.data.DataSource({
                transport: {
                    read: {
                        async: true,
                        url: $scope.apiUrl,
                        dataType: "json"
                    },
                    parameterMap: function (options, operation) {
                        if (operation !== "read" && options.models) {
                            return {
                                UnitCode: options.models[0].UnitCode,
                                UnitName: options.models[0].UnitName,
                                InActive: options.models[0].InActive,
                                Order: options.models[0].Order,
                                Type: options.models[0].Type
                            };
                        }
                        return null;
                    }
                },
                pageSize: 10,
                batch: true,
                schema: {
                    model: {
                        fields:
                        {
                            UnitCode: { editable: true, nullable: false },
                            UnitName: { editable: true, nullable: false },
                            InActive: { editable: true, nullable: false },
                            Order: { editable: true, nullable: false },
                            Type: { editable: true, nullable: false }
                        }
                    }
                }
            }),
            toolbar: ["create"],
            columns: [
                {
                    field: "UnitCode", title: "Unit code", width: "200px",
                    template: function (data) {
                        if (data.UnitCode) {
                            return "<div style='width:100%; word-wrap: break-word;'>" + data.UnitCode + "</div>";
                        }
                        return "";
                    }
                },
                {
                    field: "UnitName", title: "Unit name", width: "250px",
                    template: function (data) {
                        if (data.UnitCode) {
                            return "<div style='width:100%; word-wrap: break-word;'>" + data.UnitName + "</div>";
                        }
                        return "";
                    }
                },
                {
                    field: "Type", title: "Type", width: "70px",
                    template: function (data) {
                        if (data.Type) {
                            return "<div style='width:100%; word-wrap: break-word;'>" + data.Type + "</div>";
                        }
                        return "";
                    }
                },
                {
                    field: "InActive",
                    title: "Active",
                    width: "200px",
                    template: '<div class="text-center">#if(!data.InActive){# <i class="fa fa-check" style="color:green;"></i>#}else{#<i class="fa fa-times" style="color:red;"></i> #}#</div>',
                    editor: '<input type="checkbox" name="InActive" data-bind="checked:InActive"></input>'
                },
                {
                    field: "Order", title: "Order", width: "100px",
                    template: inquiryGrid.formatCurrency('Order')
                },
                { template: _commandTemplateAllstatus, width: '100px' }
            ],
            filterable: true,
            pageable: {
                refresh: true,
                pageSizes: true,
                buttonCount: 5
            }
        }
        $scope.gridKendoOption.toolbar = kendo.template($("#unitCodeTemplate").html());
        //$scope.gridKendoOption = function () {
        //    if (!_gridKendoOption) {
        //        $scope.odataUrl = angular.crs.url.odata('unitcodetable');
        //        $scope.apiUrl = angular.crs.url.webApi('unitcode');
              
        //        _gridKendoOption = masterGrid.gridKendoOption($scope.odataUrl, $scope.apiUrl,
        //        {
        //            parameterMap: function (options, operation) {
        //                if (operation !== "read" && options.models) {
        //                    return {
        //                        UnitCode: options.models[0].UnitCode,
        //                        UnitName: options.models[0].UnitName
        //                    };
        //                }
        //                return null;
        //            },
        //            schema: {
        //                model: {
        //                    id: "UnitCode",
        //                    fields: {
        //                        UnitCode: { editable: true, nullable: false },
        //                        UnitName: { editable: true, nullable: false }, 
        //                    }
        //                }
        //            },
        //            toolbar: ["create"],
        //            columns: [
        //                { field: "UnitCode", title: "Unit code", width: "200px" },
        //                { field: "UnitName", title: "Unit name", width: "200px" },
        //                {
        //                command: [{
        //                            name: 'edit',
        //                            text: '',
        //                            className: 'grid-button-mini',
        //                            title:'edit',
                               
        //                          }, "destroy"
        //                         ], title: "&nbsp;", width: 200
        //                }
        //            ],
        //            editable: {
        //                       mode: "inline",
        //                       createAt: "top"
        //                   }
        //        });
        //    }
        //    return _gridKendoOption;
        //}
    }
]);