﻿angular.module('app.controllers').controller('memoincomeFormController',
    ['$scope', '$routeParams', 'controlOptions', 'lineGrid', '$attrs', 'memoincomeDataSource', 'uploadHandler', 'k2',
        'IdProposalService', 'authService', '$filter', 'messageBox', '$location', 'approval', '$sce', 'approverGrids',
        function($scope, $routeParams, controlOptions, lineGrid, $attrs, dataSource, uploadHandler, k2, idProposalService, authService, $filter, messageBox, $location, approval, $sce, approverGrids) {

            //$controller('BaseController', { $scope: $scope, dataSource: dataSource, $attrs: $attrs });
            var id = 0,
                sn = k2.extractSN($routeParams),
                remoteResult,
                _memoincomeLinesGridOption,
                _invoiceGridOption;

            var isSave = false,
                checkFinalApproverButton = false;
         
            $scope.ApproverLevel = approverGrids.ApproverLevel;
            $scope.count = 1;
            $scope.checkedUnitCode = false;
            $scope.checkCategory = false;
            $scope.showBranch = false;
            $scope.showDeposit = false;
            $scope.showIncomeOther = false;
            $scope.currentDate = new Date();
            $scope.dataBranch = new kendo.data.ObservableArray([]);

            $scope.isReadonlyDetails = function() {
                if ($routeParams.mode.toUpperCase() !== "CREATE" && $routeParams.mode.toUpperCase() !== "REVISE") {
                    //$($("#texteditor").data().kendoEditor.body).attr('contenteditable', false);
                    return true;
                }
                return false;
            }

            $scope.isAccount = function () {
                if ($routeParams.mode.toUpperCase() === "ACC-APPROVE") {
                    return true;
                }
                return false;
            }

            //Current date
            $scope.currentDate = new Date();

            $scope.form = {
                ListPlace: [],
                DocTypeFiles: [],
                OtherFiles: [],
                MemoIncomeApproval: new kendo.data.ObservableArray([]),
                MemoIncomeInvoice: new kendo.data.ObservableArray([]),
                MemoIncomeLines: new kendo.data.ObservableArray([]),
                Category: [],
                Requester: [],
                InformEmail: [],
                CCEmail: [],
                BudgetDetail: 0
            };

            $scope.category = function () {
                if ($scope.form) {
                    if ($scope.form.Category && angular.uppercase($scope.form.Category) === "OTHERINCOME") {
                        return "Income Other Source";
                    } else if ($scope.form.Category && angular.uppercase($scope.form.Category) === "DEPOSITPROPOSAL") {
                        return "Deposit Proposal";
                    }
                }
                return "";
            }

            //Set Due Date
            //Current date
            $scope.currentDate = new Date();
            $scope.currentDatePlusTwo = new Date($scope.currentDate);
            $scope.currentDatePlusTwo.setDate($scope.currentDatePlusTwo.getDate() + 2);
            //Set date Propose
            if ($scope.currentDatePlusTwo.getDay() === 6) {
                $scope.currentDatePlusTwo.setDate($scope.currentDatePlusTwo.getDate() + 2);
            }
            if ($scope.currentDatePlusTwo.getDay() === 0) {
                $scope.currentDatePlusTwo.setDate($scope.currentDatePlusTwo.getDate() + 1);
            }
            $scope.form.DueDatePropose = new Date($scope.currentDatePlusTwo);
            //Set date Accept
            var dueDatePropose = new Date($scope.form.DueDatePropose);
            dueDatePropose.setDate(dueDatePropose.getDate() + 2);
            if (dueDatePropose.getDay() === 6) {
                dueDatePropose.setDate(dueDatePropose.getDate() + 2);
            }
            if (dueDatePropose.getDay() === 0) {
                dueDatePropose.setDate(dueDatePropose.getDate() + 1);
            }
            $scope.form.DueDateAccept = new Date(dueDatePropose);
            //Set date Approve
            var dueDateAccept = new Date($scope.form.DueDateAccept);
            dueDateAccept.setDate(dueDateAccept.getDate() + 2);
            if (dueDateAccept.getDay() === 6) {
                dueDateAccept.setDate(dueDateAccept.getDate() + 2);
            }
            if (dueDateAccept.getDay() === 0) {
                dueDateAccept.setDate(dueDateAccept.getDate() + 1);
            }
            $scope.form.DueDateApprove = new Date(dueDateAccept);
            //Set date Final Approve
            var dueDateFinalApprove = new Date($scope.form.DueDateApprove);
            dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 2);
            if (dueDateFinalApprove.getDay() === 6) {
                dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 2);
            }
            if (dueDateFinalApprove.getDay() === 0) {
                dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 1);
            }
            $scope.form.DueDateFinalApprove = new Date(dueDateFinalApprove);

            //SetDate function 
            //Propose
            var setDateFromPropose = function() {
                //Set date Propose
                var dueDatePropose = new Date($scope.form.DueDatePropose);
                dueDatePropose.setDate(dueDatePropose.getDate() + 2);
                if (dueDatePropose.getDay() === 6) {
                    dueDatePropose.setDate(dueDatePropose.getDate() + 2);
                }
                if (dueDatePropose.getDay() === 0) {
                    dueDatePropose.setDate(dueDatePropose.getDate() + 1);
                }
                $scope.form.DueDateAccept = new Date(dueDatePropose);
                //Set date Accept
                var dueDateAccept = new Date($scope.form.DueDateAccept);
                dueDateAccept.setDate(dueDateAccept.getDate() + 2);
                if (dueDateAccept.getDay() === 6) {
                    dueDateAccept.setDate(dueDateAccept.getDate() + 2);
                }
                if (dueDateAccept.getDay() === 0) {
                    dueDateAccept.setDate(dueDateAccept.getDate() + 1);
                }
                $scope.form.DueDateApprove = new Date(dueDateAccept);
                //Set date Final Approve
                var dueDateFinalApprove = new Date($scope.form.DueDateApprove);
                dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 2);
                if (dueDateFinalApprove.getDay() === 6) {
                    dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 2);
                }
                if (dueDateFinalApprove.getDay() === 0) {
                    dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 1);
                }
                $scope.form.DueDateFinalApprove = new Date(dueDateFinalApprove);
            } // end SetDate

            //
            $scope.changeDueDatePropose = function() {
                //Set date Accept
                var datePropose = new Date($scope.form.DueDatePropose);
                var dateAccept = new Date($scope.form.DueDateAccept);
                if (datePropose.getFullYear() > dateAccept.getFullYear()) {
                    setDateFromPropose();
                } else if (datePropose.getFullYear() === dateAccept.getFullYear() &&
                    datePropose.getMonth() > dateAccept.getMonth()) {
                    setDateFromPropose();
                } else if (datePropose.getFullYear() === dateAccept.getFullYear() &&
                    datePropose.getMonth() === dateAccept.getMonth() &&
                    datePropose.getDate() > dateAccept.getDate()) {
                    setDateFromPropose();
                }
            }

            //Accept
            var setDateFromAccept = function() {
                //Set date Accept
                var dueDateAccept = new Date($scope.form.DueDateAccept);
                dueDateAccept.setDate(dueDateAccept.getDate() + 2);
                if (dueDateAccept.getDay() === 6) {
                    dueDateAccept.setDate(dueDateAccept.getDate() + 2);
                }
                if (dueDateAccept.getDay() === 0) {
                    dueDateAccept.setDate(dueDateAccept.getDate() + 1);
                }
                $scope.form.DueDateApprove = new Date(dueDateAccept);
                //Set date Final Approve
                var dueDateFinalApprove = new Date($scope.form.DueDateApprove);
                dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 2);
                if (dueDateFinalApprove.getDay() === 6) {
                    dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 2);
                }
                if (dueDateFinalApprove.getDay() === 0) {
                    dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 1);
                }
                $scope.form.DueDateFinalApprove = new Date(dueDateFinalApprove);
            } //end Accept

            //
            $scope.changeDueDateAccept = function() {
                var dateAccept = new Date($scope.form.DueDateAccept);
                var dateApprove = new Date($scope.form.DueDateApprove);
                if (dateAccept.getFullYear() > dateApprove.getFullYear()) {
                    setDateFromAccept();
                } else if (dateAccept.getFullYear() === dateApprove.getFullYear() &&
                    dateAccept.getMonth() > dateApprove.getMonth()) {
                    setDateFromAccept();
                }
                if (dateAccept.getFullYear() === dateApprove.getFullYear() &&
                    dateAccept.getMonth() === dateApprove.getMonth() &&
                    dateAccept.getDate() > dateApprove.getDate()) {
                    setDateFromAccept();
                }
            }

            //Approve
            var setDateFromApprove = function() {
                //Set date Final Approve
                var dueDateFinalApprove = new Date($scope.form.DueDateApprove);
                dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 2);
                if (dueDateFinalApprove.getDay() === 6) {
                    dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 2);
                }
                if (dueDateFinalApprove.getDay() === 0) {
                    dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 1);
                }
                $scope.form.DueDateFinalApprove = new Date(dueDateFinalApprove);
            }

            //
            $scope.changeDueDateApprove = function() {
                var dateApprove = new Date($scope.form.DueDateApprove);
                var dateFinalApprove = new Date($scope.form.DueDateFinalApprove);
                if (dateApprove.getFullYear() > dateFinalApprove.getFullYear()) {
                    setDateFromApprove();
                } else if (dateApprove.getFullYear() === dateFinalApprove.getFullYear() &&
                    dateApprove.getMonth() > dateFinalApprove.getMonth()) {
                    setDateFromApprove();
                } else if (dateApprove.getFullYear() === dateFinalApprove.getFullYear() &&
                    dateApprove.getMonth() === dateFinalApprove.getMonth() &&
                    dateApprove.getDate() > dateFinalApprove.getDate()) {
                    setDateFromApprove();
                }
            }

            //
            $scope.temp = { listPlace: [] }
            $scope.uploadHandler = uploadHandler.init();
            $scope.attchmentSaveUrl = angular.crs.url.webApi("memoincome/:id/attachment");

            var setmemoincomeLinesGridOptionGrid = function (d) {
                $scope.form.MemoIncomeLines = new kendo.data.ObservableArray(d);
                if (_memoincomeLinesGridOption) {
                    _memoincomeLinesGridOption.dataSource.data($scope.form.MemoIncomeLines);
                }
            }

            var setinvoiceGridOptionGrid = function(d) {
                $scope.form.MemoIncomeInvoice = new kendo.data.ObservableArray(d);
                if (_invoiceGridOption) {
                    _invoiceGridOption.dataSource.data($scope.form.MemoIncomeInvoice);
                }
            }

            //Approver
            //Check read only
            //Check that user push "Get final approve" or not.
            var readonly;
            $scope.disabled = false;
            if ($routeParams.mode.toUpperCase() !== "CREATE" && $routeParams.mode.toUpperCase() !== "REVISE") {
                $scope.disabled = true;
                readonly = true;
                $scope.approverLevelFiveAndSix = true;
            } else {
                $scope.disabled = false;
                readonly = false;
                $scope.approverLevelFiveAndSix = false;
            }

            //Check add data
            var approverGridOptionLevelFourDataCheck = false;

            //Get Final Approver
            $scope.getFinalApproverData = function() {
                if ($routeParams.mode.toUpperCase() !== "CREATE" && $routeParams.mode.toUpperCase() !== "REVISE") {
                    return false;
                }
                //if (!_memoLinesGridOption ||
                //    !_memoLinesGridOption.dataSource ||
                //    !_memoLinesGridOption.dataSource._aggregateResult ||
                //    !_memoLinesGridOption.dataSource._aggregateResult.NetAmount ||
                //    !_memoLinesGridOption.dataSource._aggregateResult.NetAmount.sum) {
                //    $scope.form.Budget = 0;
                //} else {
                //    var total = _memoLinesGridOption.dataSource._aggregateResult.NetAmount.sum;
                //    //$scope.form.Budget = total;
                //}
                var id = 0;
                if ($routeParams.id !== undefined) {
                    id = $routeParams.id;
                } else if (sn) {
                    id = $scope.form.Id;
                }

                // final approve
                if ($scope.form.UnitCode && $scope.form.TotalAmount > 0) {
                    checkFinalApproverButton = true;
                    dataSource.finalApprover(
                        { budget: $scope.form.TotalAmount, id: id, unitcode: $scope.form.UnitCode.UnitCode },
                        function(data) {
                            if (data && data.Employee) {
                                approverGridOptionLevelFourDataCheck = true;
                                var approverFinal = {
                                    Employee: data.Employee,
                                    ApproverSequence: data.ApproverSequence,
                                    ApproverLevel: data.ApproverLevel,
                                    ApproverUserName: data.ApproverUserName,
                                    ApproverEmail: data.ApproverEmail,
                                    Position: data.Position,
                                    Department: data.Department,
                                    DueDate: data.DueDate,
                                    LineNo: 1,
                                    LockEditable: 1
                                };
                                //console.log(approverFinal.Deleted);
                                // $scope.ApproverGridOptionLevelFourData.push(approverFinal);

                                $scope.ApproverGridOptionLevelFourData = [];
                                $scope.ApproverGridOptionLevelFourData.push(approverFinal);
                                //$scope.ApproverGridOptionLevelFour.dataSource.data($scope.ApproverGridOptionLevelFourData);
                                //$scope.ApproverGridOptionLevelFourData = $scope.ApproverGridOptionLevelFour.dataSource.data();
                            } else {
                                messageBox.error("ไม่พบผู้อนุมัติคนสุดท้าย กรุณาติดต่อผู้ดูและระบบ");
                                $scope.ApproverGridOptionLevelFourData = new kendo.data.ObservableArray([]);
                                //$scope.ApproverGridOptionLevelFour.dataSource.data($scope.ApproverGridOptionLevelFourData);
                                //$scope.ApproverGridOptionLevelFourData = $scope.ApproverGridOptionLevelFour.dataSource.data();
                            }
                            $scope.ApproverGridOptionLevelFour.dataSource.data($scope.ApproverGridOptionLevelFourData);
                            $scope.ApproverGridOptionLevelFourData =
                                $scope.ApproverGridOptionLevelFour.dataSource.data();
                        });
                } else {
                    if (!$scope.form.UnitCode)
                        messageBox.error("Please select unit code!!!");
                    if ($scope.form.TotalAmount <= 0)
                        messageBox.error("Total Amount must not be zero!!!");
                }
            } // end final approve

            //Merge All Approver to One Array before save
            var mergeArrayData = function(data, array) {
                angular.forEach(array,
                    function(value, key) {
                        data.push(value);
                    });
                return data;
            }

            //Set approver grid data source on load data process
            var setApprovalGridDataSource = function(key, lv, data) {
                var approverData = data.filter(function(result) {
                    return result.ApproverLevel === lv;
                });
                if (lv === 'เสนอ') {
                    if (approverData.length === 0) {
                        $scope.ApproverGridOptionLevelOneData = new kendo.data.ObservableArray([]);
                    } else {
                        $scope.ApproverGridOptionLevelOneData = [];
                        angular.forEach(approverData,
                            function(value, key) {
                                $scope.ApproverGridOptionLevelOneData.push(value);
                            });
                    }
                    if (!isSave) {
                        var gridLevelOne =
                            approverGrids.ApproverGridOption(readonly, $scope.ApproverGridOptionLevelOneData, lv);
                        $scope.ApproverGridOptionLevelOne = gridLevelOne;
                    }
                    $scope.ApproverGridOptionLevelOne.dataSource.data($scope.ApproverGridOptionLevelOneData);
                    $scope.ApproverGridOptionLevelOneData = $scope.ApproverGridOptionLevelOne.dataSource.data();
                } else if (lv === 'เห็นชอบ') {
                    if (approverData.length === 0) {
                        $scope.ApproverGridOptionLevelTwoData = new kendo.data.ObservableArray([]);
                    } else {
                        $scope.ApproverGridOptionLevelTwoData = [];
                        angular.forEach(approverData,
                            function(value, key) {
                                $scope.ApproverGridOptionLevelTwoData.push(value);
                            });
                    }
                    if (!isSave) {
                        var gridLevelTwo =
                            approverGrids.ApproverGridOption(readonly, $scope.ApproverGridOptionLevelTwoData, lv);
                        $scope.ApproverGridOptionLevelTwo = gridLevelTwo;
                    }
                    $scope.ApproverGridOptionLevelTwo.dataSource.data($scope.ApproverGridOptionLevelTwoData);
                    $scope.ApproverGridOptionLevelTwoData = $scope.ApproverGridOptionLevelTwo.dataSource.data();
                } else if (lv === 'อนุมัติ') {
                    if (approverData.length === 0) {
                        $scope.ApproverGridOptionLevelThreeData = new kendo.data.ObservableArray([]);
                    } else {
                        $scope.ApproverGridOptionLevelThreeData = [];
                        angular.forEach(approverData,
                            function(value, key) {
                                $scope.ApproverGridOptionLevelThreeData.push(value);
                            });
                    }
                    if (!isSave) {
                        var gridLevelThree =
                            approverGrids.ApproverGridOption(readonly, $scope.ApproverGridOptionLevelThreeData, lv);
                        $scope.ApproverGridOptionLevelThree = gridLevelThree;
                    }
                    $scope.ApproverGridOptionLevelThree.dataSource.data($scope.ApproverGridOptionLevelThreeData);
                    $scope.ApproverGridOptionLevelThreeData = $scope.ApproverGridOptionLevelThree.dataSource.data();
                } else if (lv === 'อนุมัติขั้นสุดท้าย') {
                    approverGridOptionLevelFourDataCheck = true;
                    if (approverData.length === 0) {
                        checkFinalApproverButton = false;
                        $scope.ApproverGridOptionLevelFourData = new kendo.data.ObservableArray([]);
                    } else {
                        checkFinalApproverButton = true;
                        $scope.ApproverGridOptionLevelFourData = [];
                        angular.forEach(approverData,
                            function(value, key) {
                                $scope.ApproverGridOptionLevelFourData.push(value);
                            });
                    }
                    if (!isSave) {
                        var gridLevelFour =
                            approverGrids.ApproverGridOptionLevelFour(readonly, $scope.ApproverGridOptionLevelFourData);
                        $scope.ApproverGridOptionLevelFour = gridLevelFour;
                    }
                    $scope.ApproverGridOptionLevelFour.dataSource.data($scope.ApproverGridOptionLevelFourData);
                    $scope.ApproverGridOptionLevelFourData = $scope.ApproverGridOptionLevelFour.dataSource.data();
                }
            }

            if ($routeParams.id !== undefined) {
                remoteResult = dataSource.get({ id: $routeParams.id },
                    function (data) {
                        $scope.form = data;
                        setmemoincomeLinesGridOptionGrid($scope.form.MemoIncomeLines);
                        setinvoiceGridOptionGrid($scope.form.MemoIncomeInvoice);
                        //$scope.form.BudgetDetail = data.BudgetDetail;
                        for (var j = 0; j < $scope.ApproverLevel.length; j++) {
                            setApprovalGridDataSource(j, $scope.ApproverLevel[j], $scope.form.MemoIncomeApproval);
                        }
                        if (!$scope.form.CCEmail) {
                            $scope.form.CCEmail = [];
                        }
                        if (!$scope.form.InformEmail) {
                            $scope.form.InformEmail = [];
                        }

                        $scope.uploadHandler.setId(remoteResult.Id);
                        //Set date Propose
                        $scope.form.DueDatePropose = new Date($scope.form.DueDatePropose);
                        $scope.form.DueDatePropose.setHours(7);
                        //Set date Accept
                        $scope.form.DueDateAccept = new Date($scope.form.DueDateAccept);
                        $scope.form.DueDateAccept.setHours(7);
                        //Set date Approve
                        $scope.form.DueDateApprove = new Date($scope.form.DueDateApprove);
                        $scope.form.DueDateApprove.setHours(7);
                        //Set date Final Approve
                        $scope.form.DueDateFinalApprove = new Date($scope.form.DueDateFinalApprove);
                        $scope.form.DueDateFinalApprove.setHours(7);

                        //Check Print make uppercase
                        $scope.RequesterCheck = $scope.form.Requester.Username.toUpperCase();
                    }, k2.onSnError);
            } else if (sn) {
                // open form action k2
                remoteResult = dataSource.worklist({ sn: sn },
                    function() {
                        $scope.form = remoteResult;
                        setmemoincomeLinesGridOptionGrid(remoteResult.MemoIncomeLines);
                        setinvoiceGridOptionGrid($scope.form.MemoIncomeInvoice);
                        for (var j = 0; j < $scope.ApproverLevel.length; j++) {
                            setApprovalGridDataSource(j, $scope.ApproverLevel[j], $scope.form.MemoIncomeApproval);
                        }
                        if (!$scope.form.CCEmail) {
                            $scope.form.CCEmail = [];
                        }
                        if (!$scope.form.InformEmail) {
                            $scope.form.InformEmail = [];
                        }
                        $scope.uploadHandler.setId(remoteResult.Id).setSN(sn);

                        //Set date Propose
                        $scope.form.DueDatePropose = new Date($scope.form.DueDatePropose);
                        $scope.form.DueDatePropose.setHours(7);
                        //Set date Accept
                        $scope.form.DueDateAccept = new Date($scope.form.DueDateAccept);
                        $scope.form.DueDateAccept.setHours(7);
                        //Set date Approve
                        $scope.form.DueDateApprove = new Date($scope.form.DueDateApprove);
                        $scope.form.DueDateApprove.setHours(7);
                        //Set date Final Approve
                        $scope.form.DueDateFinalApprove = new Date($scope.form.DueDateFinalApprove);
                        $scope.form.DueDateFinalApprove.setHours(7);

                        $scope.checkCategory = false;
                    }, k2.onSnError);
            } else {
                //new form
                $scope.form.Requester = angular.copy(authService.authentication.me);
                //New Approve
                //Approver Level One(เสนอ) //ทำเป็น Grid ใน ไฟล์เดียวอยู่นะ
                $scope.ApproverGridOptionLevelOneData = new kendo.data.ObservableArray([]);;
                var gridLevelOne = approverGrids.ApproverGridOption(readonly,
                    $scope.ApproverGridOptionLevelOneData,
                    $scope.ApproverLevel[0]);
                $scope.ApproverGridOptionLevelOne = gridLevelOne;
                $scope.ApproverGridOptionLevelOneData = $scope.ApproverGridOptionLevelOne.dataSource.data();
                //Approver Level Two(เห็นชอบ)
                $scope.ApproverGridOptionLevelTwoData = new kendo.data.ObservableArray([]);
                var gridLevelTwo = approverGrids.ApproverGridOption(readonly,
                    $scope.ApproverGridOptionLevelTwoData,
                    $scope.ApproverLevel[1]);
                $scope.ApproverGridOptionLevelTwo = gridLevelTwo;
                $scope.ApproverGridOptionLevelTwoData = $scope.ApproverGridOptionLevelTwo.dataSource.data();
                //Approver Level Three(อนุมัติ)
                $scope.ApproverGridOptionLevelThreeData = new kendo.data.ObservableArray([]);
                var gridLevelThree = approverGrids.ApproverGridOption(readonly,
                    $scope.ApproverGridOptionLevelThreeData,
                    $scope.ApproverLevel[2]);
                $scope.ApproverGridOptionLevelThree = gridLevelThree;
                $scope.ApproverGridOptionLevelThreeData = $scope.ApproverGridOptionLevelThree.dataSource.data();
                //Approver Level Four(อนุมัติขั้นสุดท้าย)
                $scope.ApproverGridOptionLevelFourData = new kendo.data.ObservableArray([]);
                var gridLevelFour =
                    approverGrids.ApproverGridOptionLevelFour(readonly, $scope.ApproverGridOptionLevelFourData);
                $scope.ApproverGridOptionLevelFour = gridLevelFour;
                $scope.ApproverGridOptionLevelFourData = $scope.ApproverGridOptionLevelFour.dataSource.data();

                //Set Due Date
                //Current date
                $scope.currentDate = new Date();
                $scope.currentDatePlusTwo = new Date($scope.currentDate);
                $scope.currentDatePlusTwo.setDate($scope.currentDatePlusTwo.getDate() + 2);
                //Set date Propose
                if ($scope.currentDatePlusTwo.getDay() === 6) {
                    $scope.currentDatePlusTwo.setDate($scope.currentDatePlusTwo.getDate() + 2);
                }
                if ($scope.currentDatePlusTwo.getDay() === 0) {
                    $scope.currentDatePlusTwo.setDate($scope.currentDatePlusTwo.getDate() + 1);
                }
                $scope.form.DueDatePropose = new Date($scope.currentDatePlusTwo);
                //Set date Accept
                dueDatePropose = new Date($scope.form.DueDatePropose);
                dueDatePropose.setDate(dueDatePropose.getDate() + 2);
                if (dueDatePropose.getDay() === 6) {
                    dueDatePropose.setDate(dueDatePropose.getDate() + 2);
                }
                if (dueDatePropose.getDay() === 0) {
                    dueDatePropose.setDate(dueDatePropose.getDate() + 1);
                }
                $scope.form.DueDateAccept = new Date(dueDatePropose);
                //Set date Approve
                dueDateAccept = new Date($scope.form.DueDateAccept);
                dueDateAccept.setDate(dueDateAccept.getDate() + 2);
                if (dueDateAccept.getDay() === 6) {
                    dueDateAccept.setDate(dueDateAccept.getDate() + 2);
                }
                if (dueDateAccept.getDay() === 0) {
                    dueDateAccept.setDate(dueDateAccept.getDate() + 1);
                }
                $scope.form.DueDateApprove = new Date(dueDateAccept);
                //Set date Final Approve
                dueDateFinalApprove = new Date($scope.form.DueDateApprove);
                dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 2);
                if (dueDateFinalApprove.getDay() === 6) {
                    dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 2);
                }
                if (dueDateFinalApprove.getDay() === 0) {
                    dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 1);
                }
                $scope.form.DueDateFinalApprove = new Date(dueDateFinalApprove);
            }

            // data requester
            $scope.requesterOption = controlOptions.popupSearch('employee');
            // data select category
            $scope.incomecategoryOptions = controlOptions.dropdown('category');

            $scope.proposalNumberOption = controlOptions.popupSearch('proposalref');
            $scope.proposalNumberOption2 = $scope.proposalNumberOption();
            $scope.proposalNumberOption2.show = function() {
                this.fields("unitCode").set("value", $scope.form.UnitCode.UnitCode);
                this.fields("category").set("value", $scope.form.Category);
            }


        //var setBudgetDetailOtherIncome = function (d) {
        //    $scope.form.BudgetDetail = $scope.form.IncomeOther.Budget;
        //}

        // เปลี่ยน
        $scope.$watch('form.ProposalRef',
        function () {
            if ($scope.form.ProposalRef) {
                if ($scope.form.Category === "otherincome") {
                    $scope.branchOptions.dataSource.data(new kendo.data.ObservableArray($scope.form.ProposalRef.ListPlace));
                    $scope.showBranch = true;
                    $scope.showIncomeOther = true;
                    $scope.showDeposit = false;
                    if (!$scope.form.Branch) {
                        if ($scope.form.ProposalRef.ListPlace.length === 1) {
                            $scope.form.Branch = ($scope.form.Branch) ? $scope.form.Branch
                                : $scope.form.ProposalRef.ListPlace[0];
                        }
                    } else {
                        $scope.form.Branch = $scope.form.Branch;
                    }

                    $scope.incomeotherOption.dataSource.data($scope.form.ProposalRef.IncomeOther);
                    if (!$scope.form.IncomeOther) {
                        if ($scope.form.ProposalRef.IncomeOther.length === 1) {
                            $scope.form.IncomeOther = ($scope.form.IncomeOther) ? $scope.form.IncomeOther
                                : $scope.form.ProposalRef.IncomeOther[0];
                            $scope.form.BudgetDetail = $scope.form.ProposalRef.IncomeOther[0].Budget;
                        }
                    } else {
                        $scope.form.IncomeOther = $scope.form.IncomeOther;
                    }
                } else if ($scope.form.Category === "depositproposal") {
                    $scope.depositLineOption.dataSource.data($scope.form.ProposalRef.DepositLine);
                    $scope.showBranch = false;
                    $scope.showIncomeOther = false;
                    $scope.showDeposit = true;
                    if (!$scope.form.DepositLine) {
                        if ($scope.form.ProposalRef.DepositLine.length === 1) {
                            $scope.form.DepositLine = ($scope.form.DepositLine) ? $scope.form.DepositLine
                                : $scope.form.ProposalRef.DepositLine[0];
                            $scope.form.BudgetDetail = $scope.form.ProposalRef.DepositLine[0].Amount;
                        }
                    } else {
                        $scope.form.DepositLine = $scope.form.DepositLine;
                    }
                }
            } else {
                $scope.incomeotherOption.dataSource.data(new kendo.data.ObservableArray([]));
            }
        });

        $scope.incomeotherOption = {
            dataSource: new kendo.data.DataSource({
                type: "json",
                data: new kendo.data.ObservableArray([])
            }),
            optionLabel: "please select",
            dataTextField: 'SponcorName',
            dataValueField: 'Id',
            template: '#=data.SponcorName# - #=data.Description#',
            valueTemplate: '#=data.SponcorName# - #=data.Description#',
            filter: "contains"
        }

        $scope.depositLineOption = {
            dataSource: new kendo.data.DataSource({
                type: "json",
                data: new kendo.data.ObservableArray([])
            }),
            optionLabel: "please select",
            dataTextField: 'PaymentNo',
            dataValueField: 'Id',
            template: '#=data.PaymentNo#',
            valueTemplate: '#=data.PaymentNo#',
            filter: "contains"
        }
    
        $scope.incomeotherOption.select = function (data) {
            var dataItem = this.dataItem(data.item);
            $scope.form.BudgetDetail = dataItem.Budget;
        }

        $scope.depositLineOption.select = function (data) {
            var dataItem = this.dataItem(data.item);
            $scope.form.BudgetDetail = dataItem.Amount;
        }

        $scope.employeeOption = approverGrids.employeeOption;
        //$scope.accountingOption = controlOptions.popupSearch('accounting', { multiple: true });

        //Branch Accounting Dropdown
        $scope.branchAccountingOptions = {
                dataTextField: "PlaceName",
                dataValueField: "PlaceCode",
                autoBind: false,
                optionLabel: "please select",
                template: '#=data.PlaceCode# - #=data.PlaceName#',
                valueTemplate: '#=data.PlaceCode# - #=data.PlaceName#',
                filter: "contains",
                //enable: false,
                dataSource: {
                    type: 'json',
                    //serverFiltering: true,
                    transport: {
                        read: {
                            url: function () {
                                return angular.crs.url.webApi("accounting/getaccountingbranch");
                            }
                        }
                    }
                }
            }

        $scope.unitcodeOptionNew = { //function () {
            dataSource: {
                type: "json",
                data: angular.copy(authService.authentication.me).UnitCode
            },
            optionLabel: "please select",
            dataTextField: 'UnitName',
            dataValueField: 'UnitCode',
            template: '#=data.UnitName#',
            valueTemplate: '#=data.UnitName#',
            filter: "contains",
            filtering: function (ev) {
                if (ev.filter) {
                    var filterValue = ev.filter.value;
                    ev.preventDefault();
                    this.dataSource.filter({
                        logic: "or",
                        filters: [
                            {
                                field: "UnitName",
                                operator: "contains",
                                value: filterValue
                            },
                            {
                                field: "UnitCode",
                                operator: "contains",
                                value: filterValue
                            }
                        ]
                    });
                } else {
                    ev.preventDefault();
                }
            }
        }
        if (angular.copy(authService.authentication.me).UnitCode.length === 1) {
            //console.log(angular.copy(authService.authentication.me).UnitCode[0]);
            $scope.form.UnitCode = angular.copy(authService.authentication.me).UnitCode[0];
            if (sn) {
                $scope.checkedUnitCode = false;
            } else {
                $scope.checkedUnitCode = true;
            }
        }

        //Text Editor
        var originwidth = 0;
        var originheight = 0;
        $scope.trustAsHtml = function (string) {
            return $sce.trustAsHtml(string);
        };
        $scope.DescriptionEditor = {
            paste: function (e) {
                setTimeout(function () {
                    $("iframe").contents().find("div").attr("style", "max-width: 100%");
                    $("iframe").contents().find("img").attr("style", "max-width:100%;");
                }, 500);
            },
            tools:  [
                "bold",
                "italic",
                "underline",
                "strikethrough",
                "justifyLeft",
                "justifyCenter",
                "justifyRight",
                "justifyFull",
                "insertUnorderedList",
                "insertOrderedList",
                "fontName",
                "fontSize",
                "foreColor",
                "backColor",
                "createTable",
                "print",
                {
                    name: "maximize",
                    tooltip: "Maximize",
                    exec: function (e) {

                        if (originheight === 0 || originwidth === 0) {
                            originheight = $("td.k-editable-area iframe.k-content").height();
                            originwidth = $("td.k-editable-area iframe.k-content").width();
                        }
                        var editor = $(this).data("kendoEditor");
                        editor.wrapper.css({
                            "z-index": 9999,
                            width: $(window).width(),
                            height: $(window).height(),
                            position: "fixed",
                            left: 0,
                            top: 0
                            //left: $("aside.main-sidebar .ng-scope").width(),
                            //top: $("nav.navbar.navbar-static-top").height()
                        });
                        //editor.wrapper.css("height", "800px");
                    }
                },
                {
                    name: "restore",
                    tooltip: "Restore",
                    exec: function (e) {
                        var editor = $(this).data("kendoEditor");
                        editor.wrapper.css({
                            width: originwidth,
                            height: originheight,
                            position: "relative"
                        });
                    }
                },
            ]
        }

        //
        var invoiceSchema = {
            model: kendo.data.Model.define({
                id: "LineNo",
                fields: {
                    //LineNo: { type: "number-default" },
                    LineNo: { type: "number" },
                    InvoiceNo: { type: "string", validation: { required: true } },
                    Actual: { type: "number", validation: { min: 0, required: true } },
                    InvoiceDate: { type: "date"}
                }
            })
        };

        //
        $scope.InvoiceGridOption = function () {
            _invoiceGridOption = lineGrid.gridOption({
                schema: invoiceSchema,
                data: $scope.form.MemoIncomeInvoice,
                aggregate: [
                    { field: "Actual", aggregate: "sum" },
                ],
                columns: [
                {
                    field: "InvoiceNo",
                    title: "InvoiceNo",
                    width: "180px",
                    template: function (data) {
                        if (data.InvoiceNo !== undefined) {
                            return "<div style='width:100%; word-wrap: break-word;'>" + data.InvoiceNo + "</div>";
                        }
                        return "";
                    },
                    footerTemplate: "Total"
                },
                {
                    field: "Actual",
                    title: "Actual",
                    width: "100px",
                    template: lineGrid.formatCurrency('Actual'),
                    footerTemplate: 
                    "<div class='text-right'>#:(sum)? kendo.toString(sum,'n2'):0.00#</div>"
                },
                {
                    field: "InvoiceDate",
                    title: "InvoiceDate",
                    width: "120px",
                    template: lineGrid.formatDateTime('InvoiceDate'),
                    editor: function (container, options) {
                        var editor = $(' <input kendo-date-picker k-ng-model="dataItem.' + options.field + '" k-format="\'dd/MM/yyyy\'"/>');
                        editor.appendTo(container);
                        return editor;
                    }
                },
                {
                    field: "Remark",
                    title: "Remark",
                    width: "140px",
                    template: function (data) {
                        if (data.Remark !== undefined && data.Remark !== null) {
                            return "<div style='width:100%; word-wrap: break-word;'>" + data.Remark + "</div>";
                        }
                        return "";
                    }
                }],
                editable: {
                    mode: "incell"
                },
                readonly: !$scope.isAccount(),
                height: "400px",
                noedit: true,
                //template: $('#_IncomeDeposit').html(),
                save: function (e) {
                    this.saveChanges();
                    //$scope.setBudget(this.dataSource.data());
                },
                pageable: false,
                groupable: true
            });
            return _invoiceGridOption;
        }

        // 
        var memoincomeLineSchema = {
            model: kendo.data.Model.define({
                id: "LineNo",
                fields: {
                    //LineNo: { type: "number-default" },
                    LineNo: { type: "number" },
                    Description: { type: "string", validation: { required: true } },
                    Unit: { type: "number", validation: { min: 0,required: true } },
                    Amount: { type: "number", validation: { min: 0, required: true } },
                    Vat: { type: "number", validation: { min: 0, required: true } },
                    Tax: { type: "number", validation: { min: 0, required: true } },
                    NetAmount: { type: "number", editable: false },
                    NetAmountNoVatTax: { type: "number", validation: { min: 0 } }
                }
            })
        };
        
        //
        $scope.MemoIncomeLinesGridOption = function () {
            _memoincomeLinesGridOption = lineGrid.gridOption({
                schema: memoincomeLineSchema,
                data: $scope.form.MemoIncomeLines,
                aggregate: [
                    { field: "Amount", aggregate: "sum" },
                    { field: "NetAmount", aggregate: "sum" },
                    { field: "NetAmountNoVatTax", aggregate: "sum" }
                ],
                columns: [
                {
                    field: "Description",
                    title: "Description",
                    width: "180px",
                    template: function(data) {
                        if (data.Description !== undefined) {
                            return "<div style='width:100%; word-wrap: break-word;'>" + data.Description + "</div>";
                        }
                        return "";
                    },
                    footerTemplate: "Total"
                },
                {
                    field: "Unit",
                    title: "Unit",
                    width: "100px",
                    template: lineGrid.formatCurrency('Unit')
                },
                {
                    field: "Amount",
                    title: "Price/Unit",
                    width: "150px",
                    template: lineGrid.formatCurrency('Amount')
                    //,footerTemplate: "<div class='text-right'>Sum Amount {{sumAmount() | currency:' ':2}}</div>"
                },
                {
                    field: "Vat",
                    title: "VAT %",
                    width: "60px",
                    template: lineGrid.formatCurrency('Vat'),
                    editor: function(container, options) {
                        $scope.vatValue = approverGrids.vatOption;
                        var editor = $('<select kendo-drop-down-list name="Vat" k-options="vatValue" style="width: 100%"></select>');
                        editor.attr("data-bind", "value:Vat,source:null");
                        editor.attr("style", "width:100%; word-wrap: break-word;");
                        editor.appendTo(container);
                        return editor;
                    }
                },
                {
                    field: "Tax",
                    title: "TAX %",
                    width: "60px",
                    template: lineGrid.formatCurrency('Tax'),
                    editor: function(container, options) {
                        $scope.taxValue = approverGrids.taxOption;
                        var editor = $('<select kendo-drop-down-list name="Tax" k-options="taxValue" style="width: 100%"></select>');
                        editor.attr("data-bind", "value:Tax,source:null");
                        editor.attr("style", "width:100%; word-wrap: break-word;");
                        editor.appendTo(container);
                        return editor;
                    }
                },
                {
                    field: "NetAmount",
                    title: "Net amount",
                    width: "150px",
                    template: lineGrid.formatCurrency('NetAmount'),
                    footerTemplate: //"<div class='col-xs-6 col-sm-6 col-md-3 col-lg-3'>Total</div>" +
                        "<div class='text-right'>{{sumNetAmount() | currency:' ':2}}</div>"
                    //"<div class='text-right'>#:(sum)? kendo.toString(sum,'n2'):0.00#</div>"
                },
                {
                    field: "Remark",
                    title: "Remark",
                    width: "140px",
                    template: function(data) {
                        if (data.Remark !== undefined && data.Remark !== null) {
                            return "<div style='width:100%; word-wrap: break-word;'>" + data.Remark + "</div>";
                        }
                        return "";
                    }
                },
                {
                    field: "NetAmountNoVatTax",
                    title: "NetAmountNoVatTax",
                    width: "150px",
                    template: lineGrid.formatCurrency('NetAmountNoVatTax'),
                    footerTemplate: "<div class='text-right'>{{sumNetAmountNoVatTax() | currency:' ':2}}</div>",
                    hidden: true
                }

                ],
                editable: {
                    mode: "incell"
                },
                
                readonly: $scope.isReadonlyDetails(),
                height:"400px",
                noedit: true,
                //template: $('#_IncomeDeposit').html(),
                save: function (e) {
                    $scope.calNetAmount(e);
                    $scope.calNetAmountNoTaxVat(e);
                    checkFinalApproverButton = false;
                    this.saveChanges();
                    //$scope.setBudget(this.dataSource.data());
                },
                pageable: false,
                groupable: true
            });
            return _memoincomeLinesGridOption;
        }

        //Cal net amount
        $scope.calNetAmount = function (e)
        {
            var unitValue = e.model.Unit;
            if (e.values.Unit || e.values.Unit === 0) {
                unitValue = e.values.Unit;
            }
            //
            var vatValue = e.model.Vat;
            if (e.values.Vat || e.values.Vat === 0) {
                vatValue = e.values.Vat;
            }
            //
            var taxValue = e.model.Tax;
            if (e.values.Tax || e.values.Tax === 0) {
                taxValue = e.values.Tax;
            }
            //
            var amountValue = e.model.Amount;
            if (e.values.Amount || e.values.Amount === 0) {
                amountValue = e.values.Amount;
            }
            var rawSumAmountValue = unitValue * amountValue;
            var vat;
            var amountWithVatAndTax;
            var tax;
            if (taxValue !== 0 && vatValue !== 0) {
                tax = ((taxValue * rawSumAmountValue) / 100).toFixed(2);
                vat = ((vatValue * rawSumAmountValue) / 100).toFixed(2);
                amountWithVatAndTax = vat - tax;
                e.model.NetAmount = rawSumAmountValue + amountWithVatAndTax;
            } else {
                if (taxValue === 0 && vatValue !== 0) {
                    vat = parseFloat(((vatValue * rawSumAmountValue) / 100).toFixed(2));
                    e.model.NetAmount = rawSumAmountValue + parseFloat(vat);
                } else {
                    if (taxValue !== 0 && vatValue === 0) {
                        tax = parseFloat(((taxValue * rawSumAmountValue) / 100).toFixed(2));
                        var sum = rawSumAmountValue - tax;
                        e.model.NetAmount = sum;
                    } else {
                        if (taxValue === 0 && vatValue === 0) {
                            e.model.NetAmount = rawSumAmountValue;
                        }
                    }
                }
            }
            e.model.NetAmount = parseFloat(Math.round(e.model.NetAmount * 100) / 100).toFixed(2); //
            return e.model;
        }

        //Cal net amount no vat tax
        //Set budget 
        $scope.calNetAmountNoTaxVat = function (e) {
            var unitValue = e.model.Unit;
            if (e.values.Unit || e.values.Unit === 0) {
                unitValue = e.values.Unit;
            }
            var amountValue = e.model.Amount;
            if (e.values.Amount || e.values.Amount === 0) {
                amountValue = e.values.Amount;
            }
            var rawSumAmountValue = unitValue * amountValue;
            e.model.NetAmountNoVatTax = rawSumAmountValue;
            return e.model;
        }

        //check net budget
        $scope.sumNetAmount = function () {
            if (!_memoincomeLinesGridOption ||
                !_memoincomeLinesGridOption.dataSource ||
                !_memoincomeLinesGridOption.dataSource._aggregateResult ||
                !_memoincomeLinesGridOption.dataSource._aggregateResult.NetAmount ||
                !_memoincomeLinesGridOption.dataSource._aggregateResult.NetAmount.sum) {
                // $scope.form.BudgetDetail = 0;
                return 0; //$scope.form.BudgetDetail;
            }
            var total = _memoincomeLinesGridOption.dataSource._aggregateResult.NetAmount.sum;
            //  $scope.form.BudgetDetail = total;
            return total;
        }

        //NetAmountNoVatTax
        $scope.sumNetAmountNoVatTax = function () {
            if (!_memoincomeLinesGridOption ||
                !_memoincomeLinesGridOption.dataSource ||
                !_memoincomeLinesGridOption.dataSource._aggregateResult ||
                !_memoincomeLinesGridOption.dataSource._aggregateResult.NetAmountNoVatTax ||
                !_memoincomeLinesGridOption.dataSource._aggregateResult.NetAmountNoVatTax.sum) {
                $scope.form.TotalAmount = 0;
                return 0; //$scope.form.BudgetDetail;
            }
            var total2 = _memoincomeLinesGridOption.dataSource._aggregateResult.NetAmountNoVatTax.sum;
            $scope.form.TotalAmount = total2;
            return total2;
        }

        $scope.validateBranch = function () {
            if ($scope.form.Branch !== null) {
                if ($scope.form.Branch.PlaceCode !== null) {
                    return true;
                }
                else {
                    return false;
                }
            } else {
                return false;
            }
        }

        //Save && Submit
        $scope.validate = function (event, preventSubmit) {
            var iCheck = true;
            $scope.setDataApproverBeforeSave();

            if (preventSubmit !== false) { // click save
                checkFinalApproverButton = true;
                if ($scope.form.Category && $scope.form.ProposalRef) {
                    if ($scope.form.Category === "otherincome") {
                        if ($scope.form.IncomeOther === "" || $scope.form.IncomeOther == null || $scope.form.IncomeOther.SponcorName === "please select") {
                            messageBox.error("Please select Income other source!!!");
                            iCheck = false;
                        }
                    } else if ($scope.form.Category === "depositproposal") {
                        if ($scope.form.DepositLine === "" || $scope.form.DepositLine == null || $scope.form.DepositLine.PaymentNo === "please select") {
                            messageBox.error("Please select Deposit line!!!");
                            iCheck = false;
                        }
                    }
                }
            } else {
                if (!$scope.validateBranch()) {
                    messageBox.error("Please select branch!!!");
                    iCheck = false;
                }
            }
            if (iCheck) {
                if (checkFinalApproverButton) {
                        k2.saveAndSubmit({
                            event: event,
                            preventSubmit: preventSubmit,
                            validator: $scope.validator,
                            save: dataSource.save,
                            data: $scope.form,
                            saveSuccess: function (response) {
                                if ($routeParams.mode.toUpperCase() === "ACC-APPROVE" || $routeParams.mode.toUpperCase() === "REVISE") {
                                    location.reload();
                                } else {
                                    $scope.form = response;
                                    isSave = true;
                                    $location.path("memoincome/create/" + response.Id);
                                    setmemoincomeLinesGridOptionGrid(response.MemoIncomeLines);
                                    setinvoiceGridOptionGrid(response.MemoIncomeInvoice);

                                    for (var j = 0; j < $scope.ApproverLevel.length; j++) {
                                        setApprovalGridDataSource(j, $scope.ApproverLevel[j], $scope.form.MemoIncomeApproval);
                                    }
                                    if (!$scope.form.CCEmail) {
                                        $scope.form.CCEmail = [];
                                    }
                                    if (!$scope.form.InformEmail) {
                                        $scope.form.InformEmail = [];
                                    }
                                    $("#ccEmail").data("kendoMultiSelect").dataSource.data($scope.form.CCEmail);
                                    $("#informEmail").data("kendoMultiSelect").dataSource.data($scope.form.InformEmail);

                                    //Set date Approver 
                                    $scope.form.DueDatePropose = new Date($scope.form.DueDatePropose);
                                    $scope.form.DueDatePropose.setHours(7);
                                    $scope.form.DueDateAccept = new Date($scope.form.DueDateAccept);
                                    $scope.form.DueDateAccept.setHours(7);
                                    $scope.form.DueDateApprove = new Date($scope.form.DueDateApprove);
                                    $scope.form.DueDateApprove.setHours(7);
                                    $scope.form.DueDateFinalApprove = new Date($scope.form.DueDateFinalApprove);
                                    $scope.form.DueDateFinalApprove.setHours(7);
                                }
                            },
                            submitSuccess: function (response) {
                            },
                            uploadHandler: $scope.uploadHandler
                        });
                } else {
                    messageBox.error("Please push get final approver button!!!");
                }
             }
          }

        //
        $scope.openK2ActionModal = function (action) {
            $scope.choosenAction = action;
            $scope.modalInstance = k2.createActionModal.call($scope);
        }

        //action approve
        $scope.chooseAction = function (action) {
            if (checkFinalApproverButton) {
                    $scope.Action = action;
                    //Approver
                    $scope.setDataApproverBeforeSave();
                    k2.chooseAction({
                        validator: $scope.validator,
                        actions: $scope.form.Actions,
                        action: action,
                        resource: $scope.form,
                        uploadHandler: $scope.uploadHandler
                    });
            } else {
                messageBox.error("Please push get final approver button!!!");
            }
        }

        // Set Data Before Save (data approver)
        $scope.setDataApproverBeforeSave = function () {
            $scope.form.MemoIncomeApproval = new kendo.data.ObservableArray([]);
            $scope.form.MemoIncomeApproval =
                mergeArrayData($scope.form.MemoIncomeApproval, $scope.ApproverGridOptionLevelOneData);
            $scope.form.MemoIncomeApproval =
                mergeArrayData($scope.form.MemoIncomeApproval, $scope.ApproverGridOptionLevelTwoData);
            $scope.form.MemoIncomeApproval =
                mergeArrayData($scope.form.MemoIncomeApproval, $scope.ApproverGridOptionLevelThreeData);
            $scope.form.MemoIncomeApproval =
                mergeArrayData($scope.form.MemoIncomeApproval, $scope.ApproverGridOptionLevelFourData);
        }; // end approver


        $scope.unitCodeOptionChange = function () {
            $scope.form.ProposalRef = "";
            $scope.form.ProposalRef.DocumentNumber = "";
            $scope.form.ProposalRef.Title = "";
            $scope.form.IncomeOther = "";
            $scope.form.DepositLine = "";
            $scope.form.BudgetDetail = 0;
            $scope.form.Branch = "";
            $scope.proposalNumberOption2.gridOptions.dataSource.data([]);
            $scope.depositLineOption.dataSource.data([]);
            $scope.incomeotherOption.dataSource.data([]);
            $scope.branchOptions.dataSource.data([]);
            if ($scope.form.UnitCode) {
                if ($scope.form.UnitCode.UnitCode != "") {
                    $scope.checkedUnitCode = true;
                } else {
                    $scope.checkedUnitCode = false;
                }
            }
            if ($scope.form.Category) {
                $scope.checkCategory = true;
            }
            else
            {
                $scope.checkCategory = false;
                $scope.showBranch = false;
                $scope.showIncomeOther = false;
                $scope.showDeposit = false;
            }               
        }
        
        //Category Dropdown OnChange Function
        $scope.CategoryOptionChange = function (category) {
            $scope.form.ProposalRef = "";
            $scope.form.ProposalRef.DocumentNumber = "";
            $scope.form.ProposalRef.Title = "";
            $scope.form.IncomeOther = "";
            $scope.form.DepositLine = "";
            $scope.form.BudgetDetail = 0;
            $scope.form.Branch = "";
            $scope.proposalNumberOption2.gridOptions.dataSource.data([]);
            $scope.depositLineOption.dataSource.data([]);
            $scope.incomeotherOption.dataSource.data([]);
            $scope.branchOptions.dataSource.data([]);
            if ($scope.form.Category) { // เลือก category ใหม่
                $scope.checkCategory = true;
                //if ($scope.form.ProposalRef) { // proposal
                //    $scope.form.ProposalRef = "";
                //    $scope.form.ProposalRef.DocumentNumber = "";
                //    $scope.form.ProposalRef.Title = "";
                //    $scope.proposalNumberOption2.gridOptions.dataSource.data([]); // clear
                //    $scope.form.IncomeOther = "";
                //    $scope.form.DepositLine = "";
                //    $scope.form.BudgetDetail = 0;
                //    $scope.form.Branch = "";
                //}
            } else if ($scope.form.Category === "") { // please select
                $scope.checkCategory = false;
                //$scope.form.ProposalRef = "";
                //$scope.form.ProposalRef.DocumentNumber = "";
                //$scope.form.ProposalRef.Title = "";
                //$scope.proposalNumberOption2.gridOptions.dataSource.data([]);
                //$scope.form.IncomeOther = "";
                //$scope.form.DepositLine = "";
                //$scope.form.BudgetDetail = 0;
                //$scope.form.Branch = "";
                $scope.showBranch = false;
                $scope.showIncomeOther = false;
                $scope.showDeposit = false;
            }
            if ($scope.form.UnitCode.UnitCode != "") {
                $scope.checkedUnitCode = true;
            } else {
                $scope.checkedUnitCode = false;
            }
            if ($scope.form.Category === "otherincome") {
                $scope.showBranch = true;
                $scope.showIncomeOther = true;
                $scope.showDeposit = false;

            } else if ($scope.form.Category === "depositproposal") {
                $scope.showBranch = false;
                $scope.showIncomeOther = false;
                $scope.showDeposit = true;
            }
        }

        $scope.showComments = false;
        if ($routeParams.mode.toUpperCase() !== 'CREATE') {
            $scope.showComments = true;
        }

        //Search by Email 
        $scope.EmailOption = approverGrids.emailOption;

        //Branch Option
        $scope.branchOptions = {
            dataSource: new kendo.data.DataSource({
                type: "json",
                data: $scope.dataBranch
            }),
            dataTextField: "PlaceName",
            dataValueField: "PlaceCode",
            optionLabel: "please select",
            template: '#=data.PlaceCode# - #=data.PlaceName#',
            valueTemplate: '#=data.PlaceCode# - #=data.PlaceName#',
            open: function (e) {
                var listContainer = e.sender.list.closest(".k-list-container");
                var table = listContainer.children("template-dropdowlist-header");
                if (table.length > 0) {
                    listContainer.width('auto');
                }
            }
        }

        // branch to accounting
        $scope.BranchToAccounting = function (branch) {
            $scope.form.Accounting = branch;
        }

        //Print Report
        $scope.printReport = function () {
            if ($routeParams.id != undefined) {
                var params = {
                    Id: $routeParams.id,
                    inline: true,
                    //show: true,
                    fullscreen: true,
                }
                var url = angular.crs.url.rootweb("report/MemoIncome/?params=" + JSON.stringify(params));
                return url;
            }           
        }

}]);