﻿angular.module('app.controllers').controller('memoincomeaccountController',
    ['$scope', '$routeParams', 'inquiryGrid', 'IdProposalService', 'authService', '$window', 'messageBox', '$location', 'memoincomeDataSource', 'sweetAlert',
    function ($scope, $routeParams, inquiryGrid, idProposalService, authService,
        $window, messageBox, $location, memoincomeDataSource, sweetAlert) {

        idProposalService.set(0);

        var gridKendoOptionFunction = function (kendoData, readonly) {
            var indexOption = {
                model: {
                    id: "LineNo",
                    fields: {
                        LineNo: { type: "number" },
                        CurrentId: { type: "number", defaultValue: 0 },
                        CreatedDate: { type: "date" }
                    }
                },
                columns :
                [
                    inquiryGrid.detailColumn,

                     { field: 'Status', title: 'Status', width: '100px' },
                        {
                            field: 'ProposalRefDocumentNumber', title: 'Proposal number', width: '150px',
                            template: function (data) {
                                return '<a href="proposal/detail/' + data.ProposalRefID + '" target="_blank">' + data.ProposalRefDocumentNumber + '<a/>';
                                //return '<a href="proposal/detail/' + data.ProposalRefID + '" target="_blank">' + data.ProposalRefDocumentNumber + '<a/>';
                            },
                        },
                       { field: 'DocumentNumber', title: 'Memo income number', width: '160px' },
                       {
                           field: 'Title', title: 'Title', width: '200px',
                           template: function (data) {
                               if (data.Title !== undefined) {
                                   return "<div style='width:100%; word-wrap: break-word;'>" + data.Title + "</div>";
                               }
                           }
                       },
                        {
                            field: 'Category', title: 'Category', width: '130px',
                            template: function (data) {
                                if (data.Category === "otherincome") {
                                    return "<div style='width:100%; word-wrap: break-word;'>Income Other Source</div>";
                                } else if (data.Category === "depositproposal") {
                                    return "<div style='width:100%; word-wrap: break-word;'>Deposit Proposal</div>";
                                }
                            }
                        },
                       { field: 'TotalAmount', title: 'Budget', width: '110px', template: inquiryGrid.formatCurrency('TotalAmount') },
                       //{ field: 'Branch', title: 'Branch', width: '90px',
                       //    template: function (data) {
                       //        if (data.Branch !== undefined) {
                       //            return "<div style='width:100%; word-wrap: break-word;'>" + data.Branch + "</div>";
                       //        }
                       //    }
                       //},

                     inquiryGrid.createdDateColumn
                ]
            }

            return {
                dataSource: new kendo.data.DataSource({
                    type: "json",
                    data: kendoData,
                    sort:{ field: "CreatedDate", dir: "desc" },
                    batch: false,
                    pageSize: 5,
                    schema: {
                        total: function(data) { return kendoData.length; },
                        //data: function (data) { return data.value; },
                        model: kendo.data.Model.define({
                            id: "Id",
                            fields: {
                                TotalAmount: { type: "number" },
                                CreatedDate: { type: "date" }
                            }
                        })
                    },
                    error: function(e) {
                    }
                }),
                columns: indexOption.columns,
                readonly: false,
                scrollable: true,
                groupable: false,
                pageable: {
                    refresh: true,
                    pageSizes: [5, 10, 20, 50, 100]
                },
                reorderable: false,
                resizable: false,
                selectable: "row",
                sortable: {
                    allowUnsort: true,
                    mode: "single"
                },
                height: 290,
                mobile: false,
                filterable: {
                    extra: true, //do not show extra filters
                    operators: {
                        // redefine the string operators
                        string: {
                            contains: "Contains"
                        }
                    }
                }
            }
        }

        memoincomeDataSource.getmemoincomeforaccount({ username: CURRENT_USERNAME }, function (data) {
            $scope.gridKendoOptionRelated = gridKendoOptionFunction(data);
        });
    }]);