﻿angular.module('app.controllers').controller('placeFormController',
[
    '$scope', '$routeParams', 'masterGrid', 'controlOptions',
    'placeDataSource', 'messageBox', '$location',
    function($scope, $routeParams, masterGrid, controlOptions,
        dataSource, messageBox, $location) {
        $scope.form = {};
        $scope.checkNew = true;
        $scope.checkMemoType = false;
        $scope.temp = {};

        if (!$routeParams.placeCode) {
            $scope.canDeleted = false;
            $scope.edit = false;
            //   messageBox.error("Sequence number and name are invalid!!!");

            //$location.path("unitcodeforemployee/");
        } else {
            $scope.canDeleted = true;
            $scope.edit = true;
            dataSource.getByPlaceCode({
                placeCode: $routeParams.placeCode
            }, function (data) {
                $scope.checkNew = false;
                $scope.form = data;
                console.log(":)");
                console.log(data);
            });
        }


        $scope.validate = function (event) {
            if (!$scope.validator.validate()) return false;
            if ($scope.checkNew) {
                console.log(":)");
                dataSource.save($scope.form).$promise.then(
                    function (resp) {
                        if (resp) {
                            messageBox.success('Saved successfully');
                            $location.path('place/');
                        }
                    },
                    function (error) {
                        messageBox.extractError(error);
                    }
                );
            } else {
                console.log(":P");
                dataSource.edit($scope.form).$promise.then(
                    function (resp) {
                        if (resp) {
                            messageBox.success('Saved successfully');
                            $location.path('place/');
                        }
                    },
                    function (error) {
                        messageBox.extractError(error);
                    }
                );
            }
        };

        $scope.delete = function () {
            console.log("sd");
            var deleted = confirm("Delete this data ?");
            if (deleted) {
                console.log("8)");
                dataSource.deleted($scope.form).$promise.then(
                     function (resp) {
                         if (resp) {
                             messageBox.success("Delete successfully.");
                             $location.path('place/');
                         }
                     },
                    function (error) {
                        messageBox.extractError(error);
                    }
                    );
            }
        }
    }
]);