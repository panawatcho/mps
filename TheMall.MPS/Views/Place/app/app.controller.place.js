﻿angular.module('app.controllers').controller('placeController',
[
    '$scope', '$routeParams','masterGrid','inquiryGrid',
    function ($scope, $routeParams, masterGrid, inquiryGrid) {
    
        var _gridKendoOption;
        $scope.odataUrl = angular.crs.url.odata('placetable');
        $scope.apiUrl = angular.crs.url.webApi('place');
        var _commandTemplateAllstatus = function (data) {
            return '<div class="btn-group" role="group">' +
                '<a class="btn btn-default" style="width:60px" href="' + $routeParams.page + '/Form?placeCode=' + data.PlaceCode + '" title="edit"><i class="fa fa-edit"></i></a>' +
                '</div>';
        }
        $scope.gridKendoOption = {
            dataSource: new kendo.data.DataSource({
                transport: {
                    read: {
                        async: true,
                        url: $scope.apiUrl,
                        dataType: "json"
                    },
                    parameterMap: function(options, operation) {
                        if (operation !== "read" && options.models) {
                            return {
                                //PlaceCode: options.models[0].PlaceCode,
                                PlaceName: options.models[0].PlaceName,
                                Description: options.models[0].Description,
                                InActive: options.models[0].InActive,
                                Order: options.models[0].Order
                            };
                        }
                        return null;
                    }
                },
                pageSize: 10,
                batch: true,
                schema: {
                    model: {
                        fields:
                        {
                            //PlaceCode: { editable: true, nullable: false },
                            PlaceName: { editable: true, nullable: false },
                            Description: { editable: true, nullable: false },
                            InActive: {editable: true, nullable: false },
                            Order: { editable: true, nullable: false }
                        }
                    }
                }
            }),
            toolbar: ["create"],
            columns: [
            //{
            //    field: "PlaceCode", title: "Place code", width: "200px",
            //    template: function (data) {
            //        if (data.PlaceCode) {
            //            return "<div style='width:100%; word-wrap: break-word;'>" + data.PlaceCode + "</div>";
            //        }
            //        return "";
            //    }
            //},
            {
                field: "PlaceName", title: "Place name", width: "200px",
                template: function (data) {
                    if (data.PlaceName) {
                        return "<div style='width:100%; word-wrap: break-word;'>" + data.PlaceName + "</div>";
                    }
                    return "";
                }
            },
            {
                field: "Description", title: "Description", width: "200px",
                template: function (data) {
                    if (data.Description) {
                        return "<div style='width:100%; word-wrap: break-word;'>" + data.Description + "</div>";
                    }
                    return "";
                }
            },
            {
                field: "InActive",
                title: "Active",
                width: "200px",
                template: '<div class="text-center">#if(!data.InActive){# <i class="fa fa-check" style="color:green;"></i>#}else{#<i class="fa fa-times" style="color:red;"></i> #}#</div>',
                editor: '<input type="checkbox" name="InActive" data-bind="checked:InActive"></input>'
            },
            {
                field: "Order", title: "Order", width: "200px",
                template: inquiryGrid.formatCurrency('Order')
            },
            { template: _commandTemplateAllstatus, width: '100px' }
            ],
            filterable: true,
            pageable: {
                refresh: true,
                pageSizes: true,
                buttonCount: 5
            }
        };
        $scope.gridKendoOption.toolbar = kendo.template($("#placeTemplate").html());

        //$scope.gridKendoOption = function () {
        //    if (!_gridKendoOption) {
        //        $scope.odataUrl = angular.crs.url.odata('placetable');
        //        $scope.apiUrl = angular.crs.url.webApi('place');
              
        //        _gridKendoOption = masterGrid.gridKendoOption($scope.odataUrl, $scope.apiUrl,
        //        {
        //            parameterMap: function (options, operation) {
        //                if (operation !== "read" && options.models) {
        //                    return {
        //                        PlaceCode: options.models[0].PlaceCode,
        //                        PlaceName: options.models[0].PlaceName,
        //                        Description: options.models[0].Description,
        //                        InActive: options.models[0].InActive
        //                    };
        //                }
        //                return null;
        //            },
        //            schema: {
        //                model: {
        //                    id: "PlaceCode",
        //                    fields: {
        //                        PlaceCode: { editable: true, nullable: false },
        //                        PlaceName: { editable: true, nullable: false },
        //                        Description: { editable: true, nullable: false },
        //                        InActive: {editable: true, nullable: false }
        //                    }
        //                }
        //            },
        //            toolbar: ["create"],
        //            columns: [
        //                {
        //                    field: "PlaceCode", title: "Place code", width: "200px" ,
        //                    //editor: function (container, options) {
        //                    //    console.log(options.model.PlaceCode);
        //                    //    if (options.model && options.model.PlaceCode && options.model.PlaceCode !== "") {
        //                    //        console.log("s");
        //                    //        return '<input type="text" name="PlaceCode" disabled></input>';
        //                    //    } else {
        //                    //        console.log("a");
        //                    //        return ;
        //                    //    }
        //                    //}
        //                },
        //                { field: "PlaceName", title: "Place name", width: "200px" },
        //                { field: "Description", title: "Description", width: "200px" },
        //                {
        //                    field: "InActive", title: "In active", width: "200px" ,
        //                    template: '<input type="checkbox" #=InActive? "checked=checked" : "" # disabled="disabled"  ></input>',
        //                    editor: '<input type="checkbox" name="InActive" data-bind="checked:InActive"></input>'
        //                },
        //                { command: ["edit", "destroy"], title: "&nbsp;", width: 200 }
        //            ],
        //            editable:
        //                {
        //                    mode: "inline",
        //                    createAt: "top"
        //                }
        //        });
        //    }
        //    return _gridKendoOption;
        //}
     
        //var _gridKendoOption;
        //$scope.gridKendoOption = function () {
        //    if (!_gridKendoOption) {
        //        $scope.datasourceUrl = angular.crs.url.odata('unitcodetable');
        //        $scope.Url = angular.crs.url.webApi('unitcode');
              
        //        _gridKendoOption = {
        //            dataSource: new kendo.data.DataSource({
        //                transport: {
        //                    read: {
        //                        async: true,
        //                        url: $scope.datasourceUrl,
        //                        dataType: "json",
        //                        data: undefined
        //                    },
        //                    update: {
        //                        url: $scope.Url + "/Update",
        //                        dataType: "json",
        //                        method: "post"
        //                    },
        //                    destroy: {
        //                        url: $scope.Url + "/Delete",
        //                        dataType: "json",
        //                        method: "post"
        //                    },
        //                    create: {
        //                        url: $scope.Url + "/Create",
        //                        dataType: "json",
        //                        method: "post"
        //                    },
        //                    parameterMap: function (options, operation) {
        //                        if (operation !== "read" && options.models) {
        //                            return {
        //                                UnitCode: options.models[0].UnitCode,
        //                                UnitName: options.models[0].UnitName,
                                      
        //                            };
        //                        }
        //                        return null;
        //                    }
        //                },
        //                error: function (e) {
        //                    console.log(e);
        //                    var message = e.xhr.responseJSON.Message;
        //                    alert(message);
        //                },
                       
        //                batch: true,
        //                pageSize: 10,
        //                schema: {
        //                    data: function (data) { return data.value; },
        //                    total: function (data) { return data["odata.count"]; },
        //                    model: {
        //                        id: "UnitCode",
        //                        fields: {
        //                            UnitCode: { editable: true, nullable: false },
        //                            UnitName: { editable: true, nullable: false },
                                   
        //                        }
        //                    }
        //                }
        //            }),
        //            height: 550,
        //            toolbar: ["create"],
        //            columns: [
        //                { field: "UnitCode", title: "Unit Code", width: "200px" },
        //                { field: "UnitName", title: "Unit Namee", width: "200px" },
        //                { command: ["edit", "destroy"], title: "&nbsp;", width: 200 }
        //            ],
                   
        //            pageable: {
        //                refresh: true,
        //                pageSizes: [5, 10, 20, 50, 100]
        //            },
                  
        //            selectable: "row",
        //            editable: {
        //                mode: "inline",
        //                createAt: "top"
        //            }

        //        };
        //    }
        //    return _gridKendoOption;
        //}





    }
]);