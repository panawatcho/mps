﻿angular.module('app.controllers').controller('memoController',
[
    '$scope', '$routeParams', 'inquiryGrid', 'IdProposalService', 'authService',
    '$window', 'messageBox', '$location', 'memoDataSource', 'sweetAlert',
    function ($scope, $routeParams, inquiryGrid, idProposalService, authService,
        $window, messageBox, $location, memoDataSource, sweetAlert) {
        //console.log(angular.copy(authService.authentication.me).Username);
        idProposalService.set(0);

        var commandTemplateAllstatus = function (data) {
            if (data.CreatedBy && data.CreatedBy.toUpperCase() === CURRENT_USERNAME.toUpperCase()) {
                if (data.StatusFlag === null || data.StatusFlag === 0) {
                    return '<div class="btn-group" role="group">' +
                        '<a class="btn btn-default" style=" margin-right: 5px;" href="' + $routeParams.page + '/detail/' + data.Id + '" title="view"><i class="fa fa-file-text"></i></a>' +
                        '<a class="btn btn-default" style=" margin-right: 5px;" href="' + $routeParams.page + '/create/' + data.Id + '" title="edit"><i class="fa fa-edit"></i></a>' +
                        '<a class="btn btn-info" disabled="disabled" style=" margin-right: 5px; margin-bottom: 10px"><span class="fa fa-print"></span></a>' +
                              '<a class="btn btn-default"  style=" background-color:#dd4b39;" ng-click="delete(' + data.Id + ',' + data.StatusFlag + ')" title="Delete"><i class="fa fa-trash-o" style="color:white;"></i></a>' +
                        '</div>';
                }
                else {
                    var htmlButtonV1 = '<div class="btn-group" role="group">' +
                        '<a class="btn btn-default" style=" margin-right: 5px;" href="' + $routeParams.page + '/detail/' + data.Id + '" title="view"><i class="fa fa-file-text"></i></a>' +
                        '<span class="btn btn-default" disabled="disabled" style="margin-right: 5px;" title="edit"><i class="fa fa-edit"></i></span>';
                    if (data.StatusFlag !== null && data.StatusFlag === 9 && data.Status === "Completed") {
                        htmlButtonV1 = htmlButtonV1 + '<a class="btn btn-info" style=" margin-right: 5px; margin-bottom: 10px" ng-href="{{printReport(' + data.Id + ')}}" target="_blank"><span class="fa fa-print"></span></a>' +
                           '<a class="btn btn-default"  style=" background-color:#dd4b39;" ng-click="delete(' + data.Id + ',' + data.StatusFlag + ')" title="Delete"><i class="fa fa-trash-o" style="color:white;"></i></a>' +
                            '</div>';
                    } else {
                        htmlButtonV1 = htmlButtonV1 + '<a class="btn btn-info" disabled="disabled" style=" margin-right: 5px; margin-bottom: 10px"><span class="fa fa-print"></span></a>' +
                            '<span class="btn btn-default" disabled="disabled" style="margin-right: 5px; background-color:#dd4b39" title="Delete"><i class="fa fa-trash-o" style="color:white"></i></span>' +
                            '</div>';
                    }
                    return htmlButtonV1;
                }
            } else {
                return '<div class="btn-group" role="group">' +
                       '<span class="btn btn-default" disabled="disabled" style="margin-right: 5px;" title="edit"><i class="fa fa-file-text"></i></span>' +
                       '<span class="btn btn-default" disabled="disabled" style=" margin-right: 5px;" title="edit"><i class="fa fa-edit"></i></span>' +
                       '<a class="btn btn-info" disabled="disabled" style=" margin-right: 5px; margin-bottom: 10px"><span class="fa fa-print"></span></a>' +
                       '<span class="btn btn-default" disabled="disabled" style="margin-right: 5px; background-color:#dd4b39" title="Delete"><i class="fa fa-trash-o" style="color:white"></i></span>' +
                        '</div>';
            }
        }

        var params = {
            filters:
               [ {
                    field: "CreatedBy",
                    operator: "eq",
                    value: CURRENT_USERNAME
               }, {
                   field: "Deleted",
                   operator: "eq",
                   value: false
                   }
               ]
            , 
            height: '500px',
            //height: 'auto',
            pageSize: 7,
            scrollable: false,
            sorts: inquiryGrid.sortOption,
            fields: {
                BudgetDetail: { type: "number" },
            }
        };
        $scope.gridKendoOption = inquiryGrid.gridKendoOption(angular.crs.url.odata('memotable'),params);
        $scope.gridKendoOption.columns =
            [
                 //inquiryGrid.commandColumn,
                { template: commandTemplateAllstatus, width: '130px' },
                { field: 'Status', title: 'Status', width: '100px' },
                {
                    field: 'ProposalRefDocumentNumber', title: 'Proposal number', width: '150px' ,
                    template: function (data) {
                        return '<a href="proposal/detail/' + data.ProposalRefID + '" target="_blank">' + data.ProposalRefDocumentNumber + '<a/>';
                    },
                },
              { field: 'DocumentNumber', title: 'Memo number', width: '140px', },
                {
                    field: 'Title', title: 'Title', width: '200px',
                    template: function (data) {
                        if (data.Title !== undefined) {
                            return "<div style='width:100%; word-wrap: break-word;'>" + data.Title + "</div>";
                        }

                    }
                },
              { field: 'ExpenseTopicCode', title: 'Expense topic', width: '100px' },
              { field: 'APCode', title: 'A&P code', width: '80px' },
              //{ field: 'Description', title: 'Description', width: '180px' },
              { field: 'BudgetDetail', title: 'Budget', width: '110px', template: inquiryGrid.formatCurrency('BudgetDetail') },
              
             inquiryGrid.createdDateColumn
          ];
        $scope.gridKendoOption.toolbar = kendo.template($("#memoToolbarTemplate").html());



        $scope.printReport = function (memoId) {
            if (memoId) {
                var params = {
                    Id: memoId,
                    inline: true,
                    //show: true,
                    fullscreen: true
                }
                var url = angular.crs.url.rootweb("report/Memo/?params=" + JSON.stringify(params));
                return url;
            }
        }

       
        $scope.delete = function (id , statusFlag) {
           
            if (statusFlag === 9) {
                sweetAlert.swal({
                    title: 'Are you sure?',
                    text: "Cancel this record ?",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, Cancel it!'
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        var result = memoDataSource.deleteBy({ id: id, username: CURRENT_USERNAME },
                        function (data) {
                            sweetAlert.success("Cancel data succesful");
                            $location.path('/memo');
                        },sweetAlert.error("You can't cancel data!"));
                    }
                });
                //if ($window.confirm("Cancel this record ?")) {
                //    var result = memoDataSource.deleteBy({ id: id, username: CURRENT_USERNAME },
                //         function (data) {
                //             messageBox.success("Cancel data succesful");
                //             $location.path('/memo');
                //         });
                //} else {
                //    return false;
                //}
            } else {
                sweetAlert.swal({
                    title: 'Are you sure?',
                    text: "Delete this record ?",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        var result = memoDataSource.deleteBy({ id: id, username: CURRENT_USERNAME },
                        function (data) {
                            sweetAlert.success("Delete data succesful");
                            $location.path('/memo');
                        }, sweetAlert.error("You can't delete data!"));
                    }
                });
                //if ($window.confirm("Delete this record ?")) {
                //    var result = memoDataSource.deleteBy({ id: id, username: CURRENT_USERNAME },
                //         function (data) {
                //             messageBox.success("Delete data succesful");
                //             $location.path('/memo');
                //         });
                //} else {
                //    return false;
                //}
            }
           
        }
    }]);