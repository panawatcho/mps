﻿angular.module('app.controllers').controller('memoCompleteController',
[
    '$scope', '$routeParams', 'inquiryGrid','IdProposalService','authService','memoDataSource',
    function ($scope, $routeParams, inquiryGrid, idProposalService, authService, memoDataSource) {
        //console.log(angular.copy(authService.authentication.me).Username);
        idProposalService.set(0);
        var params = {
          //  filters:
          //  [
          //      { field: "StatusFlag", operator: "eq", value: 9 },
          //  //{
          //  //    field: "CreatedBy",
          //  //    operator: "eq",
          //  //    value: CURRENT_USERNAME
          //  //},
          ////  { field: "UnitCode", operator: "eq", value: angular.copy(authService.authentication.me).UnitCode[0] },
          //  ],
            mobile: true,
            height: '350px',
            //height: 'auto',
            pageSize: 5,
            scrollable: false,
            sorts: inquiryGrid.sortOption,
          

        };
        $scope.gridKendoOption = inquiryGrid.gridKendoOptionApi(angular.crs.url.webApi("memo/related/"), params);
   
        $scope.gridKendoOption.columns =
            [
                inquiryGrid.detailColumn,
              
              { field: 'DocumentStatus', title: 'Status', width: '100px' },
              {
                  field: 'ProposalRef.DocumentNumber', title: 'Proposal number',
                 
                  width: '140px'
              },
              { field: 'DocumentNumber', title: 'Memo number', width: '150px' },
              { field: 'Title', title: 'Title', width: '200px' },
              { field: 'ExpenseTopic.ExpenseTopicCode', title: 'Expense topic', width: '150px' },
              { field: 'APCode.APCode', title: 'A&P code', width: '130px' },
              //{ field: 'Description', title: 'Description', width: '180px' },
              { field: 'BudgetDetail', title: 'Budget', width: '110px' },
              
             inquiryGrid.createdDateColumn,
          ];
      //  $scope.gridKendoOption.toolbar = kendo.template($("#memoToolbarTemplate").html());

    }]);