﻿angular.module('app.controllers').controller('memoFormController',
    ['$scope', '$routeParams', 'controlOptions', 'lineGrid',
    '$attrs', 'memoDataSource', 'uploadHandler', 'k2',
    'cashAdvanceDataSource', 'IdProposalService', 'authService', '$filter',
    'messageBox', '$location', 'approval', '$sce', 'approverGrids',
    function ($scope, $routeParams, controlOptions, lineGrid,
        $attrs, dataSource, uploadHandler, k2,
        cashAdvanceDataSource, idProposalService, authService, $filter,
        messageBox, $location, approval, $sce, approverGrids) {

        //$controller('BaseController', { $scope: $scope, dataSource: dataSource, $attrs: $attrs });
        var id = 0,
            sn = k2.extractSN($routeParams),
            remoteResult,
           _memoLinesGridOption;
        var proposalLineId = idProposalService.get();
        var _approvalGridOption = [];
        var isSave = false;
        $scope.ApproverLevel = approverGrids.ApproverLevel;
        $scope.count = 1;
        $scope.isReadonlyDetails = function () {
            if ($routeParams.mode.toUpperCase() !== "CREATE" && $routeParams.mode.toUpperCase() !== "REVISE") {
                //$($("#texteditor").data().kendoEditor.body).attr('contenteditable', false);
                return true;
            }
            return false;
        }

        $scope.forms = {};
        //Current date
        $scope.currentDate = new Date();
        $scope.show = false;
        $scope.chkDeposit = false;
        $scope.checkSubmit = false;
        $scope.expenseCheck = false;
        $scope.checkMemoType = false;
        $scope.dataBranch = new kendo.data.ObservableArray([]);
        $scope.approverLevelFiveAndSix = true;
        $scope.TotalBudgetPlan = 0;

        $scope.form = {
            ListPlace: [],
            DocTypeFiles: [],
            OtherFiles: [],
            MemoLines: new kendo.data.ObservableArray([]),
            MemoApproval: new kendo.data.ObservableArray([]),
            Requester: [],
            InformEmail: [],
            CCEmail: []
        };

        //Set Due Date
        //Current date
        $scope.currentDate = new Date();
        $scope.currentDatePlusTwo = new Date($scope.currentDate);
        $scope.currentDatePlusTwo.setDate($scope.currentDatePlusTwo.getDate() + 2);
        //Set date Propose
        if ($scope.currentDatePlusTwo.getDay() === 6) {
            $scope.currentDatePlusTwo.setDate($scope.currentDatePlusTwo.getDate() + 2);
        }
        if ($scope.currentDatePlusTwo.getDay() === 0) {
            $scope.currentDatePlusTwo.setDate($scope.currentDatePlusTwo.getDate() + 1);
        }
        $scope.form.DueDatePropose = new Date($scope.currentDatePlusTwo);
        //Set date Accept
        var dueDatePropose = new Date($scope.form.DueDatePropose);
        dueDatePropose.setDate(dueDatePropose.getDate() + 2);
        if (dueDatePropose.getDay() === 6) {
            dueDatePropose.setDate(dueDatePropose.getDate() + 2);
        }
        if (dueDatePropose.getDay() === 0) {
            dueDatePropose.setDate(dueDatePropose.getDate() + 1);
        }
        $scope.form.DueDateAccept = new Date(dueDatePropose);
        //Set date Approve
        var dueDateAccept = new Date($scope.form.DueDateAccept);
        dueDateAccept.setDate(dueDateAccept.getDate() + 2);
        if (dueDateAccept.getDay() === 6) {
            dueDateAccept.setDate(dueDateAccept.getDate() + 2);
        }
        if (dueDateAccept.getDay() === 0) {
            dueDateAccept.setDate(dueDateAccept.getDate() + 1);
        }
        $scope.form.DueDateApprove = new Date(dueDateAccept);
        //Set date Final Approve
        var dueDateFinalApprove = new Date($scope.form.DueDateApprove);
        dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 2);
        if (dueDateFinalApprove.getDay() === 6) {
            dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 2);
        }
        if (dueDateFinalApprove.getDay() === 0) {
            dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 1);
        }
        $scope.form.DueDateFinalApprove = new Date(dueDateFinalApprove);

        //SetDate function 
        //Propose
        var setDateFromPropose = function () {
            //Set date Propose
            var dueDatePropose = new Date($scope.form.DueDatePropose);
            dueDatePropose.setDate(dueDatePropose.getDate() + 2);
            if (dueDatePropose.getDay() === 6) {
                dueDatePropose.setDate(dueDatePropose.getDate() + 2);
            }
            if (dueDatePropose.getDay() === 0) {
                dueDatePropose.setDate(dueDatePropose.getDate() + 1);
            }
            $scope.form.DueDateAccept = new Date(dueDatePropose);
            //Set date Accept
            var dueDateAccept = new Date($scope.form.DueDateAccept);
            dueDateAccept.setDate(dueDateAccept.getDate() + 2);
            if (dueDateAccept.getDay() === 6) {
                dueDateAccept.setDate(dueDateAccept.getDate() + 2);
            }
            if (dueDateAccept.getDay() === 0) {
                dueDateAccept.setDate(dueDateAccept.getDate() + 1);
            }
            $scope.form.DueDateApprove = new Date(dueDateAccept);
            //Set date Final Approve
            var dueDateFinalApprove = new Date($scope.form.DueDateApprove);
            dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 2);
            if (dueDateFinalApprove.getDay() === 6) {
                dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 2);
            }
            if (dueDateFinalApprove.getDay() === 0) {
                dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 1);
            }
            $scope.form.DueDateFinalApprove = new Date(dueDateFinalApprove);
        }

        $scope.changeDueDatePropose = function () {
            //Set date Accept
            var datePropose = new Date($scope.form.DueDatePropose);
            var dateAccept = new Date($scope.form.DueDateAccept);
            if (datePropose.getFullYear() > dateAccept.getFullYear()) {
                setDateFromPropose();
            } else if (datePropose.getFullYear() === dateAccept.getFullYear()
            && datePropose.getMonth() > dateAccept.getMonth()) {
                setDateFromPropose();
            } else if (datePropose.getFullYear() === dateAccept.getFullYear()
            && datePropose.getMonth() === dateAccept.getMonth()
            && datePropose.getDate() > dateAccept.getDate()) {
                setDateFromPropose();
            }
        }

        //Accept
        var setDateFromAccept = function () {
            //Set date Accept
            var dueDateAccept = new Date($scope.form.DueDateAccept);
            dueDateAccept.setDate(dueDateAccept.getDate() + 2);
            if (dueDateAccept.getDay() === 6) {
                dueDateAccept.setDate(dueDateAccept.getDate() + 2);
            }
            if (dueDateAccept.getDay() === 0) {
                dueDateAccept.setDate(dueDateAccept.getDate() + 1);
            }
            $scope.form.DueDateApprove = new Date(dueDateAccept);
            //Set date Final Approve
            var dueDateFinalApprove = new Date($scope.form.DueDateApprove);
            dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 2);
            if (dueDateFinalApprove.getDay() === 6) {
                dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 2);
            }
            if (dueDateFinalApprove.getDay() === 0) {
                dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 1);
            }
            $scope.form.DueDateFinalApprove = new Date(dueDateFinalApprove);
        }

        $scope.changeDueDateAccept = function () {
            var dateAccept = new Date($scope.form.DueDateAccept);
            var dateApprove = new Date($scope.form.DueDateApprove);
            if (dateAccept.getFullYear() > dateApprove.getFullYear()) {
                setDateFromAccept();
            } else if (dateAccept.getFullYear() === dateApprove.getFullYear()
                && dateAccept.getMonth() > dateApprove.getMonth()) {
                setDateFromAccept();
            }
            if (dateAccept.getFullYear() === dateApprove.getFullYear()
                && dateAccept.getMonth() === dateApprove.getMonth()
                && dateAccept.getDate() > dateApprove.getDate()) {
                setDateFromAccept();
            }
        }

        //Approve
        var setDateFromApprove = function () {
            //Set date Final Approve
            var dueDateFinalApprove = new Date($scope.form.DueDateApprove);
            dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 2);
            if (dueDateFinalApprove.getDay() === 6) {
                dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 2);
            }
            if (dueDateFinalApprove.getDay() === 0) {
                dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 1);
            }
            $scope.form.DueDateFinalApprove = new Date(dueDateFinalApprove);
        }

        $scope.changeDueDateApprove = function () {
            var dateApprove = new Date($scope.form.DueDateApprove);
            var dateFinalApprove = new Date($scope.form.DueDateFinalApprove);
            if (dateApprove.getFullYear() > dateFinalApprove.getFullYear()) {
                setDateFromApprove();
            } else if (dateApprove.getFullYear() === dateFinalApprove.getFullYear() &&
                dateApprove.getMonth() > dateFinalApprove.getMonth()) {
                setDateFromApprove();
            } else if (dateApprove.getFullYear() === dateFinalApprove.getFullYear() &&
                dateApprove.getMonth() === dateFinalApprove.getMonth() &&
                dateApprove.getDate() > dateFinalApprove.getDate()) {
                setDateFromApprove();
            }
        }

        $scope.temp = { listPlace: [] }
        $scope.uploadHandler = uploadHandler.init();
        $scope.attchmentSaveUrl = angular.crs.url.webApi("memo/:id/attachment");
        var setmemoLinesGridOptionGrid = function (d) {
            //$scope.form.MemoLines = new kendo.data.ObservableArray(d);
            if (d) {
                $scope.form.MemoLines = d;
            } else {
                $scope.form.MemoLines = new kendo.data.ObservableArray([]);
            }
           // $scope.form.MemoLines = d;
            if (_memoLinesGridOption) {
                _memoLinesGridOption.dataSource.data($scope.form.MemoLines);
                $scope.form.MemoLines = _memoLinesGridOption.dataSource.data();
            }
        }
        //var setApprovalGridDataSource = function (key, lv, d) {
        //   // $scope.form.MemoApproval = new kendo.data.ObservableArray(d);
        //    if (_approvalGridOption[key]) {
        //        _approvalGridOption[key].dataSource.data($scope.form.MemoApproval);
        //        //_approvalGridOption[key].dataSource.schema = approval.SchemaApprove(lv);
        //    } 
        //}

        //////Approver
        //check read only
        //Check that user push "Get final approve" or not.
        var checkFinalApproverButton = false;
        var readonly;
        $scope.disabled = false;
        if ($routeParams.mode.toUpperCase() !== "CREATE" && $routeParams.mode.toUpperCase() !== "REVISE") {
            $scope.disabled = true;
            readonly = true;
            $scope.approverLevelFiveAndSix = true;
        } else {
            $scope.disabled = false;
            readonly = false;
            $scope.approverLevelFiveAndSix = false;
        }
        //Check add data
        var approverGridOptionLevelFourDataCheck = false;

        //Get Final Approver
        $scope.getFinalApproverData = function () {
            if ($routeParams.mode.toUpperCase() !== "CREATE" && $routeParams.mode.toUpperCase() !== "REVISE") {
                return false;
            }
            //debugger;
            //if (!_memoLinesGridOption ||
            //    !_memoLinesGridOption.dataSource ||
            //    !_memoLinesGridOption.dataSource._aggregateResult ||
            //    !_memoLinesGridOption.dataSource._aggregateResult.NetAmount ||
            //    !_memoLinesGridOption.dataSource._aggregateResult.NetAmount.sum) {
            //    $scope.form.Budget = 0;
            //} else {
            //    var total = _memoLinesGridOption.dataSource._aggregateResult.NetAmount.sum;
            //    //$scope.form.Budget = total;
            //}
            var id = 0;
            if ($routeParams.id !== undefined) {
                id = $routeParams.id;
            } else if (sn) {
                id = $scope.form.Id;
            }

            if ($scope.form.UnitCode && $scope.form.MemoType && $scope.form.BudgetDetail > 0) {
                checkFinalApproverButton = true;
                dataSource.finalApprover({ budget: $scope.form.BudgetDetail, id: id, unitcode: $scope.form.UnitCode.UnitCode, type: $scope.form.MemoType.TypeMemoCode }, function (data) {
                    if (data && data.Employee) {                 
                        approverGridOptionLevelFourDataCheck = true;
                        var approverFinal = {
                            Employee: data.Employee,
                            ApproverSequence: data.ApproverSequence,
                            ApproverLevel: data.ApproverLevel,
                            ApproverUserName: data.ApproverUserName,
                            ApproverEmail: data.ApproverEmail,
                            Position: data.Position,
                            Department: data.Department,
                            DueDate: data.DueDate,
                            LineNo: 1,
                            LockEditable: 1
                        };
                        //console.log(approverFinal.Deleted);
                        // $scope.ApproverGridOptionLevelFourData.push(approverFinal);
                        $scope.ApproverGridOptionLevelFourData = [];
                        $scope.ApproverGridOptionLevelFourData.push(approverFinal);
                        //$scope.ApproverGridOptionLevelFour.dataSource.data($scope.ApproverGridOptionLevelFourData);
                        //$scope.ApproverGridOptionLevelFourData = $scope.ApproverGridOptionLevelFour.dataSource.data();
                    } else {
                        messageBox.error("ไม่พบผู้อนุมัติคนสุดท้าย กรุณาติดต่อผู้ดูและระบบ");
                        $scope.ApproverGridOptionLevelFourData = new kendo.data.ObservableArray([]);
                        //$scope.ApproverGridOptionLevelFour.dataSource.data($scope.ApproverGridOptionLevelFourData);
                        //$scope.ApproverGridOptionLevelFourData = $scope.ApproverGridOptionLevelFour.dataSource.data();
                    }
                    $scope.ApproverGridOptionLevelFour.dataSource.data($scope.ApproverGridOptionLevelFourData);
                    $scope.ApproverGridOptionLevelFourData = $scope.ApproverGridOptionLevelFour.dataSource.data();
                });
            } else {
                if (!$scope.form.UnitCode)
                    messageBox.error("Please select unit code!!!");
                if (!$scope.form.MemoType)
                    messageBox.error("Please select memo type!!!");
                if ($scope.form.BudgetDetail <= 0)
                    messageBox.error("Budget must not be zero!!!");
            }
        }

        //Merge All Approver to One Array before save
        var mergeArrayData = function (data, array) {
            angular.forEach(array, function (value, key) {
                data.push(value);
            });
            return data;
        }

        //Set approver grid data source on load data process
        var setApprovalGridDataSource = function (key, lv, data) {
            var approverData = data.filter(function (result) {
                return result.ApproverLevel === lv;
            });
            if (lv === 'เสนอ') {
                if (approverData.length === 0) {
                    $scope.ApproverGridOptionLevelOneData = new kendo.data.ObservableArray([]);
                } else {
                    $scope.ApproverGridOptionLevelOneData = [];
                    angular.forEach(approverData, function (value, key) {
                        $scope.ApproverGridOptionLevelOneData.push(value);
                    });
                }
                if (!isSave) {
                    var gridLevelOne = approverGrids.ApproverGridOption(readonly, $scope.ApproverGridOptionLevelOneData, lv);
                    $scope.ApproverGridOptionLevelOne = gridLevelOne;
                }
                $scope.ApproverGridOptionLevelOne.dataSource.data($scope.ApproverGridOptionLevelOneData);
                $scope.ApproverGridOptionLevelOneData = $scope.ApproverGridOptionLevelOne.dataSource.data();
            } else if (lv === 'เห็นชอบ') {
                if (approverData.length === 0) {
                    $scope.ApproverGridOptionLevelTwoData = new kendo.data.ObservableArray([]);
                } else {
                    $scope.ApproverGridOptionLevelTwoData = [];
                    angular.forEach(approverData, function (value, key) {
                        $scope.ApproverGridOptionLevelTwoData.push(value);
                    });
                }
                if (!isSave) {
                    var gridLevelTwo = approverGrids.ApproverGridOption(readonly, $scope.ApproverGridOptionLevelTwoData, lv);
                    $scope.ApproverGridOptionLevelTwo = gridLevelTwo;
                }
                $scope.ApproverGridOptionLevelTwo.dataSource.data($scope.ApproverGridOptionLevelTwoData);
                $scope.ApproverGridOptionLevelTwoData = $scope.ApproverGridOptionLevelTwo.dataSource.data();
            } else if (lv === 'อนุมัติ') {
                if (approverData.length === 0) {
                    $scope.ApproverGridOptionLevelThreeData = new kendo.data.ObservableArray([]);
                } else {
                    $scope.ApproverGridOptionLevelThreeData = [];
                    angular.forEach(approverData, function (value, key) {
                        $scope.ApproverGridOptionLevelThreeData.push(value);
                    });
                }
                if (!isSave) {
                    var gridLevelThree = approverGrids.ApproverGridOption(readonly, $scope.ApproverGridOptionLevelThreeData, lv);
                    $scope.ApproverGridOptionLevelThree = gridLevelThree;
                }
                $scope.ApproverGridOptionLevelThree.dataSource.data($scope.ApproverGridOptionLevelThreeData);
                $scope.ApproverGridOptionLevelThreeData = $scope.ApproverGridOptionLevelThree.dataSource.data();
            } else if (lv === 'อนุมัติขั้นสุดท้าย') {
                approverGridOptionLevelFourDataCheck = true;
                if (approverData.length === 0) {
                    checkFinalApproverButton = false;
                    $scope.ApproverGridOptionLevelFourData = new kendo.data.ObservableArray([]);
                } else {
                    checkFinalApproverButton = true;
                    $scope.ApproverGridOptionLevelFourData = [];
                    angular.forEach(approverData, function (value, key) {
                        $scope.ApproverGridOptionLevelFourData.push(value);
                    });
                }
                if (!isSave) {
                    var gridLevelFour = approverGrids.ApproverGridOptionLevelFour(readonly, $scope.ApproverGridOptionLevelFourData);
                    $scope.ApproverGridOptionLevelFour = gridLevelFour;
                }
                $scope.ApproverGridOptionLevelFour.dataSource.data($scope.ApproverGridOptionLevelFourData);
                $scope.ApproverGridOptionLevelFourData = $scope.ApproverGridOptionLevelFour.dataSource.data();
            }
        }

        //Set Data Before Save
        $scope.setDataApproverBeforeSave = function() {
            $scope.form.MemoApproval = new kendo.data.ObservableArray([]);
            $scope.form.MemoApproval = mergeArrayData($scope.form.MemoApproval, $scope.ApproverGridOptionLevelOneData);
            $scope.form.MemoApproval = mergeArrayData($scope.form.MemoApproval, $scope.ApproverGridOptionLevelTwoData);
            $scope.form.MemoApproval = mergeArrayData($scope.form.MemoApproval, $scope.ApproverGridOptionLevelThreeData);
            $scope.form.MemoApproval = mergeArrayData($scope.form.MemoApproval, $scope.ApproverGridOptionLevelFourData);
        };

        if (proposalLineId !== undefined && proposalLineId !== 0) {
            remoteResult = dataSource.getdatabindmemo({ propLineid: proposalLineId }, function (data) {
                $scope.form = remoteResult;
                $scope.form.Requester = angular.copy(authService.authentication.me);
                $scope.form.OtherFiles = [];
                setmemoLinesGridOptionGrid(remoteResult.MemoLines);
                //Set Due Date
                //Current date
                $scope.currentDate = new Date();
                $scope.currentDatePlusTwo = new Date($scope.currentDate);
                $scope.currentDatePlusTwo.setDate($scope.currentDatePlusTwo.getDate() + 2);
                //Set date Propose
                if ($scope.currentDatePlusTwo.getDay() === 6) {
                    $scope.currentDatePlusTwo.setDate($scope.currentDatePlusTwo.getDate() + 2);
                }
                if ($scope.currentDatePlusTwo.getDay() === 0) {
                    $scope.currentDatePlusTwo.setDate($scope.currentDatePlusTwo.getDate() + 1);
                }
                $scope.form.DueDatePropose = new Date($scope.currentDatePlusTwo);
                //Set date Accept
                dueDatePropose = new Date($scope.form.DueDatePropose);
                dueDatePropose.setDate(dueDatePropose.getDate() + 2);
                if (dueDatePropose.getDay() === 6) {
                    dueDatePropose.setDate(dueDatePropose.getDate() + 2);
                }
                if (dueDatePropose.getDay() === 0) {
                    dueDatePropose.setDate(dueDatePropose.getDate() + 1);
                }
                $scope.form.DueDateAccept = new Date(dueDatePropose);
                //Set date Approve
                dueDateAccept = new Date($scope.form.DueDateAccept);
                dueDateAccept.setDate(dueDateAccept.getDate() + 2);
                if (dueDateAccept.getDay() === 6) {
                    dueDateAccept.setDate(dueDateAccept.getDate() + 2);
                }
                if (dueDateAccept.getDay() === 0) {
                    dueDateAccept.setDate(dueDateAccept.getDate() + 1);
                }
                $scope.form.DueDateApprove = new Date(dueDateAccept);
                //Set date Final Approve
                dueDateFinalApprove = new Date($scope.form.DueDateApprove);
                dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 2);
                if (dueDateFinalApprove.getDay() === 6) {
                    dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 2);
                }
                if (dueDateFinalApprove.getDay() === 0) {
                    dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 1);
                }
                $scope.form.DueDateFinalApprove = new Date(dueDateFinalApprove);
            });
        }
        
        $scope.checkDeposit = function () {
            if ($scope.form.ProposalRef !== undefined) {
                if ($scope.form.ProposalRef.ProposalType) {
                    if ($scope.form.ProposalRef.ProposalType.TypeProposalName === "Deposit" || $scope.form.ProposalRef.ProposalType.TypeProposalName === "Deposit Internal") {
                        $scope.show = true;
                        if ($scope.form.ProposalRef.ProposalType.TypeProposalName === "Deposit") {
                            $scope.chkDeposit = true;
                        } else {
                            $scope.chkDeposit = false;
                        }
                    } else {
                        $scope.show = false;
                    }
                } else {
                    $scope.show = false;
                }
            }
        }

        $scope.clearDocNumber = function () {
            $scope.proposalNumberOptioncope.clear();
        }

        if ($routeParams.id != undefined) {
            remoteResult = dataSource.get({ id: $routeParams.id }, function (data) {
                $scope.form = data;
                setmemoLinesGridOptionGrid($scope.form.MemoLines);
                //Pay to unit code Drop down
                $scope.tempDataSource = [$scope.form.MemoType.UnitCode];
                $scope.payToUnitCodeOption.dataSource.data($scope.tempDataSource);
                if ($scope.form.MemoType.TypeMemoName === "Normal") {
                    $scope.form.PayToUnitCode = null;
                } else {
                    $scope.form.PayToUnitCode = $scope.form.MemoType.UnitCode;
                }
                for (var j = 0; j < $scope.ApproverLevel.length; j++) {
                    setApprovalGridDataSource(j, $scope.ApproverLevel[j], $scope.form.MemoApproval);
                }
                if (!$scope.form.CCEmail) {
                    $scope.form.CCEmail = [];
                }
                if (!$scope.form.InformEmail) {
                    $scope.form.InformEmail = [];
                }
                 $scope.checkDeposit();
                 $scope.uploadHandler.setId(remoteResult.Id);

                //Set date Propose
                 $scope.form.DueDatePropose = new Date($scope.form.DueDatePropose);
                 $scope.form.DueDatePropose.setHours(7);
                //Set date Accept
                 $scope.form.DueDateAccept = new Date($scope.form.DueDateAccept);
                 $scope.form.DueDateAccept.setHours(7);
                //Set date Approve
                 $scope.form.DueDateApprove = new Date($scope.form.DueDateApprove);
                 $scope.form.DueDateApprove.setHours(7);
                //Set date Final Approve
                 $scope.form.DueDateFinalApprove = new Date($scope.form.DueDateFinalApprove);
                 $scope.form.DueDateFinalApprove.setHours(7);
                //Check Print make uppercase
                 $scope.RequesterCheck = $scope.form.Requester.Username.toUpperCase();
            }, k2.onSnError);
        } else if (sn) {
            remoteResult = dataSource.worklist({ sn: sn }, function() {
                $scope.form = remoteResult;
                setmemoLinesGridOptionGrid(remoteResult.MemoLines);

                //Pay to unit code Drop down
                $scope.tempDataSource = [$scope.form.MemoType.UnitCode];
                $scope.payToUnitCodeOption.dataSource.data($scope.tempDataSource);
                if ($scope.form.MemoType.TypeMemoName === "Normal") {
                    $scope.form.PayToUnitCode = null;
                } else {
                    $scope.form.PayToUnitCode = $scope.form.MemoType.UnitCode;
                }           
                for (var j = 0; j < $scope.ApproverLevel.length; j++) {
                    setApprovalGridDataSource(j, $scope.ApproverLevel[j], $scope.form.MemoApproval);
                }
                if (!$scope.form.CCEmail) {
                    $scope.form.CCEmail = [];
                }
                if (!$scope.form.InformEmail) {
                    $scope.form.InformEmail = [];
                }
                $scope.uploadHandler.setId(remoteResult.Id).setSN(sn);
                //Set date Propose
                $scope.form.DueDatePropose = new Date($scope.form.DueDatePropose);
                $scope.form.DueDatePropose.setHours(7);
                //Set date Accept
                $scope.form.DueDateAccept = new Date($scope.form.DueDateAccept);
                $scope.form.DueDateAccept.setHours(7);
                //Set date Approve
                $scope.form.DueDateApprove = new Date($scope.form.DueDateApprove);
                $scope.form.DueDateApprove.setHours(7);
                //Set date Final Approve
                $scope.form.DueDateFinalApprove = new Date($scope.form.DueDateFinalApprove);
                $scope.form.DueDateFinalApprove.setHours(7);
            }, k2.onSnError);
        } else {
            $scope.form.Requester = angular.copy(authService.authentication.me);
            //New Approve
            //Approver Level One(เสนอ) //ทำเป็น Grid ใน ไฟล์เดียวอยู่นะ
            $scope.ApproverGridOptionLevelOneData = new kendo.data.ObservableArray([]);;
            var gridLevelOne = approverGrids.ApproverGridOption(readonly, $scope.ApproverGridOptionLevelOneData, $scope.ApproverLevel[0]);
            $scope.ApproverGridOptionLevelOne = gridLevelOne;
            $scope.ApproverGridOptionLevelOneData = $scope.ApproverGridOptionLevelOne.dataSource.data();
            //Approver Level Two(เห็นชอบ)
            $scope.ApproverGridOptionLevelTwoData = new kendo.data.ObservableArray([]);
            var gridLevelTwo = approverGrids.ApproverGridOption(readonly, $scope.ApproverGridOptionLevelTwoData, $scope.ApproverLevel[1]);
            $scope.ApproverGridOptionLevelTwo = gridLevelTwo;
            $scope.ApproverGridOptionLevelTwoData = $scope.ApproverGridOptionLevelTwo.dataSource.data();
            //Approver Level Three(อนุมัติ)
            $scope.ApproverGridOptionLevelThreeData = new kendo.data.ObservableArray([]);
            var gridLevelThree = approverGrids.ApproverGridOption(readonly, $scope.ApproverGridOptionLevelThreeData, $scope.ApproverLevel[2]);
            $scope.ApproverGridOptionLevelThree = gridLevelThree;
            $scope.ApproverGridOptionLevelThreeData = $scope.ApproverGridOptionLevelThree.dataSource.data();
            //Approver Level Four(อนุมัติขั้นสุดท้าย)
            $scope.ApproverGridOptionLevelFourData = new kendo.data.ObservableArray([]);
            var gridLevelFour = approverGrids.ApproverGridOptionLevelFour(readonly, $scope.ApproverGridOptionLevelFourData);
            $scope.ApproverGridOptionLevelFour = gridLevelFour;
            $scope.ApproverGridOptionLevelFourData = $scope.ApproverGridOptionLevelFour.dataSource.data();
            //Set Due Date
            //Current date
            $scope.currentDate = new Date();
            $scope.currentDatePlusTwo = new Date($scope.currentDate);
            $scope.currentDatePlusTwo.setDate($scope.currentDatePlusTwo.getDate() + 2);
            //Set date Propose
            if ($scope.currentDatePlusTwo.getDay() === 6) {
                $scope.currentDatePlusTwo.setDate($scope.currentDatePlusTwo.getDate() + 2);
            }
            if ($scope.currentDatePlusTwo.getDay() === 0) {
                $scope.currentDatePlusTwo.setDate($scope.currentDatePlusTwo.getDate() + 1);
            }
            $scope.form.DueDatePropose = new Date($scope.currentDatePlusTwo);
            //Set date Accept
            dueDatePropose = new Date($scope.form.DueDatePropose);
            dueDatePropose.setDate(dueDatePropose.getDate() + 2);
            if (dueDatePropose.getDay() === 6) {
                dueDatePropose.setDate(dueDatePropose.getDate() + 2);
            }
            if (dueDatePropose.getDay() === 0) {
                dueDatePropose.setDate(dueDatePropose.getDate() + 1);
            }
            $scope.form.DueDateAccept = new Date(dueDatePropose);
            //Set date Approve
            dueDateAccept = new Date($scope.form.DueDateAccept);
            dueDateAccept.setDate(dueDateAccept.getDate() + 2);
            if (dueDateAccept.getDay() === 6) {
                dueDateAccept.setDate(dueDateAccept.getDate() + 2);
            }
            if (dueDateAccept.getDay() === 0) {
                dueDateAccept.setDate(dueDateAccept.getDate() + 1);
            }
            $scope.form.DueDateApprove = new Date(dueDateAccept);
            //Set date Final Approve
            dueDateFinalApprove = new Date($scope.form.DueDateApprove);
            dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 2);
            if (dueDateFinalApprove.getDay() === 6) {
                dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 2);
            }
            if (dueDateFinalApprove.getDay() === 0) {
                dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 1);
            }
            $scope.form.DueDateFinalApprove = new Date(dueDateFinalApprove);
        }

        $scope.requesterOption = controlOptions.popupSearch('employee');
        //$scope.employeeOption = controlOptions.popupSearch('employee');
        $scope.employeeOption = approverGrids.employeeOption;
            //$scope.accountingOption = controlOptions.popupSearch('accounting', { multiple: true });
            //Branch Accounting Dropdown
            $scope.branchAccountingOptions = {
                dataTextField: "PlaceName",
                dataValueField: "PlaceCode",
                autoBind: false,
                optionLabel: "please select",
                template: '#=data.PlaceCode# - #=data.PlaceName#',
                valueTemplate: '#=data.PlaceCode# - #=data.PlaceName#',
                filter: "contains",
                //enable: false,
                dataSource: {
                    type: 'json',
                    //serverFiltering: true,
                    transport: {
                        read: {
                            url: function () {
                                return angular.crs.url.webApi("accounting/getaccountingbranch");
                            }
                        }
                    }
                }
            }

            $scope.proposalNumberOption = controlOptions.popupSearch('proposalnumberref');
            $scope.proposalNumberOption2 = $scope.proposalNumberOption();
            $scope.proposalNumberOption2.show = function () {
                this.fields("unitCode").set("value", $scope.form.UnitCode.UnitCode);
            }

            $scope.employeePopupSearchOption = controlOptions.popupSearch('employee', { multiple: true });
            $scope.useProposalNumberOption = controlOptions.popupSearch('proposalnumberref');
            $scope.depositNumberOption = controlOptions.popupSearch('depositnumber');
            $scope.proposalTypeOption = controlOptions.dropdown('typeproposal');
            $scope.memoTypeOption = controlOptions.dropdown('typememo');
            //$scope.unitcodeOption = controlOptions.dropdown('unitcode');
            $scope.unitcodeOptionNew = { //function () {
                dataSource: {
                    type: "json",
                    data: angular.copy(authService.authentication.me).UnitCode
                },
                optionLabel: "please select",
                dataTextField: 'UnitName',
                dataValueField: 'UnitCode',
                template: '#=data.UnitName#',
                valueTemplate: '#=data.UnitName#',
                filter: "contains",
                filtering: function (ev) {
                    if (ev.filter) {
                        var filterValue = ev.filter.value;
                        ev.preventDefault();

                        this.dataSource.filter({
                            logic: "or",
                            filters: [
                              {
                                  field: "UnitName",
                                  operator: "contains",
                                  value: filterValue
                              },
                              {
                                  field: "UnitCode",
                                  operator: "contains",
                                  value: filterValue
                              }
                            ]
                        });
                    } else {
                        ev.preventDefault();
                    }
                }
            }
            if (angular.copy(authService.authentication.me).UnitCode.length === 1) {
                //console.log(angular.copy(authService.authentication.me).UnitCode[0]);
                $scope.form.UnitCode = angular.copy(authService.authentication.me).UnitCode[0];
                if (sn) {
                    $scope.checkedUnitCode = false;
                } else {
                    $scope.checkedUnitCode = true;
                }
            }

        //Pay to Unit Code dropdown
        //$scope.payToUnitCodeOption = controlOptions.dropdown('unitcode');
            $scope.payToUnitCodeOption = {
                dataSource: new kendo.data.DataSource({
                    type: "json",
                    data: new kendo.data.ObservableArray([])
                }),
                dataTextField: 'UnitName',
                dataValueField: 'UnitCode',
                template: '#=data.UnitCode# - #=data.UnitName#',
                valueTemplate: '#=data.UnitCode# - #=data.UnitName#',
                optionLabel: "please select",
                filter: "contain"
                //,
                //dataBound: function () {
                //    if (sn) {
                //        this.enable(false);
                //    }
                //}
            }

        //ExpenseTopci Dropdown
            $scope.expenseTopicOption = {
                dataSource: new kendo.data.DataSource({
                    type: "json",
                    data: new kendo.data.ObservableArray([])
                    
                }),
                    dataTextField : 'ExpenseTopicName',
                    dataValueField : 'ExpenseTopicCode',
                    template : '#=data.ExpenseTopicCode# - #=data.ExpenseTopicName#',
                    valueTemplate: '#=data.ExpenseTopicCode# - #=data.ExpenseTopicName#',
                    optionLabel: "please select"
            }
            $scope.expenseTopicOption.select = function (data) {
                // $scope.checkAP = false;
                var dataItem = this.dataItem(data.item);
                var apcodes = $filter('filter')($scope.form.ProposalRef.APCode, {
                    UnitCode: $scope.form.UnitCode.UnitCode,
                    ExpenseTopicCode: dataItem.ExpenseTopicCode
                });
                $scope.apCodeOption.dataSource.data(apcodes);
                $scope.form.BudgetAPCode = null;
                $scope.TotalBudgetPlan = null;
            }

            //APCode Dropdown
            $scope.apCodeOption = {
                dataSource: new kendo.data.DataSource({
                    type: "json",
                    data: new kendo.data.ObservableArray([])                
                }),
                dataTextField: 'APName',
                dataValueField: 'ProposalLineId',
                template: '#=data.APCode# - #=data.APName# #if(data.DescriptionLine){#- #=data.DescriptionLine # #}#',
                valueTemplate: '#=data.APCode# - #=data.APName# #if(data.DescriptionLine){#- #=data.DescriptionLine # #}#',
                optionLabel: "please select",
                dataBound: function () {
                    if (sn) {
                        this.enable(false);
                    }
                }
            }
            $scope.apCodeOption.select = function (data) {
             //   checkFinalApproverButton = false;
                //console.log($scope.form.ProposalRef);
                var dataItem = this.dataItem(data.item);
              
                //if (dataItem.APCode !== "") {
                //    var list = $scope.form.ProposalRef.ProposalLine.filter(function (data) {
                //        return data.ExpenseTopicCode === dataItem.ExpenseTopicCode
                //            && data.APCode.APCode === dataItem.APCode &&
                //            data.UnitCode.UnitCode === $scope.form.UnitCode.UnitCode;
                //    });
                $scope.checkAP = false;
                $scope.form.BudgetAPCode = dataItem.BudgetPlan;
                $scope.form.ProposalLineId = dataItem.ProposalLineId;
                $scope.TotalBudgetPlan = dataItem.TotalBudgetPlan;

            }

            //Text Editor
            var originwidth = 0;
            var originheight = 0;
            $scope.trustAsHtml = function (string) {
                return $sce.trustAsHtml(string);
            };
            $scope.DescriptionEditor = {
                paste: function (e) {
                    setTimeout(function () {
                        $("iframe").contents().find("div").attr("style", "max-width: 100%");
                        $("iframe").contents().find("img").attr("style", "max-width:100%;");
                    }, 500);
                },
                tools:  [
                    "bold",
                    "italic",
                    "underline",
                    "strikethrough",
                    "justifyLeft",
                    "justifyCenter",
                    "justifyRight",
                    "justifyFull",
                    "insertUnorderedList",
                    "insertOrderedList",
                    "fontName",
                    "fontSize",
                    "foreColor",
                    "backColor",
                    "createTable",
                    "print",
                    {
                        name: "maximize",
                        tooltip: "Maximize",
                        exec: function (e) {

                            if (originheight === 0 || originwidth === 0) {
                                originheight = $("td.k-editable-area iframe.k-content").height();
                                originwidth = $("td.k-editable-area iframe.k-content").width();
                            }
                            var editor = $(this).data("kendoEditor");
                            editor.wrapper.css({
                                "z-index": 9999,
                                width: $(window).width(),
                                height: $(window).height(),
                                position: "fixed",
                                left: 0,
                                top: 0
                                //left: $("aside.main-sidebar .ng-scope").width(),
                                //top: $("nav.navbar.navbar-static-top").height()
                            });
                            //editor.wrapper.css("height", "800px");
                        }
                    },
                    {
                        name: "restore",
                        tooltip: "Restore",
                        exec: function (e) {
                            var editor = $(this).data("kendoEditor");
                            editor.wrapper.css({
                                width: originwidth,
                                height: originheight,
                                position: "relative"
                            });
                        }
                    },
                ]
            }

        //dropdown
            $scope.placeOptions = {
                dataTextField: "PlaceName",
                dataValueField: "PlaceCode",
                autoBind: false,
                enable: false,
                dataSource: new kendo.data.DataSource({
                    type: "json",
                    data: new kendo.data.ObservableArray([])
                })
            };

        var memoLineSchema = {
            model: kendo.data.Model.define({
                id: "LineNo",
                fields: {
                    //LineNo: { type: "number-default" },
                    LineNo: { type: "number" },
                    Description: { type: "string", validation: { required: true } },
                    Unit: { type: "number", validation: { min: 0,required: true } },
                    Amount: { type: "number", validation: { min: 0, required: true } },
                    Vat: { type: "number", validation: { min: 0, required: true } },
                    Tax: { type: "number", validation: { min: 0, required: true } },
                    NetAmount: { type: "number", editable: false },
                    NetAmountNoVatTax: { type: "number", validation: { min: 0 } }
                }
            })
        };

        $scope.MemoLinesGridOption = function () {
            _memoLinesGridOption = lineGrid.gridOption({
                schema: memoLineSchema,
                data: $scope.form.MemoLines,
                aggregate: [
                    { field: "Amount", aggregate: "sum" },
                    { field: "NetAmount", aggregate: "sum" },
                    { field: "NetAmountNoVatTax", aggregate: "sum" }
                ],
                columns: [
                {
                    field: "Description",
                    title: "Description",
                    width: "180px",
                    template: function(data) {
                        if (data.Description !== undefined) {
                            return "<div style='width:100%; word-wrap: break-word;'>" + data.Description + "</div>";
                        }
                        return "";
                    },
                    footerTemplate: "Total"
                },
                {
                    field: "Unit",
                    title: "Unit",
                    width: "100px",
                    template: lineGrid.formatCurrency('Unit')
                },
                {
                    field: "Amount",
                    title: "Price/Unit",
                    width: "150px",
                    template: lineGrid.formatCurrency('Amount')
                    //,footerTemplate: "<div class='text-right'>Sum Amount {{sumAmount() | currency:' ':2}}</div>"
                },
                {
                    field: "Vat",
                    title: "VAT %",
                    width: "60px",
                    template: lineGrid.formatCurrency('Vat'),
                    editor: function(container, options) {
                        $scope.vatValue = approverGrids.vatOption;
                        var editor = $('<select kendo-drop-down-list name="Vat" k-options="vatValue" style="width: 100%"></select>');
                        editor.attr("data-bind", "value:Vat,source:null");
                        editor.attr("style", "width:100%; word-wrap: break-word;");
                        editor.appendTo(container);
                        return editor;
                    }
                },
                {
                    field: "Tax",
                    title: "TAX %",
                    width: "60px",
                    template: lineGrid.formatCurrency('Tax'),
                    editor: function(container, options) {
                        $scope.taxValue = approverGrids.taxOption;
                        var editor = $('<select kendo-drop-down-list name="Tax" k-options="taxValue" style="width: 100%"></select>');
                        editor.attr("data-bind", "value:Tax,source:null");
                        editor.attr("style", "width:100%; word-wrap: break-word;");
                        editor.appendTo(container);
                        return editor;
                    }
                },
                {
                    field: "NetAmount",
                    title: "Net amount",
                    width: "150px",
                    template: lineGrid.formatCurrency('NetAmount'),
                    footerTemplate: //"<div class='col-xs-6 col-sm-6 col-md-3 col-lg-3'>Total</div>" +
                        "<div class='text-right'>{{sumNetAmount() | currency:' ':2}}</div>"
                    //"<div class='text-right'>#:(sum)? kendo.toString(sum,'n2'):0.00#</div>"
                },
                {
                    field: "Remark",
                    title: "Remark",
                    width: "140px",
                    template: function(data) {
                        if (data.Remark !== undefined && data.Remark !== null) {
                            return "<div style='width:100%; word-wrap: break-word;'>" + data.Remark + "</div>";
                        }
                        return "";
                    }
                },
                {
                    field: "NetAmountNoVatTax",
                    title: "NetAmountNoVatTax",
                    width: "150px",
                    template: lineGrid.formatCurrency('NetAmountNoVatTax'),
                    footerTemplate: "<div class='text-right'>{{sumNetAmountNoVatTax() | currency:' ':2}}</div>",
                    hidden: true
                }

                ],
                editable: {
                    mode: "incell"
                },
                
                readonly: $scope.isReadonlyDetails(),
                height:"400px",
                noedit: true,
                //template: $('#_IncomeDeposit').html(),
                save: function (e) {
                    $scope.calNetAmount(e);
                    $scope.calNetAmountNoTaxVat(e);
                    checkFinalApproverButton = false;
                    this.saveChanges();
                    //$scope.setBudget(this.dataSource.data());
                },
                pageable: false,
                groupable: true
            });
            return _memoLinesGridOption;
        }

        //Cal net amount
        $scope.calNetAmount = function (e)
        {
            var unitValue = e.model.Unit;
            if (e.values.Unit || e.values.Unit === 0) {
                unitValue = e.values.Unit;
            }
            //
            var vatValue = e.model.Vat;
            if (e.values.Vat || e.values.Vat === 0) {
                vatValue = e.values.Vat;
            }
            //
            var taxValue = e.model.Tax;
            if (e.values.Tax || e.values.Tax === 0) {
                taxValue = e.values.Tax;
            }
            //
            var amountValue = e.model.Amount;
            if (e.values.Amount || e.values.Amount === 0) {
                amountValue = e.values.Amount;
            }
            var rawSumAmountValue = unitValue * amountValue;
            var vat;
            var amountWithVatAndTax;
            var tax;
            if (taxValue !== 0 && vatValue !== 0) {
                tax = ((taxValue * rawSumAmountValue) / 100).toFixed(2);
                vat = ((vatValue * rawSumAmountValue) / 100).toFixed(2);
                amountWithVatAndTax = vat - tax;
                e.model.NetAmount = rawSumAmountValue + amountWithVatAndTax;
            } else {
                if (taxValue === 0 && vatValue !== 0) {
                    vat = parseFloat(((vatValue * rawSumAmountValue) / 100).toFixed(2));
                    e.model.NetAmount = rawSumAmountValue + parseFloat(vat);
                } else {
                    if (taxValue !== 0 && vatValue === 0) {
                        tax = parseFloat(((taxValue * rawSumAmountValue) / 100).toFixed(2));
                        var sum = rawSumAmountValue - tax;
                        e.model.NetAmount = sum;
                    } else {
                        if (taxValue === 0 && vatValue === 0) {
                            e.model.NetAmount = rawSumAmountValue; //
                        }
                    }
                }
            }
            e.model.NetAmount = parseFloat(Math.round(e.model.NetAmount * 100) / 100).toFixed(2); //
            return e.model;
        }
        //Cal net amount no vat tax

        //Set Budget
        $scope.calNetAmountNoTaxVat = function (e) {
            var unitValue = e.model.Unit;
            if (e.values.Unit || e.values.Unit === 0) {
                unitValue = e.values.Unit;
            }
            //
            var amountValue = e.model.Amount;
            if (e.values.Amount || e.values.Amount === 0) {
                amountValue = e.values.Amount;
            }
            //
            var rawSumAmountValue = unitValue * amountValue;
            e.model.NetAmountNoVatTax = rawSumAmountValue;
            return e.model;
        }

        //check net budget
        $scope.sumNetAmount = function () {
            if (!_memoLinesGridOption ||
                !_memoLinesGridOption.dataSource ||
                !_memoLinesGridOption.dataSource._aggregateResult ||
                !_memoLinesGridOption.dataSource._aggregateResult.NetAmount ||
                !_memoLinesGridOption.dataSource._aggregateResult.NetAmount.sum) {
                // $scope.form.BudgetDetail = 0;
                return 0; //$scope.form.BudgetDetail;
            }
            var total = _memoLinesGridOption.dataSource._aggregateResult.NetAmount.sum;
            //  $scope.form.BudgetDetail = total;
            return total;
        }

        //NetAmountNoVatTax
        $scope.sumNetAmountNoVatTax = function () {
            if (!_memoLinesGridOption ||
                !_memoLinesGridOption.dataSource ||
                !_memoLinesGridOption.dataSource._aggregateResult ||
                !_memoLinesGridOption.dataSource._aggregateResult.NetAmountNoVatTax ||
                !_memoLinesGridOption.dataSource._aggregateResult.NetAmountNoVatTax.sum) {
                $scope.form.BudgetDetail = 0;
                return 0; //$scope.form.BudgetDetail;
            }
            var total = _memoLinesGridOption.dataSource._aggregateResult.NetAmountNoVatTax.sum;
            $scope.form.BudgetDetail = total.toFixed(2);
            return total;
        }

        
        //Save
        $scope.validate = function(event, preventSubmit) {
            //Approver
            $scope.setDataApproverBeforeSave();

            // if ($scope.form.BudgetDetail <= $scope.form.BudgetAPCode) {
            if (preventSubmit !== false) {
                checkFinalApproverButton = true;
            }
            if (checkFinalApproverButton) {
                if ($scope.form.MemoType.ActualCharge) {
                    if ($scope.form.IncomeDepositViewModel === null ||
                        $scope.form.IncomeDepositViewModel === "" ||
                        $scope.form.IncomeDepositViewModel.DocNumber === "please select") {
                        messageBox.error("Please select an actual charge!!!");
                    } else {
                        k2.saveAndSubmit({
                            event: event,
                            preventSubmit: preventSubmit,
                            validator: $scope.validator,
                            save: dataSource.save,
                            data: $scope.form,
                            saveSuccess: function(response) {
                                if ($routeParams.mode.toUpperCase() === "REVISE") {
                                    location.reload();
                                } else {
                                    $scope.form = response;
                                    proposalLineId = undefined;
                                    //Pay to unit code Drop down
                                    $scope.tempDataSource = [$scope.form.MemoType.UnitCode];
                                    $scope.payToUnitCodeOption.dataSource.data($scope.tempDataSource);

                                    if ($scope.form.MemoType.TypeMemoName === "Normal") {
                                        $scope.form.PayToUnitCode = null;
                                    } else {
                                        $scope.form.PayToUnitCode = $scope.form.MemoType.UnitCode;
                                    }

                                    //$scope.tempDataSource = [$scope.form.MemoType.UnitCode];
                                    //$scope.payToUnitCodeOption.dataSource.data($scope.tempDataSource);
                                    //$scope.form.PayToUnitCode = $scope.form.MemoType.UnitCode;

                                    isSave = true;
                                    $location.path("memo/create/" + response.Id);
                                    setmemoLinesGridOptionGrid(response.MemoLines);
                                    for (var j = 0; j < $scope.ApproverLevel.length; j++) {
                                        setApprovalGridDataSource(j, $scope.ApproverLevel[j], $scope.form.MemoApproval);
                                    }

                                    if (!$scope.form.CCEmail) {
                                        $scope.form.CCEmail = [];
                                    }

                                    if (!$scope.form.InformEmail) {
                                        $scope.form.InformEmail = [];
                                    }

                                    $("#ccEmail").data("kendoMultiSelect").dataSource.data($scope.form.CCEmail);
                                    $("#informEmail").data("kendoMultiSelect").dataSource.data($scope.form.InformEmail);
                                }
                            },
                            submitSuccess: function(response) {
                                if (preventSubmit === false) {
                                }
                            },
                            uploadHandler: $scope.uploadHandler
                            //  uploadSuccessIsCallback: true,
                            // uploadSuccess: function (response, callback) {
                            //  },
                        });
                    }
                } else {
                    k2.saveAndSubmit({
                        event: event,
                        preventSubmit: preventSubmit,
                        validator: $scope.validator,
                        save: dataSource.save,
                        data: $scope.form,
                        saveSuccess: function(response) {
                            if ($routeParams.mode.toUpperCase() === "REVISE") {
                                location.reload();
                            } else {
                                $scope.form = response;
                                proposalLineId = undefined;
                                //Pay to unit code Drop down
                                $scope.tempDataSource = [$scope.form.MemoType.UnitCode];
                                $scope.payToUnitCodeOption.dataSource.data($scope.tempDataSource);
                                if ($scope.form.MemoType.TypeMemoName === "Normal") {
                                    $scope.form.PayToUnitCode = null;
                                } else {
                                    $scope.form.PayToUnitCode = $scope.form.MemoType.UnitCode;
                                }

                                //$scope.tempDataSource = [$scope.form.MemoType.UnitCode];
                                //$scope.payToUnitCodeOption.dataSource.data($scope.tempDataSource);
                                //$scope.form.PayToUnitCode = $scope.form.MemoType.UnitCode;

                                isSave = true;
                                $location.path("memo/create/" + response.Id);
                                setmemoLinesGridOptionGrid(response.MemoLines);
                                for (var j = 0; j < $scope.ApproverLevel.length; j++) {
                                    setApprovalGridDataSource(j, $scope.ApproverLevel[j], $scope.form.MemoApproval);
                                }

                                if (!$scope.form.CCEmail) {
                                    $scope.form.CCEmail = [];
                                }

                                if (!$scope.form.InformEmail) {
                                    $scope.form.InformEmail = [];
                                }

                                $("#ccEmail").data("kendoMultiSelect").dataSource.data($scope.form.CCEmail);
                                $("#informEmail").data("kendoMultiSelect").dataSource.data($scope.form.InformEmail);
                                console.log(response);
                            }
                        },
                        
                        submitSuccess: function(response) {
                            if (preventSubmit === false) {

                            }
                        },
                        uploadHandler: $scope.uploadHandler
                        // uploadSuccessIsCallback: true,
                        // uploadSuccess: function (response, callback) {
                        //  },
                    });
                }
            } else {
                messageBox.error("Please push get final approver button!!!");
            }
        }

        $scope.openK2ActionModal = function (action) {
            $scope.choosenAction = action;
            $scope.modalInstance = k2.createActionModal.call($scope);
        }

        $scope.chooseAction = function (action) {
            if (checkFinalApproverButton) {
                    $scope.Action = action;
                    //Approver
                    $scope.setDataApproverBeforeSave();
                    k2.chooseAction({
                        validator: $scope.validator,
                        actions: $scope.form.Actions,
                        action: action,
                        resource: $scope.form,
                        uploadHandler: $scope.uploadHandler
                    });
            }
            else
            {
                messageBox.error("Please push get final approver button!!!");
            }
        }

        $scope.expenseTopic = new kendo.data.ObservableArray([]);
        $scope.apcode = new kendo.data.ObservableArray([]);

        var checkHaveProposal = false;
        $scope.ProposalRefTitle = "";
        $scope.$watch('form.ProposalRef', function () {
            if ($scope.form.ProposalRef && $scope.form.ProposalRef.ListPlace) {
                if ($scope.form.UnitCode != undefined) {
                    checkHaveProposal = true;
                    //Place
                    var checkM02Data = $scope.form.ProposalRef.ListPlace.filter(function (data) {
                        return data.PlaceCode === "M02" && data.PlaceName === "Ramkhamhaeng";
                    });
                    $scope.placeOptions.dataSource.data(new kendo.data.ObservableArray($scope.form.ProposalRef.ListPlace));
                    $scope.temp.listPlace = new kendo.data.ObservableArray($scope.form.ProposalRef.ListPlace);
                    if (checkM02Data.length === 0) {
                        var m02Data = {
                            PlaceCode: "M02",
                            PlaceName: "Ramkhamhaeng",
                            InActive: false
                        }
                        var dataPlace = new kendo.data.ObservableArray($scope.form.ProposalRef.ListPlace);
                        var dataBrach = dataPlace;
                        dataBrach.push(m02Data);
                        $scope.branchOptions.dataSource.data(dataBrach);
                    } else {
                        $scope.branchOptions.dataSource.data(new kendo.data.ObservableArray($scope.temp.listPlace));
                    }
                    // $scope.branchOptions.dataSource.data(new kendo.data.ObservableArray($scope.temp.listPlace));
                    //////////////
                    var proposalLine = $scope.form.ProposalRef.ProposalLine.filter(function (data) {
                        return data.UnitCode.UnitCode === $scope.form.UnitCode.UnitCode;
                    });
                    //var proposalLine = $filter('filter')($scope.form.ProposalRef.ProposalLine, { UnitCode: $scope.form.UnitCode });
                    if (proposalLine && proposalLine.length > 0) {
                        $scope.expenseTopic = new kendo.data.ObservableArray([]);
                        $scope.apcode = new kendo.data.ObservableArray([]);
                        $scope.expenseTopicTemp = new kendo.data.ObservableArray([]);
                        angular.forEach(proposalLine, function (propline) {
                          
                            $scope.expenseTopicTemp.push(propline.ExpenseTopic);
                            //$scope.apcode.push(propline.APCode);
                        });
                        angular.forEach($scope.expenseTopicTemp, function(temp) {
                            if ($scope.expenseTopic.length > 0) {
                                var check = $filter('filter')($scope.expenseTopic, { ExpenseTopicCode: temp.ExpenseTopicCode });
                                if (check.length === 0) {
                                    $scope.expenseTopic.push(temp);
                                }
                            } else {
                                $scope.expenseTopic.push(temp);
                            }
                        });
                        $scope.expenseTopicOption.dataSource.data($scope.expenseTopic);
                        var apcodes = ($scope.form.ExpenseTopic) ? $filter('filter')($scope.form.ProposalRef.APCode, {
                            UnitCode: $scope.form.UnitCode.UnitCode,
                            ExpenseTopicCode: $scope.form.ExpenseTopic.ExpenseTopicCode
                        }) : $filter('filter')($scope.form.ProposalRef.APCode, {
                            UnitCode: $scope.form.UnitCode.UnitCode,
                        });
                        $scope.apCodeOption.dataSource.data(apcodes);
                        var list;
                        if ($routeParams.id != undefined) {
                            if ($scope.ProposalRefTitle === "") {
                                $scope.ProposalRefTitle = $scope.form.ProposalRef.Title;
                                list = $scope.form.ProposalRef.ProposalLine.filter(function (data) {
                                    return data.Id === $scope.form.ProposalLineId;
                                });
                                $scope.form.BudgetAPCode = list[0].BudgetPlan;
                                $scope.TotalBudgetPlan = list[0].TotalBudgetPlan;
                            } else {
                                if ($scope.ProposalRefTitle !== $scope.form.ProposalRef.Title) {
                                    $scope.ProposalRefTitle = $scope.form.ProposalRef.Title;
                                    $scope.form.BudgetAPCode = null;
                                    $scope.TotalBudgetPlan = null;
                                }
                            }

                        } else if (sn) {
                            list = $scope.form.ProposalRef.ProposalLine.filter(function (data) {
                                return data.Id === $scope.form.ProposalLineId;
                            });
                          
                            if (!angular.isUndefined(list) && list.length !== 0) {
                                $scope.form.BudgetAPCode = list[0].BudgetPlan;
                                $scope.TotalBudgetPlan = list[0].TotalBudgetPlan;
                                if ($routeParams.mode.toUpperCase() === "REVISE") {
                                    $scope.form.BudgetAPCode = $scope.form.BudgetAPCode + $scope.form.BudgetTrans;
                                    $scope.TotalBudgetPlan = $scope.TotalBudgetPlan + $scope.form.BudgetTrans;
                                }
                            }
                        } else if (proposalLineId !== undefined && proposalLineId !== 0) {
                            list = $scope.form.ProposalRef.ProposalLine.filter(function (data) {
                                return data.Id === $scope.form.ProposalLineId;
                            });
                            
                            if (!angular.isUndefined(list) && list.length !== 0) {
                                $scope.form.BudgetAPCode = list[0].Remainning;
                                $scope.TotalBudgetPlan = list[0].TotalBudgetPlan;

                            }
                        } else {
                            $scope.form.BudgetAPCode = null;
                            $scope.TotalBudgetPlan = null;
                        }
                        ///////

                    }
                } else {
                    messageBox.error("Please, select Unit Code before select Proposal.");
                }
                if (sn) {
                    $scope.checkedUnitCode = false;
                    $scope.expenseCheck = false;
                    $scope.checkSubmit = true;
                } else {
                    $scope.checkSubmit = false;
                    $scope.expenseCheck = true;
                    $scope.checkedUnitCode = true;
                }
                //$scope.expenseCheck = true;
            } else if (checkHaveProposal) {
                $scope.form.ProposalRef = "";
                $scope.form.ProposalRef.DocumentNumber = "";
                $scope.form.ProposalRef.Title = "";
                $scope.form.ExpenseTopics = [];
                $scope.form.APCodes = [];
                $scope.form.ExpenseTopic = [];
                $scope.form.APCode = [];
                $scope.form.BudgetAPCode = null;
                $scope.form.IncomeDepositViewModel = null;
                $scope.TotalBudgetPlan = null;
                $scope.temp.listPlace = [];
                $scope.expenseCheck = false;
            
                $scope.expenseTopicDropDownList.value('');
                $scope.proposalNumberOption2.gridOptions.dataSource.data([]);
               // $scope.expenseTopicOption.dataSource.data(new kendo.data.ObservableArray($scope.form.ExpenseTopic));
               // $scope.apCodeOption.dataSource.data(new kendo.data.ObservableArray($scope.form.APCode));
                $scope.branchOptions.dataSource.data(new kendo.data.ObservableArray($scope.temp.listPlace));
            }
        });


        //Unitcode Dropdown OnChange Function
        $scope.unitCodeOptionChange = function () {
            //var dataItem = this.dataItem(data.item);
            if ($scope.form.UnitCode !== "") {
                $scope.checkedUnitCode = true;
                checkFinalApproverButton = false;
                if ($scope.form.ProposalRef) {
                    $scope.form.ProposalRef = "";
                    $scope.form.ProposalRef.DocumentNumber = "";
                    $scope.form.ProposalRef.Title = "";
                    $scope.form.ExpenseTopics = [];
                    $scope.form.APCodes = [];
                    $scope.form.APCode = [];
                    $scope.form.ExpenseTopic = [];
                    $scope.form.BudgetAPCode = null;
                    $scope.TotalBudgetPlan = null;
                    $scope.temp.listPlace = [];
                    $scope.expenseCheck = false;
                    $scope.expenseTopicDropDownList.value('');
                   // $scope.expenseTopicOption.dataSource.data(new kendo.data.ObservableArray($scope.form.ExpenseTopic));
                    //$scope.apCodeOption.dataSource.data(new kendo.data.ObservableArray($scope.form.APCode));
                    $scope.branchOptions.dataSource.data(new kendo.data.ObservableArray($scope.temp.listPlace));
                    proposalLineId = 0;
                }
            } else {
                $scope.checkedUnitCode = false;
                $scope.expenseCheck = false;
                $scope.form.BudgetAPCode = null;
                $scope.TotalBudgetPlan = null;
            }
        }
        $scope.showComments = false;
        if ($routeParams.mode.toUpperCase() !== 'CREATE') {
            $scope.showComments = true;
        }


        //Memo type select function
        $scope.$watch('form.MemoType', function () {
            if ($scope.form.ProposalRef && $scope.form.ProposalRef.ListPlace) {
                $scope.memoTypeSelectFunction();
            }
        });
            $scope.memoTypeSelectFunction = function () {
           
            if ($scope.form.MemoType.TypeMemoCode.toLowerCase() === "normal" || !$scope.form.MemoType.UnitCode) {
              
                //$scope.payToUnitCodeOption.value('');
                $scope.form.PayToUnitCode = null;
                $scope.checkMemoType = false;
                $scope.form.IncomeDepositViewModel = null;
            } else {
                if ($scope.form.MemoType.UnitCode && $scope.form.MemoType.UnitCode.UnitCode) {
                    $scope.tempDataSource = [$scope.form.MemoType.UnitCode];
                    $scope.payToUnitCodeOption.dataSource.data($scope.tempDataSource);
                    $scope.form.PayToUnitCode = $scope.form.MemoType.UnitCode;
                    $scope.checkMemoType = true;
                    $scope.form.IncomeDepositViewModel = null;
                } else {
                  
                    ////$scope.payToUnitCodeOption.value('');
                    if ($scope.form.MemoType && $scope.form.MemoType.ActualCharge) {
                        if ($scope.form.ProposalRef &&
                            $scope.form.ProposalRef.IncomeDepositLine.length > 0) {
                            $scope.IncomeDepositOption.dataSource.data($scope.form.ProposalRef.IncomeDepositLine);
                        } else {
                            $scope.IncomeDepositOption.dataSource.data([]);
                        }
                    }
                    $scope.form.PayToUnitCode = null;
                    $scope.checkMemoType = false;
                }
            }

          
            }
            $scope.IncomeDepositOption = {
                dataSource: new kendo.data.DataSource({
                    type: "json",
                    data: new kendo.data.ObservableArray([])
                }),
                dataTextField: 'DocNumber',
                dataValueField: 'Id',
                template: '#=data.DocNumber# - #=data.Title#',
                valueTemplate: '#=data.DocNumber# - #=data.Title#',
                optionLabel: "please select",
                filter: "contain"
            }



        //Search by Email 
        $scope.EmailOption = approverGrids.emailOption;

        //Branch Option
        $scope.branchOptions = {
            dataSource: new kendo.data.DataSource({
                type: "json",
                data: $scope.dataBranch
            }),
            dataTextField: "PlaceName",
            dataValueField: "PlaceCode",
            optionLabel: "please select",
            template: '#=data.PlaceCode# - #=data.PlaceName#',
            valueTemplate: '#=data.PlaceCode# - #=data.PlaceName#',
            open: function (e) {
                var listContainer = e.sender.list.closest(".k-list-container");
                var table = listContainer.children("template-dropdowlist-header");
                if (table.length > 0) {
                    listContainer.width('auto');
                }
            }
        }

        //Print Report
        $scope.printReport = function () {

            if ($routeParams.id != undefined) {

                var params = {
                    Id: $routeParams.id,
                    inline: true,
                    //show: true,
                    fullscreen: true
                }
                var url = angular.crs.url.rootweb("report/Memo/?params=" + JSON.stringify(params));
                return url;
            }
        }

        //Branch to accounting
        $scope.BranchToAccounting = function (branch) {
            $scope.form.Accounting = branch;
        }
    }]);