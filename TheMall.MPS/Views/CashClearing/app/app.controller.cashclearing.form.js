﻿angular.module('app.controllers').controller('CashClearingFormController',
['$scope',
    '$routeParams', 'controlOptions',
    'lineGrid', '$attrs',
    'cashClearingDataSource', 'uploadHandler',
    'k2', '$http', 'IdCashAdvanceService',
    'messageBox', 'cashAdvanceDataSource',
    'cashClearingDataSource', 'CashClearingGridService',
    '$location', 'approval', '$sce', 'approverGrids',
    function($scope,
        $routeParams, controlOptions,
        lineGrid, $attrs,
        dataSource, uploadHandler,
        k2, $http, idCashAdvanceService,
        messageBox, cashAdvanceDataSource,
        cashClearingDataSource, cashClearingGridService,
        $location, approval, $sce, approverGrids) {

        $scope.form = {
            ListPlace: [],
            DocTypeFiles: [],
            OtherFiles: [],
            CashClearingLines: new kendo.data.ObservableArray([]),
            CashClearingApproval: new kendo.data.ObservableArray([]),
            InformEmail: [],
            CCEmail: []
        };

        $scope.vendor = new kendo.data.ObservableArray([]);

        var sn = k2.extractSN($routeParams),
            remoteResult,
            icheckHaveProposal = false,
            icheckChangeBranch = false;

        $scope.ShowSave = false;
        $scope.forms= {}
        $scope.gridApprovalData = new kendo.data.ObservableArray([]);
        $scope.approverLevelFiveAndSix = true;

        //Current date
        $scope.currentDate = new Date();

        //Grid inline
        $scope.cashClearingLinesGridOption;

        //var _approvalGridOption = [];
        $scope.ApproverLevel = approverGrids.ApproverLevel;

        //Set Due Date
        //Current date
        $scope.currentDate = new Date();
        $scope.currentDatePlusTwo = new Date($scope.currentDate);
        $scope.currentDatePlusTwo.setDate($scope.currentDatePlusTwo.getDate() + 2);
        //Set date Propose
        if ($scope.currentDatePlusTwo.getDay() === 6) {
            $scope.currentDatePlusTwo.setDate($scope.currentDatePlusTwo.getDate() + 2);
        }
        if ($scope.currentDatePlusTwo.getDay() === 0) {
            $scope.currentDatePlusTwo.setDate($scope.currentDatePlusTwo.getDate() + 1);
        }
        $scope.form.DueDatePropose = new Date($scope.currentDatePlusTwo);
        //Set date Accept
        var dueDatePropose = new Date($scope.form.DueDatePropose);
        dueDatePropose.setDate(dueDatePropose.getDate() + 2);
        if (dueDatePropose.getDay() === 6) {
            dueDatePropose.setDate(dueDatePropose.getDate() + 2);
        }
        if (dueDatePropose.getDay() === 0) {
            dueDatePropose.setDate(dueDatePropose.getDate() + 1);
        }
        $scope.form.DueDateAccept = new Date(dueDatePropose);
        //Set date Approve
        var dueDateAccept = new Date($scope.form.DueDateAccept);
        dueDateAccept.setDate(dueDateAccept.getDate() + 2);
        if (dueDateAccept.getDay() === 6) {
            dueDateAccept.setDate(dueDateAccept.getDate() + 2);
        }
        if (dueDateAccept.getDay() === 0) {
            dueDateAccept.setDate(dueDateAccept.getDate() + 1);
        }
        $scope.form.DueDateApprove = new Date(dueDateAccept);
        //Set date Final Approve
        var dueDateFinalApprove = new Date($scope.form.DueDateApprove);
        dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 2);
        if (dueDateFinalApprove.getDay() === 6) {
            dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 2);
        }
        if (dueDateFinalApprove.getDay() === 0) {
            dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 1);
        }
        $scope.form.DueDateFinalApprove = new Date(dueDateFinalApprove);

        //SetDate function 
        //Propose
        var setDateFromPropose = function () {
            //Set date Propose
            var dueDatePropose = new Date($scope.form.DueDatePropose);
            dueDatePropose.setDate(dueDatePropose.getDate() + 2);
            if (dueDatePropose.getDay() === 6) {
                dueDatePropose.setDate(dueDatePropose.getDate() + 2);
            }
            if (dueDatePropose.getDay() === 0) {
                dueDatePropose.setDate(dueDatePropose.getDate() + 1);
            }
            $scope.form.DueDateAccept = new Date(dueDatePropose);
            //Set date Accept
            var dueDateAccept = new Date($scope.form.DueDateAccept);
            dueDateAccept.setDate(dueDateAccept.getDate() + 2);
            if (dueDateAccept.getDay() === 6) {
                dueDateAccept.setDate(dueDateAccept.getDate() + 2);
            }
            if (dueDateAccept.getDay() === 0) {
                dueDateAccept.setDate(dueDateAccept.getDate() + 1);
            }
            $scope.form.DueDateApprove = new Date(dueDateAccept);
            //Set date Final Approve
            var dueDateFinalApprove = new Date($scope.form.DueDateApprove);
            dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 2);
            if (dueDateFinalApprove.getDay() === 6) {
                dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 2);
            }
            if (dueDateFinalApprove.getDay() === 0) {
                dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 1);
            }
            $scope.form.DueDateFinalApprove = new Date(dueDateFinalApprove);
        }

        $scope.changeDueDatePropose = function () {
            //Set date Accept
            var datePropose = new Date($scope.form.DueDatePropose);
            var dateAccept = new Date($scope.form.DueDateAccept);
            if (datePropose.getFullYear() > dateAccept.getFullYear()) {
                setDateFromPropose();
            } else if (datePropose.getFullYear() === dateAccept.getFullYear()
            && datePropose.getMonth() > dateAccept.getMonth()) {
                setDateFromPropose();
            } else if (datePropose.getFullYear() === dateAccept.getFullYear()
            && datePropose.getMonth() === dateAccept.getMonth()
            && datePropose.getDate() > dateAccept.getDate()) {
                setDateFromPropose();
            }
        }

        //Accept
        var setDateFromAccept = function () {
            //Set date Accept
            var dueDateAccept = new Date($scope.form.DueDateAccept);
            dueDateAccept.setDate(dueDateAccept.getDate() + 2);
            if (dueDateAccept.getDay() === 6) {
                dueDateAccept.setDate(dueDateAccept.getDate() + 2);
            }
            if (dueDateAccept.getDay() === 0) {
                dueDateAccept.setDate(dueDateAccept.getDate() + 1);
            }
            $scope.form.DueDateApprove = new Date(dueDateAccept);
            //Set date Final Approve
            var dueDateFinalApprove = new Date($scope.form.DueDateApprove);
            dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 2);
            if (dueDateFinalApprove.getDay() === 6) {
                dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 2);
            }
            if (dueDateFinalApprove.getDay() === 0) {
                dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 1);
            }
            $scope.form.DueDateFinalApprove = new Date(dueDateFinalApprove);
        }

        $scope.changeDueDateAccept = function () {
            var dateAccept = new Date($scope.form.DueDateAccept);
            var dateApprove = new Date($scope.form.DueDateApprove);
            if (dateAccept.getFullYear() > dateApprove.getFullYear()) {
                setDateFromAccept();
            } else if (dateAccept.getFullYear() === dateApprove.getFullYear()
                && dateAccept.getMonth() > dateApprove.getMonth()) {
                setDateFromAccept();
            }
            if (dateAccept.getFullYear() === dateApprove.getFullYear()
                && dateAccept.getMonth() === dateApprove.getMonth()
                && dateAccept.getDate() > dateApprove.getDate()) {
                setDateFromAccept();
            }
        }

        //Approve
        var setDateFromApprove = function () {
            //Set date Final Approve
            var dueDateFinalApprove = new Date($scope.form.DueDateApprove);
            dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 2);
            if (dueDateFinalApprove.getDay() === 6) {
                dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 2);
            }
            if (dueDateFinalApprove.getDay() === 0) {
                dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 1);
            }
            $scope.form.DueDateFinalApprove = new Date(dueDateFinalApprove);
        }

        $scope.changeDueDateApprove = function () {
            var dateApprove = new Date($scope.form.DueDateApprove);
            var dateFinalApprove = new Date($scope.form.DueDateFinalApprove);
            if (dateApprove.getFullYear() > dateFinalApprove.getFullYear()) {
                setDateFromApprove();
            } else if (dateApprove.getFullYear() === dateFinalApprove.getFullYear() &&
                dateApprove.getMonth() > dateFinalApprove.getMonth()) {
                setDateFromApprove();
            } else if (dateApprove.getFullYear() === dateFinalApprove.getFullYear() &&
                dateApprove.getMonth() === dateFinalApprove.getMonth() &&
                dateApprove.getDate() > dateFinalApprove.getDate()) {
                setDateFromApprove();
            }
        }
 
        //Readonly
        $scope.disabled = false;
        $scope.isReadonlyDetails = function () {
            if ($routeParams.mode.toUpperCase() !== "CREATE" && $routeParams.mode.toUpperCase() !== "REVISE") {
                //$($("#texteditor").data().kendoEditor.body).attr('contenteditable', false);
                $scope.disabled = true;
                return true;
            } else {
                $scope.disabled = false;
                return false;
            }
        }

        //Attachement
        $scope.uploadHandler = uploadHandler.init();
        $scope.attchmentSaveUrl = angular.crs.url.webApi("cashclearing/:id/attachment");
        $scope.checkCompleted = false;

        //var setApprovalGridDataSource = function(key, lv, d) {
        //    $scope.form.CashClearingApproval = new kendo.data.ObservableArray(d);
        //    //$scope.ApproverGridOption(key, lv)._approvalGridOption[key].dataSource.data($scope.form.ProposalApproval);
        //    if (_approvalGridOption[key]) {
        //        //debugger;
        //        _approvalGridOption[key].dataSource.data($scope.form.CashClearingApproval);
        //        _approvalGridOption[key].dataSource.schema = $scope.schemaApprove(lv);
        //    }
        //}

        //////Approver
        //Check read only
        //Check that user push "Get final approve" or not.
        var checkFinalApproverButton = false;
        var readonly;
        if ($routeParams.mode.toUpperCase() !== "CREATE" && $routeParams.mode.toUpperCase() !== "REVISE") {
            readonly = true;
            $scope.approverLevelFiveAndSix = true;
        } else {
            $scope.approverLevelFiveAndSix = false;
            readonly = false;
        }

        //Check add data
        var approverGridOptionLevelFourDataCheck = false;

        //Get Final Approver
        $scope.getFinalApproverData = function () {
            if ($routeParams.mode.toUpperCase() !== "CREATE" && $routeParams.mode.toUpperCase() !== "REVISE") {
                return false;
            }
            //if (!$scope.cashClearingLinesGridOption ||
            //    !$scope.cashClearingLinesGridOption.dataSource ||
            //    !$scope.cashClearingLinesGridOption.dataSource._aggregateResult ||
            //    !$scope.cashClearingLinesGridOption.dataSource._aggregateResult.NetAmount ||
            //    !$scope.cashClearingLinesGridOption.dataSource._aggregateResult.NetAmount.sum) {
            //    $scope.form.Budget = 0;
            //} else {
            //    var total = $scope.cashClearingLinesGridOption.dataSource._aggregateResult.NetAmount.sum;
            //    $scope.form.Budget = total;
            //}
            if (!$scope.form.Budget) {
                $scope.form.Budget = 0;
            }
            var id = 0;
            if ($routeParams.id !== undefined) {
                id = $routeParams.id;
            } else if (sn) {
                id = $scope.form.Id;
            }
            checkFinalApproverButton = true;
            if ($scope.form.UnitCode) {
                dataSource.finalApprove({ budget: $scope.form.Budget, id: id, unitcode: $scope.form.UnitCode.UnitCode }, function (data) {
                    if (data && data.Employee) {
                        approverGridOptionLevelFourDataCheck = true;
                        var approverFinal = {
                            Employee: data.Employee,
                            ApproverSequence: data.ApproverSequence,
                            ApproverLevel: data.ApproverLevel,
                            ApproverUserName: data.ApproverUserName,
                            ApproverEmail: data.ApproverEmail,
                            Position: data.Position,
                            Department: data.Department,
                            DueDate: data.DueDate,
                            LineNo: 1,
                            LockEditable: 1
                        };
                        $scope.ApproverGridOptionLevelFourData = [];
                        $scope.ApproverGridOptionLevelFourData.push(approverFinal);
                    } else {
                        messageBox.error("ไม่พบผู้อนุมัติคนสุดท้าย กรุณาติดต่อผู้ดูและระบบ");
                        $scope.ApproverGridOptionLevelFourData = new kendo.data.ObservableArray([]);
                    }
                    $scope.ApproverGridOptionLevelFour.dataSource.data($scope.ApproverGridOptionLevelFourData);
                    $scope.ApproverGridOptionLevelFourData = $scope.ApproverGridOptionLevelFour.dataSource.data();
                });
            } else {
                messageBox.error("Please select unit code!!!");
            }
        }

        //Merge All Approver to One Array before save
        var mergeArrayData = function (data, array) {
            angular.forEach(array, function (value, key) {
                data.push(value);
            });
            return data;
        }

        //Set for save
        var isSave = false;
        //Set approver grid data source on load data process
        var setApprovalGridDataSource = function (key, lv, data) {
            var approverData = data.filter(function (result) {
                return result.ApproverLevel === lv;
            });
            if (lv === 'เสนอ') {
                if (approverData.length === 0) {
                    $scope.ApproverGridOptionLevelOneData = new kendo.data.ObservableArray([]);
                } else {
                    $scope.ApproverGridOptionLevelOneData = [];
                    angular.forEach(approverData, function (value, key) {
                        $scope.ApproverGridOptionLevelOneData.push(value);
                    });
                }
                if (!isSave) {
                    var gridLevelOne = approverGrids.ApproverGridOption(readonly, $scope.ApproverGridOptionLevelOneData, lv);
                    $scope.ApproverGridOptionLevelOne = gridLevelOne;
                }
                $scope.ApproverGridOptionLevelOne.dataSource.data($scope.ApproverGridOptionLevelOneData);
                $scope.ApproverGridOptionLevelOneData = $scope.ApproverGridOptionLevelOne.dataSource.data();
            } else if (lv === 'เห็นชอบ') {
                if (approverData.length === 0) {
                    $scope.ApproverGridOptionLevelTwoData = new kendo.data.ObservableArray([]);
                } else {
                    $scope.ApproverGridOptionLevelTwoData = [];
                    angular.forEach(approverData, function (value, key) {
                        $scope.ApproverGridOptionLevelTwoData.push(value);
                    });
                }
                if (!isSave) {
                    var gridLevelTwo = approverGrids.ApproverGridOption(readonly, $scope.ApproverGridOptionLevelTwoData, lv);
                    $scope.ApproverGridOptionLevelTwo = gridLevelTwo;
                }               
                $scope.ApproverGridOptionLevelTwo.dataSource.data($scope.ApproverGridOptionLevelTwoData);
                $scope.ApproverGridOptionLevelTwoData = $scope.ApproverGridOptionLevelTwo.dataSource.data();
            } else if (lv === 'อนุมัติ') {
                if (approverData.length === 0) {
                    $scope.ApproverGridOptionLevelThreeData = new kendo.data.ObservableArray([]);
                } else {
                    $scope.ApproverGridOptionLevelThreeData = [];
                    angular.forEach(approverData, function (value, key) {
                        $scope.ApproverGridOptionLevelThreeData.push(value);
                    });
                }
                if (!isSave) {
                    var gridLevelThree = approverGrids.ApproverGridOption(readonly, $scope.ApproverGridOptionLevelThreeData, lv);
                    $scope.ApproverGridOptionLevelThree = gridLevelThree;
                }          
                $scope.ApproverGridOptionLevelThree.dataSource.data($scope.ApproverGridOptionLevelThreeData);
                $scope.ApproverGridOptionLevelThreeData = $scope.ApproverGridOptionLevelThree.dataSource.data();
            } else if (lv === 'อนุมัติขั้นสุดท้าย') {
                approverGridOptionLevelFourDataCheck = true;
                if (approverData.length === 0) {
                    checkFinalApproverButton = false;
                    $scope.ApproverGridOptionLevelFourData = new kendo.data.ObservableArray([]);
                } else {
                    checkFinalApproverButton = true;
                    $scope.ApproverGridOptionLevelFourData = [];
                    angular.forEach(approverData, function (value, key) {
                        $scope.ApproverGridOptionLevelFourData.push(value);
                    });
                }
                if (!isSave) {
                    var gridLevelFour = approverGrids.ApproverGridOptionLevelFour(readonly, $scope.ApproverGridOptionLevelFourData);
                    $scope.ApproverGridOptionLevelFour = gridLevelFour;
                }
                $scope.ApproverGridOptionLevelFour.dataSource.data($scope.ApproverGridOptionLevelFourData);
                $scope.ApproverGridOptionLevelFourData = $scope.ApproverGridOptionLevelFour.dataSource.data();
            }
        }

        //Set Data Before Save
        $scope.setDataApproverBeforeSave = function() {
            //Approver
            $scope.form.CashClearingApproval = new kendo.data.ObservableArray([]);
            $scope.form.CashClearingApproval = mergeArrayData($scope.form.CashClearingApproval, $scope.ApproverGridOptionLevelOneData);
            $scope.form.CashClearingApproval = mergeArrayData($scope.form.CashClearingApproval, $scope.ApproverGridOptionLevelTwoData);
            $scope.form.CashClearingApproval = mergeArrayData($scope.form.CashClearingApproval, $scope.ApproverGridOptionLevelThreeData);
            $scope.form.CashClearingApproval = mergeArrayData($scope.form.CashClearingApproval, $scope.ApproverGridOptionLevelFourData);
        }

        var cashAdvanceId = idCashAdvanceService.get();
        var checkProposalRef = false;
        //$scope.uploadHandler = uploadHandler.init();
        //$scope.attchmentSaveUrl = dataSource.$url + '/attachment';
        $scope.showSave = false;
        $scope.temp = { listPlace: [] }
        $scope.noShow = false;

        //Grid
        var cashClearingLineGrid = function(showData) {
            $scope.FormGrid = cashClearingGridService.CashClearingGrid(showData, false);
        }

        cashClearingLineGrid($scope.form.CashClearingLines);

        //Check NetAMount vat & tax
        $scope.setNetAmount = function (e) {
            var unitValue = e.model.Unit;
            var vatValue = e.model.Vat;
            var taxValue = e.model.Tax;
            var amountValue = e.model.Amount;
            var rawSumAmountValue = unitValue * amountValue;
            var vat = 0;
            var amountWithVatAndTax = 0;
            var tax = 0;
            if (taxValue !== 0 && vatValue !== 0) {
                tax = ((taxValue * rawSumAmountValue) / 100).toFixed(2);
                vat = ((vatValue * rawSumAmountValue) / 100).toFixed(2);
                amountWithVatAndTax = vat - tax;
                e.model.NetAmount = rawSumAmountValue + amountWithVatAndTax;
            } else {
                if (taxValue === 0 && vatValue !== 0) {
                    vat = parseFloat(((vatValue * rawSumAmountValue) / 100).toFixed(2));
                    e.model.NetAmount = rawSumAmountValue + parseFloat(vat);
                } else {
                    if (taxValue !== 0 && vatValue === 0) {
                        tax = parseFloat(((taxValue * rawSumAmountValue) / 100).toFixed(2));
                        var sum = rawSumAmountValue - tax;
                        e.model.NetAmount = sum;
                    } else {
                        if (taxValue === 0 && vatValue === 0) {
                            e.model.NetAmount = rawSumAmountValue;
                        }
                    }
                }
            }
            e.model.NetAmount = parseFloat(Math.round(e.model.NetAmount * 100) / 100).toFixed(2); //
            return e.model;
        }

        //Check Sum Actual
        $scope.setNetActual = function (e) {
            var unitValue = e.model.UnitActual;
            if (e.values.UnitActual || e.values.UnitActual === 0) {
                unitValue = e.values.UnitActual;
            }
            //
            var vatValue = e.model.VatActual;
            if (e.values.VatActual || e.values.VatActual === 0) {
                vatValue = e.values.VatActual;
            }
            //
            var taxValue = e.model.TaxActual;
            if (e.values.TaxActual || e.values.TaxActual === 0) {
                taxValue = e.values.TaxActual;
            }

            var actualValue = e.model.Actual;
            if (e.values.Actual || e.values.Actual === 0) {
                actualValue = e.values.Actual;
            }

            var rawSumActualValue = unitValue * actualValue;
            var vat = 0;
            var actualWithVatAndTax = 0;
            var tax = 0;
            if (taxValue !== 0 && vatValue !== 0) {
                tax = ((taxValue * rawSumActualValue) / 100).toFixed(2);
                vat = ((vatValue * rawSumActualValue) / 100).toFixed(2);
                actualWithVatAndTax = vat - tax;
                e.model.NetActual = rawSumActualValue + actualWithVatAndTax;
            } else {
                if (taxValue === 0 && vatValue !== 0) {
                    vat = parseFloat(((vatValue * rawSumActualValue) / 100).toFixed(2));
                    e.model.NetActual = rawSumActualValue + parseFloat(vat);
                } else {
                    if (taxValue !== 0 && vatValue === 0) {
                        tax = parseFloat(((taxValue * rawSumActualValue) / 100).toFixed(2));
                        var sum = rawSumActualValue - tax;
                        e.model.NetActual = sum;
                    } else {
                        if (taxValue === 0 && vatValue === 0) {
                            e.model.NetActual = rawSumActualValue;
                        }
                    }
                }
            }
            e.model.NetActual = parseFloat(Math.round(e.model.NetActual * 100) / 100).toFixed(2); //
            return e.model;
        }

        //Set Budget
        $scope.calNetAcutualNoTaxVat = function (e) {
            var unitValue = e.model.UnitActual;
            if (e.values.UnitActual || e.values.UnitActual === 0) {
                unitValue = e.values.UnitActual;
            }
            var actualValue = e.model.Actual;
            if (e.values.Actual || e.values.Actual === 0) {
                actualValue = e.values.Actual;
            }
            var rawSumActualValue = unitValue * actualValue;
            e.model.NetActualNoVatTax = rawSumActualValue;
            return e.model;
        }

        //
        $scope.sumAmount = function () {
            //debugger;
            if (!$scope.cashClearingLinesGridOption ||
                !$scope.cashClearingLinesGridOption.dataSource ||
                !$scope.cashClearingLinesGridOption.dataSource._aggregateResult ||
                !$scope.cashClearingLinesGridOption.dataSource._aggregateResult.NetAmount ||
                !$scope.cashClearingLinesGridOption.dataSource._aggregateResult.NetAmount.sum) {
               // $scope.form.Budget = 0;
                return $scope.form.Budget;
            }
            var total = $scope.cashClearingLinesGridOption.dataSource._aggregateResult.NetAmount.sum;
            //$scope.form.Budget = total;
            return total;
        }

        $scope.sumActual = function () {
            if (!$scope.cashClearingLinesGridOption ||
                !$scope.cashClearingLinesGridOption.dataSource ||
                !$scope.cashClearingLinesGridOption.dataSource._aggregateResult ||
                !$scope.cashClearingLinesGridOption.dataSource._aggregateResult.NetActual ||
                !$scope.cashClearingLinesGridOption.dataSource._aggregateResult.NetActual.sum) {
                //$scope.form.Actual = 0;
                //$scope.amountBalance = $scope.form.Budget; Old
                $scope.amountBalance = $scope.form.CashAdvanceLinesNetAmount;
                return $scope.form.Actual;
            }
            var total = $scope.cashClearingLinesGridOption.dataSource._aggregateResult.NetActual.sum;
            //$scope.amountBalance = $scope.form.Budget - total;
            $scope.amountBalance = $scope.form.CashAdvanceLinesNetAmount - total;
            if ($scope.amountBalance < 0) {

            }
            //$scope.form.Actual = total;
            return total;
        }

        //Sum net actual
        $scope.sumNetActualNoVatTax = function () {
            if (!$scope.cashClearingLinesGridOption ||
                !$scope.cashClearingLinesGridOption.dataSource ||
                !$scope.cashClearingLinesGridOption.dataSource._aggregateResult ||
                !$scope.cashClearingLinesGridOption.dataSource._aggregateResult.NetActualNoVatTax ||
                !$scope.cashClearingLinesGridOption.dataSource._aggregateResult.NetActualNoVatTax.sum) {
                $scope.form.Actual = 0;
                return 0; //$scope.form.BudgetDetail;
            }
            var total = $scope.cashClearingLinesGridOption.dataSource._aggregateResult.NetActualNoVatTax.sum;
            $scope.form.Actual = total.toFixed(2);
            return total;
        }

        //$scope.isReadonlyDetails = function() {
        //    if ($routeParams.mode.toUpperCase() !== "CREATE" && $routeParams.mode.toUpperCase() !== "REVISE") {
        //        return true;
        //    }
        //    return false;
        //}

        var setCashClearingLinesGridOptionGrid = function(d) {
            //$scope.form.CashClearingLines = new kendo.data.ObservableArray(d);
            $scope.form.CashClearingLines = d;
            //console.log($scope.form.PettyCashLines);
            if ($scope.cashClearingLinesGridOption) {
                $scope.cashClearingLinesGridOption.dataSource.data($scope.form.CashClearingLines);
                $scope.form.CashClearingLines = $scope.cashClearingLinesGridOption.dataSource.data();
            }
        }

        var cashClearingLineSchema = {
            model: kendo.data.Model.define({
                id: "LineNo",
                fields: {
                    LineNo: { type: "number" },
                    Description: { type: "string" },
                    Unit: { type: "number", editable: false },
                    Amount: { type: "number", editable: false },
                    Tax: { type: "number", editable: false },
                    Vat: { type: "number", editable: false },
                    NetAmount: { type: "number", editable: false },
                    Actual: { type: "number", validation: { required: true, min: 0} },
                    TaxActual: { type: "number",  validation: { min: 0}},
                    VatActual: { type: "number",  validation: { min: 0 }},
                    NetActual: { type: "number", editable: false },
                    UnitActual: { type: "number", validation: { min: 0, required: true } },
                    NetActualNoVatTax: { type: "number", validation: { min: 0 } }
                }
            })
        };

        //Line Grid
        $scope.CashClearingLinesGridOption = function() {
            $scope.cashClearingLinesGridOption = lineGrid.gridOption({
                schema: cashClearingLineSchema,
                data: $scope.form.CashClearingLines,
                aggregate: [
                    { field: "Amount", aggregate: "sum" },
                    { field: "NetAmount", aggregate: "sum" },
                    //{ field: "Actual", aggregate: "sum" }
                    { field: "NetActual", aggregate: "sum" },
                    { field: "NetActualNoVatTax", aggregate: "sum" }
                ],
                columns: [
                {
                    field: 'Description',
                    title: 'Description',
                    width: "150px",
                    template: function(data) {
                        if (data.Description !== undefined) {
                            return "<div style='width:100%; word-wrap: break-word;'>" + data.Description + "</div>";
                        }
                        return "";
                    },

                    footerTemplate: "<div class='col-xs-6 col-sm-6 col-md-3 col-lg-3'>Total</div>"
                },
                { field: 'Unit', title: 'Unit', width: "90px", hidden: true, },
                {
                    field: 'Amount',
                    title: 'Price/Unit',
                    width: "90px",
                    template: lineGrid.formatCurrency('Amount'),
                    //footerTemplate: "<div class='text-right'>#:(sum)? kendo.toString(sum,'n2'):0.00#</div>"
                    //footerTemplate: //"<div class='col-xs-6 col-sm-6 col-md-3 col-lg-3'>Total</div>" +
                    //    "<div class='text-right'>{{sumAmount()  | currency:' ':2}}</div>"
                    hidden: true,
                },
                    { field: "Vat", title: "VAT %", width: "50px", hidden: true, },
                    { field: "Tax", title: "TAX %", width: "50px", hidden: true, },
                    {
                        field: "NetAmount",
                        title: "Net amount",
                        width: "90px",
                        template: lineGrid.formatCurrency('NetAmount'),
                        footerTemplate: //"<div class='col-xs-6 col-sm-6 col-md-3 col-lg-3'>Total</div>" +
                            "<div class='text-right'>{{sumAmount() | currency:' ':2}}</div>" ,
                        //"<div class='text-right'>#:(sum)? kendo.toString(sum,'n2'):0.00#</div>"
                        hidden: true,
                    },
                    //////Actual
                      { field: 'UnitActual', title: 'Unit(Actual)', width: "100px", template: '<center>' + lineGrid.formatNo('UnitActual') + '</center>' },
                        {
                            field: 'Actual', title: 'Price/Unit(Actual)',
                            width: "120px",
                            template:lineGrid.formatCurrency('Actual') 
                        },
                    {
                        field: "VatActual", title: "VAT(Actual) %", width: "80px",
                        editor: function (container, options) {
                            $scope.vatValue = approverGrids.vatOption;
                            var editor = $('<select kendo-drop-down-list name="Vat" k-options="vatValue" style="width: 100%"></select>');
                            editor.attr("data-bind", "value:VatActual,source:null");
                            editor.attr("style", "width:100%; word-wrap: break-word;");
                            editor.appendTo(container);
                            return editor;
                        },
                        template: '<center>' + lineGrid.formatNo('VatActual') + '</center>' 
                    },
                    {
                        field: "TaxActual", title: "TAX(Actual) %", width: "80px",
                        editor: function (container, options) {
                            $scope.taxValue = approverGrids.taxOption;
                            var editor = $('<select kendo-drop-down-list name="Tax" k-options="taxValue" style="width: 100%"></select>');
                            editor.attr("data-bind", "value:TaxActual,source:null");
                            editor.attr("style", "width:100%; word-wrap: break-word;");
                            editor.appendTo(container);
                            return editor;
                        },
                        template: '<center>' + lineGrid.formatNo('TaxActual') + '</center>'
                    },
                    {
                        field: 'NetActual',
                        title: 'Net actual',
                        width: "100px",
                        template: lineGrid.formatCurrency('NetActual'),
                        footerTemplate: "<div class='text-right'>{{sumActual() | currency:' ':2}}</div>"
                    },
                     {
                         field: 'PayeeName',
                         title: 'PayeeName',
                         width: "100px",
                         
                     },
                    {
                        field: 'Remark',
                        title: 'Remark',
                        width: "150px",
                        template: function(data) {
                            if (data.Remark !== undefined) {
                                return "<div style='width:100%; word-wrap: break-word;'>" + data.Remark + "</div>";
                            }
                            return "";
                        }
                    },
                    {
                        field: 'Attachment', title: 'Attachment', width: 150,
                        editor : '<div class="k-widget k-upload k-header k-upload-empty"><div class="k-dropzone"><div class="k-button k-upload-button"><input name="otherFiles" type="file" data-role="upload" multiple="multiple" autocomplete="off"><span>Select files...</span></div><em>drop files here to upload</em></div></div>'
                    },
                    {
                        field: "NetActualNoVatTax",
                        title: "NetActualNoVatTax",
                        width: "150px",
                        template: lineGrid.formatCurrency('NetActualNoVatTax'),
                        footerTemplate: "<div class='text-right'>{{sumNetActualNoVatTax() | currency:' ':2}}</div>",
                        hidden: true
                    }
                ],
                editable: {
                    mode: "incell"
                },
               //noadd: true,
               //nodelete : true,
                readonly: $scope.isReadonlyDetails(),
                noedit: true,
                //template: $('#_IncomeDeposit').html(),
                height: "400px",
                save: function (e) {
                    checkFinalApproverButton = false;
                    $scope.setNetAmount(e);
                    $scope.setNetActual(e);
                    $scope.calNetAcutualNoTaxVat(e);
                    //$scope.setNetAmount(e);
                    //if (!$scope.cashClearingLinesGridOption ||
                    //     !$scope.cashClearingLinesGridOption.dataSource ||
                    //     !$scope.cashClearingLinesGridOption.dataSource._aggregateResult ||
                    //     !$scope.cashClearingLinesGridOption.dataSource._aggregateResult.Actual ||
                    //     !$scope.cashClearingLinesGridOption.dataSource._aggregateResult.Actual.sum) {
                    //    e.preventDefault();
                    //    return false;
                    //}else if (!$scope.cashClearingLinesGridOption ||
                    //            !$scope.cashClearingLinesGridOption.dataSource ||
                    //            !$scope.cashClearingLinesGridOption.dataSource._aggregateResult ||
                    //            !$scope.cashClearingLinesGridOption.dataSource._aggregateResult.NetAmount ||
                    //            !$scope.cashClearingLinesGridOption.dataSource._aggregateResult.NetAmount.sum) {
                    //            e.preventDefault();
                    //            return false;
                    //} else {
                    //    var netAmount = $scope.cashClearingLinesGridOption.dataSource._aggregateResult.NetAmount.sum;
                    //    var netActual = $scope.cashClearingLinesGridOption.dataSource._aggregateResult.NetActual.sum;
                    //    if (netActual > netAmount) {
                    //        messageBox.error("Actual must not be more than net amount!!!");
                    //        e.preventDefault();
                    //    }
                    //}
                    this.saveChanges();
                },
                pageable: false,
                groupable: true
            });

            return $scope.cashClearingLinesGridOption;
        }

        //All dropdown
        var dropdownFunction = function() {
            //Cash Clearing Requester Form
            //$scope.temp = { listPlace: [] }
            $scope.unitcodeOption = controlOptions.dropdown('unitcode');
            $scope.requesterOption = controlOptions.popupSearch('employee');
            $scope.requesterOption = controlOptions.popupSearch('employee');
            //$scope.employeeOption = controlOptions.popupSearch('employee');
            $scope.employeeOption = approverGrids.employeeOption;
            $scope.employeePopupSearchOption = controlOptions.popupSearch('employee', { multiple: true });

            //Accounting
            //$scope.accountingOption = controlOptions.popupSearch('accounting', {multiple : true});
            //Branch Accounting Dropdown
            $scope.branchAccountingOptions = {
                dataTextField: "PlaceName",
                dataValueField: "PlaceCode",
                autoBind: false,
                optionLabel: "please select",
                template: '#=data.PlaceCode# - #=data.PlaceName#',
                valueTemplate: '#=data.PlaceCode# - #=data.PlaceName#',
                filter: "contains",
                //enable: false,
                dataSource: {
                    type: 'json',
                    //serverFiltering: true,
                    transport: {
                        read: {
                            url: function () {
                                return angular.crs.url.webApi("accounting/getaccountingbranch");
                            }
                        }
                    }
                }
            }
            //Proposal
            $scope.proposalNumberOption = controlOptions.popupSearch('proposalnumberref');
            $scope.proposalTypeOption = controlOptions.dropdown('typeproposal');
            $scope.apCodeOption = controlOptions.dropdown('apcode');
            $scope.expenseTopicOption = controlOptions.dropdown('expensetopic');
            //$scope.branchOptions = controlOptions.dropdown('place');
        }

        //Print Button
        if ($scope.form) {
              if ($scope.form.StatusFlag === 9 && $scope.form.Requester.Username.toUpperCase() === CURRENT_USERNAME.toUpperCase()) {
              $scope.checkCompleted = true;
           }
        }
      
        var getCashAdvanceViewModel = function(id) {
            remoteResult = cashClearingDataSource.getByCashAdvanceId({ id: id }, function(data) {
                //checkExist = data.Id;
                $scope.form = data;
                //$scope.form.RefAdvanceNumber = data.DocumentNumber;
                //$scope.form.RefAdvanceNumber = data.DocumentNumber;
                $scope.form.OtherFiles = [];
                //$scope.form.RequesterFor = $scope.form.RequesterName;
                //$scope.form.CashClearingLines = new kendo.data.ObservableArray($scope.form.CashClearingLines);
                // checkFinalApproverButton = false;
                $scope.temp.listPlace = $scope.form.ProposalRef.ListPlace;

                $scope.placeOptions = {
                    dataTextField: "PlaceName",
                    dataValueField: "PlaceCode",
                    autoBind: false,
                    enable: false,
                    dataSource: $scope.form.ProposalRef.ListPlace
                };

                $scope.branchOptions = {
                    dataTextField: "PlaceName",
                    dataValueField: "PlaceCode",
                    autoBind: false,
                    enable: false,
                    template: '#=data.PlaceCode# - #=data.PlaceName#',
                    valueTemplate: '#=data.PlaceCode# - #=data.PlaceName#',
                    dataSource: $scope.form.Branch
                };

                $scope.companyOptions = {
                    dataTextField: "CompanyName",
                    dataValueField: "VendorCode",
                    autoBind: false,
                    enable: false,
                    template: "#=data.CompanyName#",
                    valueTemplate: "#=data.CompanyName#",
                    dataSource: $scope.form.Company
                };

                setCashClearingLinesGridOptionGrid($scope.form.CashClearingLines);
                $scope.amountBalance = $scope.form.Budget;

                // New Approve
                for (var j = 0; j < $scope.ApproverLevel.length; j++) {
                    setApprovalGridDataSource(j, $scope.ApproverLevel[j], $scope.form.CashClearingApproval);
                }
                if (!$scope.form.CCEmail) {
                    $scope.form.CCEmail = [];
                }
                if (!$scope.form.InformEmail) {
                    $scope.form.InformEmail = [];
                }
                //if ($scope.form.w2 != null) {
 
                $scope.FormGrid.dataSource.data($scope.form.CashClearingLines);

                //Set Due Date
                //Current date
                $scope.currentDate = new Date();
                $scope.currentDatePlusTwo = new Date($scope.currentDate);
                $scope.currentDatePlusTwo.setDate($scope.currentDatePlusTwo.getDate() + 2);
                //Set date Propose
                if ($scope.currentDatePlusTwo.getDay() === 6) {
                    $scope.currentDatePlusTwo.setDate($scope.currentDatePlusTwo.getDate() + 2);
                }
                if ($scope.currentDatePlusTwo.getDay() === 0) {
                    $scope.currentDatePlusTwo.setDate($scope.currentDatePlusTwo.getDate() + 1);
                }
                $scope.form.DueDatePropose = new Date($scope.currentDatePlusTwo);
                //Set date Accept
                dueDatePropose = new Date($scope.form.DueDatePropose);
                dueDatePropose.setDate(dueDatePropose.getDate() + 2);
                if (dueDatePropose.getDay() === 6) {
                    dueDatePropose.setDate(dueDatePropose.getDate() + 2);
                }
                if (dueDatePropose.getDay() === 0) {
                    dueDatePropose.setDate(dueDatePropose.getDate() + 1);
                }
                $scope.form.DueDateAccept = new Date(dueDatePropose);
                //Set date Approve
                dueDateAccept = new Date($scope.form.DueDateAccept);
                dueDateAccept.setDate(dueDateAccept.getDate() + 2);
                if (dueDateAccept.getDay() === 6) {
                    dueDateAccept.setDate(dueDateAccept.getDate() + 2);
                }
                if (dueDateAccept.getDay() === 0) {
                    dueDateAccept.setDate(dueDateAccept.getDate() + 1);
                }
                $scope.form.DueDateApprove = new Date(dueDateAccept);
                //Set date Final Approve
                dueDateFinalApprove = new Date($scope.form.DueDateApprove);
                dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 2);
                if (dueDateFinalApprove.getDay() === 6) {
                    dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 2);
                }
                if (dueDateFinalApprove.getDay() === 0) {
                    dueDateFinalApprove.setDate(dueDateFinalApprove.getDate() + 1);
                }
                $scope.form.DueDateFinalApprove = new Date(dueDateFinalApprove);
            });
        }

        //if (cashAdvanceId === 0 && $routeParams.id === undefined) {
        //    //MessageBox
        //    console.log("1");
        //    $scope.ShowSave = false;
        //} else {

            //Data Form Cash Advance
            if (cashAdvanceId !== 0 && $routeParams.id === undefined) {
                $scope.ShowSave = true;
                getCashAdvanceViewModel(cashAdvanceId);
                dropdownFunction();
            } else {
                //Data Form Cash Clearing
                if ($routeParams.id != undefined) {
                    $scope.ShowSave = true;
                    dropdownFunction();
                    remoteResult = cashClearingDataSource.get({ id: $routeParams.id }, function(data) {
                        $scope.form = data;
                        $scope.temp.listPlace = $scope.form.ProposalRef.ListPlace;
                        //checkFinalApproverButton = true;
                        //Print Button
                        //$scope.checkCompleted = $scope.form.StatusFlag === 9;//
                        //$scope.form.CashClearingLines = new kendo.data.ObservableArray($scope.form.CashClearingLines);

                        setCashClearingLinesGridOptionGrid($scope.form.CashClearingLines);

                        //$scope.amountBalance = $scope.form.Budget - $scope.form.Actual;

                        for (var j = 0; j < $scope.ApproverLevel.length; j++) {
                            setApprovalGridDataSource(j, $scope.ApproverLevel[j], $scope.form.CashClearingApproval);
                        }
                        if (!$scope.form.CCEmail) {
                            $scope.form.CCEmail = [];
                        }
                        if (!$scope.form.InformEmail) {
                            $scope.form.InformEmail = [];
                        }
                        $scope.placeOptions = {
                            dataTextField: "PlaceName",
                            dataValueField: "PlaceCode",
                            autoBind: false,
                            enable: false,
                            dataSource: $scope.form.ProposalRef.ListPlace
                        };

                        $scope.branchOptions = {
                            dataTextField: "PlaceName",
                            dataValueField: "PlaceCode",
                            autoBind: false,
                            enable: false,
                            template: '#=data.PlaceCode# - #=data.PlaceName#',
                            valueTemplate: '#=data.PlaceCode# - #=data.PlaceName#',
                            dataSource: $scope.form.Branch
                        };

                        $scope.companyOptions = {   
                            dataTextField: "CompanyName",
                            dataValueField: "VendorCode",
                            autoBind: false,
                            enable: false,
                            template: "#=data.CompanyName#",
                            valueTemplate: "#=data.CompanyName#",
                            dataSource: $scope.form.Company 
                        };

                        $scope.FormGrid.dataSource.data($scope.form.CashClearingLines);

                        //Print Button
                        if ($scope.form) {
                            if ($scope.form.StatusFlag === 9 && $scope.form.Requester.Username.toUpperCase() === CURRENT_USERNAME.toUpperCase()) {
                                $scope.checkCompleted = true;
                            }
                        }
                        //Set date Propose
                        $scope.form.DueDatePropose = new Date($scope.form.DueDatePropose);
                        $scope.form.DueDatePropose.setHours(7);
                        //Set date Accept
                        $scope.form.DueDateAccept = new Date($scope.form.DueDateAccept);
                        $scope.form.DueDateAccept.setHours(7);
                        //Set date Approve
                        $scope.form.DueDateApprove = new Date($scope.form.DueDateApprove);
                        $scope.form.DueDateApprove.setHours(7);
                        //Set date Final Approve
                        $scope.form.DueDateFinalApprove = new Date($scope.form.DueDateFinalApprove);
                        $scope.form.DueDateFinalApprove.setHours(7);
                    });
                    $scope.uploadHandler.setId(remoteResult.Id);
                } else if (sn) {
                    dropdownFunction();
                    remoteResult = cashClearingDataSource.worklist({ sn: sn }, function () {
                            $scope.form = remoteResult;
                            $scope.temp.listPlace = remoteResult.ProposalRef.ListPlace;
                            //$scope.form.CashClearingLines = new kendo.data.ObservableArray($scope.form.CashClearingLines);
                            setCashClearingLinesGridOptionGrid(remoteResult.CashClearingLines);
                           // checkFinalApproverButton = true;
                            //Print Button
                           // $scope.checkCompleted = $scope.form.StatusFlag === 9;//
                         
                            for (var j = 0; j < $scope.ApproverLevel.length; j++) {
                                setApprovalGridDataSource(j, $scope.ApproverLevel[j], $scope.form.CashClearingApproval);
                            }
                            if (!$scope.form.CCEmail) {
                                $scope.form.CCEmail = [];
                            }
                            if (!$scope.form.InformEmail) {
                                $scope.form.InformEmail = [];
                            }
                            //$scope.amountBalance = $scope.form.Budget - $scope.form.Actual;
                            $scope.placeOptions = {
                                dataTextField: "PlaceName",
                                dataValueField: "PlaceCode",
                                autoBind: false,
                                enable: false,
                                dataSource: remoteResult.ProposalRef.ListPlace
                            };

                            $scope.branchOptions = {
                                dataTextField: "PlaceName",
                                dataValueField: "PlaceCode",
                                autoBind: false,
                                enable: false,
                                template: '#=data.PlaceCode# - #=data.PlaceName#',
                                valueTemplate: '#=data.PlaceCode# - #=data.PlaceName#',
                                dataSource: remoteResult.Branch
                            };

                            $scope.companyOptions = {
                                dataTextField: "CompanyName",
                                dataValueField: "VendorCode",
                                autoBind: false,
                                enable: false,
                                template: "#=data.CompanyName#",
                                valueTemplate: "#=data.CompanyName#",
                                dataSource: $scope.form.Company
                            };

                            $scope.FormGrid.dataSource.data(remoteResult.CashClearingLines);
                            $scope.uploadHandler.setId(remoteResult.Id).setSN(sn);
                        //Set date Propose
                            $scope.form.DueDatePropose = new Date($scope.form.DueDatePropose);
                            $scope.form.DueDatePropose.setHours(7);
                        //Set date Accept
                            $scope.form.DueDateAccept = new Date($scope.form.DueDateAccept);
                            $scope.form.DueDateAccept.setHours(7);
                        //Set date Approve
                            $scope.form.DueDateApprove = new Date($scope.form.DueDateApprove);
                            $scope.form.DueDateApprove.setHours(7);
                        //Set date Final Approve
                            $scope.form.DueDateFinalApprove = new Date($scope.form.DueDateFinalApprove);
                            $scope.form.DueDateFinalApprove.setHours(7);
                        }, k2.onSnError);
                    } else {                      
                        $scope.ShowSave = false;
                    }
            }

        //Button Event
        $scope.validate = function(event, preventSubmit) {
            if ($scope.form.CashClearingApproval == null) {
                $scope.form.CashClearingApproval = $scope.gridApprovalData;
            }
          
            //if (!checkFinalApproverButton) {
            //    messageBox.error("Please push final approver button!!!");
            //    return false;
            //}

            //Approver
            $scope.setDataApproverBeforeSave();

            if ($scope.form.Actual <= $scope.form.Budget) {
                if ($scope.amountBalance >= 0) {
                     if (preventSubmit !== false) {
                    checkFinalApproverButton = true;
                    }
                    if (checkFinalApproverButton) {
                        k2.saveAndSubmit({
                            event: event,
                            preventSubmit: preventSubmit,
                            validator: $scope.validator,
                            save: cashClearingDataSource.save,
                            data: $scope.form,
                            saveSuccess: function (response) {
                                if ($routeParams.mode.toUpperCase() === "REVISE") {
                                    location.reload();
                                } else {
                                    $scope.form = response;

                                    isSave = true;

                                    $location.path("cashclearing/create/" + response.Id);
                                    setCashClearingLinesGridOptionGrid($scope.form.CashClearingLines);
                                    for (var j = 0; j < $scope.ApproverLevel.length; j++) {
                                        setApprovalGridDataSource(j, $scope.ApproverLevel[j], $scope.form.CashClearingApproval);
                                    }
                                }
                            },
                            submitSuccess: function (response) {
                                if (preventSubmit === false) {
                                }
                            },
                            uploadHandler: $scope.uploadHandler
                        });
                    } else {
                        messageBox.error("Please push get final approver button!!!");
                    }       
                } else {
                    messageBox.error("Amount balance must not be less than zero!!!");
                }
            } else {
                messageBox.error("The actual value must not be more than the budget!!!");
            }
        }

        $scope.openK2ActionModal = function(action) {
            $scope.choosenAction = action;
            $scope.modalInstance = k2.createActionModal.call($scope);
        }

        $scope.chooseAction = function (action) {
            if (checkFinalApproverButton) {
                $scope.Action = action;

                //Approver
                $scope.setDataApproverBeforeSave();
                k2.chooseAction({
                    validator: $scope.validator,
                    actions: $scope.form.Actions,
                    action: action,
                    resource: $scope.form,
                    uploadHandler: $scope.uploadHandler
                });
            } else {
                messageBox.error("Please push get final approver button!!!");
            }
        }

        $scope.showComments = false;
        if ($routeParams.mode.toUpperCase() !== 'CREATE') {
            $scope.showComments = true;
        }

        //Text Editor
        var originwidth = 0;
        var originheight = 0;
        $scope.trustAsHtml = function (string) {
            return $sce.trustAsHtml(string);
        };

        $scope.DescriptionEditor = {
            paste: function (e) {
                setTimeout(function () {
                    $("iframe").contents().find("div").attr("style", "max-width: 100%");
                    $("iframe").contents().find("img").attr("style", "max-width:100%;");
                }, 500);
            },
            tools: [
                     "bold",
                    "italic",
                    "underline",
                    "strikethrough",
                    "justifyLeft",
                    "justifyCenter",
                    "justifyRight",
                    "justifyFull",
                    "insertUnorderedList",
                    "insertOrderedList",
                    "fontName",
                    "fontSize",
                    "foreColor",
                    "backColor",
                    "createTable",
                    "print",
                     {
                         name: "maximize",
                         tooltip: "Maximize",
                         exec: function (e) {
                             if (originheight == 0 || originwidth == 0) {
                                 originheight = $("td.k-editable-area iframe.k-content").height();
                                 originwidth = $("td.k-editable-area iframe.k-content").width();
                             }
                             var editor = $(this).data("kendoEditor");
                             editor.wrapper.css({
                                 "z-index": 9999,
                                 width: $(window).width(),
                                 height: $(window).height(),
                                 position: "fixed",
                                 left: 0,
                                 top: 0
                                 //left: $("aside.main-sidebar .ng-scope").width(),
                                 //top: $("nav.navbar.navbar-static-top").height()
                             });
                             //editor.wrapper.css("height", "800px");
                         }
                     },
                    {
                        name: "restore",
                        tooltip: "Restore",
                        exec: function (e) {
                            var editor = $(this).data("kendoEditor");
                            editor.wrapper.css({
                                width: originwidth,
                                height: originheight,
                                position: "relative"
                            });
                        }
                    }
            ]
        }

        $scope.printReport = function () {
            if ($routeParams.id != undefined) {
                var params = {
                    Id: $routeParams.id,
                    inline: true,
                    //show: true,
                    fullscreen: true
                }
                var url = angular.crs.url.rootweb("report/CashClearing/?params=" + JSON.stringify(params));
                return url;
            }
        }

        //Search by Email 
        $scope.EmailOption = approverGrids.emailOption;
    }
]);