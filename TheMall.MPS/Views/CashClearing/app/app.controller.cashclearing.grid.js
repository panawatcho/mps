﻿app = angular.module("app.controllers");
app.factory("CashClearingGridService", [
    "lineGrid", "messageBox", '$filter',
    function (lineGrid, messageBox, $filter) {
        var cashClearingGridOption = function (kendoData, readonly) {
            var cashClearingModel = {
                model: {
                    id: "LineNo",
                    fields: {
                        LineNo: { type: "number" }
                    }
                },
                columns: [
                    { field: 'Description', title: 'Description', width: 120 },
                    { field: 'Unit', title: 'Unit', width: 150 },
                    { field: 'Amount', title: 'Amount', width: 150 },
                    { field: 'Actual', title: 'Actual', width: 150 },
                    { field: 'Remark', title: 'Remark', width: 200 },
                    { field: 'Attachment', title: 'Attachment', width: 150 },
                    { command: readonly ? [] : ["edit"], title: " ", width: 200 }
                ]
            }
            return {
                dataSource: new kendo.data.DataSource({
                    type: "json",
                    data: kendoData,
                    pageSize: 5,
                    serverPaging: true,
                    serverSorting: true,
                    schema: {
                        model: cashClearingModel.model
                    }
                }),
               toolbar:  [],
               defaultValues: function () {
                    return {
                        LineNo: lineGrid.nextLineNo($scope.form.CashClearingLine)
                    }
                },
                columns: cashClearingModel.columns,
                sortable: true,
                readonly: false,
                scrollable: true,
                template: function () {
                    return $("#AddCashClearingLine").html();
                },
                editable: {
                    mode: "popup",
                    template: $("#AddCashClearingLine").html(),
                    window: {
                        "animation": false,
                        "draggable": false
                    }
                },
                edit: function (e) {
                    e.model.set("LineNo", lineGrid.nextLineNo(this.dataSource.data()));
                }
            }
        };

        return {
            CashClearingGrid: cashClearingGridOption
        }


    }]);
