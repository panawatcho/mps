﻿'use strict';
angular.module('app.controllers').controller('K2SubmitController',
    ['$scope', '$http', 'authService', '$routeParams', 'appText', 'messageBox', '$location', 'sweetAlert',
    function ($scope, $http, authService, $routeParams, appText, messageBox, $location, sweetAlert) {
        if (authService.authentication && authService.authentication.checkLogin) {
            $http.get(angular.crs.url.webApi("workflowservice/submit") + '?process=' + $routeParams.process + '&sn=' + $routeParams.sn + '&action=' + $routeParams.action).then(function (result) {
                if (result.data === true) {
                   // messageBox.success(appText.worklistActionSuccess);
                    sweetAlert.success(appText.worklistActionSuccess);
                   // $scope.returnMessage = appText.worklistActionSuccess;
                    $location.path('/');
                } else {
                    sweetAlert.error("Error ! Failed to action worklist item sn:" + $routeParams.sn);
                    $scope.returnMessage = "Error !";
                }
            }, function (response) {
                messageBox.extractError(response);
                if (response && response.data) {
                    $scope.returnMessage = response.data.Message;
                }
                
            });
        } else {
            authService.forceLogin();
        }
    }]);