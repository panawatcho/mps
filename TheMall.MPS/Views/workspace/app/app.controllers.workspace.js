﻿'use strict';
angular.module('app.controllers').controller('WorkspaceController',
    ['$scope', '$http', 'inquiryGrid', 'workspaceDataSource', 'appText', 'controlOptions',
    function ($scope, $http, inquiryGrid, dataSource, appText, controlOptions) {

        var _employeeGridKendoOption;

        $scope.gridKendoOption = inquiryGrid.gridKendoOption(dataSource.worklist.url,
        {
            height: '550px',
            //height: 'auto',
            scrollable: true,
        });

        $scope.gridKendoOption.dataSource = dataSource.worklist.kendoDataSource;
        $scope.gridKendoOption.filterable = {
            extra: false,
            operators: {
                string: {
                    contains: appText.contains,
                    doesnotcontain: appText.doesnotcontain,
                    eq: appText.eq,
                    neq: appText.neq
                }
            }
        };

        $scope.gridKendoOption.columns =
        [
            //{
            //    template: function (data) {
            //        var img = "";
            //        if (data.DueStatus === "Due")
            //            img = "yellow-flag";
            //        else if (data.DueStatus === "Early")
            //            img = "green-flag";
            //        else if (data.DueStatus === "Late")
            //            img = "red-flag";

            //        return " <img class='worklist-flag-icon' src='Content/Images/" + img + ".png' />";
            //    }, width: "60px", attributes: {
            //        style: "text-align: center"
            //    }
            //},
            { field: "Folio", title: "Document No", width: "200px" },
            //{ field: "ProcessInstanceId", title: "ProcessInstanceId", width: "200px" },
            //{ field: "SN", title: "SN", width: "200px" },
            {
                field: "Title", title: "Title", width: "180px", filterable: false, sortable: false,
                template: function (data) {
                    if (data.Title !== undefined) {
                        return "<div style='width:100%; word-wrap: break-word;'>" + data.Title + "</div>";
                    }
                    return "";
                }
            },
            { field: "ActivityDescription", title: "Activity description", width: "150px" },
            {
                field: "StartDate", title: "Received", width: "120px", attributes: { "class": "text-center" }, template: inquiryGrid.formatDate('StartDate'),
                filterable: false
                //filterable: {
                //    extra: true,
                //    operators: {
                //        date: {
                //            contains: dtgoText.contains,
                //        }
                //    }
                //}
            },
            //{ field: "ExpectedDays", title: "Expected days", width: "100px", attributes: { "class": "text-center" }, filterable: false },
            //{ field: "SN", title: "SN", width: "75px" },
            { template: '<a class=\'k-button\' href=\'#= Data #\'><i class=\'fa fa-pencil\'></i> Open</a>', width: '90px' },
           // { template: '<button class=\'k-button\' ng-click=\'openredirectUsers()\'><i class=\'fa fa-share\' ></i> REDIRECT</button>', width: '120px' },
            { template: '<a class=\'k-button\' target=\'_blank\' href=\'#= ViewFlow #\'><i class=\'fa fa-sitemap\'></i> Flow</a>', width: '90px' }
        ];

        $scope.total = {};
        $scope.earlyPercent = "0%";
        $scope.onduePercent = "0%";
        $scope.overduePercent = "0%";

        dataSource.dataSource.total(function (data) {
            $scope.total = data;
            if (data.Total <= 0) {
                $scope.earlyPercent = "0%";
                $scope.onduePercent = "0%";
                $scope.overduePercent = "0%";
            } else {
                $scope.earlyPercent = (((data.Early / data.Total) * 100)).toFixed(2) + "%";
                $scope.onduePercent = ((data.Ondue / data.Total) * 100).toFixed(2) + "%";
                $scope.overduePercent = ((data.Overdue / data.Total) * 100).toFixed(2) + "%";
            }
        });

        $scope.openredirectUsers = function () {
            //;
            var dataItem = this.dataItem;
            $scope.selectWorklist = dataItem;
            $scope.employeeWindow.center();
            $scope.employeeWindow.open();
        }

        $scope.employeeWindowOption = {
            width: 600,
            height: 300,
            title: "Redirect",
            modal: true
        }

        $scope.employeePopupOptions = controlOptions.popupSearch('employee');

        $scope.redirectWorklistItem = function () {
            // $scope.blockUI.start();
            if (!$scope.employeeRedirect) {
                alert("กรุณาระบุชื่อที่ต้องการ Redirect งานให้");
                return;
            }
               
            if (confirm("ยืนยันการ Redirect ")) {
                   $http({
                       method: 'POST',
                       url: angular.crs.url.webApi("workspace/redirect"),
                       params: {
                           SN: $scope.selectWorklist.SN,
                           toUserFQN: $scope.employeeRedirect.Login
                       }
                   }).then(
                   function (resp) {
                       if (resp.data) {
                           $scope.grid.dataSource.read();
                           $scope.employeeWindow.close();
                           $scope.messageBox.success("Redirect worklist completed.");
                           }
                   },
                function (resp) {
                    $scope.employeeWindow.close();
                    $scope.messageBox.extractError(resp);
                });
            }
        }
    }]);