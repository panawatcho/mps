﻿angular.module('app.controllers').controller('pettycashCompleteController',
[
    '$scope', '$routeParams', 'inquiryGrid','IdProposalService','authService','appUrl','reportService',
    function ($scope, $routeParams, inquiryGrid, idProposalService, authService, appUrl, reportService) {
       

        var params = {
            //filters:
            //  [
            //      { field: "StatusFlag", operator: "eq", value: 9 },
            //  {
            //        field: "CreatedBy",
            //        operator: "eq",
            //        value: CURRENT_USERNAME
            //  }
            //  ]
             // ,
                  mobile: true,
              height: '350px',
        //height: 'auto',
        pageSize: 5,
        scrollable: false,
        sorts: inquiryGrid.sortOption
        };
        $scope.gridKendoOption = inquiryGrid.gridKendoOptionApi(angular.crs.url.webApi('pettycash/related/'), params);
        $scope.gridKendoOption.columns =
            [//inquiryGrid.commandColumn,
                  inquiryGrid.detailColumn,
                { field: 'DocumentStatus', title: 'Status', width: '150px' },
              { field: 'ProposalRef.DocumentNumber', title: 'Proposal number', width: '180px' },
              { field: 'DocumentNumber', title: 'Petty cash number', width: '180px' },
              //{ field: 'Title', title: 'Proposal name', width: '150px' },
              { field: 'Title', title: 'Title', width: '150px' },
              { field: 'Branch.PlaceCode', title: 'Branch', width: '150px' },
              { field: 'UnitCode.UnitCode', title: 'Unit code', width: '125px' },
              { field: 'ExpenseTopic.ExpenseTopicCode', title: 'Expense topic code', width: '200px' },
              { field: 'APCode.APCode', title: 'A&P code', width: '125px' },
                
             inquiryGrid.createdDateColumn
           
          ];
       // $scope.gridKendoOption.toolbar = kendo.template($("#pettycashToolbarTemplate").html());


        $scope.printReport = function (pettyCashId) {
            debugger;
            if (pettyCashId) {
                
                var params = {
                    Id: pettyCashId,
                    inline: true,
                    //show: true,
                    fullscreen: true
                }
                var url = angular.crs.url.rootweb("report/pettycash/?params=" + JSON.stringify(params));
                return url;
            }
        }
    }]);
