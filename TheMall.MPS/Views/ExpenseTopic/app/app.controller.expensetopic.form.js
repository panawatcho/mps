﻿angular.module('app.controllers').controller('expensetopicFormController',
[
    '$scope', '$routeParams', 'controlOptions', '$location', 'messageBox', 'k2', 'expensetopicDataSource','appText',
           'lineGrid',
 function ($scope, $routeParams, controlOptions, $location, messageBox, k2, dataSource, appText,
     lineGrid) {

        $scope.checkNew = true;
        $scope.form = {      
            APCode: new kendo.data.ObservableArray([])
        }

        var _apCodegridOption;

        if (!$routeParams.ExpenseTopicCode) {
            $scope.canDeleted = false;
            $scope.edit = false;
            //   messageBox.error("Sequence number and name are invalid!!!");

            //$location.path("unitcodeforemployee/");
        } else {
            $scope.canDeleted = true;
            $scope.edit = true;
            $scope.checkNew = false;
            console.log($routeParams.AllocCode);
            dataSource.expensetopic({ expenseTopicCode: $routeParams.ExpenseTopicCode }, function (response) {
                $scope.form = response;
                $scope.FormEdit = true;
                //$scope.uploadHandler.setId(response.Id);
                if ($scope.form.APCode) {
                    $scope.setKendoDataSource($scope.form.APCode);
                }
            });
        }
        //Grid AP Code
        $scope.setKendoDataSource = function (d) {
            if (d) {
                $scope.form.APCode = d;
            } else {
                $scope.form.APCode = new kendo.data.ObservableArray([]);
            }
            //console.log($scope.form.PettyCashLines);
            if (_apCodegridOption) {
                _apCodegridOption.dataSource.data($scope.form.APCode);
                $scope.form.APCode = _apCodegridOption.dataSource.data();
            }
        }
        $scope.typeAPOption = controlOptions.dropdown('typeap');
        var apCodegridOptionSchema = {
            model: kendo.data.Model.define({
                fields: {
                    APCode: { type: "string", editable: true },
                    APName: { type: "string", editable: true },
                    Description: { type: "string", editable: true},
                    CheckBudget: { type: "bool", editable: true },
                    SendMail: { type: "bool", editable: true },
                    Order: { type: "number", editable: true }
                }
            })
        };

        $scope.apCodegridOption = function() {
            _apCodegridOption = lineGrid.gridOption({
                schema: apCodegridOptionSchema,
                data: $scope.form.APCode,
                columns: [
                { field: "APCode", title: "A&P Code", width: "70px" },
                { field: "APName", title: "A&P Name", width: "180px" },
                { field: "Description", title: "Description", width: "350px" },
                {
                    field: "CheckBudget", title: "Check budget", width: "80px",
                    template: '<div class="text-center">#if(data.CheckBudget){# <i class="fa fa-check" style="color:green;"></i>#}else{#<i class="fa fa-times" style="color:red;"></i> #}#</div>',
                    editor: '<div class="text-center"><input type="checkbox" name="CheckBudget" data-bind="checked:CheckBudget"></input></div>'
                },
                {
                    field: "SendMail", title: "Send mail", width: "80px",
                    template: '<div class="text-center">#if(data.SendMail){# <i class="fa fa-check" style="color:green;"></i>#}else{#<i class="fa fa-times" style="color:red;"></i> #}#</div>',
                    editor: '<div class="text-center"><input type="checkbox" name="SendMail" data-bind="checked:SendMail"></input></div>'
                },
                {
                    field: "Order", title: "Order", width: "80px",
                    template: lineGrid.formatCurrency('Order')
                }],
                editable: {
                    mode: "incell"
                },
                height: "400px",
                noedit: true
            });
            return _apCodegridOption;
        }

        //
        //
        $scope.validate = function (event) {
            if (!$scope.validator.validate()) return false;
            if ($scope.checkNew) {
                dataSource.save($scope.form).$promise.then(
                    function (resp) {
                        if (resp) {
                            messageBox.success('Saved successfully');
                            $location.path('expensetopic/');
                        }
                    },
                    function (error) {
                        messageBox.extractError(error);
                    }
                );
            } else {
                dataSource.edit($scope.form).$promise.then(
                    function (resp) {
                        if (resp) {
                            messageBox.success('Saved successfully');
                            $location.path('expensetopic/');
                        }
                    },
                    function (error) {
                        messageBox.extractError(error);
                    }
                );
            }
        }

        $scope.delete = function () {
            var deleted = confirm("Delete this data ?");
            if (deleted) {
                dataSource.deleted($scope.form).$promise.then(
                     function (resp) {
                         if (resp) {
                             messageBox.success("Delete successfully.");
                             $location.path('expensetopic/');
                         }
                     },
                    function (error) {
                        messageBox.extractError(error);
                    }
                    );
            }
        }
        //$scope.validate = function (event, preventSubmit) {
        //    if ($scope.form.TypeAP.length > 1) {
        //        messageBox.error("Please create A&P Type only one letter.");
        //    } else {
        //        dataSource.save(
        //            $scope.form, function (response) {
        //            messageBox.success(appText.saveSuccess);
        //            $scope.form = response;
        //            $scope.setKendoDataSource(response.APCode);

        //        });
        //    }   
        //}

 }]);