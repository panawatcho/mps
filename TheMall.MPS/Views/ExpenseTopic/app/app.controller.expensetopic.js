﻿angular.module('app.controllers').controller('expensetopicController',
[
    '$scope', '$routeParams', 'inquiryGrid',
    function ($scope, $routeParams, inquiryGrid) {
    

        var _commandTemplateAllstatus = function (data) {

            return '<div class="btn-group" role="group">' +
                '<a class="btn btn-default" style="width:60px" href="' + $routeParams.page + '/Form?ExpenseTopicCode=' + data.ExpenseTopicCode + '" title="edit"><i class="fa fa-edit"></i></a>' +
                '</div>';

        }
        $scope.gridKendoOption = inquiryGrid.gridKendoOption(angular.crs.url.odata('expensetopictable'));
        // $scope.gridKendoOption.dataSource.sort(inquiryGrid.sortOption);
        $scope.gridKendoOption.columns =
        [
            {
                field: "ExpenseTopicCode", title: "Expense topic code", width: "100px" ,
                template: function (data) {
                    if (data.ExpenseTopicCode) {
                        return "<div style='width:100%; word-wrap: break-word;'>" + data.ExpenseTopicCode + "</div>";
                    }
                    return "";
                }
            },
            {
                field: "ExpenseTopicName", title: "Expense topic name", width: "120px",
                template: function (data) {
                    if (data.ExpenseTopicName) {
                        return "<div style='width:100%; word-wrap: break-word;'>" + data.ExpenseTopicName + "</div>";
                    }
                    return "";
                }
            },
            {
                field: "Description", title: "Description", width: "250px",
                template: function (data) {
                    if (data.Description) {
                        return "<div style='width:100%; word-wrap: break-word;'>" + data.Description + "</div>";
                    }
                    return "";
                }
            },
            {
                field: "TypeAP", title: "Type A&P", width: "70px",
                template: function (data) {
                    if (data.TypeAP) {
                        return "<div style='width:100%; word-wrap: break-word;'>" + data.TypeAP + "</div>";
                    }
                    return "";
                }
            },
             {
                 field: "InActive",
                 title: "Active",
                 width: "70px",
                 template: '<div class="text-center">#if(!data.InActive){# <i class="fa fa-check" style="color:green;"></i>#}else{#<i class="fa fa-times" style="color:red;"></i> #}#</div>'

             },
                {
                    field: "Order", title: "Order", width: "70px",
                    template: inquiryGrid.formatCurrency('Order')
                },
            {
                template: _commandTemplateAllstatus, width: '110px'
            }
        ];
        $scope.gridKendoOption.toolbar = kendo.template($("#expenseTopicTemplate").html());


    }
]);