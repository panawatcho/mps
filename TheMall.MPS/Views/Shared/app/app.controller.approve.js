﻿angular.module('app.services').service('approval',
[
   'controlOptions','lineGrid','messageBox',
    function (controlOptions, lineGrid, messageBox) {
        var _approvalGridOption = [];

        //var employeeOption = controlOptions.popupSearch('employee');
        var employeeOption = controlOptions.combobox('api/employee/searchAll');
        this.ApproverLevel = ["เสนอ", "เห็นชอบ", "อนุมัติ", "อนุมัติขั้นสุดท้าย","ทราบ", "สำเนาเรียน"];

        var generateAutoSequenceNo = function (level, listdata) {
            
           //console.log(listdata);
            var approvalForLevel = listdata.filter(function (data) {
                return data.ApproverLevel == level && data.Deleted !== true;
            });

            console.log(approvalForLevel);
            var sequenceNo = Math.max.apply(Math, approvalForLevel.map(function (item) { return item.ApproverSequence; }));

            return sequenceNo + 1;
        }
      
        
        this.ApproverGridOption = function (level,data,readonly) {
            return lineGrid.gridOption({
                sort: { field: "ApproverSequence", dir: "asc" },
                    schema: {
                        model: kendo.data.Model.define({
                            id: "LineNo",
                            fields: {
                                LineNo: { type: "number" },
                                ApproverSequence: { type: "number", validation: { min: 1 }, editable: false },
                                ApproverLevel: { type: "string", defaultValue: level },
                                DueDate: { type: "date" },
                                Position: {
                                    type: "string",
                                    editable: false
                                },
                                ApproverEmail: {
                                    type: "string",
                                    editable: false
                                },
                                Department: {
                                    type: "string",
                                    editable: false
                                }
                            }
                        })
                    },
                    data: data,
                    filter: [
                        {
                            logic: "and",
                            filters: [
                                {
                                    logic: "or",
                                    filters: [
                                        { field: "ApproverLevel", operator: "eq", value: level },
                                        { field: "ApproverLevel", operator: "eq", value: null },
                                    ]
                                },
                                {
                                    logic: "or",
                                    filters: [
                                        { field: "Deleted", operator: "eq", value: false },
                                        { field: "Deleted", operator: "eq", value: null }
                                    ]
                                }
                            ],

                        }
                    ],
                    toolbar: (level === "อนุมัติขั้นสุดท้าย") ? [
                   
                    {   name:"final",
                        template: '<a class="k-button btn-default" ng-click="getFinalApprover()"><i class="fa fa-plus"></i> ตรวจสอบผู้อนุมัติขั้นสุดท้าย</a>'
                    },
                     { name: "create" }, ] : null,
                    columns: [
                        {
                            field: "ApproverSequence", title: "Sequence",
                            template: lineGrid.formatNo('ApproverSequence'),
                          //  editor :'<div>'+generateAutoSequenceNo(level, data)+'</div>',
                            width: "80px"
                        },
                        {
                            field: "Employee", title: "Name", width: "150px",
                            template: function (data) {
                               
                                if (data.Employee !== undefined) {
                                    return "<div style='width:100%; word-wrap: break-word;'>" + data.Employee.FullName + "</div>";
                                }
                                return "";
                            },
                            editor: function (container, options) {
                                console.log(options); 
                                //var editor = $(' <div kendo-popup-search k-ng-model="dataItem.' + options.field + '" k-options="employeeOption()" name="Employee"></div>');
                                var editor = $('<select kendo-drop-down-list name="Employee" k-ng-model="dataItem" k-options="employeeOption" style="width: 100%"></select>');
                                editor.attr("data-bind", "value:Employee,source:null");
                                //editor.find('a.popup-search-placeholder-wrapper.form-control').attr('style', 'width:90%; height:20px;');
                                //editor.find('a.popup-search-target.form-control').attr('style', 'width:90%; height:20px;');
                                editor.attr("style", "width:100%; word-wrap: break-word;");
                                editor.appendTo(container);
                                return editor;
                            }
                        },
                        { field: "ApproverEmail", title: "Email", width: "120px" },
                        { field: "Position", title: "Position", width: "100px" },
                        {
                            field: "Department", title: "Department", width: "130px",
                            template: function (data) {
                                if (data.Department !== undefined && data.Department !== null && data.Department !== "null") {
                                    return "<div style='width:100%; word-wrap: break-word;'>" + data.Department + "</div>";
                                }
                                return "";
                            }
                        },
                        { field: "DueDate", title: "Due date", width: "110px", template: lineGrid.formatDateTime('DueDate') },
                    ],
                    readonly: readonly,
                    editable: {
                        mode: "inline"
                    },
                    edit: function (e) {

                        //if (e.model.ApproverLevel === "อนุมัติขั้นสุดท้าย") {
                        //    var finalApprover = data.filter(function(data) {
                        //        return data.ApproverLevel === "อนุมัติขั้นสุดท้าย";
                        //    });
                        //    console.log(finalApprover.length);

                        //}
                   
                        if (e.model.isNew()) {
                            e.model.ApproverSequence = generateAutoSequenceNo(level, data);
                        }
                    },
                    height: (readonly) ? 200 : 300,
                    nodelete: (level === "อนุมัติขั้นสุดท้าย") ? true:false,
                   
                    save: function (e) {
                        var check = false;
                        var count = 0;
                        var userName = e.model.Employee.Username;
                        var email = e.model.Employee.Email;
                        var positionName = e.model.Employee.PositionName;
                        var departmentName = e.model.Employee.DepartmentName;
                        var approverSequence = e.model.ApproverSequence;
                        console.log("Add Data");
                        console.log(data);
                        data.filter(function (data) {
                            if (data.ApproverLevel === level &&
                                data.Deleted !== true 
                                )
                            {
                                if (userName === data.Employee.Username &&
                                   approverSequence === data.ApproverSequence) {
                                    check = false;
                                    count = count + 1;
                                   
                                }
                                if (userName !== data.Employee.Username &&
                                    approverSequence === data.ApproverSequence) {
                                    check = false;
                                    count = count + 1;
                                   
                                }

                                if (userName === data.Employee.Username &&
                                    approverSequence !== data.ApproverSequence) {
                                    check = false;
                                    count = count + 1;
                                   
                                }

                                if (userName !== data.Employee.Username &&
                                    approverSequence !== data.ApproverSequence) {
                                    check = true;
                                    //count = count + 1;
                                  
                                        count = count + 1;
                                    }
                                    console.log("In 4");
                                }
                        
                            
                            //return data.ApproverLevel == level && data.Deleted !== true && data.ApproverUserName == e.model.Employee.Username;
                        });
                        if (count === 1 ) {
                            e.model.ApproverUserName =  userName;
                            e.model.ApproverEmail = email;
                            e.model.Position = positionName;
                            e.model.Department = departmentName;
                        } else {
                           
                            //e.model.set("Deleted", true);
                            messageBox.info("Sequence number and name are not valid!!!");
                            //try {
                            e.preventDefault();
                            return false;
                            //}
                            //catch (err) {

                            //}

                        }


                        //if (check.length === 0) {
                        //    e.model.set("ApproverUserName", e.model.Employee.Username);
                        //    e.model.set("ApproverEmail", e.model.Employee.Email);
                        //    e.model.set("Position", e.model.Employee.PositionName);
                        //    e.model.set("Department", e.model.Employee.DepartmentName);
                        //} else {
                        //    e.model.set("Deleted", true);
                        //    messageBox.info("ชื่อซ้ำ");
                       
                        //}


                    },
                    nolock: true,
                    pageable: false,
                    //cancel: function(e) {
                    //    //debugger;
                    //    e.model.set("defaults", null);
                    //    e.model.set("fields", null);
                    //    //e.model.set("idField", null);
                    //    //e.model.set("_defaultId", null);

                   
                    //}
                });
              
            }
    }]);