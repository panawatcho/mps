﻿angular.module('app.services').service('approverGrids',
[
    'controlOptions', 'lineGrid', 'messageBox',
    function (controlOptions, lineGrid, messageBox) {

        var generateAutoSequenceNo = function (listdata) {
            var approvalForLevel = listdata.filter(function (data) {
                return data.Deleted !== true;
            });
            if (approvalForLevel.length === 0) {
                return 1;
            }
           
            var sequenceNo = Math.max.apply(Math, approvalForLevel.map(function (item) { return item.ApproverSequence; }));

            return sequenceNo + 1;
            //console.log(listdata);
            //var approvalForLevel = listdata.filter(function (data) {
            //    return data.Deleted !== true;
            //});

            //console.log("I'm here2");
            //var sequenceNo = Math.max.apply(Math, approvalForLevel.map(function (item) { return item.ApproverSequence; }));

            //return sequenceNo + 1;
        }
        //Set sequence number function for Final Apporver
        var finalApproverGenerateAutoSequenceNo = function (listdata) {
            var approvalForLevel = listdata.filter(function (data) {
                return data.Deleted !== true;
            });
            if (approvalForLevel.length === 0) {
                return 1;
            }
            var sequenceNo = Math.max.apply(Math, approvalForLevel.map(function (item) { return item.ApproverSequence; }));
            //var MaxValue = Math.max.apply(Math, approvalForLevel.map(function (item) { return item.ApproverSequence; }));
            if (approvalForLevel.length !== 1) {
                angular.forEach(listdata, function (value, key) {
                    if (value.ApproverSequence === sequenceNo) {
                        value.ApproverSequence = sequenceNo + 1;
                    }
                });
                return sequenceNo;
            }
            return sequenceNo + 1;
        }

        this.ApproverLevel = ["เสนอ", "เห็นชอบ", "อนุมัติ", "อนุมัติขั้นสุดท้าย", "ทราบ", "สำเนาเรียน"];

        //อนุมัติขึ้นสุดท้าย
        this.ApproverGridOptionLevelFour = function (readonly, data, hideFinalBtn) {
            return lineGrid.gridOption({
                data: data,
                sort: { field: "ApproverSequence", dir: "asc" },
                schema: {
                    model: kendo.data.Model.define({
                        id: "LineNo",
                        fields: {
                            LineNo: { type: "number" },
                            ApproverSequence: {
                                //type: "number", validation: { min: 1 },
                                editable: false
                            },
                            ApproverLevel: {
                                type: "string",
                                defaultValue: "อนุมัติขั้นสุดท้าย",
                                editable: false
                            },
                            DueDate: {
                                type: "date",
                                validation: { min: new Date() },
                                editable: false
                            },
                            Position: {
                                type: "string",
                                editable: false
                            },
                            ApproverEmail: {
                                type: "string",
                                editable: false
                            },
                            Department: {
                                type: "string",
                                editable: false

                            },
                            ApproveStatus: {
                                editable: false
                            }
                        }
                    })
                },
                filter: [
                    {
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: [
                                    { field: "ApproverLevel", operator: "eq", value: "อนุมัติขั้นสุดท้าย" },
                                    { field: "ApproverLevel", operator: "eq", value: null }
                                ]
                            },
                            {
                                logic: "or",
                                filters: [
                                    { field: "Deleted", operator: "eq", value: false },
                                    { field: "Deleted", operator: "eq", value: null }
                                ]
                            }
                        ]
                    }
                ],
                columns: [
                    //{ field: "ApproverSequence", title: "Sequence", width: "80px" },
                    {
                        field: "Employee",
                        title: "Name",
                        width: "190px",
                        template: function (data) {
                            if (data.Employee) {
                                return "<div style='width:100%; word-wrap: break-word;'>" + data.Employee.FullName + "</div>";
                            }
                            return "";
                        },
                        editor: function (container, options) {
                            console.log(options);
                            //var editor = $(' <div kendo-popup-search k-ng-model="dataItem.' + options.field + '" k-options="employeeOption()" name="Employee"></div>');
                            //var editor = $('<select kendo-drop-down-list name="Employee" k-ng-model="dataItem" k-options="employeeOption" style="width: 100%"></select>');
                            var editor = $('<select kendo-drop-down-list name="Employee" k-options="employeeOption" style="width: 100%"></select>');
                            editor.attr("data-bind", "value:Employee,source:null");
                           // editor.find('a.popup-search-placeholder-wrapper.form-control').attr('style', 'width:130%; height:20px;');
                            //editor.find('a.popup-search-target.form-control').attr('style', 'width:90%; height:20px;');
                            editor.attr("style", "width:100%; word-wrap: break-word;");
                            editor.appendTo(container);
                            return editor;
                        }
                    },
                    {
                        field: "ApproverEmail", title: "Email", width: "120px" ,
                        template: function (data) {
                            if (data.ApproverEmail !== undefined) {
                                return "<div style='width:100%; word-wrap: break-word;'>" + data.ApproverEmail + "</div>";
                            }
                            
                        }
                    },
                    {
                        field: "Position", title: "Position", width: "100px",
                        template: function (data) {
                            if (data.Position !== undefined) {
                                return "<div style='width:100%; word-wrap: break-word;'>" + data.Position + "</div>";
                            }

                        }
                    },
                    {
                        field: "Department",
                        title: "Department",
                        width: "130px" //,
                        //template: function(data) {
                        //    if (data.Department !== undefined && data.Department !== null && data.Department !== "null") {
                        //        return "<div style='width:100%; word-wrap: break-word;'>" + data.Department + "</div>";
                        //    }
                        //    return "";
                        //}
                    },
                    {
                        field: "ApproveStatus",
                        title: "Status",
                        width: "50px",
                        template: '<div class="text-center">#if(data.ApproveStatus==true){# <i class="fa fa-check" style="color:green;"></i>#}else{#<i class="fa fa-times" style="color:red;"></i> #}#</div>'
                    }
                   
                ],
                editable: {
                    mode: "incell"
                },
                noedit: true,
                toolbar: (!readonly) ? (
                    (!hideFinalBtn) ?
                        [{
                            name: "final",
                            template: '<a class="k-button btn-default" ng-click="getFinalApproverData()"><i class="fa fa-plus"></i> Final Approve</a>'
                         }
                        , { name: "create" }
                        ]
                    : [{ name: "create" }])
                : null
                ,
                nolock: true,
                pageable: false,
                height: "300px",
                readonly: readonly,
                save: function (e) {
                    var keys = Object.keys(e.values);
                    var data = this.dataSource.data();
                    var approverData;
                    var key = keys[0];
                    var lineNo = Math.max.apply(Math,
                            data.map(function (item) { return item.LineNo; }));
                    if (key) {

                        //e.model[key] = e.values[key];
                        approverData = e.values[key];
                        if (!angular.isNumber(approverData)) {
                            var checkData = data.filter(function (result) {
                                //console.log(result);
                                //console.log(approverData.Username);
                                if (result.Employee) {
                                    if (approverData) {
                                        return result.Employee.Username === approverData.Username &&!result.Deleted;
                                    } else {
                                        return null;
                                    }
                                }
                                return null;
                            });
                            if (e.values.Employee) {
                                if (e.values.Employee.Username !== e.model.ApproverUserName) {
                                    if (checkData.length === 0) {
                                        e.model.ApproverUserName = approverData.Username;
                                        e.model.ApproverEmail = approverData.Email;
                                        e.model.Position = approverData.PositionName;
                                        e.model.Department = approverData.DepartmentName;
                                        if (e.model.LineNo === 0) {
                                            e.model.set("LineNo", lineNo + 1);
                                        }
                                        e.model.ApproverSequence = 0;
                                        // this.saveChanges();
                                    } else {
                                        messageBox.error("Username are invalid!!!");
                                        e.preventDefault();
                                        return false;
                                    }
                                }
                            } else {
                                e.model.ApproverUserName = "";
                                e.model.ApproverEmail = "";
                                e.model.Position = "";
                                e.model.Department = "";
                                e.model.ApproveStatus = null;
                                e.model.ApproverSequence = null;
                            }

                        }
                    }

                },
                edit: function (e) {
                    //debugger;
                    //console.log("1");
                    if (e.model.LockEditable || e.model.StatusFlag === 1) {
                        //console.log("2");
                        // e.preventDefault();
                        //return false;
                        e.sender.closeCell();
                    }
                },
                dataBound: function (e) {
                    //console.log("55+");
                    var grid = this;
                    //  var data = this.dataSource.data();
                    var model;
                    grid.tbody.find("tr[role='row']").each(function () {
                        model = grid.dataItem(this);
                        if (model.LockEditable === 1 || model.LockEditable || model.StatusFlag === 1) {
                            //$(this).find(".k-grid-edit").remove();
                            $(this).find(".k-grid-pseudo-delete").remove();
                            grid.closeCell();
                        }
                    });
                }
                //แบบมี Sequence Number และ in-line และมี Data bound ด้วย
                //  nodelete: true,
                //save: function (e) {
                //    var approverSequence = e.model.ApproverSequence;
                //    if (e.model.Employee) {
                //        var employee = e.model.Employee.Username;
                //    } else {
                //        messageBox.error("Please select approver!!!");
                //        e.preventDefault();
                //        return false;
                //    }

                //    var data = this.dataSource.data();
                //    var count = 0;
                //    var lineNo = Math.max.apply(Math,
                //            data.map(function (item) { return item.LineNo; }));
                //    var dataValue = data.filter(function (result) {
                //        return result.Deleted !== true;
                //    });
                //    angular.forEach(dataValue, function (value, key) {
                //        if (value.ApproverSequence === approverSequence || value.Employee.Username === employee) {
                //            count++;
                //        }
                //    });
                //    if (count === 1) {
                //        if (!e.model.ApproverSequence) {
                //            if (e.model.isNew()) {
                //                var approverSequenceMax =  Math.max.apply(Math,
                //                                            data.map(function (item) { return item.ApproverSequence; }));
                //                if (approverSequenceMax >= 1) {
                //                    angular.forEach(data, function (value, key) {
                //                        if (value.ApproverSequence === approverSequenceMax) {
                //                            value.ApproverSequence = approverSequenceMax + 1;
                //                            e.model.ApproverSequence = approverSequenceMax;
                //                            //e.model.set("ApproverSequence", approverSequenceMax);
                //                        }
                //                    });
                //                } else {
                //                    e.model.ApproverSequence = 1;
                //                    //e.model.set("ApproverSequence", 1);
                //                }
                //            }
                //        }
                //        e.model.ApproverUserName = e.model.Employee.Username;
                //        e.model.ApproverEmail = e.model.Employee.Email;
                //        e.model.Position = e.model.Employee.PositionName;
                //        e.model.Department = e.model.Employee.DepartmentName;
                //        e.model.LockEditable = 0;
                //        //e.model.set("ApproverUserName", e.model.Employee.Username);
                //        //e.model.set("ApproverEmail", e.model.Employee.Email);
                //        //e.model.set("Position", e.model.Employee.PositionName);
                //        //e.model.set("Department", e.model.Employee.DepartmentName);

                //        if (e.model.LineNo === 0) {
                //            e.model.set("LineNo", lineNo + 1);
                //        }
                //    } else {
                //        messageBox.error("Sequence number and name are invalid!!!");
                //        e.preventDefault();
                //        return false;
                //    }
                //}//, 
                //dataBound: function (e) {
                //    console.log("55+");
                //    var grid = this;
                //    var data = this.dataSource.data();
                //    var model;
                //    grid.tbody.find("tr[role='row']").each(function () {
                //        model = grid.dataItem(this);
                //        if (model.LockEditable === 1) {
                //            $(this).find(".k-grid-edit").remove();
                //            $(this).find(".k-grid-pseudo-delete").remove();
                //        }
                //    });
                //}
            });
        }

        //Main
        this.ApproverGridOption = function (readonly, dataGrid, level) {
            return lineGrid.gridOption({
                data: dataGrid,
                sort: { field: "ApproverSequence", dir: "asc" },
                schema: {
                    model: kendo.data.Model.define({
                        id: "LineNo",
                        fields: {
                            LineNo: { type: "number" },
                            ApproverSequence: {
                                //type: "number", validation: { min: 1 },
                                editable: false
                            },
                            ApproverLevel: { type: "string", defaultValue: level },
                            DueDate: { type: "date", validation: { min: new Date() } },
                            Position: {
                                type: "string",
                                editable: false
                            },
                            ApproverEmail: {
                                type: "string",
                                editable: false
                            },
                            Department: {
                                type: "string",
                                editable: false
                            },
                            ApproveStatus: {
                                editable: false
                            }
                        }
                    })
                },
                filter: [
                    {
                        logic: "and",
                        filters: [
                            //{
                            //    logic: "or",
                            //    filters: [
                            //        { field: "ApproverLevel", operator: "eq", value: "เสนอ" },
                            //        { field: "ApproverLevel", operator: "eq", value: null }
                            //    ]
                            //},
                            {
                                logic: "or",
                                filters: [
                                    { field: "Deleted", operator: "eq", value: false },
                                    { field: "Deleted", operator: "eq", value: null }
                                ]
                            }
                        ]
                    }
                ],
                columns: [
                    //{
                    //    field: "ApproverSequence", title: "Sequence", width: "80px"
                    //},
                    {
                        field: "Employee",
                        title: "Name",
                        width: "190px",
                        template: function (data) {
                            // console.log(data);
                            if (data.Employee) {
                                return "<div style='width:100%; word-wrap: break-word;'>" + data.Employee.FullName + "</div>";
                            }
                            return "";
                        },
                        editor: function (container, options) {
                            //console.log(options);
                            //var editor = $(' <div kendo-popup-search k-ng-model="dataItem.' + options.field + '" k-options="employeeOption()" name="Employee"></div>');
                            var editor = $('<select kendo-drop-down-list name="Employee" k-ng-model="dataItem" k-options="employeeOption" style="width: 100%"></select>');
                            editor.attr("data-bind", "value:Employee,source:null");
                            //editor.find('a.popup-search-placeholder-wrapper.form-control').attr('style', 'width:90%; height:20px;');
                            //editor.find('a.popup-search-target.form-control').attr('style', 'width:90%; height:20px;');
                            //editor.attr("ng-change", change(options));
                            editor.attr("style", "width:100%; word-wrap: break-word;");
                            editor.appendTo(container);
                            return editor;
                        }
                    },
                    {
                        field: "ApproverEmail", title: "Email", width: "120px",
                        //template: function (data) {
                        //    if (data.ApproverEmail !== undefined && data.ApproverEmail !== null && data.ApproverEmail !== "null") {
                        //        return "<div style='width:100%; word-wrap: break-word;'>" + data.ApproverEmail + "</div>";
                        //    }
                        //    return "";
                        //}
                    },
                    { field: "Position", title: "Position", width: "100px" },
                    {
                        field: "Department",
                        title: "Department",
                        width: "130px",
                        //template: function (data) {
                        //    if (data.Department !== undefined && data.Department !== null && data.Department !== "null") {
                        //        return "<div style='width:100%; word-wrap: break-word;'>" + data.Department + "</div>";
                        //    }
                        //    return "";
                        //}
                    },
                    {
                        field: "ApproveStatus", title: "Status", width: "50px",
                        template: '<div class="text-center">#if(data.ApproveStatus==true){# <i class="fa fa-check" style="color:green;"></i>#}else{#<i class="fa fa-times" style="color:red;"></i> #}#</div>'
                    }
                ],
                editable: {
                    mode: "incell"
                },
                noedit: true,
                toolbar: (!readonly) ? [
                    { name: "create" }
                ] : null,
                nolock: true,
                pageable: false,
                height: "300px",
                readonly: readonly,
                save: function (e) {
                    var keys = Object.keys(e.values);
                    var data = this.dataSource.data();
                    var approverData;
                    var key = keys[0];
                    var count = 0;
                    var lineNo = Math.max.apply(Math,
                            data.map(function (item) { return item.LineNo; }));
                    if (key) {
                        //e.model[key] = e.values[key];
                        approverData = e.values[key];
                        if (!angular.isNumber(approverData)) {
                            var checkData = data.filter(function (result) {
                                if (result.Employee) {
                                    if (approverData) {
                                        return result.Employee.Username === approverData.Username &&
                                            !result.Deleted;
                                    } else {
                                        return null;
                                    }
                                }
                                return null;
                            });
                            if (e.values.Employee) {
                                if (e.values.Employee.Username !== e.model.ApproverUserName) {
                                    if (checkData.length === 0) {
                                        e.model.ApproverUserName = approverData.Username;
                                        e.model.ApproverEmail = approverData.Email;
                                        e.model.Position = approverData.PositionName;
                                        e.model.Department = approverData.DepartmentName;
                                        if (e.model.LineNo === 0) {
                                            e.model.set("LineNo", lineNo + 1);
                                        }
                                        e.model.ApproverSequence = 0;
                                    } else {
                                        messageBox.error("Username are invalid!!!");
                                        e.preventDefault();
                                        return false;
                                    }
                                }
                            } else {
                                e.model.ApproverUserName = "";
                                e.model.ApproverEmail = "";
                                e.model.Position = "";
                                e.model.Department = "";
                                e.model.ApproveStatus = null;
                                e.model.ApproverSequence = null;
                            }

                        }
                    }
                },
                edit: function (e) {
                    if (e.model.LockEditable || e.model.StatusFlag === 1) {
                        e.sender.closeCell();
                    }
                }
                , dataBound: function (e) {
                    var grid = this;
                    var model;
                    grid.tbody.find("tr[role='row']").each(function () {
                        model = grid.dataItem(this);
                        if (model.LockEditable === 1 || model.LockEditable || model.StatusFlag === 1) {
                            //$(this).find(".k-grid-edit").remove();
                            $(this).find(".k-grid-pseudo-delete").remove();
                            grid.closeCell();
                        }
                    });
                }
                //,Save แบบสนใจ Sequence Number และ Edit แบบ in-line
                //save: function (e) {
                //    var approverSequence = e.model.ApproverSequence;
                //    if (e.model.Employee) {
                //        var employee = e.model.Employee.Username;
                //    } else {
                //        messageBox.error("Please select approver!!!");
                //        e.preventDefault();
                //        return false;
                //    }
                //    var data = this.dataSource.data();
                //    var count = 0;
                //    var lineNo = Math.max.apply(Math,
                //            data.map(function (item) { return item.LineNo; }));
                //    var dataValue = data.filter(function (result) {
                //        return result.Deleted !== true;
                //    });
                //    angular.forEach(dataValue, function (value, key) {
                //        if (value.ApproverSequence === approverSequence || value.Employee.Username === employee) {
                //            count++;
                //        }
                //    });
                //    if (count === 1) {
                //        if (!e.model.ApproverSequence) {
                //            if (e.model.isNew()) {
                //                var sequenceNo = Math.max.apply(Math,
                //                  dataValue.map(function (item) { return item.ApproverSequence; }));
                //                if (sequenceNo >= 1) {
                //                    e.model.ApproverSequence = sequenceNo + 1;
                //                    //e.model.set("ApproverSequence", sequenceNo + 1);
                //                } else {
                //                    //e.model.set("ApproverSequence", 1);
                //                    e.model.ApproverSequence = 1;
                //                }
                //                //console.log(sequenceNo);
                //            }
                //        }
                //        e.model.ApproverUserName = e.model.Employee.Username;
                //        e.model.ApproverEmail = e.model.Employee.Email;
                //        e.model.Position = e.model.Employee.PositionName;
                //        e.model.Department = e.model.Employee.DepartmentName;
                //        //e.model.set("ApproverUserName", e.model.Employee.Username);
                //        //e.model.set("ApproverEmail", e.model.Employee.Email);
                //        //e.model.set("Position", e.model.Employee.PositionName);
                //        //e.model.set("Department", e.model.Employee.DepartmentName);

                //        if (e.model.LineNo === 0) {
                //            e.model.set("LineNo", lineNo + 1);
                //        }
                //    } else {
                //        messageBox.error("Sequence number and name are invalid!!!");
                //        e.preventDefault();
                //        return false;
                //    }
                //}
                //edit: function (e) {
                //    console.log(dataOne);
                //    //if (e.model.isNew()) {
                //    //    var sequenceNo = Math.max.apply(Math, dataOne.map(function(item) { return item.ApproverSequence; }));
                //    //    console.log(sequenceNo);
                //    //    e.model.set("ApproverSequence", sequenceNo + 1);
                //    //}

                //    //if (e.model.defaults) {
                //    //    console.log("56789");
                //    //}
                //    //if (!e.model.ApproverEmail) {
                //    //    if (e.model.isNew()) {
                //    //        e.model.set("ApproverSequence", e.model.LineNo);
                //    //    }
                //    //}
                //}//
            });
        }

        var _emailOption = {
            placeholder: "Search email...",
            dataTextField: "Email",
            dataValueField: "Email",
            template: '#=data.Email# - #=data.Name#',
            valueTemplate: '#=data.Email# - #=data.Name#',
            autoBind: false,
            delay: 1000,
            enforceMinLength: true,
            minLength: 1,
            dataSource: {
                type: 'json',
                serverFiltering: true,
                transport: {
                    read: {
                        url: function (data) {
                            if (data.filter && data.filter.filters && data.filter.filters[0]) {
                                var search = data.filter.filters[0].value;
                                return angular.crs.url.webApi('employee/searchbyemail?email=' + search);
                            }
                            return angular.crs.url.webApi('employee/searchbyemail');
                        }
                    }
                }
            },
            autoClose: false,
           
        };
        //var _emailOption = {
        //    delay: 1000,
        //    autoClose: false,
        //    dataSource: {
        //        transport: {
        //            serverFiltering: true,
        //            read: {
        //                dataType: "json",  //instead of "type: 'jsonp',"
        //                url: angular.crs.url.webApi('employee/searchbyemail')
        //            }
        //        }
        //    },
        //    placeholder: "Search email...",
        //    dataTextField: "Email",
        //    dataValueField: "Email",
        //    template: '#=data.Email# - #=data.Name#',
        //    valueTemplate: '#=data.Email# - #=data.Name#',
        //    filter: "contains",
        //    filtering: function (ev) {
        //        if (ev.filter) {
        //            var filterValue = ev.filter.value;
        //            ev.preventDefault();

        //            this.dataSource.filter({
        //                logic: "or",
        //                filters: [
        //                  {
        //                      field: "Email",
        //                      operator: "contains",
        //                      value: filterValue
        //                  },
        //                  {
        //                      field: "Name",
        //                      operator: "contains",
        //                      value: filterValue
        //                  },
                         

        //                ]
        //            });
        //        } else {
        //            ev.preventDefault();
        //        }

        //    }
        //}
        //Vat Dropdown
        var _vatDropDown = {
            dataTextField: "Vat",
            dataValueField: "Vat",
            dataSource: {
                data: [
                    {
                        Vat: 0
                    },
                    {
                        Vat: 7
                    },
                    {
                        Vat: 10
                    }
                ]
            }
        }

        //Tax Dropdown
        var _taxDropDown = {
            dataTextField: "Tax",
            dataValueField: "Tax",
            dataSource: {
                data: [
                    {
                        Tax: 0
                    },
                    {
                        Tax: 1
                    },
                    {
                        Tax: 2
                    },
                    {
                        Tax: 3
                    },
                    {
                        Tax: 5
                    }
                ]
            }
        }

        var _employeeOption = {
            dataSource: {
                transport: {
                    serverFiltering: true,
                    read: {
                        dataType: "json",  //instead of "type: 'jsonp',"
                        url: angular.crs.url.webApi('employee/search')
                    }
                }
            },
            optionLabel: "please select",
            dataTextField: 'Email',
            dataValueField: 'FullName',
            template: '#=data.FullName# - #=data.PositionName#',
            valueTemplate: '#=data.FullName#',
            filter: "contains",
            filtering: function (ev) {
                if (ev.filter) {
                    var filterValue = ev.filter.value;
                    ev.preventDefault();

                    this.dataSource.filter({
                        logic: "or",
                        filters: [
                          {
                              field: "Email",
                              operator: "contains",
                              value: filterValue
                          },
                          {
                              field: "FullName",
                              operator: "contains",
                              value: filterValue
                          },
                          {
                              field: "FullNameEn",
                              operator: "contains",
                              value: filterValue
                          }

                        ]
                    });
                } else {
                    ev.preventDefault();
                }

            }
        }
        this.employeeOption = _employeeOption;

        this.vatOption = _vatDropDown;

        this.taxOption = _taxDropDown;

        this.emailOption = _emailOption;
    }
]);