﻿angular.module('app.controllers').controller('allocatebasisFormController',
[
    '$scope', '$routeParams', 'lineGrid', "controlOptions", "allocateBasisDataSource", 'appText',
    function ($scope, $routeParams, lineGrid, controlOptions, dataSource, appText) {
    	$scope.fromEdit = false;
    	$scope.form = {
    	    SharedPercents: new kendo.data.ObservableArray([])
    	}
    	var id,
            _gridOption;
    
    	
    	var setKendoDataSource = function (d) {
    	    $scope.form.SharedPercents = new kendo.data.ObservableArray(d);
    	    if (_gridOption) {
    	        _gridOption.dataSource.data($scope.form.SharedPercents);
    	    }
    	}
    	if (angular.isDefined($routeParams.id)) {
    	    id = $routeParams.id;
    	    dataSource.get({ id: id }, function (response) {
    	        $scope.form = response;
    	        $scope.fromEdit = true;
    	        setKendoDataSource(response.SharedPercents);


    	    });
    	} else {

    		$scope.form = {
    		    SharedPercents: new kendo.data.ObservableArray([])
    		};
    	}
    	$scope.validate = function (event, preventSubmit) {

    	    dataSource.save($scope.form, function (response) {
    	        $scope.messageBox.success(appText.saveSuccess);
    	        $scope.form = response;
    	        setKendoDataSource(response.SharedPercents);

    	    }
            );
    	}

    	
    	$scope.placeNameDropdownKendoOption = controlOptions.dropdownKendoOption(angular.crs.url.webApi('place'));
    	$scope.placeNameDropdownKendoOption.dataTextField = "PlaceName";
    	$scope.placeNameDropdownKendoOption.dataValueField = "PlaceCode";
    	//$scope.dropdownKendoOption = controlOptions.dropdownKendoOption('employee');

    	

        // $scope.gridKendoOption.dataSource.sort(inquiryGrid.sortOption);
    	$scope.gridKendoOption = function () {
    	    _gridOption = lineGrid.gridOption({
    	        data: $scope.form.SharedPercents,
    	        schema: {
    	            model: {
    	                id: "LineNo",
    	                fields: {
    	                    LineNo: { type: "number" },
    	                    Percent: { type: "number" }
    	                }
    	            }
    	        },
    	       
    	        columns: [
				
                 {
                     field: 'Place',
                     title: 'PlaceName',
                     width: 250,
                     template: function (e) {
                         return e.Place ? e.Place.PlaceName : "";
                     },
                     editor: function (container, options) {
                         var editor = $('<select id="' + options.field + '" kendo-dropdownlist k-options="placeNameDropdownKendoOption" k-ng-model="dataItem.' + options.field + '"></select>');
                         editor.appendTo(container);
                         return editor;
                     }
                 },
                 {
                     field: 'Percent', title: 'Percent', width: 250,
					
                 },
                 {
                     field: 'TR',
                     title: 'TR',
                     template: lineGrid.checkboxTemplate('TR'), width: 110,
                     editor: function (container, options) {
                         var editor = $('<center><div class=""><input type="checkbox" class="checkbox" id="' + options.field + '" ng-model="dataItem.' + options.field + '"/></div></center>');
                         editor.appendTo(container);
                         return editor;
                     }
                   
                 },
                 {
                     field: 'RE',
                     title: 'RE',
                     template: lineGrid.checkboxTemplate('RE'), width: 110,
                     editor: function (container, options) {
                         var editor = $('<center><div class=""><input type="checkbox" class="checkbox" id="' + options.field + '" ng-model="dataItem.' + options.field + '"/></div></center>');
                         editor.appendTo(container);
                         return editor;
                     }
                 }
    	        ],
    	        editable: {
    	            mode: "inline"

    	        },
    	        height:400,
    	        //noedit: true,
    	        //template: $('#_IncomeDeposit').html(),
    	        save: function (e) {

    	        },
    	        pageable: true,
    	        groupable: true
    	    });
    	    return _gridOption;
    	}
    	

    	//$scope.gridKendoOption.toolbar = kendo.template($("#toolbarTemplate").html());

    }]);