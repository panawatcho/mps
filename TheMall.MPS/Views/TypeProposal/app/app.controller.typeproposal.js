﻿angular.module('app.controllers').controller('typeproposalController',
[
    '$scope', '$routeParams','masterGrid','inquiryGrid',
    function ($scope, $routeParams, masterGrid, inquiryGrid) {
    
        var _gridKendoOption;
        var _commandTemplateAllstatus = function (data) {
            return '<div class="btn-group" role="group">' +
                '<a class="btn btn-default" style="width:60px" href="' + $routeParams.page + '/Form?proposalTypeCode=' + data.TypeProposalCode + '" title="edit"><i class="fa fa-edit"></i></a>' +
                '</div>';
        }
        $scope.apiUrl = angular.crs.url.webApi('typeproposal');
        $scope.gridKendoOption = {
            dataSource: new kendo.data.DataSource({
                transport: {
                    read: {
                        async: true,
                        url: $scope.apiUrl,
                        dataType: "json"
                    },
                    parameterMap: function (options, operation) {
                        if (operation !== "read" && options.models) {
                            return {
                                TypeProposalCode: options.models[0].TypeProposalCode,
                                TypeProposalName: options.models[0].TypeProposalName,
                                InActive: options.models[0].InActive,
                                Order: options.models[0].Order,
                                TypeYearPlan: options.models[0].TypeYearPlan
                            };
                        }
                        return null;
                    }
                },
                pageSize: 10,
                batch: true,
                schema: {
                    model: {
                        fields:
                        {
                            TypeProposalCode: { editable: true, nullable: false },
                            TypeProposalName: { editable: true, nullable: false },
                            InActive: { editable: true, nullable: false },
                            Order: { editable: true, nullable: false },
                            TypeYearPlan: { editable: true, nullable: false }
                        }
                    }
                }
            }),
            toolbar: ["create"],
            columns: [
              {
                  field: "TypeProposalCode", title: "Proposal type", width: "400px",
                  template: function (data) {
                      if (data.TypeProposalCode) {
                          return "<div style='width:100%; word-wrap: break-word;'>" + data.TypeProposalCode + ' - ' + data.TypeProposalName + "</div>";
                      }
                      return "";
                  }
              },
                {
                    field: "TypeYearPlan", title: "Year plan type", width: "200px",
                    template: function (data) {
                        if (data.TypeYearPlan) {
                            return "<div style='width:100%; word-wrap: break-word;  text-align: center;'>" + data.TypeYearPlan + "</div>";
                        }
                        return "";
                    }
                },
              {
                  field: "InActive",
                  title: "Active",
                  width: "200px",
                  template: '<div class="text-center">#if(!data.InActive){# <i class="fa fa-check" style="color:green;"></i>#}else{#<i class="fa fa-times" style="color:red;"></i> #}#</div>',
                  editor: '<input type="checkbox" name="InActive" data-bind="checked:InActive"></input>'
              },
              {
                  field: "Order", title: "Order", width: "100px",
                  template: inquiryGrid.formatCurrency('Order')
              },
              { template: _commandTemplateAllstatus, width: '100px' }
            ],
            filterable: true,
            pageable: {
                refresh: true,
                pageSizes: true,
                buttonCount: 5
            }
        };
        $scope.gridKendoOption.toolbar = kendo.template($("#typeProposalTemplate").html());
        //$scope.gridKendoOption = function () {
        //    if (!_gridKendoOption) {
        //        $scope.odataUrl = angular.crs.url.odata('typeproposaltable');
        //        $scope.apiUrl = angular.crs.url.webApi('typeproposal');

        //        _gridKendoOption = masterGrid.gridKendoOption($scope.odataUrl, $scope.apiUrl,
        //        {
        //            parameterMap: function (options, operation) {
        //                if (operation !== "read" && options.models) {
        //                    return {
        //                        TypeProposalCode: options.models[0].TypeProposalCode,
        //                        TypeProposalName: options.models[0].TypeProposalName
        //                    };
        //                }
        //                return null;
        //            },
        //            schema: {
        //                model: {
        //                    id: "TypeProposalCode",
        //                    fields: {
        //                        TypeProposalCode: { editable: true, nullable: false },
        //                        TypeProposalName: { editable: true, nullable: false }
        //                    }
        //                }
        //            },
        //            toolbar: ["create"],
        //            columns: [
        //                { field: "TypeProposalCode", title: "Proposal type code", width: "200px" },
        //                { field: "TypeProposalName", title: "Proposal type name", width: "200px" },
        //                { command: ["edit", "destroy"], title: "&nbsp;", width: 200 }
        //            ],
        //            editable: {
        //                       mode: "inline",
        //                       createAt: "top"
        //                   }
        //        });
        //    }
        //    return _gridKendoOption;
        //}


    }
]);