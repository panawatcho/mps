﻿angular.module('app.controllers').controller('typeProposalFormController',
[
    '$scope', '$routeParams', 'masterGrid', 'controlOptions',
    'typeProposalDataSource', 'messageBox', '$location',
    function($scope, $routeParams, masterGrid, controlOptions,
        dataSource, messageBox, $location) {
        $scope.form = {};
        $scope.checkNew = true;
        $scope.checkMemoType = false;
        $scope.temp = {};

        $scope.typeYearPlanOption = {
            dataTextField: "value",
            dataValueField: "value",
            autoBind: false,
            optionLabel: "please select",
            template: '#=data.value#',
            valueTemplate: '#=data.value#',
            dataSource: new kendo.data.DataSource({
                type: "json",
                data: new kendo.data.ObservableArray([
                    {
                        value : "AP"
                    }
                ])
            })
        }

        if (!$routeParams.proposalTypeCode) {
            $scope.canDeleted = false;
            $scope.edit = false;
        } else {
            $scope.canDeleted = true;
            $scope.edit = true;
            dataSource.getByTypeMemoCode({
                typeMemoCode: $routeParams.proposalTypeCode
            }, function (data) {
                $scope.checkNew = false;
                $scope.form = data;
                if ($scope.form.TypeYearPlan) {
                    $scope.temp.typeYearPlan = {
                        value: $scope.form.TypeYearPlan
                    }
                }
              

            });
        }


        $scope.validate = function (event) {
            if (!$scope.validator.validate()) return false;
            if ($scope.temp.typeYearPlan) {
                $scope.form.TypeYearPlan = $scope.temp.typeYearPlan.value;
            }

            if ($scope.checkNew) {
                dataSource.save($scope.form).$promise.then(
                    function (resp) {
                        if (resp) {
                            messageBox.success('Saved successfully');
                            $location.path('typeproposal/');
                        }
                    },
                    function (error) {
                        messageBox.extractError(error);
                    }
                );
            } else {
                dataSource.edit($scope.form).$promise.then(
                    function (resp) {
                        if (resp) {
                            messageBox.success('Saved successfully');
                            $location.path('typeproposal/');
                        }
                    },
                    function (error) {
                        messageBox.extractError(error);
                    }
                );
            }
        };

        $scope.delete = function () {
            var deleted = confirm("Delete this data ?");
            if (deleted) {
                dataSource.deleted($scope.form).$promise.then(
                     function (resp) {
                         if (resp) {
                             messageBox.success("Delete successfully.");
                             $location.path('typeproposal/');
                         }
                     },
                    function (error) {
                        messageBox.extractError(error);
                    }
                    );
            }
        }
    }
]);