﻿using System.Web;
using System.Web.Optimization;
using System.Web.UI.WebControls;

namespace TheMall.MPS
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            #if DEV || PROD
                        BundleTable.EnableOptimizations = true;
            #else
                        BundleTable.EnableOptimizations = false;
            #endif

            
            // The order in this list is important
            bundles.Add(new ScriptBundle("~/content/scripts")
                .Include(
                    "~/content/jquery/jquery-2.1.4.js",
                    "~/content/angular/angular.js",
                    "~/content/angular/angular-route.js",
                    "~/content/angular/angular-resource.js",
                    "~/content/angular/angular-animate.js",
                      "~/content/angular/angular-touch.js",
                    "~/content/angular/angular-cookies.js",
                    "~/content/angular/angular-sanitize.js",
                    "~/content/angular-gettext/angular-gettext.js",
                    //"~/content/angular-translate/angular-translate.js",
                    //"~/content/angular-translate/angular-translate-loader-static-files.js",
                    "~/content/bootstrap/js/bootstrap.js",
                    "~/content/bootstrap/js/ui-bootstrap.js",
                    "~/content/bootstrap/js/ui-bootstrap-tpls-2.0.0.min.js",
                    
                 
                 
                   
                    "~/content/adminlte/js/app.js",
                    "~/content/kendo/kendo.all.js",
                    //"~/content/kendo/cultures/kendo.culture.en-GB.js",
                    //"~/content/kendo/cultures/kendo.culture.th.js",
                    //"~/content/kendo/messages/kendo.messages.th.js"
                    "~/content/angular-block-ui/angular-block-ui.js",
                    "~/content/angular-loading-bar/loading-bar.js",
                    //"~/content/jquery-file-upload/js/jquery.ui.widget.js",
                    //"~/content/load-image/load-image.all.min.js",
                    //"~/content/canvas-to-blob/canvas-to-blob.js",
                    //"~/content/jquery-file-upload/js/jquery.iframe-transport.js",
                    //"~/content/jquery-file-upload/js/jquery.fileupload.js",
                    //"~/content/jquery-file-upload/js/jquery.fileupload-process.js",
                    //"~/content/jquery-file-upload/js/jquery.fileupload-validate.js",
                    //"~/content/jquery-file-upload/js/jquery.fileupload-image.js",
                    //"~/content/jquery-file-upload/js/jquery.fileupload-angular.js",
                    "~/content/ng-file-upload/ng-file-upload-all.js",
                    "~/ngapp/app.modules.js",
                     
                    "~/content/kendo/kendo.maskedDatePicker.js",
                    "~/content/kendo/kendo.popupSearch.js",
                    "~/content/kendo/kendo.radioButtonGroup.js",
                    "~/content/kendo/kendo.radioButtonGroupSelect.js",
                    "~/content/tree/jstree.js",
                     "~/content/sweetalert/bluebird.min.js",
                     "~/content/sweetalert/sweetalert2.min.js",
                    "~/content/sweetalert/SweetAlert.js"
                   

                )
                .Include("~/content/js/script.js")
                .IncludeDirectory("~/ngapp", "*.js", true)
                .IncludeDirectory("~/views", "*.js", true)
            );

            bundles.Add(new StyleBundle("~/content/styles").Include(
                "~/content/bootstrap/css/bootstrap.css",
                 "~/content/bootstrap/css/bootstrap.min.css",
                "~/content/kendo/kendo.common-bootstrap.css",
                "~/content/kendo/kendo.bootstrap.css",
                //"~/content/jquery-file-upload/css/jquery.fileupload.css",
                //"~/content/jquery-file-upload/css/jquery.fileupload-ui.css",
                "~/content/site.css",
                "~/content/sweetalert/sweetalert2.min.css",
                "~/content/sweetalert/sweetalert2.css"
                  
               ));

            bundles.Add(new ScriptBundle("~/bundles/ngapp")
                //.IncludeDirectory("~/NgApp", "*.js", true)
                //.IncludeDirectory("~/Views", "*.js", true)
                 .Include("~/content/ng-file-upload/ng-file-upload-all.js", 
                 "~/ngapp/app.modules.js")
            );
        }
    }
}
