(function ($) {
    var kendo = window.kendo,
        ui = kendo.ui,
        Widget = ui.Widget,
        CHANGE = "change",
        ROWCHANGED = "rowChanged",
        SHOW = "show",
        SHOWN = "shown",
        BLUR = "blur",
        DATABINDING = "dataBinding",
        DATABOUND = "dataBound",
        ns = ".kendoPopupSearch";

    var PopupSearch = Widget.extend({
        init: function (element, options) {
            var that = this,
                _target,
                _grid,
                _window,
                _rowTemplateId,
                _title,
                _btnClear,
                _searchButton,
                _selectButton,
                _searchButtonText,
                modalId,
                _kNgModel,
                _required,
                _name,
                _ngRequired;
            Widget.fn.init.call(this, element, options);
            //if (that.events) {
            //    that._events = that.events;
            //}
            that._handler = {
                requiredWatch: null
            };
            that._dataSource();
            that.element.addClass('popup-search');
            that.element.addClass('popup-search-wrapper');
            that.element.addClass('form-control');
            that.element.on(BLUR + ns, $.proxy(that._blur, that));
            _kNgModel = that.element.attr('k-ng-model');
            _required = that.element.attr('required');
            _name = that.element.attr('name');
            _ngRequired = that.element.attr('ng-required');
            if (that.options.gridOptions.dataSource == undefined) {
                throw 'dataSource is undefined for ' + _kNgModel;
            }
            modalId = 'modal_' + that.dataSource.uid;

            // Placeholder
            if (that.options.placeholderHtml == null) {
                if (that.options.placeholderText != null) {
                    that.placeholder = $('<span class="popup-search-placeholder"></span>').html(that.options.placeholderText);
                    that.placeholder = $('<a class="popup-search-placeholder-wrapper" ' +
                        'onclick="return false;" href="#" data-toggle="modal" data-target="#' + modalId + '"></a>')
                        .html(that.placeholder);
                }
            } else {
                that.placeholder = $(that.options.placeholderHtml);
            }

            that.element.append(that.placeholder);
            that.placeholder.show();

            // Result target div
            _target = $('<a class="popup-search-target" onclick="return false;" href="#" data-toggle="modal" data-target="#' + modalId + '" style="display:none"></a>');
            that.element.append(_target);
            that.target = _target;

            if (that.options.readonly) {
                that._setReadonly(that.options.readonly);
            }

            if (_required === 'required' || _required === 'true'
                || _ngRequired) {
                var n = _name;
                if (!n && _kNgModel) {
                    var ll = _kNgModel.lastIndexOf('.');
                    if (ll > 0) {
                        n = _kNgModel.substr(ll + 1);
                    } else {
                        n = _kNgModel;
                    }
                }
                if (n != undefined) {
                    var hidden;
                    if (_required === 'required' || _required == 'true' && _required != 'delegate') {
                        hidden = $('<input type="hidden" required="required" name="' + n + '" />');
                        //} else if (_ngRequired) {
                        //hidden = $('<input type="hidden" data-bind="required:"' + _ngRequired + '" name="' + n + '" />');
                        //hidden = $('<input type="hidden" ng-required="' + _ngRequired +'" name="' + n + '" />');
                        //} else {
                        //    hidden = $('<input type="hidden" name="' + n + '" />');
                    } else {
                        hidden = $('<input type="hidden" ng-required="' + _ngRequired + '" name="' + n + '" />');
                        if (that._handler.requiredWatch) {
                            that._handler.requiredWatch();
                        }
                        that._handler.requiredWatch = that.$angular_scope.$watch(_ngRequired, function (value) {
                            if (value) {
                                hidden.attr('required', 'required');
                            } else {
                                hidden.removeAttr('required');
                            }
                        });
                    }
                    that.element.append(hidden);
                    that.hidden = hidden;
                }
            }

            that.template = kendo.template(that.options.template);

            _searchButtonText = that.options.searchButtonText || '<i class="fa fa-search"></i> Search';
            that.options.searchButtonText = _searchButtonText;

            // Filter
            _title = that.options.title || 'Search';
            _rowTemplateId = that.filterRowTemplateId;
            if ($('#' + _rowTemplateId).length <= 0) {
                $('body').append($('<script type="text/x-kendo-template" id="' + _rowTemplateId + '"></script>').html(that.filterRowTemplate));
            }
            _window = that._windowTemplate(_title, _rowTemplateId, _searchButtonText);
            _window.attr('id', modalId);
            //that.element.append(_window);
            $('body').append(_window);
            that.window = _window; // API

            // Grid
            var gridOptions = that.options.gridOptions;
            var checkboxTemplate = function (dataItem) {
                if (that.valueArray.indexOf(dataItem.uid) >= 0) {
                    return '<a class="btn btn-primary popup-search-multiple-toggle active"><i class="fa fa-check"></i></a>';
                }
                return '<a class="btn btn-default popup-search-multiple-toggle"><i class="fa fa-check"></i></a>';
            }
            that._clearValueArray = function () {
                that.valueArray = [];
            }
            that._clearValueArray();
            if (that.options.multiple) {
                gridOptions.columns.splice(0, 0,
                    { template: checkboxTemplate, title: '', width: '50px', lock: true }
                );
            }
            gridOptions.change = $.proxy(that._gridOnChange, that);
            gridOptions.dataBound = $.proxy(that._gridDataBound, that);
            that._readUrl = that.options.gridOptions.dataSource.options.transport.read.url;
            _grid = _window.find('.popup-search-result-grid').kendoGrid(that.options.gridOptions).getKendoGrid();
            _grid.element.data('popupSearch', that);
            that.grid = _grid; // API

            _searchButton = _window.find('.popup-search-btn-search');
            _searchButton.click(function () {
                that.search();
            });
            _selectButton = _window.find('.popup-search-btn-select');
            _selectButton.click(function () {
                that.select(that.grid.select());
            });

            _btnClear = _window.find('.popup-search-btn-clear');
            _btnClear.on('click', function (e) {
                e.preventDefault();
                that.clear();
            });
            _btnClear.hide();
            that.clearButton = _btnClear;
            that.window.on('show.bs.modal', function (e) {
                if (that.element.attr('disabled') || that.element.attr('readonly')) {
                    e.preventDefault();
                    return;
                }
                that.trigger(SHOW);
                that._resetSearchButton();
                if (that.options.locked) {
                    var fields = that.dataSource.popupSearchFields;
                    var l = false;
                    for (var i = 0; i < fields.length; i++) {
                        if (fields[i].lock == true && (fields[i].value == undefined || fields[i].value === '')) {
                            l = true;
                            break;
                        }
                    }
                    if (l) {
                        that._disableButton(_searchButton, true);
                    } else {
                        that._disableButton(_searchButton, false);
                    }
                }
                if (that.grid.dataSource.data().length > 0) {
                    that.grid.refresh();
                }
            });
            that.window.on('shown.bs.modal', function (e) {
                that.trigger(SHOWN);
                that.window.find('input.popup-search-filter-textbox[type="text"]:first').focus();
            });

            that.window.on('hide.bs.modal', function (e) {
                that._animateStop(that);
                //that.window.remove();
            });

            _grid.element.on('dblclick', 'tbody[role="rowgroup"] tr[role="row"]', function (e) {
                if (!that.options.multiple) {
                    that.select(that.grid.select());
                }
            });
            _grid.element.on('keypress', function (e) {
                var keyCode = (window.event) ? e.keyCode : e.which;
                if (keyCode == 13) {
                    that.select(that.grid.select());
                }
            });

            that.searchButton = _searchButton;
            that.selectButton = _selectButton;
            that.refresh();

            _window.on('keyup', '.popup-search-filter-textbox', function (e) {
                var keyCode = (window.event) ? e.keyCode : e.which;
                if (keyCode == 13) {
                    var fields = that.dataSource.popupSearchFields;
                    var $t = $(this);
                    for (var i = 0; i < fields.length; i++) {
                        if (fields[i].name == $t.attr('name')) {
                            that.dataSource.popupSearchFields[i].set('value', $t.val()); // Inline edit issue
                            //that.dataSource.popupSearchFields[i].value = $t.val();
                            that.search();
                            break;
                        }
                    }
                }
            });

            _window.on('keypress', function (e) {
                // Prevent form submit on enter
                var keyCode = (window.event) ? e.keyCode : e.which;
                if (keyCode == 13) {
                    e.preventDefault();
                }
            });

            if (that.options.multiple) {
                that.grid.element.on('click', '.popup-search-multiple-toggle', function (e) {
                    var row = $(e.target).closest('tr');
                    var dataItem = that.grid.dataItem(row);
                    if (that.valueArray.indexOf(dataItem.uid) >= 0) {
                        that.valueArray.splice(that.valueArray.indexOf(dataItem.uid), 1);
                    } else {
                        that.valueArray.push(dataItem.uid);
                    }
                    if (that.valueArray.length > 0) {
                        that._disableButton(that.selectButton, false);
                    } else {
                        that._disableButton(that.selectButton, true);
                    }
                    that.grid.refresh();
                });
            }

            if (that.options.value != undefined && that.dataItem() == undefined) {
                try {
                    var s = _kNgModel;
                    var o = that.$angular_scope;
                    s = s.replace(/\[(\w+)\]/g, '.$1'); // convert indexes to properties
                    s = s.replace(/^\./, '');           // strip a leading dot
                    var a = s.split('.');
                    for (var i = 0; i < a.length; ++i) {
                        var k = a[i];
                        if (k in o) {
                            o = o[k];
                        }
                        else {
                            if (i === a.length - 1) {
                                o[k] = that.options.value;
                            } else {
                                o = o[k] = {};
                            }
                        }
                    }
                } catch (e) {
                    console.log("Failed on setting default value of " + _kNgModel);
                }
                //that.$angular_setLogicValue(that.options.value);
                //that.$angular_scope.form.Body["WeightUnit"] = that.options.value;
            }

        },

        options: {
            name: "PopupSearch",
            gridOptions: {},
            title: null,
            popupSearchFields: [],
            template: '',
            inlineTemplate: '',
            placeholderText: null,
            placeholderHtml: null,
            buttonText: null,
            buttonHtml: null,
            searchData: null, // Trigger on search (aka. behind the scene)
            locked: false,
            searchButtonText: null,
            keyField: null,
            readonly: false,
            value: null
        },
        _animateStart: function (that) {
            var lookingUp = 'Looking up';
            var wait = "";
            that._dots = window.setInterval(function () {
                if (wait.length >= 3)
                    wait = "";
                else
                    wait += ".";
                that.searchButton.html(lookingUp + " " + wait);
            }, 1000);
            that._animationCleanUp = window.setInterval(function () {
                that._animateStop(that);
            }, 15000);
        },
        _animateStop: function (that) {
            clearInterval(that._dots);
            clearInterval(that._animationCleanUp);
        },

        search: function () {
            var that = this,
                readUrl = that._readUrl;

            if (that.searchButton.hasClass('disabled'))
                return;

            that.options.gridOptions.dataSource._filter = {};
            that._disableButton(that.selectButton, true);
            that._disableButton(that.searchButton, true);
            that._clearValueArray();
            that.searchButton.html('Looking up ...');
            that._animateStart(that);
            //var lookingUp = 'Looking up';
            //var wait = "";
            //var dots = window.setInterval(function () {
            //    if (wait.length >= 3)
            //        wait = "";
            //    else
            //        wait += ".";
            //    that.searchButton.html(lookingUp + " " + wait);
            //}, 1000);
            //var q = window.setInterval(function () {
            //    clearInterval(dots);
            //    clearInterval(q);
            //}, 12000);
            var queryString = '';
            var popupSearchFields = that.dataSource.popupSearchFields;
            $.map(popupSearchFields, function (value) {
                if (value.value != undefined) {
                    var trimmed = $.trim(value.value);
                    if (trimmed !== '') {
                        queryString += value.name + '=' + trimmed + '&';
                    }
                }
            });

            if (that.options.searchData != null) {
                var data = that.options.searchData(that, popupSearchFields);
                $.map(data, function (value, key) {
                    queryString += key + '=' + value + '&';
                });
            }

            queryString = queryString.substr(0, queryString.length - 1);
            if (queryString != '') {
                if (readUrl.indexOf('?') > -1) {
                    readUrl += queryString;
                } else {
                    readUrl += '?' + queryString;
                }
            }
            that._originalReadUrl = that.grid.dataSource.transport.options.read.url;
            that.grid.dataSource.transport.options.read.url = readUrl;
            that.grid.dataSource.read().then(function () {
                that._animateStop(that);
                that.grid.dataSource.page(1);
                that.grid.dataSource.transport.options.read.url = that._originalReadUrl;
            });
        },
        _buttonTemplate: "<a class='k-button popup-search-btn-popup' onclick='return false;' href='#' data-toggle='modal' data-target=''></a>",

        _windowTemplate: function (title, filterRowTemplateId, searchButtonText) {
            return $('<div class="modal fade popup-search-modal" tabindex="-1" role="dialog">' +
                '<div class="modal-dialog modal-lg">' +
                '<div class="modal-content popup-search-modal-content">' +
                '<div class="modal-header popup-search-modal-header">' +
                '<a class="close" data-dismiss="modal" tabindex="-1"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></a>' +
                //'<h4 class="modal-title">' + title + '</h4>' +
                '<strong class="modal-title">' + title + '</strong>' +
                '</div>' +
                '<div class="modal-body form-horizontal">' +
                '<div class="popup-search-fields" data-bind="source: popupSearchFields" data-template="' + filterRowTemplateId + '">' +
                //'<div class="popup-search-fields" data-bind="source: fields">' +
                '</div>' +
                '<div class="row"><div class="col-md-offset-5 col-md-8" style="padding: 0px;">' +
                '<button type="button" class="btn btn-warning popup-search-btn-search popup-search-btn" style="margin:10px 0 10px 0px ">' + searchButtonText + '</button>' +
                '</div></div><hr style="width: 95%;">' +
                '<div class="row">' +
                '<div class="popup-search-result-grid"></div>' +
                '</div>' +
                '</div>' +
                '<div class="modal-footer popup-search-modal-footer">' +
               '<button type="button" class="btn btn-success popup-search-btn popup-search-btn-select "><i class="fa fa-check"></i> Select</button>' +
                '<button type="button" class="btn btn-danger popup-search-btn" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>' +
                 '<button type="button" class="btn btn-default popup-search-btn popup-search-btn-clear" data-dismiss="modal"><i class="fa fa-undo"></i> Clear</button>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>');
        },

        filterRowTemplateId: 'popupSearchFilterRowTemplate',

        filterRowTemplate: function () {
            var hidden = "<div class='row'>" +
                "<div class='form-group col-xs-12 col-md-12 col-sm-12 col-lg-12 '>" +
                
                "#if (typeof lock != 'undefined' && lock) {#" +
                "<div class='col-xs-6 col-md-6 col-sm-7 col-lg-7 form-control-static'>" +
                "<span data-bind='text: value'></span> #if(typeof value=='undefined'||value==null||value==''){#Please select '#= title #' first.#}#" +
                "#} else {#" +
                "<div class='col-xs-6 col-md-6 col-sm-7 col-lg-7'>" +
                "<input class='k-textbox popup-search-filter-textbox' type='hidden' data-bind='value: value' name='#= name #'/>" +
                "#}#" +
                "</div></div></div>";

            var normal = "<div class='row'>" +
                "<div class='form-group col-xs-12 col-md-12 col-sm-12 col-lg-12 '>" +
                "<label class='col-xs-5 col-md-5 col-sm-4 col-lg-4 control-label'>#= title #</label>" +
                "#if (typeof lock != 'undefined' && lock) {#" +
                "<div class='col-xs-6 col-md-6 col-sm-7 col-lg-7 form-control-static'>" +
                "<span data-bind='text: value'></span> #if(typeof value=='undefined'||value==null||value==''){#Please select '#= title #' first.#}#" +
                "#} else {#" +
                "<div class='col-xs-6 col-md-6 col-sm-7 col-lg-7'>" +
                "<input class='k-textbox popup-search-filter-textbox' type='text' data-bind='value: value' name='#= name #'/>" +
                "#}#" +
                "</div></div></div>";

            var result = "#if (typeof hidden !== 'undefined' && hidden) {# " + hidden + " #}else {#" + normal + "#}#";

            return result;
        },

        open: function () {
            this.window.modal('show');
        },
        close: function () {
            this.window.modal('hide');
        },
        toggle: function () {
            this.window.modal('toggle');
        },
        refresh: function () {
            var that = this,
                fields = that.dataSource.popupSearchFields;
            that.trigger(DATABINDING);
            kendo.bind(that.window.find('.popup-search-fields'), fields);
            that._refreshSelectButton(that.grid.select());

            for (var i = 0; i < fields.length; i++) {
                if (fields[i].lock != undefined && fields[i].lock && !that.options.locked) {
                    that.options.locked = true;
                    break;
                }
            }
            that.trigger(DATABOUND);
        },
        items: function () {
            return this.window.find('.popup-search-fields').children();
        },
        fields: function (name) {
            var d = this.dataSource.popupSearchFields;
            if (name == undefined) {
                return d;
            }
            for (var i = 0; i < d.length; i++) {
                if (d[i].name == name) {
                    return d[i];
                }
            }
            return null;
        },
        events: [
            CHANGE,
            ROWCHANGED,
            SHOW,
            SHOWN,
            DATABINDING,
            DATABOUND
        ],
        _disableButton: function ($b, state) {
            $b[0].disable = state;
            $b.toggleClass('disabled', state);
        },
        _refreshSelectButton: function (selected) {
            var that = this;
            if (!that.options.multiple && selected != null && selected.length > 0) {
                that._disableButton(that.selectButton, false);
            } else {
                that._disableButton(that.selectButton, true);
            }
        },
        _resetSearchButton: function (state) {
            var that = this;
            that._disableButton(that.searchButton, state || false);
            that.searchButton.html(that.options.searchButtonText);
        },
        _dataSource: function () {
            this.dataSource = kendo.observable({ popupSearchFields: this.options.popupSearchFields });
        },
        _gridOnChange: function (e) {
            var that = this;
            that._refreshSelectButton(that.grid.select());
            that.trigger(ROWCHANGED, e);
        },
        _gridDataBound: function (e) {
            this._resetSearchButton();
        },
        select: function (selectedRow) {
            var that = this;
            if (that.selectButton.hasClass('disabled'))
                return;
            if (that.options.multiple) {
                that._change(that.getValues());
            } else {
                that._change(that.grid.dataItem(selectedRow));
            }
            that.close();

            if (that.placeholder.parents('div[data-role="window"]').length > 0) {
                that.placeholder.parents('div[data-role="window"]').focus();
            }
        },
        _update: function (value, onclear) {
            var that = this,
                isempty = that._isEmpty(value);

            if (onclear == null) {
                onclear = false;
            }
            that._old = that._value;
            that._value = value;
            if (!isempty) {
                //that.autoComplete.value(value[that.options.keyField]);
                if (that.options.multiple) {
                    var tmp = '';
                    for (var i = 0; i < value.length; i++) {
                        tmp += that.template(value[i]);
                    }
                    that._setTargetHtml(tmp);
                } else {
                    that._setTargetHtml(that.template(value));
                }
                //that.target.show();
                //that.target.focus();
                that.placeholder.hide();
                that.clearButton.show();
                that.element.addClass('popup-search-hasvalue');
            } else {
                //that.autoComplete.value(null);
                that._setTargetHtml('');
                //that.target.hide();
                that.placeholder.show();
                that.clearButton.hide();
                that.element.removeClass('popup-search-hasvalue');
            }
            if (that.hidden) {
                that.hidden.val(value);
                if ((value == null && onclear) || (value != null && !onclear)) {
                    that.hidden.trigger('blur.kendoValidator');
                }
            }
        },
        _setTargetHtml: function (html) {
            var that = this;
            that.target.html(html);
            if (that._readonlyElement != undefined) {
                that._readonlyElement.html(html);
            }
            if (html == '') {
                that.target.hide();
            } else {
                that.target.show();
            }
        },
        _change: function (value, onclear) {
            var that = this;
            if (value != null && value.constructor === Array && !that.options.multiple) {
                value = value[0];
            }
            if (that._value == undefined || that._value != value) {
                that._update(value, onclear);
                that.trigger(CHANGE);
            }
        },
        _isEmpty: function (value) {
            if (value == undefined || value === "") {
                return true;
            } else {
                //for (prop in value) {
                //    if (value.hasOwnProperty(prop) && value[prop] == null) {
                //        return true;
                //    }
                //}
            }
            return false;
        },
        _blur: function () {
            var that = this;
            that._change(that._value);
        },
        value: function (value) {
            var that = this;

            if (typeof value === "undefined") {
                return that._value;
            }
            that._update(value);
            that._old = that._value;
            //that._change(value);
            return that._value;
        },
        clear: function () {
            var that = this;
            that._change(null, true);
        },
        dataItem: function () {
            return this.$angular_scope.dataItem;
        },
        getValues: function () {
            var that = this,
                array = that.valueArray;
            var tmp = [];
            for (var i = 0; i < array.length; i++) {
                tmp.push(that.grid.dataItem(that.grid.tbody.find('tr[data-uid="' + array[i] + '"]')));
            }
            return tmp;
        },
        _setReadonly: function (r) {
            var that = this;
            if (r == undefined) {
                return that.options.readonly;
            }
            if (r === true || r === 'readonly') {
                if (that._readonlyElement == undefined) {
                    that._readonlyElement = $('<div class="popup-search-readonly"></div>').html(that.target.html());
                } else {
                    that._readonlyElement.html(that.target.html());
                }
                that.element.hide().parent().append(that._readonlyElement);
            } else {
                if (that._readonlyElement != null) {
                    that._readonlyElement.remove();
                    that.element.show();
                }
            }
        }
    });

    ui.plugin(PopupSearch);

})(jQuery);
