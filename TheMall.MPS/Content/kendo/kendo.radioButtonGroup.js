﻿/* Radio Button Group Widget */

/// <summary>Kendo UI Radio Button Group Widget.</summary>
/// <description>Kendo UI  widget that displays a radio button group.</description>
/// <version>1.0</version>
/// <author>John DeVight</author>
/// <license>
/// Licensed under the MIT License (MIT)
/// You may obtain a copy of the License at
/// http://opensource.org/licenses/mit-license.html
/// </license>
(function ($, kendo) {
    var ns = ".kendoRadioButtonGroup",
        CHANGE = "change",
        DATABOUND = "dataBound",
        DISABLED = "disabled",
        READONLY = "readonly",
        DEFAULT = "k-state-default",
        //FOCUSED = "k-state-focused",
        SELECTED = "k-state-selected",
        STATEDISABLED = "k-state-disabled",
        ARIA_DISABLED = "aria-disabled",
        ARIA_READONLY = "aria-readonly";

    var radioButtonGroup = kendo.ui.Widget.extend({
        /// <summary>Displays radio buttons from a kendo.data.DataSource.</summary>

        options: {
            // <summary>The data source of the widget which is used to display a list of radio buttons.</summary>
            dataSource: null,

            /// <summary>The field of the data item that provides the text content of the radio buttons.</summary>
            dataTextField: "",

            /// <summary>The field of the data item that provides the value of the widget.</summary>
            dataValueField: "",

            /// <summary>The name given to the radio button group.</summary>
            groupName: "",

            /// <summary>Name of the widget.</summary>
            name: "RadioButtonGroup",

            /// <summary>Specifies the orientation of the widget. Supported values are "horizontal" and "vertical".</summary>
            orientation: "vertical",

            readonly: false
        },

        events: [
            /// <summary>Fired when the user selects a radio button.</summary>
            CHANGE,

            /// <summary>Fired when the widget is bound to data from its data source.</summary>
            DATABOUND
        ],

        /// <summary>Data source for the widget.</summary>
        dataSource: null,

        init: function (element, options) {
            /// <summary>Initialize the widget.</summary>

            kendo.ui.Widget.fn.init.call(this, element, options);

            options.readonly = options.readonly || (options.readonly != undefined && options.readonly.toLowerCase && options.readonly.toLowerCase() === "readonly");

            this._dataSource();

            // Read the data from the data source.
            this.dataSource.fetch();

            this.element.addClass('k-radio-button-group');

            // Attach an event handler to the selection of a radio button.
            this.element.on("click" + ns, ".k-radio-label", { sender: this }, this._onRadioButtonSelected);

            this.element.css({ "display": "inline-block" });
        },

        destroy: function () {
            /// <summary>Destroy the widget.</summary>

            $(this.element).off(ns);

            kendo.ui.Widget.fn.destroy.call(this);
        },

        _dataSource: function () {
            /// <summary>Initialize the data source.</summary>

            var dataSource = this.options.dataSource;

            // If the data source is an array, then define an object and set the array to the data attribute.
            dataSource = $.isArray(dataSource) ? { data: dataSource } : dataSource;

            // If there is a data source defined already. 
            if (this.dataSource && this._refreshHandler) {
                // Unbind from the change event.
                this.dataSource.unbind(CHANGE, this._refreshHandler);
            } else {
                // Create the refresh event handler for the data source change event.
                this._refreshHandler = $.proxy(this.refresh, this);
            }

            // Initialize the data source.
            this.dataSource = kendo.data.DataSource.create(dataSource).bind(CHANGE, this._refreshHandler);
        },

        _template: function () {
            /// <summary>Get the template for a radio button.</summary>

            var html = kendo.format("<div class='k-radio-button-group-item' data-uid='#: uid #' data-value='#: {1} #' data-text='#: {2} #' style='margin: 5px 10px 5px 0px;display:{3};'><input type='radio' name='{0}' value='#: {1} #' class='k-radio' /><span class='k-radio-label'>#: {2} #</span></div>",
                this.options.groupName.length === 0 ? kendo.guid() : this.options.groupName,
                this.options.dataValueField, this.options.dataTextField,
                this.options.orientation === "vertical" ? "block" : "inline-block");

            return kendo.template(html);
        },

        _onRadioButtonSelected: function (e) {
            /// <summary>Handle the selection of a radio button.</summary>

            var $target = $(this),
                that = e.data.sender;

            if (that.options.readonly) {
                return false;
            }

            that.element.find(".k-radio").prop("checked", false).removeClass(SELECTED).addClass(DEFAULT);

            $target.prev(".k-radio").prop("checked", true).removeClass(DEFAULT).addClass(SELECTED);

            var dataItem = that.dataItem();

            that.trigger(CHANGE, { dataItem: dataItem });

            return true;
        },

        //_enable: function () {
        //    var that = this,
        //        options = that.options,
        //        disabled = that.element.is("[disabled]");

        //    if (options.enable !== undefined) {
        //        options.enabled = options.enable;
        //    }

        //    if (!options.enabled || disabled) {
        //        that.enable(false);
        //    } else {
        //        that.readonly(that.element.is("[readonly]"));
        //    }
        //},

        _editable: function (options) {
            var that = this,
                element = that.element,
                radio = element.find('input[type="radio"]'),
                readonly = options.readonly,
                disable = options.disable;

            if (!readonly && !disable) {

                element.addClass(DEFAULT)
                       .removeClass(STATEDISABLED);

                radio.removeAttr(DISABLED)
                      .removeAttr(READONLY)
                      .attr(ARIA_DISABLED, false)
                      .attr(ARIA_READONLY, false);
            } else {

                element.addClass(disable ? STATEDISABLED : DEFAULT)
                       .removeClass(disable ? DEFAULT : STATEDISABLED);

                radio.attr(DISABLED, disable)
                     .attr(READONLY, readonly)
                     .attr(ARIA_DISABLED, disable)
                     .attr(ARIA_READONLY, readonly);
            }
        },

        setDataSource: function (dataSource) {
            /// <summary>Sets the data source of the widget.</summary>

            this.options.dataSource = dataSource;
            this._dataSource();
            this.dataSource.fetch();
        },

        refresh: function (e) {
            /// <summary>Renders all radio buttons using the current data items.</summary>

            var template = this._template();

            // Remove all the existing items.
            this.element.empty();

            // Add each of the radio buttons.
            for (var idx = 0; idx < e.items.length; idx++) {
                this.element.append(template(e.items[idx]));
            }

            // Fire the dataBound event.
            this.trigger(DATABOUND);
        },

        dataItem: function () {
            /// <summary>Gets the dataItem for the selected radio button.</summary>

            var uid = this.element.find(".k-radio:checked").closest(".k-radio-button-group-item").attr("data-uid");

            return this.dataSource.getByUid(uid);
        },

        text: function () {
            /// <summary>Gets or sets the text of the radio button group.</summary>

            if (arguments.length === 0) {
                return this.element.find('.'+SELECTED).closest(".k-radio-button-group-item").attr("data-text");
            } else {
                this.element.find(kendo.format(".k-radio-button-group-item[data-text='{0}']", arguments[0])).find(".k-radio-label").click();
            }
        },

        value: function () {
            /// <summary>Gets or sets the value of the radio button group.</summary>

            if (arguments.length === 0) {
                return this.element.find('.' + SELECTED).closest(".k-radio-button-group-item").attr("data-value");
            } else {
                this.element.find(kendo.format(".k-radio-button-group-item[data-value='{0}']", arguments[0])).find(".k-radio-label").click();

                this.element.find(".k-radio").prop("checked", false).removeClass(SELECTED).addClass(DEFAULT);

                this.element.find(kendo.format(".k-radio-button-group-item[data-value='{0}']", arguments[0])).find(".k-radio-label").prev(".k-radio").prop("checked", true).removeClass(DEFAULT).addClass(SELECTED);
            }
        },

        //readonly: function (readonly) {
        //// Not working at the moment
        //    this._editable({
        //        readonly: readonly === undefined ? true : readonly,
        //        disable: false
        //    });
        //},

        //enable: function (enable) {
        //    // Not working at the moment
        //    this._editable({
        //        readonly: false,
        //        disable: !(enable = enable === undefined ? true : enable)
        //    });
        //}
    });
    kendo.ui.plugin(radioButtonGroup);
})(window.kendo.jQuery, window.kendo);