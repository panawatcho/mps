﻿//http://docs.telerik.com/KENDO-UI/aspnet-mvc/helpers/datepicker/how-to/masked-date-picker-grid
(function ($) {
    var kendo = window.kendo,
        ui = kendo.ui,
        Widget = ui.Widget,
        proxy = $.proxy,
        CHANGE = "change",
        PROGRESS = "progress",
        ERROR = "error",
        NS = ".generalInfo";

    var _maskedTextboxChange = function (e) {
        var that = this;
        //console.log(that.element.data("kendoDatePicker").value()); // Not working ATM
    };

    var MaskedDatePicker = Widget.extend({
        init: function (element, options) {
            var that = this;
            Widget.fn.init.call(this, element, options);
            that.options.maskOptions = that.options.maskOptions || {};
            that.options.maskOptions.mask = that.options.maskOptions.mask || "00/00/0000";
            that.options.dateOptions = that.options.dateOptions || {};
            $(element).kendoMaskedTextBox(that.options.maskOptions)
            //.kendoDatePicker({
            //    format: that.options.dateOptions.format || "MM/dd/yyyy",
            //    parseFormats: that.options.dateOptions.parseFormats || ["MM/dd/yyyy", "MM/dd/yy"]
            //})
            .kendoDatePicker(that.options.dateOptions)
            .closest(".k-datepicker")
            .add(element)
            .removeClass("k-textbox");

            that.element.data("kendoDatePicker").bind("change", function () {
                that.trigger(CHANGE);
            });
            that.element.data("kendoMaskedTextBox").bind("change", function (e) {
                _maskedTextboxChange.call(that, e);
            });
        },
        options: {
            name: "MaskedDatePicker",
            dateOptions: {}
        },
        events: [
          CHANGE
        ],
        destroy: function () {
            var that = this;
            Widget.fn.destroy.call(that);

            kendo.destroy(that.element);
        },
        value: function (value) {
            var datepicker = this.element.data("kendoDatePicker");

            if (value === undefined) {
                return datepicker.value();
            }

            datepicker.value(value);
        }
    });

    ui.plugin(MaskedDatePicker);

})(window.kendo.jQuery);