﻿"use strict";
angular.module("app.values", []);
angular.module("app.services", []);
angular.module("app.filters", []);
angular.module("app.directives", []);
angular.module("app.datasources", []);
angular.module("app.controllers", []);
angular.module("app.constants", []);