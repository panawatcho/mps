﻿"use strict";
var app = angular.module("app", [
        "ngRoute",
        "ngResource",
        "ngAnimate",
        "ngCookies",
        "ngSanitize",
        "blockUI",
        "angular-loading-bar",
        "kendo.directives",
        "gettext",
        //"blueimp.fileupload",
        "ui.bootstrap",
        "ngFileUpload",
        "app.values",
        "app.services",
        "app.filters",
        "app.directives",
        "app.datasources",
        "app.controllers",
        "app.constants",
         "19degrees.ngSweetAlert2"
])
    .config([
        "$httpProvider", "$locationProvider", "$routeProvider", "blockUIConfig",
        function ($httpProvider, $locationProvider, $routeProvider, blockUiConfig) {
            $httpProvider.interceptors.push("authInterceptorService");

            $locationProvider.caseInsensitiveMatch = true;
            $locationProvider.html5Mode(true);

            var REJECT = 'REJECT',
                SUBMIT = 'SUBMIT',
                CREATE = 'CREATE',
                DETAIL = 'DETAIL',
                APPROVE = 'APPROVE',
                REVISE = 'REVISE';

            $routeProvider
               .when("/", {
                   templateUrl: function ($routeParams) {
                       return "app/workspace/workspace";
                   }
               })
                .when("/home", {
                    templateUrl: function ($routeParams) {
                        return "app/workspace/workspace";
                    }
                })
                 .when("/workspace", {
                     templateUrl: function ($routeParams) {
                         return "app/workspace/workspace";
                     }
                 })
                .when("/login", {
                    templateUrl: "views/home/login.html"
                })
                 .when("/loginauthen", {
                     templateUrl: "views/loginauthen/LoginAuthen.html"
                 })

                .when('/:page/', {
                    templateUrl: function ($routeParams) {
                        //if ($routeParams.page.toUpperCase() === 'HOME') {
                        //    return "app/home/index";
                        //}
                        return "app/" + $routeParams.page + '/index';

                        // return "views/" + $routeParams.page + '/index.html';
                    }
                })
                 .when("/:page/:mode/", {
                     templateUrl: function ($routeParams) {
                         var page = $routeParams.page.toUpperCase(),
                             mode = $routeParams.mode.toUpperCase();
                         if (page === "PROPOSAL") {
                             if (mode === "OWNER-DEPOSIT") {
                                 return "app/" + $routeParams.page + "/detail/";
                             }
                             else if (mode === "APPROVE-LV1") {
                                 return "app/" + $routeParams.page + "/detail/";
                             }
                             else if (mode === "APPROVE-LV2") {
                                 return "app/" + $routeParams.page + "/detail/";
                             }
                             else if (mode === "APPROVE-LV3") {
                                 return "app/" + $routeParams.page + "/detail/";
                             }
                             else if (mode === "APPROVE-LV4") {
                                 return "app/" + $routeParams.page + "/detail/";
                             }
                             else if (mode === "APPROVE-DEPOSIT-OWNER") {
                                 return "app/" + $routeParams.page + "/detail/";
                             }
                             else if (mode === "REVISE-DEPOSIT") {
                                 return "app/" + $routeParams.page + "/ReviseIncomeDeposit/";
                             }
                             else if (mode === "ACC-APPROVE") {
                                 return "app/" + $routeParams.page + "/detail/";
                             }
                             else if (mode === REVISE) {
                                 return "app/" + $routeParams.page + "/form/";
                             }
                             else if (mode === CREATE) {
                                 return "app/" + $routeParams.page + "/form";
                             }
                             else if (mode === "REVISEPROPOSAL") {
                                 return "app/" + $routeParams.page + "/ReviseProposal/";
                             }
                         }
                         if (page === "MEMO") {
                             if (mode === "APPROVE-LV1") {
                                 return "app/" + $routeParams.page + "/detail/";
                             }
                             else if (mode === "APPROVE-LV2") {
                                 return "app/" + $routeParams.page + "/detail/";
                             }
                             else if (mode === "APPROVE-LV3") {
                                 return "app/" + $routeParams.page + "/detail/";
                             }
                             else if (mode === "APPROVE-LV4") {
                                 return "app/" + $routeParams.page + "/detail/";
                             }
                                              
                             else if (mode === REVISE) {
                                 return "app/" + $routeParams.page + "/form/";
                             }
                             else if (mode === CREATE) {
                                 return "app/" + $routeParams.page + "/form";
                             }
                         }
                         if (page === "CASHADVANCE") {
                             if (mode === "APPROVE-LV1") {
                                 return "app/" + $routeParams.page + "/detail/";
                             }
                             else if (mode === "APPROVE-LV2") {
                                 return "app/" + $routeParams.page + "/detail/";
                             }
                             else if (mode === "APPROVE-LV3") {
                                 return "app/" + $routeParams.page + "/detail/";
                             }
                             else if (mode === "APPROVE-LV4") {
                                 return "app/" + $routeParams.page + "/detail/";
                             }
                             else if (mode === "ACC-APPROVE") {
                                 return "app/" + $routeParams.page + "/detail/";
                             }
                             else if (mode === REVISE) {
                                 return "app/" + $routeParams.page + "/form/";
                             }
                             else if (mode === CREATE) {
                                 return "app/" + $routeParams.page + "/form";
                             }
                         }
                         if (page === "CASHCLEARING") {
                             if (mode === "APPROVE-LV1") {
                                 return "app/" + $routeParams.page + "/detail/";
                             }
                             else if (mode === "APPROVE-LV2") {
                                 return "app/" + $routeParams.page + "/detail/";
                             }
                             else if (mode === "APPROVE-LV3") {
                                 return "app/" + $routeParams.page + "/detail/";
                             }
                             else if (mode === "APPROVE-LV4") {
                                 return "app/" + $routeParams.page + "/detail/";
                             }
                             else if (mode === "ACC-APPROVE") {
                                 return "app/" + $routeParams.page + "/detail/";
                             }
                             else if (mode === REVISE) {
                                 return "app/" + $routeParams.page + "/form/";
                             }
                             else if (mode === CREATE) {
                                 return "app/" + $routeParams.page + "/form";
                             }
                         }
                         if (page === "PETTYCASH") {
                             if (mode === "APPROVE-LV1") {
                                 return "app/" + $routeParams.page + "/detail/";
                             }
                             else if (mode === "APPROVE-LV2") {
                                 return "app/" + $routeParams.page + "/detail/";
                             }
                             else if (mode === "APPROVE-LV3") {
                                 return "app/" + $routeParams.page + "/detail/";
                             }
                             else if (mode === "APPROVE-LV4") {
                                 return "app/" + $routeParams.page + "/detail/";
                             }
                             else if (mode === "ACC-APPROVE") {
                                 return "app/" + $routeParams.page + "/detail/";
                             }
                             else if (mode === REVISE) {
                                 return "app/" + $routeParams.page + "/form/";
                             }
                             else if (mode === CREATE) {
                                 return "app/" + $routeParams.page + "/form";
                             }
                           
                         }
                         if (page === "GOODSISSUE") {
                              if (mode === CREATE) {
                                  return "app/PETTYCASH/form";
                              }
                              if (mode === DETAIL) {
                                  return "app/PETTYCASH/detail";
                              }
                         }
                         if (page === "MEMOINCOME") {
                             if (mode === "APPROVE-LV1") {
                                 return "app/" + $routeParams.page + "/detail/";
                             }
                             else if (mode === "APPROVE-LV2") {
                                 return "app/" + $routeParams.page + "/detail/";
                             }
                             else if (mode === "APPROVE-LV3") {
                                 return "app/" + $routeParams.page + "/detail/";
                             }
                             else if (mode === "APPROVE-LV4") {
                                 return "app/" + $routeParams.page + "/detail/";
                             }
                             else if (mode === "ACC-ONPROCESS") {
                                 return "app/" + $routeParams.page + "/detail/";
                             }
                             else if (mode === "ACC-APPROVE") {
                                 return "app/" + $routeParams.page + "/detail/";
                             }
                             else if (mode === REVISE) {
                                 return "app/" + $routeParams.page + "/form/";
                             }
                             else if (mode === CREATE) {
                                 return "app/" + $routeParams.page + "/form";
                             }
                             //else if (mode === "ACCOUNTINDEX") {
                             //    return "app/" + $routeParams.page + "/accountindex/";
                             //}
                             else {
                                 return "app/" + $routeParams.page + "/" + $routeParams.mode;
                             }
                         }
                         else {

                             if (mode === CREATE || mode === REVISE) {
                                 return "app/" + $routeParams.page + "/form/";
                             } else if (mode === SUBMIT || mode === APPROVE) {
                                 return "app/" + $routeParams.page + "/detail/";
                             }
                             return "app/" + $routeParams.page + "/" + $routeParams.mode;
                         }

                     }
                 })
                .when('/:page/:mode/:id?', {
                    templateUrl: function ($routeParams) {
                        var page = $routeParams.page.toUpperCase(),
                            mode = $routeParams.mode.toUpperCase();
                        if (page === "GOODSISSUE") {
                            if (mode === CREATE) {
                                return "app/PETTYCASH/form";
                            }
                            if (mode === DETAIL) {
                                return "app/PETTYCASH/detail";
                            }
                        }
                        if (mode === REJECT || mode === CREATE || mode === SUBMIT) {
                            return "app/" + $routeParams.page + '/form/';
                            // return "views/" + $routeParams.page + '/form.html';
                        } else if (mode === DETAIL) {
                            return "app/" + $routeParams.page + '/detail/';
                            // return "views/" + $routeParams.page + '/detail.html';
                        } else {
                            return "app/" + $routeParams.page + "/" + $routeParams.mode;
                            //return "views/" + $routeParams.page + "/" + $routeParams.mode + ".html";
                        }

                    }
                })
                .otherwise({
                    redirectTo: "/"
                });

            blockUiConfig.message = 'Please wait';
            blockUiConfig.template = '<div class=\"block-ui-overlay\"></div><div class=\"block-ui-message-container\" aria-live=\"assertive\" aria-atomic=\"true\"><div class=\"block-ui-message\" ng-class=\"$_blockUiMessageClass\"><i class=\"fa fa-spinner fa-spin\"></i> <i>{{ state.message }}</i></div></div>';
            blockUiConfig.delay = 100;
            blockUiConfig.requestFilter = function (config) {
                // If the request starts with '/api/rest/documenttype/process' ...
                if (/^\api\/rest\/documenttype\/process($|\/).*/.test(config.url)
                    || /\/goa$/.test(config.url)
                    || /^\api\/rest\/vendtax($|\/).*/.test(config.url)) {
                    return false;
                }
            };
        }
    ])
    .run([
        "$rootScope", "authService", "messageBox", "gettextCatalog", "$route", "$location", "$http", "$log",
        function ($rootScope, authService, messageBox, gettextCatalog, $route, $location, $http, $log) {
            //gettextCatalog.setCurrentLanguage('th');
            //gettextCatalog.loadRemote("languages/" + 'th' + ".json");
            gettextCatalog.debug = true;

            var original = $location.path;
            $location.path = function (path, reload) {
                if (reload === false) {
                    var lastRoute = $route.current;
                    var un = $rootScope.$on('$locationChangeSuccess', function () {
                        $route.current = lastRoute;
                        un();
                    });
                }
                return original.apply($location, [path]);
            };
            $rootScope.authentication = authService.authentication;
            $rootScope.$on("$locationChangeStart", function () {
                authService.fillAuthData();
            });
            $http.defaults.headers.common.Authorization = authService.bearer;
            $http.defaults.headers.common["X-Referer"] = authService.xReferer;
            $(function () {
                $(document).ajaxSend(function (e, xhr) {
                    authService.setAuthHeader(xhr);
                });
                $(document).on('click', 'a[href="#"]', function () { return false; });
                $(document).on("focus", "[data-role=numerictextbox]", function () {
                    var element = this;
                    setTimeout(function () {
                        $(element).select();
                    });
                });
            });

            //$log.info("Current language is '" + gettextCatalog.getCurrentLanguage() + "'");
        }
    ]);