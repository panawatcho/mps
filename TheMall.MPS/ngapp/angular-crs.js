﻿(function (window, angular, undefined) {
    'use strict';

    function encodeUriQuery(val, pctEncodeSpaces) {
        return encodeURIComponent(val).
            replace(/%40/gi, '@').
            replace(/%3A/gi, ':').
            replace(/%24/g, '$').
            replace(/%2C/gi, ',').
            replace(/%20/g, (pctEncodeSpaces ? '%20' : '+'));
    }

    function encodeUriSegment(val) {
        return encodeUriQuery(val, true).
            replace(/%26/gi, '&').
            replace(/%3D/gi, '=').
            replace(/%2B/gi, '+');
    }

    var interpoleUrl = function (url, params) {
        var encodedVal, val;
        var urlParams = self.urlParams = {};
        angular.forEach(url.split(/\W/), function (param) {
            if (param === 'hasOwnProperty') {
                //throw $resourceMinErr('badname', "hasOwnProperty is not a valid parameter name.");
            }
            if (!(new RegExp("^\\d+$").test(param)) && param &&
            (new RegExp("(^|[^\\\\]):" + param + "(\\W|$)").test(url))) {
                urlParams[param] = true;
            }
        });
        url = url.replace(/\\:/g, ':');
        url = url.replace(/^https?:\/\/[^\/]*/, function (match) {
            //protocolAndDomain = match;
            return '';
        });
        angular.forEach(urlParams, function (_, urlParam) {
            val = params.hasOwnProperty(urlParam) ? params[urlParam] : self.defaults[urlParam];
            if (angular.isDefined(val) && val !== null) {
                encodedVal = encodeUriSegment(val);
                url = url.replace(new RegExp(":" + urlParam + "(\\W|$)", "g"), function (match, p1) {
                    return encodedVal + p1;
                });
            } else {
                url = url.replace(new RegExp("(\/?):" + urlParam + "(\\W|$)", "g"), function (match,
                    leadingSlashes, tail) {
                    if (tail.charAt(0) == '/') {
                        return tail;
                    } else {
                        return leadingSlashes + tail;
                    }
                });
            }
        });

        return url;
    }
    //Deployment
     //var apiUrl = '//api.mps.local';
     var apiUrl = '//10.90.234.41/mps.api';
    //var apiUrl = '//10.90.240.24/mps.api';
    var url = {
        web: function (u) {
            var location = window.location;
            if (!location.origin) {
                location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
            }
            var fullUrl = location.origin + "/mps/app";
            if (u) {
                return fullUrl + "/" + u;
            }
            return fullUrl;
        },
        webApi: function(u) {
            var fullUrl = apiUrl + "/api";
            if (u) {
                return fullUrl + "/" + u;
            }
            return fullUrl;
        },
        odata: function (u) {
            var fullUrl = apiUrl + "/odata";
            if (u) {
                return fullUrl + "/" + u;
            }
            return fullUrl;
        },
        rootweb: function (u) {
            var location = window.location;
            if (!location.origin) {
                location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
            }
            var fullUrl = location.origin + "/mps";
            if (u) {
                return fullUrl + "/" + u;
            }
            return fullUrl;
        },
    }
    var crs = {
        interpoleUrl: interpoleUrl,
        url: url
    };
    angular.crs = crs;
})(window, window.angular);