﻿angular.module("app").config(['$provide', function ($provide) {
    $provide.decorator('$resource', ['$delegate', function ($delegate) {
        var decorator = function (url, paramDefaults, actions) {
            var resource = $delegate(url, paramDefaults, actions);
            resource.$url = url;
            return resource;
        };
        return decorator;
    }]);
}]);
