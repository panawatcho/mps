﻿"use strict";
angular.module("app.controllers")
    .controller("NavigationController",
        [
            "$scope", "authService", "menuService",
            function ($scope, authService, menuService) {
                $scope.logout = authService.logout;
                $scope.menus = menuService.getMenu;
            }
        ]
    )
    .controller("LicenseController",
        [
            "$scope", "projectName", "customerName",
            function ($scope, projectName, customerName) {
                $scope.projectName = projectName;
                $scope.customerName = customerName;
            }
        ]
    )
    .controller("NotificationController",
        [
            "$scope", "messageBox",
            function ($scope, messageBox) {
                if (!messageBox.instances || !messageBox.instances.length) {
                    $scope.instances = [];
                    messageBox.instances = $scope.instances;
                }
                $scope.messageBox = messageBox;
            }
        ]
    )
    .controller("I18nController",
        [
            "$scope", "langService", "gettextCatalog",
            function ($scope, langService, gettextCatalog) {
                // DON'T DO THIS
                $scope.helloWorld = gettextCatalog.getString("Hello world");
                $scope.helloWorld2 = $scope.helloWorld;

                $scope.switchLanguage = function (lang) {
                    langService.switchLanguage(lang).then(function () {
                        $scope.helloWorld2 = gettextCatalog.getString("Hello world");
                    });
                };
            }
        ]
    )
    .controller("AccountCtrl",
        [
            "$scope", "$location", "authService", "messageBox", "langService",
            function ($scope, $location, authService, messageBox, langService) {
                if (authService.isAuth()) {
                    $location.path('/workspace/');
                }
                $scope.switchLanguage = langService.switchLanguage;
                $scope.currentLanguage = langService.currentLanguage;
                $scope.form = {};

                $scope.login = function () {
                    authService.login({
                        userName: $scope.form.userName,
                        password: $scope.form.password
                    }).then(function () {
                        var qs = $location.search();
                        if (qs && qs.redirect) {
                            $location.url(qs.redirect);
                        } else {
                            $location.path('/');
                        }
                    }, function (resp) {
                        messageBox.error(resp.error_description);
                    });
                };
                $scope.logout = function () {
                    authService.logout();
                };
            }
        ]
    )
    //todo DELETE ME
    .controller("ExampleUploadController",
    [
        "$scope", "$q", "$http", "uploadService", "exampleDataSource", "gettextCatalog", "lineGrid",
        function ($scope, $q, $http, uploadService, dataSource, gettextCatalog, lineGrid) {
            $scope.dataSource = dataSource;
            $scope.onUploadChange = uploadService.onUploadChange;
            $scope._documentTypeFiles = {};
            //$scope.formData = {};

            $scope.load = function () {
                dataSource.resource.get({ id: 1 }).$promise.then(function(resp) {
                    $scope.formData = resp;
                    //$scope.formData.Lines = new kendo.data.ObservableArray($scope.formData.Lines || []);
                    return resp;
                });
                //.then(function () {
                //    $http.get(angular.crs.interpoleUrl(dataSource.url.loadAttachment, { id: 1 })).then(
                //        function (resp) {
                //            $scope.attachments = resp.data;
                //            return resp;
                //        }
                //    );
                //});

                //$scope.$watch("formData", function (newValue) {
                //    if (newValue) {
                //        $scope.formData.Lines = new kendo.data.ObservableArray(newValue.Lines || []);
                //        $scope.gridOptions();
                //    }
                //});
            }
            $scope.load();
            var _gridOptions;
            $scope.gridOptions = function () {
                if (!$scope.formData) {
                    return undefined;
                }
                if (_gridOptions) {
                    return _gridOptions;
                }
                $scope.formData.Lines = new kendo.data.ObservableArray($scope.formData.Lines || []);
                _gridOptions = {
                    dataSource: new kendo.data.DataSource({
                        data: $scope.formData.Lines,
                        schema: {
                            model: {
                                id: "LineNo",
                                fields: {
                                    Id: { type: "number" },
                                    LineNo: {
                                        type: "number"
                                    },
                                    Name: { type: "string" }
                                }
                            }
                        }
                    }),
                    columns: [
                        { command: ["edit", "destroy"], title: "&nbsp;", width: "250px" },
                        {
                            field: "Name",
                            title: "Name",
                            width: "120px"
                        }
                    ],
                    editable: {
                        mode: "popup",
                        template: kendo.template($('#editorTemplate').html()),
                        createAt: 'bottom'
                    },
                    toolbar: [
                        //{ name: "create", text: gettextCatalog.getString("Add new record") },
                        "create"
                    ],
                    edit: function (e) {
                        $(e.container).parent().css({
                            width: window.innerWidth - 40,
                            left: 20
                        });

                        if (e.model.isNew()) {
                            var w = $(e.container).getKendoWindow();
                            if (w) {
                                w.title('Add');
                            }
                            e.model.set("LineNo", lineGrid.nextLineNo($scope.formData.Lines));
                        }
                    }
                }

                return _gridOptions;
            }

            var uploadFiles = function (url, files, attachments, data) {
                //var url = angular.crs.interpoleUrl(url, { id: $scope.formData.Id });
                var promises = uploadService.uploadFiles(url, files, data);
                for (var i = 0; i < promises.length; i++) {
                    promises[i].then(
                        function (resp) {
                            attachments.push(resp.data[0]);
                        }
                    );
                }
                return promises;
            };

            $scope.submit = function () {
                if ($scope.validator.validate()) {//) && uploadService.validate($scope.validator)) {
                    dataSource.resource.save({ id: $scope.formData.Id }, $scope.formData).$promise.then(
                        function (resp) {
                            $scope.formData = resp;
                            var promises = uploadFiles(angular.crs.interpoleUrl(dataSource.url.saveAttachment, { id: $scope.formData.Id }),
                                $scope._files,
                                $scope.formData.Attachments);
                            $q.all(promises).then(function () {
                                for (var documentTypeFile in $scope._documentTypeFiles) {
                                    if ($scope._documentTypeFiles.hasOwnProperty(documentTypeFile)) {
                                        uploadFiles(angular.crs.interpoleUrl(dataSource.url.saveAttachment, { id: $scope.formData.Id }),
                                            $scope._documentTypeFiles[documentTypeFile],
                                            $scope.formData.Attachments,
                                            { documenttype: documentTypeFile });
                                    }
                                }
                            })
                            .then(function () {
                                var lines = _gridOptions.dataSource.data();
                                for (var i = 0; i < lines.length; i++) {
                                    var lineId = 0;
                                    for (var j = 0; j < resp.Lines.length; j++) {
                                        if (resp.Lines[j].LineNo === lines[i].LineNo) {
                                            lineId = resp.Lines[j].Id;
                                            break;
                                        }
                                    }
                                    if (lineId > 0) {
                                        lines[i].Attachments = lines[i].Attachments || [];
                                        uploadFiles(angular.crs.interpoleUrl(dataSource.url.saveLineAttachment, { id: lineId }),
                                            lines[i]._files,
                                            lines[i].Attachments);
                                    }
                                }
                            });
                        });
                }
                //if ($scope.form.fileControl.$valid && $scope.files) {
                //    uploadFiles($scope.files, $scope.titles);
                //}
            };
            //$scope.onDelete = function (files, attachments) {
            //    return function (file) {
            //        uploadService.delete(file)
            //        .then(
            //            function (resp) {
            //                if (resp) {
            //                    attachments.splice(attachments.indexOf(file), 1);
            //                } else {
            //                    files.splice(files.indexOf(file), 1);
            //                }
            //            }
            //        );
            //    }
            //};
            //$scope.onDelete = function(file) {
            //    uploadService.delete(file)
            //    .then(
            //        function (resp) {
            //            if (resp) {
            //                attachments.splice(attachments.indexOf(file), 1);
            //            } else {
            //                files.splice(files.indexOf(file), 1);
            //            }
            //        }
            //    );
            //};

            $scope.save = function () {
                var deferred = $q.defer();
                deferred.promise.then(function (resp) {
                    $scope.formData.Id = resp.Id;
                });
                deferred.resolve({ Id: 1 });

            };
        }
    ]
);