﻿'use strict';
angular.module('app.directives')
.directive('appUpload', [
    'uploadHandler',
    function (uploadHandler) {
        return {
            restrict: "E",
            require: 'ngModel',
            scope: {
                name: "@",
                ngModel: "=",
                saveUrl: "=",
                removeUrl: "=",
                handler: "=",
                //widget: "=",
                activity: "=",
                duplicate: "@"
            },
            controller: [
                '$scope', 'messageBox', 'uploadChecker', 'authService', function ($scope, messageBox, uploadChecker, authService) {
                    $scope.init = false;
                    var _template = "<span class='k-progress'></span>\
                                    <span class='k-icon'></span>\
                                    <span class='k-filename' title='#=name#'>#=name#</span>";
                    _template += "<label>รายละเอียดเพิ่มเติม</label>&nbsp;&nbsp;&nbsp;<input type='text' class='k-textbox k-upload-filedescription' name='filedescription' maxlength='300' value='#if(files[0].title!=undefined){# #=files[0].title# #}#'/>";
                    _template += "<strong class='k-upload-status'><button class='k-upload-action' type='button'></button></strong>";
                    var remUrl = $scope.removeUrl == null ? $scope.saveUrl : $scope.removeUrl;
                    //debugger;
                    $scope.uploadOption = {
                        async: {
                            saveUrl: $scope.saveUrl,
                            removeUrl: remUrl,
                            removeVerb: "DELETE",
                            autoUpload: false
                        },
                        template: _template,
                        //files: $scope.ngModel,
                        multiple: true,
                        select: function (e) {
                            setTimeout(function () { $(".k-upload-selected").hide(); }, 1);
                            var n = e.files[0].name, err = uploadChecker.validate(n, e.files[0]);
                            if (!err) {
                                //debugger;
                                if (!$scope.duplicate) {
                                    for (var i = 0; i < $scope.ngModel.length; i++) {
                                        if ($scope.ngModel[i].name == n) {
                                            messageBox.error("Cannot upload file with duplicate name '" + n + "'");
                                            e.preventDefault();
                                            return false;
                                        }
                                    }
                                }
                               
                                $scope.ngModel.push({ name: e.files[0].name, size: e.files[0].size, extension: e.files[0].extension });

                                if ($scope.handler.onSelect != undefined) {
                                    $scope.handler.onSelect(e);
                                }
                            } else {
                                messageBox.error(err);
                                e.preventDefault();
                                return false;
                            }
                        },
                        remove: function (e) {
                            $('li.k-file[data-uid="' + e.files[0].uid + '"]').find('.k-filename').prepend('<b>Removing ... </b>');
                            $('li.k-file[data-uid="' + e.files[0].uid + '"]').find('.k-button.k-upload-action').attr('disabled', 'disabled');
                            $('li.k-file[data-uid="' + e.files[0].uid + '"]').find('.k-button.k-upload-action > .k-icon').removeClass('k-i-close').addClass('k-i-clock');

                            var del = e.files[0];
                            delete del.uid;
                            $scope.ngModel.splice($scope.ngModel.indexOf(del), 1);
                            if ($scope.handler.data != undefined && $scope.handler.data.id != undefined && $scope.handler.data.id != 0) {
                                var r = remUrl.replace(':id', $scope.handler.data.id) + '?fileName=' + e.files[0].name;
                                //if ($scope.handler.data.sn != undefined) {
                                //    r += '&sn=' + $scope.handler.data.sn;
                                //}
                                if (e.files[0].serialNumber != undefined) {
                                    r += '&sn=' + e.files[0].serialNumber;
                                }

                                e.sender.options.async.removeUrl = r;
                            } else if ($scope.handler.data != undefined && $scope.handler.data.documentNumber != undefined) {
                                var r = remUrl.replace(':id', $scope.handler.data.documentNumber) + '?fileName=' + e.files[0].name + '&documentNumber=' + $scope.handler.data.documentNumber;
                                //if ($scope.handler.data.sn != undefined) {
                                //    r += '&sn=' + $scope.handler.data.sn;
                                //}
                                if (e.files[0].serialNumber != undefined) {
                                    r += '&sn=' + e.files[0].serialNumber;
                                }
                                e.sender.options.async.removeUrl = r;
                            }
                        },
                        upload: function (e) {
                            var url = $scope.saveUrl.replace(':id', $scope.handler.data.id);
                            var title = $('li.k-file[data-uid="' + e.files[0].uid + '"]').find('input[name="filedescription"]').val();
                            var querystring = '';
                            var name = e.files[0].name;
                            if (!name) {
                                return false;
                            } else {
                                querystring += "name=" + encodeURIComponent(name);
                            }
                            if (title != null && $.trim(title) != '') {
                                querystring += "&title=" + encodeURIComponent(title);
                            }

                            if ($scope.activity) {
                                querystring += "&activity=" + encodeURIComponent($scope.activity);
                            }
                            if ($scope.handler.data) {
                                $.each($scope.handler.data, function (key, value) {
                                    if (key.toUpperCase() !== 'ID') {
                                        if (querystring !== "") {
                                            querystring += "&";
                                        }
                                        querystring += key + "=" + value;
                                    }
                                });
                            }
                            //if ($scope.handler.data.sn != undefined) {
                            //    querystring += "&sn=" + $scope.handler.data.sn;
                            //}
                            //if ($scope.handler.data.documentNumber != undefined) {
                            //    querystring += "&documentNumber=" + $scope.handler.data.documentNumber;
                            //}
                            e.sender.options.async.saveUrl = url + '?' + querystring;
                            // add authorization header
                            var xhr = e.XMLHttpRequest;
                            if (xhr) {
                                xhr.addEventListener("readystatechange", function onReady(e) {
                                    if (xhr.readyState === 1 /* OPENED */) {
                                        authService.setAuthHeader(xhr);

                                        xhr.removeEventListener("readystatechange", onReady);
                                    }
                                });
                            }
                        },
                        error: function (e) {
                            $('li.k-file[data-uid="' + e.files[0].uid + '"]').find('.k-button.k-upload-action').append('<span class="k-icon k-i-close k-delete" title="Remove"></span>');
                            if ($scope.handler.onError != undefined) {
                                $scope.handler.onError(e);
                            }
                        },
                        success: function (e) {
                            if ($scope.handler.onSuccess != undefined) {
                                $scope.handler.onSuccess(e);
                            }
                            e.sender.wrapper.find('.k-file.k-file-success').find('.k-upload-filedescription').attr('readonly', 'readonly');
                        },
                        complete: function (e) {
                            if ($scope.handler.onComplete != undefined) {
                                $scope.handler.onComplete(e);
                            }
                        }
                    };
                    $scope._setFiles = function (f) {
                        $scope.uploadOption.files = f;
                        if (f != undefined && f.length > 0 && !$scope.init) {
                            $scope.init = true;
                            $scope.widget._renderInitialFiles(f);
                        }
                    }
                    uploadHandler.init($scope.handler);
                    $scope.handler.addCtrl($scope.name);
                }
            ],
            link: function (scope, element, attrs, ngModel) {
                var input = $('<input name="' + scope.name + '" type="file"/>');
                var wrapper = $('<div class="manual-kendo-async-upload"></div>').html(input);
                scope.widget = input.kendoUpload(scope.uploadOption).getKendoUpload();
                element.append(wrapper);
                scope.$watch(function () {
                    return ngModel.$modelValue;
                }, function (newValue) {
                    scope._setFiles(newValue);
                });
            }
        }
    }
])
.directive('appUploadDocumentType', [
    'uploadHandler', function (uploadHandler) {
        return {
            restrict: "E",
            require: 'ngModel',
            scope: {
                name: "@",
                ngModel: "=",
                saveUrl: "=",
                removeUrl: "=",
                documentType: "=",
                handler: "=",
                highlight: "@",
                //widget: "=",
                surrogate: "=",
                surrogateKey: "="
            },
            controller: [
                '$scope', 'messageBox', 'uploadChecker', "authService", function ($scope, messageBox, uploadChecker, authService) {
                    $scope.init = false;
                    var _template = "<span class='k-progress'></span>\
                                    <span class='k-icon'></span>\
                                    <span class='k-filename' title='#=name#'>#=name#</span>";

                    _template += "<strong class='k-upload-status'><button class='k-upload-action' type='button'></button></strong>";
                    var remUrl = $scope.removeUrl == null ? $scope.saveUrl : $scope.removeUrl;
                    //debugger;
                    $scope.uploadOption = {
                        async: {
                            saveUrl: $scope.saveUrl,
                            removeUrl: remUrl,
                            removeVerb: "DELETE",
                            autoUpload: false
                        },
                        template: _template,
                        //files: $scope.ngModel[$scope.documentType],
                        //files: $scope.ngModel,
                        multiple: true,
                        
                        select: function (e) {
                            setTimeout(function () { $(".k-upload-selected").hide(); }, 1);
                            if (angular.isUndefined($scope.ngModel)) {
                                $scope.ngModel = [];
                            }
                            var n = e.files[0].name, err = uploadChecker.validate(n, e.files[0]);
                            if (!err) {
                                for (var i = 0; i < $scope.ngModel.length; i++) {
                                    if ($scope.ngModel[i].name === n) {
                                        messageBox.error("Cannot upload file with duplicate name '" + n + "'");
                                        e.preventDefault();
                                        return false;
                                    }
                                }
                                $scope.init = true;
                                var that = this,
                                    documentTypeValue = that.element.data('documentTypeValue'),
                                    documentTypeName = that.element.data('documentTypeName');
                                //$scope.ngModel = [{ name: n, size: e.files[0].size, extension: e.files[0].extension, documentType: documentTypeValue, documentTypeName: documentTypeName }];
                                $scope.ngModel.push({ name: n, size: e.files[0].size, extension: e.files[0].extension, documentType: documentTypeValue, documentTypeName: documentTypeName });
                                $scope.$apply();
                                if ($scope.surrogate) {
                                    //if (angular.isFunction($scope.surrogateKey)) {
                                    //    e.files[0].key = $scope.surrogateKey();
                                    //} else {
                                    e.files[0].key = $scope.surrogateKey;
                                    //}
                                    var querystring = '';
                                    querystring += 'documentType=' + encodeURIComponent(documentTypeValue);
                                    var name = e.files[0].name;
                                    if (!name) {
                                        return false;
                                    } else {
                                        querystring += "&name=" + encodeURIComponent(name);
                                    }
                                    //if ($scope.handler.data && $scope.handler.data.sn != undefined) {
                                    //    querystring += "&sn=" + $scope.handler.data.sn;
                                    //}
                                    //if ($scope.handler.data && $scope.handler.data.documentNumber != undefined) {
                                    //    querystring += "&documentNumber=" + $scope.handler.data.documentNumber;
                                    //}
                                    e.files[0].saveUrl = $scope.saveUrl + '?' + querystring;
                                    $scope.surrogate.trigger('select', e);
                                    $scope.surrogate._module.onSelect({ target: $($scope.surrogate.element) }, e.files);
                                }
                            } else {
                                messageBox.error(err);
                                e.preventDefault();
                                return false;
                            }
                        },
                        remove: function (e) {
                            var del = e.files[0], r;
                            $('li.k-file[data-uid="' + e.files[0].uid + '"]').find('.k-filename').prepend('<b>Removing ... </b>');
                            $('li.k-file[data-uid="' + e.files[0].uid + '"]').find('.k-button.k-upload-action').attr('disabled', 'disabled');
                            $('li.k-file[data-uid="' + e.files[0].uid + '"]').find('.k-button.k-upload-action > .k-icon').removeClass('k-i-close').addClass('k-i-clock');

                            var documentTypeValue = this.element.data('documentTypeValue');
                            delete del.uid;
                            $scope.ngModel.splice(0, $scope.ngModel.length);
                            if ($scope.handler && $scope.handler.data != undefined && $scope.handler.data.id != undefined && $scope.handler.data.id != 0) {
                                r = remUrl.replace(':id', $scope.handler.data.id) + '?fileName=' + e.files[0].name + '&documentType=' + documentTypeValue;
                                //if ($scope.handler.data.sn != undefined) {
                                //    r += '&sn=' + $scope.handler.data.sn;
                                //}
                                if (e.files[0].serialNumber != undefined) {
                                    r += '&sn=' + e.files[0].serialNumber;
                                }
                                e.sender.options.async.removeUrl = r;
                            } else if ($scope.handler && $scope.handler.data != undefined && $scope.handler.data.documentNumber != undefined) {
                                r = remUrl.replace(':id', $scope.handler.data.documentNumber) + '?fileName=' + e.files[0].name + '&documentType=' + documentTypeValue + '&documentNumber=' + $scope.handler.data.documentNumber;;
                                //if ($scope.handler.data.sn != undefined) {
                                //    r += '&sn=' + $scope.handler.data.sn;
                                //}
                                if (e.files[0].serialNumber != undefined) {
                                    r += '&sn=' + e.files[0].serialNumber;
                                }
                                e.sender.options.async.removeUrl = r;
                            }
                            if ($scope.surrogate) {
                                var p = e.files[0].parentId;
                                r = remUrl;
                                if (p) {
                                    r = remUrl.replace(':id', e.files[0].parentId);
                                }
                                r += '?fileName=' + e.files[0].name + '&documentType=' + documentTypeValue;
                                if (e.files[0].serialNumber != undefined) {
                                    r += '&sn=' + e.files[0].serialNumber;
                                }
                                e.sender.options.async.removeUrl = r;
                                e.files[0].remUrl = r;
                                e.files[0].key = $scope.surrogateKey;
                                if (!$scope.surrogate.trigger('remove', e)) {
                                    $scope.surrogate._module.onRemove({ target: $($scope.surrogate.wrapper).find('.k-icon') }, e.files);
                                }
                                if (!p) {
                                    this._removeFileEntry($(this.wrapper).find('.k-file'));
                                    e.preventDefault();
                                    //e.sender.options.async.removeUrl = "";
                                }
                            }
                            //$scope.ngModel.splice(0, $scope.ngModel.length);
                            //$scope.ngModel = [];
                        },
                        
                        upload: function (e) {
                            var documentTypeValue = this.element.data('documentTypeValue');
                            var url = $scope.saveUrl.replace(':id', $scope.handler.data.id);
                            var querystring = '';
                            querystring += 'documentType=' + encodeURIComponent(documentTypeValue);
                            var name = e.files[0].name;
                            if (!name) {
                                return false;
                            } else {
                                querystring += "&name=" + encodeURIComponent(name);
                            }
                            if ($scope.handler.data) {
                                $.each($scope.handler.data, function (key, value) {
                                    if (key.toUpperCase() !== 'ID') {
                                        if (querystring !== "") {
                                            querystring += "&";
                                        }
                                        querystring += key + "=" + value;
                                    }
                                });
                            }
                            //if ($scope.handler.data.sn != undefined) {
                            //    querystring += "&sn=" + $scope.handler.data.sn;
                            //}
                            //if ($scope.handler.data.documentNumber != undefined) {
                            //    querystring += "&documentNumber=" + $scope.handler.data.documentNumber;
                            //}
                            e.sender.options.async.saveUrl = url + '?' + querystring;
                            // add authorization header
                            var xhr = e.XMLHttpRequest;
                            if (xhr) {
                                xhr.addEventListener("readystatechange", function onReady(e) {
                                    if (xhr.readyState === 1 /* OPENED */) {
                                        authService.setAuthHeader(xhr);

                                        xhr.removeEventListener("readystatechange", onReady);
                                    }
                                });
                            }
                        },
                        error: function (e) {
                            $('li.k-file[data-uid="' + e.files[0].uid + '"]').find('.k-button.k-upload-action').append('<span class="k-icon k-i-close k-delete" title="Remove"></span>');
                            if ($scope.handler && $scope.handler.onError != undefined) {
                                $scope.handler.onError(e);
                            }
                        },
                        success: function (e) {
                            if ($scope.handler && $scope.handler.onSuccess != undefined) {
                                $scope.handler.onSuccess(e);
                            }
                            e.sender.wrapper.find('.k-file.k-file-success').find('.k-upload-filedescription').attr('readonly', 'readonly');
                        },
                        complete: function (e) {
                            if ($scope.handler && $scope.handler.onComplete != undefined) {
                                $scope.handler.onComplete(e);
                            }
                        }
                    };
                    $scope._setFiles = function (f) {
                        $scope.uploadOption.files = f;
                        if (f != undefined && f.length > 0 && !$scope.init) {
                            $scope.init = true;
                            $scope.widget._renderInitialFiles(f);
                        }
                    }
                    uploadHandler.init($scope.handler);
                    if ($scope.handler) {
                        $scope.handler.addCtrl($scope.name);
                    }
                }
            ],
            link: function (scope, element, attrs, ngModel) {
                var w, build = function () {
                    var docname = $('<span class="documentTypeName manual-kendo-async-upload-documentTypeName" ng-show="documentType.name != \'\'">' + scope.documentType.Name + '</span>');
                    if (scope.documentType.isRequired) {
                        docname = $('<span class="required"> *</span>' + '<span class="documentTypeName manual-kendo-async-upload-documentTypeName" ng-show="documentType.name != \'\'">' + scope.documentType.Name + '</span>');
                    }

                    var input = $('<input name="' + scope.name + '" ng-model="ngModel" type="file" data-document-type-value="' + scope.documentType.Value + '" data-document-type-name="' + scope.documentType.Name + '"/>');
                    var wrapper = $('<div></div>').html($('<div class="manual-kendo-async-upload"></div>').html(docname).append(input));


                    scope.widget = input.kendoUpload(scope.uploadOption).getKendoUpload();
                    element.append(wrapper);
                    scope.$watch(function () {
                        return ngModel.$modelValue;
                    }, function (newValue) {
                        scope._setFiles(newValue);
                    });
                };
                if (angular.isDefined(scope.documentType.$resolved)) {
                    w = scope.$watch("documentType.$resolved", function (value) {
                        if (value) {
                            build();
                            w();
                        }
                    });
                } else {
                    build();
                }
            }
        }
    }
])
.directive('appUploadSurrogate', [
    'uploadHandler', function (uploadHandler) {
        return {
            restrict: "E",
            //require: 'ngModel',
            scope: {
                name: "@",
                //ngModel: "=",
                saveUrl: "=",
                removeUrl: "=",
                handler: "=",
                widget: "="
            },
            controller: [
                '$scope', 'messageBox', 'uploadChecker', "authService", function ($scope, messageBox, uploadChecker, authService) {
                    $scope.init = false;
                    var _template = "<span class='k-progress'></span>\
                                    <span class='k-icon'></span>\
                                    <span class='k-filename' title='#=name#'>#=name#</span>";
                    _template += "<label>รายละเอียดเพิ่มเติม</label>&nbsp;&nbsp;&nbsp;<input type='text' class='k-textbox k-upload-filedescription' name='filedescription' maxlength='300' value='#if(files[0].title!=undefined){# #=files[0].title# #}#'/>";
                    _template += "<strong class='k-upload-status'><button class='k-upload-action' type='button'></button></strong>";
                    //var remUrl = $scope.removeUrl == null ? $scope.saveUrl : $scope.removeUrl;
                    $scope.uploadOption = {
                        async: {
                            saveUrl: 'http://',
                            removeUrl: 'http://',
                            removeVerb: "DELETE",
                            autoUpload: false
                        },
                        template: _template,
                        //files: $scope.ngModel,
                        multiple: true,
                        select: function (e) {
                            setTimeout(function () { $(".k-upload-selected").hide(); }, 1);
                            var n = e.files[0].name, err = uploadChecker.validate(n, e.files[0]);
                            if (!err) {
                                //for (var i = 0; i < $scope.ngModel.length; i++) {
                                //    if ($scope.ngModel[i].name == n) {
                                //        messageBox.error("Cannot upload file with duplicate name '" + n + "'");
                                //        e.preventDefault();
                                //        return false;
                                //    }
                                //}
                                //$scope.ngModel.push({ name: e.files[0].name, size: e.files[0].size, extension: e.files[0].extension });
                                $scope.handler.addSurrogateFile({
                                    name: e.files[0].name,
                                    size: e.files[0].size,
                                    extension: e.files[0].extension,
                                    saveUrl: e.files[0].saveUrl,
                                    key: e.files[0].key
                                });
                            } else {
                                messageBox.error(err);
                                e.preventDefault();
                                return false;
                            }
                        },
                        remove: function (e) {
                            $('li.k-file[data-uid="' + e.files[0].uid + '"]').find('.k-filename').prepend('<b>Removing ... </b>');
                            $('li.k-file[data-uid="' + e.files[0].uid + '"]').find('.k-button.k-upload-action').attr('disabled', 'disabled');
                            $('li.k-file[data-uid="' + e.files[0].uid + '"]').find('.k-button.k-upload-action > .k-icon').removeClass('k-i-close').addClass('k-i-clock');

                            var del = e.files[0];
                            var remUrl = e.files[0].remUrl;
                            delete del.uid;
                            //$scope.ngModel.splice($scope.ngModel.indexOf(del), 1);
                            if ($scope.handler.data != undefined && $scope.handler.data.id != undefined && $scope.handler.data.id != 0) {
                                var r = remUrl.replace(':id', $scope.handler.data.id) + '?fileName=' + e.files[0].name;
                                //if ($scope.handler.data.sn != undefined) {
                                //    r += '&sn=' + $scope.handler.data.sn;
                                //}
                                if (e.files[0].serialNumber != undefined) {
                                    r += '&sn=' + e.files[0].serialNumber;
                                }
                                e.sender.options.async.removeUrl = r;
                            } else if ($scope.handler.data != undefined && $scope.handler.data.documentNumber != undefined) {
                                var r = remUrl.replace(':id', $scope.handler.data.documentNumber) + '?fileName=' + e.files[0].name + '&documentNumber=' + $scope.handler.data.documentNumber;
                                //if ($scope.handler.data.sn != undefined) {
                                //    r += '&sn=' + $scope.handler.data.sn;
                                //}
                                if (e.files[0].serialNumber != undefined) {
                                    r += '&sn=' + e.files[0].serialNumber;
                                }
                                e.sender.options.async.removeUrl = r;
                            }

                            var k = $scope.handler.getSurrogateKey(e.files[0]);
                            if (k) {
                                e.sender.options.async.removeUrl = e.files[0].remUrl.replace(':id', k);
                            }
                        },
                        upload: function (e) {
                            //var url = $scope.saveUrl.replace(':id', $scope.handler.data.id);
                            var url = e.files[0].saveUrl;
                            //var title = $('li.k-file[data-uid="' + e.files[0].uid + '"]').find('input[name="filedescription"]').val();
                            var querystring = '';
                            //var name = e.files[0].name;
                            //if (!name) {
                            //    return false;
                            //} else {
                            //    querystring += "name=" + encodeURIComponent(name);
                            //}
                            //if (title != null && $.trim(title) != '') {
                            //    querystring += "&title=" + encodeURIComponent(title);
                            //}
                            if ($scope.handler.data) {
                                $.each($scope.handler.data, function (key, value) {
                                    if (key.toUpperCase() !== 'ID') {
                                        if (querystring !== "") {
                                            querystring += "&";
                                        }
                                        querystring += key + "=" + value;
                                    }
                                });
                            }
                            //if ($scope.handler.data && $scope.handler.data.sn != undefined) {
                            //    querystring += "&sn=" + $scope.handler.data.sn;
                            //}
                            //if ($scope.handler.data && $scope.handler.data.documentNumber != undefined) {
                            //    querystring += "&documentNumber=" + $scope.handler.data.documentNumber;
                            //}
                            if (querystring != '') {
                                e.sender.options.async.saveUrl = url + querystring;
                            } else {
                                e.sender.options.async.saveUrl = url;
                            }
                            //e.sender.options.async.saveUrl = url + '?' + querystring;
                            // add authorization header
                            var xhr = e.XMLHttpRequest;
                            if (xhr) {
                                xhr.addEventListener("readystatechange", function onReady(e) {
                                    if (xhr.readyState === 1 /* OPENED */) {
                                        authService.setAuthHeader(xhr);

                                        xhr.removeEventListener("readystatechange", onReady);
                                    }
                                });
                            }
                        },
                        error: function (e) {
                            $('li.k-file[data-uid="' + e.files[0].uid + '"]').find('.k-button.k-upload-action').append('<span class="k-icon k-i-close k-delete" title="Remove"></span>');
                            if ($scope.handler.onError != undefined) {
                                $scope.handler.onError(e);
                            }
                        },
                        success: function (e) {
                            if ($scope.handler.onSuccess != undefined) {
                                $scope.handler.onSuccess(e);
                            }
                            e.sender.wrapper.find('.k-file.k-file-success').find('.k-upload-filedescription').attr('readonly', 'readonly');
                        },
                        complete: function (e) {
                            if ($scope.handler.onComplete != undefined) {
                                $scope.handler.onComplete(e);
                            }
                        }
                    };
                    $scope._setFiles = function (f) {
                        $scope.uploadOption.files = f;
                        if (f != undefined && f.length > 0 && !$scope.init) {
                            $scope.init = true;
                            $scope.widget._renderInitialFiles(f);
                        }
                    }
                    uploadHandler.init($scope.handler);
                    $scope.handler.addCtrl($scope.name);
                }
            ],
            link: function (scope, element, attrs, ngModel) {
                element.addClass('app-upload-surrogate');
                var input = $('<input name="' + scope.name + '" type="file"/>');
                var wrapper = $('<div class="manual-kendo-async-upload"></div>').html(input);
                scope.widget = input.kendoUpload(scope.uploadOption).getKendoUpload();
                element.append(wrapper);
                scope.$watch(function () {
                    return ngModel.$modelValue;
                }, function (newValue) {
                    scope._setFiles(newValue);
                });
            }
        }
    }
]);
