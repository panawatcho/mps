﻿"use strict";
angular.module("app.directives")
    .directive("languageSwitch",
        [
            function () {
                return {
                    restrict: "E",
                    controller: [
                        "$scope", "langService",
                        function ($scope, langService) {
                            $scope.langService = langService;
                        }
                    ],
                    template: '<div class="btn-group btn-group-justified" role="group" style="width: 20%; margin-left: 80%;" aria-label="Language">' +
                        '<div class="btn-group" role="group">' +
                        '<button type="button" class="btn btn-default" ng-click="langService.switchLanguage(\'en\')" ng-class="{\'btn-default\':langService.currentLanguage()!==\'en\',\'btn-success active\':langService.currentLanguage()===\'en\'}">EN</button>' +
                        '</div>' +
                        '<div class="btn-group" role="group">' +
                        '<button type="button" class="btn btn-default" ng-click="langService.switchLanguage(\'th\')" ng-class="{\'btn-default\':langService.currentLanguage()!==\'th\',\'btn-success active\':langService.currentLanguage()===\'th\'}">TH</button>' +
                        '</div>' +
                        '</div>'


                };
            }
        ]
    )
    .directive('box', function () {
        return {
            restrict: "E",
            transclude: true,
            scope: {
                headerTitle: "@",
                expanded: "=",
                boxHeader: "="
            },
            template: '<div class="box box-danger" ng-class="expanded==false?\'collapsed-box\':\'\'">\
                            <div class="box-header with-border" ng-if="boxHeader!=false">\
                                <h4 class="box-title">{{headerTitle}}</h4>\
                                <div class="box-tools pull-right">\
                                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">\
                                    <i class="fa" ng-class="expanded==false?\'fa-plus\':\'fa-minus\'"></i></button>\
                                    </div>\
                        </div>\
                                <div class="box-body" ng-transclude>\
                                </div>\
                        </div>'
            

        }
    })
     .directive('headerTitle', [
        "$compile", function ($compile) {
            return {
                restrict: "E",
                transclude: true,
                compile: function (element, attr) {
                    var template = "<h1>" + element.html() + "<small>&nbsp;&nbsp; " + attr.smallTitleMessage + "</small></h1>";

                    element.html(template);
                },
                template: "<ng-transclude/>"
            }
        }
     ])
    .directive('fileIcon', function () {
        return {
            restrict: "AE",
            scope: {
                ext: "@"
            },
            controller: [
                "$scope",
                function ($scope) {
                    var i = "fa fa-2x fa-";
                    $scope.fileClass = function (f) {
                        var j = f.indexOf('.');
                        f = f && j !== -1 ? f.substr(j + 1) : f;
                        switch (f) {
                            case "txt":
                            case "rtf":
                            case "msg":
                                return i + "file-text-o";
                            case "xls":
                            case "xlsx":
                                return i + "file-excel-o";
                            case "doc":
                            case "docx":
                                return i + "file-word-o";
                            case "ppt":
                            case "pptx":
                                return i + "file-powerpoint-o";
                            case "zip":
                            case "rar":
                                return i + "file-archive-o";
                            case "pdf":
                                return i + "file-pdf-o";
                            case "gif":
                            case "jpg":
                            case "jpeg":
                            case "png":
                            case "tif":
                            case "tiff":
                                return i + "file-image-o";
                            default:
                                return i + "file-o";
                        }
                    }
                }
            ],
            link: function (scope, element, attrs, controller, transcludeFn) {
                element.addClass('file-icon');
            },
            template: '<i class="{{fileClass(ext)}}"></i>'
        }
    })
    .directive('fileUploadList', [
        function () {
            return {
                restrict: 'E',
                templateUrl: './templates/file-upload-list.html',
                scope: {
                    files: "=",
                    attachments: "=",
                    onDelete: "&"
                },
                controller: [
                    "$scope", "uploadService",
                    function ($scope, uploadService) {
                        $scope.download = uploadService.download;
                        $scope.getCopy = uploadService.getCopy;
                        $scope.deleteFile = uploadService.delete;
                    }
                ]
            };
        }
    ])
    .directive('fileUploadListDocument', [
        function () {
            return {
                restrict: 'E',
                templateUrl: './templates/file-upload-list-document.html',
                scope: {
                    files: "=",
                    attachments: "=",
                    documentType: "="
                },
                controller: [
                    "$scope", "uploadService",
                    function ($scope, uploadService) {
                        $scope.download = uploadService.download;
                        $scope.getCopy = uploadService.getCopy;
                        $scope.deleteFile = uploadService.delete;
                    }
                ]
            };
        }
    ])
    .directive('crsDatePicker', [
        '$compile', function ($compile) {
            return {
                restrict: "A",
                compile: function (element, attr) {
                    element.removeAttr("crs-date-picker");
                    attr.$set("kendo-masked-date-picker", attr.crsDatePicker);
                    attr.$set("k-options", "{dateOptions:{parseFormats: [\'yyyy-MM-ddTHH:mm:ss.fffz\',\'yyyy-MM-ddTHH:mm:ssz\'], format:\'dd/MM/yyyy\'}, maskOptions:{mask:\'00/00/0000\'}}");
                    //attr.$set("kendo-date-picker", attr.pksDatePicker);
                    //attr.$set("k-options", "{parseFormats: [\'yyyy-MM-ddTHH:mm:ss.fffz\',\'yyyy-MM-ddTHH:mm:ssz\']}");
                    if (attr.name) {
                        element.parent().append('<div><span class="k-invalid-msg" data-for="' + attr.name + '"></span></div>');
                    }
                    return {
                        pre: function preLink(scope, iElement, iAttrs, controller) { },
                        post: function postLink(scope, iElement, iAttrs, controller) {
                            $compile(iElement)(scope);
                        }
                    };
                }
            }
        }
    ])
    .directive('actionView', function () {
        return {
            restrict: "E",
            scope: {
                actions: "=",
                click: "&",
                newcomment: "="
            },
            template: '<div class="comment-new row">\
                        <label class="col-xs-12 col-sm-2 control-label">Your comment</label>\
                        <div class="col-xs-12 col-sm-10">\
                            <div class="form-group">\
                                <textarea class="comment-new-textarea form-control" ng-model="newcomment"></textarea>\
                            </div>\
                        </div>\
                        <div class="col-xs-12 col-sm-10 col-sm-offset-2">\
                            <a class="btn btn-default action-button action-button-{{action}}" ng-repeat="action in actions" ng-click="click()(action)">\
                                {{action}}\
                            </a>\
                        </div>\
                    </div>'
        }
    })
    .directive('newComment', function () {
        return {
            restrict: "E",
            scope: {
                newcomment: "="
            },
            template: '<div class="comment-new row">\
                         <label class="col-xs-12 col-sm-2 control-label">Your comment</label>\
                        <div class="col-xs-12 col-sm-10">\
                            <div class="form-group">\
                                <textarea class="comment-new-textarea form-control" ng-model="newcomment"></textarea>\
                            </div>\
                        </div>\
                    </div>'
        }
    })
    .directive('workflowProgress', function () {
        return {
            restrict: "E",
            scope: {
                statusFlag: "=",
                status: '=',
                viewFlowUrl: "=",
                steps: "="
            },
            controller: [
                '$scope', function ($scope) {
                    $scope.$watch('statusFlag', function () {
                        if ($scope.statusFlag == undefined || $scope.statusFlag == null || $scope.statusFlag == '' || $scope.statusFlag == 0) {
                            $scope.classCreate = 'active';
                            $scope.classInprogress = '';
                            $scope.classCompleted = '';
                        } else if ($scope.statusFlag == 9) {
                            $scope.classCreate = 'complete';
                            $scope.classInprogress = 'complete';
                            $scope.classCompleted = 'completed';
                        } else if ($scope.statusFlag == 6 || $scope.statusFlag == 5) {
                            $scope.classCreate = 'complete';
                            $scope.classInprogress = 'complete';
                            $scope.classCompleted = 'cancelled';
                        } else {
                            $scope.classCreate = 'complete';
                            $scope.classInprogress = 'active';
                            $scope.classCompleted = '';
                        }
                    });

                    $scope.isInProgress = function () {
                        return $scope.statusFlag != undefined && $scope.statusFlag != null && $scope.statusFlag != '' && $scope.statusFlag != 0 && $scope.statusFlag != 6 && $scope.statusFlag != 9 && $scope.status != 'Draft';
                    }

                    $scope.untouched = true;
                    $scope.showStep = function () {
                        $scope.goaWindow.center().open();

                        return false;
                    }

                    $scope.toggleActive = function (step) {
                        $scope.untouched = false;
                        angular.forEach($scope.steps, function (v, k) {
                            v.Show = false;
                        });
                        step.Show = true;
                    }
                    $scope.onOpen = function (e) {
                        this.wrapper.addClass("transparent-window").addClass('goa-progress-window-wrapper');
                    }
                }
            ],
            template: '<div class="row hidden-print"><div class="col-md-12"><div class="fuelux">\
                        <div class="wizard">\
							<ul class="steps">\
								<li style="width:26%;" class="{{classCreate}}">\
                                    <span class="icon">1</span>\
                                    <span class="hidden-xs hidden-sm">Step 1: </span><span ng-show="classCreate==\'complete\'">Created</span><span ng-hide="classCreate==\'complete\'">Create</span>\
                                    <span class="chevron"></span>\
								</li>\
                                <li style="width:48%;" class="{{classInprogress}}">\
                                    <a ng-show="viewFlowUrl" class="workflow-progress viewflow hidden-xs" href="{{viewFlowUrl}}" target="_blank" title="View flow"><span><i class="fa fa-sitemap"></i></span></a>\
                                    <a href="#" ng-show="steps && steps.$resolved && steps.length > 0" class="workflow-progress goa hidden-xs" ng-click="showStep()" title="View GOA"><span><i class="fa fa-sort-numeric-asc"></i></span></a>\
									<span class="icon">2</span>\
                                    <span ng-show="!isInProgress()" class="hidden-xs hidden-sm">Step 2: </span><span ng-show="!isInProgress()">In Process</span>\
                                    <span ng-show="isInProgress()">{{status}}</span>\
                                    <span class="chevron"></span>\
								</li>\
                                <li style="width:26%;" class="{{classCompleted}}">\
									<span class="icon">3</span>\
                                    <span class="hidden-xs hidden-sm">Step 3: </span><span>{{classCompleted == \'cancelled\' ? (statusFlag == 5 ? \'Rejected\' : \'Cancelled\')  : \'Completed\'}}</span>\
                                    <span class="chevron"></span>\
								</li>\
							</ul>\
                        </div>\
            		</div></div></div>\
                    <div class="row hidden-sm hidden-md hidden-lg hidden-print"><div class="col-xs-6"><a ng-disabled="!viewFlowUrl" class="btn btn-default btn-block" href="{{viewFlowUrl}}" target="_blank"><span><i class="fa fa-sitemap"></i> View flow</span></a></div>\
                    <div class="col-xs-6"><button type="button" ng-disabled="!steps || !steps.$resolved || steps.length <= 0" class="btn btn-default btn-block" ng-click="showStep()"><span><i class="fa fa-sort-numeric-asc"></i> View GOA</span></button></div></div><div class="row hidden-sm hidden-md hidden-lg"><br/></div>\
                    <div class="goa-progress-window" kendo-window="goaWindow" k-options="{width:\'400px\', modal: true, visible: false, resizable:false, open:onOpen, title:\'\'}">\
                    <div class="goa-progress-wrapper">\
                    <ul class="goa-progress">\
                    <li class="col-md-offset-1 col-xs-10" ng-repeat="step in steps" ng-click="toggleActive(step)" ng-class="{first:$index==0,last:$index==$parent.steps.length-1,complete:step.Status==\'Approve\'||step.Status==\'Start\',active:step.Active,cancelled:step.Status==\'Revise\'||step.Status==\'Reject\',show:step.Show || (untouched && step.Active)}">\
                    <div class="row">\
                    <div class="col-xs-2"><span class="icon">{{$index+1}}</span></div>\
                    <div class="col-xs-10"><div class="row"><div class="col-xs-12">{{step.DisplayName}}</div></div><div class="row"><div class="col-xs-12 goa-detail" ng-show="step.Show || (untouched && step.Active)">{{step.Position}}<span ng-show="step.SecretaryRole"><br/>* with secretary</span><br/>Last action: {{step.Status}}<span ng-show="step.ActionOn"><br/>({{step.ActionOn|appLongDateTime}})</span></div></div></div>\
                    </div>\
                    </li>\
                    </ul>\
                    </div>\
                    </div>'
        }
    })
    .directive('priceTextBox', [
        '$compile', function ($compile) {
            return {
                restrict: "A",
                compile: function (element, attr) {
                    element.removeAttr("price-text-box");
                    attr.$set("kendo-numeric-text-box", attr.priceTextBox);
                    if (!attr.kSpinners) {
                        attr.$set("k-spinners", true);
                    }
                    if (!attr.kFormat) {
                        attr.$set("k-format", "'n2'");
                    }
                    if (!attr.kDecimals) {
                        attr.$set("k-decimals", "2");
                    }
                    if (attr.name) {
                        element.parent().append('<span class="k-invalid-msg" data-for="' + attr.name + '" ></span>');
                    }
                    return {
                        pre: function preLink(scope, iElement, iAttrs, controller) { },
                        post: function postLink(scope, iElement, iAttrs, controller) {
                            $compile(iElement)(scope);
                        }
                    };
                }
            }
        }
    ])
     .directive('attachmentView', function () {
         return {
             restrict: "E",
             scope: {
                 attachments: "=",
                 group: "=",
                 documentType: "@"
             },
             controller: [
                 "$scope",
                 function ($scope) {
                     $scope.getRootWebAPI = angular.crs.url.webApi;
                 }
             ],
             template: '<div class="attachment-view">\
                        <span class="documentTypeName attachment-view-documentTypeName" ng-if="documentType != null && documentType.name != \'\'"></span>\
                        <div ng-show="attachments == null || attachments.length == 0">\
                            <i style="margin-left:15px">There is no attachment.</i>\
                        </div>\
                        <div class="attachment-view-item" ng-repeat="a in attachments | filter:{ DocumentTypeId: documentType}">\
                            <div class="attachment-view-left pull-left">\
                                <div class="row" style="margin: 0">\
                                    <div class="col-sm-12">\
                                        <file-icon ext="{{a.extension}}"></file-icon> <span><b>{{a.name}}</b></span>\
                                    </div>\
                                </div>\
                              </div>\
                            <div class="pull-right">\
                                <div class="btn-group">\
                                   <a class="btn btn-default" href="({{a.url}})" target="_blank"><i class="fa fa-download"></i> Download</a>\
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">\
                                            <span class="caret"></span>\
                                            <span class="sr-only">Toggle Dropdown</span>\
                                        </button>\
                                        <ul class="dropdown-menu" role="menu">\
                                            <li><a href="{{(a.getcopy_url)}}">Download a copy</a>\</li>\
                                        </ul>\
                                    </div>\
                            </div>\
                            <div class="clearfix"></div>\
                        </div>\
                    </div>'
         }
     })
     .directive('attachmentViewShare', function () {
         return {
             restrict: "E",
             scope: {
                 attachments: "=",
                 documentType: "@",
                 filter: "="
             },
             controller: [
                 "$scope",
                 function ($scope) {
                     $scope.getRootWebAPI = angular.crs.url.webApi;
                     $scope.filterAttachment = { DocumentTypeId: $scope.documentType }

                     if ($scope.filter) {

                         angular.extend($scope.filterAttachment, $scope.filterAttachment, $scope.filter);
                     }
                 }
             ],
             template: '<div class="attachment-view">\
                        <span class="documentTypeName attachment-view-documentTypeName" ng-if="documentType != null && documentType.name != \'\'"></span>\
                        <div ng-show="attachments == null || attachments.length == 0">\
                            <i style="margin-left:15px">There is no attachment.</i>\
                        </div>\
                        <div class="attachment-view-item" ng-repeat="a in attachments | filter:filterAttachment">\
                            <div class="attachment-view-left pull-left">\
                                <div class="row" style="margin: 0">\
                                    <div class="col-sm-12">\
                                        <file-icon ext="{{a.extension}}"></file-icon> <span><b>{{ a.name }}</b></span><span ng-show="a.title != null"> - <code>{{ a.title }}</kbd></code>\
                                    </div>\
                                </div>\
                              </div>\
                            <div class="pull-right">\
                                <div class="btn-group">\
                                 <a class="btn btn-default" href="{{(a.url)}}" target="_blank"><i class="fa fa-download"></i> Download</a>\
                                  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">\
                                            <span class="caret"></span>\
                                            <span class="sr-only">Toggle Dropdown</span>\
                                        </button>\
                                        <ul class="dropdown-menu" role="menu">\
                                            <li><a href="{{(a.getcopy_url)}}">Download a copy</a>\</li>\
                                        </ul>\
                                </div>\
                            </div>\
                            <div class="clearfix"></div>\
                        </div>\
                    </div>'
         }
     })
    .directive('attachmentViewGroup', function () {
        //debugger;
        return {
            restrict: "E",
            scope: {
                attachments: "=",
                group: "=",
                documentType: "@"
            },

            controller: [
                "$scope",
                function ($scope) {
                    $scope.getRootWebAPI = angular.crs.url.webApi;
                }
            ],

            template: '<div class="attachment-view"><h5>{{group}}</h5>\
                        <span class="documentTypeName attachment-view-documentTypeName" ng-if="documentType != null && documentType.name != \'\'"></span>\
                        <div ng-show="attachments == null || attachments.length == 0">\
                            <i style="margin-left:15px">There is no attachment.</i>\
                        </div>\
                        <div class="attachment-view-item" ng-repeat="a in attachments | filter:{ DocumentTypeId: documentType}">\
                            <div class="attachment-view-left pull-left">\
                                <div class="row" style="margin: 0">\
                                    <div class="col-sm-12">\
                                        <file-icon ext="{{a.extension}}"></file-icon> <span><b>{{ a.name }}</b></span><span ng-show="a.title != null"> - <code>{{ a.title }}</kbd></code>\
                                    </div>\
                                </div>\
                              </div>\
                            <div class="pull-right">\
                                <div class="btn-group">\
                                   <a class="btn btn-default" href="{{a.url}}" target="_blank"><i class="fa fa-download"></i> Download</a>\
                                  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">\
                                            <span class="caret"></span>\
                                            <span class="sr-only">Toggle Dropdown</span>\
                                        </button>\
                                        <ul class="dropdown-menu" role="menu">\
                                            <li><a href="{{a.getcopy_url}}">Download a copy</a>\</li>\
                                        </ul>\
                                    </div>\
                            </div>\
                            <div class="clearfix"></div>\
                        </div>\
                    </div>'
        }
    })
    .directive('fileUpload', [
        function () {
            return {
                restrict: 'E',
                templateUrl: './templates/file-upload.html',
                scope: {
                    files: "=",
                    attachments: "="
                },
                controller: [
                    "$scope", "uploadService",
                    function ($scope, uploadService) {
                        $scope.download = uploadService.download;
                        $scope.getCopy = uploadService.getCopy;
                        $scope.deleteFile = uploadService.delete;
                    }
                ]
            };
        }
    ])
    .directive('k2ActionListAcc', function () {
        return {
            restrict: "E",
            scope: {
                actions: "=",
                click: "&"
            },
            template: '<div class="k2-action-list-acc row">\
                        <div class="col-xs-12">\
                        <button type="submit" class="k-button btn-success ng-scope"><i class="fa fa-save"></i> SAVE</button>\
                            <button type="button" class="k-button k2-action-button k2-action-button-{{action.toLowerCase().replace(\' \', \'-\')}}" ng-click="click()(action)" ng-repeat="action in actions">\
                                <i class="fa k2-action-button-fa-{{action.toLowerCase().replace(\' \', \'-\')}}"></i> {{action.toUpperCase()}}\
                            </button>\
                       </div>'

        }
    })
    .directive('k2ActionList', function () {
        return {
            restrict: "E",
            scope: {
                actions: "=",
                click: "&"
            },
            template: '<div class="k2-action-list row">\
                        <div class="col-xs-12">\
                            <button type="button" class="k-button k2-action-button k2-action-button-{{action.toLowerCase().replace(\' \', \'-\')}}" ng-click="click()(action)" ng-repeat="action in actions">\
                                <i class="fa k2-action-button-fa-{{action.toLowerCase().replace(\' \', \'-\')}}"></i> {{action.toUpperCase()}}\
                            </button>\
                       </div>'

        }
    })
    .directive('commentView', function () {
        return {
            restrict: "E",
            scope: {
                comments: "="
            },
            controller: [
                '$scope', '$filter', function ($scope, $filter) {
                    $scope.commentGridOption = function () {
                        if (!$scope.comments) return undefined;

                        //var comments =   $filter('orderBy')($scope.comments, '-CommentedDate'  )
                        return {
                            dataSource: new kendo.data.DataSource({
                                data: $scope.comments,
                                sort: { field: 'CommentedDate', dir: 'desc' }
                            }),
                            columns: [
                                { field: "Comment", title: "Comment", width: "150px" },
                                { field: "CommentedBy", title: "Commented by", width: "150px" },
                                { field: "CommentedDate", title: "Commented date", template: "#=kendo.toString(new Date(CommentedDate),'dd/MM/yyyy HH:mm:ss')#", width: "150px" },
                                { field: "Activity", title: "Activity", width: "150px" },
                                { field: "Action", title: "Action", width: "150px" },
                               
                            ]
                        };
                    }
                }
            ],
            template: ' <div kendo-grid options="commentGridOption()"></div>'
        }
    })
    .directive('modal', function () {
        return {
            restrict: "A",
            transclude: true,
            scope: {
                title: "@",
                id: "@",
                size: "@",
                style_: "@"
            },
            controller: [
                '$scope', function ($scope) {
                    // $scope.width = ($scope.width) ? $scope.width : "800";
                    $scope.size = ($scope.size) ? $scope.size : "modal-sm";
                    //modal-sm
                }
            ],
            template: '<div class="modal fade" id="{{id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">\
                    <div class="modal-dialog {{size}}" role="document" style="{{style_}}">\
                        <div class="modal-content">\
                            <div class="modal-header">\
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>\
                                <h4 class="modal-title" id="myModalLabel">{{title}}</h4>\
                            </div>\
                            <div class="modal-body" ng-transclude>\
                            </div>\
                            <div class="modal-footer">\
                                <button type="button" class="btn btn-default" >Close</button>\
                                </div>\
                        </div>\
                    </div>\
                </div>'
        }
    })
 .directive('star', function () {
     return {
         restrict: 'E',

         template: '<span class="star">*</span>'
     }
 });
