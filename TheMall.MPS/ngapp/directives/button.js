﻿'use strict';
angular.module('app.directives')
    .directive('btnCreate', [
        '$compile', function($compile) {
            return {
                restrict: "A",
                compile: function(element, attr) {

                    element.removeAttr("btn-create");
                    element.addClass("k-button btn-success");
                    element.append('<i class="fa fa-plus"></i>');

                    if (attr.label) {
                        element.append(' ' + attr.label);
                    } else {
                        element.append(' Create');
                    }
                    return {
                        pre: function preLink(scope, iElement, iAttrs, controller) {},
                        post: function postLink(scope, iElement, iAttrs, controller) {
                            $compile(iElement)(scope);
                        }
                    };
                }
            }
        }
    ])
    .directive('btnSave', [
        '$compile', function($compile) {
            return {
                restrict: "A",
                compile: function(element, attr) {

                    element.removeAttr("btn-save");
                    element.addClass("k-button btn-success");
                    element.append('<i class="fa fa-save"></i>');

                    if (attr.label) {
                        element.append(' ' + attr.label);
                    } else {
                        element.append(' Save');
                    }
                    return {
                        pre: function preLink(scope, iElement, iAttrs, controller) {},
                        post: function postLink(scope, iElement, iAttrs, controller) {
                            $compile(iElement)(scope);
                        }
                    };
                }
            }
        }
    ])
    .directive('btnApprove', [
        '$compile', function($compile) {
            return {
                restrict: "A",
                compile: function(element, attr) {

                    element.removeAttr("btn-approve");
                    element.addClass("k-button btn-success");
                    element.append('<i class="fa fa-check"></i>');

                    if (attr.label) {
                        element.append(' ' + attr.label);
                    } else {
                        element.append(' Approve');
                    }
                    return {
                        pre: function preLink(scope, iElement, iAttrs, controller) {},
                        post: function postLink(scope, iElement, iAttrs, controller) {
                            $compile(iElement)(scope);
                        }
                    };
                }
            }
        }
    ])
    .directive('btnComplete', [
        '$compile', function($compile) {
            return {
                restrict: "A",
                compile: function(element, attr) {

                    element.removeAttr("btn-complete");
                    element.addClass("k-button btn-success");
                    element.append('<i class="fa fa-check-square-o"></i>');

                    if (attr.label) {
                        element.append(' ' + attr.label);
                    } else {
                        element.append(' Complete');
                    }
                    return {
                        pre: function preLink(scope, iElement, iAttrs, controller) {},
                        post: function postLink(scope, iElement, iAttrs, controller) {
                            $compile(iElement)(scope);
                        }
                    };
                }
            }
        }
    ])
    .directive('btnCancel', [
        '$compile', function($compile) {
            return {
                restrict: "A",
                compile: function(element, attr) {

                    element.removeAttr("btn-cancel");
                    element.addClass("k-button btn-danger");
                    element.append('<i class="fa fa-times"></i>');

                    if (attr.label) {
                        element.append(' ' + attr.label);
                    } else {
                        element.append(' Cancel');
                    }
                    return {
                        pre: function preLink(scope, iElement, iAttrs, controller) {},
                        post: function postLink(scope, iElement, iAttrs, controller) {
                            $compile(iElement)(scope);
                        }
                    };
                }
            }
        }
    ])
    .directive('btnReject', [
        '$compile', function($compile) {
            return {
                restrict: "A",
                compile: function(element, attr) {

                    element.removeAttr("btn-reject");
                    element.addClass("k-button btn-danger");
                    element.append('<i class="fa fa-stop"></i>');

                    if (attr.label) {
                        element.append(' ' + attr.label);
                    } else {
                        element.append(' Reject');
                    }
                    return {
                        pre: function preLink(scope, iElement, iAttrs, controller) {},
                        post: function postLink(scope, iElement, iAttrs, controller) {
                            $compile(iElement)(scope);
                        }
                    };
                }
            }
        }
    ])
    .directive('btnRevise', [
        '$compile', function($compile) {
            return {
                restrict: "A",
                compile: function(element, attr) {

                    element.removeAttr("btn-revise");
                    element.addClass("k-button btn-warning");
                    element.append('<i class="fa fa-backward"></i>');

                    if (attr.label) {
                        element.append(' ' + attr.label);
                    } else {
                        element.append(' Revise');
                    }
                    return {
                        pre: function preLink(scope, iElement, iAttrs, controller) {},
                        post: function postLink(scope, iElement, iAttrs, controller) {
                            $compile(iElement)(scope);
                        }
                    };
                }
            }
        }
    ])
    .directive('btnSubmit', [
        '$compile', function($compile) {
            return {
                restrict: "A",
                compile: function(element, attr) {

                    element.removeAttr("btn-submit");
                    element.addClass("k-button btn-info");
                    element.append('<i class="fa fa-play"></i>');

                    if (attr.label) {
                        element.append(' ' + attr.label);
                    } else {
                        element.append(' Submit');
                    }
                    return {
                        pre: function preLink(scope, iElement, iAttrs, controller) {},
                        post: function postLink(scope, iElement, iAttrs, controller) {
                            $compile(iElement)(scope);
                        }
                    };
                }
            }
        }
    ])
    .directive('btnSendBack', [
        '$compile', function($compile) {
            return {
                restrict: "A",
                compile: function(element, attr) {

                    element.removeAttr("btn-send-back");
                    element.addClass("k-button btn-default");
                    element.append('<i class="fa fa-step-backward"></i>');

                    if (attr.label) {
                        element.append(' ' + attr.label);
                    } else {
                        element.append(' Send Back');
                    }
                    return {
                        pre: function preLink(scope, iElement, iAttrs, controller) {},
                        post: function postLink(scope, iElement, iAttrs, controller) {
                            $compile(iElement)(scope);
                        }
                    };
                }
            }
        }
    ])
    .directive('btnSendNext', [
        '$compile', function($compile) {
            return {
                restrict: "A",
                compile: function(element, attr) {

                    element.removeAttr("btn-send-next");
                    element.addClass("k-button btn-info");
                    element.append('<i class="fa fa-step-forward"></i>');

                    if (attr.label) {
                        element.append(' ' + attr.label);
                    } else {
                        element.append(' Send Next');
                    }
                    return {
                        pre: function preLink(scope, iElement, iAttrs, controller) {},
                        post: function postLink(scope, iElement, iAttrs, controller) {
                            $compile(iElement)(scope);
                        }
                    };
                }
            }
        }
    ])
    .directive('btnBack', [
        '$compile', function($compile) {
            return {
                restrict: "A",
                compile: function(element, attr) {

                    element.removeAttr("btn-back");
                    element.addClass("k-button btn-default");
                    element.append('<i class="fa fa-arrow-left"></i>');

                    if (attr.label) {
                        element.append(' ' + attr.label);
                    } else {
                        element.append(' BACK');
                    }
                    return {
                        pre: function preLink(scope, iElement, iAttrs, controller) {},
                        post: function postLink(scope, iElement, iAttrs, controller) {
                            $compile(iElement)(scope);
                        }
                    };
                }
            }
        }
    ]);
    //.directive('actionView', function () {
    //    return {
    //        restrict: "E",
    //        scope: {
    //            actions: "=",
    //            click: "&",
    //            newcomment: "="
    //        },
    //        controller: [
    //            '$scope', function ($scope) {
    //                $scope.normalizeAction = function (action) {
    //                    return action.replace(/ /g, '-');
    //                }
    //                //        $scope.chooseAction = function (action) {
    //                //            $scope.click()(action);
    //                //        }
    //            }
    //        ],
    //        template: '<div class="comment-new row">\
    //                            <label class="col-xs-12 col-sm-2 control-label">Your comment</label>\
    //                            <div class="col-xs-12 col-sm-10">\
    //                                <textarea class="comment-new-textarea" ng-model="newcomment"></textarea>\
    //                            </div>\
    //                            <div class="col-xs-12 col-sm-10 col-sm-offset-2">\
    //                                <a class="btn btn-default action-button action-button-{{normalizeAction(action)}}" ng-repeat="action in actions" ng-click="click()(action)">\
    //                                    {{action}}\
    //                                </a>\
    //                            </div>\
    //                        </div>'
    //    }
    //});
