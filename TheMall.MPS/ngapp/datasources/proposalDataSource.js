﻿'use strict';
angular.module("app.datasources").factory('proposalDataSource',
['$resource', function ($resource) {
    var u = angular.crs.url.webApi("proposal/");
    //return $resource(u + ':id');
    return angular.extend($resource(u + ':id', null, {

        startprocess: { method: 'POST', url: u + ':id/startprocess' },
        worklist: { method: 'GET', url: u + 'worklist/:sn' },
        action: { method: 'POST', url: u + 'worklist/:sn' },
        getLineByLineId: { method: 'GET', url: u + 'getProposalDataFromLineId/:id' },
        approve: { method: 'GET', url: u + 'approve/:budget' },
        closeprop: { method: 'GET', url: u + 'closeproposal/:id/:username' }, //
        finalApprove: { method: 'GET', url: u + 'approve/:budget/:id/:unitcode' },
        deleteBy: { method: 'GET', url: u + 'deleteby/:id/:username' }

       // finalApprove: { method: 'GET', url: u + 'approve/:budget' }
        //init: { method: 'GET', url: u + 'process', isArray: true }
    }));
}]);
