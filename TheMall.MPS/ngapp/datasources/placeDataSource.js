﻿'use strict';
angular.module("app.datasources").factory('placeDataSource',
['$resource', function ($resource) {
    var url = angular.crs.url.webApi("place/");

    return angular.extend(
        $resource(url + ':id', null, {
            getByPlaceCode: { method: 'GET', url: url + 'getbyplacecode/:placeCode' },
            save: { method: 'POST', url: url + 'create/' },
            edit: { method: 'POST', url: url + 'update/' },
            deleted: { method: 'POST', url: url + 'delete/' }
        }));
}]);