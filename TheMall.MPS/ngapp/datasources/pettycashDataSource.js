﻿'use strict';
angular.module("app.datasources").factory('pettycashDataSource',
['$resource', function ($resource) {
    var u = angular.crs.url.webApi("pettycash/");
    //return $resource(u + ':id');
    return angular.extend($resource(u + ':id', null, {
        startprocess: { method: 'POST', url: u + ':id/startprocess' },
        worklist: { method: 'GET', url: u + 'worklist/:sn' },
        action: { method: 'POST', url: u + 'worklist/:sn' },
        getdatabindpettycash: { method: 'GET', url: u + 'getdatabindpettycash/:propLineid' },
        approve: { method: 'GET', url: u + 'approve/' },
        finalApprove: { method: 'GET', url: u + 'approve/:budget/:id/:unitcode' },
        deleteBy: { method: 'GET', url: u + 'deleteby/:id/:username' }
        //init: { method: 'GET', url: u + 'process', isArray: true }
    }));
}]);
