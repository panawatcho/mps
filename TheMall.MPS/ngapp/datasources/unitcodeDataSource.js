﻿'use strict';
angular.module("app.datasources").factory('unitcodeDataSource',
['$resource', function ($resource) {
    var url = angular.crs.url.webApi("unitcode/");
    return angular.extend($resource(url + ':id', null, {
        getByUnitCode: { method: 'GET', url: url + 'getbyunitcode/:unitCode' },
        save: { method: 'POST', url: url + 'create/' },
        edit: { method: 'POST', url: url + 'update/' },
        deleted: { method: 'POST', url: url + 'delete/' }
        //init: { method: 'GET', url: u + 'process', isArray: true }
    }));
}]);
