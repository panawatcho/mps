﻿'use strict';
angular.module("app.datasources").factory('aspNetRolesDataSource',
['$resource', function ($resource) {
    var url = angular.crs.url.webApi("aspnetroles/");
    //var proposalUrl = angular.crs.url.webApi("");

    return angular.extend(
        $resource(url + ':id', null, {
            getById: { method: 'GET', url: url + 'getbyid/:id' },
            getAll: { method: 'GET', url: url + 'getall/' },
            save: { method: 'POST', url: url + 'create/' },
            edit: { method: 'POST', url: url + 'update/' },
            deleted: { method: 'POST', url: url + 'delete/' }
        }));
}]);