﻿'use strict';
angular.module("app.datasources").factory('totalSaleLastYearDataSource',
['$resource', function ($resource) {
    var u = angular.crs.url.webApi("totalsalelastyear/");
    //return $resource(u + ':id');
    return angular.extend($resource(u + ':id', null, {
        search: { method: 'GET', url: u + 'getdata/:branchCode/:year'},
        save: { method: 'POST', url: u + 'create' },
        edit: { method: 'POST', url: u + 'update' },
        deleted:{ method: 'POST', url: u + 'delete' }
    }));
}]);