﻿'use strict';
angular.module("app.datasources").factory('searchDocDataSource',
['$resource', function ($resource) {
    var url = angular.crs.url.webApi("searchdoc/");
    //var proposalUrl = angular.crs.url.webApi("");

    return angular.extend(
        $resource(url + ':id', null, {
            search: { method: 'POST', url: url + 'SearchDoc/', isArray: true }
            //save: { method: 'POST', url: url + 'create/' },
            //edit: { method: 'POST', url: url + 'update/' },
            //deleted: { method: 'POST', url: url + 'delete/' }
        }));
}]);