﻿'use strict';
angular.module("app.datasources").factory('themeDataSource',
['$resource', function ($resource) {
    var url = angular.crs.url.webApi("theme/");

    return angular.extend(
        $resource(url + ':id', null, {
            save: { method: 'POST', url: url + 'create/' },
            edit: { method: 'POST', url: url + 'update/' },
            deleted: { method: 'POST', url: url + 'delete/' }
        }));
}]);