﻿'use strict';
angular.module("app.datasources").factory('accCashAdvanceDataSource',
['$resource', function ($resource) {
    var url = angular.crs.url.webApi("cashadvance/");

    return angular.extend(
        $resource(url + ':id', null, {
            getIndex: { method: 'GET', url: url + 'accindex/:username', isArray: true },
        }));
}]);