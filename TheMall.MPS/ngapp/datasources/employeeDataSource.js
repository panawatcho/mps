﻿'use strict';
angular.module("app.datasources").factory('employeeDataSource',
['$resource', function ($resource) {
    var url = angular.crs.url.webApi("employee/");

    return angular.extend(
        $resource(url + ':id', null, {
            searchByEmail: { method: 'GET', url: url + 'searchbyemail/:email', isArray: true },
            searchById: { method: 'GET', url: url + 'searchbyid/:empId' },
            save: { method: 'POST', url: url + 'create/' },
            edit: { method: 'POST', url: url + 'update/' },
            deleted: { method: 'POST', url: url + 'delete/' }
        }));
}]);