﻿'use strict';
angular.module("app.datasources").factory('memoincomeDataSource',
['$resource', function ($resource) {
    var u = angular.crs.url.webApi("memoincome/");
    //return $resource(u + ':id');
    return angular.extend($resource(u + ':id', null, {

        startprocess: { method: 'POST', url: u + ':id/startprocess' },
        worklist: { method: 'GET', url: u + 'worklist/:sn' },
        action: { method: 'POST', url: u + 'worklist/:sn' },
        finalApprover: { method: 'GET', url: u + 'getfinalapprover/:budget/:id/:unitcode' },
        deleteBy: { method: 'GET', url: u + 'deleteby/:id/:username' },
        getproposalref: { method: 'GET', url: u + 'getproposalref/' },
        getmemoincomeforaccount: { method: 'GET', url: u + 'accindex/:username', isArray: true },
    }));
}]);
