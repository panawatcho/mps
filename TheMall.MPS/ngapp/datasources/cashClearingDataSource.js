﻿'use strict';
angular.module("app.datasources").factory('cashClearingDataSource',
['$resource', function ($resource) {
    var url = angular.crs.url.webApi("cashclearing/");
    return angular.extend(
        $resource(
            url + ':id', null, {
                startprocess: { method: 'POST', url: url + ':id/startprocess' },
                worklist: { method: 'GET', url: url + 'worklist/:sn' },
                action: { method: 'POST', url: url + 'worklist/:sn' },
                getByCashAdvanceId: { method: 'GET', url: url + 'GetByCashAdvanceId/:id' },
                approve: { method: 'GET', url: url + 'approve/' },
                finalApprove: { method: 'GET', url: url + 'approve/:budget/:id/:unitcode' }
                //getByCashAdvanceModel: { method: "GET", url: url + 'CashClearingFromCashAdvance/:id' }
            }));
}]);