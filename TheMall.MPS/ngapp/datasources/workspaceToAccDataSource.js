﻿'use strict';

angular.module("app.datasources").service('workspaceToAccDataSource', 
    ['messageBox', '$resource',
function (messageBox, $resource) {

    var crudServiceBaseUrl = angular.crs.url.webApi("workspaceaccount"),
        service = {};

    service.worklist = {
        url: crudServiceBaseUrl,
        kendoDataSource: new kendo.data.DataSource({
            type: "json",
            transport: {
                read: {
                    //async: true,
                    url: crudServiceBaseUrl,
                    //dataType: "json"
                }
            },
            batch: false,
            serverPaging: false, // ปิดการทำ paging ฝั่ง server
            serverSorting: true,
            serverFiltering: true,
            pageSize: 5,
            schema: {
                data: function (data) { return data.Items; },
                total: function (data) { return data.Count; },
                model: kendo.data.Model.define({
                    id: "SN",
                    fields: {
                        StartDate: { type: "date" },
                        ProcessName: { type: "string" },
                    }
                })
            },
            filterable: {
                extra: true, //do not show extra filters
                operators: { // redefine the string operators
                    string: {
                        contains: "Contains",
                        doesnotcontain: "Does not contain",
                        eq: "Is Equal To",
                        neq: "Is not equal to",
                        startswith: "Starts With",
                        endswith: "Ends with"
                    }
                }
            },
            groupable: false,
            pageable: {
                refresh: true,
                pageSizes: [5, 10, 20, 50, 100]
            },
            reorderable: false,
            resizable: false,
            error: function (response) {
                messageBox.extractError(response.xhr.responseJSON);
            }
        })
    };

    return service;
}]);