﻿'use strict';
angular.module("app.datasources").factory('budgetYearPlanDataSource',
['$resource', function ($resource) {
    var url = angular.crs.url.webApi("budgetYearPlan/");
    //var proposalUrl = angular.crs.url.webApi("");

    return angular.extend(
        $resource(url + ':id', null, {
            searchByYearAndUnitCode: { method: 'GET', url: url + 'searchByYearAndUnitCode/:year/:unitcode' },
            save: { method: 'POST', url: url + 'create/' },
            edit: { method: 'POST', url: url + 'update/' },
            deleted: { method: 'POST', url: url + 'delete/'}
        }));
}]);