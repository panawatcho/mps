﻿'use strict';
angular.module("app.datasources").factory('cashAdvanceDataSource',
['$resource', function ($resource) {
    var url = angular.crs.url.webApi("cashadvance/");
    var proposalUrl = angular.crs.url.webApi("proposal/");

    return angular.extend(
        $resource(url + ':id', null, {
            startprocess: { method: 'POST', url: url + ':id/startprocess' },
            worklist: { method: 'GET', url: url + 'worklist/:sn' },
            action: { method: 'POST', url: url + 'worklist/:sn' },
            noLineId: { method: 'GET', url: url + 'NoLineId/:id' },
            getLineProposal: { method: 'GET', url: proposalUrl + 'getProposalLineData/:UnitCode/:parentId', isArray: true },
            getIndex: { method: 'GET', url: url + 'indexdata/:username', isArray: true },
            getLineByLineId: { method: 'GET', url: url + 'getProposalDataFromLineId/:id' },
            getdatabindadvance: { method: 'GET', url: url + 'getdatabindadvance/:propLineid' },
            getProposalFromProposalLineId: { method: 'GET', url: proposalUrl + 'getProposalDataFromLineId/:id' },
            approve: { method: 'GET', url: url + 'approve/' },
            finalApprove: { method: 'GET', url: url + 'approve/:budget/:id/:unitcode' },
            deleteBy: {method:'GET', url:url+'deleteby/:id/:username'}
        }));
}]);