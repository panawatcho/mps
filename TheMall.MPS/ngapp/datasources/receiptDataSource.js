﻿'use strict';
angular.module("app.datasources").factory('receiptDataSource',
['$resource', function ($resource) {
    var url = angular.crs.url.webApi("receipt/");
    //var proposalUrl = angular.crs.url.webApi("");

    return angular.extend(
        $resource(url + ':id', null, {
            save: { method: 'POST', url: url + 'createreceipt/' },
            deleteBy: { method: 'GET', url: url + 'deleteby/:id/:username' },
            get: { method: 'GET', url: url + 'get/:id' },
        }));
}]);