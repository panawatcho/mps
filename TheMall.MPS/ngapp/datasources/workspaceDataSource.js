﻿'use strict';

angular.module("app.datasources")
    .service('workspaceDataSource', 
    ['messageBox', '$resource',
function (messageBox, $resource) {
    var crudServiceBaseUrl = angular.crs.url.webApi("workspace");
    var service = {};
    service.worklist = {
       
            url: crudServiceBaseUrl,
        kendoDataSource: new kendo.data.DataSource({
            type: "odata",
            transport: {
                read: {
                    async: true,
                    url: crudServiceBaseUrl,
                    dataType: "json"
                }
            },
         
            batch: false,
            serverPaging: true,
            serverSorting: true,
            serverFiltering: true,
            pageSize: 20,
            schema: {
                data: function (data) { return data.Items; },
                total: function (data) { return data.Count; },
                model: kendo.data.Model.define({
                    id: "SN",
                    fields: {
                        StartDate: { type: "date" },
                    }
                })
            },
            error: function (response) {
                messageBox.extractError(response.xhr.responseJSON);
            }
        })
    };
    service.dataSource = $resource(crudServiceBaseUrl, null,
    {
        total: { method: 'GET', url: crudServiceBaseUrl + '/total' }
    });
    return service;
}]);