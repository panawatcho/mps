﻿'use strict';

angular.module("app.datasources")
    .factory('taskDataSource',
    [
        "appUrl", "$resource",
        function (appUrl, $resource) {
            var u = appUrl.api + "task/";

            //return {
            //    getByFolio: function() {
            //        return $
            //    }
            //}

            return $resource(u, null, {
                folio: { method: 'GET', url: u + 'folio/', isArray: true }
            });
        }
    ]
);