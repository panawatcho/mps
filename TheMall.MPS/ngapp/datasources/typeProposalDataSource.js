﻿'use strict';
angular.module("app.datasources").factory('typeProposalDataSource',
['$resource', function ($resource) {
    var url = angular.crs.url.webApi("typeproposal/");
    //return $resource(u + ':id');
    return angular.extend($resource(url + ':id', null, {
        getByTypeMemoCode: { method: 'GET', url: url + 'searchbyproposalcodetype/:typeMemoCode' },
        save: { method: 'POST', url: url + 'create/' },
        edit: { method: 'POST', url: url + 'update/' },
        deleted: { method: 'POST', url: url + 'delete/' }
    }));
}]);