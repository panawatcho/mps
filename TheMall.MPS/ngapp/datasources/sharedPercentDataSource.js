﻿'use strict';
angular.module("app.datasources").factory('sharedPercentDataSource',
['$resource', function ($resource) {
    var u = angular.crs.url.webApi("sharedpercent/");
    //return $resource(u + ':id');
    return angular.extend($resource(u + ':id', null, {
        search: { method: 'GET', url: u + 'search', isArray: true }
    }));
}]);
