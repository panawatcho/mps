﻿'use strict';
angular.module("app.datasources").factory('mailGroupDataSource',
['$resource', function ($resource) {
    var url = angular.crs.url.webApi("mailgroup/");
    //var proposalUrl = angular.crs.url.webApi("");

    return angular.extend(
        $resource(url + ':id', null, {
            getByGroupCode: { method: 'GET', url: url + 'getbygroupcode/:groupCode' },
            save: { method: 'POST', url: url + 'create/' },
            edit: { method: 'POST', url: url + 'update/' },
            deleted: { method: 'POST', url: url + 'delete/' }
        }));
}]);