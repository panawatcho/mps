﻿'use strict';
angular.module("app.datasources").factory('expensetopicDataSource',
['$resource', function ($resource) {
    var u = angular.crs.url.webApi("expensetopic/");
    return angular.extend($resource(u + ':id', null, {
        init: { method: 'GET', url: u, isArray: true },
        expensetopic: { method: 'GET', url: u + 'expensetopic/:expenseTopicCode' },
        save: { method: 'POST', url: u + 'create/' },
        edit: { method: 'POST', url: u + 'update/' },
        deleted:{ method: 'POST', url: u + 'delete/' }
    }));
}]);