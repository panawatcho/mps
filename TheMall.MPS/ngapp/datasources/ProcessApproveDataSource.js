﻿'use strict';
angular.module("app.datasources").factory('processApproveDataSource',
['$resource', function ($resource) {
    var url = angular.crs.url.webApi("processapprove/");
    //var proposalUrl = angular.crs.url.webApi("proposal/");

    return angular.extend(
        $resource(url + ':id', null, {
            save: { method: 'POST', url: url + 'create/' },
            edit: { method: 'POST', url: url + 'update/' },
            deleted: { method: 'POST', url: url + 'delete/' },
            get: { method: 'GET', url: url + 'search/:processCode' },
        }));
}]);