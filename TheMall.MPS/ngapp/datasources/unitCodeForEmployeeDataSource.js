﻿'use strict';
angular.module("app.datasources").factory('unitCodeForEmployeeDataSource',
['$resource', function ($resource) {
    var u = angular.crs.url.webApi("unitcodeforemployee/");
    //return $resource(u + ':id');
    return angular.extend($resource(u + ':id', null, {
        Search: { method: 'GET', url: u + 'GetInMaster/:empId/:unitCode', isArray: true },
        Save: { method: 'POST', url: u + 'Save' },
        Delete: { method: 'GET', url: u + 'DeleteInMaster/:empId/:unitCode' }
        //create: { method: 'POST', url: u + 'Create'},
       // update: { method: 'POST', url: u + 'Update'}
    }));
}]);
