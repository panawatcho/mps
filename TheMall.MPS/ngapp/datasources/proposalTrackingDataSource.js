﻿'use strict';
angular.module("app.datasources").factory('proposalTrackingDataSource',
['$resource', function ($resource) {
    var u = angular.crs.url.webApi("proposaltracking/");
    //return $resource(u + ':id');
    return angular.extend($resource(u + ':id', null, {

        getTracking: { method: 'GET', url: u + 'GetTracking/:proplineId', isArray: true },
        proposalTracking: { method: 'GET', url: u + 'proposalTracking/:proplineId' },
        depositTracking: { method: 'GET', url: u + 'depositTracking/:propId', isArray: true },
    }));
}]);
