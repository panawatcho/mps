﻿'use strict';
angular.module("app.datasources").factory('aspNetUserRolesDataSource',
['$resource', function ($resource) {
    var url = angular.crs.url.webApi("aspnetuserroles/");
    //var proposalUrl = angular.crs.url.webApi("");

    return angular.extend(
        $resource(url + ':id', null, {
            getByUserIdAndRoleId: { method: 'GET', url: url + 'getbyuseridandroleid/:roleId/:username' },
            getAll: { method: 'GET', url: url + 'getall/' },
            getByRoleId: { method: 'GET', url: url + 'getbyroleid/:roleId', isArray: true },
            save: { method: 'POST', url: url + 'create/' },
            edit: { method: 'POST', url: url + 'update/' },
            deleted: { method: 'POST', url: url + 'delete/' }
        }));
}]);