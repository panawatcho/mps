﻿'use strict';
angular.module("app.datasources").factory('allocateBasisDataSource',
['$resource', function ($resource) {
    var u = angular.crs.url.webApi("allocatebasis/");
    //return $resource(u + ':id');
    return angular.extend($resource(u + ':id', null, {
        search: { method: 'GET', url: u + 'search', isArray: true }
    }));
}]);
