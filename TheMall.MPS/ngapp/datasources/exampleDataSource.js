﻿'use strict';

angular.module("app.datasources")
    .factory('exampleDataSource',
    [
        "appUrl", "$resource", "$http", "documentTypeService",
        function (appUrl, $resource, $http, documentTypeService) {

            var service = {};

            service.url = {
                save: "//api.start.local/api/example/:id/",
                load: "//api.start.local/api/example/:id/",
                saveAttachment: "//api.start.local/api/example/:id/attachment/",
                loadAttachment: "//api.start.local/api/example/:id/attachment/",
                saveLineAttachment: "//api.start.local/api/exampleline/:id/attachment/",
                loadLineAttachment: "//api.start.local/api/exampleline/:id/attachment/"
            };
            documentTypeService.getDocumentTypes("example").then(function (resp) {
                service.documentTypesOnCreate = resp.data;
                return service.documentTypesOnCreate;
            });

            var documentTypePerActivity = {};
            service.documentTypesOn = function (activity) {
                if (!documentTypePerActivity[activity]) {
                    documentTypePerActivity[activity] = [];
                    documentTypeService.getDocumentTypes("example", activity).then(function (resp) {
                        for (var i = 0; i < resp.data.length; i++) {
                            documentTypePerActivity[activity].push(resp.data[i]);
                        }
                    });
                }
                return documentTypePerActivity[activity];
            }

            service.resource = $resource(service.url.save);

            return service;
        }
    ]
);