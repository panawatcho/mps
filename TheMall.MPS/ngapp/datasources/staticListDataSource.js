﻿'use strict';
app.factory('staticListDataSource',
['$resource', function ($resource) {
    var u = angular.crs.url.webApi("list/");
    return $resource(u, null,
    {
        ischeckbudget: { method: 'GET', url: u + 'ischeckbudget', cache: true, isArray: true },
    });
}]);