﻿'use strict';
angular.module("app.datasources").factory('finalApproverDataSource',
['$resource', function ($resource) {
    var url = angular.crs.url.webApi("finalapprover/");

    return angular.extend(
        $resource(url + ':id', null, {
            searchByKeys: { method: 'GET', url: url + 'searchbykeys/:processCode/:unitCode/:username' },
            save: { method: 'POST', url: url + 'create/' },
            edit: { method: 'POST', url: url + 'update/' },
            deleted: { method: 'POST', url: url + 'delete/' }
        }));
}]);