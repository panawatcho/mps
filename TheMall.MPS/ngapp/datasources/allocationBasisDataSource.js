﻿'use strict';
angular.module("app.datasources").factory('allocationBasisDataSource',
['$resource', function ($resource) {
    var url = angular.crs.url.webApi("allocationbasis/");
    //var proposalUrl = angular.crs.url.webApi("");

    return angular.extend(
        $resource(url + ':id', null, {
            search: { method: 'GET', url: url + 'search/:allocCode' },
            save: { method: 'POST', url: url + 'create/' },
            edit: { method: 'POST', url: url + 'update/' },
            deleted: { method: 'POST', url: url + 'delete/' },
            get: { method: 'GET', url: url + 'get/:id' },
        }));
}]);