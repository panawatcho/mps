﻿'use strict';
angular.module("app.datasources").factory('accountingDataSource',
['$resource', function ($resource) {
    var url = angular.crs.url.webApi("accounting/");
    //return $resource(u + ':id');
    return angular.extend($resource(url + ':id', null, {
        search: { method: 'GET', url: url + 'searchBranchCodeAndUsername/:branchCode/:username' },
        save: { method: 'POST', url: url + 'create/' },
        edit: { method: 'POST', url: url + 'update/' },
        deleted: { method: 'GET', url: url + 'DeleteInMaster/:branchCode/:username' }
    }));
}]);
