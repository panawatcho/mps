﻿'use strict';
angular.module("app.datasources").factory('memoDataSource',
['$resource', function ($resource) {
    var u = angular.crs.url.webApi("memo/");
    //return $resource(u + ':id');
    return angular.extend($resource(u + ':id', null, {
        startprocess: { method: 'POST', url: u + ':id/startprocess' },
        worklist: { method: 'GET', url: u + 'worklist/:sn' },
        action: { method: 'POST', url: u + 'worklist/:sn' },
        getdatabindmemo: { method: 'GET', url: u + 'getdatabindmemo/:propLineid' },
        approve: { method: 'GET', url: u + 'approve/' },
        finalApprover: { method: 'GET', url: u + 'getfinalapprover/:budget/:id/:unitcode/:type' },
        relatedall: { method: 'GET', url: u + 'related/', isArray: true },
        deleteBy: { method: 'GET', url: u + 'deleteby/:id/:username' }
        //init: { method: 'GET', url: u + 'process', isArray: true }
    }));
}]);
