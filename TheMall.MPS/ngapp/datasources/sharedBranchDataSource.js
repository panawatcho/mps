﻿'use strict';
angular.module("app.datasources").factory('sharedBranchDataSource',
['$resource', function ($resource) {
    var url = angular.crs.url.webApi("sharedbranch/");
    //return $resource(u + ':id');
    return angular.extend($resource(url + ':id', null, {
        init: { method: 'GET', url: url, isArray: true },
        getwithpercent: { method: 'GET', url: url + 'getwithpercent', isArray: true },
        getByBranchCode: { method: 'GET', url: url + 'SearchByBranchCode/:branchCode' },
        save: { method: 'POST', url: url + 'create/' },
        edit: { method: 'POST', url: url + 'update/' },
        deleted: { method: 'POST', url: url + 'delete/' }
    }));
}]);
