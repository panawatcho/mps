﻿'use strict';

angular.module("app.services")
    .factory('downloadService',
    [
        "$http", "$routeParams", "$sce", "$location", "messageBox",
        function ($http, $routeParams, $sce, $location, messageBox) {
            var service = {};

            var defaultFileName = function (contentType) {
                switch (contentType) {
                    case "image/png":
                        return 'image.png';
                    case "image/jpeg":
                    case "image/jpg":
                        return 'image.jpg';
                    case "application/pdf":
                        return 'download.pdf';
                    case "text/plain":
                        return 'download.txt';
                    default:
                        return 'download.bin';
                }
            }

            service.downloadFileGet = function (httpPath, config) {
                config = config || {};
                var newWin, blob, url;
                if (config.target) {
                    newWin = window.open('', config.target);
                }
                // Use an arraybuffer
                var promise = $http.get(httpPath, { responseType: 'arraybuffer' });
                promise.then(function (resp) {
                    var data = resp.data,
                        status = resp.status,
                        headers = resp.headers,
                        octetStreamMime = 'application/octet-stream',
                        success = false;

                    // Get the headers
                    headers = headers();

                    // Determine the content type from the header or default to "application/octet-stream"
                    var contentType = headers['content-type'] || octetStreamMime;

                    // Get the filename from the x-file-name header or default to "download.bin"
                    var filename = defaultFileName(contentType);
                    var xFilename = headers['x-file-name'];
                    if (xFilename) {
                        filename = decodeURIComponent(unescape(xFilename).replace(/\+/g, '%20'));
                    }

                    try {
                        // Try using msSaveBlob if supported
                        console.log("Trying saveBlob method ...");
                        var blob = new Blob([data], { type: contentType });
                        if (navigator.msSaveBlob)
                            navigator.msSaveBlob(blob, filename);
                        else {
                            // Try using other saveBlob implementations, if available
                            var saveBlob = navigator.webkitSaveBlob || navigator.mozSaveBlob || navigator.saveBlob;
                            if (saveBlob === undefined) throw "Not supported";
                            saveBlob(blob, filename);
                        }
                        console.log("saveBlob succeeded");
                        success = true;
                    } catch (ex) {
                        console.log("saveBlob method failed with the following exception:");
                        console.log(ex);
                    }

                    if (!success) {
                        // Get the blob url creator
                        var urlCreator = window.URL || window.webkitURL || window.mozURL || window.msURL;
                        if (urlCreator) {
                            // Try to use a download link
                            var link = document.createElement('a');
                            if ('download' in link) {
                                // Try to simulate a click
                                try {
                                    // Prepare a blob URL
                                    console.log("Trying download link method with simulated click ...");
                                    var blob = new Blob([data], { type: contentType });
                                    var url = urlCreator.createObjectURL(blob);

                                    if (config.target) {
                                        link.setAttribute('target', config.target);
                                    }
                                    //if (newWin) {
                                    //    newWin.location = url;
                                    //    return;
                                    //}
                                    link.setAttribute('href', url);

                                    // Set the download attribute (Supported in Chrome 14+ / Firefox 20+)
                                    if (!config.target || config.target !== "_blank") {
                                        link.setAttribute("download", filename);
                                    }

                                    // Simulate clicking the download link
                                    var event = document.createEvent('MouseEvents');
                                    event.initMouseEvent('click', true, true, window, 1, 0, 0, 0, 0, false, false, false, false, 0, null);
                                    link.dispatchEvent(event);
                                    console.log("Download link method with simulated click succeeded");
                                    success = true;

                                } catch (ex) {
                                    console.log("Download link method with simulated click failed with the following exception:");
                                    console.log(ex);
                                }
                            }

                            if (!success) {
                                // Fallback to window.location method
                                try {
                                    // Prepare a blob URL
                                    // Use application/octet-stream when using window.location to force download
                                    console.log("Trying download link method with window.location ...");
                                    var blob = new Blob([data], { type: octetStreamMime });
                                    var url = urlCreator.createObjectURL(blob);
                                    window.location = url;
                                    console.log("Download link method with window.location succeeded");
                                    success = true;
                                } catch (ex) {
                                    console.log("Download link method with window.location failed with the following exception:");
                                    console.log(ex);
                                }
                            }

                        }
                    }

                    if (!success) {
                        // Fallback to window.open method
                        console.log("No methods worked for saving the arraybuffer, using last resort window.open");
                        window.open(httpPath, '_blank', '');
                    }
                }
                , function (data, status) {
                    console.log("Request failed with status: " + status);
                    messageBox.error(data.statusText);

                    // Optionally write the error out to scope
                    //$scope.errorDetails = "Request failed with status: " + status;
                });
                return promise;
            };

            service.downloadFilePost = function (httpPath, body) {
                // Use an arraybuffer
                var promise = $http.post(httpPath, body, { responseType: 'arraybuffer' });
                promise.then(function (resp) {
                    var data = resp.data,
                        status = resp.status,
                        headers = resp.headers,
                        octetStreamMime = 'application/octet-stream',
                        success = false;

                    // Get the headers
                    headers = headers();

                    // Determine the content type from the header or default to "application/octet-stream"
                    var contentType = headers['content-type'] || octetStreamMime;

                    // Get the filename from the x-file-name header or default to "download.bin"
                    var filename = defaultFileName(contentType);
                    var xFilename = headers['x-file-name'];
                    if (xFilename) {
                        filename = decodeURIComponent(unescape(xFilename).replace(/\+/g, '%20'));
                    }

                    try {
                        // Try using msSaveBlob if supported
                        console.log("Trying saveBlob method ...");
                        var blob = new Blob([data], { type: contentType });
                        if (navigator.msSaveBlob)
                            navigator.msSaveBlob(blob, filename);
                        else {
                            // Try using other saveBlob implementations, if available
                            var saveBlob = navigator.webkitSaveBlob || navigator.mozSaveBlob || navigator.saveBlob;
                            if (saveBlob === undefined) throw "Not supported";
                            saveBlob(blob, filename);
                        }
                        console.log("saveBlob succeeded");
                        success = true;
                    } catch (ex) {
                        console.log("saveBlob method failed with the following exception:");
                        console.log(ex);
                    }

                    if (!success) {
                        // Get the blob url creator
                        var urlCreator = window.URL || window.webkitURL || window.mozURL || window.msURL;
                        if (urlCreator) {
                            // Try to use a download link
                            var link = document.createElement('a');
                            if ('download' in link) {
                                // Try to simulate a click
                                try {
                                    // Prepare a blob URL
                                    console.log("Trying download link method with simulated click ...");
                                    var blob = new Blob([data], { type: contentType });
                                    var url = urlCreator.createObjectURL(blob);
                                    link.setAttribute('href', url);

                                    // Set the download attribute (Supported in Chrome 14+ / Firefox 20+)
                                    link.setAttribute("download", filename);

                                    // Simulate clicking the download link
                                    var event = document.createEvent('MouseEvents');
                                    event.initMouseEvent('click', true, true, window, 1, 0, 0, 0, 0, false, false, false, false, 0, null);
                                    link.dispatchEvent(event);
                                    console.log("Download link method with simulated click succeeded");
                                    success = true;

                                } catch (ex) {
                                    console.log("Download link method with simulated click failed with the following exception:");
                                    console.log(ex);
                                }
                            }

                            if (!success) {
                                // Fallback to window.location method
                                try {
                                    // Prepare a blob URL
                                    // Use application/octet-stream when using window.location to force download
                                    console.log("Trying download link method with window.location ...");
                                    var blob = new Blob([data], { type: octetStreamMime });
                                    var url = urlCreator.createObjectURL(blob);
                                    window.location = url;
                                    console.log("Download link method with window.location succeeded");
                                    success = true;
                                } catch (ex) {
                                    console.log("Download link method with window.location failed with the following exception:");
                                    console.log(ex);
                                }
                            }

                        }
                    }

                    if (!success) {
                        // Fallback to window.open method
                        console.log("No methods worked for saving the arraybuffer, using last resort window.open");
                        window.open(httpPath, '_blank', '');
                    }
                }
                , function (data, status) {
                    console.log("Request failed with status: " + status);
                    messageBox.error(data.statusText);

                    // Optionally write the error out to scope
                    //$scope.errorDetails = "Request failed with status: " + status;
                });
                return promise;
            };

            //service.openFile = function (httpPath) {
            //    var promise = $http.get(httpPath, { responseType: 'arraybuffer' });
            //    promise.then(function(resp) {
            //        var data = resp.data,
            //            status = resp.status,
            //            headers = resp.headers,
            //            octetStreamMime = 'application/octet-stream',
            //            success = false;

            //        headers = headers();

            //        var contentType = headers['content-type'] || octetStreamMime;
            //        var filename = defaultFileName(contentType);
            //        var xFilename = headers['x-file-name'];
            //        if (xFilename) {
            //            filename = decodeURIComponent(unescape(xFilename));
            //        }


            //    });
            //}

            return service;
        }
    ]
);