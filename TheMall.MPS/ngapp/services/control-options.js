﻿"use strict";
angular.module("app.services")
    .factory("controlOptions", ["$q", '$routeParams', function ($q, $routeParams) {
        var factory = {};

        var dropdown = function () {
            var defaultOptions = function (optionLabel) {
                return {
                    dataSource: {},
                    dataTextField: "text",
                    dataValueField: "value",
                    valuePrimitive : false,
                    optionLabel: optionLabel || (optionLabel === false ? undefined : "please select")
                }
            };
            var dropdownKendoOption = function (url,data) {
                if (data) {
                    return {
                        dataSource: {
                            type: "json",
                            data: data
                        },
                        dataTextField: "",
                        dataValueField: "",
                        optionLabel: "please select"
                    };
                }
                return {
                    dataSource: {
                        type: "json",
                        transport: {
                            serverFiltering: true,
                            read: {
                                url: url,
                                data: data
                            }
                        }
                    },
                    dataTextField: "",
                    dataValueField: "",
                    optionLabel: "please select"
                }
            };
            return function (typeOrUrl) {
                typeOrUrl = typeOrUrl.toLowerCase();
                switch (typeOrUrl) {
                    case "typeproposal":
                        return function () {
                            var options = dropdownKendoOption(angular.crs.url.webApi('typeproposal'));
                            options.dataTextField = 'TypeProposalName';
                            options.dataValueField = 'TypeProposalCode';
                            options.template = '#=data.TypeProposalName#';
                            options.valueTemplate = '#=data.TypeProposalName#';
                            //options.template = '#=data.TypeProposalCode# - #=data.TypeProposalName#';
                            //options.valueTemplate = '#=data.TypeProposalCode# - #=data.TypeProposalName#';
                            return options;
                        }
                    case "unitcode":
                        return function () {
                            var options = dropdownKendoOption(angular.crs.url.webApi('unitcode'));
                            options.dataTextField = 'UnitCode';
                            options.dataValueField = 'UnitName';
                            //options.template = '#=data.UnitName#';
                            //options.valueTemplate = '#=data.UnitName#';
                            options.template = '#=data.UnitCode# - #=data.UnitName#';
                            options.valueTemplate = '#=data.UnitCode# - #=data.UnitName#';
                            options.filter = "contains";
                            options.filtering = function (ev) {
                                if (ev.filter) {
                                    var filterValue = ev.filter.value;
                                    ev.preventDefault();

                                    this.dataSource.filter({
                                        logic: "or",
                                        filters: [
                                          {
                                              field: "UnitName",
                                              operator: "contains",
                                              value: filterValue
                                          },
                                          {
                                              field: "UnitCode",
                                              operator: "contains",
                                              value: filterValue
                                          }
                                        ]
                                    });
                                } else {
                                    ev.preventDefault();
                                }
                            }
                            options.autoWidth = true;
                            options.dataBound = function(e) {
                                var listContainer = e.sender.list.closest(".k-list-container");
                                listContainer.width(listContainer.width() + kendo.support.scrollbar());
                            }
                            return options;
                        }/////
                    case "employee":
                        return function () {
                            var options = dropdownKendoOption(angular.crs.url.webApi('Employee/search'));
                            options.dataTextField = 'Username';
                            options.dataValueField = 'EmpId';
                            options.template = '#=data.Username# - #=data.FullName#';
                            options.valueTemplate = '#=data.Username# - #=data.FullName#';
                            options.filter = "contains";
                            options.filtering = function (ev) {
                                if (ev.filter) {
                                    var filterValue = ev.filter.value;
                                    ev.preventDefault();
                                    
                                    this.dataSource.filter({
                                        logic: "or",
                                        filters: [
                                          {
                                              field: "FullName",
                                              operator: "contains",
                                              value: filterValue
                                          },
                                          {
                                              field: "Email",
                                              operator: "contains",
                                              value: filterValue
                                          },
                                           {
                                               field: "Username",
                                               operator: "contains",
                                               value: filterValue
                                           },
                                        ]
                                    });
                                } else {
                                    ev.preventDefault();
                                }
                            }
                            return options;
                        }
                    case "unitcodeap":
                        return function () {
                            var options = dropdownKendoOption(angular.crs.url.webApi('unitcode'));
                            options.dataTextField = 'UnitName';
                            options.dataValueField = 'UnitCode';
                            options.template = '#=data.UnitCode#';
                            options.valueTemplate = '#=data.UnitCode#';
                            //options.template = '#=data.UnitCode# - #=data.UnitName#';
                            //options.valueTemplate = '#=data.UnitCode# - #=data.UnitName#';
                            options.filter = "contains";
                            options.filtering = function (ev) {
                                if (ev.filter) {
                                    var filterValue = ev.filter.value;
                                    ev.preventDefault();

                                    this.dataSource.filter({
                                        logic: "or",
                                        filters: [
                                          {
                                              field: "UnitName",
                                              operator: "contains",
                                              value: filterValue
                                          },
                                          {
                                              field: "UnitCode",
                                              operator: "contains",
                                              value: filterValue
                                          }
                                        ]
                                    });
                                } else {
                                    ev.preventDefault();
                                }
                            }
                            return options;
                        }

                    case "typememo":
                        return function () {
                            var options = dropdownKendoOption(angular.crs.url.webApi('typememo'));
                            options.dataTextField = 'TypeMemolName';
                            options.dataValueField = 'TypeMemoCode';
                            options.template = '#=data.TypeMemoName#';
                            options.valueTemplate = '#=data.TypeMemoName#';
                            //options.template = '#=data.TypeMemoCode# - #=data.TypeMemoName#';
                            //options.valueTemplate = '#=data.TypeMemoCode# - #=data.TypeMemoName#';
                           
                            return options;
                        }
                    case "expensetopic":
                        return function () {
                            var options = dropdownKendoOption(angular.crs.url.webApi('expensetopic'));
                            options.dataTextField = 'ExpenseTopicName';
                            options.dataValueField = 'ExpenseTopicCode';
                            options.template = '#=data.ExpenseTopicCode# - #=data.ExpenseTopicName#';
                            options.valueTemplate = '#=data.ExpenseTopicCode# - #=data.ExpenseTopicName#';
                            
                            return options;
                        }
                    case "apcode":
                        return function () {
                            var options = dropdownKendoOption(angular.crs.url.webApi('apcode'));
                            options.dataTextField = 'APName';
                            options.dataValueField = 'APCode';
                            options.template = '#=data.APCode# - #=data.APName#';
                            options.valueTemplate = '#=data.APCode# - #=data.APName#';
                            return options;
                        }
                    case "processdeposit":
                        return function () {
                            var options = defaultOptions();
                            options.dataSource.data = [
                                { text: "waiting", value: "waiting" },
                                { text: "completed", value: "completed" }
                            ];
                            options.dataTextField = 'text';
                            options.dataValueField = 'value';
                           // options.template = '#=data.text#';
                            //options.valueTemplate = '#=data.text#';
                            return options;
                        }
                    case "empstatus":
                        return function () {
                            var options = defaultOptions();
                            options.dataSource.data = [
                                { text: "Active", value: "Active" },
                                { text: "Hold", value: "Hold" }
                            ];
                            options.dataTextField = 'value';
                            options.dataValueField = 'value';
                            options.valuePrimitive = true;
                            // options.template = '#=data.text#';
                            //options.valueTemplate = '#=data.text#';
                            return options;
                        }
                    case "typeap":
                        return function () {
                            var options = defaultOptions();
                            options.dataSource.data = [
                                { text: "Advertising", value: "A" },
                                { text: "Promotion", value: "P" },
                                { text: "P & L", value: "L" }
                            ];
                            options.dataTextField = 'text';
                            options.dataValueField = 'value';
                            options.valuePrimitive = true;
                            options.template = '#=data.value# - #=data.text#';
                            options.valueTemplate = '#=data.value# - #=data.text#';
                            return options;
                        }
                    case "processapprove":
                        return function () {
                            var options = dropdownKendoOption(angular.crs.url.webApi('processapprove'));
                            options.dataTextField = 'ProcessCode';
                            options.dataValueField = 'ProcessCode';
                            options.template = '#=data.ProcessCode#';
                            options.valueTemplate = '#=data.ProcessCode#';
                            return options;
                        }
                    case "sharedbranch":
                        return function () {
                            var options = dropdownKendoOption(angular.crs.url.webApi('SharedBranch'));
                            options.dataTextField = 'BranchName';
                            options.dataValueField = 'BranchCode';
                            options.template = '#=data.BranchCode# - #=data.BranchName#';
                            options.valueTemplate = '#=data.BranchCode# - #=data.BranchName#';
                            return options;
                        }
                    case "aspnetroles":
                        return function () {
                            var options = dropdownKendoOption(angular.crs.url.webApi('aspnetroles/getall'));
                            options.dataTextField = 'Name';
                            options.dataValueField = 'Id';
                            options.template = '#=data.Id# - #=data.Name#';
                            options.valueTemplate = '#=data.Id# - #=data.Name#';
                            return options;
                        }
                    case "category":
                        return function () {
                            var options = defaultOptions();
                            options.dataSource.data = [
                                { text: "Deposit Proposal", value: "depositproposal" },
                                { text: "Income Other Source", value: "otherincome" }
                            ];
                            options.dataTextField = 'text';
                            options.dataValueField = 'value';
                            options.valuePrimitive = true;
                            //options.template = '#=data.text#';
                            //options.valueTemplate = '#=data.text#';
                            return options;
                        }
                    default:
                        return function () {
                            var config = {
                                dataSource: {
                                    type: "json",
                                    transport: {
                                        serverFiltering: true,
                                        read: {
                                            url: typeOrUrl,
                                            data: data
                                        }
                                    }
                                },
                                dataTextField: "",
                                dataValueField: "",
                                optionLabel: "please select"
                            }

                            return config;
                        };
                }
            }
        }
        factory.dropdownKendoOption = factory.dropdown = dropdown();
        //var dropdownKendoOption = function (url, data) {
        //    if (data) {
        //        return {
        //            dataSource: {
        //                type: "json",
        //                data: data
        //            },
        //            dataTextField: "",
        //            dataValueField: "",
        //            optionLabel: "please select"
        //        };
        //    }
        //    return {
        //        dataSource: {
        //            type: "json",
        //            transport: {
        //                serverFiltering: true,
        //                read: {
        //                    url: url,
        //                    data: data
        //                }
        //            }
        //        },
        //        dataTextField: "",
        //        dataValueField: "",
        //        optionLabel: "please select"
        //    }
        //};
        //factory.dropdownKendoOption = dropdownKendoOption;
     

        var combobox = function (url) {
            return {
                dataSource: new kendo.data.DataSource({
                    //type: "json",
                    serverFiltering: true,
                    transport: {
                        read: {
                            async: true,
                            url: url,
                            dataType: "json",
                            //beforeSend: function (req) {
                            //    req.setRequestHeader('Authorization', "Bearer " + token);
                            //}
                        },
                        parameterMap: function (data, type) {
                            debugger;
                            if (type === "read") {
                                if (data.filter != undefined && data.filter.filters.length > 0 && data.filter.filters[0].value != null && $.trim(data.filter.filters[0].value) != '') {
                                    return {
                                      
                                        name: $.trim(data.filter.filters[0].value)
                                    }
                                } else {
                                    return data;
                                }
                            }
                        }
                    },
                    error: function (e) {
                        alert(e.xhr.responseText);
                    }
                }),
                dataTextField: '',
                dataValueField: '',
  
                filter: "contains",
                autoBind: false,
                minLength: 1
            };
        }
        factory.comboboxKendoOption = combobox;
        factory.combobox = combobox;

        var gridKendoOption = function (url, params) {
            params = angular.isDefined(params) ? params : {};
            var fields = {
                CreatedDate: { type: "date" }
            };
            if (angular.isDefined(params.fields)) {
                angular.extend(fields, params.fields);
            }
            return {
                dataSource: new kendo.data.DataSource({
                    type: "odata",
                    transport: params.transport || {
                        read: {
                            async: true,
                            url: url,
                            dataType: "json",
                            data: params.readData || undefined
                            //beforeSend: function (req) {
                            //    req.setRequestHeader('Authorization', "Bearer " + token);
                            //}
                        },
                        parameterMap: params.parameterMap || undefined
                    },
                    filter: (params.filters) ? params.filters : null,
                    sort: (params.sorts) ? params.sorts : null,
                    batch: false,
                    serverPaging: true,
                    serverSorting: true,
                    serverFiltering: true,
                    pageSize: 10,
                    schema: {
                        data: function (data) { return data.value; },
                        total: function (data) { return data["odata.count"]; },
                        model: kendo.data.Model.define({
                            id: "Id",
                            fields: fields
                        })
                    },
                    error: function (e) {
                        try {
                            var resp = angular.fromJson(e.xhr.responseText);
                            alert(resp.Message);
                        } catch (e) {
                            alert(e.xhr.responseText);
                        }
                    },
                    group: params.group
                }),
                editable: false,
                // filterable: true,
                filterable: {
                    extra: true, //do not show extra filters
                    operators: { // redefine the string operators
                        string: {
                            contains: "Contains",
                            //doesnotcontain: "Does not contain",
                            //eq: "Is Equal To",
                            //neq: "Is not equal to",
                            //startswith: "Starts With",
                            //endswith: "Ends with"

                        }
                    }
                },
                groupable: false,
                pageable: {
                    refresh: true,
                    pageSizes: [5, 10, 20, 50, 100]
                },
                reorderable: false,
                resizable: false,
                scrollable: angular.isDefined(params.scrollable) ? params.scrollable : true,
                selectable: "row",
                sortable: {
                    allowUnsort: true,
                    mode: "single"
                },
                columns: params.columns || [],
                height: params.height || 500,
                mobile: params.mobile || false,


            };
        }
        factory.gridKendoOption = gridKendoOption;

        var popupSearch = function () {
            var _defaultRawTemplate = function (options, array, prefix) {
                var str = '', i, l = array.length, item;
                if (l <= 1) {
                    item = array[0];
                    if (prefix) {
                        item = prefix + '.' + array[0];
                    }
                    if (prefix) {
                        return '#if(' + prefix + '&&' + item + '!=null){##=' + item + '##}#';
                    } else {
                        return '#if(' + item + '!=null){##=' + item + '##}#';
                    }
                }
                for (i = 0; i < l; i++) {
                    item = array[i];
                    if (prefix) {
                        item = prefix + '.' + array[i];
                    }
                    if (i === 0) {
                        if (prefix) {
                            str += '#if(' + prefix + '&&' + item + '!=null){##=' + item + '# | ';
                        } else {
                            str += '#if(' + item + '!=null){##=' + item + '# | ';
                        }
                    } else {
                        //str += ' | #= ' + item + ' #';
                        str += '#= ' + item + ' # | ';
                    }
                }
                return str.substr(0, str.length - 3) + '#}#';
            },
                _defaultDisplayTemplate = function (options, array, prefix) {
                    if (angular.isArray(array)) {
                        return "<div class='row'><div class='col-md-12'>" + options.rawTemplateFactory(options, array, prefix) + "</div></div>";
                    } else {
                        return "<div class='row'><div class='col-md-12'>" + array + "</div></div>";
                    }
                },
                _defaultInlineTemplate = function (options, array) {
                    return function (field) {
                        var prefix = 'data.' + field;
                        return '#if(data&&' + prefix + '){#' + options.rawTemplateFactory(options, array, prefix) + '#}#';
                    }
                },
                _newGridOptions = function (url) {
                    return {
                        dataSource: new kendo.data.DataSource({
                            //type: "json",
                            transport: {
                                read: {
                                    async: true,
                                    url: url,
                                    dataType: "json"
                                },
                                parameterMap: function (data, type) {
                                    if (type === "read") {
                                        if (data.filter != undefined) {
                                            var o = {};
                                            if (data.filter.filters.length > 0 && data.filter.filters[0].value != null && $.trim(data.filter.filters[0].value) != '') {
                                                $.extend(o, { code: $.trim(data.filter.filters[0].value) });
                                            }
                                            if (data.filter.extraData != undefined) {
                                                for (var i = 0; i < data.filter.extraData.length; i++) {
                                                    o[data.filter.extraData[i].field] = data.filter.extraData[i].value;
                                                }
                                            }

                                            return o;
                                        } else {
                                            return data;
                                        }

                                    }
                                }
                            },
                            batch: false,
                            serverPaging: false,
                            serverSorting: false,
                            serverFiltering: false,
                            //pageSize: 10, แบ่งหน้า
                            schema: {
                                data: function (data) { return data; },
                                total: function (data) { return data.length; },
                            },
                            error: function (e) {
                                try {
                                    var resp = angular.fromJson(e.xhr.responseText);
                                    alert(resp.Message);
                                } catch (e) {
                                    alert(e.xhr.responseText);
                                }
                                //alert(e.xhr.responseText);
                                //$scope.$rootScope.messageBox.error(e.xhr.responseText);
                            }
                        }),
                        editable: false,
                        filterable: false,
                        groupable: false,
                        pageable: true,
                        reorderable: false,
                        resizable: false,
                        scrollable: true,
                        selectable: 'row',
                        sortable: {
                            allowUnsort: true,
                            mode: "single"
                        },
                        navigatable: true,
                        autoBind: false
                    }
                };
            return function (typeOrUrl, o) {
                var options = angular.extend({}, o, {
                    multiple: o ? o.multiple : false,
                    popupSearchFields: [
                        { name: 'code', title: 'รหัส' },
                        { name: 'name', title: 'ชื่อ' }
                    ]
                });
                var type = typeOrUrl.toLowerCase(),
                    _gridOptions,
                    columnTemplate;

                switch (type) {

                    case 'employee':
                        _gridOptions = _newGridOptions(angular.crs.url.webApi("employee/search"));
                        _gridOptions.columns = [
                            { field: 'ThFName', title: 'ชื่อ' },
                            { field: 'ThLName', title: 'นามสกุล' },
                            { field: 'Username', title: 'Username' },
                            { field: 'DepartmentName', title: 'Department Name' },
                            { field: 'PositionName', title: 'Position Name' }
                        ];
                        options.title = 'พนักงาน';
                        columnTemplate = ['Username', 'ThFName', 'ThLName'];
                        options.popupSearchFields = [
                            { name: 'name', title: 'ชื่อ - นามสกุล' },
                            { name: 'code', title: 'Username' }
                        ];
                        options.rawTemplateFactory = function (options, array, prefix) {
                            //return '#if(' + prefix + '){##=' + prefix + '.' + array[0] + ' # | #=' + prefix + '.' + array[1] + "# #=" + prefix + '.' + array[2] + "##}#";
                            return '#if(' + prefix + '){##=' + prefix + '.' + array[1] + "# #=" + prefix + '.' + array[2] + "##}#";
                        }
                        break;
                    case 'employeefor':
                        _gridOptions = _newGridOptions(angular.crs.url.webApi("employee/searchFor"));
                        _gridOptions.columns = [
                            { field: 'ThFName', title: 'ชื่อ' },
                            { field: 'ThLName', title: 'นามสกุล' },
                            { field: 'Username', title: 'Username' },
                            { field: 'DepartmentName', title: 'Department Name' },
                            { field: 'PositionName', title: 'Position Name' }
                        ];
                        options.title = 'พนักงาน';
                        columnTemplate = ['Username', 'ThFName', 'ThLName'];
                        options.popupSearchFields = [
                            { name: 'name', title: 'ชื่อ - นามสกุล' },
                            { name: 'code', title: 'Username' }
                        ];
                        options.rawTemplateFactory = function (options, array, prefix) {
                            //return '#if(' + prefix + '){##=' + prefix + '.' + array[0] + ' # | #=' + prefix + '.' + array[1] + "# #=" + prefix + '.' + array[2] + "##}#";
                            return '#if(' + prefix + '){##=' + prefix + '.' + array[1] + "# #=" + prefix + '.' + array[2] + "##}#";
                        }
                        break;
                    case 'accounting':
                        _gridOptions = _newGridOptions(angular.crs.url.webApi("employee/accounting"));
                        _gridOptions.columns = [
                            { field: 'ThFName', title: 'ชื่อ' },
                            { field: 'ThLName', title: 'นามสกุล' },
                            { field: 'Username', title: 'Username' },
                            { field: 'DepartmentName', title: 'Department Name' },
                            { field: 'PositionName', title: 'Position Name' }
                        ];
                        options.title = 'ฝ่ายบัญชี';
                        columnTemplate = ['Username', 'ThFName', 'ThLName'];
                        options.popupSearchFields = [
                            { name: 'name', title: 'ชื่อ - นามสกุล' },
                            { name: 'code', title: 'Username' }
                        ];
                        options.rawTemplateFactory = function (options, array, prefix) {
                            //return '#if(' + prefix + '){##=' + prefix + '.' + array[0] + ' # | #=' + prefix + '.' + array[1] + "# #=" + prefix + '.' + array[2] + "##}#";
                            return '#if(' + prefix + '){##=' + prefix + '.' + array[1] + "# #=" + prefix + '.' + array[2] + "##}#";
                        }
                        break;
                    case 'unitcode':
                        _gridOptions = _newGridOptions(angular.crs.url.webApi("unitcode/search/"));
                        _gridOptions.columns = [
                            { field: 'UnitCode', title: 'Unit Code', width: '150px' },
                            { field: 'UnitName', title: 'Unit Name' }
                        ];
                        options.title = 'Search Unit Code';
                        columnTemplate = ['UnitCode', 'UnitName'];
                        options.popupSearchFields = [
                            { name: 'code', title: 'Unit Code' },
                            { name: 'name', title: 'Unit Name' }
                        ];
                        options.rawTemplateFactory = function (options, array, prefix) {
                            return '#if(' + prefix + '){##=' + prefix + '.' + array[0] + ' # | #=' + prefix + '.' + array[1]  + "##}#";
                        }
                        break;

                    case 'expensetopic':
                        _gridOptions = _newGridOptions(angular.crs.url.webApi("expensetopic/search/"));
                        _gridOptions.columns = [
                          
                                           { field: 'ExpenseTopicCode', title: 'ExpenseTopicCode', width: '100px' },
                                           { field: 'ExpenseTopicName', title: 'ExpenseTopicName', width: '150px' },
                                           { field: 'Description', title: 'Description', width: '200px' },
                                           { field: 'TypeAP', title: 'TypeAP', width: '50px' },
                      
                        ];
                        options.title = 'Search Expense Topic';
                        columnTemplate = ['ExpenseTopicCode', 'ExpenseTopicName'];
                        options.popupSearchFields = [
                            { name: 'code', title: 'ExpenseTopic Code' },
                            //{ name: 'name', title: 'Unit Name' }
                        ];
                        options.rawTemplateFactory = function (options, array, prefix) {
                            return '#if(' + prefix + '){##=' + prefix + '.' + array[0] + ' # | #=' + prefix + '.' + array[1] + "##}#";
                        };
                        options.multiple = true;
                        break;

                    case 'depositnumber':
                        _gridOptions = _newGridOptions(angular.crs.url.webApi("documentnumber/deposit/"));
                        _gridOptions.columns = [
                            { field: 'DocumentNumber', title: 'Deposit Number', width: '150px' },
                            { field: 'Title', title: 'Description' },
                              { field: 'Balance', title: 'Balance' }
                        ];
                        options.title = 'Search Deposit Number';
                        columnTemplate = ['DocumentNumber', 'Title'];
                        options.popupSearchFields = [
                            { name: 'code', title: 'Deposit Number' },
                            { name: 'name', title: 'Description' },

                        ];
                        options.rawTemplateFactory = function (options, array, prefix) {
                            return '#if(' + prefix + '){##=' + prefix + '.' + array[0] + ' # | #=' + prefix + '.' + array[1] + "##}#";
                        }
                        break;
                    case 'proposalnumberref':
                        _gridOptions = _newGridOptions(angular.crs.url.webApi("documentnumber/proposal/"));
                        _gridOptions.columns = [
                            { field: 'DocumentNumber', title: 'Proposal Number', width: '150px' },
                            { field: 'Title', title: 'Proposal Title' },
                            //{ field: 'Balance', title: 'Balance' }
                        ];
                        options.title = 'Search Proposal Number';
                        columnTemplate = ['DocumentNumber', 'Title'];
                        options.popupSearchFields = [
                            { name: 'code', title: 'Proposal Number' },
                            { name: 'name', title: 'Proposal Title' },
                            { name: 'unitCode', title: 'Unit Code', hidden: true }
                        ];
                        options.rawTemplateFactory = function (options, array, prefix) {
                            //return '#if(' + prefix + '){##=' + prefix + '.' + array[0] + ' # | #=' + prefix + '.' + array[1] + "##}#"; Base
                            return '#if(' + prefix +'.'+ array[0]+ '){##=' + prefix + '.' + array[0] + ' # | #=' + prefix + '.' + array[1] + '##}' +
                                'else{##=' + prefix + '.' + array[1] + '##}#';//Custom
                        }
                        break;
                    case 'incomedepositnumber':
                        _gridOptions = _newGridOptions(angular.crs.url.webApi("documentnumber/incomedeposit/"));
                        _gridOptions.columns = [
                            { field: 'DocumentNumber', title: 'Deposit Number', width: '150px' },
                            { field: 'Title', title: 'Description' },
                            { field: 'Balance', title: 'Balance' }
                        ];
                        options.title = 'Search Deposit Number';
                        columnTemplate = ['DocumentNumber', 'Title'];
                        options.popupSearchFields = [
                            { name: 'code', title: 'Deposit Number' },
                            { name: 'name', title: 'Description' }
                        ];
                        options.rawTemplateFactory = function (options, array, prefix) {
                            return '#if(' + prefix + '){##=' + prefix + '.' + array[0] + ' # | #=' + prefix + '.' + array[1] + "##}#";
                        }
                        break;
                    case 'proposalref':
                        _gridOptions = _newGridOptions(angular.crs.url.webApi("memoincome/getproposalref/"));
                        _gridOptions.columns = [
                            { field: 'DocumentNumber', title: 'Proposal Number', width: '150px' },
                            { field: 'Title', title: 'Proposal Title' },
                        ];
                        options.title = 'Search Proposal Number';
                        columnTemplate = ['DocumentNumber', 'Title'];
                        options.popupSearchFields = [
                            { name: 'code', title: 'Proposal Number' },
                            { name: 'name', title: 'Proposal Title' },
                            { name: 'unitCode', title: 'Unit Code', hidden: true },
                            { name: 'category', title: 'category', hidden: true }
                        ];
                        options.rawTemplateFactory = function (options, array, prefix) {
                            //return '#if(' + prefix + '){##=' + prefix + '.' + array[0] + ' # | #=' + prefix + '.' + array[1] + "##}#"; Base
                            return '#if(' + prefix + '.' + array[0] + '){##=' + prefix + '.' + array[0] + ' # | #=' + prefix + '.' + array[1] + '##}' +
                                'else{##=' + prefix + '.' + array[1] + '##}#';//Custom
                        }
                        break;
                    default:
                        options.popupSearchFields = null;
                        _gridOptions = _newGridOptions(typeOrUrl);
                        break;
                }
                options.gridOptions = _gridOptions;
                if (angular.isUndefined(options.placeholderText)) {
                    options.placeholderText = "<div style='text-align:right;'>" +
                        "<i class='fa fa-search-plus fa-lg'></i></div>";
                }
                //options.buttonText = 'Lookup';
                if (columnTemplate != null) {
                    if (!options.rawTemplateFactory) {
                        options.rawTemplateFactory = _defaultRawTemplate;
                    }
                    if (!options.templateFactory) {
                        options.templateFactory = _defaultDisplayTemplate;
                    }
                    if (!options.inlineTemplateFactory) {
                        options.inlineTemplateFactory = _defaultInlineTemplate;
                    }
                    options.rawTemplate = options.rawTemplateFactory(options, columnTemplate, 'data');
                    options.template = options.templateFactory(options, columnTemplate, 'data');
                    options.inlineTemplate = options.inlineTemplateFactory(options, columnTemplate, 'data');
                }
                return function () {
                    return options;
                };
            }
        }
        factory.popupSearch = factory.popupSearchOptions = popupSearch();

        return factory;
    }]);