﻿"use strict";
angular.module("app.services")
    .factory("menuService",
        [
           "appUrl", "$resource", "authService", "menuServiceData",
            function (appUrl, $resource, authService, menuServiceData) {
                var u = appUrl.api + "menu/";
                var dataSource = $resource(u);

                var service = {};
                service.getMenu = function() {
                    if (authService.authentication.isAuth) {
                        if (!menuServiceData.data.menu) {
                            menuServiceData.setData(dataSource.query().$promise);
                        }
                        return menuServiceData.data.menu;
                    }
                    return null;
                };
                return service;
            }
        ]
    )
    .service("menuServiceData",
        ["messageBox", "$log", "gettextCatalog",
            function (messageBox, $log, gettextCatalog) {
                var that = this;
                that.data = {};

                that.clear = function () {
                    delete that.data.menu;
                };

                that.setData = function (menu) {
                    that.data.menu = [];
                    menu.then(function(resp) {
                        that.data.menu = resp;
                        if (!resp.length) {
                            $log.info(gettextCatalog.getString('There is no menu defined for this user.'));
                        }
                    },
                    messageBox.extractError);
                };
            }
        ]
    );