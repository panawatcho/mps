﻿"use strict";
angular.module("app.services")
    .service("langService",
        [ "gettextCatalog",
            function (gettextCatalog) {
                this.switchLanguage = function(lang) {
                    gettextCatalog.setCurrentLanguage(lang);
                    if (lang !== 'en') {
                        return gettextCatalog.loadRemote("languages/" + lang + ".json");
                    }
                };
                this.currentLanguage = function() {
                    return gettextCatalog.getCurrentLanguage();
                };
            }
        ]
    );