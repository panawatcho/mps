﻿"use strict";
angular.module("app.services")
    .factory('k2', ['k2Factory',
        function (k2Factory) {
            return k2Factory();
        }
    ]);
angular.module("app.services")
    .factory('k2Factory', ['messageBox', '$routeParams', '$location', 'appText', 'blockUI', '$uibModal','authService',
    function (messageBox, $routeParams, $location, appText, blockUI, $uibModal, authService) {

//angular.module("app.services")
//    .factory('k2Factory', ['$rootScope', 'messageBox', '$routeParams', '$location', 'appText', '$uibModal', 'blockUI', 'authService',
//    function ($rootScope, messageBox, $routeParams, $location, appText, $uibModal, blockUI, authService) {

        return function (dataSource) {
            var _extractSN,
                _changeMode,
                _onSnError,
                _chooseAction,
                _saveAndSubmit,
                _startProcess,
                _bulkStartProcess,
                _initForm,
                _doActionInKendoGrid,
                _columnTemplateForAction,
                _hideRow,
                _doActionNgClick,
                _get,
                _getWorklist,
                _createActionModal,
                _newRequester;
  

            _extractSN = function ($routeParams) {
                if ($routeParams.SN != undefined) {
                    return $routeParams.SN;
                }
                if ($routeParams.sn != undefined) {
                    return $routeParams.sn;
                }
                if ($routeParams.Sn != undefined) {
                    return $routeParams.Sn;
                }
                if ($routeParams.sN != undefined) {
                    return $routeParams.sN;
                }
                return null;
            }
           
        _changeMode = function (m, id, reload) {
            //messageBox.clearError();
            var c = $location.path(),
                s = c.substr(0, c.indexOf('/' + $routeParams.mode) + (1 + $routeParams.mode.length)),
                p = s.replace('/' + $routeParams.mode, '/' + m.toLowerCase());
            //if (p.match(/\/$/)) { // endswith /
            //    p = p + id;
            //} else {
            if (id != undefined) {
                p = p + '/' + id;
            }
            //}
            $location.path(p, reload);
        }
        _onSnError = function (response) {
            if (response.data && response.data.WorkflowTableId) {
                _changeMode('Detail', response.data.WorkflowTableId);
                if (response.data.SN) {
                    messageBox.info('The worklist item \'' + response.data.SN + '\' is already submitted. Here\'s the detail.');
                } else {
                    messageBox.info('This worklist item is already submitted. Here\'s the detail.');
                }
            } else {
                $location.path('/');
                messageBox.extractError(response);
            }
        }
        _chooseAction = function (options) {
            var resource = options.resource,
                uploadHandler = options.uploadHandler,
                actions = options.actions,
                action = options.action,
                id = resource.Id, // Important
                sn = resource.SN,
                onAction = options.onAction || resource.$action;
            if (!options.error) {
                options.error = function (response) {
                    if (angular.isArray(response)) {
                        messageBox.eachError(response, appText.validationFail);
                    } else {
                        messageBox.extractError(response);
                    }
                    return {};
                }
            }
            if (!action) {
                messageBox.error('Please choose an action.');
            }
            if (action) {
                var upper = action.toUpperCase();
                if ((upper === 'CANCEL' || upper === 'REVISE' || upper === 'REJECT')
                    && !resource.Comment) {
                    messageBox.error('Please enter comment before ' + action);
                    return false;
                }
            }
            //if (action) {
            //    var upper = action.toUpperCase();
            //    var ret = true;
            //    if ((upper === 'CANCEL' ||  upper === 'REVISE' || upper === 'REJECT' )
            //        && !resource.Comment) {
            //        messageBox.error('Please enter comment before ' + action);
            //        ret = false;
            //    }
            //    //Fix this code for PRUKSA - LAC project (validation for choose reject reason before reject / cancel action)
            //    if ((upper === 'CANCEL' || upper === 'REJECT' )) {
            //        messageBox.error('Please choose your reason before ' + action);
            //        ret = false;
            //    }
            //    //
            //    if (ret === false)
            //        return ret;
            //}
            var finalSuccess = function (resp) {
                return function () {
                    onAction.call(resource, { sn: sn, action: action }, function () {
                        messageBox.success(appText.worklistActionSuccess);
                        if (options.success) {
                            options.success();
                        } //else {
                        //$scope.$apply(function() {
                        //    _changeMode('Detail', id);
                        //});
                       // _changeMode('Detail', id);
                        $location.path('/');
                        //}
                        return true;
                    },
                    function (response) {
                        messageBox.extractError(response);
                        return false;
                    });
                };
            }
            if (!options.validator || options.validator.validate()) {
                if (actions.indexOf(action) >= 0) {
                    uploadHandler.setId(id).setSN(sn).upload(function (response) {
                        var resp = response || resource;
                        if (options.uploadSuccess) {
                            if (options.uploadSuccessIsCallback) {
                                options.uploadSuccess(resp, finalSuccess(resp));
                            } else {
                                options.uploadSuccess(resp);
                                finalSuccess(resp)();
                            }
                        } else {
                            finalSuccess(resp)();
                        }
                    },
                        function (response) {
                            messageBox.extractError(response);
                            return false;
                        });
                } else {
                    messageBox.error(appText.worklistInvalidAction);
                    return false;
                }
            } else {
                return options.error(options.validator.errors());
            }
        }
       
        _saveAndSubmit = function (options) {
    
            if (options.event) {
                options.event.preventDefault();
            }
            if (options.success && !options.saveSuccess ) {
                options.saveSuccess = options.success;
            }
            if (options.success && !options.submitSuccess) {
                options.submitSuccess = options.success;
            }
            if (!options.beforeUpload) {
                if (options.uploadHandler) {
                    options.beforeUpload = options.uploadHandler.setId;
                }
            }

            if (!options.error) {
                options.error = function (response) {
                    if (angular.isArray(response)) {
                        messageBox.eachError(response, appText.validationFail);
                    } else {
                        messageBox.extractError(response);
                    }
                    return {};
                }
            }
            if (!options.uploadSuccess) {
                options.uploadSuccess = function (response) {
                }
            }
            var isNew = (angular.isUndefined(options.data.Id));

            var finalSuccess = function (response) {
                return function () {
                    if (angular.isUndefined(options.preventSubmit) || options.preventSubmit) {
                        blockUI.stop();
                        messageBox.success(appText.saveSuccess);
                        //if (isNew) {
                        //    _changeMode('Create', id, false);
                        //}
                        //return true;

                        if (options.saveSuccess) {
                            options.saveSuccess(response);
                            if (!options.ignoreIsNew) {
                                if (isNew) {
                                    _changeMode('Create', options.data.Id, true);
                                }
                            }

                        }
                        else {
                            if (!options.ignoreIsNew) {
                                if (isNew) {
                                    _changeMode('Create', options.data.Id, true);
                                }
                            }
                        }
                    } else {
                        if (!options.ignoreIsNew) {
                            if (isNew) {
                                _changeMode('Create', options.data.Id, true);
                            }
                        }

                        options.saveSuccess(response);
                        //
                        _startProcess(response, { id: response.Id },
                            function (response) {
                           
                                if (options.submitSuccess) {
                                    options.submitSuccess(response);
                                    blockUI.stop();
                                    _changeMode('Detail', options.data.Id, true);
                                }
                            }, function (response) {
                                blockUI.stop();
                               
                                if (options.error) {
                                    options.error(response);
                                }
                            });
                    }
                }
            };
            //if (saveMethod == null) {
            //    saveMethod = dataSource.save;
            //}
            blockUI.start();
            if (!options.validator || options.validator.validate()) {
                var instance = options.save(options.data).$promise.then(
                    function (response) { // Success
                        if (angular.isArray(instance)) {
                            //if (options.success) {
                            //    options.success(instance);
                            //}
                            //if (angular.isUndefined(options.preventSubmit) || options.preventSubmit) {
                            //    blockUI.stop();
                            //    messageBox.success(appText.saveSuccess);
                            //    //if (isNew) {
                            //    //    _changeMode('Index');
                            //    //}
                            //    //return true;
                            //} else {
                            //    return _bulkStartProcess(dataSource, instance,
                            //    function () {
                            //        blockUI.stop();
                            //    }, function () {
                            //        blockUI.stop();
                            //    });
                            //}
                        } else {
                            var id = options.data.Id = response.Id;
                            if (options.beforeUpload) {
                                options.beforeUpload(id);
                            }
                            if (options.uploadHandler) {
                                options.uploadHandler.upload(function () {
                                    if (options.uploadSuccessIsCallback) {
                                        options.uploadSuccess(response, finalSuccess(response));
                                    } else {
                                        options.uploadSuccess(response);
                                        finalSuccess(response)();
                                    }
                                }, function () {
                                    blockUI.stop();
                                    if (options.saveSuccess) {
                                        options.saveSuccess(response);
                                        if (!options.ignoreIsNew) {
                                            if (isNew) {
                                                _changeMode('Create', options.data.Id, true);
                                            }
                                        }
                                    }
                                    else {
                                        if (!options.ignoreIsNew) {
                                            if (isNew) {
                                                _changeMode('Create', options.data.Id, true);
                                            }
                                        }
                                    }
                                   
                                });
                            } else {
                                if (finalSuccess) {
                                    finalSuccess(response)();
                                }
                                blockUI.stop();
                            }
                            //    function () {
                            //        if (angular.isUndefined(options.preventSubmit) || options.preventSubmit) {
                            //            blockUI.stop();
                            //            messageBox.success(appText.saveSuccess);
                            //            //if (isNew) {
                            //            //    _changeMode('Create', id, false);
                            //            //}
                            //            //return true;
                            //            if (options.saveSuccess) {
                            //                options.saveSuccess(response);
                            //            }
                            //        } else {
                            //            _startProcess(response, { id: id },
                            //            function (response) {
                            //                blockUI.stop();
                            //                if (options.submitSuccess) {
                            //                    options.submitSuccess(response);
                            //                }
                            //            }, function (response) {
                            //                blockUI.stop();
                            //                if (options.error) {
                            //                    options.error(response);
                            //                }
                            //            });
                            //        }
                            //    }
                            //);
                        }
                    },
                    function (response) { // Error
                        blockUI.stop();
                        options.error(response);
                        return response;
                    }
                );
                return instance;
            } else {
                blockUI.stop();
                return options.error(options.validator.errors());
            }
        }
        _startProcess = function (resource, params, success, error) {
            resource.$startprocess(params,
                function (response) {
                    messageBox.success(appText.processStartSuccess);
                    if (success) {
                        success(response);
                    } //else {
                    //_changeMode('Detail', params.id);
                    //}
                    return true;
                },
                function (response) {
                    if (error) {
                        error(response);
                    } else {
                        messageBox.extractError(response);
                    }

                    return false;
                }
            );
        }
        _bulkStartProcess = function (dataSource, resource, success, error) {
            dataSource.bulkstartprocess(resource,
                function () {
                    messageBox.success(appText.processStartSuccess);
                    if (success) {
                        success();
                    } //else {
                    //_changeMode('');
                    //}
                    return true;
                },
                function (response) {
                    if (error) {
                        error();
                    }
                    messageBox.extractError(response);
                    return false;
                }
            );
        }
        _initForm = function (data) {
            return angular.extend({
                DocumentDate: new Date(),
                OtherFiles: [],
                CommentFiles: {},
                DocTypeFiles: {},
                Requester: { Username: CURRENT_USERNAME }
            }, data);
        }
        _doActionInKendoGrid = function (e, that, dataSourceInstance, action, success, error) {
            var row, dataItem, sn;
            row = $(e.currentTarget).closest("tr");
            dataItem = that.dataItem(row);
            sn = dataItem.SN;
            var remoteResult = dataSourceInstance.$action({ sn: sn, action: action }, function () {
                if (angular.isDefined(success) && success != null) {
                    success();
                }
                return true;
            },
                function (response) {
                    if (angular.isDefined(error) && error != null) {
                        error();
                    }
                    messageBox.extractError(response);
                    return false;
                });
        }
        _columnTemplateForAction = function () {
            return '# for (var i = 0; i < data.Actions.length; i++) { #\
                    <button type="button" class="btn btn-default" ng-click="doAction(\'#= data.Actions[i] #\')">#= data.Actions[i] #</div>\
                # } #';
        }
        _hideRow = function (data) {
            var row = $('tbody').find("tr[data-uid='" + data.uid + "']");
            row.hide();
        }
        _doActionNgClick = function (that, dataSourceInstance, action, success, error) {
            var data = that.dataItem, sn = data.SN;
            var remoteResult = dataSourceInstance.$action({ sn: sn, action: action }, function () {
                if (angular.isDefined(success) && success != null) {
                    success();
                    _hideRow(data);
                }
                return true;
            },
                function (response) {
                    if (angular.isDefined(error) && error != null) {
                        error();
                    }
                    messageBox.extractError(response);
                    return false;
                });
        }


        // New
        _get = function (data, opt) {
            opt = opt || {};
            var d = opt.dataSource || dataSource;
            d.get(data).$promise.then(
                opt.success,
                function (response) {
                    messageBox.extractError(response);
                }
            );
        }
        _getWorklist = function (data, opt) {
            opt = opt || {};
            var d = opt.dataSource || dataSource;
            d.worklist(data).$promise.then(opt.success, _onSnError);
        }
        _createActionModal = function () {
            return $uibModal.open({
                animation: true,
                templateUrl: 'templates/chooseActionModal.html',
                size: 'lg',
                scope: this
            });

        }
        _newRequester = function () {
            return angular.copy(authService.authentication.me);
        }

        var service = {};
        service.extractSN = _extractSN;
        service.onSnError = _onSnError;
        service.changeMode = _changeMode;
        service.chooseAction = _chooseAction;
        service.saveAndSubmit = _saveAndSubmit;
        service.startProcess = _startProcess;
        service.bulkStartProcess = _bulkStartProcess;
        service.initForm = _initForm;
        service.doActionInKendoGrid = _doActionInKendoGrid;
        service.doActionNgClick = _doActionNgClick;
        service.columnTemplateForAction = _columnTemplateForAction;

        // New
        service.get = _get;
        service.getWorklist = _getWorklist;
        service.createActionModal = _createActionModal;
        service.newRequester = _newRequester;
        return service;
    }
}]);