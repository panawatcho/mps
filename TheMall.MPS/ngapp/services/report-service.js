﻿'use strict';

angular.module("app.services")
    .factory('reportService',
    [
        "$http", "$routeParams", "$sce", "$location", "messageBox", "downloadService",
            function ($http, $routeParams, $sce, $location, messageBox, downloadService) {

                var service = {}, _dlReportUrl, _dlFunc, _vReportUrl, _vFunc, _fileUrl;
                service.params = {};
                service.result = {};
                service.downloadFileGet = downloadService.downloadFileGet;
                service.downloadFilePost = downloadService.downloadFilePost;

                service.fileTypesOption = {
                    dataSource: {
                        data: ["PDF", "PNG", "JPEG", "WORD", "XLS"]
                    }
                };

                service.initParams = function (obj) {
                    service.result.content = null;
                    service.result.contentType = null;
                    var p = $location.search();
                    delete p.params;
                    service.params = angular.extend({}, angular.fromJson($routeParams.params), obj, p);
                    if ($routeParams.params || p.show) { // Auto display
                        if (service.params.inline || p.show) {
                            if (_vFunc) {
                                _vFunc();
                            }
                        } else {
                            if (_dlFunc) {
                                _dlFunc();
                            }
                        }
                    }
                    return service.params;
                };

                service.download = function (reportUrl) {
                    if (reportUrl) {
                        _dlReportUrl = reportUrl;
                    }
                    _dlFunc = function () {
                        service.params.inline = false;
                       // service.params.format = "XLS";
                        return service.downloadFilePost(_dlReportUrl, service.params);
                    };
                    return _dlFunc;
                };

                service.view = function (reportUrl) {
                    if (reportUrl) {
                        _vReportUrl = reportUrl;
                    }
                    _vFunc = function () {
                        debugger;
                        service.params.inline = true;
                        service.result.content = null;
                        var promise = $http.post(reportUrl, service.params, { responseType : "arraybuffer" });
                        promise.then(
                            function(resp) {
                                var contentType = resp.headers("content-type");
                                var file = new Blob([resp.data], { type : contentType });
                                if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                                    window.navigator.msSaveOrOpenBlob(file);
                                }
                                else {
                                    var fileUrl = URL.createObjectURL(file);
                                    _fileUrl = fileUrl;
                                    service.result.contentType = contentType;
                                    service.result.content = $sce.trustAsResourceUrl(fileUrl);
                                    if (service.params.fullscreen) {
                                        service.goFullScreen(false);
                                    }
                                }
                            }
                        );
                        return promise;
                    };

                    return _vFunc;
                };

                service.clear = function () {
                    for (var key in service.params) {
                        if (!service.params.hasOwnProperty(key)) continue;

                        var obj = service.params[key];
                        for (var prop in obj) {
                            if (obj.hasOwnProperty(prop)) {
                                service.params[key] = null;
                            }
                        }
                    }
                    service.params.format = "PDF";
                    service.params.page = null;
                    service.params.to = null;
                    service.result.content = null;
                    service.result.contentType = null;
                };

                service.goFullScreen = function(newWindow) {
                    if (_vFunc && !_fileUrl) {
                        _vFunc().then(function() {
                            newWindow ? window.open(_fileUrl, "_blank") : window.location = _fileUrl;
                        });
                    }
                    else {
                         newWindow ? window.open(_fileUrl, "_blank") : window.location = _fileUrl;
                    }
                };

                return service;
            }
]);