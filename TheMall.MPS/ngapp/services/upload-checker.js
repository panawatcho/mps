﻿"use strict";
angular.module("app.services")
    .factory("uploadChecker", ["maxFileSize", function (maxFileSize) {

        var _validate = function (name, file) {
            if (file) {
                if (file.size > maxFileSize) {
                    return "File is too large.";
                }
            }
            if (name) {
                var xs = (/(?:\.([^.]+))?$/.exec(name)), n = name.split('\\').pop().split('/').pop(), x = xs[1], i = n.lastIndexOf('.');
                if (i > 0) {
                    n = n.substring(0, i);
                }
                if (/^[^%~#&*{}\\\\:<>?/+|\"\.]+$/i.test(n) && x) {
                    //if (/(\.|\/)(gif|jpe?g|png|tiff?|zip|rar|pdf|xlsx?|csv|pptx?|docx?|txt|rtf|msg)$/i.test('.' + x)) {
                    if (/(\.|\/)(pdf|gif|jpe?g|png)$/i.test('.' + x)) {
                        return false;
                    } else {
                        // return "Invalid file type. " + x.toUpperCase() + ' is not allowed to be upload.';
                        return "Select files type .pdf|.gif|.jpeg|.png ";
                    }
                } else {
                    if (!x) {
                        return "Invalid file name. Empty extension file is not allowed to be uploaded.";
                    }
                    return "Invalid file name. ~ % # & * { } \\ : < > / + | \" . are not allowed.";
                }
            }
            return false;
        }

        var _validateDocType = function (name, docType, file) {
            if (file) {
                if (file.size > maxFileSize) {
                    return "File is too large.";
                }
            }
            if (name) {
                var xs = (/(?:\.([^.]+))?$/.exec(name)), n = name.split('\\').pop().split('/').pop(), x = xs[1], i = n.lastIndexOf('.');
                if (i > 0) {
                    n = n.substring(0, i);
                }
                if (/^[^%~#&*{}\\\\:<>?/+|\"\.]+$/i.test(n) && x) {
                    switch (docType) {
                        case 1:
                            //if (/(\.|\/)(gif|jpe?g|png|tiff?|zip|rar|pdf|xlsx?|csv|pptx?|docx?|txt|rtf|msg)$/i.test('.' + x)) {
                            if (/(\.|\/)(pdf|gif|jpe?g|png)$/i.test('.' + x)) {
                                return false;
                            } else {
                                //return "Invalid file type. " + x.toUpperCase() + ' is not allowed to be upload.';
                                return "Select files type .pdf|.gif|.jpeg|.png ";
                            }
                            break;
                        case 2:
                            //if (/(\.|\/)(gif|jpe?g|png|tiff?|zip|rar|pdf|xlsx?|csv|pptx?|docx?|txt|rtf|msg)$/i.test('.' + x)) {
                            if (/(\.|\/)(pdf|gif|jpe?g|png)$/i.test('.' + x)) {
                                return false;
                            } else {
                                //return "Invalid file type. " + x.toUpperCase() + ' is not allowed to be upload.';
                                return "Select files type .pdf|.gif|.jpeg|.png ";
                            }
                            break;
                        case 3:
                            //if (/(\.|\/)(gif|jpe?g|png|tiff?|zip|rar|pdf|xlsx?|csv|pptx?|docx?|txt|rtf|msg)$/i.test('.' + x)) {
                            if (/(\.|\/)(pdf|gif|jpeg|png)$/i.test('.' + x)) {
                                return false;
                            } else {
                                //return "Invalid file type. " + x.toUpperCase() + ' is not allowed to be upload.';
                                return "Select files type .pdf|.gif|.jpeg|.png ";
                            }
                            break;
                        case 9:
                            //if (/(\.|\/)(gif|jpe?g|png|tiff?|zip|rar|pdf|xlsx?|csv|pptx?|docx?|txt|rtf|msg)$/i.test('.' + x)) {
                            if (/(\.|\/)(pdf|gif|jpe?g|png)$/i.test('.' + x)) {
                                return false;
                            } else {
                               // return "Invalid file type. " + x.toUpperCase() + ' is not allowed to be upload.';
                                return "Select files type .pdf|.gif|.jpeg|.png ";
                            }
                            break;
                        default:
                            //if (/(\.|\/)(gif|jpe?g|png|tiff?|zip|rar|pdf|xlsx?|csv|pptx?|docx?|txt|rtf|msg)$/i.test('.' + x)) {
                            if (/(\.|\/)(pdf|gif|jpe?g|png)$/i.test('.' + x)) {
                                return false;
                            } else {
                                // return "Invalid file type. " + x.toUpperCase() + ' is not allowed to be upload.';
                                return "Select files type .pdf|.gif|.jpeg|.png ";
                            }
                    }


                } else {
                    if (!x) {
                        return "Invalid file name. Empty extension file is not allowed to be uploaded.";
                    }
                    return "Invalid file name. ~ % # & * { } \\ : < > / + | \" . are not allowed.";
                }
            }
            return false;
        }

        return { validate: _validate, validateDocType: _validateDocType };
    }]);