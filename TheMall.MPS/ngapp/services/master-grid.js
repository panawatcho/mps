﻿"use strict";
angular.module("app.services")
    .service('masterGrid',
    ['$routeParams', 'appText','lineGrid',
        function ($routeParams, appText, lineGrid) {
           
            this.formatDateTime = function (fieldname) {
                return '#if(' + fieldname + '){##=kendo.toString(new Date(' + fieldname + '), "dd/MM/yyyy HH:mm:ss")##}#';
                //return '#if(' + fieldname + '){##=kendo.toString(new Date(' + fieldname + '), "F")##}#';
                //return '#console.log(new Date(' + fieldname + '))##if(' + fieldname + '){##=kendo.parseDate(' + fieldname + ', [\'yyyy-MM-ddTHH:mm:ss.fffz\',\'yyyy-MM-ddTHH:mm:ssz\'])##}#';

            }
            this.checkboxTemplate = function (fieldname) {
                return '<div class="align-center">#if(data.' + fieldname + '){# <i class="fa fa-check-square"></i> #}else{# <i class="fa fa-square-o"></i> #}#</div>';
            }
            this.formatDate = function (fieldname) {
                return '#if(' + fieldname + '){##=kendo.toString(new Date(' + fieldname + '), "dd/MM/yyyy" )##}#';
            }
            this.formatCurrency = function (fieldname) {
                return '<div class="text-right">#if(' + fieldname + '){##=kendo.toString(' + fieldname + ', "n2" ) ##}#</div>';
            }
            this.formatPercentage = function (fieldname) {
                return '<div class="text-right">#if(' + fieldname + '){##=kendo.toString(' + fieldname + ', "p0" ) ##}#</div>';
            }
            this.gridKendoOption = function (odataUrl,apiUrl, params) {
                params = params || {};
                var fields = {
                    CreatedDate: { type: "date" }
                };
                if (angular.isDefined(params.fields)) {
                    angular.extend(fields, params.fields);
                }
                return {
                    dataSource: new kendo.data.DataSource({
                        transport:  {
                            read: {
                                async: true,
                                url: odataUrl,
                                dataType: "json",
                                data: params.readData || undefined
                            },
                            update: {
                                url: apiUrl + "/Update",
                                dataType: "json",
                                method: "post"
                            },
                            destroy: {
                                url: apiUrl + "/Delete",
                                dataType: "json",
                                method: "post"
                            },
                            create: {
                                url: apiUrl + "/Create",
                                dataType: "json",
                                method: "post"
                            },
                            parameterMap: params.parameterMap || undefined
                        },
                        filter: (params.filters) ? params.filters : null,
                        sort: (params.sorts) ? params.sorts : null,
                        batch: true,
                        serverPaging: true,
                        serverSorting: true,
                        serverFiltering: true,
                        pageSize: 10,
                        schema: {
                            data: function (data) { return data.value; },
                            total: function (data) { return data["odata.count"]; },
                            model: params.schema.model
                        },
                        error: function (e) {
                            try {
                                var resp = angular.fromJson(e.xhr.responseText);
                                alert(resp.Message);
                            } catch (e) {
                                alert(e.xhr.responseText);
                            }
                        },
                        group: params.group
                    }),
                    editable: {
                        confirmation: true,
                        confirmDelete: "Yes",
                        cancelDelete: "No",
                        window: {
                            animation: false,
                            draggable: false
                        },
                        mode: params.editable.mode,
                        createAt: params.createAt
                               },
                    filterable: {
                        extra: true, //do not show extra filters
                        operators: { // redefine the string operators
                            string: {
                                contains: "Contains",
                                //doesnotcontain: "Does not contain",
                                //eq: "Is Equal To",
                                //neq: "Is not equal to",
                                //startswith: "Starts With",
                                //endswith: "Ends with"

                            }
                        }
                    },
                    groupable: false,
                    pageable: {
                        refresh: true,
                        pageSizes: [5, 10, 20, 50, 100]
                    },
                    height: 350,
                    reorderable: false,
                    resizable: false,
                    selectable: "row",
                    sortable: {
                        allowUnsort: true,
                        mode: "single"
                    },
                    columns: params.columns || [],
                    scrollable: true,
                    mobile: params.mobile || false,
                    toolbar: params.toolbar ||[]

                };
            }
            this.sortOption = { field: 'CreatedDate', dir: 'desc' };

            this.gridApproveKendoOption = function (params) {
                return lineGrid.gridOption({
                    schema: {
                        model: kendo.data.Model.define({
                            id: "LineNo",
                            fields: {
                                LineNo: { type: "number" },
                                ApproverSequence: { type: "number" },
                                ApproverLevel: { type: "number" },
                            }
                        })
                    },
                    data: data.params,
                    filters: filters.params,
                    columns: [
                            { field: "ApproverSequence", title: "Sequence", width: "100px", },
                            { field: "ApproverFullName", title: "FullName", width: "100px", },
                            { field: "ApproverEmail", title: "Email", width: "100px", },
                            { field: "Position", title: "Position", width: "100px", },
                            { field: "Department", title: "Department", width: "100px", },
                            { field: "DueDate", title: "DueDate", width: "100px", },
                    ],
                    editable: {
                        mode: "incell"
                    },
                    edit: edit.params,
                    height: height.params || 'auto',
                    noedit: noedit.params,
                    save: save.params,
                    pageable: true,
                    groupable: true
                });
            }

        }]);