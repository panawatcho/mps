﻿'use strict';

angular.module("app.services")
    .factory('documentTypeService',
    [
        "appUrl", "$resource", "$http",
        function (appUrl, $resource, $http) {

            var service = {};

            service.getDocumentTypes = function (process, activity) {
                var params = { process: process, activity: activity };
                return $http.get(angular.crs.interpoleUrl("//api.mps.local/api/documenttype/:process/:activity", params),
                    params);
            }

            return service;
        }
    ]
);