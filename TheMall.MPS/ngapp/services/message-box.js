﻿"use strict";

angular.module("app.services")
    .service('messageBox',
    [ "gettextCatalog",
        function (gettextCatalog) {
        var that = this;
        that.instances = [];
        var _formatMessage = function(message) {
            return message.replace(/(?:\r\n|\r|\n)/g, '<br />');
        };
        var _error = function(msg) {
            //_init();
            //_clearError();
            that.instances[0].error(msg);
        };
        var _loopError = function(errors, message) {
            var i = 0;
            message = message || '';
            $.map(errors, function(t) {
                if (angular.isObject(t)) {
                    message = _loopError(t, message);
                } else {
                    var m = _formatMessage(t);
                    if (i === 0) message += m;
                    else message += '<br />' + m;
                    i++;
                }
            });
            return message;
        };
        var _eachError = function(errors, message) {
            _error($.trim(_loopError(errors, message)));
        };

        this.notificationOption = {
            show: function onShow(e) {
                if (!$("." + e.sender._guid)[1]) {
                    var element = e.element.parent(),
                        eWidth = element.width(),
                        //eHeight = element.height(),
                        wWidth = $(window).width(),
                        //wHeight = $(window).height(),
                        newTop,
                        newLeft;

                    newLeft = Math.floor(wWidth / 2 - eWidth / 2);
                    //newTop = Math.floor(wHeight / 2 - eHeight / 2);
                    newTop = 200;
                    e.element.parent().css({ top: newTop, left: newLeft , 'z-index' :900000});
                    
                }
            },
            button: true,
            autoHideAfter: 5000,
            stacking: "down",
            //templates: [
            //    {
            //        type: "info",
            //        template: $("#infoTemplate").html()
            //    }, {
            //        type: "error",
            //        template: $("#errorTemplate").html()
            //    }, {
            //        type: "success",
            //        template: $("#successTemplate").html()
            //    }
            //]
        };

        this.info = function(msg) {
            that.instances[0].info(msg);
        };

        this.error = function(msg) {
            _error(msg);
        };

        this.success = function(msg) {
            that.instances[0].success(msg);
        };

        this.eachError = _eachError;
        this.extractError = function(response) {
            response = response || {};
            if (response.status === 404) {
                _error(gettextCatalog.getString('The requested endpoint could not be found.'));
            } else {
                var d = response, m = '';
                if (response.data) {
                    d = response.data;
                }
                if (d.ModelState && d.ModelState.ValidationErrors) {
                    if (angular.isDefined(d.Message)) {
                        m = d.Message;
                    }
                    _eachError(d.ModelState.ValidationErrors, m);
                } else if (d.ModelState) {
                    if (d.Message) {
                        m = d.Message;
                    }
                    _eachError(d.ModelState, m);
                } else if (d.Message != undefined) {
                    _error(_formatMessage(d.Message));
                } else {
                    _error(gettextCatalog.getString('An unknown error has occurred.'));
                }
            }
        };
    }
    ]
    );