﻿'use strict';

angular.module("app.services")
    .factory('uploadService',
    [
        "$q", "$http", "Upload", "downloadService", "messageBox", "gettextCatalog",
        function ($q, $http, Upload, downloadService, messageBox, gettextCatalog) {
            Upload.setDefaults(
                {
                    //ngfPattern: ".pdf",
                    ngfPattern: ".doc,.docx,.pdf,gif,.jpeg,.jpg,.png,.tiff,.zip,.rar,.xls,.xlsx,.ppt,.pptx,.txt,.msg",
                    ngfKeep: true,
                    ngfMultiple: true,
                    //ngfChange: "onUploadChange($files, $file, $newFiles, $duplicateFiles, $invalidFiles, $event)"
                }
            );

            var service = {};
            // ReSharper disable once InconsistentNaming
            var CHUNK_SIZE = 10000000;

            // WARNING! This method does not return a chainable promise like the other methods.
            // But return an array of promises instead.
            // You need to iterate through each of the promie and use its promise individually.
            service.uploadFiles = function (url, files, data) {
                //var defer = $q.defer();
                if (files && files.length) {
                    var promises = [];
                    for (var i = 0; i < files.length; i++) {
                        files[i]._uploading = true;

                        var d = {};
                        for (var prop in data) {
                            if (data.hasOwnProperty(prop)) {
                                var tmp = data[prop];
                                if (angular.isArray(tmp)) {
                                    d[prop] = tmp[i];
                                } else {
                                    d[prop] = tmp;
                                }
                            }
                        }
                        angular.extend(d, { file: files[i], title: files[i].title });

                        promises.push(Upload.upload({
                                url: url,
                                data: d,
                                resumeChunkSize: files[i].size > CHUNK_SIZE ? CHUNK_SIZE : null
                            })
                            .then(
                                function(resp) {
                                    files.splice(files.indexOf(resp.config.data.file), 1);
                                    return resp;
                                },
                                function(resp) {;
                                    resp.config.data.file._isError = true;
                                    messageBox.extractError(resp);

                                    return resp;
                                },
                                function(evt) {
                                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                                    evt.config.data.file._uploading = progressPercentage;
                                    evt.config.data.file._style = {
                                        width: progressPercentage + "%"
                                    }

                                    return evt;
                                }
                            ));
                    }
                    //$q.all(promises).then(function() {
                    //    defer.resolve(promises);
                    //});
                    return promises;
                }
                //else {
                //    defer.resolve([]);
                //}
                return [];
                //return defer.promise;
            };

            service.delete = function (file, files, attachments) {
                file._deleting = true;
                var deferred = $q.defer();
                if (file.delete_url) {
                    $http({
                        method: file.delete_type,
                        url: file.delete_url
                    })
                    .then(
                        function(resp) {
                            deferred.resolve(resp);
                            return resp;
                        },
                        function (resp) {
                            messageBox.error(gettextCatalog.getString("Cannot delete '{{filename}}' due to an error.", { filename: file.name }));
                            deferred.reject(resp);
                            return resp;
                        }
                    );
                } else {
                    deferred.resolve();
                }
                deferred.promise.then(
                    function (resp) {
                        if (resp) {
                            attachments.splice(attachments.indexOf(file), 1);
                        } else {
                            files.splice(files.indexOf(file), 1);
                        }
                        return resp;
                    }
                );
                return deferred.promise;
            };

            service.onUploadChange = function ($files, $file, $newFiles, $duplicateFiles, $invalidFiles, $event) {
                if ($invalidFiles && $invalidFiles.length) {
                    for (var i = 0; i < $invalidFiles.length; i++) {
                        if ($invalidFiles[i].$error === "pattern") {
                            messageBox.error(gettextCatalog.getString("'{{filename}}' is not allowed to be uploaded.", { filename: $invalidFiles[i].name }));
                        }
                    }
                }
            };
            service.download = function (url) {

            };
            service.getCopy = function (url) {
                downloadService.downloadFileGet(url);
            };

            service.validate = function(kendoValidator) {
                var valid = true;
                angular.forEach($("button[ngf-select]"), function (value, key) {
                    var r = kendoValidator.validateInput(value);
                    valid = valid && r;
                });
                return valid;
            }

            return service;
        }
    ]
);