﻿"use strict";

angular.module("app.services")
    .factory('uploadHandler', ['$rootScope', 'messageBox', function ($rootScope, messageBox) {

        var _init = function (handler) {
            handler = handler || {};
            if (handler.initilized != undefined) {
                return handler;
            }
            handler.initilized = true;
            handler.setId = function (id) {
                if (handler.data == undefined) {
                    handler.data = { id: id }
                } else {
                    handler.data['id'] = id;
                }
                return handler; // for chaining
            }
            handler.setSN = function (sn) {
                if (handler.data == undefined) {
                    handler.data = { sn: sn }
                } else {
                    handler.data['sn'] = sn;
                }
                return handler; // for chaining
            }
            handler.setDocumentNumber = function (documentNumber) {
                if (handler.data == undefined) {
                    handler.data = { documentNumber: documentNumber }
                } else {
                    handler.data['documentNumber'] = documentNumber;
                }
                return handler; // for chaining
            }
            handler.setData = function (data) {
                if (handler.data == undefined) {
                    handler.data = {}
                }
                angular.extend(handler.data, data);
                return handler;
            }
            handler.controls = handler.controls || [];
            handler.addCtrl = function (name) {
                var found = false;
                var n = '[name="' + name + '"]';
                for (var i = 0; i < handler.controls.length; i++) {
                    if (handler.controls[i] === n) {
                        found = true;
                    }
                }
                if (!found) {
                    handler.controls.push(n);
                }
            }
            //debugger;
            //handler.uploading = false;
            handler.upload = function (callbacksuccess, callbackerror) {
                var uploadCount = 0, completedCount = 0, successCount = 0, errorCount = 0, uploaded = false, errors = [];
                handler.onComplete = function (e) {
                    completedCount++;
                    if (uploadCount === completedCount) {
                        angular.forEach(handler.controls, function (c) {
                            $(c).find('.k-delete').show();
                        });
                        //console.log({
                        //    control: handler.controls.length,
                        //    upload: uploadCount,
                        //    completed: completedCount,
                        //    success: successCount,
                        //    error: errorCount
                        //});
                        (handler.onAllComplete || angular.noop)({
                            control: handler.controls.length,
                            upload: uploadCount,
                            completed: completedCount,
                            success: successCount,
                            error: errorCount
                        });
                        if (errorCount === 0) {
                            if (callbacksuccess != undefined) {
                                $rootScope.$apply(function () {
                                    callbacksuccess(e, {
                                        control: handler.controls.length,
                                        upload: uploadCount,
                                        completed: completedCount,
                                        success: successCount,
                                        error: errorCount
                                    });
                                });
                            }
                        } else {
                            if (callbackerror != undefined) {
                                $rootScope.$apply(function () {
                                    callbackerror(e, {
                                        control: handler.controls.length,
                                        upload: uploadCount,
                                        completed: completedCount,
                                        success: successCount,
                                        error: errorCount
                                    });
                                });
                            } //else {
                            //debugger;
                            for (var i = 0; i < errors.length; i++) {
                                var error = errors[i];
                                if (error.XMLHttpRequest != null
                                && error.XMLHttpRequest.responseText != null
                                && error.XMLHttpRequest.responseText.indexOf("Maximum request length exceeded.") > 0) {
                                    messageBox.error(error.files[0]["name"] + " upload has failed!<br/>Reason:<br/>File is too large.");
                                } else {
                                    var obj = null;
                                    try {
                                        obj = angular.fromJson(error.XMLHttpRequest.responseText);
                                        messageBox.error(error.files[0]["name"] + " upload has failed! <b>Please try again.</b><br/><br/><u>Reason:</u><br/>" + obj.Message);
                                    } catch (e) {
                                        messageBox.error(error.files[0]["name"] + " upload has failed! <b>Please try again.</b>");
                                    }
                                }
                            }
                            //}
                        }
                    }
                }
                if (handler.onError) {
                    var originalError = handler.onError;
                    handler.onError = function (e) {
                        originalError(e);
                        errorCount++;
                        errors.push(e);
                    }
                } else {
                    handler.onError = function (e) {
                        errorCount++;
                        errors.push(e);
                    }
                }

                if (handler.onSuccess) {
                    var originalSuccess = handler.onSuccess;
                    handler.onSuccess = function (e) {
                        originalSuccess(e);
                        successCount++;
                    }
                } else {
                    handler.onSuccess = function (e) {
                        successCount++;
                    };
                }
                angular.forEach(handler.controls, function (c) {
                    var retry = 0;
                    if ($(c).find('.k-retry').length > 0) {
                        retry = 1;
                    }
                    uploadCount += retry + $(c).find('.k-upload-selected').length;
                    $(c).find('.k-retry').show();
                    $(c).find('.k-delete').hide();
                });
                angular.forEach(handler.controls, function (c) {
                    var retry = $(c).find('.k-retry'), $widget, files;
                    if (retry.length > 0) {
                        $(c).find('.k-delete').remove();
                        retry.click();
                        uploaded = uploaded || true;
                    } else {
                        var $up = $(c).find('.k-upload-selected');
                        if ($up.length > 0) {
                            $up.click();
                            uploaded = uploaded || true;
                        } else {
                            $widget = $('input[type="file"][data-role="upload"]' + c).getKendoUpload();
                            if ($widget) {
                                files = $widget.wrapper.find('li.k-file').data("fileNames");
                                if (files) {
                                    var e = {
                                        files: files
                                        //XMLHttpRequest: xhr
                                    };
                                    uploaded = uploaded || $widget.trigger('upload', e);
                                }
                            } else {
                                uploaded = uploaded || false;
                            }
                        }
                        //var $widget = $('input[type="file"][data-role="upload"]' + c).getKendoUpload();
                        //var e = {
                        //    files: wrapper.find('li.k-file').data("fileNames"),
                        //    //XMLHttpRequest: xhr
                        //};
                        //uploaded = uploaded || $widget.trigger('upload',e );

                    }
                });
                if (!uploaded) {
                    if (callbacksuccess) {
                        callbacksuccess(null, {
                            control: handler.controls.length,
                            upload: uploadCount,
                            completed: completedCount,
                            success: successCount,
                            error: errorCount
                        });
                    }
                }
                return uploaded;
            }
            var keys;
            handler.setKeys = function (keyValuePairs) {
                keys = keyValuePairs;
                return handler;
            }
            handler.surrogate = function (widget, callbacksuccess, callbackerror) {
                var files = [];
                widget.wrapper.find('li.k-file').each(function (index) {
                    files.push($(this).data("fileNames"));
                });
                for (var i = 0; i < files.length; i++) {
                    //var k = files[i].key;
                    //for (var j = 0; j < keys.length; j++) {
                    //    if (k === keys[j][0]) {
                    //        files[i].saveUrl = files[i].saveUrl.replace(':id', keys[j][1]);
                    //        break;
                    //    }
                    //}
                    var k = handler.getSurrogateKey(files[i][0]);
                    files[i][0].saveUrl = files[i][0].saveUrl.replace(':id', k);
                }
                handler.upload(callbacksuccess, callbackerror);
                //console.log(widget);
                //angular.forEach(handler.controls, function(c) {
                //    //console.log(c);
                //});
            }
            var surrogateFiles = [];
            handler.addSurrogateFile = function (file) {
                surrogateFiles.push(file);
            }
            handler.getSurrogateKey = function (file) {
                if (!keys) return null;
                var k = file.key;
                for (var j = 0; j < keys.length; j++) {
                    if (k === keys[j][0]) {
                        return keys[j][1];
                    }
                }
                return null;
            }
            //handler.clearFiles = function() {
            //    angular.forEach(handler.controls, function(c) {
            //        $(c).find('.k-delete').click();
            //    });
            //}
            //            }
            //        });
            return handler;
        }
        return { init: _init };
    }]);