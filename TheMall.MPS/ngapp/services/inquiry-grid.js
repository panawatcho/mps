﻿"use strict";
angular.module("app.services")
    .service('inquiryGrid',
    ['$routeParams', 'appText',
        function ($routeParams, appText) {
        
        var _commandTemplate = function (data) {
            if ((data.StatusFlag === null || data.StatusFlag === 0) && data.CreatedBy && data.CreatedBy.toUpperCase() === CURRENT_USERNAME.toUpperCase()) {
                return '<div class="btn-group" role="group">' +
                    '<a class="btn btn-default" style="width:25px;margin-right: 5px;" href="' + $routeParams.page + '/detail/' + data.Id + '" title="view"><i class="fa fa-file-text"></i></a>' +
                    '<a class="btn btn-default" style="width:25px" href="' + $routeParams.page + '/create/' + data.Id + '" title="edit"><i class="fa fa-edit"></i></a>' +
                    '</div>';
            }
            return '<div class="btn-group" role="group">' +
                    '<a class="btn btn-default" style="width:25px;margin-right: 5px;" href="' + $routeParams.page + '/detail/' + data.Id + '" title="view"><i class="fa fa-file-text"></i></a>' +
                    '<span class="btn btn-default" disabled="disabled" style="width:25px" title="edit"><i class="fa fa-edit"></i></span>' +
                '</div>';
        }
        var _commandTemplateCurrentUserName = function (data) {
            if ((data.StatusFlag === null || data.StatusFlag === 0) && data.CreatedBy && data.CreatedBy.toUpperCase() === CURRENT_USERNAME.toUpperCase()) {
                return '<div class="btn-group" role="group">' +
                    '<a class="btn btn-default" style="width:25px;margin-right: 5px;" href="' + $routeParams.page + '/detail/' + data.Id + '" title="view"><i class="fa fa-file-text"></i></a>' +
                    '<a class="btn btn-default" style="width:25px" href="' + $routeParams.page + '/create/' + data.Id + '" title="edit"><i class="fa fa-edit"></i></a>' +
                    '</div>';
            }
            else if ((data.StatusFlag === 9 && !data.CloseFlag) && data.CreatedBy && data.CreatedBy.toUpperCase() === CURRENT_USERNAME.toUpperCase()) {
                return '<div class="btn-group" role="group">' +
                    '<a class="btn btn-default" style="width:25px;margin-right: 5px;" href="' + $routeParams.page + '/detail/' + data.Id + '" title="view"><i class="fa fa-file-text"></i></a>' +
                    '<a class="btn btn-warning" style="width:25px" href="' + $routeParams.page + '/reviseproposal/' + data.Id + '" title="revise"><i class="fa fa-reply"></i></a>' +
                    '</div>';
            }
            return '<div class="btn-group" role="group">' +
                    '<a class="btn btn-default" style="width:25px;margin-right: 5px;" href="' + $routeParams.page + '/detail/' + data.Id + '" title="view"><i class="fa fa-file-text"></i></a>' +
                    '<span class="btn btn-default" disabled="disabled" style="width:25px" title="edit"><i class="fa fa-edit"></i></span>' +
                '</div>';
        }


        var _commandTemplateforTeam = function (data) {
            if ((data.StatusFlag === null || data.StatusFlag === 0)) {
                return '<div class="btn-group" role="group">' +
                    '<a class="btn btn-default" style="width:60px" href="' + $routeParams.page + '/detail/' + data.Id + '" title="view"><i class="fa fa-file-text"></i></a>' +
                    '<a class="btn btn-default" style="width:60px" href="' + $routeParams.page + '/create/' + data.Id + '" title="edit"><i class="fa fa-edit"></i></a>' +
                    '</div>';
            }
            return '<div class="btn-group" role="group">' +
                    '<a class="btn btn-default" style="width:60px" href="' + $routeParams.page + '/detail/' + data.Id + '" title="view"><i class="fa fa-file-text"></i></a>' +
                    '<span class="btn btn-default" disabled="disabled" style="width:60px" title="edit"><i class="fa fa-edit"></i></span>' +
                '</div>';
        }
        var _commandTemplateAllowEdit = function (data) {
            return '<div class="btn-group" role="group">' +
                   '<a class="btn btn-default" style="width:60px" href="' + $routeParams.page + '/detail/' + data.Id + '" title="view"><i class="fa fa-file-text"></i></a>' +
                   '<a class="btn btn-default" style="width:60px" href="' + $routeParams.page + '/create/' + data.Id + '" title="edit"><i class="fa fa-edit"></i></a>' +
                   '</div>';
        }
        var _detailTemplate = function (data) {
            return '<div class="btn-group" role="group">' +
                    '<a class="btn btn-default" style="width:50px" href="' + $routeParams.page + '/detail/' + data.Id + '" title="view"><i class="fa fa-file-text"></i></a>' +
                '</div>';
        }
        var _editTemplate = function (data) {
            if ((data.StatusFlag === null || data.StatusFlag === 0) && data.CreatedBy && data.CreatedBy.toUpperCase() === CURRENT_USERNAME.toUpperCase()) {
                return '<div class="btn-group" role="group">' +
                    '<a class="btn btn-default" style="width:60px" href="' + $routeParams.page + '/create/' + data.Id + '" title="edit"><i class="fa fa-edit"></i></a>' +
                    '</div>';
            }
            return '<div class="btn-group" role="group">' +
                    '<span class="btn btn-default" disabled="disabled" style="width:60px" title="edit"><i class="fa fa-edit"></i></span>' +
                '</div>';
        }
        var _commandTemplateAllstatus = function (data) {
           
                return '<div class="btn-group" role="group">' +
                    '<a class="btn btn-default" style="width:50px;  margin-right: 5px;" href="' + $routeParams.page + '/detail/' + data.Id + '" title="view"><i class="fa fa-file-text"></i></a>' +
                    '<a class="btn btn-default" style="width:50px" href="' + $routeParams.page + '/create/' + data.Id + '" title="edit"><i class="fa fa-edit"></i></a>' +
                    '</div>';
           
        }
        var _commandTemplateStatusComplete = function (data) {
            if (data.StatusFlag === 9 || data.StatusFlag === 5 || data.StatusFlag === 6) {
                return '<div class="btn-group" role="group">' +
                   '<a class="btn btn-default" style="width:60px" href="' + $routeParams.page + '/detail/' + data.Id + '" title="view"><i class="fa fa-file-text"></i></a>' +
                   '<span class="btn btn-default" disabled="disabled" style="width:60px" title="edit"><i class="fa fa-edit"></i></span>' +
               '</div>';
            }
            return '<div class="btn-group" role="group">' +
                '<a class="btn btn-default" style="width:60px" href="' + $routeParams.page + '/detail/' + data.Id + '" title="view"><i class="fa fa-file-text"></i></a>' +
                '<a class="btn btn-default" style="width:60px" href="' + $routeParams.page + '/create/' + data.Id + '" title="edit"><i class="fa fa-edit"></i></a>' +
                '</div>';

        }
        //var _detailTemplate = function (data) {
        //    return '<div class="btn-group" role="group">' +
        //            '<a class="btn btn-default" style="width:60px" href="\' + $routeParams.page + '/detail/' + data.Id + '"><i class="fa fa-file-text"></i> Detail</a>' +
        //        '</div>';
            //}
           
        this.commandColumAllstatus = { template: _commandTemplateAllstatus, width: '220px' };
        this.commandTemplateAllstatus = _commandTemplateAllstatus;
        this.commandColumn = { template: _commandTemplate, width: '90px' };//ของเดิม Width 130 px นะ
        this.commandTemplateCurrentUserName = { template: _commandTemplateCurrentUserName, width: '90px' };//ของเดิม Width 130 px นะ
        this.commandColumnforTeam = { template: _commandTemplateforTeam, width: '220px' };
        this.commandColumnAllowEdit = { template: _commandTemplateAllowEdit, width: '220px' };
        this.commandTemplate = _commandTemplate;
        this.commandTemplateStatusComplete = { template: _commandTemplateStatusComplete, width: '220px' };
        this.editColumn = { template: _editTemplate, width: '100px' };
        this.detailColumn = { template: _detailTemplate, width: '120px' };
        this.formatDateTime = function (fieldname) {
            return '#if(' + fieldname + '){##=kendo.toString(new Date(' + fieldname + '), "dd/MM/yyyy HH:mm")##}#';
            //return '#if(' + fieldname + '){##=kendo.toString(new Date(' + fieldname + '), "F")##}#';
            //return '#console.log(new Date(' + fieldname + '))##if(' + fieldname + '){##=kendo.parseDate(' + fieldname + ', [\'yyyy-MM-ddTHH:mm:ss.fffz\',\'yyyy-MM-ddTHH:mm:ssz\'])##}#';

        }
        this.checkboxTemplate = function (fieldname) {
            return '<div class="align-center">#if(data.' + fieldname + '){# <i class="fa fa-check-square"></i> #}else{# <i class="fa fa-square-o"></i> #}#</div>';
        }
        this.formatDate = function (fieldname) {
            return '#if(' + fieldname + '){##=kendo.toString(new Date(' + fieldname + '), "dd/MM/yyyy" )##}#';
        }
        this.formatCurrency = function (fieldname) {
            return '<div class="text-right">#if(data.' + fieldname + '){##=kendo.toString(data.' + fieldname + ', "n2" ) ##}else{##="0.00"##}# </div>';
        }
        //this.formatCurrency = function (fieldname) {
        //    return '<div class="text-right">#if(' + fieldname + '){##=kendo.toString(' + fieldname + ', "n2" ) ##}#</div>';
        //}
        this.formatPercentage = function (fieldname) {
            return '<div class="text-right">#if(' + fieldname + '){##=kendo.toString(' + fieldname + ', "p0" ) ##}#</div>';
        }
        this.formatCurrency4 = function (fieldname) {
            return '<div class="text-right">#if(data.' + fieldname + '){##=kendo.toString(data.' + fieldname + ', "n4" ) ##}else{##="0.0000"##}# </div>';
        }
        //this.formatCurrency4 = function (fieldname) {
        //    return '<div class="text-right">#=kendo.toString(' + fieldname + ', "n4" ) #</div>';
        //}
        this.gridKendoOption = function (url, params) {
            params = params || {};
            var fields = {
                CreatedDate: { type: "date" }
            };
            if (angular.isDefined(params.fields)) {
                angular.extend(fields, params.fields);
            }
            return {
                dataSource: new kendo.data.DataSource({
                    type: "odata",
                    transport: params.transport || {
                        read: {
                            async: true,
                            url: url,
                            dataType: "json",
                            data: params.readData || undefined
                            //beforeSend: function (req) {
                            //    req.setRequestHeader('Authorization', "Bearer " + token);
                            //}
                        },
                        parameterMap: params.parameterMap || undefined
                    },
                    filter: (params.filters) ? params.filters : null,
                    sort: (params.sorts) ? params.sorts : null,
                    batch: false,
                    serverPaging: true,
                    serverSorting: true,
                    serverFiltering: true,
                    pageSize:(params.pageSize)?params.pageSize : 10,
                    schema: {
                        data: function (data) { return data.value; },
                        total: function (data) { return data["odata.count"]; },
                        model: kendo.data.Model.define({
                            id: "Id",
                            fields: fields
                        })
                    },
                    error: function (e) {
                        //try {
                        //    var resp = angular.fromJson(e.xhr.responseText);
                        //    alert(resp.Message);
                        //} catch (e) {
                        //    alert(e.xhr.responseText);
                        //}
                    },
                    group: params.group
                }),
                editable: false,
                // filterable: true,
                filterable: {
                    extra: true, //do not show extra filters
                    operators: { // redefine the string operators
                        string: {
                            contains: "Contains",
                            //doesnotcontain: "Does not contain",
                            //eq: "Is Equal To",
                            //neq: "Is not equal to",
                            //startswith: "Starts With",
                            //endswith: "Ends with"

                        }
                    }
                },
                groupable: false,
                pageable: {
                    refresh: true,
                    pageSizes: [5, 10, 20, 50, 100]
                },
                reorderable: false,
                resizable: false,
                selectable: "row",
                sortable: {
                    allowUnsort: true,
                    mode: "single"
                },
                columns: params.columns || [],
                //scrollable: angular.isDefined(params.scrollable) ? params.scrollable : true,
                height: params.height,
                scrollable: true,
                //height: 'auto',
                mobile: params.mobile || false,
                toolbar: params.toolbar

            };
        }
        this.gridKendoOptionApi = function (url, params) {
            params = params || {};
            var fields = {
                CreatedDate: { type: "date" }
            };
            if (angular.isDefined(params.fields)) {
                angular.extend(fields, params.fields);
            }
            return {
                dataSource: new kendo.data.DataSource({
                    type: "json",
                    transport: {
                        read: url
                    },
                    
                    filter: (params.filters) ? params.filters : null,
                    sort: (params.sorts) ? params.sorts : null,
                    batch: false,
                    serverPaging: true,
                    serverSorting: true,
                    serverFiltering: true,
                    pageSize: (params.pageSize) ? params.pageSize : 10,
                   
                    error: function (e) {
                        //try {
                        //    var resp = angular.fromJson(e.xhr.responseText);
                        //    alert(resp.Message);
                        //} catch (e) {
                        //    alert(e.xhr.responseText);
                        //}
                    },
                    group: params.group
                }),
                editable: false,
                // filterable: true,
                filterable: {
                    extra: true, //do not show extra filters
                    operators: { // redefine the string operators
                        string: {
                            contains: "Contains",
                            //doesnotcontain: "Does not contain",
                            //eq: "Is Equal To",
                            //neq: "Is not equal to",
                            //startswith: "Starts With",
                            //endswith: "Ends with"

                        }
                    }
                },
                groupable: false,
                pageable: {
                    refresh: true,
                    pageSizes: [5, 10, 20, 50, 100]
                },
                reorderable: false,
                resizable: false,
                selectable: "row",
                sortable: {
                    allowUnsort: true,
                    mode: "single"
                },
                columns: params.columns || [],
                //scrollable: angular.isDefined(params.scrollable) ? params.scrollable : true,
                height: params.height || 500,
                scrollable: true,
                //height: 'auto',
                mobile: params.mobile || false,
                // toolbar: null

            };
        }
        var _ownerTemplate = function (data) {
            var t = CURRENT_USERNAME.toUpperCase();
            var i = CURRENT_USERNAME.lastIndexOf('\\');
            if (i > 0) {
                t = t.substring(i + 1).toUpperCase();
            }
            if (data.CreatedBy.toUpperCase() === CURRENT_USERNAME || data.CreatedBy.toUpperCase() === t) {
                return '<span class="owner-column-wrapper" title="Your request"><i class="owner-column-icon fa fa-user"></i></span>';
            }
            if (data.Requester.toUpperCase() === CURRENT_USERNAME || data.Requester.toUpperCase() === t) {
                return '<span class="owner-column-wrapper" title="You are the requester of this request (Creator: ' + data.CreatedBy + ')"><i class="owner-column-icon fa fa-bullhorn"></i></span>';
            }
            return '<span class="owner-column-wrapper" title="Participated request (Requester:'+ data.Requester+ ')"><i class="owner-column-icon fa fa-eye"></i></span>';
        }
        this.ownerColumn = {
            template: _ownerTemplate, width: '45px',
            field: 'Requester', 
            title: ' ',
            filterable: {
                extra: false,
                operators: {
                    string: {
                        contains: "Contains",
                    }
                }
            },
        };
        this.createdDateColumn = {
            field: 'CreatedDate', title: 'Created date', template: this.formatDateTime('CreatedDate'),
            width: '125px',
            filterable: {
                ui: "datetimepicker"
            }
        };
        this.sortOption = { field: 'CreatedDate', dir: 'desc' };
        }
]);