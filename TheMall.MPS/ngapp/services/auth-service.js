﻿"use strict";
angular.module("app.services")
    .factory("authService",
    [
        "appUrl", "$http", "$q", "$cookies", "$location", "menuServiceData", "gettextCatalog",
        function (appUrl, $http, $q, $cookies, $location, menuServiceData, gettextCatalog) {

            var apiUrl = appUrl.api,
                tokenUrl = appUrl.token,
                _token = function () {
                    return $cookies.get('token');
                },
                _bearer = function () {
                    return 'Bearer ' + _token();
                },
                _xReferer = function () {
                    return $location.path();
                };

            var factory = {};

            var _authentication = {
                isAuth: false,
                userName: "",
                me: {},
                checkLogin: false
            };

            var _logout = function () {

                // delete $sessionStorage.authorizationData;
                $cookies.remove('token', { 'path': '/' });
                $cookies.remove('username', { 'path': '/' });  
                $cookies.remove('hideBroker', { 'path': '/' });
                $cookies.remove('me', { 'path': '/' });
                _authentication.isAuth = false;
                _authentication.userName = "";
                _authentication.hideBroker = false;
                _authentication.checkLogin = false;
                menuServiceData.clear();

                $location.path("/login");
            };

            var _login = function (loginData) {
                var deferred = $q.defer();

                if (!loginData.userName && !loginData.password) {
                    deferred.reject({ error_description: gettextCatalog.getString("Please enter username and password.") });
                } else if (!loginData.userName) {
                    deferred.reject({ error_description: gettextCatalog.getString("Please enter username.") });
                } else if (!loginData.password) {
                    deferred.reject({ error_description: gettextCatalog.getString("Please enter password.") });
                } else {
                    var data = "grant_type=password&username=" + encodeURIComponent(loginData.userName) + "&password=" + encodeURIComponent(loginData.password);
                    //var data = angular.extend(loginData, { "grant_type": "password" });
                    $http.post(tokenUrl, data, { headers: { 'Content-Type': "application/x-www-form-urlencoded" } }).success(function (response) {
                        if (!response.access_token || !loginData.userName) {
                            deferred.reject({ error_description: gettextCatalog.getString('There is an error with Authentication server. Please contact system administrator.') });
                        } else {
                            //$sessionStorage.authorizationData = { token: response.access_token, userName: loginData.userName };
                            var expDate = new Date();
                            expDate.setSeconds(expDate.getSeconds() + response.expires_in);
                            $cookies.put('token', response.access_token, { 'expires': expDate, 'path': '/' });
                            $cookies.put('username', loginData.userName, { 'expires': expDate, 'path': '/' });
                            $cookies.put('me', response.me, { 'expires': expDate, 'path': '/' });
                            _authentication.isAuth = true;
                            _authentication.userName = loginData.userName;
                            _authentication.me = angular.fromJson(response.me);
                            _authentication.checkLogin = true;
                            CURRENT_USERNAME = loginData.userName;
                            deferred.resolve(response);
                        }
                    }).error(function (err) {
                        _logout();
                        err = err || {
                            error_description: gettextCatalog.getString('There is an error with Authentication server. Please contact system administrator.')
                        };
                        deferred.reject(err);
                    });
                }

                return deferred.promise;
            };

            var _fillAuthData = function () {
                var authData = $cookies.get('token');
                var username = $cookies.get('username');
                var me = angular.fromJson($cookies.get('me'));
                if (authData && username) {
                    _authentication.isAuth = true;
                    _authentication.userName = username;
                    _authentication.me = me;
                    CURRENT_USERNAME = username;
                } else if ($location.path() !== "/login") {
                    var redirect = $location.url();
                    $location.path("/login");
                    if (redirect && redirect !== '/') {
                        $location.search("redirect", redirect);
                    } // Redirect to login on app start if not authenticated
                }
            };

            var _saveRegistration = function (registration) {

                _logout();

                return $http.post(apiUrl + "rest/account/register", registration).then(function (response) {
                    return response;
                });

            };

            var _forceLogin = function () {

                // delete $sessionStorage.authorizationData;
                $cookies.remove('token', { 'path': '/' });
                $cookies.remove('username', { 'path': '/' });
                $cookies.remove('hideBroker', { 'path': '/' });
                $cookies.remove('me', { 'path': '/' });
                _authentication.isAuth = false;
                _authentication.userName = "";
                _authentication.hideBroker = false;
                _authentication.checkLogin = false;
                menuServiceData.clear();

                var redirect = $location.url();
                $location.path("/login");
                if (redirect && redirect !== '/') {
                    $location.search("redirect", redirect);
                } // Redirect to login on app start if not authenticated
            };

            factory.saveRegistration = _saveRegistration;
            factory.login = _login;
            factory.logout = _logout;
            factory.fillAuthData = _fillAuthData;
            factory.authentication = _authentication;
            factory.token = _token;
            factory.bearer = _bearer;
            factory.xReferer = _xReferer;
            factory.setAuthHeader = function(xhr) {
                xhr.setRequestHeader('Authorization', _bearer());
                xhr.setRequestHeader('X-Referer', $location.path());
            };
            factory.isAuth = function() {
                return _authentication && _authentication.isAuth;
            };
            factory.forceLogin = _forceLogin;
            return factory;
        }
    ]
);