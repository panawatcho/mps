﻿'use strict';

angular.module("app.services")
    .factory('fileManagerService',
        [
            "appUrl", "$resource", "$http", "uploadService", "$q",
            function (appUrl, $resource, $http, uploadService, $q) {
                var urlDirectory = appUrl.api + "filemanager/directory/",
                    urlDocument = appUrl.api + "filemanager/directory/:directoryId/document/",
                    urlFile = appUrl.api + "filemanager/directory/:directoryId/document/:documentId/attachment";

// ReSharper disable once InconsistentNaming
                var Directory = $resource(urlDirectory);
                var Document = $resource(urlDocument);
                var File = $resource(urlFile);

                var service = {};

                service.getDirectories = function () {
                    return Directory.query().$promise;
                }

                service.getDocuments = function (directory) {
                    if (directory.Id === 0) {
                        return $q.when([]);
                    }
                    return Document.query({ directoryId: directory.Id }).$promise;
                }

                service.isDocument = function(obj) {
                    return obj.Subfolders == undefined;
                }

                service.getFiles = function (document) {
                    return File.query({ directoryId: document.DirectoryId, documentId: document.Id }).$promise;
                }
                service.uploadFiles = function (document, files, attachments, data) {
                    var url = angular.crs.interpoleUrl(urlFile, {
                            directoryId: document.DirectoryId,
                            documentId: document.Id
                        });
                    var promises = uploadService.uploadFiles(url, files, data);
                   
                    return promises;
                };

                return service;
            }
        ]
    );