﻿"use strict";
angular.module("app.services")
    .factory("authInterceptorService",
    ["appUrl", "$q", "$location", "$cookies",
        function (appUrl, $q, $location, $cookies) {
            var factory = {};
            var myApi = appUrl.api;
            var _request = function(config) {
                if (config.url.indexOf(myApi) !== -1) {
                    config.headers = config.headers || {};

                    var authData = $cookies.get('token');
                    if (authData) {
                        config.headers.Authorization = "Bearer " + authData;
                        config.headers["X-Referer"] = $location.path();
                    }
                }

                return config;
            };

            var _responseError = function(rejection) {
                if (rejection.status === 401) {
                    $location.path("/login");
                }
                return $q.reject(rejection);
            };

            factory.request = _request;
            factory.responseError = _responseError;

            return factory;
        }
    ]);