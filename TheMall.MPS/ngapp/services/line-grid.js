﻿'use strict';

angular.module("app.services")
    .service('lineGrid',
    [
        function () {

            var approveData = [];
            //ken
            var setSequenceNumber = function (dataDeleted, dataSource) {
                var data = dataSource._data;
                var dataDeletedApproverLevel = dataDeleted.ApproverLevel;
                var dataDeletedApproverSequence = dataDeleted.ApproverSequence;
                var dataInDeleteGrid = [];
               // approveData = [];
                if (data) {
                    angular.forEach(data, function(value, key) {
                        if (value.ApproverLevel === dataDeletedApproverLevel) {
                            if (!value.Deleted) {
                                dataInDeleteGrid.push(value);
                            }
                        } else {
                            approveData.push(value);
                        }
                    });
                }
                if (dataInDeleteGrid[0]) {
                    angular.forEach(dataInDeleteGrid, function (value, key) {
                        if (value.ApproverSequence > dataDeletedApproverSequence) {
                            value.ApproverSequence = value.ApproverSequence - 1;
                          //  dataSource.pushUpdate(value);
                            console.log(dataSource);
                        }
                        approveData.push(value);
                    });
           // return approveData;
                }
            }
            //ken

            var pseudoDelete = function (e) {
              
                e.preventDefault();

                var row = $(e.currentTarget).closest("tr");
                if (!this._confirmation(row)) {
                    return;
                }
                
                var dataItem = this.dataItem(row);

                //if (angular.isDefined(dataItem.StatusFlag)
                //    && dataItem.StatusFlag > 0) {

                dataItem.set('Deleted', true);
                //////////////////ken
                // var columnsData = 
               
                if (dataItem.ApproverLevel !== undefined) {
                   
                    setSequenceNumber(dataItem, this.dataSource);
                }
                //console.log(dataItem);
                //ken
                //} else {
                //    this._removeRow(row);
                //}
                //////////////////ken
                this.$angular_scope.$apply();
            }
            var _nextLineNo = function (lines) {
                if (lines === undefined || lines.length <= 0)
                    return 1;
                return Math.max.apply(Math,
                    lines.map(function (o) {
                        return o.LineNo;
                    })) + 1;
            }
            this.nextLineNo = _nextLineNo;

            var _pseudoDelete = {
                name: 'pseudo-delete',
                text: '',
                className: 'grid-button-mini',
                click: pseudoDelete,
                template: '<a class="k-button k-button-icontext grid-button-mini k-grid-pseudo-delete" href="\\#"><i class="fa fa-trash-o"></i></a>'
            },
                _edit = { name: 'edit', text: '', className: 'grid-button-mini' };
            this.pseudoDelete = _pseudoDelete;
            this.edit = _edit;
            this.commands = [_edit, _pseudoDelete];
            this.commandColumn = {
                command: this.commands,
                width: '100px',
                locked: true,
                attributes: {
                    "class": "line-grid-command-column"
                }
            };
            var _commandColumn = function (params) {
               
                var c = [];
                if (!params.noedit) {
                    c.push({
                        name: 'edit', text: '',
                        className: 'grid-button-mini',
                        template: '<a class="k-button k-button-icontext k-warning k-grid-edit" title="edit"><i class="fa fa-edit"></i></a>'
                    });

                }
                if (!params.nodelete) {
                    c.push({
                        name: 'pseudo-delete',
                        text: '',
                        className: 'grid-button-mini',
                        click: pseudoDelete,
                        template: params.pseudoDeleteButtonTemplate || '<a class="k-button k-button-icontext grid-button-mini k-grid-pseudo-delete" href="\\#" title="delete"><i class="fa fa-trash-o"></i></a>'
                    });
                }
                return {
                    command: c,
                    width: c.length === 1 ? '60px' : '100px',
                    //width: '230px',
                    locked: angular.isDefined(params.nolock) ? !params.nolock : false,
                    attributes: {
                        "class": "line-grid-command-column"
                    }
                };
            };
            this.kendoDataSource = function (o) {
                var opt = angular.extend({}, o);
                opt.filter =
                {
                    logic: "or",
                    filters: [
                        { field: "Deleted", operator: "eq", value: false },
                        { field: "Deleted", operator: "eq", value: null }
                    ]
                };
                return new kendo.data.DataSource(opt);
            }
            this.formatDateTime = function (fieldname) {
                return '#if(data && data.' + fieldname + '){##=kendo.toString(new Date(data.' + fieldname + '),"dd/MM/yyyy" )##}#';
            }
            this.formatDate = function (fieldname) {
                return '#if(data && data.' + fieldname + '){##=kendo.toString(new Date(data.' + fieldname + '), "D")##}#';
            }
            this.formatTime = function (fieldname) {
                return '#if(data && data.' + fieldname + '){##=kendo.toString(new Date(data.' + fieldname + '), "HH:mm")##}#';
            }
            this.formatCurrency = function (fieldname) {
                return '<div class="text-right">#if(data.' + fieldname + '){##=kendo.toString(data.' + fieldname + ', "n2" ) ##}else{##="0.00"##}# </div>';
            }
            this.formatCurrencyZero = function (fieldname) {
                return '<div class="text-right">#if(data.' + fieldname + '){##=kendo.toString(data.' + fieldname + ', "n2" ) ##}else{##=" "##}# </div>';
            }
            this.formatQuantity = function (fieldname) {
                return '<div class="text-right">#=kendo.toString(data.' + fieldname + ', "n0" ) #</div>';
            }
            this.formatPercent = function (fieldname) {
                return '<div class="text-right">#if(data.' + fieldname + '){##=kendo.toString(data.' + fieldname + ', "n2" ) ##}else{##=""##}# </div>';
            }
            this.checkboxTemplate = function (fieldname) {
                return '<div class="align-center">#if(data.' + fieldname + '){# <i class="fa fa-check-square"></i> #}else{# <i class="fa fa-square-o"></i> #}#</div>';
            }
            this.radioTemplate = function (fieldname) {
                return '<div class="align-center">#if(data.' + fieldname + '){# <i class="fa fa-circle"></i> #}else{# <i class="fa fa-circle-thin"></i> #}#</div>';
            }
            this.checkedTemplate = function (fieldname) {
                return "#if(data && data." + fieldname + "){# <div style='width:100%;text-align:center'><i class='fa fa-check fa-2x'></i></div> #}#";
            };
            this.formatNo = function (fieldname) {
                return '<div class="text-center">#if(data.' + fieldname + '){##=kendo.toString(data.' + fieldname + ', "n0" ) ##}else{##="0"##}# </div>';
            }

            this.kendoFastRedrawRow = function (grid, row, columnToRefresh) {
                var dataItem = grid.dataItem(row);
                var rowChildren = $(row).children('td[role="gridcell"]');

                if (columnToRefresh === undefined) {
                    columnToRefresh = grid.columns;
                } else {
                    if (!angular.isArray(columnToRefresh)) {
                        columnToRefresh = [grid.columns[columnToRefresh]];
                    } else {
                        var t = [];
                        $.map(columnToRefresh, function (c) {
                            t.push(grid.columns[c]);
                        });
                        columnToRefresh = t;
                    }
                }

                for (var i = 0; i < columnToRefresh.length; i++) {

                    var column = columnToRefresh[i];
                    var template = column.template;
                    var cell = rowChildren.eq(i);

                    if (template !== undefined) {
                        var kendoTemplate = kendo.template(template);

                        // Render using template
                        if (dataItem.get(column.field) !== null) {
                            cell.html(kendoTemplate(dataItem));
                        }
                    } else {
                        var fieldValue = dataItem[column.field];

                        var format = column.format;
                        var values = column.values;

                        if (values !== undefined && values !== null) {
                            // use the text value mappings (for enums)
                            for (var j = 0; j < values.length; j++) {
                                var value = values[j];
                                if (value.value === fieldValue) {
                                    cell.html(value.text);
                                    break;
                                }
                            }
                        } else if (format !== undefined) {
                            // use the format
                            cell.html(kendo.format(format, fieldValue));
                        } else {
                            // Just dump the plain old value
                            cell.html(fieldValue);
                        }
                    }
                }
            }
            this.maxLengthValidation = function (field, length) {
                return function (input) {
                    if (input.is("[name='" + field + "']") && input.val() !== "") {
                        input.attr("data-maxlengthvalidation-msg", field + " is too long. (max: " + length + ")");
                        return input.val().length <= length;
                    }

                    return true;
                }
            }
            var LineNo = "LineNo";
            var onEdit = function (params) {
                return function (e) {
                   
                    function base(e) {
                        //$(e.container).parent().css({
                        //    width: window.innerWidth - 40,
                        //    left: 20
                        //});

                        if (e.model) {
                            var defaultValues = {};
                            if (params.defaultValues) {
                                defaultValues = params.defaultValues;
                                if ($.isFunction(params.defaultValues)) {
                                    defaultValues = params.defaultValues();
                                }
                            }
                            else if (!params.defaultValues && e.model.idField === LineNo) {
                                defaultValues = {
                                    LineNo: _nextLineNo((e.sender.dataSource ? e.sender.dataSource.data() : false) || params.data)
                                };
                            }
                            if (e.model.isNew && e.model.isNew()) {
                                var w = $(e.container).getKendoWindow();
                                if (w) {
                                    w.title('Add');
                                }
                                if (e.model.idField === LineNo && (e.model.id === 0 || e.model.id === "")) {
                                    e.model.set(LineNo, _nextLineNo((e.sender.dataSource ? e.sender.dataSource.data() : false) || params.data));
                                    //e.model.LineNo = _nextLineNo((e.sender.dataSource ? e.sender.dataSource.data() : false) || params.data);
                                }
                                $.map(defaultValues, function (value, key) {
                                    if (key !== e.model.idField) {
                                        if ($.isFunction(value)) {
                                            e.model.set(key, value());
                                        } else {
                                            e.model.set(key, value);
                                        }
                                    }
                                });
                            } else {
                                //$.map(defaultValues, function (value, key) {
                                //    if ($.isFunction(value)) {
                                //        e.model.set(key, value());
                                //    } else {
                                //        e.model.set(key, value);
                                //    }
                                //});
                            }
                        }
                    }

                    if (params.edit) {
                        if (params.edit(e) !== false) {
                            base(e);
                        }
                    } else {
                        base(e);
                    }
                }
            }
           
            var gridKendoOption = function (params) {
                params = params || {};
                params.readonly = params.readonly || false;
                params.nocommand = params.nocommand || false;
                params.columns = params.columns || [];
                params.noadd = params.noadd || false;
                var datasource = params.dataSource;
                var cm = _commandColumn(params);
                if (params.moreCommands) {
                    for (var j = 0; j < params.moreCommands.length; j++) {
                        cm.command.push(params.moreCommands[j]);
                    }
                }
                if (!params.readonly && !params.nocommand) {
                    params.columns.unshift(cm);
                }
                if (params.readonly && params.nocommand && params.columns && params.columns.length > 1 && params.columns[0].command) {
                    var oldCol = params.columns[0].command,
                        newcol = null;
                    if (angular.isArray(oldCol)) {
                        newcol = params.columns[0].command;
                        for (var i = 0; i < params.columns[0].command.length; i++) {
                            var com = params.columns[0].command[i];
                            if (com.name === 'destroy' || com.name === 'edit') {
                                newcol = newcol.splice(0, 0);
                            }
                        }
                        if (newcol.length === 0) {
                            params.columns = params.columns.splice(1);
                        } else {
                            params.columns[0].command = newcol;
                        }
                    } else {
                        if (oldCol.name === 'destroy' || oldCol.name === 'edit') {
                            params.columns = params.columns.splice(1);
                        }
                    }
                }
                if (!params.dataSource) {
                    if (angular.isUndefined(params.group) && angular.isUndefined(params.aggregate)) {
                        datasource = new kendo.data.DataSource({
                            data: params.data,
                            schema: params.schema,
                            sort: params.sort// || { field: "LineNo", dir: "asc" }
                        });
                    } else {
                        datasource = new kendo.data.DataSource({
                            data: params.data,
                            schema: params.schema,
                            group: params.group,
                            aggregate: params.aggregate,
                            sort: params.sort// || { field: "LineNo", dir: "asc" }
                        });
                    }
                   
                }
                datasource.filter(params.filter || 
                    {
                        logic: "or",
                        filters: [
                            { field: "Deleted", operator: "eq", value: false },
                            { field: "Deleted", operator: "eq", value: null }
                        ]
                    }
                );
                return {
                    dataSource: datasource,
                    selectable: params.selectable || false,
                    groupable: false,
                    reorderable: false,
                    resizable: false,
                    sortable: params.sortable || false,
                    sort: (params.sort) ? params.sort : null,
                    filterable: params.filterable || false,
                    scrollable: params.scrollable !== undefined ? params.scrollable : true,
                    pageable: params.pageable || false,
                    height: params.height ,
                    toolbar: params.toolbar || ((params.readonly || params.noadd) ? null : ["create"]),
                    editable: params.readonly ? false : function () {
                        if (angular.isDefined(params.editable) && params.editable === false) {
                            return false;
                        }
                        var d = {
                            mode: "popup",
                            confirmation: true,
                            confirmDelete: "Yes",
                            cancelDelete: "No",
                            window: {
                                animation: false,
                                draggable: false
                            },
                            template: params.template,
                            createAt: 'bottom'
                        };
                        return !params.editable ? d : angular.extend(d, params.editable);
                    }(),
                    
                    columns: params.columns ,
                    edit: onEdit(params),
                    save: params.save,
                    cancel: params.cancel,
                    dataBound: params.dataBound,
                    detailTemplate: params.detailTemplate || undefined
                }
            }

            this.gridOption = gridKendoOption;
        
        }
    ]);