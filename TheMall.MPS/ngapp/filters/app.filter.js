﻿angular.module("app.filters")
    .filter('unsafe',
    [
        "$sce",
        function($sce) {
            return $sce.trustAsHtml;
        }
    ])
    .provider('formatFileSizeFilter', [
        function() {
            var $config = {
                // Byte units following the IEC format
                // http://en.wikipedia.org/wiki/Kilobyte
                units: [
                    { size: 1000000000, suffix: ' GB' },
                    { size: 1000000, suffix: ' MB' },
                    { size: 1000, suffix: ' KB' }
                ]
            };
            this.defaults = $config;
            this.$get = function() {
                return function(bytes) {
                    if (!angular.isNumber(bytes)) {
                        return '';
                    }
                    var unit = true,
                        i = 0,
                        prefix,
                        suffix;
                    while (unit) {
                        unit = $config.units[i];
                        prefix = unit.prefix || '';
                        suffix = unit.suffix || '';
                        if (i === $config.units.length - 1 || bytes >= unit.size) {
                            return prefix + (bytes / unit.size).toFixed(2) + suffix;
                        }
                        i += 1;
                    }
                };
            };
        }
    ])
    .filter('otherFiles',
    [
        function() {
            return function(input) {
                var out = [];
                angular.forEach(input, function(obj) {

                    if (!obj.documentType) {
                        out.push(obj);
                    }

                });
                return out;
            }
        }
    ])
    .filter('documentType',
    [
        function() {
            return function(input, type) {
                var out = [];
                angular.forEach(input, function(obj) {

                    if (obj.documentType === type) {
                        out.push(obj);
                    }

                });
                return out;
            }
        }
    ]);