﻿"use strict";
angular.module("app.values")
    .value("appUrl", {
        //Deployment
       // token: "//api.mps.local/token", // DON'T end token with slash
       // api: "//api.mps.local/api/"
        token: "//10.90.234.41/mps.api/token", // DON'T end token with slash
        api: "//10.90.234.41/mps.api/api/"
        //token: "//10.90.240.24/mps.api/token", // DON'T end token with slash
        //api: "//10.90.240.24/mps.api/"
    })
    .value("projectName", "<project name>")
    .value("customerName", "<customer name>");