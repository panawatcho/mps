﻿angular.module('app.constants')

    .constant('imageChecker', function (name) {
        if (name) {
            var xs = (/(?:\.([^.]+))?$/.exec(name)), n = name.split('\\').pop().split('/').pop(), x = xs[1], i = n.lastIndexOf('.');
            if (i > 0) {
                n = n.substring(0, i);
            }
            if (/^[^%~#&*{}\\\\:<>?/+|\"\.]+$/i.test(n) && x) {
                if (/(\.|\/)(gif|jpe?g|png|tiff?)$/i.test('.' + x)) {
                    return false;
                } else {
                    return "Invalid file type. " + x.toUpperCase() + ' is not allowed to be upload.';
                }
            } else {
                if (!x) {
                    return "Invalid file name. Empty extension file is not allowed to be uploaded.";
                }
                return "Invalid file name. ~ % # & * { } \\ : < > / + | \" . are not allowed.";
            }
        }
        return false;
    })
.value('maxFileSize', 104857600)
;

/*
            { value: 1, name: 'Quotation' },
            { value: 2, name: 'Specification' },
            { value: 3, name: 'VendorRegister' },
            { value: 9, name: 'Other' }
*/