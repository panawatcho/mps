﻿var gulp = require('gulp');
var gettext = require('gulp-angular-gettext');

gulp.task('translate:extract', function () {
    return gulp.src(['views/*/*.html', 'ngapp/*/*.js'])
        .pipe(gettext.extract('template.pot', {
            // options to pass to angular-gettext-tools... 
        }))
        .pipe(gulp.dest('po/'));
});
gulp.task('translate', function () {
    return gulp.src('po/**/*.po')
        .pipe(gettext.compile({
            // options to pass to angular-gettext-tools... 
            format: 'json'
        }))
        .pipe(gulp.dest('languages/'));
});