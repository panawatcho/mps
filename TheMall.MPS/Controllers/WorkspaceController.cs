﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PRUKSA.PRC.Controllers
{
    public class WorkspaceController : Controller
    {
        public ViewResult Index()
        {
            return View();
        }

        public ActionResult Workspace()
        {

            return PartialView();
        }

        public ActionResult K2Submit()
        {

            return PartialView();
        }
	}
}