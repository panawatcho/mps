﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PRUKSA.PRC.Controllers
{
    public class ReportController : Controller
    {
        // GET: /Report/
        public ActionResult Index()
        {
            return View();
        }

        public PartialViewResult View()
        {
            return PartialView();
        }

        public PartialViewResult CashAdvance()
        {
            return PartialView();
        }

        public PartialViewResult MemoCashAdvance()
        {
            return PartialView();
        }

        public PartialViewResult CashClearing()
        {
            return PartialView();
        }

        public PartialViewResult PettyCash()
        {
            return PartialView();
        }
 
        public PartialViewResult Memo()
        {
            return PartialView();
        }

        public PartialViewResult Proposal()
        {
            return PartialView();
        }

        public PartialViewResult Receipt()
        {
            return PartialView();
        }

        public PartialViewResult MemoIncome()
        {
            return PartialView();
        }
    }
}