﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TheMall.MPS.Controllers
{
    public class ProposalController : Abtracts.BaseAngularController
    {
        public ActionResult Complete()
        {
            return PartialView();
        }
        public ActionResult ReviseIncomeDeposit()
        {
            return PartialView();
        }
        public ActionResult ReviseProposal()
        {
            return PartialView();
        }
    }
}