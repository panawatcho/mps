﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TheMall.MPS.Controllers.Abtracts
{

    public abstract class BaseAngularController : Controller
    {
        public ActionResult Index()
        {
            return PartialView();
        }

        [HttpGet]
        public virtual ActionResult Form()
        {
            return PartialView();
        }
        [HttpGet]
        public virtual ActionResult Detail()
        {
            return PartialView();
        }

    }
}