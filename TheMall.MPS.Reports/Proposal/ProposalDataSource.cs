﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheMall.MPS.Reports.Proposal
{
    public class ProposalDataSource
    {
        public string ProposalNumber { get; set; }
        public string ProposalType { get; set; }
        public string ProposalTypeName { get; set; }
        public string ProposalTitle { get; set; }
        public string Requester { get; set; }
        public string Place { get; set; }
        public string CreateReportDate { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string CC { get; set; }
        public string Inform { get; set; }
        public decimal TotalBudget { get; set; }
        public string TotalBudgetInCharacter { get; set; }
        public string AttachmentName { set; get; }
        public string UnitCode { get; set; }
        public string UnitName { get; set; }
        public string Objective { get; set; }
        public string ObjectiveImg { get; set; }
        public string AllocationBasis { get; set; }
    }
}
