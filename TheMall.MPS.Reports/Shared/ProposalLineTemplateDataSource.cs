﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheMall.MPS.Reports.Shared
{
    public class ProposalLineTemplateDataSource
    {
        public decimal TR_Percent { get; set; }
        public decimal RE_Percent { get; set; }
        public string BranchCode { get; set; }
    }
}
