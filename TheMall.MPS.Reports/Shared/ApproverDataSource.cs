﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheMall.MPS.Reports.Shared
{
    public class ApproverDataSource
    {
        public int ApproverId { get; set; }
        public int ApproverSequence { get; set; }
        public string ApproverUsername { get; set; }
        public string ApproverFullName { get; set; }
        public string ApproverLevel { get; set; }
        public string ApproverPosition { get; set; }
        public int ApproverLevelKey { get; set; }
        public string ApproveDateTime { get; set; }

    }
}
