﻿using System;

namespace TheMall.MPS.Reports.Tests
{
    public class Report1DataSource
    {
        public int Id {get; set; }
        public string Name { get; set; }
        public string Date { get; set; }
        public string Amount { get; set; }
        public string Image { get; set; }
    }
}
