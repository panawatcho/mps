﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.Models.K2;

namespace TheMall.MPS.Reports.Memo
{
    public class MemoDataSource
    {
        public int Id { set; get; }
        public string Date { get; set; }
        public string Title { get; set; }
        public string RequesterName { set; get; }
        public string DueDate { set; get; }
        public string Requester { set; get; }
        public string RequesterDepartment { set; get; }
        public string RequestBranch { set; get; }
        public string RequesterPosition { set; get; }
        public string Branch { set; get; }
        public string AttachmentName { set; get; }
        public string MemoDocumentNumber { set; get; }
        public string ExpenseTopic { set; get; }
        public string APCode { set; get; }
        public string CC { set; get; }
        public string Inform { set; get; }
        public string ProposalNumber { set; get; }

        public string UnitCode { get; set; }
        public string ProposalName { get; set; }
        public string ProposalUnitCode { get; set; }
        public string ProposalUnitName { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string SubmitDate { get; set; }

        public string Objective { get; set; }
        public string ObjectiveImg { get; set; }
    }
}
