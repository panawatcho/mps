﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.ViewModels;
using TheMall.MPS.ViewModels.CashAdvance;
using TheMall.MPS.ViewModels.Master;

namespace TheMall.MPS.Reports.CashAdvance
{
    public class CashAdvanceDataSource
    {
        public int Id { get; set; }
        public string BranchDescription { get; set; }
        public string Branch { get; set; }
        public string RequesterName { get; set; }
        public string RequesterBranch { get; set; }
        public string RequesterDepositName { get; set; }
        public string DocumentNumber { get; set; }
        public string Approver { get; set; }
        public string ApprovalLevel { get; set; }
        public string VerifyBy { get; set; }
        public string Recipient { get; set; }
        public string AccountingName { get; set; }
        public decimal Budget { get; set; }
        public string BudgetInCharacter { get; set; }
        public string NetAmountInCharacter { get; set; }
        public string LineDescription { get; set; }
        public decimal LineAmount { get; set; }
        public decimal? LineNetAmount { get; set; }
        public decimal LineUnit { get; set; }
        public decimal LineActual { get; set; }
        public decimal? Tax { get; set; }
        public decimal? Vat { get; set; }
        public string LineRemark { get; set; }
        public string Date { get; set; }

        public string UnitCode { get; set; }
        public string UnitName { get; set; }
        public string ProposalNumber { get; set; }
        public string ProposalName { get; set; }
        public string ProposalUnitCode { get; set; }
        public string ProposalUnitName { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string SubmitDate { get; set; }

        public string Objective { get; set; }
        public string ObjectiveImg { get; set; }
        public string Company { get; set; }
        public string ExpenseTopic { set; get; }
        public string APCode { set; get; }
    }
}
