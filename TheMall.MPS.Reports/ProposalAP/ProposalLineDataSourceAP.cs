﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheMall.MPS.Reports.ProposalAP
{
    public class ProposalLineDataSourceAP
    {
        public string ExpenseTopicCode { get; set; }
        public string ExpenseTopicName { get; set; }
        public string Description { get; set; }
        public string APCode { get; set; }
        public decimal Actual { get; set; }
        public decimal BudgetPlan { get; set; }
        public string TypeAP { get; set; }
        public string CurrentYear { get; set; }
        public string LastYear { get; set; }
        public decimal Percent { get; set; }
        public decimal TotalAdvertising { get; set; }
        public decimal TotalPromotion { get; set; }
        public string UnitCode { get; set; }
        public string UnitName { get; set; }
        public decimal Different { get; set; }
        public decimal PercentAdvertingSaleTarget { get; set; }
        public decimal PercentPromotionSaleTarget { get; set; }
        public decimal PercentMarketingSaleTarget { get; set; }
        public decimal SaleTarget { get; set; }
        public decimal SaleTargetLastYear { get; set; }
        public int TypeAPSort { get; set; }
    }
}
