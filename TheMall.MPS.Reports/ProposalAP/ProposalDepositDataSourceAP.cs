﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheMall.MPS.Reports.ProposalAP
{
    public class ProposalDepositDataSourceAP
    {
        public int Id { get; set; }
        public string PaymentNo { get; set; }
        public DateTime? DatePeriod { get; set; }
        public decimal Amount { get; set; }
        public string ProcessStatus { get; set; }
        public string Source { get; set; }
        public string DepositProposalRef { get; set; }
    }
}
