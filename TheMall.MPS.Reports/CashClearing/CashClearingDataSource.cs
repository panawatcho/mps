﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.ViewModels;
using TheMall.MPS.ViewModels.CashAdvance;
using TheMall.MPS.ViewModels.CashClearing;
using TheMall.MPS.ViewModels.Master;

namespace TheMall.MPS.Reports.CashClearing
{
    public class CashClearingDataSource
    {
        public string RequesterName { get; set; }
        public string RequesterBranch { get; set; }
        public string RequesterDepositName { get; set; }
        public int Id { get; set; }
        public string Branch { get; set; }
        public string CashClearingBranch { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Date { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public decimal? Budget { get; set; }
        public string DocumentNumber { get; set; }
        //public decimal LineAmount { get; set; }
        public decimal? LineNetAmount { get; set; }
        public decimal? Vat { get; set; }
        public decimal? Tax { get; set; }

        ///Line
        public decimal CashAdvanceBudget { get; set; }
        public string CashAdvanceNo{ get; set; }
        public decimal LineUnit { get; set; }
        public decimal LineActual { get; set; }
        public decimal LineNetActual { get; set; }
        public string LineRemark { get; set; }
        public string LineDescription { get; set; }
        public decimal LineVat { get; set; }
        public decimal LineTax { get; set; }
        public string LinePayeeName { get; set; }

        public string Recipient { get; set; }
        public string VerifyBy { get; set; }
        public string ApproveBy1 { get; set; }
        public string ApproveBy2 { get; set; }
        public decimal BudgetSurplus { get; set; }
        public string BudgetSurplusInCharacter { get; set; }
        public string UnitCode { get; set; }
        public string UnitName { get; set; }

        public string ProposalNumber { get; set; }
        public string ProposalName { get; set; }
        public string ProposalUnitCode { get; set; }
        public string ProposalUnitName { get; set; }
        public string SubmitDate { get; set; }

        public string RebateInCharacter { get; set; }
        public decimal Rebate { get; set; }

        public string Objective { get; set; }
        public string ObjectiveImg { get; set; }
        public string Company { get; set; }
        public string ExpenseTopic { set; get; }
        public string APCode { set; get; }
    }
}
