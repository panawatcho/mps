﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.Reports.Abstracts;

namespace TheMall.MPS.Reports.CashClearing
{
    public class CashClearingFormCriteria : DateRangeSelectionCriteria
    {
        public int Id { get; set; }
        public string DocumentNumber { get; set; }
    }
}
