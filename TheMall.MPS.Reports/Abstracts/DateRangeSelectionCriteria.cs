﻿using System;
using TheMall.MPS.Reports.Interfaces;

namespace TheMall.MPS.Reports.Abstracts
{
    public class DateRangeSelectionCriteria : BaseReportCriteria, IDateRangeSelectionCriteria
    {
        public string DateFrom { get; set; }
        public string DateTo { get; set; }
    }
}
