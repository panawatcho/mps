﻿using TheMall.MPS.Reports.Interfaces;

namespace TheMall.MPS.Reports.Abstracts
{
    public abstract class BaseReportCriteria : IReportCriteria
    {
        public string Format { get; set; }
        public int? PageFrom { get; set; }
        public int? PageTo { get; set; }
        public bool Inline { get; set; }
    }
}
