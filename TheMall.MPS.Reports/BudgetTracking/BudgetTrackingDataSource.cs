﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.Models.K2;

namespace TheMall.MPS.Reports.BudgetTracking
{
    public class BudgetTrackingDataSource
    {
        public long RowNo { get; set; }
        public int ProposalId { get; set; }
        public int ProposalLineId { get; set; }
        public string ProposalNo { get; set; }
        public string ProposalUnitCode { get; set; }
        public string UnitCode { get; set; }
        public string APCode { get; set; }
        public string Description { get; set; }
        public decimal BudgetPlan { get; set; }
        public decimal BudgetUsed { get; set; }
        public decimal Memo { get; set; }
        public decimal CashAdvance { get; set; }
        public decimal PettyCash { get; set; }
        public decimal PR { get; set; }
        public decimal PO { get; set; }
        public decimal GR { get; set; }
        public decimal BudgetRemaining { get; set; }
        public bool CloseFlag { get; set; }
        public string ProposalTitle { get; set; }
        public string RequesterName {get; set; }
        public string CreateDate { get; set; }
        public string CampaignStartDate { get; set; }
        public string CampaignEndDate { get; set; }
        public decimal M02_TR { get; set; }
        public decimal M02_RE { get; set; }
        public decimal M03_TR { get; set; }
        public decimal M03_RE { get; set; }
        public decimal M05_TR { get; set; }
        public decimal M05_RE { get; set; }
        public decimal M06_TR { get; set; }
        public decimal M06_RE { get; set; }
        public decimal M07_TR { get; set; }
        public decimal M07_RE { get; set; }
        public decimal M08_TR { get; set; }
        public decimal M08_RE { get; set; }
        public decimal M09_TR { get; set; }
        public decimal M09_RE { get; set; }
        public decimal M10_TR { get; set; }
        public decimal M10_RE { get; set; }
        public decimal M11_TR { get; set; }
        public decimal M11_RE { get; set; }
        public decimal M14_TR { get; set; }
        public decimal M14_RE { get; set; }
        public decimal M15_TR { get; set; }
        public decimal M15_RE { get; set; }
        public decimal M17_TR { get; set; }
        public decimal M17_RE { get; set; }
        public decimal B5_TR { get; set; }
        public decimal B5_RE { get; set; }
        public decimal B7_TR { get; set; }
        public decimal B7_RE { get; set; }
        public decimal V1_TR { get; set; }
        public decimal V1_RE { get; set; }
        public decimal V2_TR { get; set; }
        public decimal V2_RE { get; set; }
        public decimal V3_TR { get; set; }
        public decimal V3_RE { get; set; }
        public decimal V4_TR { get; set; }
        public decimal V4_RE { get; set; }
        public decimal V5_TR { get; set; }
        public decimal V5_RE { get; set; }
        public decimal V9_TR { get; set; }
        public decimal V9_RE { get; set; }
        public string BranchCode { get; set; }
        public string BranchName { get; set; }
    }
}
