﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.ViewModels;
using TheMall.MPS.ViewModels.Master;
using TheMall.MPS.ViewModels.PettyCash;
using TheMall.MPS.ViewModels.Proposal;

namespace TheMall.MPS.Reports.PettyCash
{
    public class PettyCashDataSource
    {
        public int Id { set; get; }
        public string Branch { get; set; }
        public string DocumentNumber { set; get; }
        public string RequesterName { set; get; }
        public string RequesterDepositName { get; set; }
        public string Date { get; set; }

        public decimal Budget { get; set; }
        public string BudgetInCharacter { get; set; }
        public string LineCode { get; set; }
        public string LineAccountName { get; set; }
        public string LineDescription { get; set; }
        public decimal LineAmount { get; set; }
        public decimal? Tax { get; set; }
        public decimal? Vat { get; set; }
        public decimal LineUnit { get; set; }
        public decimal? LineNetAmount { get; set; }
        public string LineRemark { get; set; }
        public string Recipient { get; set; }
        public string Payer { get; set; }
        public string AccountingName { get; set; }
        public string ApproveBy { get; set; }

        public string UnitCode { get; set; }
        public string UnitName { get; set; }

        public string ProposalNumber { get; set; }
        public string ProposalName { get; set; }
        public string ProposalUnitCode { get; set; }
        public string ProposalUnitName { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string SubmitDate { get; set; }
        public bool GoodsIssue { get; set; }

        public string Objective { get; set; }
        public string ObjectiveImg { get; set; }
        public string Company { get; set; }
        public string ExpenseTopic { set; get; }
        public string APCode { set; get; }
    }
}
