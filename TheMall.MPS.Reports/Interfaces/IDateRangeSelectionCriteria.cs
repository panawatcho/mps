﻿using System;

namespace TheMall.MPS.Reports.Interfaces
{
    public interface IDateRangeSelectionCriteria : IReportCriteria
    {
        string DateFrom { get; set; }
        string DateTo { get; set; }
    }
}
