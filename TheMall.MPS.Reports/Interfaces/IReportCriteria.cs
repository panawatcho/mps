﻿namespace TheMall.MPS.Reports.Interfaces
{
    public interface IReportCriteria
    {
        string Format { get; set; }
        int? PageFrom { get; set; }
        int? PageTo { get; set; }
        bool Inline { get; set; }
    }
}