﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.ViewModels;
using TheMall.MPS.ViewModels.Master;
using TheMall.MPS.ViewModels.PettyCash;
using TheMall.MPS.ViewModels.Proposal;

namespace TheMall.MPS.Reports.Receipt
{
    public class ReceiptDataSource
    {
        public int Id { get; set; }
        public string RequesterUserName { get; set; }
        public string RequesterName { get; set; }
        public string RequesterPosition { get; set; }
        public string UnitCode { get; set; }
        public string UnitName { get; set; }
        public string RequestForUserName { get; set; }
        public string RequesterForName { get; set; }
        public string RequesterForPosition { get; set; }
        public string UnitCodeFor { get; set; }
        public string UnitNameFor { get; set; }
        public decimal BudgetDetail { get; set; }
        public string BudgetInCharacter { get; set; }
        public decimal BudgeNoVatTax { get; set; }
        public string BudgetNoVatTaxInCharacter { get; set; }
        public string DocumentNumber { get; set; }
        public string Status { get; set; }
        public int StatusFlag { get; set; }
        public int? RePrintNo { get; set; }
        public string CompletedDate { get; set; }
        public string CompletedByUserName { get; set; }
        public string CompletedByName { get; set; }
        public string PrintDate { get; set; }
    }
}
