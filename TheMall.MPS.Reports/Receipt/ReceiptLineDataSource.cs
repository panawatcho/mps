﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.ViewModels;
using TheMall.MPS.ViewModels.Master;
using TheMall.MPS.ViewModels.PettyCash;
using TheMall.MPS.ViewModels.Proposal;

namespace TheMall.MPS.Reports.Receipt
{
    public class ReceiptLineDataSource
    {
        public string Description { get; set; }
        public decimal Unit { get; set; }
        public decimal Amount { get; set; }
        public string Remark { get; set; }
        public decimal? Vat { get; set; }
        public decimal? Tax { get; set; }
        public decimal? NetAmount { get; set; }
        public decimal? NetAmountNoVatTax { get; set; }
    }
}
