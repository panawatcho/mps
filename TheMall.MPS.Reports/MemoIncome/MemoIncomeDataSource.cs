﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.Models.K2;

namespace TheMall.MPS.Reports.MemoIncome
{
    public class MemoIncomeDataSource
    {
        public int Id { set; get; }
        public int? RePrintNo { get; set; }
        public string DocumentNumber { set; get; }
        public string CompletedDate { get; set; }
        public string CreatedDate { get; set; }
        public string RequesterName { set; get; }  
        public string Requester { set; get; }
        public string RequesterDepartment { set; get; }
        public string RequesterPosition { set; get; }
        public string UnitCode { get; set; }
        public string UnitName { get; set; }
        public string Title { get; set; }
        public string Category { get; set; }
        public string ProposalNumber { set; get; }
        public string ProposalName { get; set; }
        public string ProposalUnitCode { get; set; }
        public string ProposalUnitName { get; set; }
        public string ProposalRefName { get; set; }
        public string ProposalRefDescription { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string AttachmentName { set; get; }
        public string DueDate { set; get; }
        public string CC { set; get; }
        public string Inform { set; get; }
        public string PrintDate { get; set; }
        public string Objective { get; set; }
        public string ObjectiveImg { get; set; }
    }
}
