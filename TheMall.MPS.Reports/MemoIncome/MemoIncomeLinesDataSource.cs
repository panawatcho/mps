﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheMall.MPS.Reports.MemoIncome
{
    public class MemoIncomeLinesDataSource
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public string Remark { get; set; }
        // public bool IsTotalSale { get; set; }
        public decimal Unit { get; set; }
        public decimal Amount { get; set; }
        public decimal? Vat { get; set; }
        public decimal? Tax { get; set; }
        public decimal? NetAmount { get; set; }
    }
}
