﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheMall.MPS.ViewModels
{
    public class SearchDocViewModel
    {
        public string Process { get; set; }
        public string DocumentNumber { get; set; }
        public string Username { get; set; }
        public string Status { get; set; }
        public string Title { get; set; }
        public string UnitCode { get; set; }
        public string UnitName { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? StatusFlag { get; set; }
        public int Id { get; set; }
        public string AllocationCode { get; set; }
    }
}
