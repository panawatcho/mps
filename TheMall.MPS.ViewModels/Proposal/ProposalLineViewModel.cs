﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.ViewModels.Abstracts;
using TheMall.MPS.ViewModels.Master;

namespace TheMall.MPS.ViewModels.Proposal
{
    public class ProposalLineViewModel : BaseLineViewModel
    {
        public string ExpenseTopicCode { get; set; }
        public string ExpenseTopicName { get; set; }
        public ExpenseTopicViewModel ExpenseTopic { get; set; } //
        public string TypeAP { get; set; }
        public APCodeViewModel APCode { get; set; }
        public UnitCodeViewModel UnitCode { get; set; }
        public string Description { get; set; }
        public decimal BudgetPlan { get; set; }
        public decimal? Actual { get; set; }
        public decimal? Percent { get; set; }
        public decimal? Amount { get; set; }
        public string Remark { get; set; }
        public Dictionary<string, ProposalPercentSharedViewModel> PercentShared { get; set; }
        public IEnumerable<ProposalLineTemplateViewModel> ProposalLineTemplate { get; set; }
        public bool CheckBudget { get; set; }
        public decimal? Used { get; set; }
        public decimal? Remainning { get; set; }
        public decimal TotalBudgetPlan { get; set; }
    }
}
 