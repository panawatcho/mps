﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheMall.MPS.ViewModels.Proposal
{
    public class DepositAccountTransViewModel
    {
        public int Id { get; set; }
        public decimal? Actual { get; set; }
        public string Remark { get; set; }
        public int MemoIncomeId { get; set; }
        public int DepositLineId { get; set; }
        public int LineNo { get; set; }
        public bool Deleted { get; set; }
        public string InvoiceNo { get; set; }

    }
}
