﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheMall.MPS.ViewModels.Proposal
{
    public class ProposalLineSharedTemplateViewModel
    {
        public int Id { get; set; }
        public int ParentId { get; set; }
        public string BranchCode { get; set; }
        public bool Selected { get; set; }
    }
}
