﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TheMall.MPS.ViewModels.Abstracts;
using TheMall.MPS.ViewModels.Master;

namespace TheMall.MPS.ViewModels.Proposal
{
    public class EstimateTotalSaleViewModel : BaseMpsLineViewModel
    {

        public ExpenseTopicViewModel ExpenseTopic { get; set; }
        public decimal BudgetPlan { get; set; }
        public decimal Actual { get; set; }
        public decimal PreviousActual { get; set; }
        public string Remark { get; set; }
        public IEnumerable<EstimateTotalSaleTemplateViewModel> EstimateTotalSaleTemplate { get; set; }


      
    }
}
