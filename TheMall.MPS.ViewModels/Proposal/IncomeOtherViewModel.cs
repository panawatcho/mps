﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.ViewModels.Abstracts;

namespace TheMall.MPS.ViewModels.Proposal
{
    public class IncomeOtherViewModel : BaseLineViewModel
    {
        public string SponcorName { get; set; }
     
        public string Description { get; set; }
        public decimal Budget { get; set; }
        public DateTime DueDate { get; set; }
      
        public string ContactName { get; set; }
        
        public string ContactTel { get; set; }
      
        public string ContactEmail { get; set; }
      
        public string ContactDepartment { get; set; }
        public string Remark { get; set; }
        public decimal? Actual { get; set; }
        public  bool PostActual { get; set; }
        public IEnumerable<AccountTransViewModel> AccountTrans { get; set; }
        public int? ProposalLineId { get; set; }
        public float RefLineNo { get; set; }
    }
}
