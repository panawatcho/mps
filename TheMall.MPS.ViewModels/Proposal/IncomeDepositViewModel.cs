﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.ViewModels.Abstracts;

namespace TheMall.MPS.ViewModels.Proposal
{
    public class IncomeDepositViewModel : BaseLineViewModel
    {
        public DepositNumberViewModel DocumentNumber { get; set; }
        public decimal Budget { get; set; }
        public string Remark { get; set; }
        public string Status { get; set; }
        public int? ProcInstId { get; set; }
        public bool ActualCharge { get; set; }
        public int? RefMemoId { get; set; }
        public string DocNumber { get; set; }
        public string Title { get; set; }
      
        public List<ProposalApprovalViewModel> Approver { get; set; }
    }
}
