﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.ViewModels.Abstracts;

namespace TheMall.MPS.ViewModels.Proposal
{
    public class IncomeTotalSaleTemplateViewModel 
    {
        public int Id { get; set; }
        public string BranchCode { get; set; }
        public int IncomeTotalSaleId { get; set; }
        public decimal PercentOfDepartment { get; set; }
        public decimal? Actual { get; set; }
        public decimal? ActualAmount { get; set; }
       
   
    }
}
