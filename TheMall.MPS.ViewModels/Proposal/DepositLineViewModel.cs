﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.ViewModels.Abstracts;
using TheMall.MPS.ViewModels.Master;

namespace TheMall.MPS.ViewModels.Proposal
{
    public class DepositLineViewModel : BaseLineViewModel
    {
        public string PaymentNo { get; set; }
        public DateTime? DatePeriod { get; set; }
        public decimal Amount { get; set; }
        public string ProcessStatus { get; set; }
        public string Remark { get; set; }
        public string InvoiceNo { get; set; }
        public decimal? Actual { get; set; }
        public IEnumerable<DepositAccountTransViewModel> DepositAccountTrans { get; set; }
    }
}
