﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.ViewModels.Abstracts;

namespace TheMall.MPS.ViewModels.Proposal
{
    public class ProposalPercentSharedViewModel 
    {
        public int lineId { get; set; }
        public string BranchCode { get; set; }
        public decimal TR_Amount { get; set; }
        public decimal TR_Percent { get; set; }
        public decimal RE_Amount { get; set; }
        public decimal RE_Percent { get; set; }
        public decimal? Actual { get; set; }
        public decimal? ActualAmount { get; set; }
        public bool Selected { get; set; }
    }
}
