﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.ViewModels.Abstracts;
using TheMall.MPS.ViewModels.Master;
using TheMall.MPS.ViewModels.ProposalLog;

namespace TheMall.MPS.ViewModels.Proposal
{
    public class ProposalViewModel : BaseMpsWorkflowViewModel
    {
        public TypeProposalViewModel ProposalType { get; set; }
        public string Place { get; set; }

        public List<PlaceViewModel> ListPlace { get; set; }
        public string Source { get; set; }
        public DocumentNumberViewModel DepositNumber { get; set; }
        public string Other { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public decimal Budget { get; set; }
        public string CustomersTargetGroup { get; set; }
        public bool CheckBudget { get; set; }
        public bool TransferToNextYear { get; set; }
        public string Objective { get; set; }
        public bool  TR { get; set; }
        public bool RE { get; set; }
        public bool CloseFlag { get; set; }
        public DateTime? ProjectStartDate { get; set; }
        public DateTime? ProjectEndDate { get; set; }
     
        public List<SharedBranchViewModel> DepartmentException { get; set; }
        public IEnumerable<SharedBranchViewModel> SharedBranchTemplate { get; set; }
        public IEnumerable<ProposalLineSharedTemplateViewModel> SharedBranchTemplateLines { get; set; }
        public IEnumerable<ProposalLineViewModel> ProposalLine { get; set; }
        public IEnumerable<DepositLineViewModel> DepositLines { get; set; }
        public IEnumerable<IncomeDepositViewModel> IncomeDeposit { get; set; }
        public IEnumerable<IncomeOtherViewModel> IncomeOther { get; set; }
       // public IEnumerable<AccountTransViewModel> AccountTrans { get; set; }
        public IEnumerable<IncomeTotalSaleViewModel> IncomeTotalSale { get; set; }
        public IEnumerable<ProposalApprovalViewModel> ProposalApproval { get; set; }
        public IEnumerable<EstimateTotalSaleViewModel> EstimateTotalSale { get; set; }

        public IEnumerable<UnitCodeForEmployeeViewModel> UnitCodeForEmployee { get; set; }
        public bool CheckCreateDocStatus { get; set; }
        public decimal TotalBudget { get; set; }
        public decimal? TotalIncomeDeposit { get; set; }
        public decimal? TotalIncomeOther { get; set; }
        public decimal? TotalMarketingExpense { get; set; }
        public decimal? TotalSaleTarget { get; set; }
        public decimal? TotalSaleTargetLastYear { get; set; }
        public decimal? TotalAdvertising { get; set; }
        public decimal? TotalPromotion { get; set; }
        public decimal? TotalOtherExpense { get; set; }
        public decimal? PecentAdvertising { get; set; }
        public decimal? PecentPromotion { get; set; }
        public decimal? PecentMarketingExpense { get; set; }
        public decimal? PercentOtherExpense { get; set; }
        public decimal? GrandTotal { get; set; }
        public decimal? PercentGrandTotal { get; set; }
     
        public IEnumerable<LogProposalTrackChangesViewModel> LogProposalTrackChanges { get; set; }
        public List<ProposalApprovalViewModel> Approver { get; set; }
        public string AllocationCode { get; set; }

        public ThemeViewModel Theme { get; set; }
        public string ThemeNewName { get; set; }
    }
}
