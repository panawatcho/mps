﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheMall.MPS.ViewModels.Proposal
{
    public class AccountTransViewModel
    {
        public int Id { get; set; }
        public decimal? Actual { get; set; }
        public DateTime? ActualDate { get; set; }
        public string Remark { get; set; }
        public int IncomeOtherId { get; set; }
        public string Source { get; set; }
        public int LineNo { get; set; }
        public bool Deleted { get; set; }
        public bool FlagMemoInvoice { get; set; }

    }
}
