﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.ViewModels.Abstracts;

namespace TheMall.MPS.ViewModels.Proposal
{
    public class IncomeTotalSaleViewModel : BaseMpsLineViewModel
    {
        public string Description { get; set; }
        public decimal Budget { get; set; }
        public decimal? ActualAmount { get; set; }
        public string Remark { get; set; }
        public decimal? ActualLastYear { get; set; }
        public Dictionary<string, ProposalPercentSharedViewModel> PercentShared { get; set; }
        public IEnumerable<IncomeTotalSaleTemplateViewModel> IncomeTotalSaleTemplate { get; set; }
    }
}
