﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.ViewModels.Abstracts;

namespace TheMall.MPS.ViewModels.Proposal
{
    public class EstimateTotalSaleTemplateViewModel : BaseLineViewModel
    {
        public int Id { get; set; }
        public string BranchCode { get; set; }
        public int EstimateTotalSaleId { get; set; }
        public decimal PercentOfDepartment { get; set; }
        public decimal TR_Amount { get; set; }
        public decimal TR_Percent { get; set; }
        public decimal RE_Amount { get; set; }
        public decimal RE_Percent { get; set; }

    }
}
