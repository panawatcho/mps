﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheMall.MPS.ViewModels
{
    public class MailInformViewModel
    {
        public string Name { get; set; }// user or Group
        public string Email { get; set; }
        public string TypeMail { get; set; }// user or Group
    }
}
