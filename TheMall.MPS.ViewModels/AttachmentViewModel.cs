﻿namespace TheMall.MPS.ViewModels
{
    public class AttachmentViewModel
    {
        public int id { get; set; }
        public string documentNumber { get; set; }
        public string activity { get; set; }
        //public string extension { get; set; }
        //public string downloadlink { get; set; }
        public int? documentType { get; set; }
        public string serialNumber { get; set; }
        public string getcopy_url { get; set; }
        public int parentId { get; set; }

        //public string group { get; set; }
        public string name { get; set; }
        public string title { get; set; }
        public string type { get; set; }
        public long size { get; set; }
        public string progress { get; set; }
        public string url { get; set; }
        public string thumbnail_url { get; set; }
        public string delete_url { get; set; }
        public string delete_type { get; set; }
        public string error { get; set; }

    }
}
