﻿using System;
using System.Collections.Generic;
using TheMall.MPS.ViewModels.Interfaces;
using TheMall.MPS.ViewModels.Master;


namespace TheMall.MPS.ViewModels.Abstracts
{
    public abstract class BaseMpsWorkflowViewModel : BaseWorkflowViewModel , IMpsWorkflowViewModel
    {
        public UnitCodeViewModel UnitCode { get; set; }
        public EmployeeViewModel Requester { get; set; }
        public string AccoutingBranch { get; set; }
      //  public IEnumerable<EmployeeViewModel> Accounting { get; set; }
        public PlaceViewModel Accounting { get; set; }
        public IEnumerable<MailInformViewModel> InformEmail { get; set; }
        public IEnumerable<MailInformViewModel> CCEmail { get; set; }
        public DateTime? DueDatePropose { get; set; }
        public DateTime? DueDateAccept { get; set; }
        public DateTime? DueDateApprove { get; set; }
        public DateTime? DueDateFinalApprove { get; set; }
        public int? RePrintNo { get; set; }
        public IEnumerable<EmployeeViewModel> AccountingUsers { get; set; }//
    }
}
