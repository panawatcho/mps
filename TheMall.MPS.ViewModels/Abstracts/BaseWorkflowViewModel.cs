﻿using System;
using System.Collections.Generic;
using TheMall.MPS.ViewModels.Interfaces;


namespace TheMall.MPS.ViewModels.Abstracts
{
    public abstract class BaseWorkflowViewModel : IWorkflowViewModel
    {
        public int Id { get; set; }
        public int ParentId { get; set; }
        public float LineNo { get; set; }
        public bool Deleted { get; set; }
        public int StatusFlag { get; set; }
        public string[] Actions { get; set; }
        public string SN { get; set; }
        public string Comment { get; set; }
        public IEnumerable<AttachmentViewModel> OtherFiles { get; set; }
        public Dictionary<string, IEnumerable<AttachmentViewModel>> CommentFiles { get; set; }
        public Dictionary<string, IEnumerable<AttachmentViewModel>> DocTypeFiles { get; set; }
        public IEnumerable<CommentViewModel> Comments { get; set; }
        public string Title { get; set; }
        public string DocumentNumber { get; set; }

        public int? Revision { get; set; }
        public DateTime? SubmittedDate { get; set; }
        public string Status { get; set; }
        public string DocumentStatus { get; set; }
   
        public int? ProcInstId { get; set; }
        public int? ExpectedDuration { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? DocumentDate { get; set; }
        public DateTime? CompletedDate { get; set; }
        public DateTime? CancelledDate { get; set; }
        public string ViewFlowUrl { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedByName { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

       // public EmployeeViewModel Requester { get; set; }
       
    }
}
