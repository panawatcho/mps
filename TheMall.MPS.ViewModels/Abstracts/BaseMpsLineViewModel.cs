﻿using System;
using System.Collections.Generic;
using TheMall.MPS.ViewModels.Interfaces;
using TheMall.MPS.ViewModels.Master;


namespace TheMall.MPS.ViewModels.Abstracts
{
    public abstract class BaseMpsLineViewModel : BaseLineViewModel, IMpsLineViewModel
    {
        public UnitCodeViewModel UnitCode { get; set; }
       
    }
}
