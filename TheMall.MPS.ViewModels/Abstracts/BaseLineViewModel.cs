﻿namespace TheMall.MPS.ViewModels.Abstracts
{
    public abstract class BaseLineViewModel : Interfaces.ILineViewModel
    {
        public int Id { get; set; }
        public int ParentId { get; set; }
        public float LineNo { get; set; }
        public bool Deleted { get; set; }
        public int StatusFlag { get; set; }
    }
}
