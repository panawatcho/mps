﻿using System.Collections.Generic;
using TheMall.MPS.ViewModels.Interfaces;

namespace TheMall.MPS.ViewModels.Workspace
{
    public class AccWorklistItemViewModel : IGridDataSourceResult<WorklistItemViewModel>
    {
        public int TotalCount { get; set; }
        public IEnumerable<WorklistItemViewModel> Data { get; set; }
    }
}
