﻿using System.Collections.Generic;
using TheMall.MPS.ViewModels.Interfaces;

namespace TheMall.MPS.ViewModels.Workspace
{
    public class MyWorklistItemViewModel : IGridDataSourceResult<MyWorklistItem>
    {
        public int TotalCount { get; set; }
        public IEnumerable<MyWorklistItem> Data { get; set; }
    }
}
