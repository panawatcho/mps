﻿using System;
using TheMall.MPS.ViewModels.Proposal;

namespace TheMall.MPS.ViewModels.Workspace
{
    public class MyWorklistItem
    {
        public string SN { get; set; }
        public int ProcessInstanceId { get; set; }
        public string Data { get; set; }
        public string ViewFlow { get; set; }
        public string Folio { get; set; }
        public string ProcessName { get; set; }
        public string ActivityName { get; set; }
        public string ActivityDescription { get; set; }
        public DateTime StartDate { get; set; }
        public string Status { get; set; }
        public string Title { get; set; }
        public string Module { get; set; }
        public string[] Actions { get; set; }

        public double ExpectedDays { get; set; }
        public string DueStatus { get; set; }
        public DateTime DueDate { get; set; }
        public int? ProposalId { get; set; }
        public ProposalViewModel ProposalViewModel { get; set; }
    }
}