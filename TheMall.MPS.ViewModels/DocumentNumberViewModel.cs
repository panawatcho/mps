﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.ViewModels.Proposal;

namespace TheMall.MPS.ViewModels
{
    public class DocumentNumberViewModel
    {
        public int Id { get; set; }
        public string DocumentNumber { get; set; }
        public string Title { get; set; }
        public string Type { get; set; }
        public decimal? Balance { get; set; }
        public List<ProposalApprovalViewModel> Approver { get; set; }
       
    }
}
