﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.Models.View;

namespace TheMall.MPS.ViewModels
{
    public class ProposalTrackingViewModel
    {
        public decimal? Remaining { get; set; }
        public decimal? TotalBudget { get; set; }
        public List<MPS_VW_ProposalTracking> ProposalTracking { get; set; }
    }
}
