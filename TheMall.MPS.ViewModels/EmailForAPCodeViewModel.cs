﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheMall.MPS.ViewModels
{
    public class EmailForAPCodeViewModel
    {
        public string ExpenseTopicCode { get; set; }
        public string ExpenseTopicName { get; set; }
        public string APCode { get; set; }
        public string APName { get; set; }
        public string UnitCode { get; set; }
        public string UnitName { get; set; }
        public decimal BudgetPlan { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }

    }
}
