﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.ViewModels.Master;

namespace TheMall.MPS.ViewModels
{
    public class RequesterViewModel
    {
        public string EmpId { get; set; }
        public string EmpStatus { get; set; }
        public string ThFName { get; set; }
        public string ThLName { get; set; }
        public string EnFName { get; set; }
        public string EnLName { get; set; }
        public string Username { get; set; }
        public string PositionCode { get; set; }
        public string PositionName { get; set; }
        public string FullName
        {
            get { return (string.Format("{0} {1} ", ThFName, ThLName)); }
        }

        public IEnumerable<UnitCodeViewModel> UnitCode { get; set; }

    }
}
