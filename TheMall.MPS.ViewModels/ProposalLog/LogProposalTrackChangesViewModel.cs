﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheMall.MPS.ViewModels.ProposalLog
{
    public class LogProposalTrackChangesViewModel
    {
        public long Id { get; set; }
        public int ProposalId { get; set; }
        public int ProposalLogId { get; set; }
        public string RefTableName { get; set; }
        public string RefTableDesc { get; set; }
        public int? RefTableId { get; set; }
        public int? RefLogId { get; set; }
        public string FieldName { get; set; }
        public string FieldDescription { get; set; }
        public string State { get; set; }
        public string OldValue { get; set; }
        public string NewValue { get; set; }
        public DateTime? CreatedDate { get; set; }

    }
}
