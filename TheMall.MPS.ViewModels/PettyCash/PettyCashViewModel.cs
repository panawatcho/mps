﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.Models.Master;
using TheMall.MPS.ViewModels.Abstracts;
using TheMall.MPS.ViewModels.Master;
using TheMall.MPS.ViewModels.Proposal;

namespace TheMall.MPS.ViewModels.PettyCash
{
    public class PettyCashViewModel : BaseMpsWorkflowViewModel
    {
        public ExpenseTopicViewModel ExpenseTopic { get; set; }
        public APCodeByProposalLineViewModel APCode { get; set; }
        public DateTime PettyCashDate { get; set; }
        public decimal BudgetDetail { get; set; }
        public decimal BudgetAPCode { get; set; }
        public PlaceViewModel Branch { get; set; }
        public string Description { get; set; }
    
        public ProposalInfoViewModel ProposalRef { get; set; }
        public IEnumerable<PettyCashLineViewModel> PettyCashLines { get; set; }
        public IEnumerable<PettyCashApprovalViewModel> PettyCashApproval { get; set; }
        public int ProposalLineId { get; set; }
        public decimal BudgetTrans { get; set; }
        public bool GoodsIssue { get; set; }

        public CompanyViewModel Company { get; set; }

    }
}
