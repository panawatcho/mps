﻿using System;
using System.Collections.Generic;

using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TheMall.MPS.ViewModels.Abstracts;

namespace TheMall.MPS.ViewModels.Proposal
{
    public class PettyCashApprovalViewModel : BaseMpsLineViewModel
    {
        public EmployeeViewModel Employee { get; set; }
        public int ApproverSequence { get; set; }
       
        public string ApproverLevel { get; set; }
        
        public string ApproverUserName { get; set; }
      
        public string ApproverFullName { get; set; }
        public string ApproverEmail { get; set; }
        
        public string Position { get; set; }
      
        public string Department { get; set; }
        public DateTime? DueDate { get; set; }
        public bool LockEditable { get; set; }
        public bool ApproveStatus { get; set; }
    }
}
