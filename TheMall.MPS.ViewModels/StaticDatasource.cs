﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheMall.MPS.ViewModels
{
    public class StaticDatasource
    {
        public StaticDatasource() { }
        public StaticDatasource(string value)
        {
            this.text = value;
            this.value = value;
        }
        public string text { get; set; }
        public string value { get; set; }
    }
}
