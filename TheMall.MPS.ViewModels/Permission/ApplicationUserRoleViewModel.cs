﻿namespace TheMall.MPS.ViewModels.Permission
{
    public class ApplicationUserRoleViewModel
    {
        public string RoleId { get; set; }
        public string UserId { get; set; }
        public string Name { get; set; }
        public bool Inactive { get; set; }
    }
}
