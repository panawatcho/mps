﻿using System.Reflection.Emit;


namespace TheMall.MPS.ViewModels.Permission
{
    public class RoleMemberViewModel
    {
        public int Id { get; set; }
        public string Process { get; set; }

     
        public EmployeeViewModel Employee { get; set; }

        public string RoleID { get; set; }

        public bool Deleted { get; set; }
    }
}
