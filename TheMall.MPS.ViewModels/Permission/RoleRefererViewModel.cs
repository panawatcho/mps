﻿namespace TheMall.MPS.ViewModels.Permission
{
    public class RoleRefererViewModel
    {
        public string RefererId { get; set; }
        public string RoleId { get; set; }
        public string Description { get; set; }
        public bool Enable { get; set; }
    }
}
