﻿using System.Collections.Generic;

namespace TheMall.MPS.ViewModels.Permission
{
    public class RoleViewModel
    {
        public string Id { get; set; }    
        public string Name { get; set; }
        public IEnumerable<RoleMemberViewModel> RoleMembers { get; set; } 
    }
}
