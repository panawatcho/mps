﻿using System.Collections.Generic;


namespace TheMall.MPS.ViewModels.Permission
{
    public class ApplicationRoleViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public bool Inactive { get; set; }
        public IEnumerable<ApplicationUserRoleViewModel> UserRole { get; set; }
    }
}
