﻿namespace TheMall.MPS.ViewModels.Permission
{
    public class RoleTemplateViewModel
    {
        public int Id { get; set; }
        public string RoleName { get; set; }
        public string Criteria { get; set; }
        public bool IsActive { get; set; }
    }
}
