﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.Models.Master;
using TheMall.MPS.ViewModels.Master;
using TheMall.MPS.ViewModels.Proposal;

namespace TheMall.MPS.ViewModels
{
    public class ProposalInfoViewModel
    {
        public int Id { get; set; }
        public string DocumentNumber { get; set; }
        public string Title { get; set; }
        public TypeProposalViewModel ProposalType { get; set; }
        public string Place { get; set; }
        public List<PlaceViewModel> ListPlace { get; set; }
        public string Source { get; set; }
        public DocumentNumberViewModel DepositNumber { get; set; }
        public string Other { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public decimal Budget { get; set; }
        public DateTime? ProjectStartDate { get; set; }
        public DateTime? ProjectEndDate { get; set; }
        public string AccountingUsername { get; set; }
        public int StatusFlag { get; set; }
        public List<ExpenseTopicViewModel> ExpenseTopic { get; set; }
        public List<APCodeByProposalLineViewModel> APCode { get; set; }
        public IEnumerable<ProposalLineViewModel> ProposalLine { get; set; }
        public IEnumerable<IncomeDepositViewModel> IncomeDepositLine { get; set; }
        public UnitCodeViewModel ProposalUnitCode { get; set; }
        public bool CloseFlag { get; set; }

        public List<CompanyViewModel> Company { get; set; }
    }
}
