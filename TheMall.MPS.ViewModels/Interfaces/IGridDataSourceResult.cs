﻿using System.Collections.Generic;

namespace TheMall.MPS.ViewModels.Interfaces
{
    public interface IGridDataSourceResult
    {
        int TotalCount { get; set; }
    }
    public interface IGridDataSourceResult<T> : IGridDataSourceResult
    {
        IEnumerable<T> Data { get; set; }
    }
}
