﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.ViewModels;
using TheMall.MPS.ViewModels.Interfaces;
using TheMall.MPS.ViewModels.Master;

namespace TheMall.MPS.ViewModels.Interfaces
{
    public interface IMpsWorkflowViewModel : IWorkflowViewModel
    {
        EmployeeViewModel Requester { get; set; }
        UnitCodeViewModel UnitCode { get; set; }
        string AccoutingBranch { get; set; }
        //IEnumerable<EmployeeViewModel> Accounting { get; set; }
        PlaceViewModel Accounting { get; set; }
        IEnumerable<MailInformViewModel> InformEmail { get; set; }
        IEnumerable<MailInformViewModel> CCEmail { get; set; }

        DateTime? DueDatePropose { get; set; }
        DateTime? DueDateAccept { get; set; }
        DateTime? DueDateApprove { get; set; }
        DateTime? DueDateFinalApprove { get; set; }
        int? RePrintNo { get; set; }
        IEnumerable<EmployeeViewModel> AccountingUsers { get; set; }
    }
}
