﻿namespace TheMall.MPS.ViewModels.Interfaces
{
    public interface ILineViewModel : IViewModel
    {
     
        int ParentId { get; set; }
        float LineNo { get; set; }
        bool Deleted { get; set; }
        int StatusFlag { get; set; }
    }
}
