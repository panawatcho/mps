﻿using System;
using TheMall.MPS.ViewModels.Interfaces;

namespace TheMall.MPS.ViewModels.Interfaces
{
    public interface IWorkflowViewModel : IViewModel
    {
        string[] Actions { get; set; }
        string SN { get; set; }
        string Comment { get; set; }
       
        string Title { get; set; }
        string DocumentNumber { get; set; }
        int? Revision { get; set; }
        int? ProcInstId { get; set; }
        DateTime? SubmittedDate { get; set; }
        string Status { get; set; }
        int StatusFlag { get; set; }
        DateTime? CreatedDate { get; set; }
        DateTime? DocumentDate { get; set; }
        DateTime? CompletedDate { get; set; }
        DateTime? CancelledDate { get; set; }
        string ViewFlowUrl { get; set; }
      
        string CreatedBy { get; set; }
        string CreatedByName { get; set; }
        bool Deleted { get; set; }
        string DocumentStatus { get; set; }
       //ยังขาด อีกหลายฟิล
    }
}
