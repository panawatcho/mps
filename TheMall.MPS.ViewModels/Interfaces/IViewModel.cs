﻿namespace TheMall.MPS.ViewModels.Interfaces
{
    public interface IViewModel
    {
        int Id { get; set; }
    }
}
