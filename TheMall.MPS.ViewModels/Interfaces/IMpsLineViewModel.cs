﻿using TheMall.MPS.ViewModels.Master;

namespace TheMall.MPS.ViewModels.Interfaces
{
    public interface IMpsLineViewModel : ILineViewModel
    {

        UnitCodeViewModel UnitCode { get; set; }
      
    }
}
