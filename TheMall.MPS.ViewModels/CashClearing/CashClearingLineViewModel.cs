﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.ViewModels.Abstracts;

namespace TheMall.MPS.ViewModels.CashClearing
{
   public class CashClearingLineViewModel : BaseLineViewModel
    {

        public string Description { get; set; }
        public decimal Unit { get; set; }
        public decimal Amount { get; set; }
        public decimal? Actual { get; set; }
        public decimal? Vat { get; set; }
        public decimal? Tax { get; set; }
        public string Remark { get; set; }
        public decimal? NetAmount { get; set; }
        public virtual CashClearingViewModel CashClearingTable { get; set; }
        public BaseMpsWorkflowViewModel Parent
        {
            get { return CashClearingTable; }
        }
        public decimal? VatActual { get; set; }
        public decimal? TaxActual { get; set; }
        public decimal? NetActual { get; set; }
        public int? UnitActual { get; set; }
        public decimal? NetActualNoVatTax { get; set; }
        public string PayeeName { get; set; }
    }
}
