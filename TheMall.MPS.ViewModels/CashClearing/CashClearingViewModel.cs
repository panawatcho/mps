﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.Models.Master;
using TheMall.MPS.ViewModels.Abstracts;
using TheMall.MPS.ViewModels.CashAdvance;
using TheMall.MPS.ViewModels.Master;

namespace TheMall.MPS.ViewModels.CashClearing
{
     public class CashClearingViewModel : BaseMpsWorkflowViewModel
    {
        public ExpenseTopicViewModel ExpenseTopic { get; set; }
        public APCodeByProposalLineViewModel APCode { get; set; }
        public string RefAdvanceNumber { get; set; }
        public DateTime ClearingDate { get; set; }
        public PlaceViewModel Branch { get; set; }
        public decimal Budget { get; set; }
        public decimal BudgetSum { get; set; }
        public string Description { get; set; }
     
        public int CashAdvanceID { get; set; }
        public virtual CashAdvanceViewModel CashAdvanceViewModel { get; set; }
        public ProposalInfoViewModel ProposalRef { get; set; }
        public virtual IEnumerable<CashClearingLineViewModel> CashClearingLines { get; set; }
        public virtual IEnumerable<CashClearingApprovalViewModel> CashClearingApproval { get; set; }
        public EmployeeViewModel RequesterFor { get; set; }

        public decimal Actual { get; set; }
        public decimal? CashAdvanceLinesNetAmount { get; set; }
        //public virtual IEnumerable<MPS_CashClearingComment> Comments { get; set; }
        //public virtual IEnumerable<MPS_CashClearingAttachment> Attachments { get; set; }

        public CompanyViewModel Company { get; set; }
    }
}
