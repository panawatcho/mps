﻿public static class SizeSuffixExtension
{
    static readonly string[] SizeSuffixes = { "bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB" };
    public static string SizeSuffix(this long value)
    {
        if (value > 0)
        {
            int mag = (int)System.Math.Log(value, 1024);
            decimal adjustedSize = (decimal)value / (1L << (mag * 10));

            return string.Format("{0:n1} {1}", adjustedSize, SizeSuffixes[mag]);
        }
        else
        {
            return "0 bytes";
        }
    }
    public static string SizeSuffix(this long? value)
    {
        if (value == null)
            return string.Empty;
        return SizeSuffix(value.Value);
    }
}