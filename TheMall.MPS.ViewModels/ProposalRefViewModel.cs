﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.ViewModels.Master;
using TheMall.MPS.ViewModels.Proposal;

namespace TheMall.MPS.ViewModels
{
    public class ProposalRefViewModel
    {
        public int Id { get; set; }
        public string DocumentNumber { get; set; }
        public string Title { get; set; }
        public TypeProposalViewModel ProposalType { get; set; }
        public string Place { get; set; }
        public List<PlaceViewModel> ListPlace { get; set; }
        public string Source { get; set; }
        public string Other { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public decimal Budget { get; set; }
        public DateTime? ProjectStartDate { get; set; }
        public DateTime? ProjectEndDate { get; set; }
        public string AccountingUsername { get; set; }
        public int StatusFlag { get; set; }
        public bool CloseFlag { get; set; }
        public UnitCodeViewModel ProposalUnitCode { get; set; }
        public List<IncomeOtherViewModel> IncomeOther { get; set; }
        public List<DepositLineViewModel> DepositLine { get; set; }
        public int ProcInstId { get; set; }
        public CommentViewModel ProposalComment { get; set; }
    }
}
