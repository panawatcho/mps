﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrossingSoft.Framework.Infrastructure;
using TheMall.MPS.Models;
using TheMall.MPS.ViewModels.Interfaces;

namespace TheMall.MPS.ViewModels.AspNet
{
    public class AspNetUserRolesViewModel : IMasterViewModel
    {
        public AspNetRolesViewModel Role { get; set; }
        public EmployeeViewModel User { get; set; }
        public bool Inactive { get; set; }
        public ObjectState ObjectState { get; set; }
        public string RoleId { get; set; }
        public string Username { get; set; }
    }

}
