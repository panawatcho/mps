﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrossingSoft.Framework.Infrastructure;
using TheMall.MPS.ViewModels.Interfaces;

namespace TheMall.MPS.ViewModels.AspNet
{
    public class AspNetRolesViewModel : IMasterViewModel
    {
        public bool Inactive { get; set; }
        public string Name { get; set; }
        public string Id { get; set; }
        public ObjectState ObjectState { get; set; }

    }
}
