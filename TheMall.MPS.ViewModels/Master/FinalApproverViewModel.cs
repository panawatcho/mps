﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.ViewModels.Interfaces;

namespace TheMall.MPS.ViewModels.Master
{
    public class FinalApproverViewModel : IMasterViewModel
    {
        public string ProcessCode { get; set; }
        public string Process { get; set; }
        public string EmpId { get; set; }
        public string Username { get; set; }
        public string UnitCode { get; set; }
      
        public Decimal? Amount { get; set; }
        public bool InActive { get; set; }
        //// 12/12/60
        public UnitCodeViewModel UnitCodeViewModel { get; set; }

        public EmployeeViewModel EmployeeViewModel { get; set; }

        public ProcessApproveViewModel ProcessApproveViewModel { get; set; }
        ////
    }
}
