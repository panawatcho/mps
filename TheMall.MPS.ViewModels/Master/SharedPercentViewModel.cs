﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.ViewModels.Interfaces;

namespace TheMall.MPS.ViewModels.Master
{
    public class SharedPercentViewModel : IMasterViewModel
    {
        public int Id { get; set; }
        public PlaceViewModel Place { get; set; }
        public decimal Percent { get; set; }
        public bool TR { get; set; }
        public bool RE { get; set; }
        public string AllocateType { get; set; }
        public float LineNo { get; set; }
        public bool Deleted { get; set; }
      
    }
}
