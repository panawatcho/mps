﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.ViewModels.Interfaces;

namespace TheMall.MPS.ViewModels.Master
{
    public class ExpenseTopicViewModel : IMasterViewModel
    {
        public string ExpenseTopicCode { get; set; }
        public string ExpenseTopicName { get; set; }
        public string Description { get; set; }
        public string TypeAP { get; set; }
        public bool InActive { get; set; }
        public decimal? Order { get; set; }
        public IEnumerable<APCodeViewModel> APCode { get; set; }
    }
}
