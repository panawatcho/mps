﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheMall.MPS.ViewModels.Master
{
    public class AccountingViewModel
    {
        public string BranchCode { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string EmpId { get; set; }
        public bool InActive { get; set; }
        public  EmployeeViewModel Employee { get; set; }
        public  SharedBranchViewModel SharedBranch { get; set; }
    }
}
