﻿using TheMall.MPS.ViewModels.Interfaces;

namespace TheMall.MPS.ViewModels.Master
{
    public class ThemeViewModel : IMasterViewModel
    {
        public string ThemesCode { get; set; }
        public string ThemesName { get; set; }
        public bool InActive { get; set; }
        public decimal? Order { get; set; }
    }
}
