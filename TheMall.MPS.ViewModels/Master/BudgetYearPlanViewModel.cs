﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.ViewModels.Interfaces;

namespace TheMall.MPS.ViewModels.Master
{
    public class BudgetYearPlanViewModel : IMasterViewModel
    {
      //  public string BudgetYearID { get; set; }
        public string TeamName { get; set; }
        public string Type { get; set; }
        public decimal Budget { get; set; }
        public string Year { get; set; }

        public string UnitCode { get; set; }
        public string UnitName { get; set; }
        public string TypeYearPlan { get; set; }
        public DateTime YearTime { get; set; }
        public decimal? Order { get; set; }
        public UnitCodeViewModel UnitCodeViewModel { get; set; }
    }
}
