﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.Models.Abstracts;
using TheMall.MPS.ViewModels.Interfaces;

namespace TheMall.MPS.ViewModels.Master
{
    public class MailGroupViewModel : IMasterViewModel
    {
        public string GroupCode { get; set; }
        public string Email { get; set; }
        public bool InActive { get; set; }
    }
}
