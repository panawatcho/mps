﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.ViewModels.Interfaces;

namespace TheMall.MPS.ViewModels.Master
{
    public class AllocationBasisViewModel : IMasterViewModel
    {
        public int Id { get; set; }
        public string AllocCode { get; set; }
        public string Description { get; set; }
        public bool InActive { get; set; }
        //Branch
        public bool MRT { get; set; }
        public bool B5 { get; set; }
        public bool B7 { get; set; }
        public bool M02 { get; set; }
        public bool M03 { get; set; }
        public bool M05 { get; set; }
        public bool M06 { get; set; }
        public bool M07 { get; set; }
        public bool M08 { get; set; }
        public bool M09 { get; set; }
        public bool M10 { get; set; }
        public bool M11 { get; set; }
        public bool M12 { get; set; }
        public bool M13 { get; set; }
        public bool M14 { get; set; }
        public bool M15 { get; set; }
        public bool M16 { get; set; }
        public bool M17 { get; set; }
        public bool V1 { get; set; }
        public bool V2 { get; set; }
        public bool V3 { get; set; }
        public bool V5 { get; set; }
        public bool V9 { get; set; }
        public bool TR { get; set; }
        public bool RE { get; set; }
    }
}
