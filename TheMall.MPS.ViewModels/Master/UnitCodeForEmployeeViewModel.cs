﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.ViewModels.Interfaces;

namespace TheMall.MPS.ViewModels.Master
{
    public class UnitCodeForEmployeeViewModel : IMasterViewModel
    {
        public int Id { get; set; }
        public string EmpId { get; set; }
       //public string Username { get; set; }
        public EmployeeViewModel Employee { get; set; }
        //public string UnitCode { get; set; }
        public UnitCodeViewModel UnitCode { get; set; }
        //public string UnitName { get; set; }
        public bool InActive { get; set; }
        public decimal? Order { get; set; }
    }
}
