﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.ViewModels.Interfaces;

namespace TheMall.MPS.ViewModels.Master
{
    public class ProcessApproveViewModel : IMasterViewModel
    {
        public string ProcessCode { get; set; }
        public string ProcessName { get; set; }
        public string ProcessType { get; set; }
        public bool InActive { get; set; }
        public TypeMemoViewModel TypeMemo { get; set; }
    }
}
