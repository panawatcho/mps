﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.Models;
using TheMall.MPS.Models.Master;
using TheMall.MPS.ViewModels.Interfaces;

namespace TheMall.MPS.ViewModels.Master
{
    public class TotalSaleLastYearViewModel : IMasterViewModel
    {
        public string BranchCode { get; set; }
        public decimal? TotalSale { get; set; }
        public string Year { get; set; }
        public decimal? PercentTR { get; set; }
        public decimal? PercentRE { get; set; }
        public decimal? AreaTR { get; set; }
        public decimal? AreaRE { get; set; }
        public SharedBranchViewModel SharedBranch { get; set; }
        public DateTime YearTime { get; set; }
    }
}
