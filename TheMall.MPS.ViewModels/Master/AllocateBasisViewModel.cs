﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.ViewModels.Interfaces;

namespace TheMall.MPS.ViewModels.Master
{
    public class AllocateBasisViewModel : IMasterViewModel
    {

        public string AllocateType { get; set; }
        public string Description { get; set; }
        public IEnumerable<SharedPercentViewModel> SharedPercents { get; set; }
        
    }
}
