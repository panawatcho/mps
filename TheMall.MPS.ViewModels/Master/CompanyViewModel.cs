﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.ViewModels.Interfaces;

namespace TheMall.MPS.ViewModels.Master
{
    public class CompanyViewModel : IMasterViewModel
    {
        public string VendorCode { get; set; }
        public string CompanyCode { get; set; }
        public string CompanyName { get; set; }
        public bool InActive { get; set; }
        public string Description { get; set; }
        public decimal? Order { get; set; }
        public string Address { get; set; }
        public string TelephoneNumber { get; set; }
        public string BranchCode { get; set; }
        //public SharedBranchViewModel SharedBranch { get; set; }
    }
}
