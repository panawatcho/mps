﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.ViewModels.Interfaces;

namespace TheMall.MPS.ViewModels.Master
{
    public class SharedBranchViewModel : IMasterViewModel
    {
        public string BranchCode { get; set; }
        public string BranchName { get; set; }
        public string BranchAddress { get; set; }
        public bool Selected { get; set; }
        public decimal? PercentTR { get; set; }
        public decimal? PercentRE { get; set; }
        public decimal? TotalSale { get; set; }
        public decimal? AreaTR { get; set; }
        public decimal? AreaRE { get; set; }
        public decimal? Order { get; set; }
        public bool InActive { get; set; }
    }
}
