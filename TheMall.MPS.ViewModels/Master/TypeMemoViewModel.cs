﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.ViewModels.Interfaces;

namespace TheMall.MPS.ViewModels.Master
{
    public class TypeMemoViewModel : IMasterViewModel
    {
        public string TypeMemoCode { get; set; }
        public string TypeMemoName { get; set; }
        public decimal? Order { get; set; }
        public bool InActive { get; set; }
        public bool ActualCharge { get; set; }
        public UnitCodeViewModel UnitCode { get; set; }
    }
}
