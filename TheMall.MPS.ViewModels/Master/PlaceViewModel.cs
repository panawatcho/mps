﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.Models.Master;
using TheMall.MPS.ViewModels.Interfaces;

namespace TheMall.MPS.ViewModels.Master
{
    public class PlaceViewModel : IMasterViewModel
    {
        public string PlaceCode { get; set; }
        public string PlaceName { get; set; }
        public bool InActive { get; set; }
        public string Description { get; set; }
        public decimal? Order { get; set; }
    }
}
