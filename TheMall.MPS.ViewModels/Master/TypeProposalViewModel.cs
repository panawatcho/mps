﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.ViewModels.Interfaces;

namespace TheMall.MPS.ViewModels.Master
{
    public class TypeProposalViewModel : IMasterViewModel
    {
        public string TypeProposalCode { get; set; }
        public string TypeProposalName { get; set; }
        public string TypeYearPlan { get; set; }
        public decimal? Order { get; set; }
        public bool InActive { get; set; }
    }
}
