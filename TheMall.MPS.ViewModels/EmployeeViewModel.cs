﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.ViewModels.Interfaces;
using TheMall.MPS.ViewModels.Master;

namespace TheMall.MPS.ViewModels
{
    public class EmployeeViewModel : IMasterViewModel
    {
        public string EmpId { get; set; }
        public string EmpStatus { get; set; }
        public string ThFName { get; set; }
        public string ThLName { get; set; }
        public string EnFName { get; set; }
        public string EnLName { get; set; }
        public string DepartmentCode { get; set; }
        public string DepartmentName { get; set; }
        public string Email { get; set; }
        public string TelephoneNo { get; set; }
        public string DivisionCode { get; set; }
   
        public string DivisionName { get; set; }

        public string PositionCode { get; set; }
        public string PositionName { get; set; }
        public string CompanyCode { get; set; }
        public string CompanyName { get; set; }
        public string Username { get; set; }
        public string Authority { get; set; }
        public string FullName
        {
            get { return (string.Format("{0} {1} ", ThFName, ThLName)); }
        }

        public string FullNameEn
        {
            get { return (string.Format("{0} {1} ", EnFName, EnLName)); }
        }

        public IEnumerable<UnitCodeViewModel> UnitCode { get; set; }
    }
}
