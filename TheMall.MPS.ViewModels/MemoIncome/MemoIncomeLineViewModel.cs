﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.ViewModels.Abstracts;
using TheMall.MPS.ViewModels.Master;

namespace TheMall.MPS.ViewModels.MemoIncome
{
    public class MemoIncomeLineViewModel : BaseLineViewModel
    {
        public string Description { get; set; }
        public string Remark { get; set; }
        public decimal Unit { get; set; }
        public decimal Amount { get; set; }
        public decimal? Vat { get; set; }
        public decimal? Tax { get; set; }
        public decimal? NetAmount { get; set; }
        public decimal? NetAmountNoVatTax { get; set; }
    }

    
}
