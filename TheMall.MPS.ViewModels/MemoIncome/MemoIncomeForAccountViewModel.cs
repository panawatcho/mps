﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.Models.Master;
using TheMall.MPS.Models.Proposal;
using TheMall.MPS.ViewModels.Abstracts;
using TheMall.MPS.ViewModels.Interfaces;
using TheMall.MPS.ViewModels.Master;
using TheMall.MPS.ViewModels.Proposal;

namespace TheMall.MPS.ViewModels.MemoIncome
{
    public class MemoIncomeForAccountViewModel : BaseMpsWorkflowViewModel
    {
    
     public decimal TotalAmount { get; set; }
     public decimal BudgetDetail { get; set; }
     public string Category { get; set; }
     public int ProposalRefID { get; set; }

     public string ProposalRefDocumentNumber { get; set; }
     public string ProposalTypeCode { get; set; }
     public string ProposalTypeName { get; set; }

     public string Description { get; set; }
     public string InvoiceNo { get; set; }
     public string AccountingStatus { get; set; }
     public int? OtherIncomeRefId { get; set; }
     public decimal? Actual { get; set; }
    
     public string RequesterUsername { get; set; }
     
     public string AccountingUsername { get; set; }
     
    }
}
