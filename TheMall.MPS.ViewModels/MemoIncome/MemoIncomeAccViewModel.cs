﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.Models.Master;
using TheMall.MPS.Models.Proposal;
using TheMall.MPS.ViewModels.Abstracts;
using TheMall.MPS.ViewModels.Interfaces;
using TheMall.MPS.ViewModels.Master;
using TheMall.MPS.ViewModels.Proposal;

namespace TheMall.MPS.ViewModels.MemoIncome
{
    public class MemoIncomeAccViewModel
    {
        public int Id { get; set; }
        public int ProposalRefID { get; set; }
        public string ProposalRefDocumentNumber { get; set; }
        public string DocumentNumber { get; set; }
        public string Title { get; set; }
        public string Category { get; set; }
        public decimal TotalAmount { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int StatusFlag { get; set; }
        public string Status { get; set; }

    }
}
