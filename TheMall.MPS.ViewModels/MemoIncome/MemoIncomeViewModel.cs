﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.Models.Master;
using TheMall.MPS.Models.Proposal;
using TheMall.MPS.ViewModels.Abstracts;
using TheMall.MPS.ViewModels.Interfaces;
using TheMall.MPS.ViewModels.Master;
using TheMall.MPS.ViewModels.Proposal;

namespace TheMall.MPS.ViewModels.MemoIncome
{
    public class MemoIncomeViewModel : BaseMpsWorkflowViewModel
 {
        public decimal TotalAmount { get; set; }
        public decimal BudgetDetail { get; set; }
        public string Category { get; set; }
        public string Description { get; set; }
        public string InvoiceNo { get; set; }
        public string AccountingStatus { get; set; }
        public int? OtherIncomeRefId { get; set; }
        public int? DepositLineId { get; set; }
        public decimal? Actual { get; set; }

        public PlaceViewModel Branch { get; set; }
        public ProposalRefViewModel ProposalRef { get; set; }
        public TypeProposalViewModel TypeProposal { get; set; }
        public IncomeOtherViewModel IncomeOther { get; set; }
        public DepositLineViewModel DepositLine { get; set; }


        public IEnumerable<MemoIncomeLineViewModel> MemoIncomeLines { get; set; }
        public IEnumerable<MemoIncomeInvoiceViewModel> MemoIncomeInvoice { get; set; }
        public IEnumerable<MemoIncomeApprovalViewModel> MemoIncomeApproval { get; set; }

    }
}
