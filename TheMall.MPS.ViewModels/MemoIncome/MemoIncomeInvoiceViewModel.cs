﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.Models.Master;
using TheMall.MPS.Models.Proposal;
using TheMall.MPS.ViewModels.Abstracts;
using TheMall.MPS.ViewModels.Interfaces;
using TheMall.MPS.ViewModels.Master;
using TheMall.MPS.ViewModels.Proposal;

namespace TheMall.MPS.ViewModels.MemoIncome
{
    public class MemoIncomeInvoiceViewModel : BaseLineViewModel
    {
        public string InvoiceNo { get; set; }
        public decimal Actual { get; set; }
        public DateTime? InvoiceDate { get; set; }
        public string Remark { get; set; }
    }
}
