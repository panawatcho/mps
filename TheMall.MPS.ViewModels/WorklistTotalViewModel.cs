﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheMall.MPS.ViewModels
{
    public class WorklistTotalViewModel
    {
        public int Total { get; set; }
        public int Early { get; set; }
        public int Ondue { get; set; }
        public int Overdue { get; set; }
    }
}
