﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.ViewModels;
using TheMall.MPS.ViewModels.Abstracts;
using TheMall.MPS.ViewModels.Interfaces;
using TheMall.MPS.ViewModels.Master;

namespace TheMall.MPS.ViewModels.Receipt
{
    public class ReceiptLineViewModel : IMasterViewModel
    {
        public int Id { get; set; }
        public float LineNo { get; set; }
        public string Description { get; set; }
        public decimal Unit { get; set; }
        public decimal Amount { get; set; }
        public decimal? Vat { get; set; }
        public decimal? Tax { get; set; }
        public decimal? NetAmount { get; set; }
        public decimal? NetAmountNoVatTax { get; set; }
        public string Remark { get; set; }
        public int ReceiptTableId { get; set; }
        public bool Deleted { get; set; }
        public DateTime? DeletedDate { get; set; }
        public string DeletedBy { get; set; }
        public string DeletedByName { get; set; }
    }
}
