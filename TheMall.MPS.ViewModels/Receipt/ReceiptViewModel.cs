﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.ViewModels;
using TheMall.MPS.ViewModels.Interfaces;
using TheMall.MPS.ViewModels.Master;

namespace TheMall.MPS.ViewModels.Receipt // name
{
    // match field(view)
    public class ReceiptViewModel : IMasterViewModel
    {
        public int Id { get; set; }
        public EmployeeViewModel Requester { get; set; }
        public UnitCodeViewModel UnitCode { get; set; }
        public EmployeeViewModel Requesterfor { get; set; }
        public UnitCodeViewModel UnitCodeFor { get; set; }
        public int? RePrintNo { get; set; }
        public IEnumerable<ReceiptLineViewModel> ReceiptLines { get; set; }
        public decimal BudgetNoVatTax { get; set; }
        public decimal BudgetDetail { get; set; }
        public string DocumentNumber { get; set; }
        public string Status { get; set; }
        public int StatusFlag { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? CompletedDate { get; set; }
        public string CompletedByUserName { get; set; }
        public string CompletedByName { get; set; }
        public bool Deleted { get; set; }
        public DateTime? DeletedDate { get; set; }
        public string DeletedBy { get; set; }
        public string DeletedByName { get; set; }
    }
}
