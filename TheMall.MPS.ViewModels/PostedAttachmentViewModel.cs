﻿namespace TheMall.MPS.ViewModels
{
    public interface IPostedAttachmentViewModel
    {
        int? Id { get; set; }
        string Name { get; set; }
        string Title { get; set; }
        int? DocumentType { get; set; }
        string SN { get; set; }
        string DocumentNumber { get; set; }
        string Activity { get; set; }
    }

    public class PostedAttachmentViewModel : IPostedAttachmentViewModel
    {
        public int? Id { get; set; } 
        public string Name { get; set; }
        public string Title { get; set; }
        public int? DocumentType { get; set; }
        public string SN { get; set; }
        public string DocumentNumber { get; set; }
        public string Activity { get; set; }
    }

    public class CreatedFileInfo<TPostedAttachment>
            where TPostedAttachment : IPostedAttachmentViewModel
    {
        public CreatedFileInfo(
            TPostedAttachment postedAttachment,
            string createdBy,
            long fileSize,
            string contentType,
            string createdLocation
            )
        {
            PostedAttachment = postedAttachment;
            CreatedBy = createdBy;
            FileSize = fileSize;
            FileSizeText = fileSize.SizeSuffix();
            ContentType = contentType;
            CreatedLocation = createdLocation;
        }
        public TPostedAttachment PostedAttachment { get; set; }
        public string CreatedBy { get; set; }
        public long FileSize { get; set; }
        public string FileSizeText { get; set; }
        public string ContentType { get; set; }
        public string CreatedLocation { get; set; }
    }
}
