﻿namespace TheMall.MPS.ViewModels
{
    public class DocumentTypeViewModel
    {
        public int Value { get; set; }
        public string Name { get; set; }
        public bool IsRequired { get; set; }
    }
}
