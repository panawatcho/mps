﻿using System.Collections.Generic;

namespace TheMall.MPS.ViewModels.Menu
{
    public class MenuItemViewModel
    {
        public string Id { get; set; }
        public string Html { get; set; }
        public string Link { get; set; }
        public bool NewTab { get; set; }
        public string IconClass { get; set; }
        public string Text { get; set; }
        public virtual IEnumerable<MenuItemViewModel> Children { get; set; }
    }
}
