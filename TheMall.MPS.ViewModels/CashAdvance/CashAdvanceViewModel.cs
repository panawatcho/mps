﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.Models.Master;
using TheMall.MPS.ViewModels.Abstracts;
using TheMall.MPS.ViewModels.Master;

namespace TheMall.MPS.ViewModels.CashAdvance
{
    public class CashAdvanceViewModel : BaseMpsWorkflowViewModel
    {
        public ExpenseTopicViewModel ExpenseTopics { get; set; }
        public APCodeByProposalLineViewModel APCode { get; set; }
        public DateTime? AdvanceDate { get; set; }
        public DateTime? AdvanceDueDate { get; set; }
        public PlaceViewModel Branch { get; set; }
        public decimal Budget { get; set; }
        public decimal BudgetPlan { get; set; }
        public string Description { get; set; }
        public int? RequestForEmpId { get; set; }
        public string RequestForUserName { get; set; }
        public int ProposalLineID { get; set; }
        public EmployeeViewModel RequesterFor { get; set; }
        public ProposalInfoViewModel ProposalRef { get; set; }
        //public IEnumerable<EmployeeViewModel> AccountingData { get; set; }
        public IEnumerable<CashAdvanceLineViewModel> CashAdvanceLines { get; set; }
        public IEnumerable<CashAdvanceApprovalViewModel> CashAdvanceApproval { get; set; }
        public int ProposalLineId { get; set; }
        public decimal BudgetTrans { get; set; }

        public CompanyViewModel Company { get; set; }
    }
}
