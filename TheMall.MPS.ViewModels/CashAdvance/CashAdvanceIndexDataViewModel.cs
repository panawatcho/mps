﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.ViewModels.Abstracts;
using TheMall.MPS.ViewModels.Master;

namespace TheMall.MPS.ViewModels.CashClearing
{
    public class CashAdvanceIndexDataViewModel 
    {
        public int Id { get; set; }
        public string ExpenseTopicCode { get; set; }
        public string ExpenseTopicName { get; set; }
        public string APCodeCode { get; set; }
        public string APCodeName { get; set; }
        public int ProposalRefID { get; set; }
        public string ProposalRefDocumentNumber { get; set; }
        public string DocumentNumber { get; set; }
        public string Title { get; set; }
        public DateTime? AdvanceDueDate { get; set; }
        public string Description { get; set; }
        public decimal Budget { get; set; }
        public string DocumentStatus { get; set; }
        public string CashClearingDocumentNumber { get; set; }
        public DateTime? CreatedDate { get; set; }
        public bool CheckComplete { get; set; }
        public int CashClearingId { get; set; }
        public string CashClearingStatus { get; set; }
        public int CashClearingStatusFlag { get; set; }
        public string CreatedBy { get; set; }
        public int StatusFlag { get; set; }
        public string RequestForUserName { get; set; }
        public string Status { get; set; }
    }
}
