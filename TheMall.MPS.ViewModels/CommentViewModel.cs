﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheMall.MPS.ViewModels
{
    public class CommentViewModel
    {
        public int Id { get; set; }
        public string CommentedBy { get; set; }
        public DateTime CommentedDate { get; set; }
        public string Comment { get; set; }
        public string Action { get; set; }
        public string Activity { get; set; }
        public string SN { get; set; }
    }
}
