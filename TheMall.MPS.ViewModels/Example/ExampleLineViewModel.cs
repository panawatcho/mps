﻿using System.Collections.Generic;
using TheMall.MPS.ViewModels.Abstracts;

namespace TheMall.MPS.ViewModels.Example
{
    public class ExampleLineViewModel : BaseLineViewModel
    {
        public string Name { get; set; }
        public IEnumerable<AttachmentViewModel> Attachments { get; set; }
    }
}
