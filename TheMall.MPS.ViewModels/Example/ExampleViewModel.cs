﻿using System.Collections.Generic;
using TheMall.MPS.ViewModels.Interfaces;

namespace TheMall.MPS.ViewModels.Example
{
    public class ExampleViewModel : IViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public IEnumerable<ExampleLineViewModel> Lines { get; set; }
        public IEnumerable<AttachmentViewModel> Attachments { get; set; }
    }
}
