﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.ViewModels.Interfaces;

namespace TheMall.MPS.ViewModels.Master
{
    public class APCodeByProposalLineViewModel : IMasterViewModel
    {
        public string ExpenseTopicCode { get; set; }
        public string APCode { get; set; }
        public string APName { get; set; }
        public string Description { get; set; }
        public bool CheckBudget { get; set; }
        public bool SendMail { get; set; }
        public decimal? Order { get; set; }
        public decimal? BudgetPlan { get; set; }
        public decimal? TotalBudgetPlan { get; set; }
        public string DescriptionLine { get; set; }
        public int ProposalLineId { get; set; }
        public string UnitCode { get; set; }
    }
}
