﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheMall.MPS.ViewModels.Abstracts;
using TheMall.MPS.ViewModels.Interfaces;
using TheMall.MPS.ViewModels.Master;
using TheMall.MPS.ViewModels.PettyCash;
using TheMall.MPS.ViewModels.Proposal;

namespace TheMall.MPS.ViewModels.Memo
{
    public class MemoViewModel : BaseMpsWorkflowViewModel, IMasterViewModel
    {

        public ExpenseTopicViewModel ExpenseTopic { get; set; }
        public APCodeByProposalLineViewModel APCode { get; set; }
        public decimal? BudgetAPCode { get; set; }
        public decimal? BudgetDetail { get; set; }
        public TypeMemoViewModel MemoType { get; set; }
        public string PaymentName { get; set; }
        public string PaymentDepartment { get; set; }
        public ProposalInfoViewModel UseProposal { get; set; }
        public string Description { get; set; }
        public UnitCodeViewModel PayToUnitCode { get; set; }
        public ProposalInfoViewModel ProposalRef { get; set; }
        public IEnumerable<MemoLineViewModel> MemoLines { get; set; } 
        public virtual IEnumerable<MemoApprovalViewModel> MemoApproval { get; set; }
        public bool SpecialType { get; set; }
        public PlaceViewModel Branch { get; set; }
        public int ProposalLineId { get; set; }
        public decimal BudgetTrans { get; set; }
        public IncomeDepositViewModel IncomeDepositViewModel { get; set; }

    }
}
