﻿using System.Collections.Generic;

namespace TheMall.MPS.ViewModels.Task
{
    public sealed class ProcInstStatusViewModel
    {
        public int ProcInstId { get; set; }
        public string Folio { get; set; }
        public string ProcDescription { get; set; }
        public IEnumerable<ActInstStatusViewModel> ActInsts { get; set; }
    }
}
