﻿using System.Collections.Generic;

namespace TheMall.MPS.ViewModels.FileManager
{
    public class DirectoryViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public IEnumerable<DirectoryViewModel> Subfolders { get; set; } 
    }
}
