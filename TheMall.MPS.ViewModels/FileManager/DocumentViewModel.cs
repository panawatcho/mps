﻿using System.Collections.Generic;

namespace TheMall.MPS.ViewModels.FileManager
{
    public class DocumentViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int DirectoryId { get; set; }
    }
}
