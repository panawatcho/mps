﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace TheMall.MPS.ViewModels.InterfaceToMprocurement
{
    [XmlType("Table")]
    public class TableViewModel
    {
        [XmlAttribute("TableOrStructureName")]
        public string Name { get; set; }
        public object Data { get; set; }
    }
}
