﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheMall.MPS.ViewModels.InterfaceToMprocurement
{
    public class ReturnMessage
    {
        public bool Status { get; set; }
        public string Message { get; set; }
    }
}
