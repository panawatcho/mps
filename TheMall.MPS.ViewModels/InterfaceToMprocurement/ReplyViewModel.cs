﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace TheMall.MPS.ViewModels.InterfaceToMprocurement
{
    [XmlRoot(ElementName = "Reply")]
    public class ReplyViewModel
    {
        [XmlAttribute("APPROVED")]
        public bool Approved { get; set; }
        [XmlAttribute("tablename")]
        public string TableName { get; set; }
        [XmlElement("Info")]
        public InfoViewModel Info { get; set; }
        [XmlElement("Param")]
        public ReplyParamViewModel Param { get; set; }
        [XmlElement("APCODELIST",Type = typeof(List<M_APCodeViewModel>))]
        [XmlElement("EXPENSETOPICLIST", Type = typeof(List<M_ExpenseTopicViewModel>))]
        [XmlElement("PROPOSALIDLIST", Type = typeof(List<M_ProposalIDViewModel>))]
        [XmlElement("BU_PROB_APLIST", Type = typeof(List<M_ProposalLineViewModel>))]
        [XmlElement("ALLOCATIONCODELIST", Type = typeof(List<M_AllocationBasisViewModel>))]
        public object Data { get; set; }
    }
}
