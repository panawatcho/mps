﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace TheMall.MPS.ViewModels.InterfaceToMprocurement
{
    [XmlRoot(ElementName = "Info")]
    public class InfoViewModel
    {
        [XmlAttribute("CODE")]
        public string Code { get; set; }
        [XmlAttribute("DESC")]
        public string Description { get; set; }
        [XmlAttribute("DESC_EXT")]
        public string DescriptionExt { get; set; }
        [XmlAttribute("LINE")]
        public string Line { get; set; }
        [XmlAttribute("TYPE")]
        public string Type { get; set; }
    }
}
