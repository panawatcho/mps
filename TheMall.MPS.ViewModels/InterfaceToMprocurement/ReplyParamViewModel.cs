﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace TheMall.MPS.ViewModels.InterfaceToMprocurement
{
    public class ReplyParamViewModel
    {
        public string REQ_NUM { get; set; }
        public string PO_NUM { get; set; }
        public string GR_NUM { get; set; }
    }
}
