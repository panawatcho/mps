﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace TheMall.MPS.ViewModels.InterfaceToMprocurement.GR
{
    [XmlRoot(ElementName = "Structure")]
    public class GRHeaderViewModel
    {
        [XmlAttribute("TableOrStructureName")]
        public string Name { get; set; }
        public string RECEIPT_ID { get; set; }
        public string PO_ID { get; set; }
        public string PO_NUM { get; set; }
        public string DOC_DATE { get; set; }
        public string POST_DATE { get; set; }
        public string DELIV_NOTE { get; set; }
        public string HEADER_TXT { get; set; }
        public string BILL_LAD { get; set; }
        public string SLIP_NUM { get; set; }
    }
}
