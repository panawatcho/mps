﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace TheMall.MPS.ViewModels.InterfaceToMprocurement.GR
{
    public class GRItemViewModel
    {
        [XmlAttribute("TableOrStructureName")]
        public string Name { get; set; }
        //GR_ITEMS
        public string ITEM_NUM { get; set; }
        public string LOCATION { get; set; }
        public string QUANTITY { get; set; }
        public string DELIV_CMPL { get; set; }
        public string REASON { get; set; }
        public string ITEM_TEXT { get; set; }
        public string MOVEMENTTYPE { get; set; }
        public string ACTUALPRICE { get; set; }
        public string REFERNUM { get; set; }
    }
}
