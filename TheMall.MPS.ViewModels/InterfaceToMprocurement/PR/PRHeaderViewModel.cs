﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace TheMall.MPS.ViewModels.InterfaceToMprocurement.PR
{
    [XmlRoot(ElementName = "Structure")]
    public class PRHeaderViewModel
    {
        [XmlAttribute("TableOrStructureName")]
        public string Name { get; set; }
        public string IP_ID { get; set; }
        public string REQ_ID { get; set; }
        public string PR_NUM { get; set; }
        public string SUBCOMMAND { get; set; }
        public string REQUISITIONER { get; set; }
        public string BS_REQID { get; set; }
        public string REQ_NAME { get; set; }
        public string COMPANY_CODE { get; set; }
        public string POST_DATE { get; set; }
    }
}
