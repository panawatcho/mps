﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace TheMall.MPS.ViewModels.InterfaceToMprocurement.PR
{
    public class PRItemViewModel
    {
        [XmlAttribute("TableOrStructureName")]
        public string Name { get; set; }
        //REQ_ITEMS
        public string ITEM_NUM { get; set; }
        public string QUANTITY { get; set; }
        public string ITEM_DESC { get; set; }
        public string UOM { get; set; }
        public string OLD_PRICE { get; set; }
        public string PRICE { get; set; }
        public string OLD_TAX_AMOUNT { get; set; }
        public string TAX_AMOUNT { get; set; }
        public string OLD_CURRENCY_CODE { get; set; }
        public string CURRENCY_CODE { get; set; }
        public string DELIV_DATE { get; set; }
        public string LONGDESC { get; set; }
        public string SHIP_INST { get; set; }
        public string PAYMENTDESC { get; set; }
        public string ACTUALPRICE { get; set; }
        //REQ_ACCT_ASSGNS
        public string COSTCENTER { get; set; }
        public string PODOCTYPE { get; set; }
        public string PURTYPE { get; set; }
        public string GLACCOUNT { get; set; }
        public string DEPARTMENT { get; set; }
        public string PURGROUP { get; set; }
        public string PROPOSALID { get; set; }
        public string APCODE { get; set; }
        public string APDESC { get; set; }
        public string REQUESTFORID { get; set; }
        public string OLD_BUDGET_CODE { get; set; }
        public string OLD_ORG_ID { get; set; }
    }
}
