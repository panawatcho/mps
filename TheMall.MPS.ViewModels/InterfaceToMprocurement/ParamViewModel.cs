﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace TheMall.MPS.ViewModels.InterfaceToMprocurement
{
    [XmlRoot(ElementName = "Param")]
    public class ParamViewModel
    {
        [XmlElement("KEYWORD")]
        public string Keyword { get; set; }
    }
}
