﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheMall.MPS.ViewModels.InterfaceToMprocurement
{
    public class RequestViewModel
    {
        public string ProposalNumber { get; set; }
        public string ExpenseTopic { get; set; }
        public string APCode { get; set; }
        public string UnitCode { get; set; }
        public string APDescription { get; set; }
        public decimal Amount { get; set; }
        public string DocumentType { get; set; }
        public string DocumentNumber { get; set; }
        public string DocumentStatus { get; set; }
        public string Status { get; set; }
        public string EmployeeId { get; set; }
        public int? DocumentId { get; set; }

    }
}
