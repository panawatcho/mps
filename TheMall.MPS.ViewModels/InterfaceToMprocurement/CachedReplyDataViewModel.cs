﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace TheMall.MPS.ViewModels.InterfaceToMprocurement
{
    [XmlRoot(ElementName = "CachedReplyData")]
    public class CachedReplyDataViewModel
    {
        [XmlElement("tablename")]
        public string TableName { get; set; }
    }
}
