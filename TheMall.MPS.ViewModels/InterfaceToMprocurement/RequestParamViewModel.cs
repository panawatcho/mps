﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace TheMall.MPS.ViewModels.InterfaceToMprocurement
{
    [XmlRoot(ElementName = "Request")]
    public class RequestParamViewModel
    {
        [XmlAttribute("RFCFuncName")]
        public string RFCFuncName { get; set; }
        [XmlAttribute("RFCWrapperFuncName")]
        public string RFCWrapperFuncName { get; set; }
        [XmlElement("CachedReplyData")]
        public CachedReplyDataViewModel CachedReplyData { get; set; }
        [XmlElement("Param")]
        public ParamViewModel Param { get; set; }
        [XmlElement("Structure")]
        public StructureViewModel Structure { get; set; }
        [XmlElement("Table")]
        public TableViewModel[] Table { get; set; }
    }
}
