﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace TheMall.MPS.ViewModels.InterfaceToMprocurement
{
    [XmlType(TypeName = "PROPOSALID")]
    public class M_ProposalIDViewModel : MasterTableViewModel
    {
        [XmlElement("DATA001")]
        public string ProposalNumber { get; set; }
        [XmlElement("DATA002")]
        public string Description { get; set; }
        //public List<AllocateBasis> AllocateBasis { get; set; }

    }

    //public class AllocateBasis
    //{
    //    public ExpenseTopicList ExpenseTopic { get; set; }
    //    public APCodeList APCode { get; set; }
    //    public UnitCodeList UnitCode { get; set; }
    //}
    [XmlType(TypeName = "EXPENSETOPIC")]
    public class M_ExpenseTopicViewModel : MasterTableViewModel
    {
        [XmlElement("DATA001")]
        public string ExpenseTopicCode { get; set; }
        [XmlElement("DATA002")]
        public string ExpenseTopicName { get; set; }
        //[XmlElement("DOC003")]
        //public string Description { get; set; }
        // public List<APCodeList> APCodeList { get; set; }
    }
    [XmlType(TypeName = "APCODE")]
    public class M_APCodeViewModel : MasterTableViewModel
    {
        [XmlElement("DATA001")]
        public string APCode { get; set; }
        [XmlElement("DATA002")]
        public string Description { get; set; }
        // public string APName { get; set; }
        // public string ExpenseTopicCode { get; set; }

    }

    [XmlType(TypeName = "BU_PROB_AP")]
    public class M_ProposalLineViewModel : MasterTableViewModel
    {
        [XmlElement("DATA001")]
        public string ProposalNumber { get; set; }
        [XmlElement("DATA002")]
        public string PropUnitCode { get; set; }
        [XmlElement("DATA003")]
        public string UnitCode { get; set; }
        [XmlElement("DATA004")]
        public string APCode { get; set; }
        [XmlElement("DATA005")]
        public string AllocationBasis { get; set; }
        [XmlElement("DATA006")]
        public string APDescription { get; set; }

    }
    public class UnitCodeList
    {
        public string UnitCode { get; set; }
        public string UnitName { get; set; }

    }

    [XmlType(TypeName = "ALLOCATIONCODE")]
    public class M_AllocationBasisViewModel : MasterTableViewModel
    {
        [XmlElement("DATA001")]
        public string AllocCode { get; set; }
        [XmlElement("DATA002")]
        public string Description { get; set; }

    }

    public class MasterTableViewModel
    {
        [XmlAttribute("TableOrStructureName")]
        public string TableName { get; set; }
    }
}
