﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace TheMall.MPS.ViewModels.InterfaceToMprocurement.PO
{
    [XmlRoot(ElementName = "Structure")]
    public class POHeaderViewModel
    {
        [XmlAttribute("TableOrStructureName")]
        public string Name { get; set; }
        public string IP_ID { get; set; }
        public string PO_ID { get; set; }
        public string PO_NUM { get; set; }
        public string REQ_ID { get; set; }
        public string PR_NUM { get; set; }
        public string PO_DATE { get; set; }
        public string CLOSEPO { get; set; }
    }
}
