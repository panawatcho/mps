﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace TheMall.MPS.ViewModels.InterfaceToMprocurement
{
    [XmlRoot(ElementName = "Structure")]
    public class StructureViewModel
    {
        [XmlAttribute("TableOrStructureName")]
        public string Name { get; set; }
        public object Data { get; set; }
    }
}
